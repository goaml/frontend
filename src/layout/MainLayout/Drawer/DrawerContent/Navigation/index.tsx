import { useEffect, useLayoutEffect, useState } from 'react';

// material-ui
import { Box, Typography, useMediaQuery } from '@mui/material';
import { useTheme } from '@mui/material/styles';

// project import
import { HORIZONTAL_MAX_ITEM } from 'config';
import useConfig from 'hooks/useConfig';
import { dispatch, useSelector } from 'store';
import NavGroup from './NavGroup';
// import { Menu } from 'menu-items/dashboard';
// import menuItem from 'menu-items';

// util
import { TransformResponseToMenuObject } from 'utils/transform-response-to-menu-object';

// types
import { useMenu } from 'hooks/MenuContext';
import useAuth from 'hooks/useAuth';
import { fetchGetUserDynamicDrawerSuccess } from 'store/reducers/user-dynamic-drawer';
import { MenuOrientation } from 'types/config';
import { NavItemType } from 'types/menu';

// ==============================|| DRAWER CONTENT - NAVIGATION ||============================== //

const Navigation = () => {
  const theme = useTheme();

  const downLG = useMediaQuery(theme.breakpoints.down('lg'));

  const { menuOrientation } = useConfig();
  const { drawerOpen } = useSelector((state) => state.menu);
  const { user } = useAuth()
  const { userDynamicDrawer } = useSelector(state => state.userDynamicDrawer)

  const [selectedItems, setSelectedItems] = useState<string | undefined>('');
  const [selectedLevel, setSelectedLevel] = useState<number>(0);
  const [menuItems, setMenuItems] = useState<{ items: NavItemType[] }>({ items: [] });

  //static
  // useLayoutEffect(() => {
  //   setMenuItems(menuItem);
  //   // eslint-disable-next-line
  // }, [menuItem]);

  //dynamic 
  const { menuItems: menuOptions, updateMenuItems } = useMenu();

  useEffect(() => {
    if (!user?.userId) return
    dispatch(fetchGetUserDynamicDrawerSuccess({ moduleId: 1, userId: user?.userId! }))
  }, [user])

  useLayoutEffect(() => {
    if (!userDynamicDrawer) return;
    const transformedMenuItems = TransformResponseToMenuObject(userDynamicDrawer);
    updateMenuItems(transformedMenuItems);
  }, [userDynamicDrawer]);

  useLayoutEffect(() => {

    setMenuItems({ items: [...menuOptions.items] });
    // eslint-disable-next-line
  }, [menuOptions]);

  const isHorizontal = menuOrientation === MenuOrientation.HORIZONTAL && !downLG;

  const lastItem = isHorizontal ? HORIZONTAL_MAX_ITEM : null;
  let lastItemIndex = menuItems.items.length - 1;
  let remItems: NavItemType[] = [];
  let lastItemId: string;

  // first it checks menu item is more than giving HORIZONTAL_MAX_ITEM after that get lastItemid by giving horizontal max
  // item and it sets horizontal menu by giving horizontal max item lastly slice menuItem from array and set into remItems

  if (lastItem && lastItem < menuItems.items.length) {
    lastItemId = menuItems.items[lastItem - 1].id!;
    lastItemIndex = lastItem - 1;
    remItems = menuItems.items.slice(lastItem - 1, menuItems.items.length).map((item) => ({
      title: item.title,
      elements: item.children,
      icon: item.icon
    }));
  }

  const navGroups = menuItems.items.slice(0, lastItemIndex + 1).map((item) => {
    switch (item.type) {
      case 'group':
        return (
          <NavGroup
            key={item.id}
            setSelectedItems={setSelectedItems}
            setSelectedLevel={setSelectedLevel}
            selectedLevel={selectedLevel}
            selectedItems={selectedItems}
            lastItem={lastItem!}
            remItems={remItems}
            lastItemId={lastItemId}
            item={item}
          />
        );
      default:
        return (
          <Typography key={item.id} variant="h6" color="error" align="center">
            Fix - Navigation Group
          </Typography>
        );
    }
  });

  return (
    <Box
      sx={{
        pt: drawerOpen ? (isHorizontal ? 0 : 2) : 0,
        '& > ul:first-of-type': { mt: 0 },
        display: isHorizontal ? { xs: 'block', lg: 'flex' } : 'block'
      }}
    >
      {navGroups}
    </Box>
  );
};

export default Navigation;
