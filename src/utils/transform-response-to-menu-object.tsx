// third-party
import { FormattedMessage } from 'react-intl';

// assets
import { RedEnvelopeOutlined, SwitcherOutlined, MergeCellsOutlined, UsergroupAddOutlined, FileProtectOutlined, AccountBookOutlined, InsertRowBelowOutlined, FieldTimeOutlined, AreaChartOutlined, MailOutlined, FileSearchOutlined, ApartmentOutlined, BankOutlined, CheckSquareOutlined, ClusterOutlined, DashboardOutlined, DatabaseOutlined, DollarCircleOutlined, EnvironmentOutlined, FileSyncOutlined, FileTextOutlined, FormOutlined, GlobalOutlined, HddOutlined, HeartOutlined, IdcardOutlined, LineChartOutlined, MenuOutlined, ProfileOutlined, PullRequestOutlined, ReconciliationOutlined, SecurityScanOutlined, SettingOutlined, ShopOutlined, SolutionOutlined, StopOutlined, SyncOutlined, TeamOutlined, UserAddOutlined, UserOutlined, UserSwitchOutlined, PartitionOutlined } from '@ant-design/icons';

// types
import { NavItemType } from 'types/menu';
import { UserDynamicDrawer } from 'types/user-dynamic-drawer';


// icons
const icons = {
    DashboardOutlined,
    DatabaseOutlined,
    FileSyncOutlined,
    IdcardOutlined,
    LineChartOutlined,
    UserOutlined,
    FormOutlined,
    CheckSquareOutlined,
    FileTextOutlined,
    MenuOutlined,
    SettingOutlined,
    SecurityScanOutlined,
    SolutionOutlined,
    StopOutlined,
    UserSwitchOutlined,
    AccountBookOutlined,
    ShopOutlined,
    BankOutlined,
    TeamOutlined,
    HeartOutlined,
    EnvironmentOutlined,
    ClusterOutlined,
    ApartmentOutlined,
    GlobalOutlined,
    DollarCircleOutlined,
    UserAddOutlined,
    HddOutlined,
    ProfileOutlined,
    PullRequestOutlined,
    ReconciliationOutlined,
    SyncOutlined,
    FileSearchOutlined,
    MailOutlined,
    AreaChartOutlined,
    FieldTimeOutlined,
    InsertRowBelowOutlined,
    FileProtectOutlined,
    UsergroupAddOutlined,
    PartitionOutlined,
    MergeCellsOutlined,
    SwitcherOutlined,
    RedEnvelopeOutlined
};

export const TransformResponseToMenuObject = (data: UserDynamicDrawer[]): NavItemType[] => {
    const sortedData = [...data].sort((a, b) => {
        if (a.groupSeqNo !== b.groupSeqNo) {
            return a.groupSeqNo! - b.groupSeqNo!;
        }
        if (a.subGroupSeqNo !== b.subGroupSeqNo) {
            return a.subGroupSeqNo! - b.subGroupSeqNo!;
        }
        return a.menuSeqNo! - b.menuSeqNo!;
    });

    const menuObject: { [key: string]: NavItemType } = {};

    sortedData.forEach((item) => {
        const { group, subGroup, title, url, icon } = item;

        let antDesignIcon;

        switch (icon) {
            case 'DashboardOutlined':
                antDesignIcon = icons.DashboardOutlined;
                break;
            case 'DatabaseOutlined':
                antDesignIcon = icons.DatabaseOutlined;
                break;
            case 'FileSyncOutlined':
                antDesignIcon = icons.FileSyncOutlined;
                break;
            case 'IdcardOutlined':
                antDesignIcon = icons.IdcardOutlined;
                break;
            case 'LineChartOutlined':
                antDesignIcon = icons.LineChartOutlined;
                break;
            case 'UserOutlined':
                antDesignIcon = icons.UserOutlined;
                break;
            case 'FormOutlined':
                antDesignIcon = icons.FormOutlined;
                break;
            case 'CheckSquareOutlined':
                antDesignIcon = icons.CheckSquareOutlined;
                break;
            case 'FileTextOutlined':
                antDesignIcon = icons.FileTextOutlined;
                break;
            case 'MenuOutlined':
                antDesignIcon = icons.MenuOutlined;
                break;
            case 'SettingOutlined':
                antDesignIcon = icons.SettingOutlined;
                break;
            case 'SecurityScanOutlined':
                antDesignIcon = icons.SecurityScanOutlined;
                break;
            case 'SolutionOutlined':
                antDesignIcon = icons.SolutionOutlined;
                break;
            case 'StopOutlined':
                antDesignIcon = icons.StopOutlined;
                break;
            case 'UserSwitchOutlined':
                antDesignIcon = icons.UserSwitchOutlined;
                break;
            case 'ShopOutlined':
                antDesignIcon = icons.ShopOutlined;
                break;
            case 'BankOutlined':
                antDesignIcon = icons.BankOutlined;
                break;
            case 'TeamOutlined':
                antDesignIcon = icons.TeamOutlined;
                break;
            case 'HeartOutlined':
                antDesignIcon = icons.HeartOutlined;
                break;
            case 'EnvironmentOutlined':
                antDesignIcon = icons.EnvironmentOutlined;
                break;
            case 'ClusterOutlined':
                antDesignIcon = icons.ClusterOutlined;
                break;
            case 'ApartmentOutlined':
                antDesignIcon = icons.ApartmentOutlined;
                break;
            case 'GlobalOutlined':
                antDesignIcon = icons.GlobalOutlined;
                break;
            case 'DollarCircleOutlined':
                antDesignIcon = icons.DollarCircleOutlined;
                break;
            case 'UserAddOutlined':
                antDesignIcon = icons.UserAddOutlined;
                break;
            case 'HddOutlined':
                antDesignIcon = icons.HddOutlined;
                break;
            case 'ProfileOutlined':
                antDesignIcon = icons.ProfileOutlined;
                break;
            case 'PullRequestOutlined':
                antDesignIcon = icons.PullRequestOutlined;
                break;
            case 'ReconciliationOutlined':
                antDesignIcon = icons.ReconciliationOutlined;
                break;
            case 'SyncOutlined':
                antDesignIcon = icons.SyncOutlined;
                break;
            case 'FileSearchOutlined':
                antDesignIcon = icons.FileSearchOutlined;
                break;
            case 'MailOutlined':
                antDesignIcon = icons.MailOutlined;
                break;
            case 'AreaChartOutlined':
                antDesignIcon = icons.AreaChartOutlined;
                break;
            case 'FieldTimeOutlined':
                antDesignIcon = icons.FieldTimeOutlined;
                break;
            case 'InsertRowBelowOutlined':
                antDesignIcon = icons.InsertRowBelowOutlined;
                break;
            case 'FileProtectOutlined':
                antDesignIcon = icons.FileProtectOutlined;
                break;
            case 'UsergroupAddOutlined':
                antDesignIcon = icons.UsergroupAddOutlined;
                break;
            case 'PartitionOutlined':
                antDesignIcon = icons.PartitionOutlined;
                break;
            case 'AccountBookOutlined':
                antDesignIcon = icons.AccountBookOutlined;
                break;
            case 'MergeCellsOutlined':
                antDesignIcon = icons.MergeCellsOutlined;
                break;
            case 'SwitcherOutlined':
                antDesignIcon = icons.SwitcherOutlined;
                break;
            case 'RedEnvelopeOutlined':
                antDesignIcon = icons.RedEnvelopeOutlined;
                break;
            // Add more cases for other icons if needed
            default:
                // Handle unknown icons or set antDesignIcon to null
                antDesignIcon = null;
                break;
        }

        if (!menuObject[group!]) {
            menuObject[group!] = {
                id: group!.toLowerCase(),
                title: <FormattedMessage id={group!} />,
                type: 'group',
                children: [],
            };
        }

        const groupItem = menuObject[group!];

        if (subGroup) {
            let subGroupItem = groupItem.children?.find((sub) => sub.id === subGroup.toLowerCase());

            if (!subGroupItem) {
                subGroupItem = {
                    id: subGroup.toLowerCase(),
                    title: <FormattedMessage id={subGroup} />,
                    type: 'collapse',
                    icon: antDesignIcon,// You might want to set a proper icon
                    children: [],
                };

                groupItem.children?.push(subGroupItem);
            }

            subGroupItem.children?.push({
                id: title!.toLowerCase().replace(/\s/g, '-'),
                title: <FormattedMessage id={title} />,
                type: 'item',
                url,
                icon: antDesignIcon
            });
        } else {
            groupItem.children?.push({
                id: title!.toLowerCase().replace(/\s/g, '-'),
                title: <FormattedMessage id={title} />,
                type: 'item',
                url,
                icon: antDesignIcon, // You might want to set a proper icon
            });
        }
    });

    return Object.values(menuObject);
};
