export type Ingredient = {
};

export type ReportCodeListResType = {
    pagination?: paginationType
    result?: ReportCodesType[]
};

export type ReportCodeResType = { 
    reportCode?: string,
    description?:string,
};

export type ReportCodeReqType = { 
    
    reportCodeId?: number,
    reportCode?: string,
    description?:string,
};

export interface ReportTypeStateProps {
    reportCodeList: ReportCodeListResType | null;
    reportCode: ReportCodeResType | null;
    error: object | string | null;
    Rsuccess: object | string | null;
    isLoading: boolean;
    selectreportCode: ReportCodeType[] |null
    xmlData: string| null, 
    isActionSuccess:object | string | null;
    reportTypeValidate: string | null;
    xmlDataIsValidated: xmlDataIsValidated | null;
    downloadCsv: downloadCsvProps | null;
}

export interface DefaultRootStateProps {
    reportCode: ReportTypeStateProps;
}

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type ReportCodesType = {
    reportCode?: string,
    reportCodeId?: number,
    description?:string,
    
}

export type ReportCodeType = {
    reportCodeId?: number,
    reportCode?: string,
    description?:string,
    reportTypeId?:number,
}

export type xmlDataIsValidated = {
    isValidated?: boolean,
    validationMessage?: string
}

export interface downloadCsvProps {
    reportTypeId?: number,
    reportCode?: string
}