export type ForeignCurrency ={
    currenciesId?: number,
      foreignAmount?: number,
      foreignCurrencyId?: number,
      foreignExchangeRate?: number,
      isActive?: boolean,
      rcurrency?: {
        createdBy?: string,
        createdOn?: string,
        currenciesCode?: string,
        currenciesId?: number,
        description?: string,
        isActive?: boolean,
        rstatus?: {
          description?: string,
          isActive?: boolean,
          statusCode?: string,
          statusId?: number
        },
        statusId?: number,
        updatedBy?: string,
        updatedOn?: string
      },
      rstatus?: {
        description?: string,
        isActive?: boolean,
        statusCode?: string,
        statusId?: number
      },
      statusId?: number

  }

export type ForeignCurrencyList = {
    pagination?: {
      count?: number;
      from?: number;
      to?: number;
      total?: number;
    }
    result?: Array<ForeignCurrency>;
  }
export interface ForeignCurrencyStateProps {
    foreignCurrencyList: ForeignCurrencyList | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean
}

export interface DefaultRootStateProps {
    foreignCurrency: ForeignCurrencyStateProps;
}

export interface queryParamsProps {
    page: number
    per_page: number
    sort: string
    direction: "asc" | "desc"
    search: string
}