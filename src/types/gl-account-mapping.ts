export type Ingredient = {
};

export type GLAccountMappingListResType = {
    pagination?: paginationType
    result?: GLAccountsType[]

};

export type GLAccountMappingResType = { 
    GLAccountId?: number,
    GLAccountName?: string,
    GLAccountDescription?: string,
    isActive?: boolean
    goamlTrxCodeId?:number,
        goAmlTrxCodes?:{
        transactionCodeName?:string,
    }
};

export type GLAccountMappingReqType = {
    institutionName?: string,
    mappingAccount?: string,
    rptCode?: string,
    swift?: string
};

export interface GLAccountMappingStateProps {
    GLAccountList: GLAccountMappingListResType | null;
    GLAccount: GLAccountMappingResType | null;
    error: object | string | null;
    GLsuccess: object | string | null;
    isLoading: boolean;
    selectGLAccountType: GLAccountType[] | null;
    isActionSuccess: object | string | null;
}


export interface DefaultRootStateProps {
    GLAccount: GLAccountMappingStateProps;
}

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type GLAccountsType = {
    
    accountStatusTypeId?: number,
    accountTypeId?: number,
    branch?: string,
    currenciesId?: number,
    glAccount?: string,
    glAccountMappingId?: number,
    institutionName?: string,
    isActive?: boolean,
    mappingAccount?: string,
    rptCode?: string,
    rstatus?: {
      description?: string,
      isActive?: boolean,
      statusCode?: string,
      statusId?: number
    },
    statusId?: number,
    swift?: string
    
    
}

export type GLAccountType = {
    accountStatusTypeId?: number,
    accountTypeId?: number,
    branch?: string,
    currenciesId?: number,
    glAccount?: string,
    glAccountMappingId?: number,
    institutionName?: string,
    isActive?: boolean,
    mappingAccount?: string,
    rptCode?: string,
    rstatus?: {
        description?: string,
        isActive?: boolean,
        statusCode?: string,
        statusId?: number
    },
    statusId?: number,
    swift?: string
}
