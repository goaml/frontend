
export interface UserLoginTypeStateProps {
    selectUserLoginTypes: UserLoginType[] | null;
    error: object | string | null;
    typesuccess: object | string | null;
    isLoading: boolean
}

export interface DefaultRootStateProps {
    userLoginType: UserLoginTypeStateProps;
}


export type UserLoginTypesType = {
    isActive: boolean,
    loginTypeId: number,
    typeCode: string,
    typeDescription: string

}

// Define the type for a single user Type
export type UserLoginType = {
    isActive: boolean,
    loginTypeId: number,
    typeCode: string,
    typeDescription: string
}

// Define the type for the 'user' slice of the state
export interface UserState {
    Types: UserLoginType[]; // Assuming 'Types' is where the user Types are stored
    // Add more properties if necessary
}

// Define RootState to include the 'user' slice of the state
export interface UserLoginTypeRootState {
    userLoginType: UserState;
    // Add other slices of the state as needed
}

export type UserLoginTypeOptionType = {
    typeCode: string,
    typeDescription: string
};