export type transPersonIdentification ={
      comments?: string,
      expiryDate?: string,
      identifierTypeId?: number,
      isActive?: boolean,
      issueCountryCodeId?: number,
      issueDate?: string,
      issuedBy?: string,
      number?: string,
      personIdentificationId?: number,
      rcountryCodes?: {
        countryCodeCode?: string,
        countryCodeId?: number,
        createdBy?: string,
        createdOn?: string,
        description?: string,
        isActive?: boolean,
        rstatus?: {
          description?: string,
          isActive?: boolean,
          statusCode?: string,
          statusId?: number
        },
        statusId?: number,
        updatedBy?: string,
        updatedOn?: string
      },
      ridentifierType?: {
        createdBy?: string,
        createdOn?: string,
        description?: string,
        identifierTypeCode?: string,
        identifierTypeId?: number,
        isActive?: boolean,
        rstatus?: {
          description?: string,
          isActive?: boolean,
          statusCode?: string,
          statusId?: number
        },
        statusId?: number,
        updatedBy?: string,
        updatedOn?: string
      },
      rstatus?: {
        description?: string,
        isActive?: boolean,
        statusCode?: string,
        statusId?: number
      },
      statusId?: number
}

export type transPersonIdentificationList = {
    pagination?: {
      count?: number;
      from?: number;
      to?: number;
      total?: number;
    }
    result?: Array<transPersonIdentification>;
  }
export interface transPersonIdentificationStateProps {
    transPersonIdentificationList: transPersonIdentificationList | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean
}

export interface DefaultRootStateProps {
    transPersonIdentification: transPersonIdentificationStateProps;
}

export interface queryParamsProps {
    page: number
    per_page: number
    sort: string
    direction: "asc" | "desc"
    search: string
}