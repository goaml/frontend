export type Ingredient = {
};

export type UserRoleListResType = {
    pagination?: paginationType
    result?: UserRolesType[]
};

export type UserRoleResType = { 
    userRoleId?: number,
    userRoleName?: string,
    userRoleDesc?: string,
    usRStatusDetail?: {
        statusId?: number,
        statusCode?: string,
        statusDesc?: string,
        statusName?: string
    }
};

export type UserRoleReqType = { 
    userRoleName?: string,
    userRoleDesc?: string
};

export interface UserRoleStateProps {
    userRoleList: UserRoleListResType | null;
    userRole: UserRoleResType | null;
    selectUserRoles: UserRole[] | null;
    error: object | string | null;
    Rolesuccess: object | string | null;
    isLoading: boolean
}

export interface DefaultRootStateProps {
    userRole: UserRoleStateProps;
}

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type UserRolesType = {
    userRoleId?: number,
    roleApprvReason?: string,
    roleInBussiness?: string,
    statusFlag?: string,
    userRoleDesc?: string,
    userRoleName?: string,
    statusId?: number,
    usRStatusDetail?: {
        statusId?: number,
        statusCode?: string,
        statusDesc?: string,
        statusName?: string
    },
    ccode?: number
}

export type UserRoleType = {
    userRoleId?: number,
    roleApprvReason?: string,
    roleInBussiness?: string,
    statusFlag?: string,
    userRoleDesc?: string,
    userRoleName?: string,
    statusId?: number,
    ccode?: number
}

export type UserRoleOptionType = { 
    userRoleName?: string,
    userRoleId?: number
};

// Define the type for a single user role
export interface UserRole {
    userRoleId: number;
    userRoleName: string;
    // Add more properties if necessary
}

// Define the type for the 'user' slice of the state
export interface UserState {
    roles: UserRole[]; // Assuming 'roles' is where the user roles are stored
    // Add more properties if necessary
}

// Define RootState to include the 'user' slice of the state
export interface RootState {
    user: UserState;
    // Add other slices of the state as needed
}




