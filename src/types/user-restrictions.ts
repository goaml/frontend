// ==============================|| USER RESTRICTION Types ||============================== //

export type UserResPostReq = {
  branchId: number,
  userId: number,
  menuActionId: number,
  statusId: number
};

export type UserRestrictions = {
  actionId: number,
  actionName: string,
  menuActionId: number,
  menuActionName: string,
  menuId: number,
  menuName: string,
  moduleId: number,
  moduleName: string,
  statusId: number,
  url?: string,
  usTMenuAction?: usTMenuprops
};

export type userResPostprops = {
  userRestrictionList?: Array<UserResPostReq>
}

export type usTMenuprops = {
  name?: string
  url?: string
  usMAction?: usTMenuActionprops
}

export type usTMenuActionprops = {
  actionName?: string
}

export type UserRestrictionList = {
  pagination?: {
    count?: number
    from?: number
    to?: number
    total?: number
  }
  result?: Array<UserRestrictions>
}

export interface RoleStateProps {
  userRestrictionList: UserRestrictionList | null;
  newuserRestrictionList: UserRestrictionList | null;
  userRestrictions: UserRestrictions | null;
  error: object | string | null;
  success: object | string | null;
  postsuccess: object | string | null;
  disablepostsuccess: object | string | null;
  isPostSuccess?: boolean
  isLoading: boolean
  isActionSuccess: object | string | null
}

export interface DefaultRootStateProps {
  userRestriction: RoleStateProps;
}

export interface queryParamsProps {
  page?: number
  per_page?: number
  sort?: "userId"
  direction?: "asc" | "desc"
  search?: string
  userId?: number
  branchId?: number
  menuActionId?: number
}