// ==============================|| USER AUTHORIZATION TYPES ||============================== //

export type UserPostReq = {
  userId?: number,
  status?: string
};

export type Users = {
  address?: string,
  desc?: string,
  empEmail?: string,
  empMobileNumber?: string,
  empNic?: string,
  empNumber?: string,
  firstName?: string,
  gender?: string,
  lastName?: string,
  designationId?: number,
  employeeId?: null,
  userRoleId?: number,
  statusId?: number,
  userId?: number,
  userName?: string,
  isActive?: boolean,
  wfProcessId?: number,
  keycloakUserId?: number,
  departmentId?: number,
  branchId?: number,
  fullName?: string
};

export type UserList = {
  pagination?: {
    count?: number
    from?: number
    to?: number
    total?: number
  }
  result?: Array<Users>
}

export interface UserStateProps {
  userList: UserList | null;
  users: Users | null;
  error: object | string | null;
  success: object | string | null;
  isLoading: boolean
  isActionSuccess:object | string | null;
}

export interface DefaultRootStateProps {
  users: UserStateProps;
}

export interface queryParamsProps {
  page: number
  per_page: number
  sort: "userId"
  direction: "asc" | "desc"
  search: string
  isAuthorizationList?: boolean
  userID: number | string

}