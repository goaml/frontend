
export type transactioncode_summary_monthlyType = {
    creditAverage?: number,
      custId?: string,
      debitAverage?: number,
      maxCreditAmt?: number,
      maxDebitAmt?: number,
      minCreditAmt?: number,
      minDebitAmt?: number,
      month?: string,
      noOfCreditTransactions?: number,
      noOfDebitTransactions?: number,
      noOfTransactions?: number,
      summaryLogId?: number,
      totNetAmt?: number,
      totalCreditAmt?: number,
      totalDebitAmt?: number,
      year?: string,
      rptCode?: string
    
}

export type transactioncode_summary_monthlyListResType = {
    pagination?: paginationType
    result?: transactioncode_summary_monthlyType[]
};

export type transactioncode_summary_monthlyReqType = { 
    transactioncode_summary_monthlyCode?: string,
    description?: string, 
};

export interface transactioncode_summary_monthlyStateProps {
    transactioncode_summary_monthlyList: transactioncode_summary_monthlyListResType | null;
    transactioncode_summary_monthly: transactioncode_summary_monthlyType | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
    isActionSuccess: object | string | null;
}

export interface DefaultRootStateProps {
    transactioncode_summary_monthly: transactioncode_summary_monthlyStateProps;
}

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
    toMonth?: string
    toYear?: string
    custId?: string
    fromMonth?: string
    fromYear?: string
    rptCode?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

