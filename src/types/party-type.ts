export type Ingredient = {
};

export type PartyTypeListResType = {
    pagination?: paginationType
    result?: PartyTypesType[]
};

export type PartyTypeResType = { 
    partyTypeId?: number,
    partyTypeCode?: string,
    description?: string,
    statusId?:number,
    isActive?: boolean
};

export type PartyTypeReqType = { 
    partyTypeCode?: string,
    description?: string, 
};

export interface PartyTypeStateProps {
    partyTypeList: PartyTypeListResType | null;
    partyType: PartyTypeResType | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
    isActionSuccess: object | string | null;
}

export interface DefaultRootStateProps {
    partyType: PartyTypeStateProps;
}

export type listParametersType = {
    page?: number
    perPage?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type PartyTypesType = {
    partyTypeCode?: string,
    description?: string,
    partyTypeId?: number,
    statusId?:number,
    isActive?: boolean
}

export type PartyTypeType = {
    partyTypeId?: number,
    partyTypeCode?: string,
    description?: string,
    statusId?:number,
    isActive?: boolean
}