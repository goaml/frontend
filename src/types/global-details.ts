// ==============================|| Global Details Types ||============================== //

//to identify response body in list

export type GlobalDetails = {
  created_by?: string,
  created_on?: string,
  updated_by?: string,
  updated_on?: string,
  globalID?: number,
  frontURL?: string,
  domainDestription?: string,
  coreURL?: string,
  datetime?: string,
  userExpirationDays?: number,
  userLoginAttempts?: number,
  encryptKey?: string,
  companyName?: string,
  footerTitle?: string,
  versionNumber?: number,
  logoPath?: string,
  userMalePath?: string,
  userFemalePath?: string,
  status?: number,
  plocaltime?: string,
  plocaldate?: string,
  createOn?: string,
  lastChanged?: string,
  userExpDate?: number,
  passwordExpDate?: number,
  resetDateCount?: number,
  currentWorkingDate?: [number,number,number],
  nextWorkingDate?: [number,number,number],
  userNotificationMode?: string,
  passwordMinLength?: number,
  passwordMaxLength?: number,
  passwordNumOfSpecialCharacter?: number,
  passwordNumOfCapitalLetters?: number,
  passwordNumOfSimpleLetters?: number,
  passwordNumOfDigits?: number,
  goamlLimit?: number,
  goAMLCoreURL?: string,
}

export type GlobalDetailsList = {
  pagination?: {
    count?: number
    from?: number
    to?: number
    total?: number
  }
  result?: GlobalDetails
}

export interface GlobalDetailsStateProps {
  globalDetails: GlobalDetailsList | null;
  globalDetail: GlobalDetails | null;
  error: object | string | null;
  success: object | string | null;
  isLoading: boolean;
  isActionSuccess:object | string | null;
}

export interface DefaultRootStateProps {
  globalDetail: GlobalDetailsStateProps;
}

export interface queryParamsProps {
  page: number
  per_page: number
  sort: "globalID"
  direction: "asc" | "desc"
  search: string
}