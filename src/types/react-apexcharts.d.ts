declare module 'react-apexcharts' {
    import { Component } from 'react';

    interface Props {
        options: any;
        series: any;
        type: string;
        width?: string | number;
        height?: string | number;
    }

    export default class ReactApexChart extends Component<Props> {}
}
