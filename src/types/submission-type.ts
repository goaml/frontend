export type Ingredient = {
};

export type SubmissionTypeListResType = {
    pagination?: paginationType
    result?: SubmissionTypesType[]
};

export type SubmissionTypeResType = { 
    submissionTypeId?: number,
    submissionTypeCode?: string,
    description?: string,
    statusId?:number,
    isActive?: boolean
};

export type SubmissionTypeReqType = { 
    submissionTypeCode?: string,
    description?: string, 
};

export interface SubmissionTypeStateProps {
    submissionTypeList: SubmissionTypeListResType | null;
    submissionType: SubmissionTypeResType | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
    isActionSuccess:object | string | null;
}

export interface DefaultRootStateProps {
    submissionType: SubmissionTypeStateProps;
}

export type listParametersType = {
    page?: number
    perPage?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type SubmissionTypesType = {
    submissionTypeCode?: string,
    description?: string,
    submissionTypeId?: number,
    statusId?:number,
    isActive?: boolean
}

export type SubmissionTypeType = {
    submissionTypeId?: number,
    submissionTypeCode?: string,
    description?: string,
    statusId?:number,
    isActive?: boolean
}