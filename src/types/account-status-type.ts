export type Ingredient = {
};

export type AccountStatusTypeListResType = {
    pagination?: paginationType
    result?: AccountStatusTypesType[]
};

export type AccountStatusTypeResType = { 
    accountStatusTypeId?: number,
    accountStatusTypeCode?: string,
    description?: string,
    statusId?:number,
    isActive?: boolean
};

export type AccountStatusTypeReqType = { 
    accountStatusTypeCode?: string,
    description?: string, 
};

export interface AccountStatusTypeStateProps {
    accountStatusTypeList: AccountStatusTypeListResType | null;
    accountStatusType: AccountStatusTypeResType | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
    isActionSuccess: object | string | null;
}

export interface DefaultRootStateProps {
    accountStatusType: AccountStatusTypeStateProps;
}

export type listParametersType = {
    page?: number
    perPage?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type AccountStatusTypesType = {
    accountStatusTypeCode?: string,
    description?: string,
    accountStatusTypeId?: number,
    statusId?:number,
    isActive?: boolean
}

export type AccountStatusTypeType = {
    accountStatusTypeId?: number,
    accountStatusTypeCode?: string,
    description?: string,
    statusId?:number,
    isActive?: boolean
}