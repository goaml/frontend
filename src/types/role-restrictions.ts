// ==============================|| ROLE RESTRICTION Types ||============================== //

export type RoleResPostReqBody = {
  menuActionId: number,
  //statusId: number,
  userRoleId: number
};

export type RoleResPostReq = Array<RoleResPostReqBody>

export type RoleRestrictions = {
  ccode: number,
  roleApprvReason: string,
  roleInBussiness: string,
  statusFlag: string,
  statusId: number,
  usRStatusDetail: {
    statusCode: string,
    statusDesc: string,
    statusId: number,
    statusName: string
  },
  userRoleDesc: string,
  userRoleId: number,
  userRoleName: string
};

export type RoleRestrictionList = {
  pagination?: {
    count?: number
    from?: number
    to?: number
    total?: number
  }
  result?: Array<RoleRestrictionListdata>
}


export type RoleRestrictionListdata = {

  menuActionId?: number,
  status?: boolean,
  menuActionName?: string,
  menuName?: string,
  url?: string,
  usTMenuAction?: usTMenuprops
  usMUserRole?: {
    userRoleId: number,
  }
}

export type usTMenuprops = {
  name?: string
  url?: string
  usMAction?: usTMenuActionprops
  menuActionId: number
}
export type usTMenuActionprops = {
  actionName?: string
}

export type RolAccessList = {
  pagination?: {
    count?: number
    from?: number
    to?: number
    total?: number
  }
  roleOptions?: Array<RolAccesss>
}


export type RolAccesss = {

  menuActionId?: number,
  status?: boolean,
  menuActionName?: string,
  menuName?: string,
  url?: string

}

export interface RoleStateProps {
  roleRestrictionList?: RoleRestrictionList | null;
  roleRestrictions: RoleRestrictions | null;
  selectRoles: RoleRestrictions[] | null;
  rolAccessList: RolAccessList | null;
  rolAccesss: RolAccesss | null;
  error: object | string | null;
  success: object | string | null;
  postsuccess: object | string | null;
  disablepostsuccess: object | string | null;
  isPostSuccess?: boolean
  isLoading: boolean
  isActionSuccess:object | string | null;
}

// export interface RoleStateProps2 {
//   rolAccessList: RolAccessList | null;
//   rolAccesss: RolAccesss | null;
//   error: object | string | null;
//   success: object | string | null;
//   isLoading: boolean
// }

export interface DefaultRootStateProps {
  rolesRestriction: RoleStateProps;
}

export interface queryParamsProps {
  page: number
  per_page: number
  sort: "userRoleId"
  direction: "asc" | "desc"
  search: string
}