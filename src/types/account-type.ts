export type Ingredient = {
};

export type AccountTypeListResType = {
    pagination?: paginationType
    result?: AccountTypesType[]
};

export type AccountTypeResType = { 
    accountTypeId?: number,
    accountTypeCode?: string,
    description?: string,
    statusId?:number,
    isActive?: boolean
};

export type AccountTypeReqType = { 
    accountTypeCode?: string,
    description?: string, 
};

export interface AccountTypeStateProps {
    accountTypeList: AccountTypeListResType | null;
    accountType: AccountTypeResType | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
    isActionSuccess: object | string | null;
}

export interface DefaultRootStateProps {
    accountType: AccountTypeStateProps;
}

export type listParametersType = {
    page?: number
    perPage?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type AccountTypesType = {
    accountTypeCode?: string,
    description?: string,
    accountTypeId?: number,
    statusId?:number,
    isActive?: boolean
}

export type AccountTypeType = {
    accountTypeId?: number,
    accountTypeCode?: string,
    description?: string,
    statusId?:number,
    isActive?: boolean
}