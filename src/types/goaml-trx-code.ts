export type Ingredient = {
};

export type TransactionListResType = {
    pagination?: paginationType
    result?: TransactionsType[]
};

export type TransactionResType = {
    categoryName?: string,
    description?: string,
    fromFundTypeDescription?: string,
    fromFundTypeName?: string,
    goamlTrxCodeScenario?: string,
    goamlTrxCodeComments?: string,
    goamlTrxCodeExample?: string,
    goamlTrxCodeToFundType?: string,
    multiPartyFundsCodeName?: string,
    multiPartyInvolvePartyName?: string,
    multiPartyRoleName?: string,
    reportTypeName?: string,
    rptCodeName?: string,
    toFundTypeDescription?: string,
    toFundTypeName?: string,
    goamlTrxCodeId?: number,
    transactionCodeName?: string,

};

export type TransactionReqType = {
    isActive?: boolean,
    goamlTrxCodeId?: number,
    description?: string,
    categoryId?: number,
    fromFundTypeDescriptionId?: number,
    fromFundTypeId?: number,
    goamlTrxCodeScenario?: string,
    goamlTrxCodeComments?: string,
    goamlTrxCodeExample?: string,
    goamlTrxCodeToFundType?: string,
    multiPartyFundsCodeId?: number,
    multiPartyInvolvePartyId?: number,
    multiPartyRoleId?: number,
    reportTypeId?: number,
    rptCodeId?: number,
    toFundTypeDescriptionId?: number,
    toFundTypeId?: number,
    transactionCodeId?: number,
    transactionCodeName?: string,
};

export interface exportData {
    startDate: string
    endDate: string
    executeType: string
}

export interface TransactionStateProps {
    transactionList: TransactionListResType | null;
    transaction: TransactionResType | null;
    error: object | string | null;
    trsuccess: object | string | null;
    isLoading: boolean;
    selectTransactionCodeType: TransactionType[] | null;
    isActionSuccess: object | string | null;
}

export interface DefaultRootStateProps {
    transaction: TransactionStateProps;
}

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type TransactionsType = {
    isActive?: boolean,
    description?: string,
    goamlTrxCodeId?: number,
    categoryId?: number,
    fromFundTypeDescriptionId?: number,
    fromFundTypeId?: number,
    goamlTrxCodeScenario?: string,
    goamlTrxCodeComments?: string,
    goamlTrxCodeExample?: string,
    goamlTrxCodeToFundType?: string,
    multiPartyFundsCodeId?: number,
    multiPartyInvolvePartyId?: number,
    multiPartyRoleId?: number,
    reportTypeId?: number,
    rptCodeId?: number,
    toFundTypeDescriptionId?: number,
    toFundTypeId?: number,
    transactionCodeId?: number,
    transactionCodeName?: string,
}

export type TransactionType = {
    isActive?: boolean,
    description?: string,
    goamlTrxCodeId?: number,
    categoryId?: number,
    fromFundTypeDescriptionId?: number,
    fromFundTypeId?: number,
    goamlTrxCodeScenario?: string,
    goamlTrxCodeComments?: string,
    goamlTrxCodeExample?: string,
    goamlTrxCodeToFundType?: string,
    multiPartyFundsCodeId?: number,
    multiPartyInvolvePartyId?: number,
    multiPartyRoleId?: number,
    reportTypeId?: number,
    rptCodeId?: number,
    toFundTypeDescriptionId?: number,
    toFundTypeId?: number,
    transactionCodeId?: number,
    transactionCodeName?: string,
}
