export type TransErrLog = {
    acid?: string,
    amountLocal?: number,
    custId?: string,
    dateTransaction?: string,
    failReason?: string,
    foracid?: string,
    isValidated?: boolean,
    logId?: number,
    processDate?: string,
    rptCode?: string,
    transactionNumber?: string,
    trxNo?: string
  }

export type TransErrLogList = {
    pagination?: paginationType
    result?: TransErrLog[]
};


export interface TransErrLogListStateProps {
    transErrLogList: TransErrLogList | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
    selectTransErrLog: transErrLogListFilterType[] | null;
    rptCode: ReportCodeResType | null;
}

export interface DefaultRootStateProps {
    transErrLogList: TransErrLogListStateProps;
}

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
    acid?: string,
    amountLocal?: number,
    custId?: string,
    transactionDateFrom ?: string,
    transactionDateTo ?: string,
    failReason?: string,
    foracid?: string,
    isValidated?: boolean,
    logId?: number,
    processDate?: string,
    rptCode?: string,
    transactionNumber?: string,
    trxNo?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type transErrLogListFilterType = {
    transactionNumber?: string,
    rptCode?: string,
    custId?: string,
    acid?: string,
}

export type ReportCodeResType = {
    rptCode: string,
    description: string,
}