// ==============================|| CivilStatus Types ||============================== //


export type UserPostReq = {
  userName: string,
  email: string,
  mobileNo: string,
  firstName: string,
  lastName: string,
  password: string,
  userRoleId: number,
  branchId?: number,
  departmentId: number,
  userId?: number
};

export type Branches = {

  brachDeptId: number,
  branchId: number,
  branchName: string,
  departmentId: number,

};

export type BranchesForUsers = {
  brachDeptId?: number
  branchAddress?: string,
  branchId?: number,
  branchLocation?: string,
  branchName?: string,
  contactNumber?: string,
  created_by?: string,
  created_on?: {
    date?: number,
    day?: number,
    hours?: number,
    minutes?: number,
    month?: number,
    nanos?: number,
    seconds?: number,
    time?: number,
    timezoneOffset?: number,
    year?: number
  },
  desc?: string,
  email?: string,
  limitAmt?: number,
  statusId?: number,
  updated_by?: string,
  updated_on?: {
    date?: number,
    day?: number,
    hours?: number,
    minutes?: number,
    month?: number,
    nanos?: number,
    seconds?: number,
    time?: number,
    timezoneOffset?: number,
    year?: number
  },
  usRStatusDetail?: {
    statusCode?: string,
    statusDesc?: string,
    statusId?: number,
    statusName?: string
  }

};

export type MultipleBranches = {
  brachDeptId: number,
  branchId: number,
  branchName: string,
  departmentId: number,
  isActive: boolean,
  usMBranch: {
    branchAddress: string,
    branchId: number,
    branchLocation: string,
    branchName: string,
    contactNumber: string,
    created_by: string,
    created_on: {
      date: number,
      day: number,
      hours: number,
      minutes: number,
      month: number,
      nanos: number,
      seconds: number,
      time: number,
      timezoneOffset: number,
      year: number
    },
    desc: string,
    email: string,
    limitAmt: number,
    statusId: number,
    updated_by: string,
    updated_on: {
      date: number,
      day: number,
      hours: number,
      minutes: number,
      month: number,
      nanos: number,
      seconds: number,
      time: number,
      timezoneOffset: number,
      year: number
    },
    usRStatusDetail: {
      statusCode: string,
      statusDesc: string,
      statusId: number,
      statusName: string
    }
  },
  usMDepartment: {
    contactNumber: string,
    departmentId: number,
    deptAddress: string,
    deptLocation: string,
    deptName: string,
    desc: string,
    email: string,
    statusId: number,
    usRStatusDetail: {
      statusCode: string,
      statusDesc: string,
      statusId: number,
      statusName: string
    }
  }
}

export type MBlist = {
  employeeBranchId: number,
  from: string,
  isDefualtDept: number,
  to: string,
  userId: number,
  brachDeptId: number,
  statusId: number,
  branchName: string,
  deptName: string,
  isActive: boolean
}

export type UserList = {
  pagination?: {
    count?: number
    from?: number
    to?: number
    total?: number
  }
  result?: Array<Branches>
}

export type MultipleBranchesProps = {
  pagination?: {
    count?: number
    from?: number
    to?: number
    total?: number
  }
  result?: Array<MBlist>
}

export type BranchCodeListResType = {
  pagination?: paginationType
  result?: BranchCodesType[]
};

export type paginationType = {
  count?: number,
  from?: number,
  to?: number,
  total?: number
}

export type BranchCodesType = {
  branchAddress: string,
  branchId: number,
  branchLocation: string,
  branchName: string,
  contactNumber: string,
  created_by: string,
  created_on: {
    date: number,
    day: number,
    hours: number,
    minutes: number,
    month: number,
    nanos: number,
    seconds: number,
    time: number,
    timezoneOffset: number,
    year: number
  },
  desc: string,
  email: string,
  limitAmt: number,
  statusId: number,
  updated_by: string,
  updated_on: {
    date: number,
    day: number,
    hours: number,
    minutes: number,
    month: number,
    nanos: number,
    seconds: number,
    time: number,
    timezoneOffset: number,
    year: number
  },
  usRStatusDetail: {
    statusCode: string,
    statusDesc: string,
    statusId: number,
    statusName: string
  }
}

export type listParametersType = {
  page?: number
  per_page?: number
  direction?: "asc" | "desc"
  sort?: string
}

export type BranchCodeType = {
  branchId?: number,
  branchName?: string,
  desc?: string,
  statusDesc?: string
}

export type BranchCodeResType = {
  branchId?: number,
  branchName?: string,
  desc?: string,
  statusDesc?: string
};

export type BranchCodeReqType = {
  branchAddress?: string,
  branchId?: number,
  branchLocation?: string,
  branchName?: string,
  contactNumber?: string,
  desc?: string,
  email?: string,
  limitAmt?: number,
};

export interface UserStateProps {
  userList: UserList | null;
  users: Branches | null;
  selectBranches: Branches[] | null;
  selectBranchesForUsers: BranchesForUsers[] | null;
  error: object | string | null;
  success: object | string | null;
  MultipleBranches: MultipleBranchesProps | null;
  branchCodeList: BranchCodeListResType | null;
  isLoading: boolean;
  isActionSuccess:object | string | null;
}

export interface DefaultRootStateProps {
  userbranches: UserStateProps;
}

export interface queryParamsProps {
  page: number
  per_page: number
  sort: "userId"
  direction: "asc" | "desc"
  search: string
}
