export type Ingredient = {
};

export type CurrenciesListResType = {
    pagination?: paginationType
    result?: CurrenciessType[]
};

export type CurrenciesResType = { 
    currenciesId?: number,
    currenciesCode?: string,
    description?: string,
    statusId?:number,
    isActive?: boolean
};

export type CurrenciesReqType = { 
    currenciesCode?: string,
    description?: string, 
};

export interface CurrenciesStateProps {
    currenciesList: CurrenciesListResType | null;
    currencies: CurrenciesResType | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean
    isActionSuccess: object | string | null;
}

export interface DefaultRootStateProps {
    currencies: CurrenciesStateProps;
}

export type listParametersType = {
    page?: number
    perPage?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type CurrenciessType = {
    currenciesCode?: string,
    description?: string,
    currenciesId?: number,
    statusId?:number,
    isActive?: boolean
}

export type CurrenciesType = {
    currenciesId?: number,
    currenciesCode?: string,
    description?: string,
    statusId?:number,
    isActive?: boolean
}