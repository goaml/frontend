
export interface UserTypeStateProps {
    selectUserTypes: UserType[] | null;
    error: object | string | null;
    typesuccess: object | string | null;
    isLoading: boolean,
    userTypeList: UserTypeListResType | null;
    userType: UserTypesType | null;
    isActionSuccess: object | string | null;
}

export interface DefaultRootStateProps {
    userType: UserTypeStateProps;
}

export type UserTypeListResType = {
    pagination?: paginationType
    result?: UserTypesType[]
};

export type UserReqType = {
    typeCode?: string,
    userTypeId?: number,
    typeDescription?: string
};

export type UserTypePost = {
    isActive?: boolean,
    loginTypeId?: number,
    usRLoginType?: {
        isActive?: boolean,
        loginTypeId?: number,
        typeCode?: string,
        typeDescription?: string
    },
    userCategory?: string,
    userTypeId?: number
};

export type UserTypesType = {
    isActive?: boolean,
    loginTypeId?: number,
    usRLoginType?: {
        isActive?: boolean,
        loginTypeId?: number,
        typeCode?: string,
        typeDescription?: string
    },
    userCategory?: string,
    userTypeId?: number
}
export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}
// Define the type for a single user Type
export type UserType = {
    userCategory?: string,
    userTypeId?: number,
    isActive?: boolean;
    loginTypeId?: number;
    usRLoginType?: {
        isActive?: boolean;
        loginTypeId?: number;
        typeCode?: string;
        typeDescription?: string
    };
}

// Define the type for the 'user' slice of the state
export interface UserState {
    Types: UserType[]; // Assuming 'Types' is where the user Types are stored
    // Add more properties if necessary
}

// Define RootState to include the 'user' slice of the state
export interface UserTypeRootState {
    userType: UserState;
    // Add other slices of the state as needed
}

export type UserTypeOptionType = {
    userCategory?: string,
    userTypeId?: number
};

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    sort: "userTypeId"
    search?: string
}