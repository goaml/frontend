export type Ingredient = {
};

export type CityListResType = {
    pagination?: paginationType
    result?: CitysType[]
};

export type CityResType = { 
    cityId?: number,
    cityCode?: string,
    description?: string,
    statusId?: number,
    isActive?: boolean
};

export type CityReqType = { 
    cityCode?: string,
    description?: string, 
};

export interface CityStateProps {
    cityList: CityListResType | null;
    city: CityResType | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
    isActionSuccess: object | string | null;
}

export interface DefaultRootStateProps {
    city: CityStateProps;
}

export type listParametersType = {
    page?: number
    perPage?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type CitysType = {
    cityCode?: string,
    description?: string,
    statusId?: number,
    cityId?: number,
    isActive?: boolean
}

export type CityType = {
    cityId?: number,
    cityCode?: string,
    statusId?: number,
    description?: string,
    isActive?: boolean
}