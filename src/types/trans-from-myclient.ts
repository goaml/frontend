export type transFromMyClient= {
    fromMyClientId?:number,
    fromFundsComment?:string,
    fromForeignCurrencyId?:number,
    conductorPersonMyClientId?:number,
    fromAccountMyClientId?:number,
    fromPersonMyClientId?:number,
    fromEntityMyClientId?:number,
    transactionId?:number,
    transactionNumber?:string,
    isActive?:boolean,
    rfundsType?:{
        fundTypeCode?: string,
    },
    rcountryCodes?:{
        description?:string,
    }
    rstatus?:{
        statusId?:number,
        isActive?:boolean,
        statusCode?:string,
    }

}

export type transFromMyClientList = {
    pagination?: {
      count?: number;
      from?: number;
      to?: number;
      total?: number;
    }
    result?: Array<transFromMyClient>;
  }
export interface transFromMyClientStateProps {
    transFromMyClientList: transFromMyClientList | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean
}

export interface DefaultRootStateProps {
    transFromMyClient: transFromMyClientStateProps;
}

export interface queryParamsProps {
    page: number
    per_page: number
    sort: string
    direction: "asc" | "desc"
    search: string
    trxId: number
}