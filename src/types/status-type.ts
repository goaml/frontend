export type Ingredient = {
};

export type StatusListResType = {
    pagination?: paginationType
    result?: StatussType[]
};

export type StatusResType = { 
    statusId?: number,
    statusCode?: string,
    description?: string,
    isActive?: boolean
};

export type StatusReqType = { 
    statusCode?: string,
    description?: string, 
};

export interface StatusStateProps {
    statusList: StatusListResType | null;
    status: StatusResType | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean
    isActionSuccess: object | string | null;
}

export interface DefaultRootStateProps {
    status: StatusStateProps;
}

export type listParametersType = {
    page?: number
    perPage?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type StatussType = {
    statusCode?: string,
    description?: string,
    statusId?: number,
    isActive?: boolean
}

export type StatusType = {
    statusId?: number,
    statusCode?: string,
    description?: string,
    isActive?: boolean
}