export type Ingredient = {
};

export type AccountPersonRoleTypeListResType = {
    pagination?: paginationType
    result?: AccountPersonRoleTypesType[]
};

export type AccountPersonRoleTypeResType = { 
    accountPersonRoleTypeId?: number,
    accountPersonRoleTypeCode?: string,
    description?: string,
    statusId?:number,
    isActive?: boolean
};

export type AccountPersonRoleTypeReqType = { 
    accountPersonRoleTypeCode?: string,
    description?: string, 
};

export interface AccountPersonRoleTypeStateProps {
    accountPersonRoleTypeList: AccountPersonRoleTypeListResType | null;
    accountPersonRoleType: AccountPersonRoleTypeResType | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
    isActionSuccess: object | string | null;
}

export interface DefaultRootStateProps {
    accountPersonRoleType: AccountPersonRoleTypeStateProps;
}

export type listParametersType = {
    page?: number
    perPage?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type AccountPersonRoleTypesType = {
    accountPersonRoleTypeCode?: string,
    description?: string,
    accountPersonRoleTypeId?: number,
    statusId?:number,
    isActive?: boolean
}

export type AccountPersonRoleTypeType = {
    accountPersonRoleTypeId?: number,
    accountPersonRoleTypeCode?: string,
    description?: string,
    statusId?:number,
    isActive?: boolean
}