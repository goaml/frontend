export type uploadData = {
    fileName: string,
    fileSize: string,
    fileType: string,
    lastUpdatedDate: string
};

export interface UploadDataStateProps {
    UploadDataList: uploadData[] | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean
}

export interface DefaultRootStateProps {
    UploadData: UploadDataStateProps;
}
