// ==============================|| CivilStatus Types ||============================== //

export type UserPostReq = {
    userName?: string;
    email?: string;
    mobileNo?: string;
    firstName?: string;
    lastName?: string;
    userRoleId?: number;
    userId?: number;
    branchId?: number;
    userTypeId?: number;
    nic?: string;
    referenceNo?: string;
  };
  
  export type Users = {
    address: string;
    desc: string;
    empEmail: string;
    empMobileNumber: string;
    empNic: string;
    empNumber: string;
    firstName: string;
    gender: string;
    lastName: string;
    designationId: number;
    employeeId: null;
    userRoleId: number;
    statusId: number;
    userId: number;
    userName: string;
    isActive: boolean;
    wfProcessId: number;
    keycloakUserId: number;
    departmentId: number;
    branchId: number;
    fullName: string;
  };
  
  export type UserDefaultBranch = {
    defaultBranch: string,
  };
  
  export type UserList = {
    pagination?: {
      count?: number;
      from?: number;
      to?: number;
      total?: number;
    }
    result?: Array<Users>
  }
  
  export interface UserStateProps {
    userList: UserList | null;
    users: Users | null;
    user: Users | null;
    userById?: Users | null;
    selectUsers: Users[] | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
    EditProfileSuccess?: object | string | null;
    userDefaultBranchById?: UserDefaultBranch | null;
    roles : any
    isActionSuccess: object | string | null;
  }
  
  export interface DefaultRootStateProps {
    users: UserStateProps;
    user: UserStateProps;
  }
  
  export interface queryParamsProps {
    page: number;
    per_page: number;
    sort: "userId";
    direction: "asc" | "desc";
    search: string;
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  export type PostUserReqType = {
    branchId?: number,
    comId?: number,
    departmentId?: number,
    description?: string,
    email?: string,
    firstName?: string,
    isExistingCompany?: boolean,
    lastName?: string,
    mobileNo?: string,
    nic?: string,
    referenceNo?: string,
    userRoleId?: number,
    userTypeId?: number,
    userId?: number;
  };
  
  
  export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    search: string
    sort?: string
  }
  
  export type updateUserType = {
    description?: string,
    email?: string,
    mobileNo?: string,
    firstName?: string,
    lastName?: string,
    userRoleId?: number,
    statusId?: number,
    userId?: number,
    userName?: string,
    isActive?: boolean,
    nic?: string,
    referenceNo?: string,
    departmentId?: number,
    branchId?: number,
    statusName?: string,
    userRoleName?: string,
    branchName?: string,
    departmentName?: string,
    userTypeId?: number,
    userTypeName?: string,
    companyId?: number
  
  }