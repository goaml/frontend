export type XmlFileLog = {
    executeTime: string,
    logId: number,
    processMonth: string,
    processYear: string,
    recordCount: number,
    reportType: string,
    status: string
    totalTranAmt: number
  }

export type XmlFileLogList = {
    pagination?: paginationType
    result?: XmlFileLog[]
};


export interface XmlFileLogListStateProps {
    xmlFileLogList: XmlFileLogList | null;
    xmlFileLogListForDashBoard: XmlFileLogList | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
}

export interface DefaultRootStateProps {
    xmlFileLogList: XmlFileLogListStateProps;
}

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
    // executeTime?: string
    executeTimeFrom? : string
    executeTimeTo? : string
    logId?: number
    processMonth?: string
    processYear?: string
    recordCount?: number
    reportType?: string
    status?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

