export type Ingredient = {
};

export type MultiPartyInvolvePartyListResType = {
    pagination?: paginationType
    result?: MultiPartyInvolvePartysType[]
};

export type MultiPartyInvolvePartyResType = { 
    multiPartyInvolvePartyName?: string,
    multiPartyInvolvePartyDescription?:string,
};

export type MultiPartyInvolvePartyReqType = { 
    multiPartyInvolvePartyName?: string,
};

export interface ReferenceControllerStateProps {
    MultiPartyInvolvePartyList: MultiPartyInvolvePartyListResType | null;
    MultiPartyInvolveParty: MultiPartyInvolvePartyResType | null;
    error: object | string | null;
    MPIsuccess: object | string | null;
    isLoading: boolean;
    selectMultiPartyInvolveParty: MultiPartyInvolvePartyType[] |null
}

export interface DefaultRootStateProps {
    multiPartyInvolveParty: ReferenceControllerStateProps;
}

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type MultiPartyInvolvePartysType = {
    multiPartyInvolvePartyName?: string,
    multiPartyInvolvePartyId?: number,
    multiPartyInvolvePartyDescription?:string,
    
}

export type MultiPartyInvolvePartyType = {
    multiPartyInvolvePartyId?: number,
    multiPartyInvolvePartyName?: string,
    multiPartyInvolvePartyDescription?:string,
    
}