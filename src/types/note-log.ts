// ==============================|| Note LOg  Types ||============================== //

//to identify response body in list

export type NoteLogs = {
    actionId?: number,
    actionName?: string,
    activityId?: number,
    branchId?: number,
    branchName?: string,
    companyId?: number,
    createdBy?: string,
    createdOn?: string,
    currentDate?: string,
    deptId?: number,
    deptName?: string,
    description?: string,
    isActive?: boolean,
    keyValue?: string,
    keyValueId?: number,
    menuId?: number,
    menuName?: string,
    serverTime?: {
        hour?: number,
        minute?: number,
        nano?: number,
        second?: number
    },
    systemDate?: string,
    updatedBy?: string,
    updatedOn?: string,
    userName?: string

}


export type NoteLogList = {
    pagination?: {
        count?: number
        from?: number
        to?: number
        total?: number
    }
    result?: Array<NoteLogs>
}

export interface NoteLogStateProps {
    noteLogs: NoteLogList | null;
    noteLog: NoteLogs | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean
}

export interface DefaultRootStateProps {
    noteLog: NoteLogStateProps;
}

export interface queryParamsProps {
    page: number
    per_page: number
    sort: "activityId"
    direction: "asc" | "desc"
    search: string
}