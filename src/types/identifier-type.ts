export type Ingredient = {
};

export type IdentifierTypeListResType = {
    pagination?: paginationType
    result?: IdentifierTypesType[]
};

export type IdentifierTypeResType = { 
    identifierTypeId?: number,
    identifierTypeCode?: string,
    description?: string,
    statusId?:number,
    isActive?: boolean
};

export type IdentifierTypeReqType = { 
    identifierTypeCode?: string,
    description?: string, 
};

export interface IdentifierTypeStateProps {
    identifierTypeList: IdentifierTypeListResType | null;
    identifierType: IdentifierTypeResType | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
    isActionSuccess: object | string | null;
}

export interface DefaultRootStateProps {
    identifierType: IdentifierTypeStateProps;
}

export type listParametersType = {
    page?: number
    perPage?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type IdentifierTypesType = {
    identifierTypeCode?: string,
    description?: string,
    identifierTypeId?: number,
    statusId?:number,
    isActive?: boolean
}

export type IdentifierTypeType = {
    identifierTypeId?: number,
    identifierTypeCode?: string,
    description?: string,
    statusId?:number,
    isActive?: boolean
}