export type Ingredient = {
};

export type RPTCodeListResType = {
    pagination?: paginationType
    result?: RPTCodesType[]
};

export type RPTCodeResType = { 
    rptCodeId?: number,
    rptCodeName?: string,
    rptCodeDescription?: string,
    isActive?: boolean
    goamlTrxCodeId?:number,
        goAmlTrxCodes?:{
        transactionCodeName?:string,
    }
};

export type RPTCodeReqType = { 
    rptCodeName?: string,
    rptCodeDescription?: string,
    isActive?: boolean 
    goamlTrxCodeId?:number,
    goAmlTrxCodes?:{
        transactionCodeName?:string,
    }
};

export interface RPTCodeStateProps {
    rptCodeList: RPTCodeListResType | null;
    rptCode: RPTCodeResType | null;
    error: object | string | null;
    RPTsuccess: object | string | null;
    isLoading: boolean;
    selectRPTCodeType:RPTCodeType [] |null;
    isActionSuccess:object | string | null;

}

export interface DefaultRootStateProps {
    rptCode: RPTCodeStateProps;
}

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type RPTCodesType = {
    rptCodeName?: string,
    rptCodeDescription?: string,
    rptCodeId?: number,
    isActive?: boolean,
    goamlTrxCodeId?:number,
    goAmlTrxCodes?:{
        transactionCodeName?:string,
    }
    
    
}

export type RPTCodeType = {
    rptCodeId?: number,
    rptCodeName?: string,
    rptCodeDescription?: string,
    isActive?: boolean,
    goamlTrxCodeId?:number,
    goAmlTrxCodes?:{
        transactionCodeName?:string,
    }
}
