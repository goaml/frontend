export type transPhone ={
    comments?: string,
      communicationTypeId?: number,
      contactTypeId?: number,
      isActive?: boolean,
      phoneId?: number,
      rcommunicationType?: {
        communicationTypeCode?: string,
        communicationTypeId?: number,
        createdBy?: string,
        createdOn?: string,
        description?: string,
        isActive?: boolean,
        rstatus?: {
          description?: string,
          isActive?: boolean,
          statusCode?: string,
          statusId?: number
        },
        statusId?: number,
        updatedBy?: string,
        updatedOn?: string
      },
      rcontactType?: {
        contactTypeCode?: string,
        contactTypeId?: number,
        createdBy?: string,
        createdOn?: string,
        description?: string,
        isActive?: boolean,
        rstatus?: {
          description?: string,
          isActive?: boolean,
          statusCode?: string,
          statusId?: number
        },
        statusId?: number,
        updatedBy?: string,
        updatedOn?: string
      },
      rstatus?: {
        description?: string,
        isActive?: boolean,
        statusCode?: string,
        statusId?: number
      },
      statusId?: number,
      tphCountryPrefix?: string,
      tphExtension?: string,
      tphNumber?: string
}

export type transPhoneList = {
    pagination?: {
      count?: number;
      from?: number;
      to?: number;
      total?: number;
    }
    result?: Array<transPhone>;
  }
export interface transPhoneStateProps {
    transPhoneList: transPhoneList | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean
}

export interface DefaultRootStateProps {
    transPhone: transPhoneStateProps;
}

export interface queryParamsProps {
    page: number
    per_page: number
    sort: string
    direction: "asc" | "desc"
    search: string
}