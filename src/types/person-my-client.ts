export type MyClientPerson = {
    personMyClientId?: number;
    genderTypeId?: number;
    title?: string;
    firstName?: string;
    lastName?: string;
    birthdate?: Date;
    birthPlace?: string;
    mothersName?: string;
    alias?: string;
    ssn?: string;
    passportNumber?: string;
    passportCountryCodeId?: number;
    idNumber?: string;
    nationalityCountryId?: number;
    nationalityCountry2Id?: number;
    nationalityCountry3Id?: number;
    residenceCountryId?: number;
    phones?: string;
    addresses?: string;
    email?: string;
    occupation?: string;
    comments?: string;
    statusId?: number;
    isActive?: boolean;

}

export type MyClientPersonList = {
    pagination?: {
        count?: number;
        from?: number;
        to?: number;
        total?: number;
    }
    result?: Array<MyClientPerson>;
}
export interface MyClientPersonStateProps {
    MyClientPersonList: MyClientPersonList | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean
}

export interface DefaultRootStateProps {
    MyClientPerson: MyClientPersonStateProps;
}

export interface queryParamsProps {
    page: number
    per_page: number
    sort: string
    direction: "asc" | "desc"
    search: string
    personMyClientId?: number
}