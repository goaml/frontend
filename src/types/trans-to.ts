export type transTo= {
    toId?:number,
    toFundsComment?:string,
    toForeignCurrencyId?:number,
    conductorPersonId?:number,
    toAccountId?:number,
    toPersonId?:number,
    toEntityId?:number,
    transactionId?:number,
    isActive?:boolean,
    rfundsType?:{
        fundTypeCode?: string,
    },
    rcountryCodes?:{
        description?:string,
    }
    rstatus?:{
        statusId?:number,
        isActive?:boolean,
        statusCode?:string,
    }

}

export type transToList = {
    pagination?: {
      count?: number;
      from?: number;
      to?: number;
      total?: number;
    }
    result?: Array<transTo>;
  }
export interface transToStateProps {
    transToList: transToList | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean
}

export interface DefaultRootStateProps {
    transTo: transToStateProps;
}

export interface queryParamsProps {
    page: number
    per_page: number
    sort: string
    direction: "asc" | "desc"
    search: string
    trxId: number
}