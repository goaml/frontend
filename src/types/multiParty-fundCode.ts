export type Ingredient = {
};

export type MultiPartyFundsCodeListResType = {
    pagination?: paginationType
    result?: MultiPartyFundsCodesType[]
};

export type MultiPartyFundsCodeResType = { 
    multiPartyFundsCodeName?: string,
    multiPartyFundsCodeDescription?:string,
};

export type MultiPartyFundsCodeReqType = { 
    multiPartyFundsCodeName?: string,
};

export interface ReferenceControllerStateProps {
    MultiPartyFundsCodeList: MultiPartyFundsCodeListResType | null;
    MultiPartyFundsCode: MultiPartyFundsCodeResType | null;
    error: object | string | null;
    MPFsuccess: object | string | null;
    isLoading: boolean;
    selectMultiPartyFundsCode: MultiPartyFundsCodeType[] |null
}

export interface DefaultRootStateProps {
    multiPartyFundsCode: ReferenceControllerStateProps;
}

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type MultiPartyFundsCodesType = {
    multiPartyFundsCodeName?: string,
    multiPartyFundsCodeId?: number,
    multiPartyFundsCodeDescription?:string,
    
}

export type MultiPartyFundsCodeType = {
    multiPartyFundsCodeId?: number,
    multiPartyFundsCodeName?: string,
    multiPartyFundsCodeDescription?:string,
    
}