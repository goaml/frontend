// ==============================|| ROLES Types ||============================== //

export type RolePostReq = {
    userRoleDesc?: string,
    userRoleName?: string,
    userRoleId?: number
};

export type Roles = {
    ccode?: number,
    roleApprvReason?: string,
    roleInBussiness?: string,
    statusFlag?: string,
    statusId?: number,
    usRStatusDetail?: {
        statusCode?: string,
        statusDesc?: string,
        statusId?: number,
        statusName?: string
    },
    userRoleDesc?: string,
    userRoleId?: number,
    userRoleName?: string
};

export type RoleList = {
    pagination?: {
        count?: number
        from?: number
        to?: number
        total?: number
    }
    result?: Array<Roles>
}

export interface RoleStateProps {
    roleList: RoleList | null;
    roles: Roles | null;
    selectRoles: Roles[] | null;
    selectUserRoles: Roles[] | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
    isActionSuccess:object | string | null
}

export interface DefaultRootStateProps {
    roles: RoleStateProps;
}

export interface queryParamsProps {
    page: number
    per_page: number
    sort: "userRoleId"
    direction: "asc" | "desc"
    search: string
}