export type Ingredient = {
};

export type ContactTypeListResType = {
    pagination?: paginationType
    result?: ContactTypesType[]
};

export type ContactTypeResType = { 
    contactTypeId?: number,
    contactTypeCode?: string,
    description?: string,
    StatusId?:number,
    isActive?: boolean
};

export type ContactTypeReqType = { 
    contactTypeCode?: string,
    description?: string, 
};

export interface ContactTypeStateProps {
    contactTypeList: ContactTypeListResType | null;
    contactType: ContactTypeResType | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
    isActionSuccess: object | string | null;
}

export interface DefaultRootStateProps {
    contactType: ContactTypeStateProps;
}

export type listParametersType = {
    page?: number
    perPage?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type ContactTypesType = {
    contactTypeCode?: string,
    description?: string,
    contactTypeId?: number,
    statusId?:number,
    isActive?: boolean
}

export type ContactTypeType = {
    contactTypeId?: number,
    contactTypeCode?: string,
    description?: string,
    statusId?:number,
    isActive?: boolean
}