// ==============================|| MultipleBranch Types ||============================== //
export type MultipleBranchPostReq = {
    brachDeptId?: number,
    from?: string,
    to?: string,
    userId?: number,
    employeeBranchId?: number
};

export type BranchesNotAssignedToUser = {

    brachDeptId?: number,
    branchId?: number,
    branchName?: string,
    departmentId?: number,
    isActive?: true,
    toDate?: string,
    fromDate?: string,
    usMBranch?: {
        branchAddress?: string,
        branchId?: number,
        branchLocation?: string,
        branchName?: string,
        contactNumber?: string,
        created_by?: string,
        created_on?: {
            date?: number,
            day?: number,
            hours?: number,
            minutes?: number,
            month?: number,
            nanos?: number,
            seconds?: number,
            time?: number,
            timezoneOffset?: number,
            year?: number
        },
        desc?: string,
        email?: string,
        limitAmt?: number,
        statusId?: number,
        updated_by?: string,
        updated_on?: {
            date?: number,
            day?: number,
            hours?: number,
            minutes?: number,
            month?: number,
            nanos?: number,
            seconds?: number,
            time?: number,
            timezoneOffset?: number,
            year?: number
        },
        usRStatusDetail?: {
            statusCode?: string,
            statusDesc?: string,
            statusId?: number,
            statusName?: string
        }
    },
    usMDepartment?: {
        contactNumber?: string,
        departmentId?: number,
        deptAddress?: string,
        deptLocation?: string,
        deptName?: string,
        desc?: string,
        email?: string,
        statusId?: number,
        usRStatusDetail?: {
            statusCode?: string,
            statusDesc?: string,
            statusId?: number,
            statusName?: string
        }
    }

};

export interface MultipleBranchStateProps {
    NotAssignedBranchesForUsers: BranchesNotAssignedToUser[] | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean
}

export interface DefaultRootStateProps {
    multipleBranch: MultipleBranchStateProps;
}

export interface queryParamsProps {
    page: number
    per_page: number
    sort: "userId"
    direction: "asc" | "desc"
    search: string
}