export type MyClientAccount = {
    accountMyClientId?: number,
    institutionName?: string,
    institutionCode?: string,
    swift?: string,
    nonBankInstitution?: boolean,
    branch?: string,
    account?: string,
    currenciesId?: number,
    accountName?: string,
    isActive?: boolean,
    iban?: string,
    clientNumber?: string
    entityMyClientId?: number,
    personMyClientId?: number,
    accountTypeId?: number,
    opened?: string,
    closed?: string,
    balance?: number,
    beneficiary?: string,
    beneficiaryComment?: string,
    comments?: string,
    dateBalance?: string

}

export type MyClientAccountList = {
    pagination?: {
        count?: number;
        from?: number;
        to?: number;
        total?: number;
    }
    result?: Array<MyClientAccount>;
}
export interface MyClientAccountStateProps {
    MyClientAccountList: MyClientAccountList | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean
}

export interface DefaultRootStateProps {
    MyClientAccount: MyClientAccountStateProps;
}

export interface queryParamsProps {
    page: number
    per_page: number
    sort: string
    direction: "asc" | "desc"
    search: string
    accountMyClientId?: number
}