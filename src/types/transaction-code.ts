export type Ingredient = {
};

export type TransactionCodeListResType = {
    pagination?: paginationType
    result?: TransactionCodesType[]
};

export type TransactionCodeResType = { 
    transactionCodeId?: number,
    transactionCodeName?: string,
    transactionCodeDescription?: string,
    isActive?: boolean
};

export type TransactionCodeReqType = { 
    transactionCodeName?: string,
    transactionCodeDescription?: string, 
};

export interface TransactionCodeStateProps {
    transactionCodeList: TransactionCodeListResType | null;
    transactionCode: TransactionCodeResType | null;
    error: object | string | null;
    TRCsuccess: object | string | null;
    isLoading: boolean;
    selectTransactionCodeType:TransactionCodeType [] |null;

}

export interface DefaultRootStateProps {
    transactionCode: TransactionCodeStateProps;
}

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type TransactionCodesType = {
    transactionCodeName?: string,
    transactionCodeDescription?: string,
    transactionCodeId?: number,
    isActive?: boolean
}

export type TransactionCodeType = {
    transactionCodeId?: number,
    transactionCodeName?: string,
    transactionCodeDescription?: string,
    isActive?: boolean
}
