export type TransactionInquiry = {
    acid?: string,
    amtReservationInd?: string,
    bankCode?: string,
    bankId?: string,
    bkdtTranFlg?: string,
    brCode?: string,
    crncyCode?: string,
    crncyHolChkDoneFlg?: string,
    custId?: string,
    delFlg?: string,
    delMemoPad?: string,
    dthInitSolId?: string,
    eabfabUpdFlg?: string,
    entryDate?: Date,
    entryUserId?: string,
    fxTranAmt?: number,
    glDate?: Date,
    glSegmentString?: string,
    glSubHeadCode?: string,
    goAmlTrxCode?: string,
    gstUpdFlg?: string,
    id?: number,
    implCashPartTranFlg?: string,
    instrmntAlpha?: string,
    instrmntDate?: Date,
    instrmntNum?: string,
    instrmntType?: string,
    isoFlg?: string,
    lchgTime?: Date,
    lchgUserId?: string,
    liftLienFlg?: string,
    moduleId?: string,
    mudPoolBalBuildFlg?: string,
    navigationFlg?: string,
    partTranSrlNum?: string,
    partTranType?: string,
    partyCode?: string,
    prSrlNum?: string,
    principalPortionAmt?: number,
    prntAdvcInd?: string,
    proxyAcid?: string,
    proxyPostInd?: string,
    pstdDate?: Date,
    pstdFlg?: string,
    pstdUserId?: string,
    ptranChrgExistsFlg?: string,
    pttmEventType?: string,
    rate?: number,
    rateCode?: string,
    rcreTime?: Date,
    rcreUserId?: string,
    refAmt?: number,
    refCrncyCode?: string,
    refNum?: string,
    referralId?: string,
    regularizationAmt?: number,
    reservationAmt?: number,
    restrictModifyInd?: string,
    reversalDate?: Date,
    reversalValueDate?: Date,
    rptCode?: string,
    serialNum?: string,
    siOrgExecDate?: Date,
    siSrlNum?: string,
    solId?: string,
    svsTranId?: string,
    sysPartTranCode?: string,
    tfEntitySolId?: string,
    todEntityId?: string,
    todEntityType?: string,
    trStatus?: string,
    tranAmt?: number,
    tranCrncyCode?: string,
    tranDate?: Date,
    tranFreeCode1?: string,
    tranFreeCode2?: string,
    tranId?: string,
    tranParticular?: string,
    tranParticular2?: string,
    tranParticularCode?: string,
    tranRmks?: string,
    tranSubType?: string,
    tranType?: string,
    treaRate?: number,
    treaRefNum?: string,
    tsCnt?: number,
    uadModuleId?: string,
    uadModuleKey?: string,
    userPartTranCode?: string,
    valueDate?: Date,
    vfdDate?: Date,
    vfdUserId?: string,
    voucherPrintFlg?: string
  }

export type TransactionInquiryList = {
    pagination?: paginationType
    result?: TransactionInquiry[]
};


export interface TransactionInquiryStateProps {
    transactionInquiryList: TransactionInquiryList | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
}

export interface DefaultRootStateProps {
    transactionInquiry: TransactionInquiryStateProps;
}

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
    tranId?:string
    amountFrom? :number
    amountTo?: number
    dateFrom?: string
    dateTo?:string
    goAmlTrxCode?: string
    rptCode?: string
    custId?: string

}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

