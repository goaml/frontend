// ==============================|| Note LOg  Types ||============================== //

//to identify response body in list

export type xmlReport = {
    createdBy?: string,
      createdOn?: string,
      fileName?: string,
      reportCode?: string,
      reportLogId?: number,
      reportType?: string,
      reportTypeId?: number,
      updatedBy?: string,
      updatedOn?: string,
      xmlContent?: string
}


export type xmlReportList = {
    pagination?: {
        count?: number
        from?: number
        to?: number
        total?: number
    }
    result?: Array<xmlReport>
}

export interface xmlReportStateProps {
    xmlReports: xmlReportList | null;
    xmlReport: xmlReport | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean
    downloadBackupXml: downloadBackupXml | null;
}

export interface DefaultRootStateProps {
    xmlReport: xmlReportStateProps;
}

export interface queryParamsProps {
    page: number
    per_page: number
    sort: "reportLogId"
    direction: "asc" | "desc"
    search: string
}

export type downloadBackupXml = {
        timestamp?: string,
        message?: string,
        error?: string
}

export interface xmlReportProps {
    reportLogId?: number
}