
export type transactioncode_summary_dailyType = {
    creditAverage?: number,
      custId?: string,
      debitAverage?: number,
      maxCreditAmt?: number,
      maxDebitAmt?: number,
      minCreditAmt?: number,
      minDebitAmt?: number,
      month?: string,
      noOfCreditTransactions?: number,
      noOfDebitTransactions?: number,
      noOfTransactions?: number,
      summaryLogId?: number,
      totNetAmt?: number,
      totalCreditAmt?: number,
      totalDebitAmt?: number,
      year?: string,
      rtpCode?: string
    
}

export type transactioncode_summary_dailyListResType = {
    pagination?: paginationType
    result?: transactioncode_summary_dailyType[]
};

export type transactioncodeSummaryDailyReqType = { 
    transactioncodeSummaryDailyCode?: string,
    description?: string, 
};

export interface transactioncode_summary_dailyStateProps {
    transactioncode_summary_dailyList: transactioncode_summary_dailyListResType | null;
    transactioncode_summary_daily: transactioncode_summary_dailyType | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
    isActionSuccess: object | string | null;
}

export interface DefaultRootStateProps {
    transactioncode_summary_daily: transactioncode_summary_dailyStateProps;
}

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
    toMonth?: string
    toYear?: string
    custId?: string
    fromMonth?: string
    fromYear?: string
    rptCode?: string

}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

