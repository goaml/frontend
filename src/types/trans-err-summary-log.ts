export type TransErrSummaryLog = {
    processDate?: string,
    failReason?: string,
    attemptCode?: string
    count?: number,
    logId?: number,
}

export type TransErrSummaryLogList = {
    pagination?: paginationType
    result?: TransErrSummaryLog[]
};


export interface TransErrLogSummaryListStateProps {
    transErrSummaryLogList: TransErrSummaryLogList | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
}

export interface DefaultRootStateProps {
    transErrSummaryLogList: TransErrLogSummaryListStateProps;
}

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
    processDateFrom?: string,
    processDateTo?: string,
    logId?: number,
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}


