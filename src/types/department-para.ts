export type Ingredient = {
};

export type DepartmentCodeListResType = {
    pagination?: paginationType
    result?: DepartmentCodesType[]
};

export type DepartmentCodeResType = { 
    departmentId?: number,
    deptName?: string,
    desc?: string,
    statusDesc?:string 
};

export type DepartmentCodeReqType = { 
    deptAddress?: string,
    deptLocation?: string,
    contactNumber?:number,
    deptName?: string,
    desc?: string, 
    email?: string,
    
};

export interface DepartmentCodeStateProps {
    departmentCodeList: DepartmentCodeListResType | null;
    departmentCode: DepartmentCodeResType | null;
    selectDepartments: DeptType[] | null;
    error: object | string | null;
    deptSuccess: object | string | null;
    isLoading: boolean
}

export interface DefaultRootStateProps {
    dept: DepartmentCodeStateProps;
}

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    search?: string,
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type DepartmentCodesType = 
{
    departmentId: number,
    contactNumber: string,
    deptAddress: string,
    deptLocation: string,
    deptName: string,
    desc: string,
    email: string,
    statusId: number,
    usRStatusDetail: {
      statusId: number,
      statusCode: number,
      statusDesc: string,
      statusName: string
    }
}


export type DepartmentCodeType = {
    departmentId?: number,
    deptName?: string,
    desc?: string,
    statusDesc?: string
}

// Define the type for a single user role
export interface DeptType {
    departmentId: number;
    deptName: string;
    // Add more properties if necessary
}

// Define the type for the 'user' slice of the state
export interface DepartmentState {
    depts: DeptType[]; // Assuming 'roles' is where the user roles are stored
    // Add more properties if necessary
}

// Define RootState to include the 'user' slice of the state
export interface RootState {
    department: DepartmentState;
    // Add other slices of the state as needed
}
