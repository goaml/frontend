import { ReactElement } from 'react';

// third-party
import firebase from 'firebase/compat/app';

// ==============================|| AUTH TYPES  ||============================== //

export type GuardProps = {
  children: ReactElement | null;
};

export type UserProfile = {
  id?: string;
  // email?: string;
  avatar?: string;
  image?: string;
  name?: string;
  role?: string;
  tier?: string;

  // API   
  branchList?: [
    {
      branchAddress?: string;
      branchId?: number;
      branchLocation?: string;
      branchName?: string;
      contactNumber?: string;
      created_by?: string;
      created_on?: {
        date?: number;
        day?: number;
        hours?: number;
        minutes?: number;
        month?: number;
        nanos?: number;
        seconds?: number;
        time?: number;
        timezoneOffset?: number;
        year?: number;
      };
      desc?: string;
      email?: string;
      limitAmt?: number;
      statusId?: number;
      updated_by?: string;
      updated_on?: {
        date?: number;
        day?: number;
        hours?: number;
        minutes?: number;
        month?: number;
        nanos?: number;
        seconds?: number;
        time?: number;
        timezoneOffset?: number;
        year?: number;
      };
      usRStatusDetail?: {
        statusCode?: string;
        statusDesc?: string;
        statusId?: number;
        statusName?: string;
      }
    }
  ];
  companyId?: number;
  companyName?: string;
  departmentList?: [
    {
      contactNumber?: string;
      departmentId?: number;
      deptAddress?: string;
      deptLocation?: string;
      deptName?: string;
      desc?: string;
      email?: string;
      statusId?: number;
      usRStatusDetail?: {
        statusCode?: string;
        statusDesc?: string;
        statusId?: number;
        statusName?: string;
      }
    }
  ];
  email?: string;
  firstName?: string;
  isFirstLogin?: boolean;
  lastName?: string;
  mobile?: string;
  passwordResetDateCount?: number;
  userId?: number;
  userName?: string;
  userResetDateCount?: number;
  userRoleId?: number;
  userRoleName?: string;
  userType?: string;
  userTypeId?: number
  accessToken?: string;
  expiresIn?: number;
};

export interface AuthProps {
  isLoggedIn: boolean;
  isInitialized?: boolean;
  user?: UserProfile | null;
  token?: string | null;
}

export interface AuthActionProps {
  type: string;
  payload?: AuthProps;
}

export type FirebaseContextType = {
  isLoggedIn: boolean;
  isInitialized?: boolean;
  user?: UserProfile | null | undefined;
  logout: () => Promise<void>;
  login: () => void;
  firebaseRegister: (email: string, password: string) => Promise<firebase.auth.UserCredential>;
  firebaseEmailPasswordSignIn: (email: string, password: string) => Promise<firebase.auth.UserCredential>;
  firebaseGoogleSignIn: () => Promise<firebase.auth.UserCredential>;
  firebaseTwitterSignIn: () => Promise<firebase.auth.UserCredential>;
  firebaseFacebookSignIn: () => Promise<firebase.auth.UserCredential>;
  resetPassword: (email: string) => Promise<void>;
  updateProfile: VoidFunction;
};

export type AWSCognitoContextType = {
  isLoggedIn: boolean;
  isInitialized?: boolean;
  user?: UserProfile | null | undefined;
  logout: () => void;
  login: (email: string, password: string) => Promise<void>;
  register: (email: string, password: string, firstName: string, lastName: string) => Promise<unknown>;
  resetPassword: (verificationCode: string, newPassword: string) => Promise<any>;
  forgotPassword: (email: string) => Promise<void>;
  updateProfile: VoidFunction;
};

export interface InitialLoginContextProps {
  isLoggedIn: boolean;
  isInitialized?: boolean;
  user?: UserProfile | null | undefined;
}

export interface JWTDataProps {
  userId: string;
}

export type JWTContextType = {
  isLoggedIn: boolean;
  isInitialized?: boolean;
  user?: UserProfile | null | undefined;
  logout: () => void;
  login: (userName: string, password: string) => Promise<void>;
  register: (email: string, password: string, firstName: string, lastName: string) => Promise<void>;
  resetPassword: (email: string) => Promise<void>;
  updateProfile: VoidFunction;
  resetPasswordUser: (userName: string) => Promise<void>;
};

export type Auth0ContextType = {
  isLoggedIn: boolean;
  isInitialized?: boolean;
  user?: UserProfile | null | undefined;
  logout: () => void;
  login: () => void;
  resetPassword: (email: string) => Promise<void>;
  updateProfile: VoidFunction;
}; 