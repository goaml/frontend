export type transToMyClient= {
    toMyClientId?:number,
    toFundsComment?:string,
    toForeignCurrencyId?:number,
    conductorPersonMyClientId?:number,
    toAccountMyClientId?:number,
    toPersonMyClientId?:number,
    toEntityMyClientId?:number,
    transactionId?:number,
    isActive?:boolean,
    rfundsType?:{
        fundTypeCode?: string,
    },
    rcountryCodes?:{
        description?:string,
    }
    rstatus?:{
        statusId?:number,
        isActive?:boolean,
        statusCode?:string,
    }

}

export type transToMyClientList = {
    pagination?: {
      count?: number;
      from?: number;
      to?: number;
      total?: number;
    }
    result?: Array<transToMyClient>;
  }
export interface transToMyClientStateProps {
    transToMyClientList: transToMyClientList | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean
}

export interface DefaultRootStateProps {
    transToMyClient: transToMyClientStateProps;
}

export interface queryParamsProps {
    page: number
    per_page: number
    sort: string
    direction: "asc" | "desc"
    search: string
    trxId: number
}