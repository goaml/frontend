export type transAddress ={
    address?: string,
    addressId?: number,
    cityId?: number,
    comments?: string,
    contactTypeId?: number,
    countryCodeId?: number,
    isActive?: boolean,
    rcityList?: {
      cityCode?: string,
      cityId?: number,
      createdBy?: string,
      createdOn?: string,
      description?: string,
      isActive?: boolean,
      rstatus?: {
        description?: string,
        isActive?: boolean,
        statusCode?: string,
        statusId?: number
      },
      statusId?: number,
      updatedBy?: string,
      updatedOn?: string
    },
    rcontactType?: {
      contactTypeCode?: string,
      contactTypeId?: number,
      createdBy?: string,
      createdOn?: string,
      description?: string,
      isActive?: boolean,
      rstatus?: {
        description?: string,
        isActive?: boolean,
        statusCode?: string,
        statusId?: number
      },
      statusId?: number,
      updatedBy?: string,
      updatedOn?: string
    },
    rcountryCodes?: {
      countryCodeCode?: string,
      countryCodeId?: number,
      createdBy?: string,
      createdOn?: string,
      description?: string,
      isActive?: boolean,
      rstatus?: {
        description?: string,
        isActive?: boolean,
        statusCode?: string,
        statusId?: number
      },
      statusId?: number,
      updatedBy?: string,
      updatedOn?: string
    },
    rstatus?: {
      description?: string,
      isActive?: boolean,
      statusCode?: string,
      statusId?: number
    },
    state?: string,
    statusId?: number,
    town?: string,
    zip?: string
  }

export type transAddressList = {
    pagination?: {
      count?: number;
      from?: number;
      to?: number;
      total?: number;
    }
    result?: Array<transAddress>;
  }
export interface transAddressStateProps {
    transAddressList: transAddressList | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean
}

export interface DefaultRootStateProps {
    transAddress: transAddressStateProps;
}

export interface queryParamsProps {
    page: number
    per_page: number
    sort: string
    direction: "asc" | "desc"
    search: string
}