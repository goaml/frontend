export type Ingredient = {
};

export type FundTypeListResType = {
    pagination?: paginationType
    result?: FundTypesType[]
};

export type FundTypeResType = { 
    fundTypeId?: number,
    fundTypeCode?: string,
    description?:string,
    isActive?: boolean
};

export type FundTypeReqType = { 
    fundTypeCode?: string,
};

export interface ReferenceControllerStateProps {
    FundTypeList: FundTypeListResType | null;
    FundType: FundTypeResType | null;
    error: object | string | null;
    FTsuccess: object | string | null;
    isLoading: boolean;
    selectFundType: FundTypeType[] |null;
    isActionSuccess:{
        message: string | null;
        keyValue:string |null;
        keyValueId: number |null;
    }
}

export interface DefaultRootStateProps {
    fundType: ReferenceControllerStateProps;
}

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type FundTypesType = {
    fundTypeCode?: string,
    fundTypeId?: number,
    description?:string,
    isActive?: boolean
    
}

export type FundTypeType = {
    fundTypeId?: number,
    fundTypeCode?: string,
    description?:string,
    isActive?: boolean
}