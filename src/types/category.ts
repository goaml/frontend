export type Ingredient = {
};

export type CategoryListResType = {
    pagination?: paginationType
    result?: CategorysType[]
};

export type CategoryResType = { 
    categoryId?: number,
    categoryName?: string,
    categoryDescription?: string,
    isActive?: boolean
};

export type CategoryReqType = { 
    categoryName?: string,
    categoryDescription?: string
};

export interface ReferenceControllerStateProps {
    CategoryList: CategoryListResType | null;
    Category: CategoryResType | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
    selectCategory: CategoryType[] |null;
    isActionSuccess:object | string | null;
}

export interface DefaultRootStateProps {
    category: ReferenceControllerStateProps;
}

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type CategorysType = {
    categoryName?: string,
    categoryDescription?: string,
    categoryId?: number,
    isActive?: boolean
}

export type CategoryType = {
    categoryId?: number,
    categoryName?: string,
    categoryDescription?: string,
    isActive?: boolean
}