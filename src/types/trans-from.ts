export type transFrom = {
    fromId?: number,
    fromFundsComment?: string,
    fromForeignCurrencyId?: number,
    conductorPersonId?: number,
    fromAccountId?: number,
    fromPersonId?: number,
    fromEntityId?: number,
    transactionId?: number,
    isActive?: boolean,
    rfundsType?: {
        fundTypeCode?: string,
    },
    rcountryCodes?: {
        description?: string,
    }
    rstatus?: {
        statusId?: number,
        isActive?: boolean,
        statusCode?: string,
    }

}

export type transFromList = {
    pagination?: {
        count?: number;
        from?: number;
        to?: number;
        total?: number;
    }
    result?: Array<transFrom>;
}
export interface transFromStateProps {
    transFromList: transFromList | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean
}

export interface DefaultRootStateProps {
    transFrom: transFromStateProps;
}

export interface queryParamsProps {
    page: number
    per_page: number
    sort: string
    direction: "asc" | "desc"
    search: string
    trxId: number
}