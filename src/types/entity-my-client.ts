export type MyClientEntity = {
    entityMyClientId?: number | undefined;
    name?: string | undefined;
    commercialName?: string | undefined;
    entityLegalFormTypeId?: number | undefined;
    incorporationNumber?: string | undefined;
    business?: string | undefined;
    phones?: string;
    addresses?: string;
    email?: string | undefined;
    url?: string | undefined;
    incorporationState?: string | undefined;
    countryCodeId?: number | undefined;
    personMyClientId?: number | undefined;
    entityPersonRoleTypeId?: number | undefined;
    incorporationDate?: string | undefined;
    businessClosed?: boolean | undefined;
    dateBusinessClosed?: string | undefined;
    taxNumber?: string | undefined;
    taxRegNumber?: string | undefined;
    comments?: string | undefined;
    statusId?: number | undefined;
    isActive?: boolean | undefined;

}

export type MyClientEntityList = {
    pagination?: {
        count?: number;
        from?: number;
        to?: number;
        total?: number;
    }
    result?: Array<MyClientEntity>;
}
export interface MyClientEntityStateProps {
    MyClientEntityList: MyClientEntityList | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean
}

export interface DefaultRootStateProps {
    MyClientEntity: MyClientEntityStateProps;
}

export interface queryParamsProps {
    page: number
    per_page: number
    sort: string
    direction: "asc" | "desc"
    search: string
    entityMyClientId?: number
}