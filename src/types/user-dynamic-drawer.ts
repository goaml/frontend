// ==============================|| USER DYNAMIC DRAWER TYPES ||============================== //

export type UserDynamicDrawer = {
    group?: string;
    groupSeqNo?: number;
    subGroup?: string;
    subGroupSeqNo?: number;
    id?: number;
    title?: string;
    menuSeqNo?: number;
    url?: string;
    icon?: string;
}

export interface UserDynamicDrawerStateProps {
    userDynamicDrawer: UserDynamicDrawer[] | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean
}

export interface DefaultRootStateProps {
    userDynamicDrawer: UserDynamicDrawerStateProps;
}

export interface queryParamsProps {
    moduleId: number
    userId: number
}