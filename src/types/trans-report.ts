// ==============================|| trans-report Types ||============================== //

export type transReport = {
    action?: string,
    currencyCodeLocal?: string,
    entityReference?: string,
    fiuRefNumber?: string,
    isActive?: boolean,
    location?: string,
    reason?: string,
    rentityBranch?: string,
    rentityId?: number,
    reportCode?: string,
    reportId?: number,
    reportingPerson?: string,
    rstatus?: {
      description?: string,
      isActive?: boolean,
      statusCode?: string,
      statusId?: number
    },
    statusId?: number,
    submissionCode?: string,
    submissionDate?:Date,
    tactivities?: [
      {
        activityId?: number,
        comments?: string,
        isActive?: boolean,
        reason?: string,
        reportParties?: string,
        reportParty?: string,
        rstatus?: {
          categories?: [
            {
              categoryDescription?: string,
              categoryId?: number,
              categoryName?: string,
              isActive?: boolean
            }
          ],
          description?: string,
          isActive?: boolean,
          rsubmissionTypes?: [
            {
              description?: string,
              isActive?: boolean,
              submissionTypeCode?: string,
              submissionTypeId?: number
            }
          ],
          statusCode?: string,
          statusId?: number
        },
        significance?: number,
        taccountMyClient?: {
          account?: string,
          accountMyClientId?: number,
          accountName?: string,
          accountPersonRoleTypeId?: number,
          accountStatusTypeId?: number,
          accountTypeId?: number,
          balance?: number,
          beneficiary?: string,
          beneficiaryComment?: string,
          branch?: string,
          clientNumber?: string,
          closed?:Date,
          comments?: string,
          currenciesId?: number,
          dateBalance?:Date,
          entityMyClientId?: number,
          iban?: string,
          institutionCode?: string,
          institutionName?: string,
          isActive?: boolean,
          isPrimary?: boolean,
          nonBankInstitution?: boolean,
          opened?: Date,
          personMyClientId?: number,
          rstatus?: {
            categories?: [
              {
                categoryDescription?: string,
                categoryId?: number,
                categoryName?: string,
                isActive?: boolean
              }
            ],
            description?: string,
            isActive?: boolean,
            rsubmissionTypes?: [
              {
                description?: string,
                isActive?: boolean,
                submissionTypeCode?: string,
                submissionTypeId?: number
              }
            ],
            statusCode?: string,
            statusId?: number
          },
          signatory?: string,
          swift?: string,
          tactivities?: [
            null
          ]
        },
        tentityMyClient?: {
          addressId?: number,
          addresses?: string,
          business?: string,
          businessClosed?: boolean,
          comments?: string,
          commercialName?: string,
          countryCodeId?: number,
          dateBusinessClosed?: Date,
          directorPersonId?: number,
          email?: string,
          entityLegalFormTypeId?: number,
          entityMyClientId?: number,
          entityPersonRoleTypeId?: number,
          incorporationDate?: Date,
          incorporationNumber?: string,
          incorporationState?: string,
          isActive?: boolean,
          name?: string,
          phoneId?: number,
          phones?: string,
          rstatus?: {
            categories?: [
              {
                categoryDescription?: string,
                categoryId?: number,
                categoryName?: string,
                isActive?: boolean
              }
            ],
            description?: string,
            isActive?: boolean,
            rsubmissionTypes?: [
              {
                description?: string,
                isActive?: boolean,
                submissionTypeCode?: string,
                submissionTypeId?: number
              }
            ],
            statusCode?: string,
            statusId?: number
          },
          tactivities?: [
            null
          ],
          taxNumber?: string,
          taxRegNumber?: string,
          url?: string
        },
        tpersonMyClient?: {
          addressId?: number,
          addresses?: string,
          alias?: string,
          birthPlace?: string,
          birthdate?: Date,
          comments?: string,
          deceased?: boolean,
          deceasedDate?: Date,
          email?: string,
          employerAddressId?: number,
          employerName?: string,
          employerPhoneId?: number,
          firstName?: string,
          genderTypeId?: number,
          idNumber?: string,
          isActive?: boolean,
          lastName?: string,
          middleName?: string,
          mothersName?: string,
          nationalityCountry2Id?: number,
          nationalityCountry3Id?: number,
          nationalityCountryId?: number,
          occupation?: string,
          passportCountryId?: number,
          passportNumber?: string,
          personIdentificationId?: number,
          personMyClientId?: number,
          phoneId?: number,
          phones?: string,
          prefix?: string,
          residenceCountryId?: number,
          rstatus?: {
            categories?: [
              {
                categoryDescription?: string,
                categoryId?: number,
                categoryName?: string,
                isActive?: boolean
              }
            ],
            description?: string,
            isActive?: boolean,
            rsubmissionTypes?: [
              {
                description?: string,
                isActive?: boolean,
                submissionTypeCode?: string,
                submissionTypeId?: number
              }
            ],
            statusCode?: string,
            statusId?: number
          },
          sourceOfWealth?: string,
          ssn?: string,
          tactivities?: [
            null
          ],
          taxNumber?: string,
          taxRegNumber?: string,
          title?: string
        },
        treport?: {
          action?: string,
          currencyCodeLocal?: string,
          entityReference?: string,
          fiuRefNumber?: string,
          isActive?: boolean,
          location?: string,
          reason?: string,
          rentityBranch?: string,
          rentityId?: number,
          reportCode?: string,
          reportId?: number,
          reportingPerson?: string,
          rstatus?: {
            categories?: [
              {
                categoryDescription?: string,
                categoryId?: number,
                categoryName?: string,
                isActive?: boolean
              }
            ],
            description?: string,
            isActive?: boolean,
            rsubmissionTypes?: [
              {
                description?: string,
                isActive?: boolean,
                submissionTypeCode?: string,
                submissionTypeId?: number
              }
            ],
            statusCode?: string,
            statusId?: number
          },
          submissionCode?: string,
          submissionDate?: Date,
          tactivities?: [
            null
          ],
          ttransactions?: [
            {
              acid?: string,
              amountLocal?: number,
              authorized?: string,
              comments?: string,
              custId?: string,
              datePosting?: Date,
              dateTransaction?: Date,
              goodsServices?: string,
              internalRefNumber?: string,
              isActive?: boolean,
              lateDeposit?: boolean,
              rconductionType?: {
                conductionTypeCode?: string,
                conductionTypeId?: number,
                description?: string,
                isActive?: boolean,
                rstatus?: {
                  categories?: [
                    {
                      categoryDescription?: string,
                      categoryId?: number,
                      categoryName?: string,
                      isActive?: boolean
                    }
                  ],
                  description?: string,
                  isActive?: boolean,
                  rsubmissionTypes?: [
                    {
                      description?: string,
                      isActive?: boolean,
                      submissionTypeCode?: string,
                      submissionTypeId?: number
                    }
                  ],
                  statusCode?: string,
                  statusId?: number
                }
              },
              rptCode?: string,
              rstatus?: {
                categories?: [
                  {
                    categoryDescription?: string,
                    categoryId?: number,
                    categoryName?: string,
                    isActive?: boolean
                  }
                ],
                description?: string,
                isActive?: boolean,
                rsubmissionTypes?: [
                  {
                    description?: string,
                    isActive?: boolean,
                    submissionTypeCode?: string,
                    submissionTypeId?: number
                  }
                ],
                statusCode?: string,
                statusId?: number
              },
              teller?: string,
              tfromMyClients?: [
                {
                  conductorPersonMyClientId?: number,
                  fromAccountMyClientId?: number,
                  fromEntityMyClientId?: number,
                  fromForeignCurrencyId?: number,
                  fromFundsComment?: string,
                  fromMyClientId?: number,
                  fromPersonMyClientId?: number,
                  isActive?: boolean,
                  rcountryCodes?: {
                    countryCodeCode?: string,
                    countryCodeId?: number,
                    description?: string,
                    isActive?: boolean,
                    rstatus?: {
                      categories?: [
                        {
                          categoryDescription?: string,
                          categoryId?: number,
                          categoryName?: string,
                          isActive?: boolean
                        }
                      ],
                      description?: string,
                      isActive?: boolean,
                      rsubmissionTypes?: [
                        {
                          description?: string,
                          isActive?: boolean,
                          submissionTypeCode?: string,
                          submissionTypeId?: number
                        }
                      ],
                      statusCode?: string,
                      statusId?: number
                    }
                  },
                  rfundsType?: {
                    description?: string,
                    fundTypeCode?: string,
                    fundTypeId?: number,
                    isActive?: boolean,
                    rstatus?: {
                      categories?: [
                        {
                          categoryDescription?: string,
                          categoryId?: number,
                          categoryName?: string,
                          isActive?: boolean
                        }
                      ],
                      description?: string,
                      isActive?: boolean,
                      rsubmissionTypes?: [
                        {
                          description?: string,
                          isActive?: boolean,
                          submissionTypeCode?: string,
                          submissionTypeId?: number
                        }
                      ],
                      statusCode?: string,
                      statusId?: number
                    }
                  },
                  rstatus?: {
                    categories?: [
                      {
                        categoryDescription?: string,
                        categoryId?: number,
                        categoryName?: string,
                        isActive?: boolean
                      }
                    ],
                    description?: string,
                    isActive?: boolean,
                    rsubmissionTypes?: [
                      {
                        description?: string,
                        isActive?: boolean,
                        submissionTypeCode?: string,
                        submissionTypeId?: number
                      }
                    ],
                    statusCode?: string,
                    statusId?: number
                  }
                }
              ],
              tfroms?: [
                {
                  conductorPersonId?: number,
                  fromAccountId?: number,
                  fromEntityId?: number,
                  fromForeignCurrencyId?: number,
                  fromFundsComment?: string,
                  fromId?: number,
                  fromPersonId?: number,
                  isActive?: boolean,
                  rcountryCodes?: {
                    countryCodeCode?: string,
                    countryCodeId?: number,
                    description?: string,
                    isActive?: boolean,
                    rstatus?: {
                      categories?: [
                        {
                          categoryDescription?: string,
                          categoryId?: number,
                          categoryName?: string,
                          isActive?: boolean
                        }
                      ],
                      description?: string,
                      isActive?: boolean,
                      rsubmissionTypes?: [
                        {
                          description?: string,
                          isActive?: boolean,
                          submissionTypeCode?: string,
                          submissionTypeId?: number
                        }
                      ],
                      statusCode?: string,
                      statusId?: number
                    }
                  },
                  rfundsType?: {
                    description?: string,
                    fundTypeCode?: string,
                    fundTypeId?: number,
                    isActive?: boolean,
                    rstatus?: {
                      categories?: [
                        {
                          categoryDescription?: string,
                          categoryId?: number,
                          categoryName?: string,
                          isActive?: boolean
                        }
                      ],
                      description?: string,
                      isActive?: boolean,
                      rsubmissionTypes?: [
                        {
                          description?: string,
                          isActive?: boolean,
                          submissionTypeCode?: string,
                          submissionTypeId?: number
                        }
                      ],
                      statusCode?: string,
                      statusId?: number
                    }
                  },
                  rstatus?: {
                    categories?: [
                      {
                        categoryDescription?: string,
                        categoryId?: number,
                        categoryName?: string,
                        isActive?: boolean
                      }
                    ],
                    description?: string,
                    isActive?: boolean,
                    rsubmissionTypes?: [
                      {
                        description?: string,
                        isActive?: boolean,
                        submissionTypeCode?: string,
                        submissionTypeId?: number
                      }
                    ],
                    statusCode?: string,
                    statusId?: number
                  }
                }
              ],
              transactionDescription?: string,
              transactionId?: number,
              transactionLocation?: string,
              transactionNumber?: string,
              transmodeComment?: string,
              trxNo?: string,
              ttoMyClients?: [
                {
                  isActive?: boolean,
                  rcountryCodes?: {
                    countryCodeCode?: string,
                    countryCodeId?: number,
                    description?: string,
                    isActive?: boolean,
                    rstatus?: {
                      categories?: [
                        {
                          categoryDescription?: string,
                          categoryId?: number,
                          categoryName?: string,
                          isActive?: boolean
                        }
                      ],
                      description?: string,
                      isActive?: boolean,
                      rsubmissionTypes?: [
                        {
                          description?: string,
                          isActive?: boolean,
                          submissionTypeCode?: string,
                          submissionTypeId?: number
                        }
                      ],
                      statusCode?: string,
                      statusId?: number
                    }
                  },
                  rfundsType?: {
                    description?: string,
                    fundTypeCode?: string,
                    fundTypeId?: number,
                    isActive?: boolean,
                    rstatus?: {
                      categories?: [
                        {
                          categoryDescription?: string,
                          categoryId?: number,
                          categoryName?: string,
                          isActive?: boolean
                        }
                      ],
                      description?: string,
                      isActive?: boolean,
                      rsubmissionTypes?: [
                        {
                          description?: string,
                          isActive?: boolean,
                          submissionTypeCode?: string,
                          submissionTypeId?: number
                        }
                      ],
                      statusCode?: string,
                      statusId?: number
                    }
                  },
                  rstatus?: {
                    categories?: [
                      {
                        categoryDescription?: string,
                        categoryId?: number,
                        categoryName?: string,
                        isActive?: boolean
                      }
                    ],
                    description?: string,
                    isActive?: boolean,
                    rsubmissionTypes?: [
                      {
                        description?: string,
                        isActive?: boolean,
                        submissionTypeCode?: string,
                        submissionTypeId?: number
                      }
                    ],
                    statusCode?: string,
                    statusId?: number
                  },
                  toAccountMyClientId?: number,
                  toEntityMyClientId?: number,
                  toForeignCurrencyId?: number,
                  toFundsComment?: string,
                  toMyClientId?: number,
                  toPersonMyClientId?: number
                }
              ],
              ttos?: [
                {
                  isActive?: boolean,
                  rcountryCodes?: {
                    countryCodeCode?: string,
                    countryCodeId?: number,
                    description?: string,
                    isActive?: boolean,
                    rstatus?: {
                      categories?: [
                        {
                          categoryDescription?: string,
                          categoryId?: number,
                          categoryName?: string,
                          isActive?: boolean
                        }
                      ],
                      description?: string,
                      isActive?: boolean,
                      rsubmissionTypes?: [
                        {
                          description?: string,
                          isActive?: boolean,
                          submissionTypeCode?: string,
                          submissionTypeId?: number
                        }
                      ],
                      statusCode?: string,
                      statusId?: number
                    }
                  },
                  rfundsType?: {
                    description?: string,
                    fundTypeCode?: string,
                    fundTypeId?: number,
                    isActive?: boolean,
                    rstatus?: {
                      categories?: [
                        {
                          categoryDescription?: string,
                          categoryId?: number,
                          categoryName?: string,
                          isActive?: boolean
                        }
                      ],
                      description?: string,
                      isActive?: boolean,
                      rsubmissionTypes?: [
                        {
                          description?: string,
                          isActive?: boolean,
                          submissionTypeCode?: string,
                          submissionTypeId?: number
                        }
                      ],
                      statusCode?: string,
                      statusId?: number
                    }
                  },
                  rstatus?: {
                    categories?: [
                      {
                        categoryDescription?: string,
                        categoryId?: number,
                        categoryName?: string,
                        isActive?: boolean
                      }
                    ],
                    description?: string,
                    isActive?: boolean,
                    rsubmissionTypes?: [
                      {
                        description?: string,
                        isActive?: boolean,
                        submissionTypeCode?: string,
                        submissionTypeId?: number
                      }
                    ],
                    statusCode?: string,
                    statusId?: number
                  },
                  toAccountId?: number,
                  toEntityId?: number,
                  toForeignCurrencyId?: number,
                  toFundsComment?: string,
                  toId?: number,
                  toPersonId?: number
                }
              ],
              valueDate?: Date
            }
          ]
        }
      }
    ],
    transactions?: [
      {
        acid?: string,
        amountLocal?: number,
        authorized?: string,
        comments?: string,
        conductionTypeId?: number,
        custId?: string,
        datePosting?: Date,
        dateTransaction?: Date,
        goodsServices?: string,
        internalRefNumber?: string,
        isActive?: boolean,
        lateDeposit?: boolean,
        rconductionType?: {
          conductionTypeCode?: string,
          conductionTypeId?: number,
          createdBy?: string,
          createdOn?: Date,
          description?: string,
          isActive?: boolean,
          rstatus?: {
            description?: string,
            isActive?: boolean,
            statusCode?: string,
            statusId?: number
          },
          statusId?: number,
          updatedBy?: string,
          updatedOn?: Date
        },
        reportId?: number,
        rptCode?: string,
        rstatus?: {
          description?: string,
          isActive?: boolean,
          statusCode?: string,
          statusId?: number
        },
        statusId?: number,
        teller?: string,
        transactionDescription?: string,
        transactionId?: number,
        transactionLocation?: string,
        transactionNumber?: string,
        transmodeComment?: string,
        trxNo?: string,
        valueDate?:Date
      }
    ]
  }

  export type transReportList = {
    pagination?: {
      count?: number;
      from?: number;
      to?: number;
      total?: number;
    }
    result?: Array<transReport>;
  }
export interface transReportStateProps {
    transReportList: transReportList | null;
    error: object | string | null;
    success: object | string | null;
    selectReportType: transReport[] |null
    isLoading: boolean
}

export interface DefaultRootStateProps {
    transReport: transReportStateProps;
}

export interface queryParamsProps {
    page: number
    per_page: number
    sort: string
    direction: "asc" | "desc"
    search: string
}