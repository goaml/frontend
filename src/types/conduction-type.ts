export type Ingredient = {
};

export type ConductionTypeListResType = {
    pagination?: paginationType
    result?: ConductionTypesType[]
};

export type ConductionTypeResType = { 
    conductionTypeId?: number,
    conductionTypeCode?: string,
    description?: string,
    StatusId?:number,
    isActive?: boolean
};

export type ConductionTypeReqType = { 
    conductionTypeCode?: string,
    description?: string, 
};

export interface ConductionTypeStateProps {
    conductionTypeList: ConductionTypeListResType | null;
    conductionType: ConductionTypeResType | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
    isActionSuccess: object | string | null;
}

export interface DefaultRootStateProps {
    conductionType: ConductionTypeStateProps;
}

export type listParametersType = {
    page?: number
    perPage?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type ConductionTypesType = {
    conductionTypeCode?: string,
    description?: string,
    conductionTypeId?: number,
    statusId?:number,
    isActive?: boolean
}

export type ConductionTypeType = {
    conductionTypeId?: number,
    conductionTypeCode?: string,
    description?: string,
    statusId?:number,
    isActive?: boolean
}