export type Ingredient = {
};

export type ReportIndicatorTypeListResType = {
    pagination?: paginationType
    result?: ReportIndicatorTypesType[]
};

export type ReportIndicatorTypeResType = { 
    reportIndicatorTypeCode: string,
    description?:string,
};

export type ReportIndicatorTypeReqType = { 
    
    reportIndicatorTypeId?: number,
    reportIndicatorTypeCode: string,
    description?:string,
};

export interface ReferenceControllerStateProps {
    reportIndicatorTypeList: ReportIndicatorTypeListResType | null;
    reportIndicatorType: ReportIndicatorTypeResType | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
    selectreportIndicatorType: ReportIndicatorTypeType[] |null
    xmlData: string| null, 
    isActionSuccess: object | string | null;
}

export interface DefaultRootStateProps {
    reportIndicatorType: ReferenceControllerStateProps;
}

export type listParametersType = {
    page?: number
    perPage?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type ReportIndicatorTypesType = {
    reportIndicatorTypeCode: string,
    reportIndicatorTypeId?: number,
    description?:string,
    
}

export type ReportIndicatorTypeType = {
    reportIndicatorTypeId?: number,
    reportIndicatorTypeCode?: string,
    description?:string,
    
}