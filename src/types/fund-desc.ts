export type Ingredient = {
};

export type FundTypeListResType = {
    pagination?: paginationType
    result?: FundTypesType[]
};

export type FundTypeResType = { 
    fundTypeDescriptionId?: number,
    fundTypeDescription?: string,
    isActive?: boolean
};

export type FundTypeReqType = { 
    fundTypeDescription?: string,
};

export interface ReferenceControllerStateProps {
    FundTypeList: FundTypeListResType | null;
    FundType: FundTypeResType | null;
    error: object | string | null;
    FTDsuccess: object | string | null;
    isLoading: boolean;
    selectFundTypeDesc: FundTypeType[] |null
}

export interface DefaultRootStateProps {
    fundTypeDescription: ReferenceControllerStateProps;
}

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type FundTypesType = {
    fundTypeDescription?: string,
    fundTypeDescriptionId?: number,
    isActive?: boolean
}

export type FundTypeType = {
    fundTypeDescriptionId?: number,
    fundTypeDescription?: string,
    isActive?: boolean
}