export type Ingredient = {
};

export type BranchCodeListResType = {
    pagination?: paginationType
    result?: BranchCodesType[]
};

export type BranchCodeResType = {
    branchId?: number,
    branchName?: string,
    branchLocation?:string,
    desc?: string,
    statusDesc?: string
};

export type Branches = {

    brachDeptId: number,
    branchId: number,
    branchName: string,
    branchLocation:string,
    departmentId: number,

};

export type BranchCodeReqType = {
    branchAddress?: string,
    branchId?: number,
    branchLocation?: string,
    branchName?: string,
    contactNumber?: string,
    desc?: string,
    email?: string,
    limitAmt?: number,
};

export type BranchesForUsers = {
    brachDeptId?: number
    branchAddress?: string,
    branchId?: number,
    branchLocation?: string,
    branchName?: string,
    contactNumber?: string,
    created_by?: string,
    created_on?: {
        date?: number,
        day?: number,
        hours?: number,
        minutes?: number,
        month?: number,
        nanos?: number,
        seconds?: number,
        time?: number,
        timezoneOffset?: number,
        year?: number
    },
    desc?: string,
    email?: string,
    limitAmt?: number,
    statusId?: number,
    updated_by?: string,
    updated_on?: {
        date?: number,
        day?: number,
        hours?: number,
        minutes?: number,
        month?: number,
        nanos?: number,
        seconds?: number,
        time?: number,
        timezoneOffset?: number,
        year?: number
    },
    usRStatusDetail?: {
        statusCode?: string,
        statusDesc?: string,
        statusId?: number,
        statusName?: string
    }

};

export type MBlist = {
    employeeBranchId: number,
    from: string,
    isDefualtDept: number,
    to: string,
    userId: number,
    brachDeptId: number,
    statusId: number,
    branchName: string,
    deptName: string,
    isActive: boolean
}

export type MultipleBranchesProps = {
    pagination?: {
        count?: number
        from?: number
        to?: number
        total?: number
    }
    result?: Array<MBlist>
}
export interface BranchCodeStateProps {
    branchCodeList: BranchCodeListResType | null;
    branchCode: BranchCodeResType | null;
    selectBranchCodes: BranchCode[] | null;
    MultipleBranches: MultipleBranchesProps | null;
    selectBranchesForUsers: BranchesForUsers[] | null;
    selectBranches: Branches[] | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean
}

export interface DefaultRootStateProps {
    branch: BranchCodeStateProps;
}

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type BranchCodesType =
    {
        branchAddress: string,
        branchId: number,
        branchLocation: string,
        branchName: string,
        contactNumber: string,
        created_by: string,
        created_on: {
            date: number,
            day: number,
            hours: number,
            minutes: number,
            month: number,
            nanos: number,
            seconds: number,
            time: number,
            timezoneOffset: number,
            year: number
        },
        desc: string,
        email: string,
        limitAmt: number,
        statusId: number,
        updated_by: string,
        updated_on: {
            date: number,
            day: number,
            hours: number,
            minutes: number,
            month: number,
            nanos: number,
            seconds: number,
            time: number,
            timezoneOffset: number,
            year: number
        },
        usRStatusDetail: {
            statusCode: string,
            statusDesc: string,
            statusId: number,
            statusName: string
        }
    }


export type BranchCodeType = {
    branchId?: number,
    branchName?: string,
    branchLocation?:string,
    desc?: string,
    statusDesc?: string
}

// Define the type for a single user role
export interface BranchCode {
    branchId: number;
    branchName: string;
    // Add more properties if necessary
}

// Define the type for the 'user' slice of the state
export interface BranchState {
    branch: BranchCode[]; // Assuming 'roles' is where the user roles are stored
    // Add more properties if necessary
}

// Define RootState to include the 'user' slice of the state
export interface RootState {
    branch: BranchState;
    // Add other slices of the state as needed
}
