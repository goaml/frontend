export type Ingredient = {
};

export type MultiPartyRoleListResType = {
    pagination?: paginationType
    result?: MultiPartyRolesType[]
};

export type MultiPartyRoleResType = { 
    multiPartyRoleName?: string,
    multiPartyRoleDescription?:string,
    isActive?:boolean
};

export type MultiPartyRoleReqType = { 
    multiPartyRoleName?: string,
};

export interface ReferenceControllerStateProps {
    MultiPartyRoleList: MultiPartyRoleListResType | null;
    MultiPartyRole: MultiPartyRoleResType | null;
    error: object | string | null;
    MPRsuccess: object | string | null;
    isLoading: boolean;
    selectMultiPartyRole: MultiPartyRoleType[] |null
}

export interface DefaultRootStateProps {
    multiPartyRole: ReferenceControllerStateProps;
}

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type MultiPartyRolesType = {
    multiPartyRoleName?: string,
    multiPartyRoleId?: number,
    multiPartyRoleDescription?:string,
    isActive?:boolean
}

export type MultiPartyRoleType = {
    multiPartyRoleId?: number,
    multiPartyRoleName?: string,
    multiPartyRoleDescription?:string,
    isActive?:boolean
    
}