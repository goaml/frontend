export type Ingredient = {
};

export type EntityPersonRoleTypeListResType = {
    pagination?: paginationType
    result?: EntityPersonRoleTypesType[]
};

export type EntityPersonRoleTypeResType = { 
    entityPersonRoleTypeId?: number,
    entityPersonRoleTypeCode?: string,
    description?: string,
    statusId?:number,
    isActive?: boolean
};

export type EntityPersonRoleTypeReqType = { 
    entityPersonRoleTypeCode?: string,
    description?: string, 
};

export interface EntityPersonRoleTypeStateProps {
    entityPersonRoleTypeList: EntityPersonRoleTypeListResType | null;
    entityPersonRoleType: EntityPersonRoleTypeResType | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
    isActionSuccess: object | string | null;
}

export interface DefaultRootStateProps {
    entityPersonRoleType: EntityPersonRoleTypeStateProps;
}

export type listParametersType = {
    page?: number
    perPage?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type EntityPersonRoleTypesType = {
    entityPersonRoleTypeCode?: string,
    description?: string,
    entityPersonRoleTypeId?: number,
    statusId?:number,
    isActive?: boolean
}

export type EntityPersonRoleTypeType = {
    entityPersonRoleTypeId?: number,
    entityPersonRoleTypeCode?: string,
    description?: string,
    statusId?:number,
    isActive?: boolean
}