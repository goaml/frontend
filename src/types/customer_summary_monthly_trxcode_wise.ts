
export type customer_summary_monthly_trxcode_wiseType = {
    creditAverage?: number,
      custId?: string,
      debitAverage?: number,
      maxCreditAmt?: number,
      maxDebitAmt?: number,
      minCreditAmt?: number,
      minDebitAmt?: number,
      month?: string,
      noOfCreditTransactions?: number,
      noOfDebitTransactions?: number,
      noOfTransactions?: number,
      summaryLogId?: number,
      totNetAmt?: number,
      totalCreditAmt?: number,
      totalDebitAmt?: number,
      year?: string,
      rptCode?: string
    
}

export type customer_summary_monthly_trxcode_wiseListResType = {
    pagination?: paginationType
    result?: customer_summary_monthly_trxcode_wiseType[]
};

export type customer_summary_monthly_trxcode_wiseReqType = { 
    customer_summary_monthly_trxcode_wiseCode?: string,
    description?: string, 
};

export interface customer_summary_monthly_trxcode_wiseStateProps {
    customer_summary_monthly_trxcode_wiseList: customer_summary_monthly_trxcode_wiseListResType | null;
    customer_summary_monthly_trxcode_wise: customer_summary_monthly_trxcode_wiseType | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
    isActionSuccess: object | string | null;
}

export interface DefaultRootStateProps {
    customer_summary_monthly_trxcode_wise: customer_summary_monthly_trxcode_wiseStateProps;
}

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
    toMonth?: string
    toYear?: string
    custId?: string
    fromMonth?: string
    fromYear?: string
    rptCode?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

