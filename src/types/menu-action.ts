// ==============================|| Menu action Types ||============================== //

//to identify response body in list

export type MenuActions = {

  actionId: number,
  isMenu: boolean,
  menuActionId: number,
  menuId: number,
  moduleId: number,
  name: string,
  statusId: number,
  url: string,
  usMAction: {
    actionCode: string,
    actionId: number,
    actionName: string,
    actionStatus: number,
    created_by: string,
    created_on: {
      date: number,
      day: number,
      hours: number,
      minutes: number,
      month: number,
      nanos: number,
      seconds: number,
      time: number,
      timezoneOffset: number,
      year: number
    },
    statusId: number,
    updated_by: string,
    updated_on: {
      date: number,
      day: number,
      hours: number,
      minutes: number,
      month: number,
      nanos: number,
      seconds: number,
      time: number,
      timezoneOffset: number,
      year: number
    },
    usRStatusDetail: {
      statusCode: string,
      statusDesc: string,
      statusId: number,
      statusName: string
    }
  },
  usMMenu: {
    created_by: string,
    created_on: {
      date: number,
      day: number,
      hours: number,
      minutes: number,
      month: number,
      nanos: number,
      seconds: number,
      time: number,
      timezoneOffset: number,
      year: number
    },
    menuDesc: string,
    menuHashcode: string,
    menuId: number,
    menuName: string,
    menuUrl: string,
    statusId: number,
    updated_by: string,
    updated_on: {
      date: number,
      day: number,
      hours: number,
      minutes: number,
      month: number,
      nanos: number,
      seconds: number,
      time: number,
      timezoneOffset: number,
      year: number
    },
    usRStatusDetail: {
      statusCode: string,
      statusDesc: string,
      statusId: number,
      statusName: string
    }
  },
  usMModule: {
    created_by: string,
    created_on: {
      date: number,
      day: number,
      hours: number,
      minutes: number,
      month: number,
      nanos: number,
      seconds: number,
      time: number,
      timezoneOffset: number,
      year: number
    },
    moduleId: number,
    moduleName: string,
    updated_by: string,
    updated_on: {
      date: number,
      day: number,
      hours: number,
      minutes: number,
      month: number,
      nanos: number,
      seconds: number,
      time: number,
      timezoneOffset: number,
      year: number
    }
  },
  usRUserStatusDetail: {
    statusCode: string,
    statusDesc: string,
    statusId: number,
    statusName: string
  }
}


export type MenuActionList = {
  pagination?: {
    count?: number
    from?: number
    to?: number
    total?: number
  }
  result?: Array<MenuActions>
}

export interface MenuActionStateProps {
  menuActions: MenuActionList | null;
  menuAction: MenuActions | null;
  error: object | string | null;
  success: object | string | null;
  isLoading: boolean;
  isActionSuccess:object | string | null;
}

export interface DefaultRootStateProps {
  menuAction: MenuActionStateProps;
}

export interface queryParamsProps {
  page: number
  per_page: number
  sort: "menuActionId"
  statusId:number
  direction: "asc" | "desc"
  search: string
}