export type transParty= {
    partyId?:number,
    accountMyClientId?:number,
    entityMyClientId?:number,
    personMyClientId?:number,
    fundsComment?:string,
    foreignCurrencyId?:number,
    country?:string,
    countryCodeId?:number,
    comments?:string,
    isActive?:boolean,
    rfundsType?:{
        fundTypeCode?: string,
    },
    ttransactions?:{
        transactionId?:string,
    }

}

export type transPartyList = {
    pagination?: {
      count?: number;
      from?: number;
      to?: number;
      total?: number;
    }
    result?: Array<transParty>;
  }
export interface transPartyStateProps {
    transPartyList: transPartyList | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean
}

export interface DefaultRootStateProps {
    transParty: transPartyStateProps;
}

export interface queryParamsProps {
    page: number
    per_page: number
    sort: string
    direction: "asc" | "desc"
    search: string
}