export type Ingredient = {
};

export type DepartmentCodeListResType = {
    pagination?: paginationType
    result?: DepartmentCodesType[]
};

export type DepartmentCodeResType = { 
    departmentId?: number,
    deptName?: string,
    desc?: string,
    statusDesc?:string 
};

export type DepartmentCodeReqType = { 
    deptAddress?: string,
    deptLocation?: string,
    contactNumber?:number,
    deptName?: string,
    desc?: string, 
    email?: string,
    branchId?: number
};

export interface DepartmentCodeStateProps {
    departmentCodeList: DepartmentCodeListResType | null;
    departmentCode: DepartmentCodeResType | null;
    selectDepartments: DeptType[] | null;
    error: object | string | null;
    deptSuccess: object | string | null;
    isLoading: boolean
}

export interface DefaultRootStateProps {
    dept: DepartmentCodeStateProps;
}

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    search?: string,
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type DepartmentCodesType = 
{
    departmentId: number,
    contactNumber: string,
    deptAddress: string,
    deptLocation: string,
    deptName: string,
    desc: string,
    email: string,
    statusId: number,
    usRStatusDetail: {
      statusId: number,
      statusCode: number,
      statusDesc: string,
      statusName: string
    }
}


export type DepartmentCodeType = {
    departmentId?: number,
    deptName?: string,
    desc?: string,
    statusDesc?: string
}

// Define the type for a single user role
export interface DeptType {
    departmentId: number;
    deptName: string;
    // Add more properties if necessary
}

// Define the type for the 'user' slice of the state
export interface DepartmentState {
    depts: DeptType[]; // Assuming 'roles' is where the user roles are stored
    // Add more properties if necessary
}

// Define RootState to include the 'user' slice of the state
export interface RootState {
    department: DepartmentState;
    // Add other slices of the state as needed
}

// ==============================|| CivilStatus Types ||============================== //


export type UserPostReq = {
    userName: string,
    email: string,
    mobileNo: string,
    firstName: string,
    lastName: string,
    password: string,
    userRoleId: number,
    deptId?: number,
    departmentId: number,
    userId?: number
  };
  
  export type Departments = {
  
    brachDeptId: number,
    deptId: number,
    deptName: string,
    departmentId: number,
  
  };
  
  export type DepartmentsForUsers = {
    brachDeptId?: number
    deptAddress?: string,
    deptId?: number,
    deptLocation?: string,
    deptName?: string,
    contactNumber?: string,
    created_by?: string,
    created_on?: {
      date?: number,
      day?: number,
      hours?: number,
      minutes?: number,
      month?: number,
      nanos?: number,
      seconds?: number,
      time?: number,
      timezoneOffset?: number,
      year?: number
    },
    desc?: string,
    email?: string,
    limitAmt?: number,
    statusId?: number,
    updated_by?: string,
    updated_on?: {
      date?: number,
      day?: number,
      hours?: number,
      minutes?: number,
      month?: number,
      nanos?: number,
      seconds?: number,
      time?: number,
      timezoneOffset?: number,
      year?: number
    },
    usRStatusDetail?: {
      statusCode?: string,
      statusDesc?: string,
      statusId?: number,
      statusName?: string
    }
  
  };
  
  export type MultipleDepartments = {
    brachDeptId: number,
    deptId: number,
    deptName: string,
    departmentId: number,
    isActive: boolean,
    usMDept: {
      deptAddress: string,
      deptId: number,
      deptLocation: string,
      deptName: string,
      contactNumber: string,
      created_by: string,
      created_on: {
        date: number,
        day: number,
        hours: number,
        minutes: number,
        month: number,
        nanos: number,
        seconds: number,
        time: number,
        timezoneOffset: number,
        year: number
      },
      desc: string,
      email: string,
      limitAmt: number,
      statusId: number,
      updated_by: string,
      updated_on: {
        date: number,
        day: number,
        hours: number,
        minutes: number,
        month: number,
        nanos: number,
        seconds: number,
        time: number,
        timezoneOffset: number,
        year: number
      },
      usRStatusDetail: {
        statusCode: string,
        statusDesc: string,
        statusId: number,
        statusName: string
      }
    },
    usMDepartment: {
      contactNumber: string,
      departmentId: number,
      deptAddress: string,
      deptLocation: string,
      deptName: string,
      desc: string,
      email: string,
      statusId: number,
      usRStatusDetail: {
        statusCode: string,
        statusDesc: string,
        statusId: number,
        statusName: string
      }
    }
  }
  
  export type MBlist = {
    employeedeptId: number,
    from: string,
    isDefualtDept: number,
    to: string,
    userId: number,
    brachDeptId: number,
    statusId: number,
    deptName: string,
    isActive: boolean
  }
  
  export type UserList = {
    pagination?: {
      count?: number
      from?: number
      to?: number
      total?: number
    }
    result?: Array<Departments>
  }
  
  export type MultipleDepartmentsProps = {
    pagination?: {
      count?: number
      from?: number
      to?: number
      total?: number
    }
    result?: Array<MBlist>
  }
  
  export type DeptCodeListResType = {
    pagination?: paginationType
    result?: DeptCodesType[]
  };
  
  
  export type DeptCodesType = {
    deptAddress: string,
    deptId: number,
    deptLocation: string,
    deptName: string,
    contactNumber: string,
    created_by: string,
    created_on: {
      date: number,
      day: number,
      hours: number,
      minutes: number,
      month: number,
      nanos: number,
      seconds: number,
      time: number,
      timezoneOffset: number,
      year: number
    },
    desc: string,
    email: string,
    limitAmt: number,
    statusId: number,
    updated_by: string,
    updated_on: {
      date: number,
      day: number,
      hours: number,
      minutes: number,
      month: number,
      nanos: number,
      seconds: number,
      time: number,
      timezoneOffset: number,
      year: number
    },
    usRStatusDetail: {
      statusCode: string,
      statusDesc: string,
      statusId: number,
      statusName: string
    }
  }
 
  export type DeptCodeType = {
    deptId?: number,
    deptName?: string,
    desc?: string,
    statusDesc?: string
  }
  
  export type DeptCodeResType = {
    deptId?: number,
    deptName?: string,
    desc?: string,
    statusDesc?: string
  };
  
  export type DeptCodeReqType = {
    deptAddress?: string,
    deptId?: number,
    deptLocation?: string,
    deptName?: string,
    contactNumber?: string,
    desc?: string,
    email?: string,
    limitAmt?: number,
  };
  
  export interface UserStateProps {
    userList: UserList | null;
    users: Departments | null;
    selectDepartments: Departments[] | null;
    selectDepartmentsForUsers: DepartmentsForUsers[] | null;
    error: object | string | null;
    success: object | string | null;
    MultipleDepartments: MultipleDepartmentsProps | null;
    DeptCodeList: DeptCodeListResType | null;
    isLoading: boolean
  }
  
  export interface DefaultRootStateProps {
    userDepartments: UserStateProps;
  }
  
  export interface queryParamsProps {
    page: number
    per_page: number
    sort: "userId"
    direction: "asc" | "desc"
    search: string
  }
  