export type Ingredient = {
};

export type EntityLegalFormTypeListResType = {
    pagination?: paginationType
    result?: EntityLegalFormTypesType[]
};

export type EntityLegalFormTypeResType = { 
    entityLegalFormTypeId?: number,
    entityLegalFormTypeCode?: string,
    description?: string,
    StatusId?:number,
    isActive?: boolean
};

export type EntityLegalFormTypeReqType = { 
    entityLegalFormTypeCode?: string,
    description?: string, 
};

export interface EntityLegalFormTypeStateProps {
    entityLegalFormTypeList: EntityLegalFormTypeListResType | null;
    entityLegalFormType: EntityLegalFormTypeResType | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
    isActionSuccess: object | string | null;
}

export interface DefaultRootStateProps {
    entityLegalFormType: EntityLegalFormTypeStateProps;
}

export type listParametersType = {
    page?: number
    perPage?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type EntityLegalFormTypesType = {
    entityLegalFormTypeCode?: string,
    description?: string,
    entityLegalFormTypeId?: number,
    statusId?:number,
    isActive?: boolean
}

export type EntityLegalFormTypeType = {
    entityLegalFormTypeId?: number,
    entityLegalFormTypeCode?: string,
    description?: string,
    statusId?:number,
    isActive?: boolean
}