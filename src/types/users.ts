

export type Ingredient = {
};

export type UserListResType = {
    pagination?: paginationType
    result?: UsersType[]
};

export type UserResType = {
    userId?: number,
    userRoleName?: string,
    userTypeName?: string,
    empEmail?: string,
    empMobileNumber?: string,
    statusName?: string,
    nic?: string,
    isActive?: boolean
};

export type PostUserReqType = {
    branchId?: number,
    comId?: number,
    departmentId?: number,
    description?: string,
    email?: string,
    firstName?: string,
    isExistingCompany?: boolean,
    lastName?: string,
    mobileNo?: string,
    nic?: string,
    referenceNo?: string,
    userRoleId?: number,
    userTypeId?: number,
    userId?: number;
};

export type UserDefaultBranch = {
    defaultBranch: string,
};

export type Users = {
    address?: string;
    desc?: string;
    empEmail?: string;
    empMobileNumber?: string;
    empNic?: string;
    empNumber?: string;
    firstName?: string;
    gender?: string;
    lastName?: string;
    designationId?: number;
    employeeId?: null;
    userRoleId?: number;
    statusId?: number;
    userId?: number;
    userName?: string;
    isActive?: boolean;
    wfProcessId?: number;
    keycloakUserId?: number;
    departmentId?: number;
    branchId?: number;
    fullName?: string;
};

export interface UserStateProps {
    roles: any;
    userList: UserListResType | null;
    user: UserResType | null;
    userDefaultBranchById?: UserDefaultBranch | null;
    selectUsers?: Users[] | null;
    error: object | string | null;
    success: object | string | null;
    userById?: UserResType | null;
    EditProfileSuccess?: object | string | null;
    isLoading: boolean;
    isActionSuccess: object | string | null;
}

export interface DefaultRootStateProps {
    user: UserStateProps;
}

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    search: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type UsersType = {
    description?: string,
    empEmail?: string,
    empMobileNumber?: string,
    firstName?: string,
    lastName?: string,
    userRoleId?: number,
    statusId?: number,
    userId?: number,
    userName?: string,
    isActive?: boolean,
    nic?: string,
    referenceNo?: string,
    departmentId?: number,
    branchId?: number,
    statusName?: string,
    userRoleName?: string,
    branchName?: string,
    departmentName?: string,
    userTypeId?: number,
    userTypeName?: string,
    companyId?: number
}

export type UserType = {
    description?: string,
    empEmail?: string,
    empMobileNumber?: string,
    firstName?: string,
    lastName?: string,
    userRoleId?: number,
    statusId?: number,
    userId?: number,
    userName?: string,
    isActive?: boolean,
    nic?: string,
    referenceNo?: string,
    departmentId?: number,
    branchId?: number,
    statusName?: string,
    userRoleName?: string,
    branchName?: string,
    departmentName?: string,
    userTypeId?: number,
    userTypeName?: string,
    companyId?: number,
    companyName?: string
}

export type updateUserType = {
    description?: string,
    email?: string,
    mobileNo?: string,
    firstName?: string,
    lastName?: string,
    userRoleId?: number,
    statusId?: number,
    userId?: number,
    userName?: string,
    isActive?: boolean,
    nic?: string,
    referenceNo?: string,
    departmentId?: number,
    branchId?: number,
    statusName?: string,
    userRoleName?: string,
    branchName?: string,
    departmentName?: string,
    userTypeId?: number,
    userTypeName?: string,
    companyId?: number

}
export interface queryParamsProps {
    page: number;
    per_page: number;
    sort: "userId";
    direction: "asc" | "desc";
    search: string;
}
