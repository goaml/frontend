

export type ReportType =  {
    isActive: boolean,
    reportTypeDescription: string,
    reportTypeId: number,
    reportTypeName: string,
    rstatus: {
      description: string,
      isActive: boolean,
      statusCode: string,
      statusId: number
    },
    statusId: number
  }

export type ReportTypeList = {
    pagination?: paginationType
    result?: ReportType[]
};

export interface ReportTypeStateProps {
    reportTypeList: ReportType | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
    selectReportType: ReportType[] |null
    isActionSuccess:object | string | null;   
}

export interface DefaultRootStateProps {
    reportType: ReportTypeStateProps;
}

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

