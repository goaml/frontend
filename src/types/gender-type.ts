export type Ingredient = {
};

export type GenderTypeListResType = {
    pagination?: paginationType
    result?: GenderTypesType[]
};

export type GenderTypeResType = { 
    genderTypeId?: number,
    genderTypeCode?: string,
    description?: string,
    statusId?:number,
    isActive?: boolean
};

export type GenderTypeReqType = { 
    genderTypeCode?: string,
    description?: string, 
};

export interface GenderTypeStateProps {
    genderTypeList: GenderTypeListResType | null;
    genderType: GenderTypeResType | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
    isActionSuccess: object | string | null;
}

export interface DefaultRootStateProps {
    genderType: GenderTypeStateProps;
}

export type listParametersType = {
    page?: number
    perPage?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type GenderTypesType = {
    genderTypeCode?: string,
    description?: string,
    genderTypeId?: number,
    statusId?:number,
    isActive?: boolean
}

export type GenderTypeType = {
    genderTypeId?: number,
    genderTypeCode?: string,
    description?: string,
    statusId?:number,
    isActive?: boolean
}