export type Ingredient = {
};

export type CommunicationTypeListResType = {
    pagination?: paginationType
    result?: CommunicationTypesType[]
};

export type CommunicationTypeResType = { 
    communicationTypeId?: number,
    communicationTypeCode?: string,
    description?: string,
    StatusId?:number,
    isActive?: boolean
};

export type CommunicationTypeReqType = { 
    communicationTypeCode?: string,
    description?: string, 
};

export interface CommunicationTypeStateProps {
    communicationTypeList: CommunicationTypeListResType | null;
    communicationType: CommunicationTypeResType | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
    isActionSuccess: object | string | null;
}

export interface DefaultRootStateProps {
    communicationType: CommunicationTypeStateProps;
}

export type listParametersType = {
    page?: number
    perPage?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

export type CommunicationTypesType = {
    communicationTypeCode?: string,
    description?: string,
    communicationTypeId?: number,
    statusId?:number,
    isActive?: boolean
}

export type CommunicationTypeType = {
    communicationTypeId?: number,
    communicationTypeCode?: string,
    description?: string,
    statusId?:number,
    isActive?: boolean
}