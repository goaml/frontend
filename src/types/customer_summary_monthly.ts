
export type customerSummaryType = {
    creditAverage?: number,
      custId?: string,
      debitAverage?: number,
      maxCreditAmt?: number,
      maxDebitAmt?: number,
      minCreditAmt?: number,
      minDebitAmt?: number,
      month?: string,
      noOfCreditTransactions?: number,
      noOfDebitTransactions?: number,
      noOfTransactions?: number,
      summaryLogId?: number,
      totNetAmt?: number,
      totalCreditAmt?: number,
      totalDebitAmt?: number,
      year?: string
      
    
}

export type customerSummaryListResType = {
    pagination?: paginationType
    result?: customerSummaryType[]
};

export type customerSummaryReqType = { 
    customerSummaryCode?: string,
    description?: string, 
};

export interface customerSummaryStateProps {
    customerSummaryList: customerSummaryListResType | null;
    customerSummary: customerSummaryType | null;
    error: object | string | null;
    success: object | string | null;
    isLoading: boolean;
    isActionSuccess: object | string | null;
}

export interface DefaultRootStateProps {
    customerSummary: customerSummaryStateProps;
}

export type listParametersType = {
    page?: number
    per_page?: number
    direction?: "asc" | "desc"
    search?: string
    sort?: string
    toMonth?: string
    toYear?: string
    custId?: string
    fromMonth?: string
    fromYear?: string

}

export type paginationType = {
    count?: number,
    from?: number,
    to?: number,
    total?: number
}

