	// third-party
    import { createSlice } from '@reduxjs/toolkit';

    // project imports
    import { axiosServices } from 'utils/axios';
    import { dispatch } from '../index';
    
    // types 
    import { DefaultRootStateProps,  GLAccountMappingReqType, listParametersType, GLAccountType } from 'types/gl-account-mapping';

    
    
    
    // ----------------------------------------------------------------------
    
    const initialState: DefaultRootStateProps['GLAccount'] = {
      error: null,
      GLsuccess: null,
      isLoading: false,
      GLAccountList: null,
      GLAccount: null,
      selectGLAccountType: null,
      isActionSuccess: null
    };
    
    const slice = createSlice({
      name: 'GLAccount',
      initialState,
      reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
          state.error = null;
          state.GLsuccess = null;
          state.isLoading = false;
        },
    
        // HAS ERROR
        hasError(state, action) {
          state.error = action.payload;
        },
    
        startLoading(state) {
          state.isLoading = true;
        },
    
        finishLoading(state) {
          state.isLoading = false;
        },
        resetIsActionState(state) {
          state.isActionSuccess = null;
      },
    
        // POST GLAccount
        addGLAccountSuccess(state, action) {
          state.GLsuccess = "GL Account created successfully."
          state.isActionSuccess = "CREATE"
        },
    
        // GET GLAccount_BY_ID
        fetchGLAccountsuccess(state, action) {
          state.GLAccount = action.payload;
          state.GLsuccess = null
        },
    
        // GET ALL GLAccount
        fetchGLAccountsSuccess(state, action) {
          state.GLAccountList = action.payload;
          state.GLsuccess = null;
          state.isActionSuccess = "LIST"
        },
    
        // UPDATE GLAccount
        updateGLAccountSuccess(state, action) {
          state.GLsuccess = "GL Account updated successfully."
          state.isActionSuccess = "UPDATE"
        },
    
        // DELETE GLAccount
        deleteGLAccountSuccess(state, action) {
          state.GLsuccess = "GL Account deleted successfully."
          state.isActionSuccess = "INACTIVE"
        },
        // GET USER_Type_LIST
        fetchGLAccountListSuccess(state, action) {
            state.selectGLAccountType= action.payload;
            state.GLsuccess = null
        },
    
      }
    });
    
    // Reducer
    export default slice.reducer;
    
    // ----------------------------------------------------------------------
    
    /**
     * TO INITIAL STATE
     * @returns 
     */
    export function toInitialState() {
      return async () => {
        dispatch(slice.actions.hasInitialState())
      }
    }

          /**
   * TO INITIAL STATE
   * @returns 
   */
  export function toResetIsActionSuccessState() {
    return async () => {
        dispatch(slice.actions.resetIsActionState())
    }
  }
    
    /**
     * POST GLAccount
     * @param newGLAccount 
     * @returns 
     */
    export function addGLAccount(newGLAccount: GLAccountMappingReqType) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.post('/transaction/gl-account-mapping/add', newGLAccount);
          dispatch(slice.actions.addGLAccountSuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
    /**
     * GET GLAccount
     * @param GLAccountId  
     * @returns 
     */
    export function fetchGLAccount(GLAccountId: number) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.get(`/transaction/gl-account-mapping/get/${GLAccountId}`);
          dispatch(slice.actions.fetchGLAccountsuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
    /**
     * GET ALL GLAccount
     * @param listParameters 
     * @returns 
     */
    export function fetchGLAccounts(listParameters: listParametersType) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.get('/transaction/gl-account-mapping/tbl', { params: listParameters });
          dispatch(slice.actions.fetchGLAccountsSuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
    /**
     * UPDATE GLAccount
     * @param updateGLAccount
     * @returns 
     */
    export function updateGLAccount(updateGLAccount: GLAccountType) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.put(`/transaction/gl-account-mapping/update/${updateGLAccount.glAccountMappingId}`, updateGLAccount);
          dispatch(slice.actions.updateGLAccountSuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
    /**
     * DELETE GLAccount
     * @param GLAccountId 
     * @returns 
     */
    export function deleteGLAccount(GLAccountId: number) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          await axiosServices.delete(`/transaction/gl-account-mapping/delete/${GLAccountId}`);
          dispatch(slice.actions.deleteGLAccountSuccess(GLAccountId));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }

    /**
     * GET ALL USER_TypeS
     * @param listParameters 
     * @returns 
     */
export function fetchGLAccountList() {
    return async () => {
      dispatch(slice.actions.startLoading());
  
      try {
        const response = await axiosServices.get('/transaction/gl-account-mapping/all');
        dispatch(slice.actions.fetchGLAccountListSuccess(response.data));
      } catch (error) {
        dispatch(slice.actions.hasError(error));
      } finally {
        dispatch(slice.actions.finishLoading());
      }
    };
  }