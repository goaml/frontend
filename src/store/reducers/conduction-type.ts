// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, ConductionTypeReqType, listParametersType, ConductionTypesType } from 'types/conduction-type';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['conductionType'] = {
  error: null,
  success: null,
  isLoading: false,
  conductionTypeList: null,
  conductionType: null,
  isActionSuccess: null
};

const slice = createSlice({
  name: 'conductionType',
  initialState,
  reducers: {
    // TO INITIAL STATE
    hasInitialState(state) {
      state.error = null;
      state.success = null;
      state.isLoading = false;
    },

    // HAS ERROR
    hasError(state, action) {
      state.error = action.payload;
    },

    startLoading(state) {
      state.isLoading = true;
    },

    finishLoading(state) {
      state.isLoading = false;
    },
    resetIsActionState(state) {
      state.isActionSuccess = null;
  },

    // POST Account_TYPE 
    addConductionTypeSuccess(state, action) {
      state.success = "Conduction Type created successfully."
      state.isActionSuccess = "CREATE"
    },

    // GET Account_TYPE_BY_ID
    fetchConductionTypeSuccess(state, action) {
      state.conductionType = action.payload;
      state.success = null
    },

    // GET ALL Account_TYPE
    fetchConductionTypesSuccess(state, action) {
      state.conductionTypeList = action.payload;
      state.success = null
      state.isActionSuccess = "LIST"
    },

    // UPDATE Account_TYPE
    updateConductionTypeSuccess(state, action) {
      state.success = "Conduction Type updated successfully."
      state.isActionSuccess = "UPDATE"
    },

    // DELETE Account_TYPE
    deleteConductionTypeSuccess(state, action) {
      state.success = "Conduction Type deleted successfully."
      state.isActionSuccess = "INACTIVE"
    },

  }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
  return async () => {
    dispatch(slice.actions.hasInitialState())
  }
}

/* TO INITIAL STATE
* @returns 
*/
export function toResetIsActionSuccessState() {
  return async () => {
      dispatch(slice.actions.resetIsActionState())
  }
}

/**
 * POST Account_TYPE
 * @param newConductionType 
 * @returns 
 */
export function addConductionType(newConductionType: ConductionTypeReqType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.post('/base-para/conduction-type', newConductionType);
      dispatch(slice.actions.addConductionTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET Account_TYPE
 * @param ConductionTypeId  
 * @returns 
 */
export function fetchConductionType(ConductionTypeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get(`/base-para/conduction-type/${ConductionTypeId}`);
      dispatch(slice.actions.fetchConductionTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET ALL Account_TYPE
 * @param listParameters 
 * @returns 
 */
export function fetchConductionTypes(listParameters: listParametersType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/base-para/conduction-type/tbl', { params: listParameters });
      dispatch(slice.actions.fetchConductionTypesSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * UPDATE Account_TYPE
 * @param updateConductionType
 * @returns 
 */
export function updateConductionType(updatedConductionType: ConductionTypesType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.put(`/base-para/conduction-type/${updatedConductionType.conductionTypeId}`, updatedConductionType);
      dispatch(slice.actions.updateConductionTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * DELETE Account_TYPE
 * @param ConductionTypeId 
 * @returns 
 */
export function deleteConductionType(ConductionTypeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      await axiosServices.delete(`/base-para/conduction-type/${ConductionTypeId}`);
      dispatch(slice.actions.deleteConductionTypeSuccess(ConductionTypeId));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}
