// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { CategoryReqType, CategorysType, DefaultRootStateProps, listParametersType} from 'types/category';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['category'] = {
    error: null,
    success: null,
    isLoading: false,
    CategoryList: null,
    Category: null,
    selectCategory:null,
    isActionSuccess:null
};

const slice = createSlice({
    name: 'category',
    initialState,
    reducers: {

        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.success = null;
            state.isLoading = false;
          },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
          },
      
          startLoading(state) {
            state.isLoading = true;
          },
      
          finishLoading(state) {
            state.isLoading = false;
          },
          resetIsActionState(state) {
            state.isActionSuccess = null;
        },
          
        // POST CATEGORY 
        addCategorySuccess(state, action) {
          state.success = "Category created successfully."
          state.isActionSuccess = "CREATE"
        },
    
        // GET CATEGORY_BY_ID
        fetchCategorySuccess(state, action) {
          state.Category = action.payload;
          state.success = null
        },
    
        // UPDATE CATEGORY
        updateCategorySuccess(state, action) {
          state.success = "Category updated successfully."
          state.isActionSuccess = "UPDATE"
        },
    
        // DELETE CATEGORY
        deleteCategorySuccess(state, action) {
          state.success = "Category deleted successfully."
          state.isActionSuccess = "INACTIVE"
        },
        // GET USER_Type_LIST
         fetchUserTypeListSuccess(state, action) {
        state.selectCategory= action.payload;
        state.success = null
        },

        // GET ALL CATEGORY
        fetchCategoryListSuccess(state, action) {
          state.CategoryList = action.payload;
          state.success = null;
          state.isActionSuccess = "LIST"
        },
    }})

    // Reducer
    export default slice.reducer;
    
    // ----------------------------------------------------------------------
    
    /**
     * TO INITIAL STATE
     * @returns 
     */
    export function toInitialState() {
      return async () => {
        dispatch(slice.actions.hasInitialState())
      }
    }

      /**
   * TO INITIAL STATE
   * @returns 
   */
  export function toResetIsActionSuccessState() {
    return async () => {
        dispatch(slice.actions.resetIsActionState())
    }
  }

    /**
     * POST CATEGORY
     * @param newCategory 
     * @returns 
     */
    export function addCategory(newCategory: CategoryReqType) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.post('/base-para/category/add', newCategory);
          dispatch(slice.actions.addCategorySuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
    /**
     * GET CATEGORY
     * @param CategoryId  
     * @returns 
     */
    export function fetchCategoryById(CategoryId: number) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.get(`/base-para/category/get/${CategoryId}`);
          dispatch(slice.actions.fetchCategorySuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }



    /**
     * UPDATE CATEGORY
     * @param updateCategory
     * @returns 
     */
    export function updateCategory(updatedCategory: CategorysType) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.put(`/base-para/category/update/${updatedCategory.categoryId}`, updatedCategory);
          dispatch(slice.actions.updateCategorySuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
    /**
     * DELETE CATEGORY
     * @param categoryId 
     * @returns 
     */
    export function deleteCategory(id: number) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          await axiosServices.delete(`/base-para/category/delete/${id}`);
          dispatch(slice.actions.deleteCategorySuccess(id));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    

/**
     * GET ALL USER_TypeS
     * @param listParameters 
     * @returns 
     */
export function fetchCategoryList() {
    return async () => {
      dispatch(slice.actions.startLoading());
  
      try {
        const response = await axiosServices.get('/base-para/category/all');
        dispatch(slice.actions.fetchUserTypeListSuccess(response.data));
      } catch (error) {
        dispatch(slice.actions.hasError(error));
      } finally {
        dispatch(slice.actions.finishLoading());
      }
    };
  }
   /**
     * GET ALL CATEGORY
     * @param listParameters 
     * @returns 
     */
   export function fetchCategory(listParameters: listParametersType) {
    return async () => {
      dispatch(slice.actions.startLoading());
  
      try {
        const response = await axiosServices.get('/base-para/category/tbl', { params: listParameters });
        dispatch(slice.actions.fetchCategoryListSuccess(response.data));
      } catch (error) {
        dispatch(slice.actions.hasError(error));
      } finally {
        dispatch(slice.actions.finishLoading());
      }
    };
  }