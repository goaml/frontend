// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types
import { DefaultRootStateProps, queryParamsProps } from 'types/menu-action';

// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['menuAction'] = {
    error: null,
    success: null,
    menuActions: null,
    menuAction: null,
    isLoading: false,
    isActionSuccess: null
};

const slice = createSlice({
    name: 'menuAction',
    initialState,
    reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.success = null;
            state.isLoading = false;
        },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
        },

        startLoading(state) {
            state.isLoading = true;
        },

        finishLoading(state) {
            state.isLoading = false;
        },
        resetIsActionState(state) {
            state.isActionSuccess = null;
        },
        // GET ALL MENU ACTIONS 
        fetchMenuActionsSuccess(state, action) {
            state.menuActions = action.payload;
            state.success = null;
            state.isActionSuccess = "LIST"
        },

    }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
    return async () => {
        dispatch(slice.actions.hasInitialState())
    }
}

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toResetIsActionSuccessState() {
    return async () => {
        dispatch(slice.actions.resetIsActionState())
    }
}
/**
 * GET ALL MENU ACTIONS
 * @param queryParams 
 * @returns 
 */
export function fetchMenuActions(queryParams: queryParamsProps) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get('/user-management/transaction/getMenuActionsList/', { params: queryParams });
            dispatch(slice.actions.fetchMenuActionsSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}