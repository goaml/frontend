	// third-party
    import { createSlice } from '@reduxjs/toolkit';

    // project imports
    import { axiosServices } from 'utils/axios';
    import { dispatch } from '../index';
    
    // types 
    import { DefaultRootStateProps, DepartmentCodeReqType, listParametersType, DepartmentCodeType } from 'types/department';
    
    
    
    // ----------------------------------------------------------------------
    
    const initialState: DefaultRootStateProps['dept'] = {
      error: null,
      deptSuccess: null,
      isLoading: false,
      selectDepartments:null,
      departmentCodeList: null,
      departmentCode: null
    };
    
    const slice = createSlice({
      name: 'dept',
      initialState,
      reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
          state.error = null;
          state.deptSuccess = null;
          state.isLoading = false;
        },
    
        // HAS ERROR
        hasError(state, action) {
          state.error = action.payload;
        },
    
        startLoading(state) {
          state.isLoading = true;
        },
    
        finishLoading(state) {
          state.isLoading = false;
        },
    
        // POST DEPT_CODE 
        addDepartmentCodedeptSuccess(state, action) {
          state.deptSuccess = "Department Code created Successfully."
        },
    
        // GET DEPT_CODE_BY_ID
        fetchDepartmentCodedeptSuccess(state, action) {
          state.departmentCode = action.payload;
          state.deptSuccess = null
        },
    
        // GET ALL DEPT_CODE
        fetchDepartmentCodesdeptSuccess(state, action) {
          state.departmentCodeList = action.payload;
          state.deptSuccess = null
        },
    
        // UPDATE DEPT_CODE
        updateDepartmentCodedeptSuccess(state, action) {
          state.deptSuccess = "Department Code updated Successfully."
        },
    
        // DELETE DEPT_CODE
        deleteDepartmentCodedeptSuccess(state, action) {
          state.deptSuccess = "Department Code deleted Successfully."
        },
        fetchDeptListSuccess(state, action) {
          state.selectDepartments = action.payload;
          state.deptSuccess = null
      },
    
      }
    });
    
    // Reducer
    export default slice.reducer;
    
    // ----------------------------------------------------------------------
    
    /**
     * TO INITIAL STATE
     * @returns 
     */
    export function toInitialState() {
      return async () => {
        dispatch(slice.actions.hasInitialState())
      }
    }
    
    /**
     * POST DEPT_CODE
     * @param newDepartmentCode 
     * @returns 
     */
    export function addDepartmentCode(newDepartmentCode: DepartmentCodeReqType) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.post('/user-management/department', newDepartmentCode);
          dispatch(slice.actions.addDepartmentCodedeptSuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
    /**
     * GET DEPT_CODE
     * @param DepartmentCodeId  
     * @returns 
     */
    export function fetchDepartmentCode(DepartmentCodeId: number) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.get(`/user-management/department/${DepartmentCodeId}`);
          dispatch(slice.actions.fetchDepartmentCodedeptSuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
    /**
     * GET ALL DEPT_CODE
     * @param listParameters 
     * @returns 
     */
    export function fetchDepartmentCodes(listParameters: listParametersType) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.get('/user-management/getDepartments', { params: listParameters });
          dispatch(slice.actions.fetchDepartmentCodesdeptSuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
    /**
     * UPDATE DEPT_CODE
     * @param updateDepartmentCode
     * @returns 
     */
    export function updateDepartmentCode(updatedDepartmentCode: DepartmentCodeType) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.put(`/user-management/department/${updatedDepartmentCode.departmentId}`, updatedDepartmentCode);
          dispatch(slice.actions.updateDepartmentCodedeptSuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
    /**
     * DELETE DEPT_CODE
     * @param DepartmentCodeId 
     * @returns 
     */
    export function deleteDepartmentCode(departmentCodeId: number) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          await axiosServices.delete(`/user-management/department/${departmentCodeId}`);
          dispatch(slice.actions.deleteDepartmentCodedeptSuccess(departmentCodeId));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }



    /**
     * GET ALL USER_TypeS
     * @param listParameters 
     * @returns 
     */
export function fetchDepartmentList() {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/user-management/department-list/fdd');
      dispatch(slice.actions.fetchDeptListSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}