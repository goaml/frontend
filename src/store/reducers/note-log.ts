// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types
import { DefaultRootStateProps, NoteLogs, queryParamsProps } from 'types/note-log';

// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['noteLog'] = {
    error: null,
    success: null,
    noteLogs: null,
    noteLog: null,
    isLoading: false
};

const slice = createSlice({
    name: 'noteLog',
    initialState,
    reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.success = null;
            state.isLoading = false;
        },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
        },

        startLoading(state) {
            state.isLoading = true;
        },

        finishLoading(state) {
            state.isLoading = false;
        },

        // GET ALL NOTE LOG
        fetchNoteLogSuccess(state, action) {
            state.noteLogs = action.payload;
            state.success = null
        },
                // POST ROLES
                addActivitySuccess(state, action) {
                    state.success = null
                },

    }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
    return async () => {
        dispatch(slice.actions.hasInitialState())
    }
}
/**
 * POST ROLE
 * @param newRole 
 * @returns 
 */
export function AddActivityLog(newLog: NoteLogs) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.post('/user-management/api-log/create', newLog);
            dispatch(slice.actions.addActivitySuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}
/**
 * GET ALL NOTE LOG
 * @param queryParams 
 * @returns 
 */
export function fetchNoteLogs(queryParams: queryParamsProps) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get('/user-management/api-log/view', { params: queryParams });
            dispatch(slice.actions.fetchNoteLogSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}
