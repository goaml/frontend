// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, IdentifierTypeReqType, listParametersType,  IdentifierTypesType } from 'types/identifier-type';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['identifierType'] = {
  error: null,
  success: null,
  isLoading: false,
  identifierTypeList: null,
  identifierType: null,
  isActionSuccess: null
};

const slice = createSlice({
  name: 'identifierType',
  initialState,
  reducers: {
    // TO INITIAL STATE
    hasInitialState(state) {
      state.error = null;
      state.success = null;
      state.isLoading = false;
    },

    // HAS ERROR
    hasError(state, action) {
      state.error = action.payload;
    },

    startLoading(state) {
      state.isLoading = true;
    },

    finishLoading(state) {
      state.isLoading = false;
    },
    resetIsActionState(state) {
      state.isActionSuccess = null;
  },

    // POST Identifier_TYPE 
    addIdentifierTypeSuccess(state, action) {
      state.success = "Identifier Type created successfully.";
      state.isActionSuccess = "CREATE"
    },

    // GET Identifier_TYPE_BY_ID
    fetchIdentifierTypeSuccess(state, action) {
      state.identifierType = action.payload;
      state.success = null
    },

    // GET ALL Identifier_TYPE
    fetchIdentifierTypesSuccess(state, action) {
      state.identifierTypeList = action.payload;
      state.success = null;
      state.isActionSuccess = "LIST"
    },

    // UPDATE Identifier_TYPE
    updateIdentifierTypeSuccess(state, action) {
      state.success = "Identifier Type updated successfully."
      state.isActionSuccess = "UPDATE"
    },

    // DELETE Identifier_TYPE
    deleteIdentifierTypeSuccess(state, action) {
      state.success = "Identifier Type deleted successfully."
      state.isActionSuccess = "INACTIVE"
    },

  }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
  return async () => {
    dispatch(slice.actions.hasInitialState())
  }
}

/* TO INITIAL STATE
* @returns 
*/
   export function toResetIsActionSuccessState() {
     return async () => {
         dispatch(slice.actions.resetIsActionState())
     }
   }
/**
 * POST Identifier_TYPE
 * @param newIdentifierType 
 * @returns 
 */
export function addIdentifierType(newIdentifierType: IdentifierTypeReqType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.post('/base-para/identifier-type', newIdentifierType);
      dispatch(slice.actions.addIdentifierTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET Identifier_TYPE
 * @param IdentifierTypeId  
 * @returns 
 */
export function fetchIdentifierType(IdentifierTypeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get(`/base-para/identifier-type/${IdentifierTypeId}`);
      dispatch(slice.actions.fetchIdentifierTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET ALL Identifier_TYPE
 * @param listParameters 
 * @returns 
 */
export function fetchIdentifierTypes(listParameters: listParametersType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/base-para/identifier-type/tbl', { params: listParameters });
      dispatch(slice.actions.fetchIdentifierTypesSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * UPDATE Identifier_TYPE
 * @param updateIdentifierType
 * @returns 
 */
export function updateIdentifierType(updatedIdentifierType: IdentifierTypesType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.put(`/base-para/identifier-type/${updatedIdentifierType.identifierTypeId}`, updatedIdentifierType);
      dispatch(slice.actions.updateIdentifierTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * DELETE Identifier_TYPE
 * @param IdentifierTypeId 
 * @returns 
 */
export function deleteIdentifierType(IdentifierTypeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      await axiosServices.delete(`/base-para/identifier-type/${IdentifierTypeId}`);
      dispatch(slice.actions.deleteIdentifierTypeSuccess(IdentifierTypeId));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}
