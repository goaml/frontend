// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, MultiPartyInvolvePartyReqType, MultiPartyInvolvePartysType, listParametersType} from 'types/multiParty-Involve-party';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['multiPartyInvolveParty'] = {
    error: null,
    MPIsuccess: null,
    isLoading: false,
    MultiPartyInvolvePartyList: null,
    MultiPartyInvolveParty: null,
    selectMultiPartyInvolveParty:null
};

const slice = createSlice({
    name: 'MultiPartyInvolveParty',
    initialState,
    reducers: {


        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.MPIsuccess = null;
            state.isLoading = false;
          },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
          },
      
          startLoading(state) {
            state.isLoading = true;
          },
      
          finishLoading(state) {
            state.isLoading = false;
          },
        // POST MULTI_INVOLVE_PARTY 
        addMultiPartyInvolvePartySuccess(state, action) {
          state.MPIsuccess = "MRI Code created successfully."
        },
    
        // GET MULTI_INVOLVE_PARTY_BY_ID
        fetchMultiPartyInvolveParty(state, action) {
          state.MultiPartyInvolveParty = action.payload;
          state.MPIsuccess = null
        },
    
        // GET ALL MULTI_INVOLVE_PARTY
        fetchMRICodesSuccess(state, action) {
          state.MultiPartyInvolveParty = action.payload;
          state.MPIsuccess = null
        },
    
        // UPDATE MULTI_INVOLVE_PARTY
        updateMultiPartyInvolveParty(state, action) {
          state.MPIsuccess = "MRI Code updated successfully."
        },
    
        // DELETE MULTI_INVOLVE_PARTY
        deleteMultiPartyInvolveParty(state, action) {
          state.MPIsuccess = "MRI Code deleted successfully."
        },
            // GET USER_Type_LIST
            fetchUserTypeListSuccess(state, action) {
                state.selectMultiPartyInvolveParty= action.payload;
                state.MPIsuccess = null
            },

                      // GET ALL MULTI_INVOLVE_PARTY
          fetchMultiPartyInvolePartysSuccess(state, action) {
            state.MultiPartyInvolvePartyList = action.payload;
            state.MPIsuccess = null
          },
    }})

    // Reducer
    export default slice.reducer;
    
    // ----------------------------------------------------------------------
    
    /**
     * TO INITIAL STATE
     * @returns 
     */
    export function toInitialState() {
      return async () => {
        dispatch(slice.actions.hasInitialState())
      }
    }



/**
     * GET ALL MULTI_INVOLVE_PARTY
     * @param listParameters 
     * @returns 
     */
export function fetchMultiPartyInvolvePartyList() {
    return async () => {
      dispatch(slice.actions.startLoading());
  
      try {
        const response = await axiosServices.get('/base-para/multi-party-involve-party/all');
        dispatch(slice.actions.fetchUserTypeListSuccess(response.data));
      } catch (error) {
        dispatch(slice.actions.hasError(error));
      } finally {
        dispatch(slice.actions.finishLoading());
      }
    };
  }


          /**
     * GET ALL MULTI_INVOLVE_PARTY
     * @param listParameters 
     * @returns 
     */
          export function fetchMultiPartyInvolePartys(listParameters: listParametersType) {
            return async () => {
              dispatch(slice.actions.startLoading());
          
              try {
                const response = await axiosServices.get('/base-para/multi-party-involve-party/tbl', { params: listParameters });
                dispatch(slice.actions.fetchMultiPartyInvolePartysSuccess(response.data));
              } catch (error) {
                dispatch(slice.actions.hasError(error));
              } finally {
                dispatch(slice.actions.finishLoading());
              }
            };
          }


              /**
     * GET MULTI_INVOLVE_PARTY
     * @param MultiPartyInvolvePartyId  
     * @returns 
     */
              export function fetchMultiPartyInvolveParty(MultiPartyInvolvePartyId: number) {
                return async () => {
                  dispatch(slice.actions.startLoading());
              
                  try {
                    const response = await axiosServices.get(`/base-para//multi-party-involve-party/get/${MultiPartyInvolvePartyId}`);
                    dispatch(slice.actions.fetchMRICodesSuccess(response.data));
                  } catch (error) {
                    dispatch(slice.actions.hasError(error));
                  } finally {
                    dispatch(slice.actions.finishLoading());
                  }
                };
              }
                  /**
               * POST MULTI_INVOLVE_PARTY
               * @param newMultiPartyInvolveParty 
               * @returns 
               */
                  export function addMultiPartyInvolveParty(newMultiPartyInvolveParty: MultiPartyInvolvePartyReqType) {
                    return async () => {
                      dispatch(slice.actions.startLoading());
                  
                      try {
                        const response = await axiosServices.post('/base-para//multi-party-involve-party/add', newMultiPartyInvolveParty);
                        dispatch(slice.actions.addMultiPartyInvolvePartySuccess(response.data));
                      } catch (error) {
                        dispatch(slice.actions.hasError(error));
                      } finally {
                        dispatch(slice.actions.finishLoading());
                      }
                    };
                  }
                   
              /**
               * UPDATE MULTI_INVOLVE_PARTY
               * @param updateMultiPartyInvolveParty
               * @returns 
               */
              export function updateMultiPartyInvolveParty(updatedMultiPartyInvolveParty: MultiPartyInvolvePartysType) {
                return async () => {
                  dispatch(slice.actions.startLoading());
              
                  try {
                    const response = await axiosServices.put(`/base-para//multi-party-involve-party/update/${updatedMultiPartyInvolveParty.multiPartyInvolvePartyId}`, updatedMultiPartyInvolveParty);
                    dispatch(slice.actions.updateMultiPartyInvolveParty(response.data));
                  } catch (error) {
                    dispatch(slice.actions.hasError(error));
                  } finally {
                    dispatch(slice.actions.finishLoading());
                  }
                };
              }
              
              /**
               * DELETE MULTI_INVOLVE_PARTY
               * @param MultiPartyInvolvePartyId 
               * @returns 
               */
              export function deleteMultiPartyInvolveParty(MultiPartyInvolvePartyId: number) {
                return async () => {
                  dispatch(slice.actions.startLoading());
              
                  try {
                    await axiosServices.delete(`/base-para//multi-party-involve-party/delete/${MultiPartyInvolvePartyId}`);
                    dispatch(slice.actions.deleteMultiPartyInvolveParty(MultiPartyInvolvePartyId));
                  } catch (error) {
                    dispatch(slice.actions.hasError(error));
                  } finally {
                    dispatch(slice.actions.finishLoading());
                  }
                };
              }