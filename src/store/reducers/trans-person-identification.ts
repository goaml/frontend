// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types
import { DefaultRootStateProps,  queryParamsProps } from 'types/trans-person-identification';

// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['transPersonIdentification'] = {
    error: null,
    success: null,
    transPersonIdentificationList: null,
    isLoading: false,
};

const slice = createSlice({
    name: 'transPersonIdentification',
    initialState,
    reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.success = null;
            state.isLoading = false;
        },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
        },

        startLoading(state) {
            state.isLoading = true;
        },

        finishLoading(state) {
            state.isLoading = false;
        },

    
        // GET ALL transPersonIdentification 
        fetchTransPersonIdentificationsSuccess(state, action) {
            state.transPersonIdentificationList = action.payload;
            state.success = null
        },

    }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
    return async () => {
        dispatch(slice.actions.hasInitialState())
    }
}



/**
 * GET ALL transPersonIdentification
 * @param queryParams 
 * @returns 
 */
export function fetchTransPersonIdentificationsTable(queryParams: queryParamsProps) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get('/transaction/person-identification/tbl', { params: queryParams });
            dispatch(slice.actions.fetchTransPersonIdentificationsSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}


