// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types
import { DefaultRootStateProps,  queryParamsProps } from 'types/trans-from-myclient';

// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['transFromMyClient'] = {
    error: null,
    success: null,
    transFromMyClientList: null,
    isLoading: false,
};

const slice = createSlice({
    name: 'transFromMyClient',
    initialState,
    reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.success = null;
            state.isLoading = false;
        },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
        },

        startLoading(state) {
            state.isLoading = true;
        },

        finishLoading(state) {
            state.isLoading = false;
        },

    
        // GET ALL transFromMyClient 
        fetchTransFromMyClientsSuccess(state, action) {
            state.transFromMyClientList = action.payload;
            state.success = null
        },

    }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
    return async () => {
        dispatch(slice.actions.hasInitialState())
    }
}



/**
 * GET ALL transFromMyClient
 * @param queryParams 
 * @returns 
 */
export function fetchTransFromMyClientsTable(queryParams: queryParamsProps) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get('/transaction/from-my-client/tbl', { params: queryParams });
            dispatch(slice.actions.fetchTransFromMyClientsSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}


