// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, ContactTypeReqType, listParametersType, ContactTypesType } from 'types/contact-type';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['contactType'] = {
  error: null,
  success: null,
  isLoading: false,
  contactTypeList: null,
  contactType: null,
  isActionSuccess: null
};

const slice = createSlice({
  name: 'contactType',
  initialState,
  reducers: {
    // TO INITIAL STATE
    hasInitialState(state) {
      state.error = null;
      state.success = null;
      state.isLoading = false;
    },

    // HAS ERROR
    hasError(state, action) {
      state.error = action.payload;
    },

    startLoading(state) {
      state.isLoading = true;
    },

    finishLoading(state) {
      state.isLoading = false;
    },

    resetIsActionState(state) {
      state.isActionSuccess = null;
  },

    // POST Account_TYPE 
    addContactTypeSuccess(state, action) {
      state.success = "Contact Type created successfully."
      state.isActionSuccess = "CREATE"
    },

    // GET Account_TYPE_BY_ID
    fetchContactTypeSuccess(state, action) {
      state.contactType = action.payload;
      state.success = null
    },

    // GET ALL Account_TYPE
    fetchContactTypesSuccess(state, action) {
      state.contactTypeList = action.payload;
      state.success = null
      state.isActionSuccess = "LIST"
    },

    // UPDATE Account_TYPE
    updateContactTypeSuccess(state, action) {
      state.success = "Contact Type updated successfully."
      state.isActionSuccess = "UPDATE"
    },

    // DELETE Account_TYPE
    deleteContactTypeSuccess(state, action) {
      state.success = "Contact Type deleted successfully."
      state.isActionSuccess = "INACTIVE"
    },

  }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
  return async () => {
    dispatch(slice.actions.hasInitialState())
  }
}


/* TO INITIAL STATE
* @returns 
*/
export function toResetIsActionSuccessState() {
  return async () => {
      dispatch(slice.actions.resetIsActionState())
  }
}

/**
 * POST Account_TYPE
 * @param newContactType 
 * @returns 
 */
export function addContactType(newContactType: ContactTypeReqType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.post('/base-para/contact-type', newContactType);
      dispatch(slice.actions.addContactTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET Account_TYPE
 * @param ContactTypeId  
 * @returns 
 */
export function fetchContactType(ContactTypeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get(`/base-para/contact-type/${ContactTypeId}`);
      dispatch(slice.actions.fetchContactTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET ALL Account_TYPE
 * @param listParameters 
 * @returns 
 */
export function fetchContactTypes(listParameters: listParametersType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/base-para/contact-type/tbl', { params: listParameters });
      dispatch(slice.actions.fetchContactTypesSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * UPDATE Account_TYPE
 * @param updateContactType
 * @returns 
 */
export function updateContactType(updatedContactType: ContactTypesType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.put(`/base-para/contact-type/${updatedContactType.contactTypeId}`, updatedContactType);
      dispatch(slice.actions.updateContactTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * DELETE Account_TYPE
 * @param ContactTypeId 
 * @returns 
 */
export function deleteContactType(ContactTypeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      await axiosServices.delete(`/base-para/contact-type/${ContactTypeId}`);
      dispatch(slice.actions.deleteContactTypeSuccess(ContactTypeId));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}
