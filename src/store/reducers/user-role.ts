	// third-party
    import { createSlice } from '@reduxjs/toolkit';

    // project imports
    import { axiosServices } from 'utils/axios';
    import { dispatch } from '../index';
    
    // types 
    import { DefaultRootStateProps, UserRoleReqType, listParametersType, UserRolesType } from 'types/user-role';
    
    
    
    // ----------------------------------------------------------------------
    
    const initialState: DefaultRootStateProps['userRole'] = {
      error: null,
      Rolesuccess: null,
      isLoading: false,
      userRoleList: null,
      userRole: null,
      selectUserRoles:null
    };
    
    const slice = createSlice({
      name: 'userRole',
      initialState,
      reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
          state.error = null;
          state.Rolesuccess = null;
          state.isLoading = false;
        },
    
        // HAS ERROR
        hasError(state, action) {
          state.error = action.payload;
        },
    
        startLoading(state) {
          state.isLoading = true;
        },
    
        finishLoading(state) {
          state.isLoading = false;
        },
    
        // POST USER_ROLE 
        addUserRoleRolesuccess(state, action) {
          state.Rolesuccess = "User Role created Rolesuccessfully."
        },
    
        // GET USER_ROLE_BY_ID
        fetchUserRoleRolesuccess(state, action) {
          state.userRole = action.payload;
          state.Rolesuccess = null
        },
        // GET USER_ROLE_LIST
        fetchUserRoleListRolesuccess(state, action) {
          state.selectUserRoles = action.payload;
          state.Rolesuccess = null
        },


    
        // GET ALL USER_ROLE
        fetchUserRolesRolesuccess(state, action) {
          state.userRoleList = action.payload;
          state.Rolesuccess = null
        },
    
        // UPDATE USER_ROLE
        updateUserRoleRolesuccess(state, action) {
          state.Rolesuccess = "User Role updated Rolesuccessfully."
        },
    
        // DELETE USER_ROLE
        deleteUserRoleRolesuccess(state, action) {
          state.Rolesuccess = "User Role deleted Rolesuccessfully."
        },
    
      }
    });
    
    // Reducer
    export default slice.reducer;
    
    // ----------------------------------------------------------------------
    
    /**
     * TO INITIAL STATE
     * @returns 
     */
    export function toInitialState() {
      return async () => {
        dispatch(slice.actions.hasInitialState())
      }
    }
    
    /**
     * POST USER_ROLE
     * @param newUserRole 
     * @returns 
     */
    export function addUserRole(newUserRole: UserRoleReqType) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.post('/user-management/createUserRole', newUserRole);
          dispatch(slice.actions.addUserRoleRolesuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
    /**
     * GET USER_ROLE
     * @param UserRoleId  
     * @returns 
     */
    export function fetchUserRole(UserRoleId: number) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.get(`/reference/cluster/${UserRoleId}`);
          dispatch(slice.actions.fetchUserRoleRolesuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
    /**
     * GET ALL USER_ROLE
     * @param listParameters 
     * @returns 
     */
    export function fetchUserRoles(listParameters: listParametersType) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.get('/user-management/roleView', { params: listParameters });
          dispatch(slice.actions.fetchUserRolesRolesuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
    /**
     * UPDATE USER_ROLE
     * @param updateUserRole
     * @returns 
     */
    export function updateUserRole(updatedUserRole: UserRolesType) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.put(`/user-management/updateUserRole`, updatedUserRole);
          dispatch(slice.actions.updateUserRoleRolesuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
    /**
     * DELETE USER_ROLE
     * @param UserRoleId 
     * @returns 
     */
    export function deleteUserRole(UserRoleId: number) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          await axiosServices.delete(`/user-management/deleteUserRole/${UserRoleId}`);
          dispatch(slice.actions.deleteUserRoleRolesuccess(UserRoleId));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }

    /**
     * GET ALL USER_ROLES
     * @param listParameters 
     * @returns 
     */
    export function fetchUserRoleList() {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.get('/user-management/userRole/fdd');
          dispatch(slice.actions.fetchUserRoleListRolesuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }