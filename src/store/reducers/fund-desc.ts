// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, FundTypeReqType, FundTypesType, listParametersType} from 'types/fund-desc';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['fundTypeDescription'] = {
    error: null,
    FTDsuccess: null,
    isLoading: false,
    FundTypeList: null,
    FundType: null,
    selectFundTypeDesc:null
};

const slice = createSlice({
    name: 'fundTypeDescription',
    initialState,
    reducers: {


        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.FTDsuccess = null;
            state.isLoading = false;
          },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
          },
      
          startLoading(state) {
            state.isLoading = true;
          },
      
          finishLoading(state) {
            state.isLoading = false;
          },


         // POST FUND_TYPE 
        addFundTypeSuccess(state, action) {
          state.FTDsuccess = "Fund Type Description created successfully."
        },
    
        // GET FUND_TYPE_BY_ID
        fetchFundType(state, action) {
          state.FundType = action.payload;
          state.FTDsuccess = null
        },
    
        // GET ALL FUND_TYPE
        fetchMPRCodesSuccess(state, action) {
          state.FundType = action.payload;
          state.FTDsuccess = null
        },
    
        // UPDATE FUND_TYPE
        updateFundType(state, action) {
          state.FTDsuccess = "Fund Type Description updated successfully."
        },
    
        // DELETE FUND_TYPE
        deleteFundType(state, action) {
          state.FTDsuccess = "Fund Type Description deleted successfully."
        },

            // GET USER_Type_LIST
            fetchUserTypeListSuccess(state, action) {
                state.selectFundTypeDesc= action.payload;
                state.FTDsuccess = null
            },
                        // GET ALL rpt_CODE
        fetchFundTypesSuccess(state, action) {
          state.FundTypeList = action.payload;
          state.FTDsuccess = null
        },
    }})

    // Reducer
    export default slice.reducer;
    
    // ----------------------------------------------------------------------
    
    /**
     * TO INITIAL STATE
     * @returns 
     */
    export function toInitialState() {
      return async () => {
        dispatch(slice.actions.hasInitialState())
      }
    }



/**
     * GET ALL USER_TypeS
     * @param listParameters 
     * @returns 
     */
export function fetchfundTypeDescriptionList() {
    return async () => {
      dispatch(slice.actions.startLoading());
  
      try {
        const response = await axiosServices.get('/base-para/fund-type-description/all');
        dispatch(slice.actions.fetchUserTypeListSuccess(response.data));
      } catch (error) {
        dispatch(slice.actions.hasError(error));
      } finally {
        dispatch(slice.actions.finishLoading());
      }
    };
  }

      /**
     * GET ALL RPT_CODE
     * @param listParameters 
     * @returns 
     */
      export function fetchFundTypeDescs(listParameters: listParametersType) {
        return async () => {
          dispatch(slice.actions.startLoading());
      
          try {
            const response = await axiosServices.get('/base-para/fund-type-description/tbl', { params: listParameters });
            dispatch(slice.actions.fetchFundTypesSuccess(response.data));
          } catch (error) {
            dispatch(slice.actions.hasError(error));
          } finally {
            dispatch(slice.actions.finishLoading());
          }
        };
      }



              /**
     * GET FUND_TYPE
     * @param FundTypeId  
     * @returns 
     */
              export function fetchFundType(FundTypeId: number) {
                return async () => {
                  dispatch(slice.actions.startLoading());
              
                  try {
                    const response = await axiosServices.get(`/base-para/fund-type-description/get/${FundTypeId}`);
                    dispatch(slice.actions.fetchMPRCodesSuccess(response.data));
                  } catch (error) {
                    dispatch(slice.actions.hasError(error));
                  } finally {
                    dispatch(slice.actions.finishLoading());
                  }
                };
              }
                  /**
               * POST FUND_TYPE
               * @param newFundType 
               * @returns 
               */
                  export function addFundType(newFundType: FundTypeReqType) {
                    return async () => {
                      dispatch(slice.actions.startLoading());
                  
                      try {
                        const response = await axiosServices.post('/base-para/fund-type-description/add', newFundType);
                        dispatch(slice.actions.addFundTypeSuccess(response.data));
                      } catch (error) {
                        dispatch(slice.actions.hasError(error));
                      } finally {
                        dispatch(slice.actions.finishLoading());
                      }
                    };
                  }
                   
              /**
               * UPDATE FUND_TYPE
               * @param updateFundType
               * @returns 
               */
              export function updateFundType(updatedFundType: FundTypesType) {
                return async () => {
                  dispatch(slice.actions.startLoading());
              
                  try {
                    const response = await axiosServices.put(`/base-para/fund-type-description/update/${updatedFundType.fundTypeDescriptionId}`, updatedFundType);
                    dispatch(slice.actions.updateFundType(response.data));
                  } catch (error) {
                    dispatch(slice.actions.hasError(error));
                  } finally {
                    dispatch(slice.actions.finishLoading());
                  }
                };
              }
              
              /**
               * DELETE FUND_TYPE
               * @param FundTypeId 
               * @returns 
               */
              export function deleteFundType(FundTypeId: number) {
                return async () => {
                  dispatch(slice.actions.startLoading());
              
                  try {
                    await axiosServices.delete(`/base-para/fund-type-description/delete/${FundTypeId}`);
                    dispatch(slice.actions.deleteFundType(FundTypeId));
                  } catch (error) {
                    dispatch(slice.actions.hasError(error));
                  } finally {
                    dispatch(slice.actions.finishLoading());
                  }
                };
              }