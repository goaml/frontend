// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, MultiPartyRoleReqType, MultiPartyRolesType, listParametersType} from 'types/multiParty-Role';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['multiPartyRole'] = {
    error: null,
    MPRsuccess: null,
    isLoading: false,
    MultiPartyRoleList: null,
    MultiPartyRole: null,
    selectMultiPartyRole:null
};

const slice = createSlice({
    name: 'MultiPartyRole',
    initialState,
    reducers: {


        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.MPRsuccess = null;
            state.isLoading = false;
          },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
          },
      
          startLoading(state) {
            state.isLoading = true;
          },
      
          finishLoading(state) {
            state.isLoading = false;
          },

        // POST rpt_CODE 
        addMultiPartyRoleSuccess(state, action) {
          state.MPRsuccess = "MPR Code created successfully."
        },
    
        // GET MPR_CODE_BY_ID
        fetchMultiPartyRole(state, action) {
          state.MultiPartyRole = action.payload;
          state.MPRsuccess = null
        },
    
        // GET ALL MPR_CODE
        fetchMPRCodesSuccess(state, action) {
          state.MultiPartyRole = action.payload;
          state.MPRsuccess = null
        },
    
        // UPDATE MPR_CODE
        updateMultiPartyRole(state, action) {
          state.MPRsuccess = "MPR Code updated successfully."
        },
    
        // DELETE MPR_CODE
        deleteMultiPartyRole(state, action) {
          state.MPRsuccess = "MPR Code deleted successfully."
        },
            // GET USER_Type_LIST
            fetchUserTypeListSuccess(state, action) {
                state.selectMultiPartyRole= action.payload;
                state.MPRsuccess = null
            },
                      // GET ALL rpt_CODE
          fetchMultiPartyRolesSuccess(state, action) {
            state.MultiPartyRoleList = action.payload;
            state.MPRsuccess = null
          },
    }})

    // Reducer
    export default slice.reducer;
    
    // ----------------------------------------------------------------------
    
    /**
     * TO INITIAL STATE
     * @returns 
     */
    export function toInitialState() {
      return async () => {
        dispatch(slice.actions.hasInitialState())
      }
    }



/**
     * GET ALL USER_TypeS
     * @param listParameters 
     * @returns 
     */
export function fetchMultiPartyRoleList() {
    return async () => {
      dispatch(slice.actions.startLoading());
  
      try {
        const response = await axiosServices.get('/base-para/multi-party-role/all');
        dispatch(slice.actions.fetchUserTypeListSuccess(response.data));
      } catch (error) {
        dispatch(slice.actions.hasError(error));
      } finally {
        dispatch(slice.actions.finishLoading());
      }
    };
  }


          /**
     * GET ALL RPT_CODE
     * @param listParameters 
     * @returns 
     */
          export function fetchMultiPartyRoles(listParameters: listParametersType) {
            return async () => {
              dispatch(slice.actions.startLoading());
          
              try {
                const response = await axiosServices.get('/base-para/multi-party-role/tbl', { params: listParameters });
                dispatch(slice.actions.fetchMultiPartyRolesSuccess(response.data));
              } catch (error) {
                dispatch(slice.actions.hasError(error));
              } finally {
                dispatch(slice.actions.finishLoading());
              }
            };
          }

           /**
     * GET RPT_CODE
     * @param MultiPartyRoleId  
     * @returns 
     */
    export function fetchMultiPartyRole(MultiPartyRoleId: number) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.get(`/base-para/multi-party-role/get/${MultiPartyRoleId}`);
          dispatch(slice.actions.fetchMPRCodesSuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
        /**
     * POST RPT_CODE
     * @param newMultiPartyRole 
     * @returns 
     */
        export function addMultiPartyRole(newMultiPartyRole: MultiPartyRoleReqType) {
          return async () => {
            dispatch(slice.actions.startLoading());
        
            try {
              const response = await axiosServices.post('/base-para/multi-party-role/add', newMultiPartyRole);
              dispatch(slice.actions.addMultiPartyRoleSuccess(response.data));
            } catch (error) {
              dispatch(slice.actions.hasError(error));
            } finally {
              dispatch(slice.actions.finishLoading());
            }
          };
        }
         
    /**
     * UPDATE RPT_CODE
     * @param updateMultiPartyRole
     * @returns 
     */
    export function updateMultiPartyRole(updatedMultiPartyRole: MultiPartyRolesType) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.put(`/base-para/multi-party-role/update/${updatedMultiPartyRole.multiPartyRoleId}`, updatedMultiPartyRole);
          dispatch(slice.actions.updateMultiPartyRole(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
    /**
     * DELETE RPT_CODE
     * @param MultiPartyRoleId 
     * @returns 
     */
    export function deleteMultiPartyRole(MultiPartyRoleId: number) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          await axiosServices.delete(`/base-para/multi-party-role/delete/${MultiPartyRoleId}`);
          dispatch(slice.actions.deleteMultiPartyRole(MultiPartyRoleId));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }