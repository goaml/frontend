// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, listParametersType, ReportType } from 'types/report-types';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['reportType'] = {
    error: null,
    success: null,
    isLoading: false,
    reportTypeList: null,
    selectReportType: null,
    isActionSuccess: null

};

const slice = createSlice({
    name: 'report',
    initialState,
    reducers: {


        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.success = null;
            state.isLoading = false;
        },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
        },

        startLoading(state) {
            state.isLoading = true;
        },

        finishLoading(state) {
            state.isLoading = false;
        },
        resetIsActionState(state) {
            state.isActionSuccess = null;
        },
        // GET Report Type
        fetchReportTypeSelectSuccess(state, action) {
            state.selectReportType = action.payload;
            state.success = null

        },

        // POST REPORT_TYPE 
        addReportSuccess(state, action) {
            state.success = "Report Type created successfully."
            state.isActionSuccess = "CREATE"
        },

        // UPDATE REPORT_TYPE
        updateReport(state, action) {
            state.success = "Report Type updated successfully."
            state.isActionSuccess = "UPDATE"
        },

        // DELETE REPORT_TYPE
        deleteReport(state, action) {
            state.success = "Report Type deleted successfully."
            state.isActionSuccess = "INACTIVE"
        },

        // GET ALL REPORT_TYPE
        fetchReportsSuccess(state, action) {
            state.reportTypeList = action.payload;
            state.success = null;
            state.isActionSuccess = "LIST"
        }
    }
})

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
    return async () => {
        dispatch(slice.actions.hasInitialState())
    }
}
/**
 * TO INITIAL STATE
 * @returns 
 */
export function toResetIsActionSuccessState() {
    return async () => {
        dispatch(slice.actions.resetIsActionState())
    }
}


/**
     * GET ALL SELECT REPORT_TYPES
     * @param listParameters 
     * @returns 
     */
export function fetchReportTypeSelect() {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get('/base-para/report-type/all');
            dispatch(slice.actions.fetchReportTypeSelectSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}


/**
* GET ALL REPORT_TYPE
* @param listParameters 
* @returns 
*/
export function fetchReports(listParameters: listParametersType) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get('/base-para/report-type/tbl', { params: listParameters });
            dispatch(slice.actions.fetchReportsSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}


/**
* POST REPORT_TYPE
* @param newReport 
* @returns 
*/
export function addReport(newReport: ReportType) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.post('/base-para/report-type/add', newReport);
            dispatch(slice.actions.addReportSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}

/**
 * UPDATE REPORT_TYPE
 * @param updateReport
 * @returns 
 */
export function updateReport(updatedReport: ReportType) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.put(`/base-para/report-type/update/${updatedReport.reportTypeId}`, updatedReport);
            dispatch(slice.actions.updateReport(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}

/**
 * DELETE REPORT_TYPE
 * @param ReportId 
 * @returns 
 */
export function deleteReport(ReportId: number) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            await axiosServices.delete(`/base-para/report-type/delete/${ReportId}`);
            dispatch(slice.actions.deleteReport(ReportId));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}

