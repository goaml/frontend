// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types
import { DefaultRootStateProps,  queryParamsProps } from 'types/foreign-currency';

// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['foreignCurrency'] = {
    error: null,
    success: null,
    foreignCurrencyList: null,
    isLoading: false,
};

const slice = createSlice({
    name: 'foreignCurrency',
    initialState,
    reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.success = null;
            state.isLoading = false;
        },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
        },

        startLoading(state) {
            state.isLoading = true;
        },

        finishLoading(state) {
            state.isLoading = false;
        },

    
        // GET ALL FOREIGN-CURRENCY 
        fetchForeignCurrencySuccess(state, action) {
            state.foreignCurrencyList = action.payload;
            state.success = null
        },

    }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
    return async () => {
        dispatch(slice.actions.hasInitialState())
    }
}



/**
 * GET ALL FOREIGN-CURRENCY
 * @param queryParams 
 * @returns 
 */
export function fetchForeignCurrencyTable(queryParams: queryParamsProps) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get('/transaction/foreign-currency/tbl', { params: queryParams });
            dispatch(slice.actions.fetchForeignCurrencySuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}


