// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, listParametersType } from 'types/transactioncode_summary_monthly';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['transactioncode_summary_monthly'] = {
  error: null,
  success: null,
  isLoading: false,
  transactioncode_summary_monthlyList: null,
  transactioncode_summary_monthly: null,
  isActionSuccess: null
};

const slice = createSlice({
  name: 'transactioncode_summary_monthly',
  initialState,
  reducers: {
    // TO INITIAL STATE
    hasInitialState(state) {
      state.error = null;
      state.success = null;
      state.isLoading = false;
    },

    // HAS ERROR
    hasError(state, action) {
      state.error = action.payload;
    },

    startLoading(state) {
      state.isLoading = true;
    },

    finishLoading(state) {
      state.isLoading = false;
    },
    resetIsActionState(state) {
      state.isActionSuccess = null;
  },


    // GET ALL transactioncode_summary_monthly
    fetchCitysSuccess(state, action) {
      state.transactioncode_summary_monthlyList = action.payload;
      state.success = null
      state.isActionSuccess = "LIST"
    },

  }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
  return async () => {
    dispatch(slice.actions.hasInitialState())
  }
}

/* TO INITIAL STATE
* @returns 
*/
export function toResetIsActionSuccessState() {
  return async () => {
      dispatch(slice.actions.resetIsActionState())
  }
}


/**
 * GET ALL transactioncode_summary_monthly
 * @param listParameters 
 * @returns 
 */
export function fetchtransactioncode_summary_monthlyList(listParameters: listParametersType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/base-summery/trx-code-summary-monthly/tbl', { params: listParameters });
      dispatch(slice.actions.fetchCitysSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}
