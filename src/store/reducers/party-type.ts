// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, PartyTypeReqType, listParametersType, PartyTypesType } from 'types/party-type';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['partyType'] = {
  error: null,
  success: null,
  isLoading: false,
  partyTypeList: null,
  partyType: null,
  isActionSuccess: null
};

const slice = createSlice({
  name: 'partyType',
  initialState,
  reducers: {
    // TO INITIAL STATE
    hasInitialState(state) {
      state.error = null;
      state.success = null;
      state.isLoading = false;
    },

    // HAS ERROR
    hasError(state, action) {
      state.error = action.payload;
    },

    startLoading(state) {
      state.isLoading = true;
    },

    finishLoading(state) {
      state.isLoading = false;
    },
    resetIsActionState(state) {
      state.isActionSuccess = null;
  },

    // POST Party_TYPE 
    addPartyTypeSuccess(state, action) {
      state.success = "Party Type created successfully."
      state.isActionSuccess = "CREATE"
    },

    // GET Party_TYPE_BY_ID
    fetchPartyTypeSuccess(state, action) {
      state.partyType = action.payload;
      state.success = null
    },

    // GET ALL Party_TYPE
    fetchPartyTypesSuccess(state, action) {
      state.partyTypeList = action.payload;
      state.success = null
      state.isActionSuccess = "LIST"
    },

    // UPDATE Party_TYPE
    updatePartyTypeSuccess(state, action) {
      state.success = "Party Type updated successfully."
      state.isActionSuccess = "UPDATE"
    },

    // DELETE Party_TYPE
    deletePartyTypeSuccess(state, action) {
      state.success = "Party Type deleted successfully."
      state.isActionSuccess = "INACTIVE"
    },

  }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
  return async () => {
    dispatch(slice.actions.hasInitialState())
  }
}

/* TO INITIAL STATE
* @returns 
*/
export function toResetIsActionSuccessState() {
  return async () => {
      dispatch(slice.actions.resetIsActionState())
  }
}

/**
 * POST Party_TYPE
 * @param newPartyType 
 * @returns 
 */
export function addPartyType(newPartyType: PartyTypeReqType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.post('/base-para/party-type', newPartyType);
      dispatch(slice.actions.addPartyTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET Party_TYPE
 * @param PartyTypeId  
 * @returns 
 */
export function fetchPartyType(PartyTypeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get(`/base-para/party-type/${PartyTypeId}`);
      dispatch(slice.actions.fetchPartyTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET ALL Party_TYPE
 * @param listParameters 
 * @returns 
 */
export function fetchPartyTypes(listParameters: listParametersType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/base-para/party-type/tbl', { params: listParameters });
      dispatch(slice.actions.fetchPartyTypesSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * UPDATE Party_TYPE
 * @param updatePartyType
 * @returns 
 */
export function updatePartyType(updatedPartyType: PartyTypesType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.put(`/base-para/party-type/${updatedPartyType.partyTypeId}`, updatedPartyType);
      dispatch(slice.actions.updatePartyTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * DELETE Party_TYPE
 * @param PartyTypeId 
 * @returns 
 */
export function deletePartyType(PartyTypeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      await axiosServices.delete(`/base-para/party-type/${PartyTypeId}`);
      dispatch(slice.actions.deletePartyTypeSuccess(PartyTypeId));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}
