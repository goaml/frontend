// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, StatusReqType, listParametersType, StatussType } from 'types/status-type';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['status'] = {
  error: null,
  success: null,
  isLoading: false,
  statusList: null,
  status: null,
  isActionSuccess: null
};

const slice = createSlice({
  name: 'status',
  initialState,
  reducers: {
    // TO INITIAL STATE
    hasInitialState(state) {
      state.error = null;
      state.success = null;
      state.isLoading = false;
    },

    // HAS ERROR
    hasError(state, action) {
      state.error = action.payload;
    },

    startLoading(state) {
      state.isLoading = true;
    },

    finishLoading(state) {
      state.isLoading = false;
    },
    resetIsActionState(state) {
      state.isActionSuccess = null;
  },

    // POST status_TYPE 
    addStatusSuccess(state, action) {
      state.success = "Status Type created successfully."
      state.isActionSuccess = "CREATE"
    },

    // GET status_TYPE_BY_ID
    fetchStatusSuccess(state, action) {
      state.status = action.payload;
      state.success = null
    },

    // GET ALL status_TYPE
    fetchStatussSuccess(state, action) {
      state.statusList = action.payload;
      state.success = null
      state.isActionSuccess = "LIST"
    },

    // UPDATE status_TYPE
    updateStatusSuccess(state, action) {
      state.success = "Status Type updated successfully."
      state.isActionSuccess = "UPDATE"
    },

    // DELETE status_TYPE
    deleteStatusSuccess(state, action) {
      state.success = "Status Type deleted successfully."
      state.isActionSuccess = "INACTIVE"
    },

  }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
  return async () => {
    dispatch(slice.actions.hasInitialState())
  }
}

/* TO INITIAL STATE
* @returns 
*/
export function toResetIsActionSuccessState() {
  return async () => {
      dispatch(slice.actions.resetIsActionState())
  }
}

/**
 * POST status_TYPE
 * @param newStatus 
 * @returns 
 */
export function addStatus(newStatus: StatusReqType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.post('/base-para/status', newStatus);
      dispatch(slice.actions.addStatusSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET status_TYPE
 * @param StatusId  
 * @returns 
 */
export function fetchStatus(StatusId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get(`/base-para/status/${StatusId}`);
      dispatch(slice.actions.fetchStatusSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET ALL status_TYPE
 * @param listParameters 
 * @returns 
 */
export function fetchStatuss(listParameters: listParametersType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/base-para/status/tbl', { params: listParameters });
      dispatch(slice.actions.fetchStatussSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * UPDATE status_TYPE
 * @param updateStatus
 * @returns 
 */
export function updateStatus(updatedStatus: StatussType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.put(`/base-para/status/${updatedStatus.statusId}`, updatedStatus);
      dispatch(slice.actions.updateStatusSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * DELETE status_TYPE
 * @param StatusId 
 * @returns 
 */
export function deleteStatus(StatusId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      await axiosServices.delete(`/base-para/status/${StatusId}`);
      dispatch(slice.actions.deleteStatusSuccess(StatusId));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}
