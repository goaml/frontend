// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, SubmissionTypeReqType, listParametersType, SubmissionTypesType } from 'types/submission-type';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['submissionType'] = {
  error: null,
  success: null,
  isLoading: false,
  submissionTypeList: null,
  submissionType: null,
  isActionSuccess: null
};

const slice = createSlice({
  name: 'submissionType',
  initialState,
  reducers: {
    // TO INITIAL STATE
    hasInitialState(state) {
      state.error = null;
      state.success = null;
      state.isLoading = false;
    },

    // HAS ERROR
    hasError(state, action) {
      state.error = action.payload;
    },

    startLoading(state) {
      state.isLoading = true;
    },

    finishLoading(state) {
      state.isLoading = false;
    },
    resetIsActionState(state) {
      state.isActionSuccess = null;
  },

    // POST Submission_TYPE 
    addSubmissionTypeSuccess(state, action) {
      state.success = "Submission Type created successfully."
      state.isActionSuccess = "CREATE"
    },

    // GET Submission_TYPE_BY_ID
    fetchSubmissionTypeSuccess(state, action) {
      state.submissionType = action.payload;
      state.success = null
    },

    // GET ALL Submission_TYPE
    fetchSubmissionTypesSuccess(state, action) {
      state.submissionTypeList = action.payload;
      state.success = null;
      state.isActionSuccess = "LIST"
    },

    // UPDATE Submission_TYPE
    updateSubmissionTypeSuccess(state, action) {
      state.success = "Submission Type updated successfully."
      state.isActionSuccess = "UPDATE"
    },

    // DELETE Submission_TYPE
    deleteSubmissionTypeSuccess(state, action) {
      state.success = "Submission Type deleted successfully."
      state.isActionSuccess = "INACTIVE"
    },

  }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
  return async () => {
    dispatch(slice.actions.hasInitialState())
  }
}      /**
* TO INITIAL STATE
* @returns 
*/
export function toResetIsActionSuccessState() {
 return async () => {
     dispatch(slice.actions.resetIsActionState())
 }
}




/**
 * POST Submission_TYPE
 * @param newSubmissionType 
 * @returns 
 */
export function addSubmissionType(newSubmissionType: SubmissionTypeReqType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.post('/base-para/submission-type', newSubmissionType);
      dispatch(slice.actions.addSubmissionTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET Submission_TYPE
 * @param SubmissionTypeId  
 * @returns 
 */
export function fetchSubmissionType(SubmissionTypeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get(`/base-para/submission-type/${SubmissionTypeId}`);
      dispatch(slice.actions.fetchSubmissionTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET ALL Submission_TYPE
 * @param listParameters 
 * @returns 
 */
export function fetchSubmissionTypes(listParameters: listParametersType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/base-para/submission-type/pagination', { params: listParameters });
      dispatch(slice.actions.fetchSubmissionTypesSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * UPDATE Submission_TYPE
 * @param updateSubmissionType
 * @returns 
 */
export function updateSubmissionType(updatedSubmissionType: SubmissionTypesType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.put(`/base-para/submission-type/${updatedSubmissionType.submissionTypeId}`, updatedSubmissionType);
      dispatch(slice.actions.updateSubmissionTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * DELETE Submission_TYPE
 * @param SubmissionTypeId 
 * @returns 
 */
export function deleteSubmissionType(SubmissionTypeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      await axiosServices.delete(`/base-para/submission-type/${SubmissionTypeId}`);
      dispatch(slice.actions.deleteSubmissionTypeSuccess(SubmissionTypeId));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}
