// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types
import { DefaultRootStateProps, queryParamsProps, userResPostprops } from 'types/user-restrictions';

// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['userRestriction'] = {
    error: null,
    success: null,
    userRestrictions: null,
    userRestrictionList: null,
    newuserRestrictionList: null,
    isLoading: false,
    isPostSuccess: false,
    postsuccess: null,
    disablepostsuccess: null,
    isActionSuccess: null
};

const slice = createSlice({
    name: 'userRestriction',
    initialState,
    reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.success = null;
            state.isLoading = false;
            state.isPostSuccess = false;
            state.postsuccess = null;
        },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
        },

        startLoading(state) {
            state.isLoading = true;
        },

        finishLoading(state) {
            state.isLoading = false;
        },

        resetIsActionState(state) {
            state.isActionSuccess = null;
        },

        // POST USER_RESTRICTION
        addUserRestrictionSuccess(state, action) {
            state.postsuccess = "User-Restriction added successfully.";
            state.isPostSuccess = true
            state.isActionSuccess = "APPROVE"
        },

        //get all restrictions
        fetchRestrictionSuccess(state, action) {
            state.newuserRestrictionList = action.payload;
            //state.postsuccess = null;
            state.success = null;
            state.isActionSuccess = "LIST"
        },

        //get all restrictions
        fetchRestrictionSuccessTwo(state, action) {
            state.userRestrictionList = action.payload;
            //  state.postsuccess = null;
            state.success = null;
        },

        //disable restrictions
        fetchRestrictionDisable(state, action) {
            state.disablepostsuccess = "Restricted";
            state.isPostSuccess = true
            state.isActionSuccess = "REJECT"
        },

    }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
    return async () => {
        dispatch(slice.actions.hasInitialState())
    }
}

export function toResetIsActionSuccessState() {
    return async () => {
        dispatch(slice.actions.resetIsActionState())
    }
  }

/**
 * POST URestriction
 * @param newURestriction 
 * @returns 
 */
export function addUserRestrictionSuccess(newURestriction: userResPostprops) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.post('/user-management/user/userRestrictions', newURestriction);
            dispatch(slice.actions.addUserRestrictionSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}

/**
 * GET ALL Menu Table
 * @param queryParams 
 * @returns 
 */
export function fetchRestrictionSuccess(queryParams: queryParamsProps) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get(`/user-management/newUserRestriction/user/${queryParams?.userId}/branch/${queryParams?.branchId}`, { params: queryParams });
            dispatch(slice.actions.fetchRestrictionSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}

/**
 * GET ALL Menu Table
 * @param queryParams 
 * @returns 
 */
export function fetchRestrictionSuccessTwo(queryParams: queryParamsProps) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get(`/user-management/userRestriction/user/${queryParams?.userId}/brach/${queryParams?.branchId}`, { params: queryParams });
            dispatch(slice.actions.fetchRestrictionSuccessTwo(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}

/**
 * DISABLE resctrictions
 * @param menuActionId 
 * @param userId
 * @param branchId
 * @returns 
 */
export function restrictionDisable(menuActionId: any, userId: any, branchId: any) {
    return async () => {
        dispatch(slice.actions.startLoading());
        try {
            const response = await axiosServices.put(`/user-management/userRestriction/user/${userId}/branch/${branchId}/menuAction/${menuActionId}`);
            dispatch(slice.actions.fetchRestrictionDisable(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}

