	// third-party
    import { createSlice } from '@reduxjs/toolkit';

    // project imports
    import { axiosServices } from 'utils/axios';
    import { dispatch } from '../index';
    
    // types 
    import { DefaultRootStateProps, TransactionCodeReqType, listParametersType, TransactionCodesType } from 'types/transaction-code';
    
    
    
    // ----------------------------------------------------------------------
    
    const initialState: DefaultRootStateProps['transactionCode'] = {
        error: null,
        TRCsuccess: null,
        isLoading: false,
        transactionCodeList: null,
        transactionCode: null,
        selectTransactionCodeType: null
    };
    
    const slice = createSlice({
      name: 'TransactionCode',
      initialState,
      reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
          state.error = null;
          state.TRCsuccess = null;
          state.isLoading = false;
        },
    
        // HAS ERROR
        hasError(state, action) {
          state.error = action.payload;
        },
    
        startLoading(state) {
          state.isLoading = true;
        },
    
        finishLoading(state) {
          state.isLoading = false;
        },
    
        // POST Transaction_CODE 
        addTransactionCodeSuccess(state, action) {
          state.TRCsuccess = "Transaction Code created successfully."
        },
    
        // GET Transaction_CODE_BY_ID
        fetchTransactionCodeSuccess(state, action) {
          state.transactionCode = action.payload;
          state.TRCsuccess = null
        },
    
        // GET ALL Transaction_CODE
        fetchTransactionCodesSuccess(state, action) {
          state.transactionCodeList = action.payload;
          state.TRCsuccess = null
        },
    
        // UPDATE Transaction_CODE
        updateTransactionCodeSuccess(state, action) {
          state.TRCsuccess = "Transaction Code updated successfully."
        },
    
        // DELETE Transaction_CODE
        deleteTransactionCodeSuccess(state, action) {
          state.TRCsuccess = "Transaction Code deleted successfully."
        },
        // GET USER_Type_LIST
        fetchTransactionCodeListSuccess(state, action) {
            state.selectTransactionCodeType= action.payload;
            state.TRCsuccess = null
        },
    
      }
    });
    
    // Reducer
    export default slice.reducer;
    
    // ----------------------------------------------------------------------
    
    /**
     * TO INITIAL STATE
     * @returns 
     */
    export function toInitialState() {
      return async () => {
        dispatch(slice.actions.hasInitialState())
      }
    }
    
    /**
     * POST Transaction_CODE
     * @param newTransactionCode 
     * @returns 
     */
    export function addTransactionCode(newTransactionCode: TransactionCodeReqType) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.post('/base-para/transaction-code/add', newTransactionCode);
          dispatch(slice.actions.addTransactionCodeSuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
    /**
     * GET Transaction_CODE
     * @param TransactionCodeId  
     * @returns 
     */
    export function fetchTransactionCode(TransactionCodeId: number) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.get(`/base-para/transaction-code/get/${TransactionCodeId}`);
          dispatch(slice.actions.fetchTransactionCodeSuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
    /**
     * GET ALL Transaction_CODE
     * @param listParameters 
     * @returns 
     */
    export function fetchTransactionCodes(listParameters: listParametersType) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.get('/base-para/transaction-code/tbl', { params: listParameters });
          dispatch(slice.actions.fetchTransactionCodesSuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
    /**
     * UPDATE Transaction_CODE
     * @param updateTransactionCode
     * @returns 
     */
    export function updateTransactionCode(updatedTransactionCode: TransactionCodesType) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.put(`/base-para/transaction-code/update/${updatedTransactionCode.transactionCodeId}`, updatedTransactionCode);
          dispatch(slice.actions.updateTransactionCodeSuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
    /**
     * DELETE Transaction_CODE
     * @param TransactionCodeId 
     * @returns 
     */
    export function deleteTransactionCode(TransactionCodeId: number) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          await axiosServices.delete(`/base-para/transaction-code/delete/${TransactionCodeId}`);
          dispatch(slice.actions.deleteTransactionCodeSuccess(TransactionCodeId));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }

    /**
     * GET ALL USER_TypeS
     * @param listParameters 
     * @returns 
     */
export function fetchTransactionCodeList() {
    return async () => {
      dispatch(slice.actions.startLoading());
  
      try {
        const response = await axiosServices.get('/base-para/transaction-code/all');
        dispatch(slice.actions.fetchTransactionCodeListSuccess(response.data));
      } catch (error) {
        dispatch(slice.actions.hasError(error));
      } finally {
        dispatch(slice.actions.finishLoading());
      }
    };
  }