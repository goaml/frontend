// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, listParametersType, UserTypePost } from 'types/user-type';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['userType'] = {
  error: null,
  typesuccess: null,
  isLoading: false,
  selectUserTypes: null,
  userTypeList: null,
  userType: null,
  isActionSuccess: null
};

const slice = createSlice({
  name: 'userType',
  initialState,
  reducers: {


    // TO INITIAL STATE
    hasInitialState(state) {
      state.error = null;
      state.typesuccess = null;
      state.isLoading = false;
    },

    // POST USER_ 
    addUserSuccess(state, action) {
      state.typesuccess = "User Type created successfully."
      state.isActionSuccess = "CREATE"
    },
    updateUserSuccess(state, action) {
      state.typesuccess = "User Type updated successfully."
      state.isActionSuccess = "UPDATE"
    },
    // HAS ERROR
    hasError(state, action) {
      state.error = action.payload;
    },

    startLoading(state) {
      state.isLoading = true;
    },

    finishLoading(state) {
      state.isLoading = false;
    },
    resetIsActionState(state) {
      state.isActionSuccess = null;
    },
    // GET USER_Type_LIST
    fetchUserTypeListSuccess(state, action) {
      state.selectUserTypes = action.payload;
      state.typesuccess = null
      state.isActionSuccess = "LIST"
    },
    // GET USER_Type_LIST
    fetchUserTypesSuccess(state, action) {
      state.userTypeList = action.payload;
      state.typesuccess = null
    },
    // DELETE USER_
    deleteUserSuccess(state, action) {
      state.typesuccess = "User Type deleted successfully."
      state.isActionSuccess = "INACTIVE"
    },
  }
})

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
  return async () => {
    dispatch(slice.actions.hasInitialState())
  }
}

export function toResetIsActionSuccessState() {
  return async () => {
    dispatch(slice.actions.resetIsActionState())
  }
}
/**
* POST USER_
* @param newUser 
* @returns 
*/
export function addUserType(newUser: UserTypePost) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.post(`/user-management/user-type`, newUser);
      dispatch(slice.actions.addUserSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
     * GET ALL USER_TypeS
     * @param listParameters 
     * @returns 
     */
export function fetchUserTypeList() {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/user-management/user-type/fdd');
      dispatch(slice.actions.fetchUserTypeListSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
* GET ALL USER_
* @param listParameters 
* @returns 
*/
export function fetchUserTypes(listParameters: listParametersType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/user-management/user-type', { params: listParameters });
      dispatch(slice.actions.fetchUserTypesSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}
/**
 * UPDATE USER_
 * @param updateUser
 * @returns 
 */
export function updateUserType(updatedUser: UserTypePost) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.put(`/user-management/user-type/${updatedUser.userTypeId}`, updatedUser);
      dispatch(slice.actions.updateUserSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * DELETE USER_
 * @param UserId 
 * @returns 
 */
export function deleteUserType(UserId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      await axiosServices.delete(`/user-management/user-type/${UserId}`);
      dispatch(slice.actions.deleteUserSuccess(UserId));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}
