// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types
import { DefaultRootStateProps, queryParamsProps } from 'types/user-dynamic-drawer';

// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['userDynamicDrawer'] = {
    error: null,
    success: null,
    isLoading: false,
    userDynamicDrawer: null,
};

const slice = createSlice({
    name: 'userDynamicDrawer',
    initialState,
    reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.success = null;
            state.isLoading = false;
        },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
        },

        startLoading(state) {
            state.isLoading = true;
        },

        finishLoading(state) {
            state.isLoading = false;
        },

        //GET USER DYNAMIC DRAWER
        fetchGetUserDynamicDrawerSuccess(state, action) {
            state.userDynamicDrawer = action.payload;
            state.success = null;
        },

    }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
    return async () => {
        dispatch(slice.actions.hasInitialState())
    }
}

/**
 * GET ALL Dynamic Menu Items by Module_ID and User_ID
 * @param queryParams 
 * @returns 
 */
export function fetchGetUserDynamicDrawerSuccess(queryParams: queryParamsProps) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get(`/user-management/module/${queryParams.moduleId}/user/${queryParams.userId}/side-menu`, { params: queryParams });
            dispatch(slice.actions.fetchGetUserDynamicDrawerSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}
