// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types
import { DefaultRootStateProps,  queryParamsProps } from 'types/account-my-client';

// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['MyClientAccount'] = {
    error: null,
    success: null,
    MyClientAccountList: null,
    isLoading: false,
};

const slice = createSlice({
    name: 'MyClientAccount',
    initialState,
    reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.success = null;
            state.isLoading = false;
        },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
        },

        startLoading(state) {
            state.isLoading = true;
        },

        finishLoading(state) {
            state.isLoading = false;
        },

    
        // GET ALL MyClientAccount 
        fetchMyClientAccountSuccess(state, action) {
            state.MyClientAccountList = action.payload;
            state.success = null
        },

    }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
    return async () => {
        dispatch(slice.actions.hasInitialState())
    }
}



/**
 * GET ALL MyClientAccount
 * @param queryParams 
 * @returns 
 */
export function fetchMyClientAccountTable(queryParams: queryParamsProps) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get('/transaction/account-my-client/tbl', { params: queryParams });
            dispatch(slice.actions.fetchMyClientAccountSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}


