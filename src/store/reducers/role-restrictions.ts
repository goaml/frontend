// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types
import { DefaultRootStateProps, RoleResPostReq, queryParamsProps } from 'types/role-restrictions';

// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['rolesRestriction'] = {
    error: null,
    success: null,
    roleRestrictions: null,
    roleRestrictionList: null,
    rolAccessList: null,
    isLoading: false,
    selectRoles: null,
    rolAccesss: null,
    isPostSuccess: false,
    postsuccess: null,
    disablepostsuccess: null,
    isActionSuccess:null
};

const slice = createSlice({
    name: 'rolesRestriction',
    initialState,
    reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.success = null;
            state.isLoading = false;
            state.isPostSuccess = false
        },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
        },

        startLoading(state) {
            state.isLoading = true;
        },

        finishLoading(state) {
            state.isLoading = false;
        },
        resetIsActionState(state) {
            state.isActionSuccess = null;
        },

        // POST ROLE_RESTRICTION
        addRoleRestrictionSuccess(state, action) {
            state.postsuccess = "Role-Restriction added successfully.";
            state.isPostSuccess = true
            state.isActionSuccess = "APPROVE"
        },

        // GET ROLE_RESTRICTION
        fetchRoleSuccess(state, action) {
            state.roleRestrictions = action.payload;
            state.postsuccess = null;
            state.success = null
        },

        // GET ALL ROLE_RESTRICTION
        fetchRolesSuccess(state, action) {
            state.rolAccessList = action.payload;
            state.postsuccess = null;
            state.success = null
        },

        // GET ALL ROLE_RESTRICTION
        fetchRolesSuccessTwo(state, action) {
            state.roleRestrictionList = action.payload;
            state.postsuccess = null;
            state.success = null
        },

        //get all roles
        fetchGetAllRolesSuccess(state, action) {
            state.selectRoles = action.payload?.result;
            state.postsuccess = null;
            state.success = null
            state.isActionSuccess = "LIST"
        },

        //disable restrictions
        fetchRestrictionDisable(state, action) {
            state.disablepostsuccess = "Restricted";
            state.isPostSuccess = true
            state.isActionSuccess = "REJECT"
        },
    }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
    return async () => {
        dispatch(slice.actions.hasInitialState())
    }
}

export function toResetIsActionSuccessState() {
    return async () => {
        dispatch(slice.actions.resetIsActionState())
    }
  }
 
/**
 * POST RRestrictions
 * @param newRRestriction 
 * @returns 
 */
export function addRoleRestrictionSuccess(newRRestriction: RoleResPostReq) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.post('/user-management/role-restriction', newRRestriction);
            dispatch(slice.actions.addRoleRestrictionSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}


/**
 * GET ALL ROLE_RESTRICTION
 * @param queryParams 
 * @returns 
 */
export function fetchRolesSuccess(roleId: number) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get(`/user-management/roleAccess/allowed/false/userRoleId/${roleId}`);
            dispatch(slice.actions.fetchRolesSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}

/**
 * GET ALL ROLE_RESTRICTION
 * @param queryParams 
 * @returns 
 */
export function fetchRolesSuccessTwo(roleId: number) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get(`/user-management/roleRestriction/role/${roleId}`);
            dispatch(slice.actions.fetchRolesSuccessTwo(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}

/**
 * GET ALL Roles for dropdown
 * @param queryParams 
 * @returns 
 */
export function fetchSelectRoles(queryParams: queryParamsProps) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get('/user-management/roleView', { params: queryParams });
            dispatch(slice.actions.fetchGetAllRolesSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}

/**
 * DISABLE resctrictions
 * @param menuActionId 
 * @param userRoleId
 * @returns 
 */
export function restrictionDisable(menuActionId: any, userRoleId: any) {
    return async () => {
        dispatch(slice.actions.startLoading());
        try {
            const response = await axiosServices.put(`/user-management/roleRestriction/role/${userRoleId}/menuAction/${menuActionId}`);
            dispatch(slice.actions.fetchRestrictionDisable(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}