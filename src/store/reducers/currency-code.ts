// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, CurrenciesReqType, listParametersType, CurrenciessType } from 'types/currency-code';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['currencies'] = {
  error: null,
  success: null,
  isLoading: false,
  currenciesList: null,
  currencies: null,
  isActionSuccess: null
};

const slice = createSlice({
  name: 'currencies',
  initialState,
  reducers: {
    // TO INITIAL STATE
    hasInitialState(state) {
      state.error = null;
      state.success = null;
      state.isLoading = false;
    },

    // HAS ERROR
    hasError(state, action) {
      state.error = action.payload;
    },

    startLoading(state) {
      state.isLoading = true;
    },

    finishLoading(state) {
      state.isLoading = false;
    },
    resetIsActionState(state) {
      state.isActionSuccess = null;
  },

    // POST Submission_TYPE 
    addCurrenciesSuccess(state, action) {
      state.success = "Currency Code created successfully."
      state.isActionSuccess = "CREATE"
    },

    // GET Submission_TYPE_BY_ID
    fetchCurrenciesSuccess(state, action) {
      state.currencies = action.payload;
      state.success = null
    },

    // GET ALL Submission_TYPE
    fetchCurrenciessSuccess(state, action) {
      state.currenciesList = action.payload;
      state.success = null
      state.isActionSuccess = "LIST"
    },

    // UPDATE Submission_TYPE
    updateCurrenciesSuccess(state, action) {
      state.success = "Currency Code updated successfully."
      state.isActionSuccess = "UPDATE"
    },

    // DELETE Submission_TYPE
    deleteCurrenciesSuccess(state, action) {
      state.success = "Currency Code deleted successfully."
      state.isActionSuccess = "INACTIVE"
    },

  }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
  return async () => {
    dispatch(slice.actions.hasInitialState())
  }
}
/* TO INITIAL STATE
* @returns 
*/
export function toResetIsActionSuccessState() {
  return async () => {
      dispatch(slice.actions.resetIsActionState())
  }
}

/**
 * POST Submission_TYPE
 * @param newCurrencies 
 * @returns 
 */
export function addCurrencies(newCurrencies: CurrenciesReqType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.post('/base-para/currency', newCurrencies);
      dispatch(slice.actions.addCurrenciesSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET Submission_TYPE
 * @param CurrenciesId  
 * @returns 
 */
export function fetchCurrencies(CurrenciesId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get(`/base-para/currency/${CurrenciesId}`);
      dispatch(slice.actions.fetchCurrenciesSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET ALL Submission_TYPE
 * @param listParameters 
 * @returns 
 */
export function fetchCurrenciess(listParameters: listParametersType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/base-para/currency/tbl', { params: listParameters });
      dispatch(slice.actions.fetchCurrenciessSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * UPDATE Submission_TYPE
 * @param updateCurrencies
 * @returns 
 */
export function updateCurrencies(updatedCurrencies: CurrenciessType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.put(`/base-para/currency/${updatedCurrencies.currenciesId}`, updatedCurrencies);
      dispatch(slice.actions.updateCurrenciesSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * DELETE Submission_TYPE
 * @param CurrenciesId 
 * @returns 
 */
export function deleteCurrencies(CurrenciesId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      await axiosServices.delete(`/base-para/currency/${CurrenciesId}`);
      dispatch(slice.actions.deleteCurrenciesSuccess(CurrenciesId));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}
