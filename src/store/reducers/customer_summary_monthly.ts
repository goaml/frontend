// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, listParametersType } from 'types/customer_summary_monthly';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['customerSummary'] = {
  error: null,
  success: null,
  isLoading: false,
  customerSummaryList: null,
  customerSummary: null,
  isActionSuccess: null
};

const slice = createSlice({
  name: 'customerSummary',
  initialState,
  reducers: {
    // TO INITIAL STATE
    hasInitialState(state) {
      state.error = null;
      state.success = null;
      state.isLoading = false;
    },

    // HAS ERROR
    hasError(state, action) {
      state.error = action.payload;
    },

    startLoading(state) {
      state.isLoading = true;
    },

    finishLoading(state) {
      state.isLoading = false;
    },
    resetIsActionState(state) {
      state.isActionSuccess = null;
  },


    // GET ALL customerSummary
    fetchCitysSuccess(state, action) {
      state.customerSummaryList = action.payload;
      state.success = null
      state.isActionSuccess = "LIST"
    },

  }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
  return async () => {
    dispatch(slice.actions.hasInitialState())
  }
}

/* TO INITIAL STATE
* @returns 
*/
export function toResetIsActionSuccessState() {
  return async () => {
      dispatch(slice.actions.resetIsActionState())
  }
}


/**
 * GET ALL customerSummary
 * @param listParameters 
 * @returns 
 */
export function fetchCustomerSummaryMonthly(listParameters: listParametersType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/base-summery/customer-summary-monthly/tbl', { params: listParameters });
      dispatch(slice.actions.fetchCitysSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}


