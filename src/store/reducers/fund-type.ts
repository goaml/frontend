// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, FundTypeReqType, FundTypesType, listParametersType} from 'types/fund-type';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['fundType'] = {
  error: null,
  FTsuccess: null,
  isLoading: false,
  FundTypeList: null,
  FundType: null,
  selectFundType: null,
  isActionSuccess: {
    message: null,
    keyValue: null,
    keyValueId: null
  }
};

const slice = createSlice({
    name: 'fundType',
    initialState,
    reducers: {


        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.FTsuccess = null;
            state.isLoading = false;
          },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
          },
      
          startLoading(state) {
            state.isLoading = true;
          },
      
          finishLoading(state) {
            state.isLoading = false;
          },
          resetIsActionState(state) {
            state.isActionSuccess = { message: null, keyValue: null ,keyValueId:null };
        },
                            // POST FUND_TYPE 
        addFundTypeSuccess(state, action) {
          state.FTsuccess = "Fund Type created successfully."
          state.isActionSuccess = {
            message: "CREATE",
            keyValue: action.payload?.fundTypeCode,
            keyValueId: action.payload.fundTypeId
          };
        },
    
        // GET FUND_TYPE_BY_ID
        fetchFundType(state, action) {
          state.FundType = action.payload;
          state.FTsuccess = null
        },
    
        // GET ALL FUND_TYPE
        fetchMPRCodesSuccess(state, action) {
          state.FundType = action.payload;
          state.FTsuccess = null
        },
    
        // UPDATE FUND_TYPE
        updateFundType(state, action) {
          state.FTsuccess = "Fund Type updated successfully."
          state.isActionSuccess = {
            message: "UPDATE",
            keyValue: action.payload.fundTypeCode,
            keyValueId: action.payload.fundTypeId
          };
        },
    
        // DELETE FUND_TYPE
        deleteFundType(state, action) {
          state.FTsuccess = "Fund Type deleted successfully."
          state.isActionSuccess = {
            message: "DELETE",
            keyValue: action.payload.fundTypeCode,
            keyValueId: action.payload.fundTypeId
          };
        },
            // GET USER_Type_LIST
            fetchUserTypeListSuccess(state, action) {
                state.selectFundType= action.payload;
                state.FTsuccess = null
            },
            
            // GET ALL FUND_TYPE
        fetchFundTypesSuccess(state, action) {
          state.FundTypeList = action.payload;
          state.FTsuccess = null;
          state.isActionSuccess = {
            message: "LIST",
            keyValue: "N/A" ,
            keyValueId: 0
          };
        },
    }})

    // Reducer
    export default slice.reducer;
    
    // ----------------------------------------------------------------------
    
    /**
     * TO INITIAL STATE
     * @returns 
     */
    export function toInitialState() {
      return async () => {
        dispatch(slice.actions.hasInitialState())
      }
    }

      /**
   * TO INITIAL STATE
   * @returns 
   */
      export function toResetIsActionSuccessState() {
        return async () => {
            dispatch(slice.actions.resetIsActionState())
        }
      }

/**
     * GET ALL USER_TypeS
     * @param listParameters 
     * @returns 
     */
export function fetchFundTypeList() {
    return async () => {
      dispatch(slice.actions.startLoading());
  
      try {
        const response = await axiosServices.get('/base-para/fund-type/all');
        dispatch(slice.actions.fetchUserTypeListSuccess(response.data));
      } catch (error) {
        dispatch(slice.actions.hasError(error));
      } finally {
        dispatch(slice.actions.finishLoading());
      }
    };
  }
      /**
     * GET ALL FUND_TYPE
     * @param listParameters 
     * @returns 
     */
      export function fetchFundTypes(listParameters: listParametersType) {
        return async () => {
          dispatch(slice.actions.startLoading());
      
          try {
            const response = await axiosServices.get('/base-para/fund-type/tbl', { params: listParameters });
            dispatch(slice.actions.fetchFundTypesSuccess(response.data));
          } catch (error) {
            dispatch(slice.actions.hasError(error));
          } finally {
            dispatch(slice.actions.finishLoading());
          }
        };
      }



            /**
     * GET FUND_TYPE
     * @param FundTypeId  
     * @returns 
     */
            export function fetchFundType(FundTypeId: number) {
              return async () => {
                dispatch(slice.actions.startLoading());
            
                try {
                  const response = await axiosServices.get(`/base-para/fund-type/get/${FundTypeId}`);
                  dispatch(slice.actions.fetchMPRCodesSuccess(response.data));
                } catch (error) {
                  dispatch(slice.actions.hasError(error));
                } finally {
                  dispatch(slice.actions.finishLoading());
                }
              };
            }
                /**
             * POST FUND_TYPE
             * @param newFundType 
             * @returns 
             */
                export function addFundType(newFundType: FundTypeReqType) {
                  return async () => {
                    dispatch(slice.actions.startLoading());
                
                    try {
                      const response = await axiosServices.post('/base-para/fund-type/add', newFundType);
                      dispatch(slice.actions.addFundTypeSuccess(response.data));
                    } catch (error) {
                      dispatch(slice.actions.hasError(error));
                    } finally {
                      dispatch(slice.actions.finishLoading());
                    }
                  };
                }
                 
            /**
             * UPDATE FUND_TYPE
             * @param updateFundType
             * @returns 
             */
            export function updateFundType(updatedFundType: FundTypesType) {
              return async () => {
                dispatch(slice.actions.startLoading());
            
                try {
                  const response = await axiosServices.put(`/base-para/fund-type/update/${updatedFundType.fundTypeId}`, updatedFundType);
                  dispatch(slice.actions.updateFundType(response.data));
                } catch (error) {
                  dispatch(slice.actions.hasError(error));
                } finally {
                  dispatch(slice.actions.finishLoading());
                }
              };
            }
            
            /**
             * DELETE FUND_TYPE
             * @param FundTypeId 
             * @returns 
             */
            export function deleteFundType(FundTypeId: number) {
              return async () => {
                dispatch(slice.actions.startLoading());
            
                try {
                  await axiosServices.delete(`/base-para/fund-type/delete/${FundTypeId}`);
                  dispatch(slice.actions.deleteFundType(FundTypeId));
                } catch (error) {
                  dispatch(slice.actions.hasError(error));
                } finally {
                  dispatch(slice.actions.finishLoading());
                }
              };
            }