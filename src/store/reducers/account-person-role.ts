// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, AccountPersonRoleTypeReqType, listParametersType, AccountPersonRoleTypesType } from 'types/account-person-role';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['accountPersonRoleType'] = {
  error: null,
  success: null,
  isLoading: false,
  accountPersonRoleTypeList: null,
  accountPersonRoleType: null,
  isActionSuccess: null
};

const slice = createSlice({
  name: 'accountPersonRoleTypeType',
  initialState,
  reducers: {
    // TO INITIAL STATE
    hasInitialState(state) {
      state.error = null;
      state.success = null;
      state.isLoading = false;
    },

    // HAS ERROR
    hasError(state, action) {
      state.error = action.payload;
    },

    startLoading(state) {
      state.isLoading = true;
    },

    finishLoading(state) {
      state.isLoading = false;
    },
    resetIsActionState(state) {
      state.isActionSuccess = null;
  },


    // POST AccountPersonRoleType_TYPE 
    addAccountPersonRoleTypeTypeSuccess(state, action) {
      state.success = "Account Person Role Type created successfully."
      state.isActionSuccess = "CREATE"
    },

    // GET AccountPersonRoleType_TYPE_BY_ID
    fetchAccountPersonRoleTypeTypeSuccess(state, action) {
      state.accountPersonRoleType = action.payload;
      state.success = null
    },

    // GET ALL AccountPersonRoleType_TYPE
    fetchAccountPersonRoleTypeTypesSuccess(state, action) {
      state.accountPersonRoleTypeList = action.payload;
      state.success = null;
      state.isActionSuccess = "LIST"
    },

    // UPDATE AccountPersonRoleType_TYPE
    updateAccountPersonRoleTypeTypeSuccess(state, action) {
      state.success = "Account Person Role Type updated successfully."
      state.isActionSuccess = "UPDATE"
    },

    // DELETE AccountPersonRoleType_TYPE
    deleteAccountPersonRoleTypeTypeSuccess(state, action) {
      state.success = "Account Person Role Type deleted successfully."
      state.isActionSuccess = "INACTIVE"
    },

  }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
  return async () => {
    dispatch(slice.actions.hasInitialState())
  }
}

/* TO INITIAL STATE
* @returns 
*/
export function toResetIsActionSuccessState() {
  return async () => {
      dispatch(slice.actions.resetIsActionState())
  }
}

/**
 * POST AccountPersonRoleType_TYPE
 * @param newAccountPersonRoleTypeType 
 * @returns 
 */
export function addAccountPersonRoleTypeType(newAccountPersonRoleTypeType: AccountPersonRoleTypeReqType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.post('/base-para/account-person-role-types', newAccountPersonRoleTypeType);
      dispatch(slice.actions.addAccountPersonRoleTypeTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET AccountPersonRoleType_TYPE
 * @param AccountPersonRoleTypeTypeId  
 * @returns 
 */
export function fetchAccountPersonRoleTypeType(AccountPersonRoleTypeTypeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get(`/base-para/account-person-role-types/${AccountPersonRoleTypeTypeId}`);
      dispatch(slice.actions.fetchAccountPersonRoleTypeTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET ALL AccountPersonRoleType_TYPE
 * @param listParameters 
 * @returns 
 */
export function fetchAccountPersonRoleTypeTypes(listParameters: listParametersType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/base-para/account-person-role-types/tbl', { params: listParameters });
      dispatch(slice.actions.fetchAccountPersonRoleTypeTypesSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * UPDATE AccountPersonRoleType_TYPE
 * @param updateAccountPersonRoleTypeType
 * @returns 
 */
export function updateAccountPersonRoleTypeType(updatedAccountPersonRoleTypeType: AccountPersonRoleTypesType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.put(`/base-para/account-person-role-types/${updatedAccountPersonRoleTypeType.accountPersonRoleTypeId}`, updatedAccountPersonRoleTypeType);
      dispatch(slice.actions.updateAccountPersonRoleTypeTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * DELETE AccountPersonRoleType_TYPE
 * @param AccountPersonRoleTypeTypeId 
 * @returns 
 */
export function deleteAccountPersonRoleTypeType(AccountPersonRoleTypeTypeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      await axiosServices.delete(`/base-para/account-person-role-types/${AccountPersonRoleTypeTypeId}`);
      dispatch(slice.actions.deleteAccountPersonRoleTypeTypeSuccess(AccountPersonRoleTypeTypeId));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}
