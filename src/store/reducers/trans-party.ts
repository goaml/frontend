// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types
import { DefaultRootStateProps,  queryParamsProps } from 'types/trans-party';

// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['transParty'] = {
    error: null,
    success: null,
    transPartyList: null,
    isLoading: false,
};

const slice = createSlice({
    name: 'transParty',
    initialState,
    reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.success = null;
            state.isLoading = false;
        },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
        },

        startLoading(state) {
            state.isLoading = true;
        },

        finishLoading(state) {
            state.isLoading = false;
        },

    
        // GET ALL transParty 
        fetchTransPartysSuccess(state, action) {
            state.transPartyList = action.payload;
            state.success = null
        },

    }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
    return async () => {
        dispatch(slice.actions.hasInitialState())
    }
}



/**
 * GET ALL transParty
 * @param queryParams 
 * @returns 
 */
export function fetchTransPartysTable(queryParams: queryParamsProps) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get('/transaction/party/tbl', { params: queryParams });
            dispatch(slice.actions.fetchTransPartysSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}


