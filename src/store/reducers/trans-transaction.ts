// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types
import { DefaultRootStateProps,  queryParamsProps } from 'types/trans-transaction';

// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['transTransaction'] = {
    error: null,
    success: null,
    transTransactionList: null,
    isLoading: false,
};

const slice = createSlice({
    name: 'transTransaction',
    initialState,
    reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.success = null;
            state.isLoading = false;
        },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
        },

        startLoading(state) {
            state.isLoading = true;
        },

        finishLoading(state) {
            state.isLoading = false;
        },

    
        // GET ALL transTransaction 
        fetchTransTransactionsSuccess(state, action) {
            state.transTransactionList = action.payload;
            state.success = null
        },

    }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
    return async () => {
        dispatch(slice.actions.hasInitialState())
    }
}



/**
 * GET ALL transTransaction
 * @param queryParams 
 * @returns 
 */
export function fetchTransTransactionsTable(queryParams: queryParamsProps) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get('/transaction/transaction/tbl', { params: queryParams });
            dispatch(slice.actions.fetchTransTransactionsSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}


