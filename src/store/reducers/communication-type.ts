// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, CommunicationTypeReqType, listParametersType, CommunicationTypesType } from 'types/communication-type';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['communicationType'] = {
  error: null,
  success: null,
  isLoading: false,
  communicationTypeList: null,
  communicationType: null,
  isActionSuccess: null
};

const slice = createSlice({
  name: 'communicationType',
  initialState,
  reducers: {
    // TO INITIAL STATE
    hasInitialState(state) {
      state.error = null;
      state.success = null;
      state.isLoading = false;
    },

    // HAS ERROR
    hasError(state, action) {
      state.error = action.payload;
    },

    startLoading(state) {
      state.isLoading = true;
    },

    finishLoading(state) {
      state.isLoading = false;
    },
    resetIsActionState(state) {
      state.isActionSuccess = null;
  },
    // POST Account_TYPE 
    addCommunicationTypeSuccess(state, action) {
      state.success = "Communication Type created successfully."
      state.isActionSuccess = "CREATE"
    },

    // GET Account_TYPE_BY_ID
    fetchCommunicationTypeSuccess(state, action) {
      state.communicationType = action.payload;
      state.success = null
    },

    // GET ALL Account_TYPE
    fetchCommunicationTypesSuccess(state, action) {
      state.communicationTypeList = action.payload;
      state.success = null
      state.isActionSuccess = "LIST"
    },

    // UPDATE Account_TYPE
    updateCommunicationTypeSuccess(state, action) {
      state.success = "Communication Type updated successfully."
      state.isActionSuccess = "UPDATE"
    },

    // DELETE Account_TYPE
    deleteCommunicationTypeSuccess(state, action) {
      state.success = "Communication Type deleted successfully."
      state.isActionSuccess = "INACTIVE"
    },

  }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
  return async () => {
    dispatch(slice.actions.hasInitialState())
  }
}
/* TO INITIAL STATE
* @returns 
*/
export function toResetIsActionSuccessState() {
  return async () => {
      dispatch(slice.actions.resetIsActionState())
  }
}
/**
 * POST Account_TYPE
 * @param newCommunicationType 
 * @returns 
 */
export function addCommunicationType(newCommunicationType: CommunicationTypeReqType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.post('/base-para/communication-type', newCommunicationType);
      dispatch(slice.actions.addCommunicationTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET Account_TYPE
 * @param CommunicationTypeId  
 * @returns 
 */
export function fetchCommunicationType(CommunicationTypeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get(`/base-para/communication-type/${CommunicationTypeId}`);
      dispatch(slice.actions.fetchCommunicationTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET ALL Account_TYPE
 * @param listParameters 
 * @returns 
 */
export function fetchCommunicationTypes(listParameters: listParametersType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/base-para/communication-type/tbl', { params: listParameters });
      dispatch(slice.actions.fetchCommunicationTypesSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * UPDATE Account_TYPE
 * @param updateCommunicationType
 * @returns 
 */
export function updateCommunicationType(updatedCommunicationType: CommunicationTypesType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.put(`/base-para/communication-type/${updatedCommunicationType.communicationTypeId}`, updatedCommunicationType);
      dispatch(slice.actions.updateCommunicationTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * DELETE Account_TYPE
 * @param CommunicationTypeId 
 * @returns 
 */
export function deleteCommunicationType(CommunicationTypeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      await axiosServices.delete(`/base-para/communication-type/${CommunicationTypeId}`);
      dispatch(slice.actions.deleteCommunicationTypeSuccess(CommunicationTypeId));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}
