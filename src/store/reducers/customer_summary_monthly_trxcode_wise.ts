// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, listParametersType } from 'types/customer_summary_monthly_trxcode_wise';

// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['customer_summary_monthly_trxcode_wise'] = {
  error: null,
  success: null,
  isLoading: false,
  customer_summary_monthly_trxcode_wiseList: null,
  customer_summary_monthly_trxcode_wise: null,
  isActionSuccess: null
};

const slice = createSlice({
  name: 'customer_summary_monthly_trxcode_wise',
  initialState,
  reducers: {
    // TO INITIAL STATE
    hasInitialState(state) {
      state.error = null;
      state.success = null;
      state.isLoading = false;
    },

    // HAS ERROR
    hasError(state, action) {
      state.error = action.payload;
    },

    startLoading(state) {
      state.isLoading = true;
    },

    finishLoading(state) {
      state.isLoading = false;
    },
    resetIsActionState(state) {
      state.isActionSuccess = null;
  },


    // GET ALL customer_summary_monthly_trxcode_wise
    fetchCitysSuccess(state, action) {
      state.customer_summary_monthly_trxcode_wiseList = action.payload;
      state.success = null
      state.isActionSuccess = "LIST"
    },

  }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
  return async () => {
    dispatch(slice.actions.hasInitialState())
  }
}

/* TO INITIAL STATE
* @returns 
*/
export function toResetIsActionSuccessState() {
  return async () => {
      dispatch(slice.actions.resetIsActionState())
  }
}


/**
 * GET ALL customer_summary_monthly_trxcode_wise
 * @param listParameters 
 * @returns 
 */
export function fetchcustomer_summary_monthly_trxcode_wiseList(listParameters: listParametersType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/base-summery/customer-monthly-trx-code-wise/tbl', { params: listParameters });
      dispatch(slice.actions.fetchCitysSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}


