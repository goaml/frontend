// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types
import { BranchCodeReqType, BranchCodeType, DefaultRootStateProps, listParametersType } from 'types/branch';
import { MultipleBranchPostReq } from 'types/multiple-branch';

// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['userbranches'] = {
  error: null,
  success: null,
  users: null,
  userList: null,
  isLoading: false,
  selectBranches: null,
  selectBranchesForUsers: null,
  MultipleBranches: null,
  branchCodeList: null,
  isActionSuccess:null
};

const slice = createSlice({
  name: 'userbranches',
  initialState,
  reducers: {
    // TO INITIAL STATE
    hasInitialState(state) {
      state.error = null;
      state.success = null;
      state.isLoading = false;
    },

    // HAS ERROR
    hasError(state, action) {
      state.error = action.payload;
    },

    startLoading(state) {
      state.isLoading = true;
    },

    finishLoading(state) {
      state.isLoading = false;
    },
    resetIsActionState(state) {
      state.isActionSuccess = null;
  },

    // GET ALL CIVIL_STATUSES  
    fetchUsersSuccess(state, action) {
      state.userList = action.payload;
      state.success = null
    },

    //get all user
    fetchGetAllBranchesSuccess(state, action) {
      state.selectBranches = action.payload?.usRBrachDepartmentDto;
      state.success = null;
    },


    //get all branches for users
    fetchGetAllUserBranchSuccess(state, action) {
      state.selectBranchesForUsers = action.payload;
      state.success = null;
    },
    //get all user
    AddedmultpleBranchSuccess(state, action) {
      state.MultipleBranches = action.payload
      state.success = null;
      state.isActionSuccess = "LIST"
    },

    // POST MultipleBranch
    addMultipleBranchSuccess(state, action) {
      state.success = "Branch Assigned successfully."
      state.isActionSuccess = "CREATE"
    },

    // UPDATE MultipleBranch
    updateMultipleBranchSuccess(state, action) {
      state.success = "Assigned Branch Updated Successfully"
      state.isActionSuccess = "UPDATE"
    },

    // DELETE MultipleBranch
    deleteMultipleBranchSuccess(state, action) {
      state.success = "Assigned Branch Deleted Successfully"
      state.isActionSuccess = "INACTIVE"
    },

    // GET ALL BRANCH_CODE
    fetchBranchCodesSuccess(state, action) {
      state.selectBranches = action.payload;
      state.success = null
    },

    // POST BRANCH_CODE 
    addBranchCodeSuccess(state, action) {
      state.success = "Branch Code created successfully."
    },

    // UPDATE BRANCH_CODE
    updateBranchCodeSuccess(state, action) {
      state.success = "Branch Code updated successfully."
    },

    // DELETE BRANCH_CODE
    deleteBranchCodeSuccess(state, action) {
      state.success = "Branch Code deleted successfully."
    },
  }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
  return async () => {
    dispatch(slice.actions.hasInitialState())
  }
}

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toResetIsActionSuccessState() {
  return async () => {
      dispatch(slice.actions.resetIsActionState())
  }
}
/**
 * POST BRANCH_CODE
 * @param newBranchCode 
 * @returns 
 */
export function addBranchCode(newBranchCode: BranchCodeReqType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.post('/user-management/branch', newBranchCode);
      dispatch(slice.actions.addBranchCodeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET BRANCH_CODE
 * @param BranchCodeId  
 * @returns 
 */
export function fetchBranchCode(BranchCodeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get(`/user-management/branch/${BranchCodeId}`);
      dispatch(slice.actions.fetchBranchCodesSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET ALL BRANCH_CODE
 * @param listParameters 
 * @returns 
 */
export function fetchBranchCodes(listParameters: listParametersType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/user-management/getBranchList', { params: listParameters });
      dispatch(slice.actions.fetchBranchCodesSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * UPDATE BRANCH_CODE
 * @param updateBranchCode
 * @returns 
 */
export function updateBranchCode(updatedBranchCode: BranchCodeType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.put(`/user-management/branch/${updatedBranchCode.branchId}`, updatedBranchCode);
      dispatch(slice.actions.updateBranchCodeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * DELETE BRANCH_CODE
 * @param BranchCodeId 
 * @returns 
 */
export function deleteBranchCode(BranchCodeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      await axiosServices.delete(`/user-management/branch/${BranchCodeId}`);
      dispatch(slice.actions.deleteBranchCodeSuccess(BranchCodeId));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}


/**
 * GET ALL USER_TypeS
 * @param listParameters 
 * @returns 
 */
export function fetchBranchList() {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/user-management/branch/fdd');
      dispatch(slice.actions.fetchBranchCodesSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
* GET ALL Branches of the user
* @param userId
* @returns
*/
export function fetchGetAllBranchesOfUserForMultipleBranches(userId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get(`/user-management/user-branch`, { params: { userId: userId } });
      dispatch(slice.actions.AddedmultpleBranchSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
* POST MultipleBranch
* @param MultipleBranch 
* @returns 
*/
export function addMultipleBranches(MultipleBranch: MultipleBranchPostReq) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.post('/user-management/user-branch', MultipleBranch);
      dispatch(slice.actions.addMultipleBranchSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
* UPDATE MultipleBranch
* @param MultipleBranch
* @returns 
*/
export function updateMultipleBranch(MultipleBranch: MultipleBranchPostReq) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.put(`/user-management/user-branch/${MultipleBranch.employeeBranchId}`, MultipleBranch);
      dispatch(slice.actions.updateMultipleBranchSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
* DELETE MultipleBranch
* @param userRoleId 
* @returns 
*/
export function deleteMultipleBranch(brachDeptId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      await axiosServices.delete(`/user-management/user-branch/${brachDeptId}`);
      dispatch(slice.actions.deleteMultipleBranchSuccess(brachDeptId));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET ALL Branches of the user
 * @param queryParams 
 * @returns 
 */
export function fetchGetAllBranchesOfUser(userId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get(`/user-management/branchForUser/user/${userId}`);
      dispatch(slice.actions.fetchGetAllBranchesSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}