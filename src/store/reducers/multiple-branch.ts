// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types
import { DefaultRootStateProps } from 'types/multiple-branch';

// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['multipleBranch'] = {
    error: null,
    success: null,
    isLoading: false,
    NotAssignedBranchesForUsers: null,
};

const slice = createSlice({
    name: 'multipleBranch',
    initialState,
    reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.success = null;
            state.isLoading = false;
        },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
        },

        startLoading(state) {
            state.isLoading = true;
        },

        finishLoading(state) {
            state.isLoading = false;
        },

        // POST MultipleBranch
        addMultipleBranchSuccess(state, action) {
            state.success = "Branch created successfully."
        },

        //get not assigned branches for user
        fetchBranchesNotAssignedToUserSuccess(state, action) {
            state.NotAssignedBranchesForUsers = action.payload;
            state.success = null;
        },

    }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
    return async () => {
        dispatch(slice.actions.hasInitialState())
    }
}

/**
 * GET Not Assigned Branches for User
 * @param queryParams 
 * @returns 
 */
export function getBranchesNotAssignedToUserSuccess(userId: number) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get(`/user-management/branch-department-not-assigned-to-user/fdd/userId/${userId}`);
            dispatch(slice.actions.fetchBranchesNotAssignedToUserSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}