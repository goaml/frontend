// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps} from 'types/login-type';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['userLoginType'] = {
  error: null,
  typesuccess: null,
  isLoading: false,
  selectUserLoginTypes:null
};

const slice = createSlice({
    name: 'userLoginType',
    initialState,
    reducers: {


        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.typesuccess = null;
            state.isLoading = false;
          },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
          },
      
          startLoading(state) {
            state.isLoading = true;
          },
      
          finishLoading(state) {
            state.isLoading = false;
          },
            // GET USER_Type_LIST
            fetchUserTypeListSuccess(state, action) {
                state.selectUserLoginTypes = action.payload;
                state.typesuccess = null
            },
    }})

    // Reducer
    export default slice.reducer;
    
    // ----------------------------------------------------------------------
    
    /**
     * TO INITIAL STATE
     * @returns 
     */
    export function toInitialState() {
      return async () => {
        dispatch(slice.actions.hasInitialState())
      }
    }



/**
     * GET ALL USER_TypeS
     * @param listParameters 
     * @returns 
     */
export function fetchUserLoginTypeList() {
    return async () => {
      dispatch(slice.actions.startLoading());
  
      try {
        const response = await axiosServices.get('/user-management/login-type/fdd');
        dispatch(slice.actions.fetchUserTypeListSuccess(response.data));
      } catch (error) {
        dispatch(slice.actions.hasError(error));
      } finally {
        dispatch(slice.actions.finishLoading());
      }
    };
  }