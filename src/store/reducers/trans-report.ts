// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types
import { DefaultRootStateProps, queryParamsProps } from 'types/trans-report';

// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['transReport'] = {
    error: null,
    success: null,
    transReportList: null,
    isLoading: false,
    selectReportType: null
};

const slice = createSlice({
    name: 'transReport',
    initialState,
    reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.success = null;
            state.isLoading = false;
        },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
        },

        startLoading(state) {
            state.isLoading = true;
        },

        finishLoading(state) {
            state.isLoading = false;
        },


        // GET ALL transReport 
        fetchTransReportsSuccess(state, action) {
            state.transReportList = action.payload;
            state.success = null
        },
        // GET Report Type
        fetchReportTypeSelectSuccess(state, action) {
            state.selectReportType = action.payload;
            state.success = null

        }
    }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
    return async () => {
        dispatch(slice.actions.hasInitialState())
    }
}



/**
 * GET ALL transReport
 * @param queryParams 
 * @returns 
 */
export function fetchTransReportsTable(queryParams: queryParamsProps) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get('/transaction/report/tbl', { params: queryParams });
            dispatch(slice.actions.fetchTransReportsSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}


/**
     * GET ALL SELECT REPORT_TYPES
     * @param listParameters 
     * @returns 
     */
export function fetchReportForSelect() {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get('/transaction/report/all');
            dispatch(slice.actions.fetchReportTypeSelectSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}
