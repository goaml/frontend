// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, MultiPartyFundsCodeResType, MultiPartyFundsCodesType, listParametersType} from 'types/multiParty-fundCode';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['multiPartyFundsCode'] = {
    error: null,
    MPFsuccess: null,
    isLoading: false,
    MultiPartyFundsCodeList: null,
    MultiPartyFundsCode: null,
    selectMultiPartyFundsCode:null
};

const slice = createSlice({
    name: 'MultiPartyFundsCode',
    initialState,
    reducers: {


        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.MPFsuccess = null;
            state.isLoading = false;
          },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
          },
      
          startLoading(state) {
            state.isLoading = true;
          },
      
          finishLoading(state) {
            state.isLoading = false;
          },
        // POST MRF_CODE 
        addMultiPartyFundCodeSuccess(state, action) {
          state.MPFsuccess = "MRF Code created successfully."
        },
    
        // GET MRF_CODE_BY_ID
        fetchMultiPartyFundCode(state, action) {
          state.MultiPartyFundsCode = action.payload;
          state.MPFsuccess = null
        },
    
        // GET ALL MRF_CODE
        fetchMRFCodesSuccess(state, action) {
          state.MultiPartyFundsCode = action.payload;
          state.MPFsuccess = null
        },
    
        // UPDATE MRF_CODE
        updateMultiPartyFundCode(state, action) {
          state.MPFsuccess = "MRF Code updated successfully."
        },
    
        // DELETE MRF_CODE
        deleteMultiPartyFundCode(state, action) {
          state.MPFsuccess = "MRF Code deleted successfully."
        },
            // GET USER_Type_LIST
            fetchUserTypeListSuccess(state, action) {
                state.selectMultiPartyFundsCode= action.payload;
                state.MPFsuccess = null
            },
          // GET ALL MRF_CODE
          fetchMultiPartyFundsSuccess(state, action) {
          state.MultiPartyFundsCodeList = action.payload;
          state.MPFsuccess = null
        },
    }})

    // Reducer
    export default slice.reducer;
    
    // ----------------------------------------------------------------------
    
    /**
     * TO INITIAL STATE
     * @returns 
     */
    export function toInitialState() {
      return async () => {
        dispatch(slice.actions.hasInitialState())
      }
    }



/**
     * GET ALL USER_TypeS
     * @param listParameters 
     * @returns 
     */
export function fetchMultiPartyFundsCodeList() {
    return async () => {
      dispatch(slice.actions.startLoading());
  
      try {
        const response = await axiosServices.get('/base-para/multi-party-fund-code/all');
        dispatch(slice.actions.fetchUserTypeListSuccess(response.data));
      } catch (error) {
        dispatch(slice.actions.hasError(error));
      } finally {
        dispatch(slice.actions.finishLoading());
      }
    };
  }


        /**
     * GET ALL MRF_CODE
     * @param listParameters 
     * @returns 
     */
        export function fetchMultiPartyFundDescs(listParameters: listParametersType) {
          return async () => {
            dispatch(slice.actions.startLoading());
        
            try {
              const response = await axiosServices.get('/base-para/multi-party-fund-code/tbl', { params: listParameters });
              dispatch(slice.actions.fetchMultiPartyFundsSuccess(response.data));
            } catch (error) {
              dispatch(slice.actions.hasError(error));
            } finally {
              dispatch(slice.actions.finishLoading());
            }
          };
        }


                /**
     * GET MRF_CODE
     * @param MultiPartyFundCodePartyId  
     * @returns 
     */
                export function fetchMultiPartyFundCodeParty(MultiPartyFundCodePartyId: number) {
                  return async () => {
                    dispatch(slice.actions.startLoading());
                
                    try {
                      const response = await axiosServices.get(`/base-para/multi-party-fund-code/get/${MultiPartyFundCodePartyId}`);
                      dispatch(slice.actions.fetchMRFCodesSuccess(response.data));
                    } catch (error) {
                      dispatch(slice.actions.hasError(error));
                    } finally {
                      dispatch(slice.actions.finishLoading());
                    }
                  };
                }
                    /**
                 * POST MRF_CODE
                 * @param newMultiPartyFundCodeParty 
                 * @returns 
                 */
                    export function addMultiPartyFundCodeParty(newMultiPartyFundCodeParty: MultiPartyFundsCodeResType) {
                      return async () => {
                        dispatch(slice.actions.startLoading());
                    
                        try {
                          const response = await axiosServices.post('/base-para//multi-party-fund-code/add', newMultiPartyFundCodeParty);
                          dispatch(slice.actions.addMultiPartyFundCodeSuccess(response.data));
                        } catch (error) {
                          dispatch(slice.actions.hasError(error));
                        } finally {
                          dispatch(slice.actions.finishLoading());
                        }
                      };
                    }
                     
                /**
                 * UPDATE MRF_CODE
                 * @param updateMultiPartyFundCodeParty
                 * @returns 
                 */
                export function updateMultiPartyFundCodeParty(updatedMultiPartyFundCodeParty: MultiPartyFundsCodesType) {
                  return async () => {
                    dispatch(slice.actions.startLoading());
                
                    try {
                      const response = await axiosServices.put(`/base-para/multi-party-fund-code/update/${updatedMultiPartyFundCodeParty.multiPartyFundsCodeId}`, updatedMultiPartyFundCodeParty);
                      dispatch(slice.actions.updateMultiPartyFundCode(response.data));
                    } catch (error) {
                      dispatch(slice.actions.hasError(error));
                    } finally {
                      dispatch(slice.actions.finishLoading());
                    }
                  };
                }
                
                /**
                 * DELETE MRF_CODE
                 * @param MultiPartyFundCodePartyId 
                 * @returns 
                 */
                export function deleteMultiPartyFundCodeParty(MultiPartyFundCodePartyId: number) {
                  return async () => {
                    dispatch(slice.actions.startLoading());
                
                    try {
                      await axiosServices.delete(`/base-para/multi-party-fund-code/delete/${MultiPartyFundCodePartyId}`);
                      dispatch(slice.actions.deleteMultiPartyFundCode(MultiPartyFundCodePartyId));
                    } catch (error) {
                      dispatch(slice.actions.hasError(error));
                    } finally {
                      dispatch(slice.actions.finishLoading());
                    }
                  };
                }      