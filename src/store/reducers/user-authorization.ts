// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types
import { DefaultRootStateProps, queryParamsProps } from 'types/user-authorization';
import { UserPostReq } from 'types/user-authorization';

// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['users'] = {
    error: null,
    success: null,
    users: null,
    userList: null,
    isLoading: false,
    isActionSuccess: null
};

const slice = createSlice({
    name: 'users',
    initialState,
    reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.success = null;
            state.isLoading = false;
        },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
        },

        startLoading(state) {
            state.isLoading = true;
        },

        finishLoading(state) {
            state.isLoading = false;
        },
        resetIsActionState(state) {
            state.isActionSuccess = null;
        },

        // APPROVE user
        approveAuthorizationSuccess(state, action) {
            state.success = "User Approved Successfully."
            state.isActionSuccess = "APPROVE"
        },

        // REJECT user
        rejectAuthorizationSuccess(state, action) {
            state.error = action.payload;
            state.isActionSuccess = "REJECT"
        },

        // GET ALL users  
        fetchUsersSuccess(state, action) {
            state.userList = action.payload;
            state.success = null
            state.isActionSuccess = "LIST"
        },

    }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
    return async () => {
        dispatch(slice.actions.hasInitialState())
    }
}
export function toResetIsActionSuccessState() {
    return async () => {
        dispatch(slice.actions.resetIsActionState())
    }
  }
/**
 * APPROVE user
 * @param queryParams 
 * @returns 
 */
export function addAuthorization(queryParams: UserPostReq) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.post(`/user-management/user/${queryParams?.userId}/authorize/${queryParams?.status}`);
            dispatch(slice.actions.approveAuthorizationSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}

/**
 * Reject user
 * @param queryParams 
 * @returns 
 */
export function rejectAuthorization(queryParams: UserPostReq) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.post(`/user-management/user/${queryParams?.userId}/authorize/${queryParams?.status}`);
            dispatch(slice.actions.rejectAuthorizationSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
            // if(error === "User Restricted"){
            //     dispatch(slice.actions.rejectAuthorizationSuccess); 
            // }
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}

/**
 * GET ALL users
 * @param queryParams 
 * @returns 
 */
export function fetchUsersSuccess(queryParams: queryParamsProps) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get('/user-management/getUsersList', { params: queryParams });
            dispatch(slice.actions.fetchUsersSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}