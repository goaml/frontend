// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, EntityLegalFormTypeReqType, listParametersType, EntityLegalFormTypesType } from 'types/entity-legal-form-type';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['entityLegalFormType'] = {
  error: null,
  success: null,
  isLoading: false,
  entityLegalFormTypeList: null,
  entityLegalFormType: null,
  isActionSuccess: null
};

const slice = createSlice({
  name: 'entityLegalFormType',
  initialState,
  reducers: {
    // TO INITIAL STATE
    hasInitialState(state) {
      state.error = null;
      state.success = null;
      state.isLoading = false;
    },

    // HAS ERROR
    hasError(state, action) {
      state.error = action.payload;
    },

    startLoading(state) {
      state.isLoading = true;
    },

    finishLoading(state) {
      state.isLoading = false;
    },
    resetIsActionState(state) {
      state.isActionSuccess = null;
  },

    // POST Account_TYPE 
    addEntityLegalFormTypeSuccess(state, action) {
      state.success = "Entity Legal Form Type created successfully."
      state.isActionSuccess = "CREATE"
    },

    // GET Account_TYPE_BY_ID
    fetchEntityLegalFormTypeSuccess(state, action) {
      state.entityLegalFormType = action.payload;
      state.success = null
    },

    // GET ALL Account_TYPE
    fetchEntityLegalFormTypesSuccess(state, action) {
      state.entityLegalFormTypeList = action.payload;
      state.success = null
      state.isActionSuccess = "LIST"
    },

    // UPDATE Account_TYPE
    updateEntityLegalFormTypeSuccess(state, action) {
      state.success = "Entity Legal Form Type updated successfully."
      state.isActionSuccess = "UPDATE"
    },

    // DELETE Account_TYPE
    deleteEntityLegalFormTypeSuccess(state, action) {
      state.success = "Entity Legal Form Type deleted successfully."
      state.isActionSuccess = "INACTIVE"
    },

  }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
  return async () => {
    dispatch(slice.actions.hasInitialState())
  }
}


/* TO INITIAL STATE
* @returns 
*/
export function toResetIsActionSuccessState() {
  return async () => {
      dispatch(slice.actions.resetIsActionState())
  }
}

/**
 * POST Account_TYPE
 * @param newEntityLegalFormType 
 * @returns 
 */
export function addEntityLegalFormType(newEntityLegalFormType: EntityLegalFormTypeReqType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.post('/base-para/entity-legal-form-type', newEntityLegalFormType);
      dispatch(slice.actions.addEntityLegalFormTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET Account_TYPE
 * @param EntityLegalFormTypeId  
 * @returns 
 */
export function fetchEntityLegalFormType(EntityLegalFormTypeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get(`/base-para/entity-legal-form-type/${EntityLegalFormTypeId}`);
      dispatch(slice.actions.fetchEntityLegalFormTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET ALL Account_TYPE
 * @param listParameters 
 * @returns 
 */
export function fetchEntityLegalFormTypes(listParameters: listParametersType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/base-para/entity-legal-form-type/tbl', { params: listParameters });
      dispatch(slice.actions.fetchEntityLegalFormTypesSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * UPDATE Account_TYPE
 * @param updateEntityLegalFormType
 * @returns 
 */
export function updateEntityLegalFormType(updatedEntityLegalFormType: EntityLegalFormTypesType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.put(`/base-para/entity-legal-form-type/${updatedEntityLegalFormType.entityLegalFormTypeId}`, updatedEntityLegalFormType);
      dispatch(slice.actions.updateEntityLegalFormTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * DELETE Account_TYPE
 * @param EntityLegalFormTypeId 
 * @returns 
 */
export function deleteEntityLegalFormType(EntityLegalFormTypeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      await axiosServices.delete(`/base-para/entity-legal-form-type/${EntityLegalFormTypeId}`);
      dispatch(slice.actions.deleteEntityLegalFormTypeSuccess(EntityLegalFormTypeId));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}
