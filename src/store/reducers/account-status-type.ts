// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, AccountStatusTypeReqType, listParametersType, AccountStatusTypesType } from 'types/account-status-type';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['accountStatusType'] = {
  error: null,
  success: null,
  isLoading: false,
  accountStatusTypeList: null,
  accountStatusType: null,
  isActionSuccess: null
};

const slice = createSlice({
  name: 'accountStatusType',
  initialState,
  reducers: {
    // TO INITIAL STATE
    hasInitialState(state) {
      state.error = null;
      state.success = null;
      state.isLoading = false;
    },

    // HAS ERROR
    hasError(state, action) {
      state.error = action.payload;
    },

    startLoading(state) {
      state.isLoading = true;
    },

    finishLoading(state) {
      state.isLoading = false;
    },
    resetIsActionState(state) {
      state.isActionSuccess = null;
  },

    // POST ACCOUNT_TYPE 
    addAccountStatusTypeSuccess(state, action) {
      state.success = "Account Status Type created successfully."
      state.isActionSuccess = "CREATE"
    },

    // GET ACCOUNT_TYPE_BY_ID
    fetchAccountStatusTypeSuccess(state, action) {
      state.accountStatusType = action.payload;
      state.success = null
    },

    // GET ALL ACCOUNT_TYPE
    fetchAccountStatusTypesSuccess(state, action) {
      state.accountStatusTypeList = action.payload;
      state.success = null
      state.isActionSuccess = "LIST"
    },

    // UPDATE ACCOUNT_TYPE
    updateAccountStatusTypeSuccess(state, action) {
      state.success = "Account Status Type updated successfully."
      state.isActionSuccess = "UPDATE"
    },

    // DELETE ACCOUNT_TYPE
    deleteAccountStatusTypeSuccess(state, action) {
      state.success = "Account Status Type deleted successfully."
      state.isActionSuccess = "DELETE"
    },

  }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
  return async () => {
    dispatch(slice.actions.hasInitialState())
  }
}
      /**
   * TO INITIAL STATE
   * @returns 
   */
      export function toResetIsActionSuccessState() {
        return async () => {
            dispatch(slice.actions.resetIsActionState())
        }
      }

/**
 * POST ACCOUNT_TYPE
 * @param newAccountStatusType 
 * @returns 
 */
export function addAccountStatusType(newAccountStatusType: AccountStatusTypeReqType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.post('/base-para/account-status-type', newAccountStatusType);
      dispatch(slice.actions.addAccountStatusTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET ACCOUNT_TYPE
 * @param AccountStatusTypeId  
 * @returns 
 */
export function fetchAccountStatusType(AccountStatusTypeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get(`/base-para/account-status-type/${AccountStatusTypeId}`);
      dispatch(slice.actions.fetchAccountStatusTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET ALL ACCOUNT_TYPE
 * @param listParameters 
 * @returns 
 */
export function fetchAccountStatusTypes(listParameters: listParametersType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/base-para/account-status-type/tbl', { params: listParameters });
      dispatch(slice.actions.fetchAccountStatusTypesSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * UPDATE ACCOUNT_TYPE
 * @param updateAccountStatusType
 * @returns 
 */
export function updateAccountStatusType(updatedAccountStatusType: AccountStatusTypesType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.put(`/base-para/account-status-type/${updatedAccountStatusType.accountStatusTypeId}`, updatedAccountStatusType);
      dispatch(slice.actions.updateAccountStatusTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * DELETE ACCOUNT_TYPE
 * @param AccountStatusTypeId 
 * @returns 
 */
export function deleteAccountStatusType(AccountStatusTypeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      await axiosServices.delete(`/base-para/account-status-type/${AccountStatusTypeId}`);
      dispatch(slice.actions.deleteAccountStatusTypeSuccess(AccountStatusTypeId));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}
