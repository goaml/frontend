// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types
import { DefaultRootStateProps, RolePostReq, queryParamsProps } from 'types/role';

// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['roles'] = {
    error: null,
    success: null,
    roles: null,
    roleList: null,
    isLoading: false,
    selectRoles: null,
    selectUserRoles: null,
    isActionSuccess:null
};

const slice = createSlice({
    name: 'roles',
    initialState,
    reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.success = null;
            state.isLoading = false;
        },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
        },

        startLoading(state) {
            state.isLoading = true;
        },

        finishLoading(state) {
            state.isLoading = false;
        },

        resetIsActionState(state) {
            state.isActionSuccess = null;
        },
        // POST ROLES
        addRoleSuccess(state, action) {
            state.success = "Role created successfully."
            state.isActionSuccess = "CREATE"
        },

        // GET ROLES
        fetchRoleSuccess(state, action) {
            state.roles = action.payload;
            state.success = null
        },

        // GET ALL ROLE 
        fetchRolesSuccess(state, action) {
            state.roleList = action.payload;
            state.success = null
            state.isActionSuccess = "LIST"
        },

        // UPDATE ROLE
        updateRoleSuccess(state, action) {
            state.success = "Role updated successfully."
            state.isActionSuccess = "UPDATE"
        },

        // DELETE ROLE
        deleteRoleSuccess(state, action) {
            state.success = "Role deleted successfully."
            state.isActionSuccess = "INACTIVE"
        },

        //get all roles
        fetchGetAllRolesSuccess(state, action) {
            state.selectRoles = action.payload?.result;
            state.success = null;
           
        },

        //get all roles for users
        fetchGetAllUserRolesSuccess(state, action) {
            state.selectUserRoles = action.payload;
            state.success = null;
        },
    }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
    return async () => {
        dispatch(slice.actions.hasInitialState())
    }
}

export function toResetIsActionSuccessState() {
    return async () => {
        dispatch(slice.actions.resetIsActionState())
    }
  }


/**
 * POST ROLE
 * @param newRole 
 * @returns 
 */
export function addRoleSuccess(newRole: RolePostReq) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.post('/user-management/createUserRole', newRole);
            dispatch(slice.actions.addRoleSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}

/**
 * GET ALL ROLE
 * @param queryParams 
 * @returns 
 */
export function fetchRolesSuccess(queryParams: queryParamsProps) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get('/user-management/roleView', { params: queryParams });
            dispatch(slice.actions.fetchRolesSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}

/**
 * UPDATE ROLE
 * @param updatedRole
 * @returns 
 */
export function updateRoleSuccess(updatedRole: RolePostReq) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.put(`/user-management/updateUserRole?id=${updatedRole.userRoleId}`, updatedRole);
            dispatch(slice.actions.updateRoleSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}

/**
 * DELETE ROLE
 * @param userRoleId 
 * @returns 
 */
export function deleteRoleSuccess(userRoleId: number) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            await axiosServices.delete(`/user-management/deleteUserRole/${userRoleId}`);
            dispatch(slice.actions.deleteRoleSuccess(userRoleId));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}
/**
 * GET ALL Roles for dropdown
 * @param queryParams 
 * @returns 
 */
export function fetchSelectRoles(queryParams: queryParamsProps) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get('/user-management/roleView', { params: queryParams });
            dispatch(slice.actions.fetchGetAllRolesSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}

/**
* GET ALL Roles for Users Dropdown 
* @returns 
*/

export function fetchUserSelectRoles() {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get('/user-management/userRole/fdd');
            dispatch(slice.actions.fetchGetAllUserRolesSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}
