	// third-party
    import { createSlice } from '@reduxjs/toolkit';

    // project imports
    import { axiosServices } from 'utils/axios';
    import { dispatch } from '../index';
    
    // types 
    import { DefaultRootStateProps, listParametersType,  } from 'types/transaction-inquiry';
    
    
    
    // ----------------------------------------------------------------------
    
    const initialState: DefaultRootStateProps['transactionInquiry'] = {
        error: null,
        success: null,
        isLoading: false,
        transactionInquiryList: null,
    };
    
    const slice = createSlice({
      name: 'transactionInquiry',
      initialState,
      reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
          state.error = null;
          state.success = null;
          state.isLoading = false;
        },
    
        // HAS ERROR
        hasError(state, action) {
          state.error = action.payload;
        },
    
        startLoading(state) {
          state.isLoading = true;
        },
    
        finishLoading(state) {
          state.isLoading = false;
        },
    
        // GET ALL Transaction_Inquiry
        fetchTransactionCodesSuccess(state, action) {
          state.transactionInquiryList = action.payload;
          state.success = null
        },
    
      }
    });
    
    // Reducer
    export default slice.reducer;
    
    // ----------------------------------------------------------------------
    
    /**
     * TO INITIAL STATE
     * @returns 
     */
    export function toInitialState() {
      return async () => {
        dispatch(slice.actions.hasInitialState())
      }
    }
    

    /**
     * GET ALL Transaction_Inquiry
     * @param listParameters 
     * @returns 
     */
    export function fetchTransactionInquires(listParameters: listParametersType) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.get('/transaction/daily-tran-table/tbl', { params: listParameters });
          dispatch(slice.actions.fetchTransactionCodesSuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
    