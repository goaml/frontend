// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, EntityPersonRoleTypeReqType, listParametersType, EntityPersonRoleTypesType } from 'types/entity-person-role';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['entityPersonRoleType'] = {
  error: null,
  success: null,
  isLoading: false,
  entityPersonRoleTypeList: null,
  entityPersonRoleType: null,
  isActionSuccess: null
};

const slice = createSlice({
  name: 'entityPersonRoleTypeType',
  initialState,
  reducers: {
    // TO INITIAL STATE
    hasInitialState(state) {
      state.error = null;
      state.success = null;
      state.isLoading = false;
    },

    // HAS ERROR
    hasError(state, action) {
      state.error = action.payload;
    },

    startLoading(state) {
      state.isLoading = true;
    },

    finishLoading(state) {
      state.isLoading = false;
    },
    resetIsActionState(state) {
      state.isActionSuccess = null;
  },

    // POST EntityPersonRoleType_TYPE 
    addEntityPersonRoleTypeTypeSuccess(state, action) {
      state.success = "Entity Person Role Type created successfully."
      state.isActionSuccess = "CREATE"
    },

    // GET EntityPersonRoleType_TYPE_BY_ID
    fetchEntityPersonRoleTypeTypeSuccess(state, action) {
      state.entityPersonRoleType = action.payload;
      state.success = null
    },

    // GET ALL EntityPersonRoleType_TYPE
    fetchEntityPersonRoleTypeTypesSuccess(state, action) {
      state.entityPersonRoleTypeList = action.payload;
      state.success = null
      state.isActionSuccess = "LIST"
    },

    // UPDATE EntityPersonRoleType_TYPE
    updateEntityPersonRoleTypeTypeSuccess(state, action) {
      state.success = "Entity Person Role Type updated successfully."
      state.isActionSuccess = "UPDATE"
    },

    // DELETE EntityPersonRoleType_TYPE
    deleteEntityPersonRoleTypeTypeSuccess(state, action) {
      state.success = "Entity Person Role Type deleted successfully."
      state.isActionSuccess = "INACTIVE"
    },

  }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
  return async () => {
    dispatch(slice.actions.hasInitialState())
  }
}

/* TO INITIAL STATE
* @returns 
*/
export function toResetIsActionSuccessState() {
  return async () => {
      dispatch(slice.actions.resetIsActionState())
  }
}

/**
 * POST EntityPersonRoleType_TYPE
 * @param newEntityPersonRoleTypeType 
 * @returns 
 */
export function addEntityPersonRoleTypeType(newEntityPersonRoleTypeType: EntityPersonRoleTypeReqType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.post('/base-para/entity-person-role-types', newEntityPersonRoleTypeType);
      dispatch(slice.actions.addEntityPersonRoleTypeTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET EntityPersonRoleType_TYPE
 * @param EntityPersonRoleTypeTypeId  
 * @returns 
 */
export function fetchEntityPersonRoleTypeType(EntityPersonRoleTypeTypeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get(`/base-para/entity-person-role-types/${EntityPersonRoleTypeTypeId}`);
      dispatch(slice.actions.fetchEntityPersonRoleTypeTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET ALL EntityPersonRoleType_TYPE
 * @param listParameters 
 * @returns 
 */
export function fetchEntityPersonRoleTypeTypes(listParameters: listParametersType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/base-para/entity-person-role-types/tbl', { params: listParameters });
      dispatch(slice.actions.fetchEntityPersonRoleTypeTypesSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * UPDATE EntityPersonRoleType_TYPE
 * @param updateEntityPersonRoleTypeType
 * @returns 
 */
export function updateEntityPersonRoleTypeType(updatedEntityPersonRoleTypeType: EntityPersonRoleTypesType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.put(`/base-para/entity-person-role-types/${updatedEntityPersonRoleTypeType.entityPersonRoleTypeId}`, updatedEntityPersonRoleTypeType);
      dispatch(slice.actions.updateEntityPersonRoleTypeTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * DELETE EntityPersonRoleType_TYPE
 * @param EntityPersonRoleTypeTypeId 
 * @returns 
 */
export function deleteEntityPersonRoleTypeType(EntityPersonRoleTypeTypeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      await axiosServices.delete(`/base-para/entity-person-role-types/${EntityPersonRoleTypeTypeId}`);
      dispatch(slice.actions.deleteEntityPersonRoleTypeTypeSuccess(EntityPersonRoleTypeTypeId));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}
