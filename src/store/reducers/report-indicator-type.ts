// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, ReportIndicatorTypeReqType, ReportIndicatorTypesType, listParametersType} from 'types/report-indicator-type';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['reportIndicatorType'] = {
  error: null,
  success: null,
  isLoading: false,
  reportIndicatorTypeList: null,
  reportIndicatorType: null,
  selectreportIndicatorType: null,
  xmlData: null,
  isActionSuccess: null
};

const slice = createSlice({
    name: 'report',
    initialState,
    reducers: {


        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.success = null;
            state.isLoading = false;
          },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
          },
      
          startLoading(state) {
            state.isLoading = true;
          },
      
          finishLoading(state) {
            state.isLoading = false;
          },

          resetIsActionState(state) {
            state.isActionSuccess = null;
        },
            // GET USER_Type_LIST
            fetchReportIndicatorTypeListSuccess(state, action) {
                state.selectreportIndicatorType= action.payload;
                state.success = null
            },

                             // POST REPORT_TYPE 
        addReportIndicatorTypeSuccess(state, action) {
          state.success = "Report Indicator Type created successfully."
          state.isActionSuccess = "CREATE"
        },
    
        // GET REPORT_TYPE_BY_ID
        fetchReportIndicatorType(state, action) {
          state.reportIndicatorType = action.payload;
          state.success = null
        },
    
        // GET ALL REPORT_TYPE
        fetchMPRCodesSuccess(state, action) {
          state.reportIndicatorType = action.payload;
          state.success = null
        },
    
        // UPDATE REPORT_TYPE
        updateReportIndicatorType(state, action) {
          state.success = "Report Indicator Type updated successfully."
          state.isActionSuccess = "UPDATE"
        },
    
        // DELETE REPORT_TYPE
        deleteReportIndicatorType(state, action) {
          state.success = "Report Indicator Type deleted successfully."
          state.isActionSuccess = "INACTIVE"
        },

        // GET ALL REPORT_TYPE
          fetchReportsSuccess(state, action) {
            state.reportIndicatorTypeList = action.payload;
            state.success = null
            state.isActionSuccess = "LIST"
          },
          // FETCH XML DATA
    fetchXmlDataSuccess(state, action) {
      state.xmlData = action.payload;
      state.success = null;
    },
    }})

    // Reducer
    export default slice.reducer;
    
    // ----------------------------------------------------------------------
    
    /**
     * TO INITIAL STATE
     * @returns 
     */
    export function toInitialState() {
      return async () => {
        dispatch(slice.actions.hasInitialState())
      }
    }
/* TO INITIAL STATE
* @returns 
*/
export function toResetIsActionSuccessState() {
  return async () => {
      dispatch(slice.actions.resetIsActionState())
  }
}


/**
     * GET ALL REPORT_TYPES
     * @param listParameters 
     * @returns 
     */
export function fetchReportIndicatorTypeList() {
    return async () => {
      dispatch(slice.actions.startLoading());
  
      try {
        const response = await axiosServices.get('/base-para/report-indicator-type/all');
        dispatch(slice.actions.fetchReportIndicatorTypeListSuccess(response.data));
      } catch (error) {
        dispatch(slice.actions.hasError(error));
      } finally {
        dispatch(slice.actions.finishLoading());
      }
    };
  }


          /**
     * GET ALL REPORT_TYPE
     * @param listParameters 
     * @returns 
     */
          export function fetchReports(listParameters: listParametersType) {
            return async () => {
              dispatch(slice.actions.startLoading());
          
              try {
                const response = await axiosServices.get('/base-para/report-indicator-type/tbl', { params: listParameters });
                dispatch(slice.actions.fetchReportsSuccess(response.data));
              } catch (error) {
                dispatch(slice.actions.hasError(error));
              } finally {
                dispatch(slice.actions.finishLoading());
              }
            };
          }

               /**
     * GET REPORT_TYPE
     * @param ReportIndicatorTypeId  
     * @returns 
     */
               export function fetchReportIndicatorType(ReportIndicatorTypeId: number) {
                return async () => {
                  dispatch(slice.actions.startLoading());
              
                  try {
                    const response = await axiosServices.get(`/base-para/report-indicator-type/get/${ReportIndicatorTypeId}`);
                    dispatch(slice.actions.fetchMPRCodesSuccess(response.data));
                  } catch (error) {
                    dispatch(slice.actions.hasError(error));
                  } finally {
                    dispatch(slice.actions.finishLoading());
                  }
                };
              }
                  /**
               * POST REPORT_TYPE
               * @param newReportIndicatorType 
               * @returns 
               */
                  export function addReportIndicatorType(newReportIndicatorType: ReportIndicatorTypeReqType) {
                    return async () => {
                      dispatch(slice.actions.startLoading());
                  
                      try {
                        const response = await axiosServices.post('/base-para/report-indicator-type', newReportIndicatorType);
                        dispatch(slice.actions.addReportIndicatorTypeSuccess(response.data));
                      } catch (error) {
                        dispatch(slice.actions.hasError(error));
                      } finally {
                        dispatch(slice.actions.finishLoading());
                      }
                    };
                  }
                   
              /**
               * UPDATE REPORT_TYPE
               * @param updateReportIndicatorType
               * @returns 
               */
              export function updateReportIndicatorType(updatedReportIndicatorType: ReportIndicatorTypesType) {
                return async () => {
                  dispatch(slice.actions.startLoading());
              
                  try {
                    const response = await axiosServices.put(`/base-para/report-indicator-type/${updatedReportIndicatorType.reportIndicatorTypeId}`, updatedReportIndicatorType);
                    dispatch(slice.actions.updateReportIndicatorType(response.data));
                  } catch (error) {
                    dispatch(slice.actions.hasError(error));
                  } finally {
                    dispatch(slice.actions.finishLoading());
                  }
                };
              }
              
              /**
               * DELETE REPORT_TYPE
               * @param ReportIndicatorTypeId 
               * @returns 
               */
              export function deleteReportIndicatorType(ReportIndicatorTypeId: number) {
                return async () => {
                  dispatch(slice.actions.startLoading());
              
                  try {
                    await axiosServices.delete(`/base-para/report-indicator-type/${ReportIndicatorTypeId}`);
                    dispatch(slice.actions.deleteReportIndicatorType(ReportIndicatorTypeId));
                  } catch (error) {
                    dispatch(slice.actions.hasError(error));
                  } finally {
                    dispatch(slice.actions.finishLoading());
                  }
                };
              }


              export function fetchReport(reportIndicatorType: string) {
                return async () => {
                  dispatch(slice.actions.startLoading());
              
                  try {
                    const response = await axiosServices.get(`/base/xml/download?reportIndicatorType=${reportIndicatorType}`, {
                      responseType: 'blob'
                    });
                    const xmlFile = new Blob([response.data], { type: 'application/xml' });
              
                    const downloadLink = document.createElement('a');
                    downloadLink.href = window.URL.createObjectURL(xmlFile);
                    downloadLink.download = 'report.xml';
                    downloadLink.click();
              
                    // If you need to dispatch the fetched XML data
                    // dispatch(slice.actions.fetchXmlDataSuccess(response.data));
                  } catch (error) {
                    dispatch(slice.actions.hasError(error));
                  } finally {
                    dispatch(slice.actions.finishLoading());
                  }
                };
              }