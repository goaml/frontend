// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import axiosServices from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, PostUserReqType, listParametersType, updateUserType } from 'types/user';


// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['users'] = {
  error: null,
  success: null,
  isLoading: false,
  userList: null,
  user: null,
  users: null,
  roles: null,
  EditProfileSuccess: null,
  userById: null,
  selectUsers: null,
  userDefaultBranchById: null,
  isActionSuccess:null
};

const slice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    // TO INITIAL STATE
    hasInitialState(state) {
      state.error = null;
      state.success = null;
      state.isLoading = false;
    },

    // HAS ERROR
    hasError(state, action) {
      state.error = action.payload;
    },

    startLoading(state) {
      state.isLoading = true;
    },

    finishLoading(state) {
      state.isLoading = false;
    },
    resetIsActionState(state) {
      state.isActionSuccess = null;
  },

    // POST USER_ 
    addUserSuccess(state, action) {
      state.success = "User  created successfully."
      state.isActionSuccess = "CREATE"
    },

    // GET USER__BY_ID
    fetchUserSuccess(state, action) {
      state.user = action.payload;
      state.success = null
    },

    // GET ALL USER_
    fetchUsersSuccess(state, action) {
      state.userList = action.payload;
      state.success = null
      state.isActionSuccess = "LIST"
    },

    // GET USER BY ID
    GetByUserSuccess(state, action) {
      state.userById = action.payload;
      state.success = null
    },

    // UPDATE USER_
    updateUserSuccess(state, action) {
      state.success = "User updated successfully."
      state.EditProfileSuccess = "User updated successfully."
      state.isActionSuccess = "UPDATE"
    },

    // DELETE USER_
    deleteUserSuccess(state, action) {
      state.success = "User In-Active successfully."
      state.isActionSuccess = "INACTIVE"
    },

    //get all user
    fetchGetAllUserSuccess(state, action) {
      state.selectUsers = action.payload?.result;
      state.success = null;
    },

    // Get Default Branch
    GetByUserDefaultBranchSuccess(state, action) {
      state.userDefaultBranchById = action.payload;
      state.success = null
    },
  }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
  return async () => {
    dispatch(slice.actions.hasInitialState())
  }
}

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toResetIsActionSuccessState() {
  return async () => {
      dispatch(slice.actions.resetIsActionState())
  }
}
/**
 * POST USER_
 * @param newUser 
 * @returns 
 */
export function addUser(newUser: PostUserReqType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      // const response = await axiosServices.post(`/auth/${newUser.userTypeId}/register`, newUser);
      // dispatch(slice.actions.addUserSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET USER_
 * @param UserId  
 * @returns 
 */
export function fetchUser(UserId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      // const response = await axiosServices.get(`/user-management/cluster/${UserId}`);
      // dispatch(slice.actions.fetchUserSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET ALL USER_
 * @param listParameters 
 * @returns 
 */
export function fetchUsers(listParameters: listParametersType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/user-management/get-all-users', { params: listParameters });
      dispatch(slice.actions.fetchUsersSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * UPDATE USER_
 * @param updateUser
 * @returns 
 */
export function updateUser(updatedUser: updateUserType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      // const response = await axiosServices.put(`/user-management/user/${updatedUser.userId}/updateUser`, updatedUser);
      // dispatch(slice.actions.updateUserSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * DELETE USER_
 * @param UserId 
 * @returns 
 */
export function deleteUser(UserId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      // await axiosServices.delete(`/user-management/user/inactivate/${UserId}`);
      // dispatch(slice.actions.deleteUserSuccess(UserId));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET USER BY ID
 * @param id
 * @returns
 */
export function getUserById(userId: string) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      // const response = await axiosServices.get(`/user-management/adMUser/${userId}`);
      // dispatch(slice.actions.GetByUserSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
* UPDATE USER PROFILE
* @param updatedUser 
* @returns 
*/
export function updateUserSuccess(updatedUser: PostUserReqType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      // const response = await axiosServices.put(`/user-management/adMUser/${updatedUser.userId}`, updatedUser);
      // dispatch(slice.actions.updateUserSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET ALL Users for dropdown
 * @param queryParams 
 * @returns 
 */
export function fetchGetAllUserSuccess(queryParams: listParametersType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      // const response = await axiosServices.get('/user-management/getUsers', { params: queryParams });
      // dispatch(slice.actions.fetchGetAllUserSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}


/**
 * GET USER Default Branch
 * @param id
 * @returns
 */
export function getDefaultBranchUserById(userId: string) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      // const response = await axiosServices.get(`/user-management/user/getUser/${userId}`);
      // dispatch(slice.actions.GetByUserDefaultBranchSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}


