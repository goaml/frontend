// third-party
import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

// project import
import chat from './chat';
import calendar from './calendar';
import menu from './menu';
import snackbar from './snackbar';
import productReducer from './product';
import cartReducer from './cart';
import kanban from './kanban';
import invoice from './invoice';
import multipleBranches from './multiple-branch';
import branch from './branch';
import user from './user';
import users from './users';
import userType from './user-type';
import userRole from './user-role';
import roles from './role';
import userAuthorization from './user-authorization';
import roleRestriction from './role-restrictions';
import userRestriction from './user-restrictions';
import menuActions from './menu-action';
import passwordChange from './password-change';
import noteLog from './note-log';
import department from './department'
import globalDetails from './global-details';
import rptCode from './rpt-code';
import category from './category';
import fundType from './fund-type';
import fundDesc from './fund-desc';
import multiPartyInvolveParty from './multiParty-Involve-party';
import multiPartyRole from './multiParty-Role';
import multiPartyFundCode from './multyParty-fundCode';
import transactionCode from './transaction-code';
import reportType from './report-type';
import transaction from './goaml-trx-code';
import departmentPara from './department-para';
import branchPara from './branch-para';
import transReport from './trans-report';
import transTransaction from './trans-transaction';
import transFrom from './trans-from';
import transTo from './trans-to';
import transFromMyclient from './trans-from-myclient';
import transToMyclient from './trans-to-myclient';
import accountMyClient from './account-my-client';
import personMyClient from './person-my-client';
import entityMyClient from './entity-my-client';
import accountType from './account-type';
import accountStatusType from './account-status-type';
import identifierType from './identifier-type';
import conductionType from './conduction-type';
import contactType from './contact-type';
import communicationType from './communication-type';
import entityLegalFormType from './entity-legal-form-type';
import submissionType from './submission-type';
import currencyCode from './currency-code';
import accountPersonRole from './account-person-role';
import entityPersonRole from './entity-person-role';
import genderType from './gender-type';
import partyType from './party-type';
import reportIndicatorType from './report-indicator-type';
import statusType from './status-type';
import cityCode from './city-code';
import transAddress from './trans-address';
import transPhone from './trans-phone';
import transPersonIdentification from './trans-person-identification';
import foreignCurrency from './foreign-currency';
import loginType from './login-type';
import transParty from './trans-party';
import userDynamicDrawer from './user-dynamic-drawer';
import uploadData from './upload-data';
import reportTypes from './report-types';
import transactionInquiry from './transaction-Inquiry';
import GLAccountMapping from './gl-account-mapping';
import xmlFileLog from './xml-file-log';
import reportTypeValidate from './report-type-validate'
import transactionErrorLog from './trans-err-log';
import xmlBackupReport from './xml-report-backup'
import transactionErrorSummaryLog from './trans-err-summary-log'
import customerSummaryMonthly from './customer_summary_monthly'
import customer_summary_monthly_trxcode_wise from './customer_summary_monthly_trxcode_wise'
import transactioncode_summary_daily from './transactioncode_summary_daily'
import transactioncode_summary_monthly from './transactioncode_summary_monthly'


// ==============================|| COMBINE REDUCERS ||============================== //

const reducers = combineReducers({
  chat,
  calendar,
  menu,
  snackbar,
  cart: persistReducer(
    {
      key: 'cart',
      storage,
      keyPrefix: 'mantis-ts-'
    },
    cartReducer
  ),
  product: productReducer,
  kanban,
  invoice,
  multipleBranches,
  branch,
  user,
  users,
  department,
  userType,
  userRole,
  roles,
  userAuthorization,
  roleRestriction,
  userRestriction,
  menuActions,
  globalDetails,
  noteLog,
  rptCode,
  category,
  fundType,
  fundDesc,
  multiPartyInvolveParty,
  multiPartyRole,
  multiPartyFundCode,
  transactionCode,
  reportType,
  transaction,
  departmentPara,
  branchPara,
  transReport,
  transTransaction,
  transFrom,
  transTo,
  transFromMyclient,
  transToMyclient,
  passwordChange,
  accountMyClient,
  personMyClient,
  entityMyClient,
  accountType,
  accountStatusType,
  identifierType,
  conductionType,
  contactType,
  communicationType,
  entityLegalFormType,
  submissionType,
  currencyCode,
  accountPersonRole,
  entityPersonRole,
  genderType,
  partyType,
  reportIndicatorType,
  statusType,
  cityCode,
  transAddress,
  transPhone,
  transPersonIdentification,
  foreignCurrency,
  loginType,
  transParty,
  userDynamicDrawer,
  uploadData,
  reportTypes,
  transactionInquiry,
  GLAccountMapping,
  xmlFileLog,
  reportTypeValidate,
  transactionErrorLog,
  xmlBackupReport,
  transactionErrorSummaryLog,
  customerSummaryMonthly,
  customer_summary_monthly_trxcode_wise,
  transactioncode_summary_daily,
  transactioncode_summary_monthly,
});

export default reducers;
