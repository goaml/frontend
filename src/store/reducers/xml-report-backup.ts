// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, queryParamsProps, xmlReportProps, } from 'types/xml-report-backup';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['xmlReport'] = {
    error: null,
    success: null,
    xmlReports: null,
    xmlReport: null,
    isLoading: false,
    downloadBackupXml: null
};

const slice = createSlice({
    name: 'xmlReports',
    initialState,
    reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.success = null;
            state.isLoading = false;
        },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
        },

        startLoading(state) {
            state.isLoading = true;
        },

        finishLoading(state) {
            state.isLoading = false;
        },

        // GET ALL Transaction_Inquiry
        fetchXMLReportSuccess(state, action) {
            state.xmlReports = action.payload;
            state.success = null
        },

        // FETCH downloadBackupXml
        downloadBackupXml(state, action) {
            state.downloadBackupXml = action.payload;
            state.success = null;
        },

    }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
    return async () => {
        dispatch(slice.actions.hasInitialState())
    }
}


/**
 * GET ALL fetchXML
 * @param listParameters 
 * @returns 
 */
export function fetchXmlReport(queryParams: queryParamsProps) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get('/base/xml/xml-report/backup/tbl', { params: queryParams });
            dispatch(slice.actions.fetchXMLReportSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}


/**
 * GET ALL fetch Download
 * @param reportLogId 
 * @returns 
 */
export function fetchdownloadBackupXml(queryParams: xmlReportProps) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get('/base/xml/xml-report/backup/download', { params: queryParams });
            const contentDisposition = response.headers['content-disposition'];

            let fileName = 'report.xml'; // Default fallback name
            if (contentDisposition) {
                const fileNameMatch = contentDisposition.match(/filename="(.+?)"/);
                if (fileNameMatch) {
                    fileName = fileNameMatch[1];
                }
            }


            const xmlFile = new Blob([response.data], { type: 'application/xml' });

            const downloadLink = document.createElement('a');
            downloadLink.href = window.URL.createObjectURL(xmlFile);
            downloadLink.download = fileName;
            downloadLink.click();
            dispatch(slice.actions.downloadBackupXml(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}

