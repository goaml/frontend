// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, AccountTypeReqType, listParametersType, AccountTypesType } from 'types/account-type';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['accountType'] = {
  error: null,
  success: null,
  isLoading: false,
  accountTypeList: null,
  accountType: null,
  isActionSuccess: null
};

const slice = createSlice({
  name: 'accountType',
  initialState,
  reducers: {
    // TO INITIAL STATE
    hasInitialState(state) {
      state.error = null;
      state.success = null;
      state.isLoading = false;
    },

    // HAS ERROR
    hasError(state, action) {
      state.error = action.payload;
    },

    startLoading(state) {
      state.isLoading = true;
    },

    finishLoading(state) {
      state.isLoading = false;
    },
    resetIsActionState(state) {
      state.isActionSuccess = null;
  },

    // POST Account_TYPE 
    addAccountTypeSuccess(state, action) {
      state.success = "Account Type created successfully."
      state.isActionSuccess = "CREATE"
    },

    // GET Account_TYPE_BY_ID
    fetchAccountTypeSuccess(state, action) {
      state.accountType = action.payload;
      state.success = null

    },

    // GET ALL Account_TYPE
    fetchAccountTypesSuccess(state, action) {
      state.accountTypeList = action.payload;
      state.success = null;
      state.isActionSuccess = "LIST"
    },

    // UPDATE Account_TYPE
    updateAccountTypeSuccess(state, action) {
      state.success = "Account Type updated successfully."
      state.isActionSuccess = "UPDATE"
    },

    // DELETE Account_TYPE
    deleteAccountTypeSuccess(state, action) {
      state.success = "Account Type deleted successfully."
      state.isActionSuccess = "INACTIVE"
    },

  }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
  return async () => {
    dispatch(slice.actions.hasInitialState())
  }
}

      /**
   * TO INITIAL STATE
   * @returns 
   */
      export function toResetIsActionSuccessState() {
        return async () => {
            dispatch(slice.actions.resetIsActionState())
        }
      }
    

/**
 * POST Account_TYPE
 * @param newAccountType 
 * @returns 
 */
export function addAccountType(newAccountType: AccountTypeReqType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.post('/base-para/account-type', newAccountType);
      dispatch(slice.actions.addAccountTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET Account_TYPE
 * @param AccountTypeId  
 * @returns 
 */
export function fetchAccountType(AccountTypeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get(`/base-para/account-type/${AccountTypeId}`);
      dispatch(slice.actions.fetchAccountTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET ALL Account_TYPE
 * @param listParameters 
 * @returns 
 */
export function fetchAccountTypes(listParameters: listParametersType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/base-para/account-type/tbl', { params: listParameters });
      dispatch(slice.actions.fetchAccountTypesSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * UPDATE Account_TYPE
 * @param updateAccountType
 * @returns 
 */
export function updateAccountType(updatedAccountType: AccountTypesType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.put(`/base-para/account-type/${updatedAccountType.accountTypeId}`, updatedAccountType);
      dispatch(slice.actions.updateAccountTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * DELETE Account_TYPE
 * @param AccountTypeId 
 * @returns 
 */
export function deleteAccountType(AccountTypeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      await axiosServices.delete(`/base-para/account-type/${AccountTypeId}`);
      dispatch(slice.actions.deleteAccountTypeSuccess(AccountTypeId));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}
