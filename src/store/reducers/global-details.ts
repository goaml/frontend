// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types
import { DefaultRootStateProps, GlobalDetails } from 'types/global-details';

// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['globalDetail'] = {
    error: null,
    success: null,
    globalDetails: null,
    globalDetail: null,
    isLoading: false,
    isActionSuccess: null
};

const slice = createSlice({
    name: 'globalDetail',
    initialState,
    reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.success = null;
            state.isLoading = false;
        },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
        },

        startLoading(state) {
            state.isLoading = true;
        },

        finishLoading(state) {
            state.isLoading = false;
        },
        resetIsActionState(state) {
            state.isActionSuccess = null;
        },
        // GET ALL Global_Details  
        fetchGlobalDetailsSuccess(state, action) {
            state.globalDetails = action.payload;
            state.success = null;
            state.isActionSuccess = "LIST"
        },
        // UPDATE Global Detail
        updateGlobalDetailSuccess(state, action) {
            state.success = "Limit Updated Successfully"
            state.isActionSuccess = "UPDATE"
        },

    }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
    return async () => {
        dispatch(slice.actions.hasInitialState())
    }
}


export function toResetIsActionSuccessState() {
    return async () => {
        dispatch(slice.actions.resetIsActionState())
    }
}

/**
 * GET ALL Global Details
 * @param queryParams 
 * @returns 
 */
export function fetchGlobalDetailsSuccess() {

    const id = 1
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get(`/user-management/global-details/${id}`);
            dispatch(slice.actions.fetchGlobalDetailsSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}

export function fetchGlobalDetailsPass() {
    const id = 1;
    return async (dispatch: (arg0: { payload: any; type: "globalDetail/startLoading" | "globalDetail/fetchGlobalDetailsSuccess" | "globalDetail/hasError" | "globalDetail/finishLoading"; }) => void) => {
        dispatch(slice.actions.startLoading());
        try {
            const response = await axiosServices.get(`/global-details/${id}`);
            dispatch(slice.actions.fetchGlobalDetailsSuccess(response.data));
            return response.data; // Return the response data
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}


/**
 * UPDATE Global Detail
 * @param GlobalDetail
 * @returns 
 */
export function UpdateGlobalDetailLimit(GlobalDetail: GlobalDetails) {
    return async () => {
      dispatch(slice.actions.startLoading());
  
      try {
        const response = await axiosServices.put(`/user-management/global-details/${1}`, GlobalDetail);
        dispatch(slice.actions.updateGlobalDetailSuccess(response.data));
      } catch (error) {
        dispatch(slice.actions.hasError(error));
      } finally {
        dispatch(slice.actions.finishLoading());
      }
    };
  }
  