// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, TransactionReqType, exportData, listParametersType } from 'types/goaml-trx-code';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['transaction'] = {
  error: null,
  trsuccess: null,
  isLoading: false,
  transactionList: null,
  transaction: null,
  selectTransactionCodeType: null,
  isActionSuccess: null
};

const slice = createSlice({
  name: 'transaction',
  initialState,
  reducers: {
    // TO INITIAL STATE
    hasInitialState(state) {
      state.error = null;
      state.trsuccess = null;
      state.isLoading = false;
    },

    // HAS ERROR
    hasError(state, action) {
      state.error = action.payload;
    },

    startLoading(state) {
      state.isLoading = true;
    },

    finishLoading(state) {
      state.isLoading = false;
    },
    resetIsActionState(state) {
      state.isActionSuccess = null;
    },
    // POST TRANSACTION 
    addTransactionSuccess(state, action) {
      state.trsuccess = "Transaction created successfully."
      state.isActionSuccess = "CREATE"
    },
    addTransactionData(state, action) {
      state.trsuccess = "Transaction data exported successfully."
      state.isActionSuccess = "CREATE"
    },

    // GET TRANSACTION_BY_ID
    fetchTransactionSuccess(state, action) {
      state.transaction = action.payload;
      state.trsuccess = null;

    },

    // GET ALL TRANSACTION
    fetchTransactionsSuccess(state, action) {
      state.transactionList = action.payload;
      state.trsuccess = null;
      state.isActionSuccess = "LIST"
    },

    // UPDATE TRANSACTION
    updateTransactionSuccess(state, action) {
      state.trsuccess = "Transaction updated successfully."
      state.isActionSuccess = "UPDATE"
    },

    // DELETE TRANSACTION
    deleteTransactionSuccess(state, action) {
      state.trsuccess = "Transaction deleted successfully."
      state.isActionSuccess = "INACTIVE"
    },
    // GET USER_Type_LIST
    fetchTransactionCodeListSuccess(state, action) {
      state.selectTransactionCodeType = action.payload;
      state.trsuccess = null
    },


  }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
  return async () => {
    dispatch(slice.actions.hasInitialState())
  }
}

/**
* TO INITIAL STATE
* @returns 
*/
export function toResetIsActionSuccessState() {
  return async () => {
    dispatch(slice.actions.resetIsActionState())
  }
}

/**
 * POST TRANSACTION
 * @param newRPTCode 
 * @returns 
 */
export function addTransaction(newRPTCode: TransactionReqType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.post('/base-para/go-aml-trx-code/add', newRPTCode);
      dispatch(slice.actions.addTransactionSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET TRANSACTION
 * @param TransactionId  
 * @returns 
 */
export function fetchTransaction(TransactionId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get(`/base-para/go-aml-trx-code/get/${TransactionId}`);
      dispatch(slice.actions.fetchTransactionSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET ALL TRANSACTION
 * @param listParameters 
 * @returns 
 */
export function fetchTransactions(listParameters: listParametersType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/base-para/go-aml-trx-code/tbl', { params: listParameters });
      dispatch(slice.actions.fetchTransactionsSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * UPDATE TRANSACTION
 * @param updateTransaction
 * @returns 
 */
export function updateTransaction(updatedTransaction: TransactionReqType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.put(`/base-para/go-aml-trx-code/update/${updatedTransaction.goamlTrxCodeId}`, updatedTransaction);
      dispatch(slice.actions.updateTransactionSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * DELETE TRANSACTION
 * @param TransactionId 
 * @returns 
 */
export function deleteTransaction(TransactionId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      await axiosServices.delete(`/base-para/go-aml-trx-code/delete/${TransactionId}`);
      dispatch(slice.actions.deleteTransactionSuccess(TransactionId));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}
/**
 * GET ALL USER_TypeS
 * @param listParameters 
 * @returns 
 */
export function fetchTransactionCodeList() {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/base-para/go-aml-trx-code/all');
      dispatch(slice.actions.fetchTransactionCodeListSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}
/**
 * export data
 * @param queryParams 
 * @returns 
 */
export function addTransactionData(queryParams: exportData) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.post('/dwh/execute/run', null,{ params: queryParams });
      dispatch(slice.actions.addTransactionData(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}
