// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps } from 'types/report-type';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['reportCode'] = {
  error: null,
  Rsuccess: null,
  isLoading: false,
  reportCodeList: null,
  reportCode: null,
  selectreportCode: null,
  xmlData: null,
  isActionSuccess: null,
  reportTypeValidate: null,
  xmlDataIsValidated: null,
  downloadCsv: null
};

const slice = createSlice({
  name: 'reportTypeValidate',
  initialState,
  reducers: {


    // TO INITIAL STATE
    hasInitialState(state) {
      state.error = null;
      state.Rsuccess = null;
      state.isLoading = false;
      state.reportTypeValidate = null
    },

    // HAS ERROR
    hasError(state, action) {
      state.error = action.payload;
    },

    startLoading(state) {
      state.isLoading = true;
    },

    finishLoading(state) {
      state.isLoading = false;
    },
    // GET USER_Type_LIST
    fetchReportCodeListSuccess(state, action) { //
      state.selectreportCode = action.payload;
      state.Rsuccess = null
    },

    // FETCH XML DATA
    fetchXmlDataValidateSuccess(state, action) {
      state.Rsuccess = "Validate Process Successful";
    },
  }
})

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
  return async () => {
    dispatch(slice.actions.hasInitialState())
  }
}


/**
     * GET ALL REPORT_TYPES
     * @param listParameters 
     * @returns 
     */
export function fetchReportCodeListForValidate() {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/base-para/report-code/all');
      dispatch(slice.actions.fetchReportCodeListSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}




export function reportValidate(reportCode: string) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      // Using GET with query parameters
      const response = await axiosServices.put(`/base/xml/validate?reportCode=${reportCode}`);
      const validationResult = response.data;


      // Dispatch validation result or error message
      dispatch(slice.actions.fetchXmlDataValidateSuccess(validationResult));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}
