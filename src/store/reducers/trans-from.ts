// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types
import { DefaultRootStateProps,  queryParamsProps } from 'types/trans-from';

// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['transFrom'] = {
    error: null,
    success: null,
    transFromList: null,
    isLoading: false,
};

const slice = createSlice({
    name: 'transFrom',
    initialState,
    reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.success = null;
            state.isLoading = false;
        },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
        },

        startLoading(state) {
            state.isLoading = true;
        },

        finishLoading(state) {
            state.isLoading = false;
        },

    
        // GET ALL transFrom 
        fetchTransFormsSuccess(state, action) {
            state.transFromList = action.payload;
            state.success = null
        },

    }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
    return async () => {
        dispatch(slice.actions.hasInitialState())
    }
}



/**
 * GET ALL transFrom
 * @param queryParams 
 * @returns 
 */
export function fetchTransFormsTable(queryParams: queryParamsProps) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get('/transaction/from/tbl', { params: queryParams });
            dispatch(slice.actions.fetchTransFormsSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}


