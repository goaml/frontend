	// third-party
    import { createSlice } from '@reduxjs/toolkit';

    // project imports
    import { axiosServices } from 'utils/axios';
    import { dispatch } from '../index';
    
    // types 
    import { DefaultRootStateProps, RPTCodeReqType, listParametersType, RPTCodesType } from 'types/rpt-code';
    
    
    
    // ----------------------------------------------------------------------
    
    const initialState: DefaultRootStateProps['rptCode'] = {
      error: null,
      RPTsuccess: null,
      isLoading: false,
      rptCodeList: null,
      rptCode: null,
      selectRPTCodeType: null,
      isActionSuccess: null
    };
    
    const slice = createSlice({
      name: 'rpt',
      initialState,
      reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
          state.error = null;
          state.RPTsuccess = null;
          state.isLoading = false;
        },
    
        // HAS ERROR
        hasError(state, action) {
          state.error = action.payload;
        },
    
        startLoading(state) {
          state.isLoading = true;
        },
    
        finishLoading(state) {
          state.isLoading = false;
        },
        resetIsActionState(state) {
          state.isActionSuccess = null;
      },
    
        // POST rpt_CODE 
        addRPTCodeSuccess(state, action) {
          state.RPTsuccess = "RPT Code created successfully."
          state.isActionSuccess = "CREATE"
        },
    
        // GET RPT_CODE_BY_ID
        fetchRPTCodeSuccess(state, action) {
          state.rptCode = action.payload;
          state.RPTsuccess = null
        },
    
        // GET ALL rpt_CODE
        fetchRPTCodesSuccess(state, action) {
          state.rptCodeList = action.payload;
          state.RPTsuccess = null;
          state.isActionSuccess = "LIST"
        },
    
        // UPDATE rpt_CODE
        updateRPTCodeSuccess(state, action) {
          state.RPTsuccess = "RPT Code updated successfully."
          state.isActionSuccess = "UPDATE"
        },
    
        // DELETE rpt_CODE
        deleteRPTCodeSuccess(state, action) {
          state.RPTsuccess = "RPT Code deleted successfully."
          state.isActionSuccess = "iNACTIVE"
        },
        // GET USER_Type_LIST
        fetchRPTCodeListSuccess(state, action) {
            state.selectRPTCodeType= action.payload;
            state.RPTsuccess = null
        },
    
      }
    });
    
    // Reducer
    export default slice.reducer;
    
    // ----------------------------------------------------------------------
    
    /**
     * TO INITIAL STATE
     * @returns 
     */
    export function toInitialState() {
      return async () => {
        dispatch(slice.actions.hasInitialState())
      }
    }

          /**
   * TO INITIAL STATE
   * @returns 
   */
  export function toResetIsActionSuccessState() {
    return async () => {
        dispatch(slice.actions.resetIsActionState())
    }
  }
    
    /**
     * POST RPT_CODE
     * @param newRPTCode 
     * @returns 
     */
    export function addRPTCode(newRPTCode: RPTCodeReqType) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.post('/base-para/rpt-code/add', newRPTCode);
          dispatch(slice.actions.addRPTCodeSuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
    /**
     * GET RPT_CODE
     * @param RPTCodeId  
     * @returns 
     */
    export function fetchRPTCode(RPTCodeId: number) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.get(`/base-para/rpt-code/get/${RPTCodeId}`);
          dispatch(slice.actions.fetchRPTCodeSuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
    /**
     * GET ALL RPT_CODE
     * @param listParameters 
     * @returns 
     */
    export function fetchRPTCodes(listParameters: listParametersType) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.get('/base-para/rpt-code/tbl', { params: listParameters });
          dispatch(slice.actions.fetchRPTCodesSuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
    /**
     * UPDATE RPT_CODE
     * @param updateRPTCode
     * @returns 
     */
    export function updateRPTCode(updatedRPTCode: RPTCodesType) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.put(`/base-para/rpt-code/update/${updatedRPTCode.rptCodeId}`, updatedRPTCode);
          dispatch(slice.actions.updateRPTCodeSuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
    /**
     * DELETE RPT_CODE
     * @param RPTCodeId 
     * @returns 
     */
    export function deleteRPTCode(RPTCodeId: number) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          await axiosServices.delete(`/base-para/rpt-code/delete/${RPTCodeId}`);
          dispatch(slice.actions.deleteRPTCodeSuccess(RPTCodeId));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }

    /**
     *  GET ALL RPT FDD
     * @param listParameters 
     * @returns 
     */
export function fetchRPTList() {
    return async () => {
      dispatch(slice.actions.startLoading());
  
      try {
        const response = await axiosServices.get('/base-para/rpt-code/all');
        dispatch(slice.actions.fetchRPTCodeListSuccess(response.data));
      } catch (error) {
        dispatch(slice.actions.hasError(error));
      } finally {
        dispatch(slice.actions.finishLoading());
      }
    };
  }