// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, GenderTypeReqType, listParametersType, GenderTypesType } from 'types/gender-type';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['genderType'] = {
  error: null,
  success: null,
  isLoading: false,
  genderTypeList: null,
  genderType: null,
  isActionSuccess: null
};

const slice = createSlice({
  name: 'genderType',
  initialState,
  reducers: {
    // TO INITIAL STATE
    hasInitialState(state) {
      state.error = null;
      state.success = null;
      state.isLoading = false;
    },

    // HAS ERROR
    hasError(state, action) {
      state.error = action.payload;
    },

    startLoading(state) {
      state.isLoading = true;
    },

    finishLoading(state) {
      state.isLoading = false;
    },
    resetIsActionState(state) {
      state.isActionSuccess = null;
  },

    // POST Gender_TYPE 
    addGenderTypeSuccess(state, action) {
      state.success = "Gender Type created successfully."
      state.isActionSuccess = "CREATE"
    },

    // GET Gender_TYPE_BY_ID
    fetchGenderTypeSuccess(state, action) {
      state.genderType = action.payload;
      state.success = null
    },

    // GET ALL Gender_TYPE
    fetchGenderTypesSuccess(state, action) {
      state.genderTypeList = action.payload;
      state.success = null
      state.isActionSuccess = "LIST"
    },

    // UPDATE Gender_TYPE
    updateGenderTypeSuccess(state, action) {
      state.success = "Gender Type updated successfully."
      state.isActionSuccess = "UPDATE"
    },

    // DELETE Gender_TYPE
    deleteGenderTypeSuccess(state, action) {
      state.success = "Gender Type deleted successfully."
      state.isActionSuccess = "INACTIVE"
    },

  }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
  return async () => {
    dispatch(slice.actions.hasInitialState())
  }
}
/* TO INITIAL STATE
* @returns 
*/
export function toResetIsActionSuccessState() {
  return async () => {
      dispatch(slice.actions.resetIsActionState())
  }
}

/**
 * POST Gender_TYPE
 * @param newGenderType 
 * @returns 
 */
export function addGenderType(newGenderType: GenderTypeReqType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.post('/base-para/gender-type', newGenderType);
      dispatch(slice.actions.addGenderTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET Gender_TYPE
 * @param GenderTypeId  
 * @returns 
 */
export function fetchGenderType(GenderTypeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get(`/base-para/gender-type/${GenderTypeId}`);
      dispatch(slice.actions.fetchGenderTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET ALL Gender_TYPE
 * @param listParameters 
 * @returns 
 */
export function fetchGenderTypes(listParameters: listParametersType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/base-para/gender-type/tbl', { params: listParameters });
      dispatch(slice.actions.fetchGenderTypesSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * UPDATE Gender_TYPE
 * @param updateGenderType
 * @returns 
 */
export function updateGenderType(updatedGenderType: GenderTypesType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.put(`/base-para/gender-type/${updatedGenderType.genderTypeId}`, updatedGenderType);
      dispatch(slice.actions.updateGenderTypeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * DELETE Gender_TYPE
 * @param GenderTypeId 
 * @returns 
 */
export function deleteGenderType(GenderTypeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      await axiosServices.delete(`/base-para/gender-type/${GenderTypeId}`);
      dispatch(slice.actions.deleteGenderTypeSuccess(GenderTypeId));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}
