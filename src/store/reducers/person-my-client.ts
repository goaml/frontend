// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types
import { DefaultRootStateProps,  queryParamsProps } from 'types/person-my-client';

// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['MyClientPerson'] = {
    error: null,
    success: null,
    MyClientPersonList: null,
    isLoading: false,
};

const slice = createSlice({
    name: 'MyClientPerson',
    initialState,
    reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
            state.error = null;
            state.success = null;
            state.isLoading = false;
        },

        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
        },

        startLoading(state) {
            state.isLoading = true;
        },

        finishLoading(state) {
            state.isLoading = false;
        },

    
        // GET ALL MyClientPerson 
        fetchMyClientPersonSuccess(state, action) {
            state.MyClientPersonList = action.payload;
            state.success = null
        },

    }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
    return async () => {
        dispatch(slice.actions.hasInitialState())
    }
}



/**
 * GET ALL MyClientPerson
 * @param queryParams 
 * @returns 
 */
export function fetchMyClientPersonTable(queryParams: queryParamsProps) {
    return async () => {
        dispatch(slice.actions.startLoading());

        try {
            const response = await axiosServices.get('/transaction/person-my-client/tbl', { params: queryParams });
            dispatch(slice.actions.fetchMyClientPersonSuccess(response.data));
        } catch (error) {
            dispatch(slice.actions.hasError(error));
        } finally {
            dispatch(slice.actions.finishLoading());
        }
    };
}


