// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, CityReqType, listParametersType, CitysType } from 'types/city-code';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['city'] = {
  error: null,
  success: null,
  isLoading: false,
  cityList: null,
  city: null,
  isActionSuccess: null
};

const slice = createSlice({
  name: 'city',
  initialState,
  reducers: {
    // TO INITIAL STATE
    hasInitialState(state) {
      state.error = null;
      state.success = null;
      state.isLoading = false;
    },

    // HAS ERROR
    hasError(state, action) {
      state.error = action.payload;
    },

    startLoading(state) {
      state.isLoading = true;
    },

    finishLoading(state) {
      state.isLoading = false;
    },
    resetIsActionState(state) {
      state.isActionSuccess = null;
  },

    // POST City_TYPE 
    addCitySuccess(state, action) {
      state.success = "City Code created successfully."
      state.isActionSuccess = "CREATE"
    },

    // GET City_TYPE_BY_ID
    fetchCitySuccess(state, action) {
      state.city = action.payload;
      state.success = null
    },

    // GET ALL City_TYPE
    fetchCitysSuccess(state, action) {
      state.cityList = action.payload;
      state.success = null
      state.isActionSuccess = "LIST"
    },

    // UPDATE City_TYPE
    updateCitySuccess(state, action) {
      state.success = "City Code updated successfully."
      state.isActionSuccess = "UPDATE"
    },

    // DELETE City_TYPE
    deleteCitySuccess(state, action) {
      state.success = "City Code deleted successfully."
      state.isActionSuccess = "INACTIVE"
    },

  }
});

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
  return async () => {
    dispatch(slice.actions.hasInitialState())
  }
}

/* TO INITIAL STATE
* @returns 
*/
export function toResetIsActionSuccessState() {
  return async () => {
      dispatch(slice.actions.resetIsActionState())
  }
}

/**
 * POST City_TYPE
 * @param newCity 
 * @returns 
 */
export function addCity(newCity: CityReqType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.post('/base-para/city-list', newCity);
      dispatch(slice.actions.addCitySuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET City_TYPE
 * @param CityId  
 * @returns 
 */
export function fetchCity(CityId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get(`/base-para/city-list/${CityId}`);
      dispatch(slice.actions.fetchCitySuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET ALL City_TYPE
 * @param listParameters 
 * @returns 
 */
export function fetchCitys(listParameters: listParametersType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/base-para/city-list/tbl', { params: listParameters });
      dispatch(slice.actions.fetchCitysSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * UPDATE City_TYPE
 * @param updateCity
 * @returns 
 */
export function updateCity(updatedCity: CitysType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.put(`/base-para/city-list/${updatedCity.cityId}`, updatedCity);
      dispatch(slice.actions.updateCitySuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * DELETE City_TYPE
 * @param CityId 
 * @returns 
 */
export function deleteCity(CityId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      await axiosServices.delete(`/base-para/city-list/${CityId}`);
      dispatch(slice.actions.deleteCitySuccess(CityId));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}
