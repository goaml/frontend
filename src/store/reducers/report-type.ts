// third-party
import { createSlice } from '@reduxjs/toolkit';

// project imports
import { axiosServices } from 'utils/axios';
import { dispatch } from '../index';

// types 
import { DefaultRootStateProps, ReportCodeReqType, ReportCodesType, downloadCsvProps, listParametersType } from 'types/report-type';



// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['reportCode'] = {
    error: null,
    Rsuccess: null,
    isLoading: false,
    reportCodeList: null,
    reportCode: null,
    selectreportCode:null,
    xmlData: null, 
    isActionSuccess:null,
    reportTypeValidate: null,
    xmlDataIsValidated: null,
    downloadCsv: null
};

const slice = createSlice({
  name: 'report',
  initialState,
  reducers: {


    // TO INITIAL STATE
    hasInitialState(state) {
      state.error = null;
      state.Rsuccess = null;
      state.isLoading = false;
    },

    // HAS ERROR
    hasError(state, action) {
      state.error = action.payload;
    },

    startLoading(state) {
      state.isLoading = true;
    },

    finishLoading(state) {
      state.isLoading = false;
    },
    resetIsActionState(state) {
      state.isActionSuccess = null;
    },
    // GET USER_Type_LIST
    fetchReportCodeListSuccess(state, action) {
      state.selectreportCode = action.payload;
      state.Rsuccess = null

    },

    // POST REPORT_TYPE 
    addReportCodeSuccess(state, action) {
      state.Rsuccess = "Report Type created successfully."
      state.isActionSuccess = "CREATE"
    },

    // GET REPORT_TYPE_BY_ID
    fetchReportCode(state, action) {
      state.reportCode = action.payload;
      state.Rsuccess = null
    },

    // GET ALL REPORT_TYPE
    fetchMPRCodesSuccess(state, action) {
      state.reportCode = action.payload;
      state.Rsuccess = null
    },

    // UPDATE REPORT_TYPE
    updateReportCode(state, action) {
      state.Rsuccess = "Report Type updated successfully."
      state.isActionSuccess = "UPDATE"
    },

    // DELETE REPORT_TYPE
    deleteReportCode(state, action) {
      state.Rsuccess = "Report Type deleted successfully."
      state.isActionSuccess = "INACTIVE"
    },

    // GET ALL REPORT_TYPE
    fetchReportsSuccess(state, action) {
      state.reportCodeList = action.payload;
      state.Rsuccess = null;
      state.isActionSuccess = "LIST"
    },
    // FETCH XML DATA
    fetchXmlDataSuccess(state, action) {
      state.xmlData = action.payload;
      state.Rsuccess = null;
      state.isActionSuccess = "VIEW"
    },

    // VALIDATE XML DATA
    validateXmlData(state, action) {
      state.xmlDataIsValidated = action.payload;
      state.Rsuccess = null;
    },

    // Fetch downloadCsv
    downloadCsv(state, action) {
      state.downloadCsv = action.payload;
      state.Rsuccess = null;
    },
  }
})

// Reducer
export default slice.reducer;

// ----------------------------------------------------------------------

/**
 * TO INITIAL STATE
 * @returns 
 */
export function toInitialState() {
  return async () => {
    dispatch(slice.actions.hasInitialState())
  }
}
/**
 * TO INITIAL STATE
 * @returns 
 */
export function toResetIsActionSuccessState() {
  return async () => {
    dispatch(slice.actions.resetIsActionState())
  }
}


/**
     * GET ALL REPORT_TYPES
     * @param listParameters 
     * @returns 
     */
export function fetchReportCodeList() {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/base-para/report-code/all');
      dispatch(slice.actions.fetchReportCodeListSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}


/**
* GET ALL REPORT_TYPE
* @param listParameters 
* @returns 
*/
export function fetchReports(listParameters: listParametersType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/base-para/report-code/tbl', { params: listParameters });
      dispatch(slice.actions.fetchReportsSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
* GET REPORT_TYPE
* @param ReportCodeId  
* @returns 
*/
export function fetchReportCode(ReportCodeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get(`/base-para/report-code/get/${ReportCodeId}`);
      dispatch(slice.actions.fetchMPRCodesSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
* POST REPORT_TYPE
* @param newReportCode 
* @returns 
*/
export function addReportCode(newReportCode: ReportCodeReqType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.post('/base-para/report-code/add', newReportCode);
      dispatch(slice.actions.addReportCodeSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * UPDATE REPORT_TYPE
 * @param updateReportCode
 * @returns 
 */
export function updateReportCode(updatedReportCode: ReportCodesType) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.put(`/base-para/report-code/update/${updatedReportCode.reportCodeId}`, updatedReportCode);
      dispatch(slice.actions.updateReportCode(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * DELETE REPORT_TYPE
 * @param ReportCodeId 
 * @returns 
 */
export function deleteReportCode(ReportCodeId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      await axiosServices.delete(`/base-para/report-code/delete/${ReportCodeId}`);
      dispatch(slice.actions.deleteReportCode(ReportCodeId));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * Fetch Report XML
 * @param reportCode 
 * @param errorId 
 * @returns 
 */
export function fetchReport(reportCode: string, errorId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get(`/base/xml/download?reportCode=${reportCode}&reportTypeId=${errorId}`, {
        responseType: 'blob'
      });

      const contentDisposition = response.headers['content-disposition'];

      let fileName = 'report.xml'; // Default fallback name
      if (contentDisposition) {
        const fileNameMatch = contentDisposition.match(/filename="(.+?)"/);
        if (fileNameMatch) {
          fileName = fileNameMatch[1];
        }
      }


      const xmlFile = new Blob([response.data], { type: 'application/xml' });

      const downloadLink = document.createElement('a');
      downloadLink.href = window.URL.createObjectURL(xmlFile);
      downloadLink.download = fileName;
      downloadLink.click();

      // If you need to dispatch the fetched XML data
      dispatch(slice.actions.fetchXmlDataSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
* GET IS_Validated XML DATA
*@param reportCode 
* @param errorId 
* @returns 
*/
export function fetchIsValidatedXML(reportCode: string, errorId: number) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get(`/base/xml/data-is-validated?reportCode=${reportCode}&reportTypeId=${errorId}`);
      dispatch(slice.actions.validateXmlData(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}

/**
 * GET downloadCsv
 * @param reportCode 
 * @param errorId 
 * @returns 
 */
export function fetchDownloadCsv(queryParams: downloadCsvProps) {
  return async () => {
    dispatch(slice.actions.startLoading());

    try {
      const response = await axiosServices.get('/base/xml/download-csv', { params: queryParams, responseType: 'blob' });

      const contentDisposition = response.headers['content-disposition'];
      
      let fileName = 'report.csv'; // Default fallback name
      if (contentDisposition) {
        const fileNameMatch = contentDisposition.match(/filename="(.+?)"/);
        if (fileNameMatch) {
          fileName = fileNameMatch[1];
        }
      }

      const csvFile = new Blob([response.data], { type: 'text/csv' });

      const downloadLink = document.createElement('a');
      downloadLink.href = window.URL.createObjectURL(csvFile);
      downloadLink.download = fileName;
      document.body.appendChild(downloadLink); 
      downloadLink.click();
      document.body.removeChild(downloadLink); 
      
      dispatch(slice.actions.downloadCsv(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    } finally {
      dispatch(slice.actions.finishLoading());
    }
  };
}
