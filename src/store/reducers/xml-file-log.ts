	// third-party
    import { createSlice } from '@reduxjs/toolkit';

    // project imports
    import { axiosServices } from 'utils/axios';
    import { dispatch } from '../index';
    
    // types 
    import { DefaultRootStateProps, listParametersType,  } from 'types/xml-file-log';
    
    
    
    // ----------------------------------------------------------------------
    
    const initialState: DefaultRootStateProps['xmlFileLogList'] = {
        error: null,
        success: null,
        isLoading: false,
        xmlFileLogList: null,
        xmlFileLogListForDashBoard: null
    };
    
    const slice = createSlice({
      name: 'TransactionCode',
      initialState,
      reducers: {
        // TO INITIAL STATE
        hasInitialState(state) {
          state.error = null;
          state.success = null;
          state.isLoading = false;
        },
    
        // HAS ERROR
        hasError(state, action) {
          state.error = action.payload;
        },
    
        startLoading(state) {
          state.isLoading = true;
        },
    
        finishLoading(state) {
          state.isLoading = false;
        },
    
        // GET ALL xmlFileLogList
        fetchXMLFileLogListSuccess(state, action) {
          state.xmlFileLogList = action.payload;
          state.success = null
        },
    
        
        // GET ALL xmlFileLogList  for Dashboard
        fetchXMLFileLogListForSuccess(state, action) {
          state.xmlFileLogListForDashBoard = action.payload;
          state.success = null
        },
    
        
    
      }
    });
    
    // Reducer
    export default slice.reducer;
    
    // ----------------------------------------------------------------------
    
    /**
     * TO INITIAL STATE
     * @returns 
     */
    export function toInitialState() {
      return async () => {
        dispatch(slice.actions.hasInitialState())
      }
    }
    

    /**
     * GET ALL xmlFileLogList
     * @param listParameters 
     * @returns 
     */
    export function fetchSummaryLog(listParameters: listParametersType) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.get('/log/xml-summary-log/tbl', { params: listParameters });
          dispatch(slice.actions.fetchXMLFileLogListSuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
     /**
     * GET ALL xmlFileLogList
     * @param listParameters 
     * @returns 
     */
     export function fetchSummaryLogForDashboard(listParameters: listParametersType) {
      return async () => {
        dispatch(slice.actions.startLoading());
    
        try {
          const response = await axiosServices.get('/log/xml-summary-log/tbl', { params: listParameters });
          dispatch(slice.actions.fetchXMLFileLogListForSuccess(response.data));
        } catch (error) {
          dispatch(slice.actions.hasError(error));
        } finally {
          dispatch(slice.actions.finishLoading());
        }
      };
    }
    
    