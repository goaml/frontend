import { Link } from 'react-router-dom';
import { Button, InputLabel, OutlinedInput, Stack } from '@mui/material';
import AnimateButton from 'components/@extended/AnimateButton';
//import useAuth from 'hooks/useAuth';
import AuthWrapper from 'sections/auth/AuthWrapper';
import { ChangeEvent, useRef, useState, useEffect } from 'react';
import { axiosServices } from 'utils/axios';
import { dispatch } from 'store';
import { openSnackbar } from 'store/reducers/snackbar';

const CheckMail = () => {
  // const { isLoggedIn } = useAuth();
  const [otp, setOtp] = useState(['', '', '', '', '', '']);
  const inputRefs = useRef<HTMLInputElement[]>([]);
  const [timer, setTimer] = useState(30); // Timer set to 30 seconds
  const [isResendEnabled, setIsResendEnabled] = useState(false);

  const handleInputChange = (index: number, value: string) => {
    const newOtp = [...otp];
    newOtp[index] = value;
    setOtp(newOtp);

    if (value && index < otp.length - 1 && inputRefs.current[index + 1]) {
      inputRefs.current[index + 1].focus();
    }
  };

  const handleFocus = (index: number) => {
    if (inputRefs.current[index]) {
      inputRefs.current[index].focus();
    }
  };

  let interval: NodeJS.Timeout | undefined = undefined;

  // Function to handle timer countdown
  useEffect(() => {
    if (timer > 0 && isResendEnabled) {
      interval = setInterval(() => {
        setTimer((prevTimer) => prevTimer - 1);
      }, 1000);
    } else if (timer === 0 && isResendEnabled) {
      if (interval) {
        clearInterval(interval);
      }
      setIsResendEnabled(false); // Disable resend option after countdown
    }
    return () => {
      if (interval) {
        clearInterval(interval);
      }
    };
  }, [timer, isResendEnabled]);

  // Validate OTP
  const validateOtp = async (otp: string) => {
    try {
      await axiosServices.post(`/auth/validate-otp/${username}/${otp}`);
      fogetPassword();
    } catch (err) {
      dispatch(
        openSnackbar({
          open: true,
          message: 'Error validating OTP. Please try again.',
          variant: 'alert',
          alert: {
            color: 'error'
          },
          close: false
        })
      );
      console.log(err);
    }
  };


  // Function to resend OTP
  const handleResendOTP = async () => {
    try {
      setTimer(30); // Reset timer
      setIsResendEnabled(true); // Enable resend
      // logic to resend OTP
      await axiosServices.post(`/auth/send-otp/${username}`);
    } catch (err) {
      dispatch(
        openSnackbar({
          open: true,
          message: 'Error resending OTP. Please try again.',
          variant: 'alert',
          alert: {
            color: 'error'
          },
          close: false
        })
      );
      console.log(err);
    }
  };

  const getSessionValue = (key: string) => {
    const storedValue = localStorage.getItem(key);
    return storedValue ? JSON.parse(storedValue) : null;
  };

  const removeSessionValue = (key: string) => {
    localStorage.removeItem(key);
  };

  const username = getSessionValue('userName');

  // Function to reset password
  const fogetPassword = async () => {
    try {
      await axiosServices.post(`/auth/forgot-password`, { userName: username });
      dispatch(
        openSnackbar({
          open: true,
          message: 'New Password sent Successfully',
          variant: 'alert',
          alert: {
            color: 'success'
          },
          close: false
        })
      );
      setTimeout(() => {
        window.location.replace("/login");
        removeSessionValue('userName');
      }, 1500);
    } catch (err) {
      dispatch(
        openSnackbar({
          open: true,
          message: 'Error resetting password. Please try again.',
          variant: 'alert',
          alert: {
            color: 'error'
          },
          close: false
        })
      );
      console.log(err);
    }
  };

  // Function to clear OTP fields
  const clearOtp = () => {
    setOtp(['', '', '', '', '', '']); // Reset OTP state
    inputRefs.current[0]?.focus(); // Focus on the first input field
  };

  return (
    <AuthWrapper>
      <Stack direction="column" spacing={1}>
        <Stack direction="row" mt={1} justifyContent="space-between">
          <InputLabel htmlFor="otpCode">OTP Code</InputLabel>
          <Link to="#" onClick={() => window.history.back()}>
            Back
          </Link>
        </Stack>
        <Stack direction="row" spacing={1}>
          {otp.map((digit, index) => (
            <OutlinedInput
              key={index}
              id={`otpCode${index}`}
              type="text"
              value={otp[index]}
              inputRef={(el) => {
                if (el) inputRefs.current[index] = el;
              }}
              onFocus={() => handleFocus(index)}
              onChange={(e: ChangeEvent<HTMLInputElement>) =>
                handleInputChange(index, e.target.value)
              }
              sx={{ width: '3rem', textAlign: 'center' }}
            />
          ))}
        </Stack>
      </Stack>
      <Stack mt={2}>
        <AnimateButton>
          <Button
            //component={Link}
            //to={isLoggedIn ? '/auth/login' : '/login'}
            disableElevation
            fullWidth
            size="large"
            type="submit"
            variant="contained"
            color="primary"
            onClick={() => {
              validateOtp(otp.join(''))
            }}
          >
            Reset Password
          </Button>
        </AnimateButton>
      </Stack>
      <Stack direction="row" mt={1} justifyContent="space-between">
        {isResendEnabled ? (
          <div>Resend OTP in {timer} seconds</div>
        ) : (
          <Link to="#" onClick={handleResendOTP}>
            Resend OTP
          </Link>
        )}
        <Button
          // variant="outlined"
          color="secondary"
          onClick={clearOtp}
          size="small"
          sx={{ marginLeft: 2 }}
        >
          Clear
        </Button>
      </Stack>
    </AuthWrapper>
  );
};

export default CheckMail;
