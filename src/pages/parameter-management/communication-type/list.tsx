import { MouseEvent, useMemo, useState, useEffect, Fragment } from 'react';

// material-ui
import {
  alpha,
  Button,
  Dialog,
  IconButton,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
  useMediaQuery,
  useTheme,
} from '@mui/material';

// third-party
import { Cell, Column, HeaderGroup, Row, useExpanded, useFilters, useGlobalFilter, usePagination, useRowSelect, useSortBy, useTable } from 'react-table';

// project import
import { PopupTransition } from 'components/@extended/Transitions';
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import { HeaderSort, SortingSelect, TablePagination, TableRowSelection } from 'components/third-party/ReactTable';

// utils
import {
  GlobalFilter,
  renderFilterTypes
} from 'utils/react-table';

// sections

// assets
import { DeleteTwoTone, EditTwoTone, PlusOutlined } from '@ant-design/icons';

//types 
// import { useDispatch, useSelector } from 'store';
// import { fetchBranchCodes, toInitialState } from 'store/reducers/branch-para';
// import { openSnackbar } from 'store/reducers/snackbar';
// import { listParametersType } from 'types/branch-para';
import { ReactTableProps, dataProps, communicationProps, TableParamsType } from './types/types';
import AddEditCommunicationCode from 'sections/parameter-management/communication-type/AddEditComunicationType';
import AlertCommunicationDelete from 'sections/parameter-management/communication-type/DeleteComunicationType';
import { useDispatch, useSelector } from 'store';
import { listParametersType } from 'types/communication-type';
import { fetchCommunicationTypes, toInitialState, toResetIsActionSuccessState } from 'store/reducers/communication-type';
import { openSnackbar } from 'store/reducers/snackbar';
import useAuth from 'hooks/useAuth';
import { AddActivityLog } from 'store/reducers/note-log';


// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, handleAddEdit, getHeaderProps }: ReactTableProps) {
  const filterTypes = useMemo(() => renderFilterTypes, []);
  const theme = useTheme();
  const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));
  const sortBy = { id: 'id', desc: false };


  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    allColumns,
    prepareRow,
    preGlobalFilteredRows,
    setGlobalFilter,
    page,
    gotoPage,
    setPageSize,
    setSortBy,
    state: { pageIndex, pageSize, selectedRowIds, globalFilter }
  }  = useTable(
    {
        columns,
        data,
        filterTypes,
        initialState: { pageIndex: 0, pageSize: 10, hiddenColumns: ['avatar'], sortBy: [sortBy] }
    },
    useGlobalFilter,
    useFilters,
    useSortBy,
    useExpanded,
    usePagination,
    useRowSelect
);

  return (
    <>
      <TableRowSelection selected={Object.keys(selectedRowIds).length} />
        <Stack spacing={3}>
          <Stack
              direction={matchDownSM ? 'column' : 'row'}
              spacing={4}
              justifyContent="space-between"
              alignItems="center"
              sx={{ p: 3, pb: 0 }}
          >
              <GlobalFilter
                  preGlobalFilteredRows={preGlobalFilteredRows}
                  globalFilter={globalFilter}
                  setGlobalFilter={setGlobalFilter}
                  size="small"
              />
              <Stack direction={matchDownSM ? 'column' : 'row'} alignItems="center" spacing={1}>
                  <SortingSelect sortBy={sortBy.id} setSortBy={setSortBy} allColumns={allColumns}  />
                  <Button variant="contained" startIcon={<PlusOutlined />} onClick={handleAddEdit} size="small">
                      Add
                  </Button>

              </Stack>
          </Stack>
          <Table {...getTableProps()}>
              <TableHead>
                  {headerGroups.map((headerGroup: HeaderGroup<{}>) => (
                      <TableRow {...headerGroup.getHeaderGroupProps()} sx={{ '& > th:first-of-type': { width: '58px' } }}>
                          {headerGroup.headers.map((column: HeaderGroup) => (
                              <TableCell {...column.getHeaderProps([{ className: column.className }, getHeaderProps(column)])}>
                                  <HeaderSort column={column} />
                              </TableCell>
                          ))}
                      </TableRow>
                  ))}
              </TableHead>
              <TableBody {...getTableBodyProps()}>
                  {page.map((row: Row, i: number) => {
                      prepareRow(row);
                      return (
                          <Fragment key={i}>
                              <TableRow
                                  {...row.getRowProps()}
                                  onClick={() => {
                                      row.toggleRowSelected();
                                  }}
                                  sx={{ cursor: 'pointer', bgcolor: row.isSelected ? alpha(theme.palette.primary.lighter, 0.35) : 'inherit' }}
                              >
                                  {row.cells.map((cell: Cell) => (
                                      <TableCell {...cell.getCellProps([{ className: cell.column.className }])}>{cell.render('Cell')}</TableCell>
                                  ))}
                              </TableRow>
                              {/* {row.isExpanded && renderRowSubComponent({ row, rowProps, visibleColumns, expanded })} */}
                          </Fragment>
                      );
                  })}
                  <TableRow>
                      <TableCell sx={{ p: 2 }} colSpan={12}>
                          <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
                      </TableCell>
                  </TableRow>
              </TableBody>
          </Table>
      </Stack>
    </>
  );
}


// ==============================|| List ||============================== //

const List = () => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const { communicationTypeList, error, success,isActionSuccess } = useSelector(state => state.communicationType);
  const {user}= useAuth()

  // table
  const [data, setData] = useState<dataProps[]>([])


  const columns = useMemo(
    () =>
      [
        {
          Header: '#',
          accessor: 'id',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            if (row.id === undefined || row.id === null || row.id === '') {
              return <>-</>
            }
            if (typeof row.id === 'string') {
              return <>{(parseInt(row.id) + 1).toString()}</>;
            }
            if (typeof row.id === 'number') {
              return <>{row.id + 1}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          Header: 'Communication Type Code',
          accessor: 'communicationTypeCode',
          Cell: ({ row }: { row: Row }) => {
            if (row.values.communicationTypeCode === undefined || row.values.communicationTypeCode === null || row.values.communicationTypeCode === '') {
              return <>-</>
            }
            if (typeof row.values.communicationTypeCode === 'string') {
              return <>{row.values.communicationTypeCode}</>;
            }
            if (typeof row.values.communicationTypeCode === 'number') {
              return <>{row.values.communicationTypeCode}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          Header: 'Communication Type Description',
          accessor: 'description',
          Cell: ({ row }: { row: Row }) => {
            if (row.values.description === undefined || row.values.description === null || row.values.description === '') {
              return <>-</>
            }
            if (typeof row.values.description === 'string') {
              return <>{row.values.description}</>;
            }
            if (typeof row.values.description === 'number') {
              return <>{row.values.description}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }


        },
        {
          Header: 'Communication Type Status',
          accessor: 'isActive',
          Cell: ({ row }: { row: Row }) => {
              if (row.values.isActive === undefined || row.values.isActive === null || row.values.isActive === '') {
                  return <>-</>
              }
              if (typeof row.values.isActive === 'string') {
                  return <>{row.values.isActive}</>;
              }
              if (typeof row.values.isActive === 'number') {
                  return <>{row.values.isActive}</>;
              }
              if (typeof row.values.isActive === 'boolean') {
                  return <>{row.values.isActive ? "ACTIVE" : "IN-ACTIVE"}</>;
              }
              // Handle any other data types if necessary
              return <>-</>;
          }
      },
        // {
        //   Header: 'Branch Status',
        //   accessor: 'usRStatusDetail',
        //   Cell: ({ row }: { row: Row }) => {
        //     if (row.values.usRStatusDetail.statusDesc === undefined || row.values.usRStatusDetail.statusDesc === null || row.values.usRStatusDetail.statusDesc === '') {
        //       return <>-</>
        //     }
        //     if (typeof row.values.usRStatusDetail.statusDesc === 'string') {
        //       return <>{row.values.usRStatusDetail.statusDesc}</>;
        //     }
        //     if (typeof row.values.usRStatusDetail.statusDesc === 'number') {
        //       return <>{row.values.usRStatusDetail.statusDesc}</>;
        //     }
        //     // Handle any other data types if necessary
        //     return <>-</>;
        //   }
        // },
        {
          id: "actions",
          Header: 'Actions',
          accessor: 'actions',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            const data: dataProps = row.original
            return (
              <>
                <Stack direction="row" alignItems="left" justifyContent="left" spacing={0}>
                  {/* <Tooltip title="View">
                                        <IconButton
                                            color="secondary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                            }}
                                        >
                                            <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                                        </IconButton>
                                    </Tooltip> */}
                  <Tooltip title="Edit">
                    <IconButton
                      color="primary"
                      onClick={(e: MouseEvent<HTMLButtonElement>) => {
                        e.stopPropagation();
                        handleAddEdit()
                        setCommunicationType({
                          ...data
                        })
                      }}
                    >
                      <EditTwoTone twoToneColor={theme.palette.primary.main} />
                    </IconButton>
                  </Tooltip>
                  <Tooltip title="Delete">
                    <IconButton
                      color="error"
                      onClick={(e: MouseEvent<HTMLButtonElement>) => {
                        e.stopPropagation();
                        setCommunicationTypeId(data.communicationTypeId!)
                        setCommunicationTypeCode(data.communicationTypeCode!)
                        setOpenAlert(true)
                      }}
                    >
                      <DeleteTwoTone twoToneColor={theme.palette.error.main} />
                    </IconButton>
                  </Tooltip>
                </Stack>
              </>
            )
          }
        }
      ] as Column[],
    []
  );

  //dialog model 
  const [addEdit, setAddEdit] = useState<boolean>(false);
  const [CommunicationType, setCommunicationType] = useState<communicationProps>();

  const handleAddEdit = () => {
    setAddEdit(!addEdit);
    if (CommunicationType && !addEdit) setCommunicationType(undefined);
  };

  //alert model
  const [openAlert, setOpenAlert] = useState(false);
  const [communicationTypeId, setCommunicationTypeId] = useState<number | null>(null)
  const [communicationTypeCode, setCommunicationTypeCode] = useState<string | null>(null)

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
  };
  // //================================API CONFIG=============================//

  const [page, setPage] = useState<number>(0);
  const [perPage, setPerPage] = useState<number>(1000);
  const [direction, setDirection] = useState<"asc" | "desc">("desc");
  const [sort, setSort] = useState<string>("communicationTypeId"); //CHANGE ID

  const tableParams: TableParamsType = {
    page,
    setPage,
    perPage,
    setPerPage,
    direction,
    setDirection,
    sort,
    setSort
  }

  useEffect(() => {
    const listParameters: listParametersType = {
      page: page,
      perPage: perPage,
      direction: direction,
      sort: sort
    };
    dispatch(fetchCommunicationTypes(listParameters));
  }, [dispatch, success, page, perPage, direction, sort]);

  useEffect(() => {
    setData(communicationTypeList?.result! || []);
  }, [communicationTypeList])

  //  handel error 
  useEffect(() => {
    if (error != null && typeof error === 'object' && 'message' in error) {
      dispatch(
        openSnackbar({
          open: true,
          message: (error as { message: string }).message, // Type assertion
          variant: 'alert',
          alert: {
            color: 'error'
          },
          close: true
        })
      );
      dispatch(toInitialState());
    }
  }, [error]);

  //  handel success
  useEffect(() => {
    if (success != null) {
      dispatch(
        openSnackbar({
          open: true,
          message: success,
          variant: 'alert',
          alert: {
            color: 'success'
          },
          close: true
        })
      );
      dispatch(toInitialState())
    }
  }, [success])

  useEffect(()=>{
    if (isActionSuccess != null) {
        let actionId, actionName ,keyValue,keyValueId,description;

        switch (isActionSuccess) {
            case 'LIST':
                actionId = 2;
                actionName = 'LIST';
                keyValueId = 0 ;
                keyValue = 'N/A' ;
                description='Get Communication Type List'
                break;
            case 'CREATE':
                actionId = 1;
                actionName = 'CREATE';
                keyValueId = 0 ;
                keyValue = 'N/A' 
                description='Create a Communication Type Code'
                break;
            case 'UPDATE':
                actionId = 3;
                actionName = 'UPDATE';
                keyValueId = CommunicationType?.communicationTypeId! || 0 ;
                keyValue = CommunicationType?.communicationTypeCode! || 0 ;
                description=`Edit Communication Type Details : ${CommunicationType?.communicationTypeId} `
                break;
            case 'INACTIVE':
                actionId = 12;
                actionName = 'INACTIVE';
                keyValueId = communicationTypeId || 0;
                keyValue = communicationTypeCode || 'N/A';
                description=`Delete Communication Type: -${communicationTypeId}`
                
                break;
            default:
                return; // Exit early if no valid action is found
        }
        dispatch(AddActivityLog({
            actionId: actionId,
            actionName: actionName,
            branchId: user?.branchList?.[0]?.branchId ?? undefined,
            companyId: user?.companyId,
            deptId: user?.departmentList?.[0]?.departmentId ?? undefined,
            description: description,
            keyValue: keyValue.toString(),
            keyValueId: keyValueId,
            menuId: 29,
            menuName: "Communication Type",
            deptName: user?.departmentList?.[0]?.deptName ?? undefined
        }));

        dispatch(toResetIsActionSuccessState());
    }

},[isActionSuccess]);


  // if (isLoading) {
  //     return <div>Loading...</div>;
  // }


  return (
    <>
      <MainCard content={false}>
        <ScrollX>
          <ReactTable columns={columns} data={data} handleAddEdit={handleAddEdit} tableParams={tableParams}  getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()} />
        </ScrollX>
        {/* add / edit submissionCode dialog */}
        <Dialog
          maxWidth="sm"
          TransitionComponent={PopupTransition}
          keepMounted
          fullWidth
          onClose={handleAddEdit}
          open={addEdit}
          sx={{ '& .MuiDialog-paper': { p: 0 }, transition: 'transform 225ms' }}
          aria-describedby="alert-dialog-slide-description"
        >
          <AddEditCommunicationCode communicationType={CommunicationType} onCancel={handleAddEdit} />
        </Dialog>
        {/* alert model */}
        {communicationTypeId && <AlertCommunicationDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={communicationTypeId} />}
      </MainCard>
    </>
  );
}

export default List;
