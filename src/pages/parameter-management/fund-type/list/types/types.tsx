import { Dispatch, SetStateAction } from 'react';
import { Column, HeaderGroup } from 'react-table';
import { FundTypeType } from 'types/fund-type';

export interface dataProps extends FundTypeType {
}

export interface ReactTableProps {
    columns: Column[]
    data: dataProps[]
    handleAddEdit: () => void
    getHeaderProps: (column: HeaderGroup) => {};
    tableParams:TableParamsType
}
export interface TableHeaderProps {
    headerGroups: HeaderGroup[];
}

export interface userProps extends FundTypeType{ }

export interface TableParamsType {
    page: number;
    setPage: Dispatch<SetStateAction<number>>;
    perPage: number;
    setPerPage: Dispatch<SetStateAction<number>>;
    direction: "asc" | "desc";
    setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
    sort: string;
    setSort: Dispatch<SetStateAction<string>>;
    search: string;
    setSearch: Dispatch<SetStateAction<string>>;
}
