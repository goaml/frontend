// import { Dispatch, SetStateAction } from 'react';
import { Dispatch, SetStateAction } from 'react';
import { Column, HeaderGroup } from 'react-table';
import { EntityLegalFormTypeType } from 'types/entity-legal-form-type';

export interface dataProps extends EntityLegalFormTypeType {}

export interface ReactTableProps {
  columns: Column[]
  data: dataProps[]
  handleAddEdit: () => void  
  getHeaderProps: (column: HeaderGroup) => {};
  tableParams: TableParamsType
}

export interface entityLegalFormProps extends EntityLegalFormTypeType {}

export interface TableParamsType {
  page: number;
  setPage: Dispatch<SetStateAction<number>>;
  perPage: number;
  setPerPage: Dispatch<SetStateAction<number>>;
  direction: "asc" | "desc";
  setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
  sort: string;
  setSort: Dispatch<SetStateAction<string>>;
}
