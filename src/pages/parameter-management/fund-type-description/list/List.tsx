/* eslint-disable prettier/prettier */
import { Fragment, useMemo, useState, MouseEvent, useEffect } from 'react';

// material ui
import {
    Button,
    Dialog,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    alpha,
    useMediaQuery,
    useTheme,
    Tooltip,
    IconButton

} from '@mui/material';

import { DeleteTwoTone, EditTwoTone } from '@ant-design/icons';

// third-party
import { HeaderSort, SortingSelect, TablePagination, TableRowSelection } from 'components/third-party/ReactTable';
import { Cell, Column, HeaderGroup, Row, useExpanded, useFilters, useGlobalFilter, usePagination, useRowSelect, useSortBy, useTable } from 'react-table';

import {
    GlobalFilter,
    renderFilterTypes
} from 'utils/react-table';

// project import
import { PlusOutlined } from '@ant-design/icons';
import { ReactTableProps, TableParamsType, dataProps, userProps} from './types/types';
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import { PopupTransition } from 'components/@extended/Transitions';
import AddEditFundTypeDescription from 'sections/parameter-management/fund-type-description/AddEditFundTypeDescription';
import AlertFundTypeDescriptionDelete from 'sections/parameter-management/fund-type-description/AlertFundTypeDescriptionDelete'
import { useDispatch, useSelector } from 'store';
import { listParametersType } from 'types/fund-desc';
import { fetchFundTypeDescs, toInitialState } from 'store/reducers/fund-desc';
import { openSnackbar } from 'store/reducers/snackbar';

// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, handleAddEdit, getHeaderProps }: ReactTableProps) {
    const theme = useTheme();

    const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));
    const sortBy = { id: 'id', desc: false };

    const filterTypes = useMemo(() => renderFilterTypes, []);

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        allColumns,
        rows,
        page,
        gotoPage,
        setPageSize,
        state: { globalFilter, selectedRowIds, pageIndex, pageSize },
        preGlobalFilteredRows,
        setGlobalFilter,
        setSortBy,
    } = useTable(
        {
            columns,
            data,
            filterTypes,
            initialState: { pageIndex: 0, pageSize: 10, hiddenColumns: ['avatar'], sortBy: [sortBy] }
        },
        useGlobalFilter,
        useFilters,
        useSortBy,
        useExpanded,
        usePagination,
        useRowSelect
    );

    return (
        <>
            <TableRowSelection selected={Object.keys(selectedRowIds).length} />
            <Stack spacing={3}>
                <Stack
                    direction={matchDownSM ? 'column' : 'row'}
                    spacing={1}
                    justifyContent="space-between"
                    alignItems="center"
                    sx={{ p: 3, pb: 0 }}
                >
                    <GlobalFilter
                        preGlobalFilteredRows={preGlobalFilteredRows}
                        globalFilter={globalFilter}
                        setGlobalFilter={setGlobalFilter}
                        size="small"
                    />
                    <Stack direction={matchDownSM ? 'column' : 'row'} alignItems="center" spacing={1}>
                        <SortingSelect sortBy={sortBy.id} setSortBy={setSortBy} allColumns={allColumns} />
                        <Button variant="contained" startIcon={<PlusOutlined />} onClick={handleAddEdit} size="small">
                            Add New
                        </Button>

                    </Stack>
                </Stack>
                <Table {...getTableProps()}>
                    <TableHead>
                        {headerGroups.map((headerGroup: HeaderGroup<{}>) => (
                            <TableRow {...headerGroup.getHeaderGroupProps()} sx={{ '& > th:first-of-type': { width: '58px' } }}>
                                {headerGroup.headers.map((column: HeaderGroup) => (
                                    <TableCell {...column.getHeaderProps([{ className: column.className }, getHeaderProps(column)])}>
                                        <HeaderSort column={column} />
                                    </TableCell>
                                ))}
                            </TableRow>
                        ))}
                    </TableHead>
                    <TableBody {...getTableBodyProps()}>
                        {page.map((row: Row, i: number) => {
                            prepareRow(row);
                            return (
                                <Fragment key={i}>
                                    <TableRow
                                        {...row.getRowProps()}
                                        onClick={() => {
                                            row.toggleRowSelected();
                                        }}
                                        sx={{ cursor: 'pointer', bgcolor: row.isSelected ? alpha(theme.palette.primary.lighter, 0.35) : 'inherit' }}
                                    >
                                        {row.cells.map((cell: Cell) => (
                                            <TableCell {...cell.getCellProps([{ className: cell.column.className }])}>{cell.render('Cell')}</TableCell>
                                        ))}
                                    </TableRow>
                                    {/* {row.isExpanded && renderRowSubComponent({ row, rowProps, visibleColumns, expanded })} */}
                                </Fragment>
                            );
                        })}
                        <TableRow>
                            <TableCell sx={{ p: 2 }} colSpan={12}>
                                <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </Stack>
        </>
    );
}

// ==============================|| List ||============================== //

const List = () => {
    const theme = useTheme();
    const [userId, setUserId] = useState<number | null>(null)
    const [openAlert, setOpenAlert] = useState(false);
    const dispatch = useDispatch();
    const { FundTypeList, error, FTDsuccess } = useSelector(state => state.fundDesc); 

    const [data, setData] = useState<dataProps[]>([
    
    ])

    // const handleAddEdit = () => {
    //     setData(data)
    //     window.location.replace('/user-management/create')
    // }

    const columns = useMemo(
        () =>
            [
                {
                    Header: '#',
                    accessor: 'id',
                    className: 'cell-center',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.id === undefined || row.id === null || row.id === '') {
                            return <>-</>
                        }
                        if (typeof row.id === 'string') {
                            return <>{(parseInt(row.id) + 1).toString()}</>;
                        }
                        if (typeof row.id === 'number') {
                            return <>{row.id + 1}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Fund Type Name',
                    accessor: 'fundTypeDescription'
                },
                {
                    id: "actions",
                    Header: 'Actions',
                    accessor: 'actions',
                    className: 'cell-center',
                    Cell: ({ row }: { row: Row }) => {
                        const data: dataProps = row.original
                        return (
                            <>
                                <Stack direction="row" alignItems="center" justifyContent="center" spacing={0}>
                                    {/* <Tooltip title="View">
                                        <IconButton
                                            color="secondary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                            }}
                                        >
                                            <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                                        </IconButton>
                                    </Tooltip> */}
                                    <Tooltip title="Edit">
                                        <IconButton
                                            color="primary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                                handleAddEdit()
                                                setUser({
                                                    ...data
                                                })
                                            }}
                                        >
                                            <EditTwoTone twoToneColor={theme.palette.primary.main} />
                                        </IconButton>
                                    </Tooltip>
                                    <Tooltip title="Delete">
                                        <IconButton
                                            color="error"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                                setUserId(data.fundTypeDescriptionId!)
                                                setOpenAlert(true)
                                            }}
                                        >
                                            <DeleteTwoTone twoToneColor={theme.palette.error.main} />
                                        </IconButton>
                                    </Tooltip>
                                </Stack>
                            </>
                        )
                    }
                }
                
            ] as Column[],
        []
    );

    const [addEdit, setAddEdit] = useState<boolean>(false);
    const [user, setUser] = useState<userProps>();
    
    const handleAddEdit = () => {

        setAddEdit(!addEdit);
        if (user && !addEdit) setUser(undefined);
    };

    const handleAlertClose = () => {
        setOpenAlert(!openAlert);
    };


        // ==============================|| API-Config ||============================== //

   const [page, setPage] = useState<number>(0);
   const [perPage, setPerPage] = useState<number>(1000);
   const [direction, setDirection] = useState<"asc" | "desc">("asc");
   const [search, setSearch] = useState<string>("");
   const [sort, setSort] = useState<string>("fundTypeDescriptionId");

   const tableParams: TableParamsType = {
       page,
       setPage,
       perPage,
       setPerPage,
       direction,
       setDirection,
       sort,
       setSort,
       search,
       setSearch
   }

   useEffect(() => {
       const listParameters: listParametersType = {
           page: page,
           per_page: perPage,
           direction: direction,
           search: search,
           sort: sort
       };
       dispatch(fetchFundTypeDescs(listParameters));
   }, [dispatch, FTDsuccess, page, perPage, direction, search, sort]);

   useEffect(() => {
       setData(FundTypeList?.result! || []);
   }, [FundTypeList])

   //  handel error 
   useEffect(() => {
    if (error != null && typeof error === 'object' && 'message' in error) {
        dispatch(
            openSnackbar({
                open: true,
                message: (error as { message: string }).message, // Type assertion
                variant: 'alert',
                alert: {
                    color: 'error'
                },
                close: true
            })
        );
        dispatch(toInitialState());
    }
}, [error]);

   //  handel success
   useEffect(() => {
       if (FTDsuccess != null) {
           dispatch(
               openSnackbar({
                   open: true,
                   message: FTDsuccess,
                   variant: 'alert',
                   alert: {
                       color: 'success'
                   },
                   close: true
               })
           );
           dispatch(toInitialState())
       }
   }, [FTDsuccess])

   // if (isLoading) {
   //     return <div>Loading...</div>;
   // }

    return (
        <>
            <MainCard content={false}>
                <ScrollX>
                    <ReactTable columns={columns}
                    getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()}
                    data={data} handleAddEdit={handleAddEdit} tableParams={tableParams} />
                </ScrollX>
                {/* add / edit user dialog */}
                <Dialog
                    maxWidth="sm"
                    TransitionComponent={PopupTransition}
                    keepMounted
                    fullWidth
                    onClose={handleAddEdit}
                    open={addEdit}
                    sx={{ '& .MuiDialog-paper': { p: 0 }, transition: 'transform 225ms' }}
                    aria-describedby="alert-dialog-slide-description"
                >
                    <AddEditFundTypeDescription user={user} onCancel={handleAddEdit} />
                </Dialog>
                {/* alert model */}
                {userId && <AlertFundTypeDescriptionDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={userId} />}
            </MainCard>
        </>
    )
};

export default List;
