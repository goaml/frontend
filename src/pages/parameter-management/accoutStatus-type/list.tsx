import { MouseEvent, useMemo, useState, useEffect, Fragment } from 'react';

// material-ui
import {
  alpha,
  Button,
  Dialog,
  IconButton,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
  useMediaQuery,
  useTheme,
} from '@mui/material';

// third-party
import { Cell, Column, HeaderGroup, Row, useExpanded, useFilters, useGlobalFilter, usePagination, useRowSelect, useSortBy, useTable } from 'react-table';

// project import
import { PopupTransition } from 'components/@extended/Transitions';
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import {  HeaderSort, SortingSelect, TablePagination, TableRowSelection } from 'components/third-party/ReactTable';

// utils
import {
  GlobalFilter,
  renderFilterTypes
} from 'utils/react-table';

// sections

// assets
import { DeleteTwoTone, EditTwoTone, PlusOutlined } from '@ant-design/icons';

//types 
// import { useDispatch, useSelector } from 'store';
// import { fetchBranchCodes, toInitialState } from 'store/reducers/branch-para';
// import { openSnackbar } from 'store/reducers/snackbar';
// import { listParametersType } from 'types/branch-para';
import { ReactTableProps, dataProps, accountStatusProps, TableParamsType } from './types/types';
import AddEditAccountStatus from 'sections/parameter-management/accountStatus-type/AddEditAccStatusType';
import AlertAccountStatusDelete from 'sections/parameter-management/accountStatus-type/DeleteAccStatusType';
import { useDispatch, useSelector } from 'store';
import { listParametersType } from 'types/account-status-type';
import { fetchAccountStatusTypes, toInitialState, toResetIsActionSuccessState } from 'store/reducers/account-status-type';
import { openSnackbar } from 'store/reducers/snackbar';
import { AddActivityLog } from 'store/reducers/note-log';
import useAuth from 'hooks/useAuth';


// ==============================|| REACT TABLE ||============================== //


function ReactTable({ columns, data, handleAddEdit, getHeaderProps, tableParams }: ReactTableProps) {

  const filterTypes = useMemo(() => renderFilterTypes, []);
  const theme = useTheme();
  const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));
  const sortBy = { id: 'id', desc: false };

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    allColumns,
    prepareRow,
    preGlobalFilteredRows,
    setGlobalFilter,
    page,
    gotoPage,
    setPageSize,
    setSortBy,
    state: { pageIndex, pageSize, selectedRowIds, globalFilter }
  }  = useTable(
    {
        columns,
        data,
        filterTypes,
        initialState: { pageIndex: 0, pageSize: 10, hiddenColumns: ['avatar'], sortBy: [sortBy] }
    },
    useGlobalFilter,
    useFilters,
    useSortBy,
    useExpanded,
    usePagination,
    useRowSelect
);

  return (
    <>
      <TableRowSelection selected={Object.keys(selectedRowIds).length} />
        <Stack spacing={3}>
          <Stack
              direction={matchDownSM ? 'column' : 'row'}
              spacing={4}
              justifyContent="space-between"
              alignItems="center"
              sx={{ p: 3, pb: 0 }}
          >
              <GlobalFilter
                  preGlobalFilteredRows={preGlobalFilteredRows}
                  globalFilter={globalFilter}
                  setGlobalFilter={setGlobalFilter}
                  size="small"
              />
              <Stack direction={matchDownSM ? 'column' : 'row'} alignItems="center" spacing={1}>
                  <SortingSelect sortBy={sortBy.id} setSortBy={setSortBy} allColumns={allColumns}  />
                  <Button variant="contained" startIcon={<PlusOutlined />} onClick={handleAddEdit} size="small">
                      Add
                  </Button>

              </Stack>
          </Stack>
          <Table {...getTableProps()}>
              <TableHead>
                  {headerGroups.map((headerGroup: HeaderGroup<{}>) => (
                      <TableRow {...headerGroup.getHeaderGroupProps()} sx={{ '& > th:first-of-type': { width: '58px' } }}>
                          {headerGroup.headers.map((column: HeaderGroup) => (
                              <TableCell {...column.getHeaderProps([{ className: column.className }, getHeaderProps(column)])}>
                                  <HeaderSort column={column} />
                              </TableCell>
                          ))}
                      </TableRow>
                  ))}
              </TableHead>
              <TableBody {...getTableBodyProps()}>
                  {page.map((row: Row, i: number) => {
                      prepareRow(row);
                      return (
                          <Fragment key={i}>
                              <TableRow
                                  {...row.getRowProps()}
                                  onClick={() => {
                                      row.toggleRowSelected();
                                  }}
                                  sx={{ cursor: 'pointer', bgcolor: row.isSelected ? alpha(theme.palette.primary.lighter, 0.35) : 'inherit' }}
                              >
                                  {row.cells.map((cell: Cell) => (
                                      <TableCell {...cell.getCellProps([{ className: cell.column.className }])}>{cell.render('Cell')}</TableCell>
                                  ))}
                              </TableRow>
                              {/* {row.isExpanded && renderRowSubComponent({ row, rowProps, visibleColumns, expanded })} */}
                          </Fragment>
                      );
                  })}
                  <TableRow>
                      <TableCell sx={{ p: 2 }} colSpan={12}>
                          <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
                      </TableCell>
                  </TableRow>
              </TableBody>
          </Table>
      </Stack>
    </>
  );
}


// ==============================|| List ||============================== //

const List = () => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const { accountStatusTypeList, error, success ,isActionSuccess} = useSelector(state => state.accountStatusType);
  const {user} = useAuth()

  // table
  const [data, setData] = useState<dataProps[]>([])

  const columns = useMemo(
    () =>
      [
        {
          Header: '#',
          accessor: 'id',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            if (row.id === undefined || row.id === null || row.id === '') {
              return <>-</>
            }
            if (typeof row.id === 'string') {
              return <>{(parseInt(row.id) + 1).toString()}</>;
            }
            if (typeof row.id === 'number') {
              return <>{row.id + 1}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          Header: 'Account Status Type Code',
          accessor: 'accountStatusTypeCode',
          Cell: ({ row }: { row: Row }) => {
            if (row.values.accountStatusTypeCode === undefined || row.values.accountStatusTypeCode === null || row.values.accountStatusTypeCode === '') {
              return <>-</>
            }
            if (typeof row.values.accountStatusTypeCode === 'string') {
              return <>{row.values.accountStatusTypeCode}</>;
            }
            if (typeof row.values.accountStatusTypeCode === 'number') {
              return <>{row.values.accountStatusTypeCode}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          Header: 'Account Status Type Description',
          accessor: 'description',
          Cell: ({ row }: { row: Row }) => {
            if (row.values.description === undefined || row.values.description === null || row.values.description === '') {
              return <>-</>
            }
            if (typeof row.values.description === 'string') {
              return <>{row.values.description}</>;
            }
            if (typeof row.values.description === 'number') {
              return <>{row.values.description}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }


        },
        {
          Header: 'Account Status Type Status',
          accessor: 'isActive',
          Cell: ({ row }: { row: Row }) => {
              if (row.values.isActive === undefined || row.values.isActive === null || row.values.isActive === '') {
                  return <>-</>
              }
              if (typeof row.values.isActive === 'string') {
                  return <>{row.values.isActive}</>;
              }
              if (typeof row.values.isActive === 'number') {
                  return <>{row.values.isActive}</>;
              }
              if (typeof row.values.isActive === 'boolean') {
                  return <>{row.values.isActive ? "ACTIVE" : "IN-ACTIVE"}</>;
              }
              // Handle any other data types if necessary
              return <>-</>;
          }
      },
        // {
        //   Header: 'Branch Status',
        //   accessor: 'usRStatusDetail',
        //   Cell: ({ row }: { row: Row }) => {
        //     if (row.values.usRStatusDetail.statusDesc === undefined || row.values.usRStatusDetail.statusDesc === null || row.values.usRStatusDetail.statusDesc === '') {
        //       return <>-</>
        //     }
        //     if (typeof row.values.usRStatusDetail.statusDesc === 'string') {
        //       return <>{row.values.usRStatusDetail.statusDesc}</>;
        //     }
        //     if (typeof row.values.usRStatusDetail.statusDesc === 'number') {
        //       return <>{row.values.usRStatusDetail.statusDesc}</>;
        //     }
        //     // Handle any other data types if necessary
        //     return <>-</>;
        //   }
        // },
        {
          id: "actions",
          Header: 'Actions',
          accessor: 'actions',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            const data: dataProps = row.original
            return (
              <>
                <Stack direction="row" alignItems="left" justifyContent="left" spacing={0}>
                  {/* <Tooltip title="View">
                                        <IconButton
                                            color="secondary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                            }}
                                        >
                                            <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                                        </IconButton>
                                    </Tooltip> */}
                  <Tooltip title="Edit">
                    <IconButton
                      color="primary"
                      onClick={(e: MouseEvent<HTMLButtonElement>) => {
                        e.stopPropagation();
                        handleAddEdit()
                        setaccountStatusType({
                          ...data
                        })
                      }}
                    >
                      <EditTwoTone twoToneColor={theme.palette.primary.main} />
                    </IconButton>
                  </Tooltip>
                  <Tooltip title="Delete">
                    <IconButton
                      color="error"
                      onClick={(e: MouseEvent<HTMLButtonElement>) => {
                        e.stopPropagation();
                        setaccountStatusTypeId(data.accountStatusTypeId!)
                        setaccountStatusTypeCode(data.accountStatusTypeCode!)
                        setOpenAlert(true)
                      }}
                    >
                      <DeleteTwoTone twoToneColor={theme.palette.error.main} />
                    </IconButton>
                  </Tooltip>
                </Stack>
              </>
            )
          }
        }
      ] as Column[],
    []
  );

  //dialog model 
  const [addEdit, setAddEdit] = useState<boolean>(false);
  const [accountStatusType, setaccountStatusType] = useState<accountStatusProps>();

  const handleAddEdit = () => {
    setAddEdit(!addEdit);
    if (accountStatusType && !addEdit) setaccountStatusType(undefined);
  };

  //alert model
  const [openAlert, setOpenAlert] = useState(false);
  const [accountStatusTypeId, setaccountStatusTypeId] = useState<number | null>(null)
  const [accountStatusTypeCode, setaccountStatusTypeCode] = useState<string | null>(null)

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
  };
  // //================================API CONFIG=============================//

  const [page, setPage] = useState<number>(0);
  const [perPage, setPerPage] = useState<number>(1000);
  const [direction, setDirection] = useState<"asc" | "desc">("desc");
  const [sort, setSort] = useState<string>("accountStatusTypeId"); //CHANGE ID

  const tableParams: TableParamsType = {
    page,
    setPage,
    perPage,
    setPerPage,
    direction,
    setDirection,
    sort,
    setSort
  }

  useEffect(() => {
    const listParameters: listParametersType = {
      page: page,
      perPage: perPage,
      direction: direction,
      sort: sort
    };
    dispatch(fetchAccountStatusTypes(listParameters));
  }, [dispatch, success, page, perPage, direction, sort]);

  useEffect(() => {
    setData(accountStatusTypeList?.result! || []);
  }, [accountStatusTypeList])

  //  handel error 
  useEffect(() => {
    if (error != null && typeof error === 'object' && 'message' in error) {
      dispatch(
        openSnackbar({
          open: true,
          message: (error as { message: string }).message, // Type assertion
          variant: 'alert',
          alert: {
            color: 'error'
          },
          close: true
        })
      );
      dispatch(toInitialState());
    }
  }, [error]);

  //  handel success
  useEffect(() => {
    if (success != null) {
      dispatch(
        openSnackbar({
          open: true,
          message: success,
          variant: 'alert',
          alert: {
            color: 'success'
          },
          close: true
        })
      );
      dispatch(toInitialState())
    }
  }, [success])

  useEffect(()=>{
    if (isActionSuccess != null) {
        let actionId, actionName ,keyValue,keyValueId,description;

        switch (isActionSuccess) {
            case 'LIST':
                actionId = 2;
                actionName = 'LIST';
                keyValueId = 0 ;
                keyValue = 'N/A' ;
                description='Get Account Status Type List'
                break;
            case 'CREATE':
                actionId = 1;
                actionName = 'CREATE';
                keyValueId = 0 ;
                keyValue = 'N/A' 
                description='Create a Account Status Type Code'
                break;
            case 'UPDATE':
                actionId = 3;
                actionName = 'UPDATE';
                keyValueId = accountStatusType?.accountStatusTypeId! || 0 ;
                keyValue = accountStatusType?.accountStatusTypeCode! || 0 ;
                description=`Edit Account Status Type Details : ${accountStatusType?.accountStatusTypeId} `
                break;
            case 'INACTIVE':
                actionId = 12;
                actionName = 'INACTIVE';
                keyValueId = accountStatusTypeId || 0;
                keyValue = accountStatusTypeCode || 'N/A';
                description=`Delete Account Status Type: -${accountStatusTypeId}`
                
                break;
            default:
                return; // Exit early if no valid action is found
        }
        dispatch(AddActivityLog({
            actionId: actionId,
            actionName: actionName,
            branchId: user?.branchList?.[0]?.branchId ?? undefined,
            companyId: user?.companyId,
            deptId: user?.departmentList?.[0]?.departmentId ?? undefined,
            description: description,
            keyValue: keyValue.toString(),
            keyValueId: keyValueId,
            menuId: 25,
            menuName: "Account Status Type",
            deptName: user?.departmentList?.[0]?.deptName ?? undefined
        }));

        dispatch(toResetIsActionSuccessState());
    }

},[isActionSuccess]);

  // if (isLoading) {
  //     return <div>Loading...</div>;
  // }


  return (
    <>
      <MainCard content={false}>
        <ScrollX>
          <ReactTable columns={columns} data={data} handleAddEdit={handleAddEdit} tableParams={tableParams} getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()} />
        </ScrollX>
        {/* add / edit submissionCode dialog */}
        <Dialog
          maxWidth="sm"
          TransitionComponent={PopupTransition}
          keepMounted
          fullWidth
          onClose={handleAddEdit}
          open={addEdit}
          sx={{ '& .MuiDialog-paper': { p: 0 }, transition: 'transform 225ms' }}
          aria-describedby="alert-dialog-slide-description"
        >
          <AddEditAccountStatus accountStatusCode={accountStatusType} onCancel={handleAddEdit} />
        </Dialog>
        {/* alert model */}
        {accountStatusTypeId && <AlertAccountStatusDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={accountStatusTypeId} />}
      </MainCard>
    </>
  );
}

export default List;
