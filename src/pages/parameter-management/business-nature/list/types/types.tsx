
import { Column } from 'react-table';


export interface dataProps {
    BusinessId?: number | undefined;
    BusinessNatureCode?: string | undefined;
    desc?: string | undefined;
    statusDesc?: string | undefined;
 }

export interface ReactTableProps {
  columns: Column[]
  data: dataProps[]
  handleAddEdit: () => void

}

export interface BusinessProps  { 
  BusinessId?: number | undefined;
  desc?: string | undefined;
  statusDesc?: string | undefined;
}


