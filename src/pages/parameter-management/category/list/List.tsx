/* eslint-disable prettier/prettier */
import { DeleteTwoTone, EditTwoTone, PlusOutlined } from '@ant-design/icons';
// material ui
import {
    alpha, Button,
    Dialog,
    IconButton, Stack,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Tooltip, useMediaQuery,
    useTheme
} from '@mui/material';

import { PopupTransition } from 'components/@extended/Transitions';
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
// third-party
import { HeaderSort, SortingSelect, TablePagination, TableRowSelection } from 'components/third-party/ReactTable';
import { Fragment, MouseEvent, useEffect, useMemo, useState } from 'react';
import { Cell, Column, HeaderGroup, Row, useExpanded, useFilters, useGlobalFilter, usePagination, useRowSelect, useSortBy, useTable } from 'react-table';
import AddEditCatgory from 'sections/parameter-management/category/AddEditCategory';
import AlertCategoryDelete from 'sections/parameter-management/category/AlertCategoryDelete';
import {
    GlobalFilter,
    renderFilterTypes
} from 'utils/react-table';
import { dataProps, ReactTableProps, TableParamsType } from './types/types';
import { useDispatch, useSelector } from 'store';
import { listParametersType } from 'types/category';
import { fetchCategory, toInitialState, toResetIsActionSuccessState } from 'store/reducers/category';
import { openSnackbar } from 'store/reducers/snackbar';
import useAuth from 'hooks/useAuth';
import { AddActivityLog } from 'store/reducers/note-log';





// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, handleAddEdit, getHeaderProps }: ReactTableProps) {
    const theme = useTheme();

    const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));
    const sortBy = { id: 'id', desc: false };

    const filterTypes = useMemo(() => renderFilterTypes, []);

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        allColumns,
        rows,
        page,
        gotoPage,
        setPageSize,
        state: { globalFilter, selectedRowIds, pageIndex, pageSize },
        preGlobalFilteredRows,
        setGlobalFilter,
        setSortBy,
    } = useTable(
        {
            columns,
            data,
            filterTypes,
            initialState: { pageIndex: 0, pageSize: 10, hiddenColumns: ['avatar'], sortBy: [sortBy] }
        },
        useGlobalFilter,
        useFilters,
        useSortBy,
        useExpanded,
        usePagination,
        useRowSelect
    );

    return (
        <>
            <TableRowSelection selected={Object.keys(selectedRowIds).length} />
            <Stack spacing={3}>
                <Stack
                    direction={matchDownSM ? 'column' : 'row'}
                    spacing={1}
                    justifyContent="space-between"
                    alignItems="center"
                    sx={{ p: 3, pb: 0 }}
                >
                    <GlobalFilter
                        preGlobalFilteredRows={preGlobalFilteredRows}
                        globalFilter={globalFilter}
                        setGlobalFilter={setGlobalFilter}
                        size="small"
                    />
                    <Stack direction={matchDownSM ? 'column' : 'row'} alignItems="center" spacing={1}>
                        <SortingSelect sortBy={sortBy.id} setSortBy={setSortBy} allColumns={allColumns} />
                        <Button variant="contained" startIcon={<PlusOutlined />} onClick={handleAddEdit} size="small">
                            Add
                        </Button>

                    </Stack>
                </Stack>
                <Table {...getTableProps()}>
                    <TableHead>
                        {headerGroups.map((headerGroup: HeaderGroup<{}>) => (
                            <TableRow {...headerGroup.getHeaderGroupProps()} sx={{ '& > th:first-of-type': { width: '58px' } }}>
                                {headerGroup.headers.map((column: HeaderGroup) => (
                                    <TableCell {...column.getHeaderProps([{ className: column.className }, getHeaderProps(column)])}>
                                        <HeaderSort column={column} />
                                    </TableCell>
                                ))}
                            </TableRow>
                        ))}
                    </TableHead>
                    <TableBody {...getTableBodyProps()}>
                        {page.map((row: Row, i: number) => {
                            prepareRow(row);
                            return (
                                <Fragment key={i}>
                                    <TableRow
                                        {...row.getRowProps()}
                                        onClick={() => {
                                            row.toggleRowSelected();
                                        }}
                                        sx={{ cursor: 'pointer', bgcolor: row.isSelected ? alpha(theme.palette.primary.lighter, 0.35) : 'inherit' }}
                                    >
                                        {row.cells.map((cell: Cell) => (
                                            <TableCell {...cell.getCellProps([{ className: cell.column.className }])}>{cell.render('Cell')}</TableCell>
                                        ))}
                                    </TableRow>
                                    {/* {row.isExpanded && renderRowSubComponent({ row, rowProps, visibleColumns, expanded })} */}
                                </Fragment>
                            );
                        })}
                        <TableRow>
                            <TableCell sx={{ p: 2 }} colSpan={12}>
                                <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </Stack>
        </>
    );
}

// ==============================|| List ||============================== //

const List = () => {
    const theme = useTheme();
    const [id, setCategoryId] = useState<number | null>(null)
    const dispatch= useDispatch();
    const [openAlert, setOpenAlert] = useState(false);
    const { CategoryList, error, success,isActionSuccess } = useSelector(state => state.category);
    const { user } = useAuth();
    const [data, setData] = useState<dataProps[]>([
        
    ])

    // const handleAddEdit = () => {
    //     setData(data)
    //     window.location.replace('/user-management/create')
    // }

    const columns = useMemo(
        () =>
            [
                {
                    Header: '#',
                    accessor: 'id',
                    className: 'cell-center',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.id === undefined || row.id === null || row.id === '') {
                            return <>-</>
                        }
                        if (typeof row.id === 'string') {
                            return <>{(parseInt(row.id) + 1).toString()}</>;
                        }
                        if (typeof row.id === 'number') {
                            return <>{row.id + 1}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Category Name',
                    accessor: 'categoryName'
                },
                {
                    Header: 'Category Description',
                    accessor: 'categoryDescription'
                },
                {
                    id: "actions",
                    Header: 'Actions',
                    accessor: 'actions',
                    className: 'cell-center',
                    Cell: ({ row }: { row: Row }) => {
                        const data: dataProps = row.original
                        return (
                            <>
                                <Stack direction="row" alignItems="" justifyContent="" spacing={0}>
                                    {/* <Tooltip title="View">
                                        <IconButton
                                            color="secondary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                            }}
                                        >
                                            <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                                        </IconButton>
                                    </Tooltip> */}
                                    <Tooltip title="Edit">
                                        <IconButton
                                            color="primary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                                handleAddEdit()
                                                setCategory({
                                                    ...data
                                                })
                                            }}
                                        >
                                            <EditTwoTone twoToneColor={theme.palette.primary.main} />
                                        </IconButton>
                                    </Tooltip>
                                    <Tooltip title="Delete">
                                        <IconButton
                                            color="error"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                                setCategoryId(data.categoryId!)
                                                setOpenAlert(true)
                                            }}
                                        >
                                            <DeleteTwoTone twoToneColor={theme.palette.error.main} />
                                        </IconButton>
                                    </Tooltip>
                                </Stack>
                            </>
                        )
                    }
                }
                
            ] as Column[],
        []
    );

    const [addEdit, setAddEdit] = useState<boolean>(false);
    const [category, setCategory] = useState<dataProps>();
    
    const handleAddEdit = () => {

        setAddEdit(!addEdit);
        if (category && !addEdit) setCategory(undefined);
    };

    const handleAlertClose = () => {
        setOpenAlert(!openAlert);
    };


    // ==============================|| API-Config ||============================== //

   const [page, setPage] = useState<number>(0);
   const [perPage, setPerPage] = useState<number>(1000);
   const [direction, setDirection] = useState<"asc" | "desc">("desc");
   const [search, setSearch] = useState<string>("");
   const [sort, setSort] = useState<string>("categoryId");

   const tableParams: TableParamsType = {
       page,
       setPage,
       perPage,
       setPerPage,
       direction,
       setDirection,
       sort,
       setSort,
       search,
       setSearch
   }

   useEffect(() => {
       const listParameters: listParametersType = {
           page: page,
           per_page: perPage,
           direction: direction,
           search: search,
           sort: sort
       };
       dispatch(fetchCategory(listParameters));
   }, [dispatch, success, page, perPage, direction, search, sort]);

   useEffect(() => {
       setData(CategoryList?.result! || []);
   }, [CategoryList])

   //  handel error 
   useEffect(() => {
    if (error != null && typeof error === 'object' && 'message' in error) {
        dispatch(
            openSnackbar({
                open: true,
                message: (error as { message: string }).message, // Type assertion
                variant: 'alert',
                alert: {
                    color: 'error'
                },
                close: true
            })
        );
        dispatch(toInitialState());
    }
}, [error]);

   //  handel success
   useEffect(() => {
       if (success != null) {
           dispatch(
               openSnackbar({
                   open: true,
                   message: success,
                   variant: 'alert',
                   alert: {
                       color: 'success'
                   },
                   close: true
               })
           );
           dispatch(toInitialState())
       }
   }, [success])

   useEffect(()=>{
    if (isActionSuccess != null) {
        let actionId, actionName ,keyValue,description;

        switch (isActionSuccess) {
            case 'LIST':
                actionId = 2;
                actionName = 'LIST';
                keyValue = 0 ;
                description='Get Category List'
                break;
            case 'CREATE':
                actionId = 1;
                actionName = 'CREATE';
                keyValue = 0 ;
                description='Create a Catagory Code'
                break;
            case 'UPDATE':
                actionId = 3;
                actionName = 'UPDATE';
                keyValue = category?.categoryId! || 0 ;
                description=`Edit Category Details : ${category?.categoryId} `
                break;
            case 'INACTIVE':
                actionId = 12;
                actionName = 'INACTIVE';
                keyValue = id || 0;
                description=`Delete Category: -${id}`
                break;
            default:
                return; // Exit early if no valid action is found
        }
        dispatch(AddActivityLog({
            actionId: actionId,
            actionName: actionName,
            branchId: user?.branchList?.[0]?.branchId ?? undefined,
            companyId: user?.companyId,
            deptId: user?.departmentList?.[0]?.departmentId ?? undefined,
            description: description,
            keyValue: "categoryId",
            keyValueId: keyValue,
            menuId: 17,
            menuName: "Category",
            deptName: user?.departmentList?.[0]?.deptName ?? undefined
        }));

        dispatch(toResetIsActionSuccessState());
    }

},[isActionSuccess]);

   // if (isLoading) {
   //     return <div>Loading...</div>;
   // }
 
    return (
        <>
            <MainCard content={false}>
                <ScrollX>
                    <ReactTable columns={columns}
                    getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()}
                    data={data} handleAddEdit={handleAddEdit} tableParams={tableParams} />
                </ScrollX>
                {/* add / edit category dialog */}
                <Dialog
                    maxWidth="sm"
                    TransitionComponent={PopupTransition}
                    keepMounted
                    fullWidth
                    onClose={handleAddEdit}
                    open={addEdit}
                    sx={{ '& .MuiDialog-paper': { p: 0 }, transition: 'transform 225ms' }}
                    aria-describedby="alert-dialog-slide-description"
                >
                    <AddEditCatgory user={category} onCancel={handleAddEdit} />
                </Dialog>
                {/* alert model */}
                {id && <AlertCategoryDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={id} />}
            </MainCard>
        </>
    )
};

export default List;
