// import { Dispatch, SetStateAction } from 'react';
import { Dispatch, SetStateAction } from 'react';
import { Column, HeaderGroup } from 'react-table';
import { IdentifierTypeType } from 'types/identifier-type';

export interface dataProps  extends IdentifierTypeType {}

export interface ReactTableProps {
  columns: Column[]
  data: dataProps[]
  handleAddEdit: () => void
  getHeaderProps: (column: HeaderGroup) => {};
  tableParams: TableParamsType
}

export interface identifierProps  extends IdentifierTypeType {}

export interface TableParamsType {
  page: number;
  setPage: Dispatch<SetStateAction<number>>;
  perPage: number;
  setPerPage: Dispatch<SetStateAction<number>>;
  direction: "asc" | "desc";
  setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
  sort: string;
  setSort: Dispatch<SetStateAction<string>>;

}
