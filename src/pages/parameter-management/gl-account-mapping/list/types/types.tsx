import { Dispatch, SetStateAction } from 'react';
import { Column, HeaderGroup } from 'react-table';
import {GLAccountType, GLAccountsType} from 'types/gl-account-mapping';

export interface dataProps extends GLAccountsType { }

export interface ReactTableProps {
    columns: Column[]
    data: dataProps[]
    handleAddEdit: () => void
    getHeaderProps: (column: HeaderGroup) => {};
    tableParams:TableParamsType
}
export interface GLProps extends GLAccountType{}

export interface TableParamsType {
    page: number;
    setPage: Dispatch<SetStateAction<number>>;
    perPage: number;
    setPerPage: Dispatch<SetStateAction<number>>;
    direction: "asc" | "desc";
    setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
    sort: string;
    setSort: Dispatch<SetStateAction<string>>;
    search: string;
    setSearch: Dispatch<SetStateAction<string>>;
}

export interface userProps { }