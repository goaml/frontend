/* eslint-disable prettier/prettier */
import { Fragment, useMemo, useState, MouseEvent, useEffect } from 'react';

// material ui
import {
    Button,
    Dialog,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    alpha,
    useMediaQuery,
    useTheme,
    Tooltip,
    IconButton

} from '@mui/material';

import { DeleteTwoTone, EditTwoTone } from '@ant-design/icons';

// third-party
import { HeaderSort, SortingSelect, TablePagination, TableRowSelection } from 'components/third-party/ReactTable';
import { Cell, Column, HeaderGroup, Row, useExpanded, useFilters, useGlobalFilter, usePagination, useRowSelect, useSortBy, useTable } from 'react-table';

import {
    GlobalFilter,
    renderFilterTypes
} from 'utils/react-table';

// project import
import { PlusOutlined } from '@ant-design/icons';
import { ReactTableProps, GLProps, TableParamsType, dataProps } from './types/types';
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import { PopupTransition } from 'components/@extended/Transitions';
import AlertGLAccountDelete from 'sections/parameter-management/gl-account-mapping/AlertGLAccountMappingDelete'
import { useDispatch, useSelector } from 'store';
import { listParametersType } from 'types/gl-account-mapping';
import { fetchGLAccounts, toInitialState, toResetIsActionSuccessState } from 'store/reducers/gl-account-mapping';
import { openSnackbar } from 'store/reducers/snackbar';
import useAuth from 'hooks/useAuth';
import { AddActivityLog } from 'store/reducers/note-log';
import AddEditGLAccount from 'sections/parameter-management/gl-account-mapping/AddEditGLAccountMapping';

// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, handleAddEdit, getHeaderProps }: ReactTableProps) {
    const theme = useTheme();

    const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));
    const sortBy = { id: 'id', desc: false };

    const filterTypes = useMemo(() => renderFilterTypes, []);

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        allColumns,
        rows,
        page,
        gotoPage,
        setPageSize,
        state: { globalFilter, selectedRowIds, pageIndex, pageSize },
        preGlobalFilteredRows,
        setGlobalFilter,
        setSortBy,
    } = useTable(
        {
            columns,
            data,
            filterTypes,
            initialState: { pageIndex: 0, pageSize: 10, hiddenColumns: ['avatar'], sortBy: [sortBy] }
        },
        useGlobalFilter,
        useFilters,
        useSortBy,
        useExpanded,
        usePagination,
        useRowSelect
    );

    return (
        <>
            <TableRowSelection selected={Object.keys(selectedRowIds).length} />

            <Stack spacing={3}>
                <Stack
                    direction={matchDownSM ? 'column' : 'row'}
                    spacing={4}
                    justifyContent="space-between"
                    alignItems="center"
                    sx={{ p: 3, pb: 0 }}
                >
                    <GlobalFilter
                        preGlobalFilteredRows={preGlobalFilteredRows}
                        globalFilter={globalFilter}
                        setGlobalFilter={setGlobalFilter}
                        size="small"
                    />
                    <Stack direction={matchDownSM ? 'column' : 'row'} alignItems="center" spacing={1}>
                        <SortingSelect sortBy={sortBy.id} setSortBy={setSortBy} allColumns={allColumns} />
                        <Button variant="contained" startIcon={<PlusOutlined />} onClick={handleAddEdit} size="small">
                            Add
                        </Button>

                    </Stack>
                </Stack>
                <Table {...getTableProps()}>
                    <TableHead>
                        {headerGroups.map((headerGroup: HeaderGroup<{}>) => (
                            <TableRow {...headerGroup.getHeaderGroupProps()} sx={{ '& > th:first-of-type': { width: '58px' } }}>
                                {headerGroup.headers.map((column: HeaderGroup) => (
                                    <TableCell {...column.getHeaderProps([{ className: column.className }, getHeaderProps(column)])}>
                                        <HeaderSort column={column} />
                                    </TableCell>
                                ))}
                            </TableRow>
                        ))}
                    </TableHead>
                    <TableBody {...getTableBodyProps()}>
                        {page.map((row: Row, i: number) => {
                            prepareRow(row);
                            return (
                                <Fragment key={i}>
                                    <TableRow
                                        {...row.getRowProps()}
                                        onClick={() => {
                                            row.toggleRowSelected();
                                        }}
                                        sx={{ cursor: 'pointer', bgcolor: row.isSelected ? alpha(theme.palette.primary.lighter, 0.35) : 'inherit' }}
                                    >
                                        {row.cells.map((cell: Cell) => (
                                            <TableCell {...cell.getCellProps([{ className: cell.column.className }])}>{cell.render('Cell')}</TableCell>
                                        ))}
                                    </TableRow>
                                    {/* {row.isExpanded && renderRowSubComponent({ row, rowProps, visibleColumns, expanded })} */}
                                </Fragment>
                            );
                        })}
                        <TableRow>
                            <TableCell sx={{ p: 2 }} colSpan={12}>
                                <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </Stack>
        </>
    );
}

// ==============================|| List ||============================== //

const List = () => {
    const theme = useTheme();
    const dispatch = useDispatch();
    const { GLAccountList, error, GLsuccess, isActionSuccess } = useSelector(state => state.GLAccountMapping);
    const { user } = useAuth()

    const [data, setData] = useState<dataProps[]>([])

    const columns = useMemo(
        () =>
            [
                {
                    Header: '#',
                    accessor: 'id',
                    className: 'cell-center',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.id === undefined || row.id === null || row.id === '') {
                            return <>-</>
                        }
                        if (typeof row.id === 'string') {
                            return <>{(parseInt(row.id) + 1).toString()}</>;
                        }
                        if (typeof row.id === 'number') {
                            return <>{row.id + 1}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'RPT Code ',
                    accessor: 'rptCode',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.rptCode === undefined || row.values.rptCode === null || row.values.rptCode === '') {
                            return <>-</>
                        }
                        if (typeof row.values.rptCode === 'string') {
                            return <>{row.values.rptCode}</>;
                        }
                        if (typeof row.values.rptCode === 'number') {
                            return <>{row.values.rptCode}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }

                },
                {
                    Header: 'Mapping Account',
                    accessor: 'mappingAccount',

                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.mappingAccount === undefined || row.values.mappingAccount === null || row.values.mappingAccount === '') {
                            return <>-</>
                        }
                        if (typeof row.values.mappingAccount === 'string') {
                            return <>{row.values.mappingAccount}</>;
                        }
                        if (typeof row.values.mappingAccount === 'number') {
                            return <>{row.values.mappingAccount}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Institution Name',
                    accessor: 'institutionName',

                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.institutionName === undefined || row.values.institutionName === null || row.values.institutionName === '') {
                            return <>-</>
                        }
                        if (typeof row.values.institutionName === 'string') {
                            return <>{row.values.institutionName}</>;
                        }
                        if (typeof row.values.institutionName === 'number') {
                            return <>{row.values.institutionName}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Swift Code',
                    accessor: 'swift',

                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.swift === undefined || row.values.swift === null || row.values.swift === '') {
                            return <>-</>
                        }
                        if (typeof row.values.swift === 'string') {
                            return <>{row.values.swift}</>;
                        }
                        if (typeof row.values.swift === 'number') {
                            return <>{row.values.swift}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    id: "actions",
                    Header: 'Actions',
                    accessor: 'actions',
                    className: 'cell-center',
                    Cell: ({ row }: { row: Row }) => {
                        const data: dataProps = row.original
                        return (
                            <>
                                <Stack direction="row" alignItems="left" justifyContent="left" spacing={0}>
                                    {/* <Tooltip title="View">
                                        <IconButton
                                            color="secondary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                            }}
                                        >
                                            <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                                        </IconButton>
                                    </Tooltip> */}

                                    <Tooltip title="Edit">
                                        <IconButton
                                            color="primary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                                handleAddEdit()
                                                setGLAccount({ ...data })
                                            }}
                                        >
                                            <EditTwoTone twoToneColor={theme.palette.primary.main} />
                                        </IconButton>
                                    </Tooltip>
                                    <Tooltip title="Delete">
                                        <IconButton
                                            color="error"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                                setGlAccountId(data.glAccountMappingId!)
                                                setRptCode(data.rptCode!)
                                                setOpenAlert(true)
                                            }}
                                        >
                                            <DeleteTwoTone twoToneColor={theme.palette.error.main} />
                                        </IconButton>
                                    </Tooltip>
                                </Stack>
                            </>
                        )
                    }
                }

            ] as Column[],
        []
    );

    const [addEdit, setAddEdit] = useState<boolean>(false);
    const [GLAccount, setGLAccount] = useState<GLProps>();

    const handleAddEdit = () => {

        setAddEdit(!addEdit);
        if (GLAccount && !addEdit) setGLAccount(undefined);
    };

    // alert model
    const [openAlert, setOpenAlert] = useState(false);
    const [GLAccountId, setGlAccountId] = useState<number | null>(null)
    const [rptCode, setRptCode] = useState<string | null>(null)

    const handleAlertClose = () => {
        setOpenAlert(!openAlert);
    };


    // ==============================|| API-Config ||============================== //

    const [page, setPage] = useState<number>(0);
    const [perPage, setPerPage] = useState<number>(1000);
    const [direction, setDirection] = useState<"asc" | "desc">("desc");
    const [search, setSearch] = useState<string>("");
    const [sort, setSort] = useState<string>("rptCode");

    const tableParams: TableParamsType = {
        page,
        setPage,
        perPage,
        setPerPage,
        direction,
        setDirection,
        sort,
        setSort,
        search,
        setSearch
    }

    useEffect(() => {
        const listParameters: listParametersType = {
            page: page,
            per_page: perPage,
            direction: direction,
            search: search,
            sort: sort
        };
        dispatch(fetchGLAccounts(listParameters));
    }, [dispatch, GLsuccess, page, perPage, direction, search, sort]);

    useEffect(() => {
        setData(GLAccountList?.result ?? []);
    }, [GLAccountList]);

    //  handel error 
    useEffect(() => {
        if (error != null && typeof error === 'object' && 'message' in error) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: (error as { message: string }).message,
                    variant: 'alert',
                    alert: { color: 'error' },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [error]);

    //  handel success
    useEffect(() => {
        if (GLsuccess != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: GLsuccess,
                    variant: 'alert',
                    alert: {
                        color: 'success'
                    },
                    close: true
                })
            );
            dispatch(toInitialState())
        }
    }, [GLsuccess])


    useEffect(() => {
        if (isActionSuccess != null) {
            let actionId, actionName, keyValue, keyValueId, description;

            switch (isActionSuccess) {
                case 'LIST':
                    actionId = 2;
                    actionName = 'LIST';
                    keyValueId = 0;
                    keyValue = 'N/A';
                    description = 'Get GL Account List'
                    break;
                case 'CREATE':
                    actionId = 1;
                    actionName = 'CREATE';
                    keyValueId = 0;
                    keyValue = 'N/A';
                    description = 'Create a GL Account Code'
                    break;
                case 'UPDATE':
                    actionId = 3;
                    actionName = 'UPDATE';
                    keyValueId = GLAccount?.glAccount! || 0;
                    keyValue = GLAccount?.rptCode! || 0;
                    description = `Edit GL Account Details : ${GLAccount?.glAccount} `
                    break;
                case 'INACTIVE':
                    actionId = 12;
                    actionName = 'INACTIVE';
                    keyValueId = GLAccountId || 0;
                    keyValue = rptCode || 'N/A';
                    description = `Delete GL Account: -${GLAccountId}`

                    break;
                default:
                    return; // Exit early if no valid action is found
            }
            dispatch(AddActivityLog({
                actionId: actionId,
                actionName: actionName,
                branchId: user?.branchList?.[0]?.branchId ?? undefined,
                companyId: user?.companyId,
                deptId: user?.departmentList?.[0]?.departmentId ?? undefined,
                description: description,
                keyValue: keyValue.toString(),
                keyValueId: typeof keyValueId === 'number' ? keyValueId : undefined,
                menuId: 38,
                menuName: "RPT Code",
                deptName: user?.departmentList?.[0]?.deptName ?? undefined
            }));

            dispatch(toResetIsActionSuccessState());
        }

    }, [isActionSuccess]);

    return (
        <>
            <MainCard content={false}>
                <ScrollX>
                    <ReactTable columns={columns} data={data} handleAddEdit={handleAddEdit} tableParams={tableParams} getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()} />
                </ScrollX>
                {/* add / edit user dialog */}
                <Dialog
                    maxWidth="sm"
                    TransitionComponent={PopupTransition}
                    keepMounted
                    fullWidth
                    onClose={handleAddEdit}
                    open={addEdit}
                    sx={{ '& .MuiDialog-paper': { p: 0 }, transition: 'transform 225ms' }}
                    aria-describedby="alert-dialog-slide-description"
                >
                    <AddEditGLAccount rptCode={GLAccount} onCancel={handleAddEdit} />
                </Dialog>
                {/* alert model */}
                {GLAccountId && <AlertGLAccountDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={GLAccountId} />}
            </MainCard>
        </>
    )
};

export default List;
