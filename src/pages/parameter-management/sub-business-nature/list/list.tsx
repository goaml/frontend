import { MouseEvent, useEffect, useMemo, useState } from 'react';

// material-ui
import {
  Button,
  Dialog,
  IconButton,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
  useTheme,
} from '@mui/material';

// third-party
import { Cell, Column, HeaderGroup, Row, useFilters, useGlobalFilter, usePagination, useTable } from 'react-table';

// project import
import { PopupTransition } from 'components/@extended/Transitions';
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import { CSVExport, EmptyTable, TablePagination } from 'components/third-party/ReactTable';

// utils
import {
  DefaultColumnFilter,
  GlobalFilter,
  renderFilterTypes
} from 'utils/react-table';

// sections

// assets
import { DeleteTwoTone, EditTwoTone, PlusOutlined } from '@ant-design/icons';

//types 

import { ReactTableProps, SubBusinessProps, dataProps } from './types/types';
import AddEditSubBusinessNature from 'sections/parameter-management/sub-business-nature/AddEditSubBusinessNature';
import AlertSubBusinessNatureDelete from 'sections/parameter-management/sub-business-nature/DeleteSubBusinessNature';


// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, handleAddEdit }: ReactTableProps) {
  const filterTypes = useMemo(() => renderFilterTypes, []);
  const defaultColumn = useMemo(() => ({ Filter: DefaultColumnFilter }), []);

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    preGlobalFilteredRows,
    setGlobalFilter,
    globalFilter,
    page,
    gotoPage,
    setPageSize,
    state: { pageIndex, pageSize }
  } = useTable(
    {
      columns,
      data,
      defaultColumn,
      filterTypes,

      initialState: { pageIndex: 0, pageSize: 10 }
    },
    useGlobalFilter,
    useFilters,
    usePagination
  );

  return (
    <>
      <Stack direction="row" spacing={2} justifyContent="space-between" sx={{ padding: 2 }}>
        <GlobalFilter preGlobalFilteredRows={preGlobalFilteredRows} globalFilter={globalFilter} setGlobalFilter={setGlobalFilter} />
        <Stack direction="row" alignItems="center" spacing={1}>
          <CSVExport data={rows.map((d: Row) => d.original)} filename={'filtering-table.csv'} />
          <Button variant="contained" startIcon={<PlusOutlined />} onClick={handleAddEdit}>
            Add New
          </Button>
        </Stack>
      </Stack>

      <Table {...getTableProps()}>
        <TableHead sx={{ borderTopWidth: 2 }}>
          {headerGroups.map((headerGroup) => (
            <TableRow {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column: HeaderGroup) => (
                <TableCell {...column.getHeaderProps([{ className: column.className }])}>{column.render('Header')}</TableCell>
              ))}
            </TableRow>
          ))}
        </TableHead>
        <TableBody {...getTableBodyProps()}>
          {page.length > 0 ? (
            page.map((row, i) => {
              prepareRow(row);
              return (
                <TableRow {...row.getRowProps()}>
                  {row.cells.map((cell: Cell) => (
                    <TableCell {...cell.getCellProps([{ className: cell.column.className }])}>{cell.render('Cell')}</TableCell>
                  ))}
                </TableRow>
              );
            })
          ) : (
            <EmptyTable msg="No Data" colSpan={12} />
          )}
          <TableRow>
            <TableCell sx={{ p: 2 }} colSpan={12}>
              <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </>
  );
}

// ==============================|| List ||============================== //

const List = () => {
  const theme = useTheme();


  // table
  const [data, setData] = useState<dataProps[]>([])

  useEffect(() => {
    const exampleData: dataProps[] = [
      { SubBusinessId: 1, BusinessNatureCode: 'BN001',SubBusinessNatureCode: 'SBN001', desc: 'Retail', statusDesc: 'Active' },
      { SubBusinessId: 2, BusinessNatureCode: 'BN002',SubBusinessNatureCode: 'SBN001', desc: 'Wholesale', statusDesc: 'Inactive' },
      { SubBusinessId: 3, BusinessNatureCode: 'BN003',SubBusinessNatureCode: 'SBN001', desc: 'Service', statusDesc: 'Pending' },
    ];

    setData(exampleData);
  }, []);

  const columns = useMemo(
    () =>
      [
        {
          Header: '#',
          accessor: 'id',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            if (row.id === undefined || row.id === null || row.id === '') {
              return <>-</>
            }
            if (typeof row.id === 'string') {
              return <>{(parseInt(row.id) + 1).toString()}</>;
            }
            if (typeof row.id === 'number') {
              return <>{row.id + 1}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          Header: 'Business Nature Code',
          accessor: 'BusinessNatureCode',
          Cell: ({ row }: { row: Row }) => {
            if (row.values.BusinessNatureCode === undefined || row.values.BusinessNatureCode === null || row.values.BusinessNatureCode === '') {
              return <>-</>
            }
            if (typeof row.values.BusinessNatureCode === 'string') {
              return <>{row.values.BusinessNatureCode}</>;
            }
            if (typeof row.values.BusinessNatureCode === 'number') {
              return <>{row.values.BusinessNatureCode}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          Header: 'Business Sub Nature Code',
          accessor: 'SubBusinessNatureCode',
          Cell: ({ row }: { row: Row }) => {
            if (row.values.SubBusinessNatureCode === undefined || row.values.SubBusinessNatureCode === null || row.values.SubBusinessNatureCode === '') {
              return <>-</>
            }
            if (typeof row.values.SubBusinessNatureCode === 'string') {
              return <>{row.values.SubBusinessNatureCode}</>;
            }
            if (typeof row.values.SubBusinessNatureCode === 'number') {
              return <>{row.values.SubBusinessNatureCode}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          Header: 'Business Sub Nature Description',
          accessor: 'desc',
          Cell: ({ row }: { row: Row }) => {
            if (row.values.desc === undefined || row.values.desc === null || row.values.desc === '') {
              return <>-</>
            }
            if (typeof row.values.desc === 'string') {
              return <>{row.values.desc}</>;
            }
            if (typeof row.values.desc === 'number') {
              return <>{row.values.desc}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        // {
        //   Header: 'Branch Status',
        //   accessor: 'usRStatusDetail',
        //   Cell: ({ row }: { row: Row }) => {
        //     if (row.values.usRStatusDetail.statusDesc === undefined || row.values.usRStatusDetail.statusDesc === null || row.values.usRStatusDetail.statusDesc === '') {
        //       return <>-</>
        //     }
        //     if (typeof row.values.usRStatusDetail.statusDesc === 'string') {
        //       return <>{row.values.usRStatusDetail.statusDesc}</>;
        //     }
        //     if (typeof row.values.usRStatusDetail.statusDesc === 'number') {
        //       return <>{row.values.usRStatusDetail.statusDesc}</>;
        //     }
        //     // Handle any other data types if necessary
        //     return <>-</>;
        //   }
        // },
        {
          id: "actions",
          Header: 'Actions',
          accessor: 'actions',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            const data: dataProps = row.original
            return (
              <>
                <Stack direction="row" alignItems="center" justifyContent="center" spacing={0}>
                  {/* <Tooltip title="View">
                                        <IconButton
                                            color="secondary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                            }}
                                        >
                                            <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                                        </IconButton>
                                    </Tooltip> */}
                  <Tooltip title="Edit">
                    <IconButton
                      color="primary"
                      onClick={(e: MouseEvent<HTMLButtonElement>) => {
                        e.stopPropagation();
                        handleAddEdit()
                        setBusinessNature({
                          ...data
                        })
                      }}
                    >
                      <EditTwoTone twoToneColor={theme.palette.primary.main} />
                    </IconButton>
                  </Tooltip>
                  <Tooltip title="Delete">
                    <IconButton
                      color="error"
                      onClick={(e: MouseEvent<HTMLButtonElement>) => {
                        e.stopPropagation();
                        setBusinessNatureId(row.values.SubBusinessId)
                        setOpenAlert(true)
                      }}
                    >
                      <DeleteTwoTone twoToneColor={theme.palette.error.main} />
                    </IconButton>
                  </Tooltip>
                </Stack>
              </>
            )
          }
        }
      ] as Column[],
    []
  );

  //dialog model 
  const [addEdit, setAddEdit] = useState<boolean>(false);
  const [branch, setBusinessNature] = useState<SubBusinessProps>();

  const handleAddEdit = () => {
    setAddEdit(!addEdit);
    if (branch && !addEdit) setBusinessNature(undefined);
  };

  //alert model
  const [openAlert, setOpenAlert] = useState(false);
  const [SubBusinessId, setBusinessNatureId] = useState<number | null>(null)

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
  };



  return (
    <>
      <MainCard content={false}>
        <ScrollX>
          <ReactTable columns={columns} data={data} handleAddEdit={handleAddEdit} />
        </ScrollX>
        {/* add / edit branch dialog */}
        <Dialog
          maxWidth="sm"
          TransitionComponent={PopupTransition}
          keepMounted
          fullWidth
          onClose={handleAddEdit}
          open={addEdit}
          sx={{ '& .MuiDialog-paper': { p: 0 }, transition: 'transform 225ms' }}
          aria-describedby="alert-dialog-slide-description"
        >
          <AddEditSubBusinessNature businessNature={branch} onCancel={handleAddEdit} />
        </Dialog>
        {/* alert model */}
        {!SubBusinessId && <AlertSubBusinessNatureDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={SubBusinessId} />}
      </MainCard>
    </>
  );
}

export default List;
