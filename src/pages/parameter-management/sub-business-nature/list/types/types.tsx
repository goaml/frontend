
import { Column } from 'react-table';


export interface dataProps {
    SubBusinessId?: number | undefined;
    BusinessNatureCode?: string | undefined;
    SubBusinessNatureCode?: string | undefined;
    desc?: string | undefined;
    statusDesc?: string | undefined;
 }

export interface ReactTableProps {
  columns: Column[]
  data: dataProps[]
  handleAddEdit: () => void

}

export interface SubBusinessProps  { 
  SubBusinessId?: number | undefined;
  BusinessNatureCode?: string | undefined;
  SubBusinessNatureCode?: string | undefined;
  desc?: string | undefined;
  statusDesc?: string | undefined;
}


