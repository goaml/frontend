import { Dispatch, SetStateAction } from 'react';
import { Column, HeaderGroup } from 'react-table';
import { TransactionCodeType } from 'types/transaction-code';

export interface dataProps  extends TransactionCodeType {
    
}

export interface ReactTableProps {
    columns: Column[]
    data: dataProps[]
    handleAddEdit: () => void
    getHeaderProps: (column: HeaderGroup) => {};
    tableParams:TableParamsType
}
export interface TableHeaderProps {
    headerGroups: HeaderGroup[];
}

export interface userProps  extends TransactionCodeType{ }

export interface TableParamsType {
    page: number;
    setPage: Dispatch<SetStateAction<number>>;
    perPage: number;
    setPerPage: Dispatch<SetStateAction<number>>;
    direction: "asc" | "desc";
    setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
    sort: string;
    setSort: Dispatch<SetStateAction<string>>;
    search: string;
    setSearch: Dispatch<SetStateAction<string>>;
}