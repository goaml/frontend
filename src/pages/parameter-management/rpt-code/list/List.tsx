/* eslint-disable prettier/prettier */
import { Fragment, useMemo, useState, MouseEvent, useEffect } from 'react';

// material ui
import {
    Button,
    Dialog,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    alpha,
    useMediaQuery,
    useTheme,
    Tooltip,
    IconButton

} from '@mui/material';

import { DeleteTwoTone, EditTwoTone } from '@ant-design/icons';

// third-party
import { HeaderSort, SortingSelect, TablePagination, TableRowSelection } from 'components/third-party/ReactTable';
import { Cell, Column, HeaderGroup, Row, useExpanded, useFilters, useGlobalFilter, usePagination, useRowSelect, useSortBy, useTable } from 'react-table';

import {
    GlobalFilter,
    renderFilterTypes
} from 'utils/react-table';

// project import
import { PlusOutlined } from '@ant-design/icons';
import { ReactTableProps, TableHeaderProps, TableParamsType, dataProps} from './types/types';
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import { PopupTransition } from 'components/@extended/Transitions';
import AddEditRptCode from 'sections/parameter-management/rpt-code/AddEditRptCode';
import AlertRptCodeDelete from 'sections/parameter-management/rpt-code/AlertRptCodeDelete'
import { useDispatch, useSelector } from 'store';
import { listParametersType } from 'types/rpt-code';
import { fetchRPTCodes, toInitialState, toResetIsActionSuccessState } from 'store/reducers/rpt-code';
import { openSnackbar } from 'store/reducers/snackbar';
import useAuth from 'hooks/useAuth';
import { AddActivityLog } from 'store/reducers/note-log';

// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, handleAddEdit, getHeaderProps }: ReactTableProps) {
    const theme = useTheme();

    const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));
    const sortBy = { id: 'id', desc: false };

    const filterTypes = useMemo(() => renderFilterTypes, []);

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        allColumns,
        rows,
        page,
        gotoPage,
        setPageSize,
        state: { globalFilter, selectedRowIds, pageIndex, pageSize },
        preGlobalFilteredRows,
        setGlobalFilter,
        setSortBy,
    } = useTable(
        {
            columns,
            data,
            filterTypes,
            initialState: { pageIndex: 0, pageSize: 10, hiddenColumns: ['avatar'], sortBy: [sortBy] }
        },
        useGlobalFilter,
        useFilters,
        useSortBy,
        useExpanded,
        usePagination,
        useRowSelect
    );

    return (
        <>
            <TableRowSelection selected={Object.keys(selectedRowIds).length} />
            <Stack spacing={3}>
                <Stack
                    direction={matchDownSM ? 'column' : 'row'}
                    spacing={1}
                    justifyContent="space-between"
                    alignItems="center"
                    sx={{ p: 3, pb: 0 }}
                >
                    <GlobalFilter
                        preGlobalFilteredRows={preGlobalFilteredRows}
                        globalFilter={globalFilter}
                        setGlobalFilter={setGlobalFilter}
                        size="small"
                    />
                    <Stack direction={matchDownSM ? 'column' : 'row'} alignItems="center" spacing={1}>
                        <SortingSelect sortBy={sortBy.id} setSortBy={setSortBy} allColumns={allColumns} />
                        <Button variant="contained" startIcon={<PlusOutlined />} onClick={handleAddEdit} size="small">
                            Add
                        </Button>

                    </Stack>
                </Stack>
                <Table {...getTableProps()}>
                    <TableHead>
                        {headerGroups.map((headerGroup: HeaderGroup<{}>) => (
                            <TableRow {...headerGroup.getHeaderGroupProps()} sx={{ '& > th:first-of-type': { width: '58px' } }}>
                                {headerGroup.headers.map((column: HeaderGroup) => (
                                    <TableCell {...column.getHeaderProps([{ className: column.className }, getHeaderProps(column)])}>
                                        <HeaderSort column={column} />
                                    </TableCell>
                                ))}
                            </TableRow>
                        ))}
                    </TableHead>
                    <TableBody {...getTableBodyProps()}>
                        {page.map((row: Row, i: number) => {
                            prepareRow(row);
                            return (
                                <Fragment key={i}>
                                    <TableRow
                                        {...row.getRowProps()}
                                        onClick={() => {
                                            row.toggleRowSelected();
                                        }}
                                        sx={{ cursor: 'pointer', bgcolor: row.isSelected ? alpha(theme.palette.primary.lighter, 0.35) : 'inherit' }}
                                    >
                                        {row.cells.map((cell: Cell) => (
                                            <TableCell {...cell.getCellProps([{ className: cell.column.className }])}>{cell.render('Cell')}</TableCell>
                                        ))}
                                    </TableRow>
                                    {/* {row.isExpanded && renderRowSubComponent({ row, rowProps, visibleColumns, expanded })} */}
                                </Fragment>
                            );
                        })}
                        <TableRow>
                            <TableCell sx={{ p: 2 }} colSpan={12}>
                                <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </Stack>
        </>
    );
}

// ==============================|| List ||============================== //

const List = () => {
    const theme = useTheme();
    const dispatch = useDispatch();
    const { rptCodeList, error, RPTsuccess, isActionSuccess } = useSelector(state => state.rptCode); 
    const [rptCodeId, setUserId] = useState<number | null>(null)
    const [openAlert, setOpenAlert] = useState(false);
    const {user}= useAuth()
    const [data, setData] = useState<dataProps[]>([
    ])

    // const handleAddEdit = () => {
    //     setData(data)
    //     window.location.replace('/user-management/create')
    // }

    const columns = useMemo(
        () =>
            [
                {
                    Header: '#',
                    accessor: 'id',
                    className: 'cell-center',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.id === undefined || row.id === null || row.id === '') {
                            return <>-</>
                        }
                        if (typeof row.id === 'string') {
                            return <>{(parseInt(row.id) + 1).toString()}</>;
                        }
                        if (typeof row.id === 'number') {
                            return <>{row.id + 1}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'RPT Code ',
                    accessor: 'rptCodeName',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.rptCodeName === undefined || row.values.rptCodeName === null || row.values.rptCodeName === '') {
                            return <>-</>
                        }
                        if (typeof row.values.rptCodeName === 'string') {
                            return <>{row.values.rptCodeName}</>;
                        }
                        if (typeof row.values.rptCodeName === 'number') {
                            return <>{row.values.rptCodeName}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }

                },
                
                {
                    Header: 'RPT Code Description',
                    accessor: 'rptCodeDescription',

                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.rptCodeDescription === undefined || row.values.rptCodeDescription === null || row.values.rptCodeDescription === '') {
                            return <>-</>
                        }
                        if (typeof row.values.rptCodeDescription === 'string') {
                            return <>{row.values.rptCodeDescription}</>;
                        }
                        if (typeof row.values.rptCodeDescription === 'number') {
                            return <>{row.values.rptCodeDescription}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Report Type',
                    accessor: 'treport',

                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.treport?.reportCode! === undefined || row.values.treport?.reportCode! === null || row.values.treport?.reportCode! === '') {
                            return <>-</>
                        }
                        if (typeof row.values.treport?.reportCode! === 'string') {
                            return <>{row.values.treport?.reportCode!}</>;
                        }
                        if (typeof row.values.treport?.reportCode! === 'number') {
                            return <>{row.values.treport?.reportCode!}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'GOAML Trx Code',
                    accessor: 'goAmlTrxCodes',

                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.goAmlTrxCodes?.transactionCodeName === undefined || row.values.goAmlTrxCodes?.transactionCodeName === null || row.values.goAmlTrxCodes?.transactionCodeName === '') {
                            return <>-</>
                        }
                        if (typeof row.values.goAmlTrxCodes?.transactionCodeName === 'string') {
                            return <>{row.values.goAmlTrxCodes?.transactionCodeName}</>;
                        }
                        if (typeof row.values.goAmlTrxCodes?.transactionCodeName === 'number') {
                            return <>{row.values.goAmlTrxCodes?.transactionCodeName}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    id: "actions",
                    Header: 'Actions',
                    accessor: 'actions',
                    className: 'cell-center',
                    Cell: ({ row }: { row: Row }) => {
                        const data: dataProps = row.original
                        return (
                            <>
                                <Stack direction="row" alignItems="left" justifyContent="left" spacing={0}>
                                    {/* <Tooltip title="View">
                                        <IconButton
                                            color="secondary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                            }}
                                        >
                                            <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                                        </IconButton>
                                    </Tooltip> */}
                                    <Tooltip title="Edit">
                                        <IconButton
                                            color="primary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                                handleAddEdit()
                                                setRPTCode({
                                                    ...data
                                                })
                                            }}
                                        >
                                            <EditTwoTone twoToneColor={theme.palette.primary.main} />
                                        </IconButton>
                                    </Tooltip>
                                    <Tooltip title="Delete">
                                        <IconButton
                                            color="error"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                                setUserId(data.rptCodeId!)
                                                setOpenAlert(true)
                                            }}
                                        >
                                            <DeleteTwoTone twoToneColor={theme.palette.error.main} />
                                        </IconButton>
                                    </Tooltip>
                                </Stack>
                            </>
                        )
                    }
                }
                
            ] as Column[],
        []
    );

    const [addEdit, setAddEdit] = useState<boolean>(false);
    const [rptCode, setRPTCode] = useState<TableHeaderProps>();
    
    const handleAddEdit = () => {
        
        setAddEdit(!addEdit);
        if (rptCode && !addEdit) setRPTCode(undefined);
    };

    const handleAlertClose = () => {
        setOpenAlert(!openAlert);
    };


    // ==============================|| API-Config ||============================== //

   const [page, setPage] = useState<number>(0);
   const [perPage, setPerPage] = useState<number>(1000);
   const [direction, setDirection] = useState<"asc" | "desc">("desc");
   const [search, setSearch] = useState<string>("");
   const [sort, setSort] = useState<string>("rptCodeId");

   const tableParams: TableParamsType = {
       page,
       setPage,
       perPage,
       setPerPage,
       direction,
       setDirection,
       sort,
       setSort,
       search,
       setSearch
   }

   useEffect(() => {
       const listParameters: listParametersType = {
           page: page,
           per_page: perPage,
           direction: direction,
           search: search,
           sort: sort
       };
       dispatch(fetchRPTCodes(listParameters));
   }, [dispatch, RPTsuccess, page, perPage, direction, search, sort]);

   useEffect(() => {
       setData(rptCodeList?.result! || []);
   }, [rptCodeList])

   //  handel error 
   useEffect(() => {
    if (error != null && typeof error === 'object' && 'message' in error) {
        dispatch(
            openSnackbar({
                open: true,
                message: (error as { message: string }).message, // Type assertion
                variant: 'alert',
                alert: {
                    color: 'error'
                },
                close: true
            })
        );
        dispatch(toInitialState());
    }
}, [error]);

   //  handel success
   useEffect(() => {
       if (RPTsuccess != null) {
           dispatch(
               openSnackbar({
                   open: true,
                   message: RPTsuccess,
                   variant: 'alert',
                   alert: {
                       color: 'success'
                   },
                   close: true
               })
           );
           dispatch(toInitialState())
       }
   }, [RPTsuccess])


   useEffect(()=>{
    if (isActionSuccess != null) {
        let actionId, actionName ,keyValue,description;

        switch (isActionSuccess) {
            case 'LIST':
                actionId = 2;
                actionName = 'LIST';
                keyValue = 0 ;
                description='Get RPT List'
                break;
            case 'CREATE':
                actionId = 1;
                actionName = 'CREATE';
                keyValue = 0 ;
                description='Create a RPT Code'
                break;
            case 'UPDATE':
                actionId = 3;
                actionName = 'UPDATE';
                keyValue = rptCode?.rptCodeId! || 0 ;
                description=`Edit RPT Details : ${rptCode?.rptCodeId} `
                break;
            case 'INACTIVE':
                actionId = 12;
                actionName = 'INACTIVE';
                keyValue = rptCodeId || 0;
                description=`Delete RPT: -${rptCodeId}`
                
                break;
            default:
                return; // Exit early if no valid action is found
        }
        dispatch(AddActivityLog({
            actionId: actionId,
            actionName: actionName,
            branchId: user?.branchList?.[0]?.branchId ?? undefined,
            companyId: user?.companyId,
            deptId: user?.departmentList?.[0]?.departmentId ?? undefined,
            description: description,
            keyValue: "rptCodeID",
            keyValueId: keyValue,
            menuId: 20,
            menuName: "RPT Code",
            deptName: user?.departmentList?.[0]?.deptName ?? undefined
        }));

        dispatch(toResetIsActionSuccessState());
    }

},[isActionSuccess]);

   // if (isLoading) {
   //     return <div>Loading...</div>;
   // }

    return (
        <>
            <MainCard content={false}>
                <ScrollX>
                    <ReactTable columns={columns}
                    getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()}
                    data={data} handleAddEdit={handleAddEdit} tableParams={tableParams} />
                </ScrollX>
                {/* add / edit user dialog */}
                <Dialog
                    maxWidth="sm"
                    TransitionComponent={PopupTransition}
                    keepMounted
                    fullWidth
                    onClose={handleAddEdit}
                    open={addEdit}
                    sx={{ '& .MuiDialog-paper': { p: 0 }, transition: 'transform 225ms' }}
                    aria-describedby="alert-dialog-slide-description"
                >
                    <AddEditRptCode rptCode={rptCode} onCancel={handleAddEdit} />
                </Dialog>
                {/* alert model */}
                {rptCodeId && <AlertRptCodeDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={rptCodeId} />}
            </MainCard>
        </>
    )
};

export default List;
