
// material-ui
import {
    // Autocomplete,
    Button,
    Divider,
    // FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField
} from '@mui/material';
// import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports

// assets
// import { optionType, options } from 'data/options';
import { useEffect, useState } from 'react';
import { dispatch, useSelector } from 'store';
import { fetchGlobalDetailsSuccess, toInitialState, UpdateGlobalDetailLimit } from 'store/reducers/global-details';
import { openSnackbar } from 'store/reducers/snackbar';
import { dataProps } from '../../../sections/applications/global-configuration/ApplicationSettings/types/types';
// types

// constant
const getInitialValues = (limit: FormikValues | null) => {

    const newLimit = {
        goamlLimit: undefined,
    }

    if (limit) {
        return _.merge({}, newLimit, limit);
    }

    return newLimit;
};

// ==============================|| Limit Update ||============================== //

export interface Props {
    limit?: any
}

const Limit = ({ limit }: Props) => {
    // const theme = useTheme();
    // //================================API CONFIG=============================//
    const { globalDetails, isLoading, error, success } = useSelector(state => state.globalDetails)

    const [data, setData] = useState<dataProps | null>(null)


    const limitSchema = Yup.object().shape({

        goamlLimit: Yup.string().required('New limit is required'),

    });

    const formik = useFormik({
        initialValues: getInitialValues(data!),
        validationSchema: limitSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                // Create the object with only globalID and goamlLimit
                const updatePayload = {
                    globalID: globalDetails?.result?.globalID,  // Extract the globalID from the current globalDetails
                    goamlLimit: values.goamlLimit              // Use the new goamlLimit from the form
                };

                // Dispatch the object
                dispatch(UpdateGlobalDetailLimit(updatePayload));
                resetForm()
                setSubmitting(false);
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

    // ----------------------- | API Call - ApplicationSettings | ---------------------

    useEffect(() => {
        dispatch(fetchGlobalDetailsSuccess())
    }, [success])

    useEffect(() => {
        if (!globalDetails) {
            setData(null)
            return
        }
        if (globalDetails == null) {
            setData(null)
            return
        }
        setData(globalDetails.result!)
    }, [globalDetails])

    useEffect(() => {
        if (error != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: "Error",
                    variant: 'alert',
                    alert: {
                        color: 'error'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [error])

    useEffect(() => {
        if (success != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: success,
                    variant: 'alert',
                    alert: {
                        color: 'success'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [success])

    if (isLoading) {
        return <>Loading...</>
    }


    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <Grid container spacing={3}>
                            <Grid item xs={12} md={12}>
                                <Grid container spacing={3}>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                            <InputLabel htmlFor="ExistingLimit">Existing Limit </InputLabel>
                                            <TextField
                                                fullWidth
                                                id="ExistingLimit"
                                                placeholder="Existing Limit"
                                                value={globalDetails?.result?.goamlLimit!}
                                                disabled
                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                            <InputLabel htmlFor="goamlLimit">New Limit<span style={{ color: 'red' }}>*</span></InputLabel>
                                            <TextField
                                                fullWidth
                                                id="goamlLimit"
                                                placeholder="Enter new Limit"
                                                {...getFieldProps('goamlLimit')}
                                                error={Boolean(touched.goamlLimit && errors.goamlLimit)}
                                                helperText={touched.goamlLimit && errors.goamlLimit}
                                            />
                                        </Stack>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>

                        <Divider />
                        <Grid container justifyContent="flex-end" alignItems="center" style={{ marginTop: '16px' }}>
                            <Grid item>
                                {/* Add marginRight to align button with some space */}
                                <Stack direction="row" spacing={2} alignItems="center" style={{ marginRight: '16px' }}>
                                    <Button type="submit" variant="contained" disabled={isSubmitting}>
                                        {limit ? 'Edit' : 'Add'}
                                    </Button>
                                </Stack>
                            </Grid>
                        </Grid>
                    </Form>
                </LocalizationProvider>
            </FormikProvider>
        </>
    );
};

export default Limit;
