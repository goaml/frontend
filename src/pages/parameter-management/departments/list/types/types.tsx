import { Dispatch, SetStateAction } from 'react';
import { Column, HeaderGroup } from 'react-table';
import { DepartmentCodeResType, DepartmentCodeType } from 'types/department-para';

export interface dataProps extends DepartmentCodeResType { }

export interface ReactTableProps {
  columns: Column[]
  data: dataProps[]
  handleAddEdit: () => void
  getHeaderProps: (column: HeaderGroup) => {}
  tableParams: TableParamsType
}

export interface departmentProps extends DepartmentCodeType { }

export interface TableParamsType {
  page: number;
  setPage: Dispatch<SetStateAction<number>>;
  perPage: number;
  setPerPage: Dispatch<SetStateAction<number>>;
  direction: "asc" | "desc";
  setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
  sort: string;
  setSort: Dispatch<SetStateAction<string>>;

}
