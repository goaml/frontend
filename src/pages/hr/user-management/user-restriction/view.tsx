import * as React from 'react';

// material-ui
import {
    Autocomplete,
    Grid,
    InputLabel,
    Stack,
    TextField
} from '@mui/material';
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import Typography from '@mui/material/Typography';
import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import MainCard from 'components/MainCard';

// third-party
import { Form, FormikProvider, useFormik } from 'formik';
import { useEffect, useState } from 'react';
import GrantedUserPermissionTable from 'sections/hr/user-management/user-restriction/GrantedUserPermissionTable';
import RestrictedUserAccessTable from 'sections/hr/user-management/user-restriction/RestrictedUserAccessTable';
import { dispatch, useSelector } from 'store';
import { fetchGetAllBranchesOfUser } from 'store/reducers/branch';
import { openSnackbar } from 'store/reducers/snackbar';
import { fetchGetAllUserSuccess } from 'store/reducers/users';
import { toInitialState } from 'store/reducers/user-restrictions';
import { Branches } from 'types/branch';
import { Users, queryParamsProps } from 'types/users';
import * as Yup from 'yup';

// project import

// assets

//types  
interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}

function CustomTabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

function a11yProps(index: number) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

// ==============================|| View ||============================== //

const View = () => {
    const theme = useTheme();

    // form
    const formik = useFormik({
        initialValues: {
            userId: undefined,
            branchId: undefined,
        },
        validationSchema: Yup.object().shape({
            userId: Yup.number().required('User is required'),
            branchId: Yup.number().required('Branch is required'),
        }),
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                // API call

                resetForm();
                setSubmitting(false);
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { handleSubmit } = formik;

    // tab
    const [value, setValue] = React.useState(0);

    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        setValue(newValue);
    };

    const { success, selectUsers } = useSelector((state) => state.user);
    const { selectBranches, error } = useSelector((state) => state.branch);
    const { isPostSuccess, postsuccess } = useSelector((state) => state.userRestriction)

    const [userId, setUserId] = useState<any>(undefined)
    const [branchId, setBranchId] = useState<any>(undefined)

    //USER DROPDOWN 
    useEffect(() => {
        const queryParams: queryParamsProps = {
            direction: "asc",
            page: 0,
            per_page: 1000,
            search: "",
            sort: "userId"
        }

        dispatch(fetchGetAllUserSuccess(queryParams))
    }, [success])

    // BRANCH DROPDOWN
    useEffect(() => {
        if (typeof userId == "undefined") return
        dispatch(fetchGetAllBranchesOfUser(userId!))
    }, [success, userId])

    useEffect(() => {
        if (error != null) {

            let defaultErrorMessage = "ERROR";
            // @ts-ignore
            const errorExp = error as Template1Error
            if (errorExp.message) {
                defaultErrorMessage = errorExp.message
            }
            dispatch(
                openSnackbar({
                    open: true,
                    message: defaultErrorMessage,
                    variant: 'alert',
                    alert: {
                        color: 'error'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [error]);


    useEffect(() => {
        if (postsuccess != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: postsuccess,
                    variant: 'alert',
                    alert: {
                        color: 'success'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [isPostSuccess])


    // if (isLoading) {
    //     return <>Loading...</>
    // }

    return (
        <>
            <MainCard content={false} sx={{ mb: 2 }}>
                <Stack direction="column" spacing={2} justifyContent="space-between" sx={{ padding: 2 }}>
                    <FormikProvider value={formik}>
                        <LocalizationProvider dateAdapter={AdapterDateFns}>
                            <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
                                <Grid container spacing={2}>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                            <InputLabel htmlFor="userId">Login ID <span style={{ color: 'red' }}>*</span></InputLabel>
                                            <Autocomplete
                                                fullWidth
                                                id="userId"
                                                value={selectUsers?.find((option: Users) => option?.userId === userId) || null}
                                                onChange={(event: any, newValue: Users | null) => {
                                                    setUserId(newValue?.userId);
                                                }}
                                                options={selectUsers || []}
                                                getOptionLabel={(item) => `${item.userName}`}
                                                renderInput={(params) => (
                                                    <TextField
                                                        {...params}
                                                        placeholder="Select Login ID"
                                                        sx={{ '& .MuiAutocomplete-input.Mui-disabled': { WebkitTextFillColor: theme.palette.text.primary } }}
                                                    />
                                                )}
                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                            <InputLabel htmlFor="userId">Branch ID <span style={{ color: 'red' }}>*</span></InputLabel>
                                            <Autocomplete
                                                fullWidth
                                                id="branchId"
                                                value={selectBranches?.find((option: Branches) => option.branchId === branchId) || null}
                                                onChange={(event: any, newValue: Branches | null) => {
                                                    setBranchId(newValue?.branchId)
                                                }}
                                                options={selectBranches || []}
                                                getOptionLabel={(item) => `${item.branchName}`}
                                                renderInput={(params) => (
                                                    <TextField
                                                        {...params}
                                                        placeholder="Select Branch ID"
                                                        sx={{ '& .MuiAutocomplete-input.Mui-disabled': { WebkitTextFillColor: theme.palette.text.primary } }}
                                                    />
                                                )}
                                            />
                                        </Stack>
                                    </Grid>
                                </Grid>
                            </Form>
                        </LocalizationProvider>
                    </FormikProvider >
                </Stack>
            </MainCard>
            <MainCard content={false} sx={{ mb: 2 }}>
                <Box sx={{ width: '100%' }}>
                    <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                        <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                            <Tab label="Granted User Permissions" {...a11yProps(0)} />
                            <Tab label="Restricted User Access" {...a11yProps(1)} />
                        </Tabs>
                    </Box>
                    <CustomTabPanel value={value} index={0}>
                        <GrantedUserPermissionTable userId={userId!} branchId={branchId!} />
                    </CustomTabPanel>
                    <CustomTabPanel value={value} index={1}>
                        <RestrictedUserAccessTable userId={userId!} branchId={branchId!} />
                    </CustomTabPanel>
                </Box>
            </MainCard>
        </>
    );
}

export default View;
