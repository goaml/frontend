import * as React from 'react';

// material-ui
import {
    Autocomplete,
    Grid,
    InputLabel,
    Stack,
    TextField
} from '@mui/material';
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import Typography from '@mui/material/Typography';
import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import MainCard from 'components/MainCard';

// third-party
import { Form, FormikProvider, useFormik } from 'formik';
import { useEffect, useState } from 'react';
import GrantedRolePermissionTable from 'sections/hr/user-management/user-role-restriction/GrantedRolePermissionTable';
import RestrictedRoleAccessTable from 'sections/hr/user-management/user-role-restriction/RestrictedRoleAccessTable';
import { dispatch, useSelector } from 'store';
import { fetchSelectRoles } from 'store/reducers/role';
import { Roles, queryParamsProps } from 'types/role';
import * as Yup from 'yup';

// project import

// assets

//types  
interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}

function CustomTabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

function a11yProps(index: number) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

// ==============================|| View ||============================== //

const View = () => {
    const theme = useTheme();

    // form
    const formik = useFormik({
        initialValues: {
            userRoleId: undefined,
        },
        validationSchema: Yup.object().shape({
            roleId: Yup.number().required('Role is required'),
        }),
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                // API call

                resetForm();
                setSubmitting(false);
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { handleSubmit } = formik;

    // tab
    const [value, setValue] = React.useState(0);

    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        setValue(newValue);
    };

    const [userRoleid, setRoleId] = useState<number | undefined>(undefined)

    const { success, selectRoles } = useSelector((state) => state.roles);

    useEffect(() => {
        const queryParams: queryParamsProps = {
            direction: "asc",
            page: 0,
            per_page: 1000,
            search: "",
            sort: "userRoleId"
        }

        dispatch(fetchSelectRoles(queryParams))
    }, [success])

    return (
        <>
            <MainCard content={false} sx={{ mb: 2 }}>
                <Stack direction="column" spacing={2} justifyContent="space-between" sx={{ padding: 2 }}>
                    <FormikProvider value={formik}>
                        <LocalizationProvider dateAdapter={AdapterDateFns}>
                            <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
                                <Grid container spacing={3}>
                                    <Grid item xs={4}>
                                        <Stack spacing={1.25}>
                                            <InputLabel htmlFor="roleId">Role <span style={{ color: 'red' }}>*</span></InputLabel>
                                            <Autocomplete
                                                fullWidth
                                                id="userRoleId"
                                                value={selectRoles?.find((option: Roles) => option.userRoleId === formik.values.userRoleId) || null}
                                                onChange={(event: any, newValue: Roles | null) => {
                                                    formik.setFieldValue('userRoleId', newValue?.userRoleId);
                                                    setRoleId(newValue?.userRoleId)
                                                }}
                                                options={selectRoles || []}
                                                getOptionLabel={(item) => `${item.userRoleName}`}
                                                renderInput={(params) => (
                                                    <TextField
                                                        {...params}
                                                        placeholder="Select User Role"
                                                        sx={{ '& .MuiAutocomplete-input.Mui-disabled': { WebkitTextFillColor: theme.palette.text.primary } }}
                                                    />
                                                )}
                                            />
                                        </Stack>
                                    </Grid>
                                </Grid>
                            </Form>
                        </LocalizationProvider>
                    </FormikProvider >
                </Stack>
            </MainCard>
            <MainCard content={false} sx={{ mb: 2 }}>
                <Box sx={{ width: '100%' }}>
                    <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                        <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                            <Tab label="Granted Role Permissions" {...a11yProps(0)} />
                            <Tab label="Restricted Role Access" {...a11yProps(1)} />
                        </Tabs>
                    </Box>
                    <CustomTabPanel value={value} index={0}>
                        <GrantedRolePermissionTable userRoleid={userRoleid!} />
                    </CustomTabPanel>
                    <CustomTabPanel value={value} index={1}>
                        <RestrictedRoleAccessTable userRoleid={userRoleid!} />
                    </CustomTabPanel>
                </Box>
            </MainCard>
        </>
    );
}

export default View;
