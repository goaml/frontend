import { Column, HeaderGroup } from 'react-table';
import { Roles } from 'types/role';

export interface dataProps extends Roles { }

export interface ReactTableProps {
    columns: Column[]
    data: dataProps[]
    handleAddEdit: () => void
    getHeaderProps: (column: HeaderGroup) => {};
}
export interface TableHeaderProps {
    headerGroups: HeaderGroup[];

}

export interface userProps extends Roles { }

