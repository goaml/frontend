import { Column } from 'react-table';
import { HeaderGroup } from 'react-table';
import { GlobalDetails } from 'types/global-details';

export interface TableProps {
    columns: Column[];
    data: [];
    getHeaderProps: (column: HeaderGroup) => {};

}

export interface TableHeaderProps {
    headerGroups: HeaderGroup[];

}

export interface dataProps extends GlobalDetails { }

export interface ReactTableProps {
    columns: Column[]
    data: dataProps[]
    handleAddEdit: () => void
}

export interface civilStatusProps extends GlobalDetails { }





