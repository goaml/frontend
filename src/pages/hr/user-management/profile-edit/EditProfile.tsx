import {
    Button,
    Grid,
    InputLabel,
    Stack,
    TextField
} from '@mui/material';

// third party
import { FormikValues, useFormik } from 'formik';

// project import
import MainCard from 'components/MainCard';
import useAuth from 'hooks/useAuth';
import _ from 'lodash';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'store';
import { openSnackbar } from 'store/reducers/snackbar';
import { getUserById, toInitialState, updateUserSuccess } from 'store/reducers/users';
import { Users } from 'types/users';
import { AddActivityLog } from 'store/reducers/note-log';
import { toResetIsActionSuccessState } from 'store/reducers/user';

const getInitialValues = (userById: FormikValues | null) => {

    const newUser = {
        userRoleId: undefined,
        branchId: undefined,
        userTypeId: undefined,
        //userName: "",
        email: "",
        mobileNo: "",
        firstName: "",
        lastName: "",
        nic: "",
        statusId: undefined,
        isActive: true,
        referenceNo: "",
        // employeeNo: ""
    }

    if (userById) {
        return _.merge({}, newUser, userById);
    }

    return newUser;
};

export interface Props {
    userById?: Users
}
export interface dataProps extends Users { }

const TabProfile = ({ }: Props) => {

    const { user } = useAuth()
    const [data, setData] = useState<dataProps>()
    console.log(data);

    const dispatch = useDispatch();
    const { userById, error, EditProfileSuccess,isActionSuccess } = useSelector(state => state.users)

    useEffect(() => {
        dispatch(getUserById(user?.id!))
    }, [user])

    useEffect(() => {
        if (!userById) {
            setData(undefined)
            return
        }
        if (userById == null) {
            setData(undefined)
            return
        }
        setData(userById)
    }, [userById])

    useEffect(() => {
        if (error != null) {

            let defaultErrorMessage = "ERROR";
            // @ts-ignore
            const errorExp = error as Template1Error
            if (errorExp.message) {
                defaultErrorMessage = errorExp.message
            }
            dispatch(
                openSnackbar({
                    open: true,
                    message: defaultErrorMessage,
                    variant: 'alert',
                    alert: {
                        color: 'error'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [error]);

    useEffect(() => {
        if (EditProfileSuccess != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: EditProfileSuccess,
                    variant: 'alert',
                    alert: {
                        color: 'success'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [EditProfileSuccess])

    useEffect(()=>{
        if (isActionSuccess != null) {
            let actionId, actionName ,keyValue,description;
    
            switch (isActionSuccess) {
                case 'LIST':
                    actionId = 2;
                    actionName = 'LIST';
                    keyValue = 0 ;
                    description='Get All User Types'
                    break;
                case 'CREATE':
                    actionId = 1;
                    actionName = 'CREATE';
                    keyValue = 0 ;
                    description='Create a User Types'
                    break;
                case 'UPDATE':
                    actionId = 3;
                    actionName = 'UPDATE';
                    keyValue = user?.userId! || 0 ;
                    description=`Edit User  Details  `
                    break;
                case 'INACTIVE':
                    actionId = 12;
                    actionName = 'INACTIVE';
                    keyValue = user?.userId! || 0;
                    description=`Inactive User Type  `
                    break;
                default:
                    return; // Exit early if no valid action is found
            }
            dispatch(AddActivityLog({
                actionId: actionId,
                actionName: actionName,
                branchId: user?.branchList?.[0]?.branchId ?? undefined,
                companyId: user?.companyId,
                deptId: user?.departmentList?.[0]?.departmentId ?? undefined,
                description: description,
                keyValue: "userTypeId",
                keyValueId: keyValue,
                menuId: 6,
                menuName: "Users",
                deptName: user?.departmentList?.[0]?.deptName ?? undefined
            }));
    
            dispatch(toResetIsActionSuccessState());
        }
    
    },[isActionSuccess]);
    // ----------------------- | API Call - Edit Profile | ---------------------

    const formik = useFormik({
        initialValues: getInitialValues(userById!),
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (userById) {
                    // PUT API
                    dispatch(updateUserSuccess({ ...values }))
                }
                resetForm()
                setSubmitting(false);
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { getFieldProps, isSubmitting, handleSubmit } = formik;

    return (

        <Grid item xs={12} sm={6} spacing={2}>
            <MainCard title="Edit Profile Details">
                <form noValidate onSubmit={handleSubmit}>
                    <Grid container spacing={3}>
                        <Grid item xs={6}>
                            <Stack spacing={1}>
                                <InputLabel htmlFor="companyname-global">User ID</InputLabel>
                                <TextField
                                    fullWidth
                                    placeholder="Enter UserID"
                                    id="userId"
                                    {...getFieldProps('userId')}
                                    disabled
                                />
                            </Stack>
                        </Grid>
                        <Grid item xs={6}>
                            <Stack spacing={1}>
                                <InputLabel htmlFor="fronturl-global">Mobile Number</InputLabel>
                                <TextField
                                    fullWidth
                                    placeholder="Enter Mobile No"
                                    id="mobileNo"
                                    {...getFieldProps('mobileNo')}
                                    onChange={(event) => {
                                        formik.setFieldValue('mobileNo', event.target.value);
                                    }}
                                />
                            </Stack>
                        </Grid>
                        <Grid item xs={6}>
                            <Stack spacing={1}>
                                <InputLabel htmlFor="fronturl-global">First Name</InputLabel>
                                <TextField
                                    fullWidth
                                    placeholder="Enter First Name"
                                    id="firstName"
                                    {...getFieldProps('firstName')}
                                    onChange={(event) => {
                                        formik.setFieldValue('firstName', event.target.value);
                                    }}
                                />
                            </Stack>
                        </Grid>
                        <Grid item xs={6}>
                            <Stack spacing={1}>
                                <InputLabel htmlFor="fronturl-global">Last Name</InputLabel>
                                <TextField
                                    fullWidth
                                    placeholder="Enter Last Name"
                                    id="lastName"
                                    {...getFieldProps('lastName')}
                                    onChange={(event) => {
                                        formik.setFieldValue('lastName', event.target.value);
                                    }}
                                />
                            </Stack>
                        </Grid>
                        <Grid item xs={6}>
                            <Stack spacing={1}>
                                <InputLabel htmlFor="footertitle-global">Email Address</InputLabel>
                                <TextField
                                    fullWidth
                                    placeholder="Enter Email"
                                    id="email"
                                    {...getFieldProps('email')}
                                    onChange={(event) => {
                                        formik.setFieldValue('email', event.target.value);
                                    }}
                                />

                            </Stack>
                        </Grid>
                        <Grid item xs={6}>
                            <Stack spacing={1}>
                                <InputLabel htmlFor="domaindescription-global">User Name</InputLabel>
                                <TextField
                                    fullWidth
                                    placeholder="Enter User Name"
                                    id="userName"
                                    {...getFieldProps('userName')}
                                    onChange={(event) => {
                                        formik.setFieldValue('userName', event.target.value);
                                    }}
                                />
                            </Stack>
                        </Grid>
                    </Grid>
                    <Grid container justifyContent="space-between" alignItems="center" style={{ marginTop: '20px' }}>
                        <Grid item xs={6}>
                        </Grid>
                        <Grid item xs={6} style={{ display: 'flex', justifyContent: 'flex-end' }}>
                            <Stack direction="row" spacing={2} alignItems="center">
                                <Button type="submit" variant="contained" disabled={isSubmitting}>
                                    Update
                                </Button>
                            </Stack>
                        </Grid>
                    </Grid>
                </form>
                {/* <Grid container justifyContent="space-between" alignItems="center" style={{ marginTop: '20px' }}>
                    <Grid item xs={6}>
                    </Grid>
                    <Grid item xs={6} style={{ display: 'flex', justifyContent: 'flex-end' }}>
                        <Stack direction="row" spacing={2} alignItems="center">
                            <Button type="submit" variant="contained" disabled={isSubmitting}>
                                Update
                            </Button>
                        </Stack>
                    </Grid>
                </Grid> */}
            </MainCard>
        </Grid>
    );
};

export default TabProfile;