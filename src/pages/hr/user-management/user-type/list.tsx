/* eslint-disable prettier/prettier */
import { Fragment, MouseEvent, useEffect, useMemo, useState } from 'react';

// material-ui
import {
    Button,
    Dialog,
    IconButton,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Tooltip,
    Typography,
    alpha,
    useMediaQuery,
    useTheme
} from '@mui/material';

// third-party
import { Cell, Column, HeaderGroup, Row, useExpanded, useFilters, useGlobalFilter, usePagination, useRowSelect, useSortBy, useTable } from 'react-table';

// project import
import { PopupTransition } from 'components/@extended/Transitions';
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import { HeaderSort, SortingSelect, TablePagination, TableRowSelection } from 'components/third-party/ReactTable';

import {
    GlobalFilter,
    renderFilterTypes
} from 'utils/react-table';

// assets
import { DeleteTwoTone, EditTwoTone, PlusOutlined } from '@ant-design/icons';

//types
import Dot from 'components/@extended/Dot';
import useAuth from 'hooks/useAuth';
import AddEditTypeStatus from 'sections/hr/user-management/user-type/AddEditType';
import AlertTypeDelete from 'sections/hr/user-management/user-type/AlertTypeDelete';
import { useDispatch, useSelector } from 'store';
import { AddActivityLog } from 'store/reducers/note-log';
import { openSnackbar } from 'store/reducers/snackbar';
import { fetchUserTypes, toInitialState, toResetIsActionSuccessState } from 'store/reducers/user-type';
import { ColorProps } from 'types/extended';
import { listParametersType } from 'types/user-type';
import { ReactTableProps, dataProps, userProps } from './types/types';
//import Dot from 'components/@extended/Dot';
interface Props {
    status: boolean;
}
const IsActiveStatus = ({ status }: Props) => {
    let color: ColorProps;
    let title: string;

    switch (status) {
        case true:
            color = 'success';
            title = 'Active';
            break;
        case false:
            color = 'error';
            title = 'In-Active';
            break;
        default:
            color = 'primary';
            title = 'None';
    }

    return (
        <Stack direction="row" spacing={1} alignItems="center">
            <Dot color={color} />
            <Typography>{title}</Typography>
        </Stack>
    );
};
// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, handleAddEdit, getHeaderProps }: ReactTableProps) {
    const theme = useTheme();

    const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));
    const sortBy = { id: 'id', desc: false };

    const filterTypes = useMemo(() => renderFilterTypes, []);

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        allColumns,
        rows,
        page,
        gotoPage,
        setPageSize,
        state: { globalFilter, selectedRowIds, pageIndex, pageSize },
        preGlobalFilteredRows,
        setGlobalFilter,
        setSortBy,
    } = useTable(
        {
            columns,
            data,
            filterTypes,
            initialState: { pageIndex: 0, pageSize: 10, hiddenColumns: ['avatar', 'email'], sortBy: [sortBy] }
        },
        useGlobalFilter,
        useFilters,
        useSortBy,
        useExpanded,
        usePagination,
        useRowSelect
    );

    return (
        <>
            <TableRowSelection selected={Object.keys(selectedRowIds).length} />
            <Stack spacing={3}>
                <Stack
                    direction={matchDownSM ? 'column' : 'row'}
                    spacing={1}
                    justifyContent="space-between"
                    alignItems="center"
                    sx={{ p: 3, pb: 0 }}
                >
                    <GlobalFilter
                        preGlobalFilteredRows={preGlobalFilteredRows}
                        globalFilter={globalFilter}
                        setGlobalFilter={setGlobalFilter}
                        size="small"
                    />
                    <Stack direction={matchDownSM ? 'column' : 'row'} alignItems="center" spacing={1}>
                        <SortingSelect sortBy={sortBy.id} setSortBy={setSortBy} allColumns={allColumns} />
                        <Button variant="contained" startIcon={<PlusOutlined />} onClick={handleAddEdit} size="small">
                            Add
                        </Button>

                    </Stack>
                </Stack>
                <Table {...getTableProps()}>
                    <TableHead>
                        {headerGroups.map((headerGroup: HeaderGroup<{}>) => (
                            <TableRow {...headerGroup.getHeaderGroupProps()} sx={{ '& > th:first-of-type': { width: '58px' } }}>
                                {headerGroup.headers.map((column: HeaderGroup) => (
                                    <TableCell {...column.getHeaderProps([{ className: column.className }, getHeaderProps(column)])}>
                                        <HeaderSort column={column} />
                                    </TableCell>
                                ))}
                            </TableRow>
                        ))}
                    </TableHead>
                    <TableBody {...getTableBodyProps()}>
                        {page.map((row: Row, i: number) => {
                            prepareRow(row);
                            return (
                                <Fragment key={i}>
                                    <TableRow
                                        {...row.getRowProps()}
                                        onClick={() => {
                                            row.toggleRowSelected();
                                        }}
                                        sx={{ cursor: 'pointer', bgcolor: row.isSelected ? alpha(theme.palette.primary.lighter, 0.35) : 'inherit' }}
                                    >
                                        {row.cells.map((cell: Cell) => (
                                            <TableCell {...cell.getCellProps([{ className: cell.column.className }])}>{cell.render('Cell')}</TableCell>
                                        ))}
                                    </TableRow>
                                    {/* {row.isExpanded && renderRowSubComponent({ row, rowProps, visibleColumns, expanded })} */}
                                </Fragment>
                            );
                        })}
                        <TableRow>
                            <TableCell sx={{ p: 2 }} colSpan={12}>
                                <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </Stack>
        </>
    );
}

// ==============================|| List ||============================== //

const List = () => {
    const theme = useTheme();
    const dispatch = useDispatch();
    const { userTypeList, isLoading, error, typesuccess, isActionSuccess } = useSelector(state => state.userType)
    const { user } = useAuth();
    // table 
    const [data, setData] = useState<dataProps[]>([])

    const columns = useMemo(
        () =>
            [
                {
                    Header: '',
                    accessor: 'id',
                    className: 'cell-center',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.id === undefined || row.id === null || row.id === '') {
                            return <>-</>
                        }
                        if (typeof row.id === 'string') {
                            return <>{(parseInt(row.id) + 1).toString()}</>;
                        }
                        if (typeof row.id === 'number') {
                            return <>{row.id + 1}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Type Name',
                    accessor: 'userCategory',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.userCategory === undefined || row.values.userCategory === null || row.values.userCategory === '') {
                            return <>-</>
                        }
                        if (typeof row.values.userCategory === 'string') {
                            return <>{row.values.userCategory}</>;
                        }
                        if (typeof row.values.userCategory === 'number') {
                            return <>{row.values.userCategory}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: ' Login Type',
                    accessor: 'usRLoginType',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.usRLoginType.typeCode === undefined || row.values.usRLoginType.typeCode === null || row.values.usRLoginType.typeCode === '') {
                            return <>-</>
                        }
                        if (typeof row.values.usRLoginType.typeCode === 'string') {
                            return <>{row.values.usRLoginType.typeCode}</>;
                        }
                        if (typeof row.values.usRLoginType.typeCode === 'number') {
                            return <>{row.values.usRLoginType.typeCode}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Is Active',
                    accessor: 'isActive',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.isActive === undefined || row.values.isActive === null) {
                            return <>-</>;
                        }
                        if (typeof row.values.isActive === 'boolean') {
                            return (
                                <>
                                    <IsActiveStatus status={row.values.isActive} />{' '}
                                </>
                            );
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    id: "actions",
                    Header: 'Actions',
                    accessor: 'actions',
                    className: 'cell-left',
                    Cell: ({ row }: { row: Row }) => {
                        return (
                            <>
                                <Stack direction="row" spacing={0}>
                                    <Tooltip title="Edit">
                                        <IconButton
                                            color="primary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                //@ts-ignore
                                                const data: Types = row.original;
                                                e.stopPropagation();
                                                setTypes({ ...data });
                                                handleAddEdit();
                                            }}
                                        >
                                            <EditTwoTone twoToneColor={theme.palette.primary.main} />
                                        </IconButton>
                                    </Tooltip>
                                    <Tooltip title="Delete">
                                        <IconButton
                                            color="error"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                //@ts-ignore
                                                let data: Types = row.original;
                                                e.stopPropagation();
                                                setuserTypeId(data.userTypeId!)
                                                setOpenAlert(true)
                                            }}
                                        >
                                            <DeleteTwoTone twoToneColor={theme.palette.error.main} />
                                        </IconButton>
                                    </Tooltip>
                                </Stack>
                            </>
                        )
                    }
                }
            ] as Column[],
        []
    );

    //dialog model 
    const [addEdit, setAddEdit] = useState<boolean>(false);
    const [Types, setTypes] = useState<userProps>();

    const handleAddEdit = () => {
        setAddEdit(!addEdit);
        if (Types && !addEdit) setTypes(undefined);
    };

    //alert model
    const [openAlert, setOpenAlert] = useState(false);
    const [userTypeId, setuserTypeId] = useState<number | null>(null)

    const handleAlertClose = () => {
        setOpenAlert(!openAlert);
    };

    // ----------------------- | API Call - Types | ---------------------

    useEffect(() => {
        const listParams: listParametersType = {
            direction: "desc",
            page: 0,
            per_page: 1000,
            search: "",
            sort: "userTypeId"
        }

        dispatch(fetchUserTypes(listParams))
    }, [typesuccess])

    useEffect(() => {
        if (!userTypeList) {
            setData([])
            return
        }
        if (userTypeList == null) {
            setData([])
            return
        }
        setData(userTypeList.result!)
    }, [userTypeList])

    useEffect(() => {
        if (error != null) {
            let defaultErrorMessage = "ERROR";
            // @ts-ignore
            const errorExp = error as Template1Error
            if (errorExp.message) {
                defaultErrorMessage = errorExp.message
            }
            dispatch(
                openSnackbar({
                    open: true,
                    message: defaultErrorMessage,
                    variant: 'alert',
                    alert: {
                        color: 'error'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [error]);

    useEffect(() => {
        if (typesuccess != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: typesuccess,
                    variant: 'alert',
                    alert: {
                        color: 'success'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [typesuccess])

    useEffect(() => {
        if (isActionSuccess != null) {
            let actionId, actionName, keyValueId, keyValue, description;

            switch (isActionSuccess) {
                case 'LIST':
                    actionId = 2;
                    actionName = 'LIST';
                    keyValueId = 0;
                    keyValue = 'N/A';
                    description = 'Get All User Types'
                    break;
                case 'CREATE':
                    actionId = 1;
                    actionName = 'CREATE';
                    keyValueId = 0;
                    keyValue = 'N/A';
                    description = 'Create a User Types'
                    break;
                case 'UPDATE':
                    actionId = 3;
                    actionName = 'UPDATE';
                    keyValueId = user?.userId! || 0;
                    keyValue = Types?.userCategory || 'N/A';
                    description = `Edit User Type Details : ${Types?.userTypeId} `
                    break;
                case 'INACTIVE':
                    actionId = 12;
                    actionName = 'INACTIVE';
                    keyValueId = userTypeId || 0;
                    keyValue = Types?.userCategory || 'N/A';
                    description = `Inactive User Type : ${userTypeId} `
                    break;
                default:
                    return; // Exit early if no valid action is found
            }
            dispatch(AddActivityLog({
                actionId: actionId,
                actionName: actionName,
                branchId: user?.branchList?.[0]?.branchId ?? undefined,
                companyId: user?.companyId,
                deptId: user?.departmentList?.[0]?.departmentId ?? undefined,
                description: description,
                keyValue: keyValue,
                keyValueId: keyValueId,
                menuId: 6,
                menuName: "Users",
                deptName: user?.departmentList?.[0]?.deptName ?? undefined
            }));

            dispatch(toResetIsActionSuccessState());
        }

    }, [isActionSuccess]);

    if (isLoading) {
        return <>Loading...</>
    }

    return (
        <>
            <MainCard content={false}>
            <ScrollX>
                    <ReactTable columns={columns}
                        getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()}
                        data={data} handleAddEdit={handleAddEdit} />
                </ScrollX>
                <Dialog
                    maxWidth="sm"
                    TransitionComponent={PopupTransition}
                    keepMounted
                    fullWidth
                    onClose={handleAddEdit}
                    open={addEdit}
                    sx={{ '& .MuiDialog-paper': { p: 0 }, transition: 'transform 225ms' }}
                    aria-describedby="alert-dialog-slide-description"
                >
                    <AddEditTypeStatus Types={Types} onCancel={handleAddEdit} />
                </Dialog>
                {/* alert model */}
                {userTypeId && <AlertTypeDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={userTypeId} />}
            </MainCard>
        </>
    );
}

export default List;
