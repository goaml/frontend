import { Fragment, MouseEvent, useEffect, useMemo, useState } from 'react';

// material-ui
import {
    IconButton,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Tooltip,
    Typography,
    alpha,
    useMediaQuery,
    useTheme
} from '@mui/material';

// third-party
import { HeaderSort, SortingSelect, TablePagination, TableRowSelection } from 'components/third-party/ReactTable';
import { Cell, Column, HeaderGroup, Row, useExpanded, useFilters, useGlobalFilter, usePagination, useRowSelect, useSortBy, useTable } from 'react-table';

// project import
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import {
    GlobalFilter,
    renderFilterTypes
} from 'utils/react-table';

// assets
import { CheckCircleTwoTone, CloseCircleTwoTone } from '@ant-design/icons';

//types 
import Dot from 'components/@extended/Dot';
import useAuth from 'hooks/useAuth';
import { useDispatch, useSelector } from 'store';
import { openSnackbar } from 'store/reducers/snackbar';
import { addAuthorization, fetchUsersSuccess, rejectAuthorization, toInitialState, toResetIsActionSuccessState } from 'store/reducers/user-authorization';
import { ColorProps } from 'types/extended';
import { queryParamsProps } from 'types/user-authorization';
import { ReactTableProps, dataProps, originaldataProps, userProps } from './types/types';
import { AddActivityLog } from 'store/reducers/note-log';

// Status
interface Props {
    status: boolean;
}

const IsActiveStatus = ({ status }: Props) => {
    let color: ColorProps;
    let title: string;

    switch (status) {
        case true:
            color = 'success';
            title = 'Active';
            break;
        case false:
            color = 'error';
            title = 'In-Active';
            break;
        default:
            color = 'primary';
            title = 'None';
    }

    return (
        <Stack direction="row" spacing={1} alignItems="center">
            <Dot color={color} />
            <Typography>{title}</Typography>
        </Stack>
    );
};
// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, handleAddEdit, getHeaderProps }: ReactTableProps) {
    const theme = useTheme();

    const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));
    const sortBy = { id: 'id', desc: false };

    const filterTypes = useMemo(() => renderFilterTypes, []);

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        allColumns,
        rows,
        page,
        gotoPage,
        setPageSize,
        state: { globalFilter, selectedRowIds, pageIndex, pageSize },
        preGlobalFilteredRows,
        setGlobalFilter,
        setSortBy,
    } = useTable(
        {
            columns,
            data,
            filterTypes,
            initialState: { pageIndex: 0, pageSize: 10, hiddenColumns: ['avatar', 'email'], sortBy: [sortBy] }
        },
        useGlobalFilter,
        useFilters,
        useSortBy,
        useExpanded,
        usePagination,
        useRowSelect
    );

    return (
        <>
            <TableRowSelection selected={Object.keys(selectedRowIds).length} />
            <Stack spacing={3}>
                <Stack
                    direction={matchDownSM ? 'column' : 'row'}
                    spacing={1}
                    justifyContent="space-between"
                    alignItems="center"
                    sx={{ p: 3, pb: 0 }}
                >
                    <GlobalFilter
                        preGlobalFilteredRows={preGlobalFilteredRows}
                        globalFilter={globalFilter}
                        setGlobalFilter={setGlobalFilter}
                        size="small"
                    />
                    <Stack direction={matchDownSM ? 'column' : 'row'} alignItems="center" spacing={1}>
                        <SortingSelect sortBy={sortBy.id} setSortBy={setSortBy} allColumns={allColumns} />
                    </Stack>
                </Stack>
                <Table {...getTableProps()}>
                    <TableHead>
                        {headerGroups.map((headerGroup: HeaderGroup<{}>) => (
                            <TableRow {...headerGroup.getHeaderGroupProps()} sx={{ '& > th:first-of-type': { width: '58px' } }}>
                                {headerGroup.headers.map((column: HeaderGroup) => (
                                    <TableCell {...column.getHeaderProps([{ className: column.className }, getHeaderProps(column)])}>
                                        <HeaderSort column={column} />
                                    </TableCell>
                                ))}
                            </TableRow>
                        ))}
                    </TableHead>
                    <TableBody {...getTableBodyProps()}>
                        {page.map((row: Row, i: number) => {
                            prepareRow(row);
                            return (
                                <Fragment key={i}>
                                    <TableRow
                                        {...row.getRowProps()}
                                        onClick={() => {
                                            row.toggleRowSelected();
                                        }}
                                        sx={{ cursor: 'pointer', bgcolor: row.isSelected ? alpha(theme.palette.primary.lighter, 0.35) : 'inherit' }}
                                    >
                                        {row.cells.map((cell: Cell) => (
                                            <TableCell {...cell.getCellProps([{ className: cell.column.className }])}>{cell.render('Cell')}</TableCell>
                                        ))}
                                    </TableRow>
                                    {/* {row.isExpanded && renderRowSubComponent({ row, rowProps, visibleColumns, expanded })} */}
                                </Fragment>
                            );
                        })}
                        <TableRow>
                            <TableCell sx={{ p: 2 }} colSpan={12}>
                                <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </Stack>
        </>
    );
}
// ==============================|| List ||============================== //

const List = () => {

    const theme = useTheme();
    const dispatch = useDispatch();
    const { userList, isLoading, error, success,isActionSuccess } = useSelector(state => state.userAuthorization)


    // table
    const [data, setData] = useState<dataProps[]>([])

    const { user } = useAuth()

    const columns = useMemo(
        () =>
            [
                {
                    Header: '',
                    accessor: 'id',
                    className: 'cell-center',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.id === undefined || row.id === null || row.id === '') {
                            return <>-</>
                        }
                        if (typeof row.id === 'string') {
                            return <>{(parseInt(row.id) + 1).toString()}</>;
                        }
                        if (typeof row.id === 'number') {
                            return <>{row.id + 1}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'User Id',
                    accessor: 'userName',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.userName === undefined || row.values.userName === null || row.values.userName === '') {
                            return <>-</>
                        }
                        if (typeof row.values.userName === 'string') {
                            return <>{row.values.userName}</>;
                        }
                        if (typeof row.values.userName === 'number') {
                            return <>{row.values.userName}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'User Role',
                    accessor: 'userRoleName',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.userRoleName === undefined || row.values.userRoleName === null || row.values.userRoleName === '') {
                            return <>-</>
                        }
                        if (typeof row.values.userRoleName === 'string') {
                            return <>{row.values.userRoleName}</>;
                        }
                        if (typeof row.values.userRoleName === 'number') {
                            return <>{row.values.userRoleName}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'User Type',
                    accessor: 'userTypeName',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.userTypeName === undefined || row.values.userTypeName === null || row.values.userTypeName === '') {
                            return <>-</>
                        }
                        if (typeof row.values.userTypeName === 'string') {
                            return <>{row.values.userTypeName}</>;
                        }
                        if (typeof row.values.userTypeName === 'number') {
                            return <>{row.values.userTypeName}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Email',
                    accessor: 'empEmail',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.empEmail === undefined || row.values.empEmail === null || row.values.empEmail === '') {
                            return <>-</>
                        }
                        if (typeof row.values.empEmail === 'string') {
                            return <>{row.values.empEmail}</>;
                        }
                        if (typeof row.values.empEmail === 'number') {
                            return <>{row.values.empEmail}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'NIC',
                    accessor: 'nic',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.nic === undefined || row.values.nic === null || row.values.nic === '') {
                            return <>-</>
                        }
                        if (typeof row.values.nic === 'string') {
                            return <>{row.values.nic}</>;
                        }
                        if (typeof row.values.nic === 'number') {
                            return <>{row.values.nic}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Contact No.',
                    accessor: 'empMobileNumber',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.empMobileNumber === undefined || row.values.empMobileNumber === null || row.values.empMobileNumber === '') {
                            return <>-</>
                        }
                        if (typeof row.values.empMobileNumber === 'string') {
                            return <>{row.values.empMobileNumber}</>;
                        }
                        if (typeof row.values.empMobileNumber === 'number') {
                            return <>{row.values.empMobileNumber}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Is Active',
                    accessor: 'isActive',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.isActive === undefined || row.values.isActive === null) {
                            return <>-</>;
                        }
                        if (typeof row.values.isActive === 'boolean') {
                            return (
                                <>
                                    <IsActiveStatus status={row.values.isActive} />{' '}
                                </>
                            );
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Status',
                    accessor: 'statusName',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.statusName === 'RESTRICTED') {
                            return (
                                <Stack direction="row" alignItems="center" spacing={1}>
                                    <Dot color="error" />
                                    <Typography>{row.values.statusName}</Typography>
                                </Stack>
                            );
                        }
                        else if (row.values.statusName === 'EDITED') {
                            return (
                                <Stack direction="row" alignItems="center" spacing={1}>
                                    <Dot color="primary" />
                                    <Typography>{row.values.statusName}</Typography>
                                </Stack>
                            );
                        }
                        else {
                            return (
                                <Stack direction="row" alignItems="center" spacing={1}>
                                    <Dot color="success" />
                                    <Typography>{row.values.statusName}</Typography>
                                </Stack>
                            );
                        }
                    },
                },
                {
                    id: "actions",
                    Header: 'Actions',
                    accessor: 'actions',
                    className: 'cell-center',
                    Cell: ({ row }: { row: Row }) => {
                        const isRestricted = row.values.statusName === 'RESTRICTED';
                        return (
                            <>
                                <Stack direction="row" alignItems="center" justifyContent="center" spacing={0}>
                                    <Tooltip title="Approve">
                                        <IconButton
                                            disabled={isRestricted || row.values?.statusId == 3 ? true : false}
                                            color="primary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                const aData: originaldataProps = row.original
                                                e.stopPropagation();
                                                dispatch(addAuthorization({ status: "true", userId: aData?.userId }))

                                            }}
                                        >
                                            <CheckCircleTwoTone twoToneColor={theme.palette.primary.main} />
                                        </IconButton>
                                    </Tooltip>
                                    <Tooltip title="Reject">
                                        <IconButton
                                            disabled={isRestricted}

                                            color="primary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                const aData: originaldataProps = row.original
                                                e.stopPropagation();
                                                setUserId(aData.userId!); 
                                                setUserStatus(aData)
                                                dispatch(rejectAuthorization({ status: "false", userId: aData?.userId }))

                                                // window.location.reload();
                                            }}

                                        >
                                            <CloseCircleTwoTone twoToneColor={theme.palette.primary.main} />
                                        </IconButton>
                                    </Tooltip>
                                </Stack>
                            </>
                        )
                    }
                }
            ] as Column[],
        []
    );

    //dialog model 
    const [addEdit, setAddEdit] = useState<boolean>(false);
    const [users, setUserStatus] = useState<userProps>();

    const handleAddEdit = () => {
        setAddEdit(!addEdit);
        if (users && !addEdit) setUserStatus(undefined);
    };
    const [userId, setUserId] = useState<number | null>(null)
    // ----------------------- | API Call - USER AUTHORIZATION | ---------------------

    useEffect(() => {
        const queryParams: queryParamsProps = {
            direction: "desc",
            page: 0,
            per_page: 1000,
            search: "",
            sort: "userId",
            isAuthorizationList: true,
            userID: user?.id!
        }

        dispatch(fetchUsersSuccess(queryParams))

    }, [success, error])

    useEffect(() => {
        if (!userList) {
            setData([])
            return
        }
        if (userList == null) {
            setData([])
            return
        }
        setData(userList.result!)
    }, [userList])

    useEffect(() => {
        if (error != null) {

            let defaultErrorMessage = "ERROR";
            // @ts-ignore
            const errorExp = error as Template1Error
            if (errorExp.message) {
                defaultErrorMessage = errorExp.message
            }
            dispatch(
                openSnackbar({
                    open: true,
                    message: defaultErrorMessage,
                    variant: 'alert',
                    alert: {
                        color: 'error'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [error]);

    useEffect(() => {
        if (success != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: success,
                    variant: 'alert',
                    alert: {
                        color: 'success'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [success])


    useEffect(()=>{
        if (isActionSuccess != null) {
            let actionId, actionName ,keyValueId,description,keyValue;
    
            switch (isActionSuccess) {
                case 'LIST':
                    actionId = 2;
                    actionName = 'LIST';
                    keyValueId = 0 ;
                    keyValue= 'N/A' ;
                    description='Get All Users List'
                    break;
                case 'APPROVE':
                    actionId = 1;
                    actionName = 'APPROVE';
                    keyValueId = 0 ;
                    keyValue= 'N/A' 
                    description='Approve a User'
                    break;
                case 'UPDATE':
                    actionId = 3;
                    actionName = 'UPDATE';
                    keyValueId = users?.userId! || 0 ;
                    keyValue = users?.userName||'N/A' ;
                    description=`Edit User Details : ${users?.userName} `
                    break;
                case 'REJECT':
                    actionId = 12;
                    actionName = 'REJECT';
                    keyValueId = userId || 0;
                    keyValue = users?.userName||'N/A' ;
                    description=`Reject User: -${users?.userName}`
                    
                    break;
                default:
                    return; // Exit early if no valid action is found
            }
            dispatch(AddActivityLog({
                actionId: actionId,
                actionName: actionName,
                branchId: user?.branchList?.[0]?.branchId ?? undefined,
                companyId: user?.companyId,
                deptId: user?.departmentList?.[0]?.departmentId ?? undefined,
                description: description,
                keyValue: keyValue,
                keyValueId: keyValueId,
                menuId: 3,
                menuName: "User Authorization",
                deptName: user?.departmentList?.[0]?.deptName ?? undefined
            }));
    
            dispatch(toResetIsActionSuccessState());
        }
    
    },[isActionSuccess]);

    if (isLoading) {
        return <>Loading...</>
    }

    return (
        <>
            <MainCard content={false}>
                <ScrollX>
                    <ReactTable columns={columns}
                        getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()}
                        data={data} handleAddEdit={handleAddEdit} />
                </ScrollX>
            </MainCard>
        </>
    );
}

export default List;
