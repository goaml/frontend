import { Column, HeaderGroup } from 'react-table';
import { Users } from 'types/user-authorization';

export interface dataProps extends Users { }

export interface originaldataProps {
    userId?: number,
}

export interface ReactTableProps {
    columns: Column[]
    data: dataProps[]
    handleAddEdit: () => void
    getHeaderProps: (column: HeaderGroup) => {};
}

export interface TableHeaderProps {
    headerGroups: HeaderGroup[];

}


export interface userProps extends Users { }