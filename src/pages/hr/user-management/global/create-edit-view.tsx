import {
    Grid,
    InputLabel,
    OutlinedInput,
    Stack
} from '@mui/material';

// third party
import { Formik } from 'formik';

// project import
import MainCard from 'components/MainCard';
import useScriptRef from 'hooks/useScriptRef';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'store';
import { fetchGlobalDetailsSuccess, toInitialState, toResetIsActionSuccessState } from 'store/reducers/global-details';
import { openSnackbar } from 'store/reducers/snackbar';
import { dataProps } from './types/types';
import useAuth from 'hooks/useAuth';
import { AddActivityLog } from 'store/reducers/note-log';

const CreateEditView = ({ isDemo = false }: { isDemo?: boolean }) => {

    const { user } = useAuth()

    useEffect(() => { }, [user])

    const scriptedRef = useScriptRef();
    const dispatch = useDispatch();
    const { globalDetails, isLoading, error, success, isActionSuccess } = useSelector(state => state.globalDetails)

    const [data, setData] = useState<dataProps | null>(null)

    // ----------------------- | API Call - Roles | ---------------------

    useEffect(() => {
        dispatch(fetchGlobalDetailsSuccess())
    }, [success])

    useEffect(() => {
        if (!globalDetails) {
            setData(null)
            return
        }
        if (globalDetails == null) {
            setData(null)
            return
        }
        setData(globalDetails.result!)
    }, [globalDetails])

    useEffect(() => {
        if (error != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: "Error",
                    variant: 'alert',
                    alert: {
                        color: 'error'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [error])

    useEffect(() => {
        if (success != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: success,
                    variant: 'alert',
                    alert: {
                        color: 'success'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [success])

    useEffect(() => {
        if (isActionSuccess != null) {
            let actionId, actionName, keyValue, description;

            switch (isActionSuccess) {
                case 'LIST':
                    actionId = 2;
                    actionName = 'LIST';
                    keyValue = 0;
                    description = 'Get Global details'
                    break;
                case 'CREATE':
                    actionId = 1;
                    actionName = 'CREATE';
                    keyValue = 0;
                    description = 'Create a User Types'
                    break;
                case 'UPDATE':
                    actionId = 3;
                    actionName = 'UPDATE';
                    keyValue = user?.userId! || 0;
                    description = `Edit Global Details  `
                    break;

                default:
                    return; // Exit early if no valid action is found
            }
            dispatch(AddActivityLog({
                actionId: actionId,
                actionName: actionName,
                branchId: user?.branchList?.[0]?.branchId ?? undefined,
                companyId: user?.companyId,
                deptId: user?.departmentList?.[0]?.departmentId ?? undefined,
                description: description,
                keyValue: "globalID",
                keyValueId: keyValue,
                menuId: 9,
                menuName: "Global Parameter",
                deptName: user?.departmentList?.[0]?.deptName ?? undefined
            }));

            dispatch(toResetIsActionSuccessState());
        }

    }, [isActionSuccess]);


    if (isLoading) {
        return <>Loading...</>
    }

    // Helper function to format the date
    const formatDate = (dateString: number[]) => {
        if (!dateString || !Array.isArray(dateString) || dateString.length !== 3) {
            return ''; // Return an empty string or any other appropriate fallback value
        }

        const [year, month, day] = dateString;
        return `${year}.${String(month).padStart(2, '0')}.${String(day).padStart(2, '0')}`;
    };


    return (

        <Grid item xs={12} sm={6} spacing={2}>
            <MainCard title="View Global Details">
                <>
                    <Formik
                        initialValues={{
                            companyname: data?.companyName,
                            fronturl: data?.frontURL,
                            versionnum: data?.versionNumber,
                            coreurl: data?.coreURL,
                            footertitle: data?.footerTitle,
                            domaindescription: data?.domainDestription,
                            encryptKey: data?.encryptKey,
                            logopath: data?.logoPath,
                            usermalepath: data?.userMalePath,
                            userfemalepath: data?.userFemalePath,
                            loginattempt: data?.userLoginAttempts,
                            resetdate: data?.resetDateCount,
                            expirationdays: data?.userExpirationDays,
                            passwordexpiration: data?.passwordExpDate,
                            currentWorkingDate: data?.currentWorkingDate,
                            nextWorkingDate: data?.nextWorkingDate,
                            userNotificationMode: data?.userNotificationMode,
                            passwordMinLength: data?.passwordMinLength,
                            passwordMaxLength: data?.passwordMaxLength,
                            passwordNumOfSpecialCharacter: data?.passwordNumOfSpecialCharacter,
                            passwordNumOfCapitalLetters: data?.passwordNumOfCapitalLetters,
                            passwordNumOfSimpleLetters: data?.passwordNumOfSimpleLetters,
                            passwordNumOfDigits: data?.passwordNumOfDigits,
                            submit: null
                        }}
                        onSubmit={async (values, { setErrors, setStatus, setSubmitting }) => {
                            try {

                                if (scriptedRef.current) {
                                    setStatus({ success: true });
                                    setSubmitting(false);
                                }
                            } catch (err: any) {
                                console.error(err);
                                if (scriptedRef.current) {
                                    setStatus({ success: false });
                                    setErrors({ submit: err.message });
                                    setSubmitting(false);
                                }
                            }
                        }}
                    >
                        {({ values }) => (

                            <form noValidate>
                                <Grid container spacing={3}>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="companyname-global">Company Name</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="companyname-global"
                                                type="text"
                                                value={data?.companyName}
                                                name="companyname"
                                                fullWidth
                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="versionnum-global">Version Number</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="versionnum-global"
                                                type="text"
                                                value={data?.versionNumber}
                                                name="versionnum"
                                                fullWidth

                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="fronturl-global">Front URL</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="fronturl-global"
                                                type="text"
                                                value={data?.frontURL}
                                                name="frontURL"
                                                fullWidth

                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="fronturl-global">Core URL</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="coreurl-global"
                                                type="text"
                                                value={data?.coreURL}
                                                name="coreurl"
                                                fullWidth

                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="footertitle-global">Footer Title</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="footertitle-global"
                                                type="text"
                                                value={data?.footerTitle}
                                                name="footertitle"
                                                fullWidth

                                            />

                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="domaindescription-global">Domain Description</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="domaindescription-global"
                                                type="text"
                                                value={data?.domainDestription}
                                                name="domaindescription"
                                                fullWidth

                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="encryptKey-global">encryptKey</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="encryptKey-global"
                                                type="text"
                                                value={data?.encryptKey}
                                                name="encryptKey"
                                                fullWidth
                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="logopath-global">Logo Path</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="logopath-global"
                                                type="text"
                                                value={data?.logoPath}
                                                name="logopath"
                                                fullWidth
                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="usermalepath-global">User Male Path</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="usermalepath-global"
                                                type="text"
                                                value={data?.userMalePath}
                                                name="usermalepath"
                                                fullWidth

                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="userfemalepath-global">User Female Path</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="userfemalepath-global"
                                                type="text"
                                                value={data?.userFemalePath}
                                                name="userfemalepath"
                                                fullWidth
                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="loginattempt-global">User Login Attempt</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="loginattempt-global"
                                                type="text"
                                                value={data?.userLoginAttempts}
                                                name="loginattempt"
                                                fullWidth
                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="resetdate-global">Reset Date Count</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="resetdate-global"
                                                type="text"
                                                value={data?.resetDateCount}
                                                name="resetdate"
                                                fullWidth
                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="expirationdays-global">User Expiration Days</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="expirationdays-global"
                                                type="text"
                                                value={data?.userExpirationDays}
                                                name="expirationdays"
                                                fullWidth

                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="passwordexpiration-global">Password Expire Date</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="passwordexpiration-global"
                                                type="text"
                                                value={data?.passwordExpDate}
                                                name="passwordexpiration"
                                                fullWidth

                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="domaindescription-global">Current Working Date</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="currentWorkingDate-global"
                                                type="text"
                                                value={formatDate(data?.currentWorkingDate || [])}

                                                name="currentWorkingDate"
                                                fullWidth

                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="encryptKey-global">Next Working Date</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="encryptKey-global"
                                                type="text"
                                                value={formatDate(data?.nextWorkingDate || [])}
                                                name="nextWorkingDate"
                                                fullWidth
                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="logopath-global">User Notification Mode</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="logopath-global"
                                                type="text"
                                                value={data?.userNotificationMode}
                                                name="userNotificationMode"
                                                fullWidth
                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="usermalepath-global">Password Min Length</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="usermalepath-global"
                                                type="text"
                                                value={data?.passwordMinLength}
                                                name="passwordMinLength"
                                                fullWidth

                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="userfemalepath-global">Password Max Length</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="userfemalepath-global"
                                                type="text"
                                                value={data?.passwordMaxLength}
                                                name="passwordMaxLength"
                                                fullWidth
                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="loginattempt-global">Password Number Of Special Character</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="loginattempt-global"
                                                type="text"
                                                value={data?.passwordNumOfSpecialCharacter}
                                                name="passwordNumOfSpecialCharacter"
                                                fullWidth
                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="resetdate-global">Password Num Of Capital Letters</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="resetdate-global"
                                                type="text"
                                                value={data?.passwordNumOfCapitalLetters}
                                                name="resetdate"
                                                fullWidth
                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="expirationdays-global">Password Num Of Simple Letters</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="expirationdays-global"
                                                type="text"
                                                value={data?.passwordNumOfSimpleLetters}
                                                name="expirationdays"
                                                fullWidth

                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="passwordexpiration-global">Password Number Of Digits</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="passwordexpiration-global"
                                                type="text"
                                                value={data?.passwordNumOfDigits}
                                                name="passwordexpiration"
                                                fullWidth

                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="lasProcessDate">Last Global Process Date</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="lasProcessDate"
                                                type="text"
                                                value={formatDate(data?.currentWorkingDate || [])}
                                                name="lasProcessDate"
                                                fullWidth

                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="goAmlLimit">GoAML Limit</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="goAmlLimit"
                                                type="text"
                                                value={data?.goamlLimit?.toLocaleString('en-US', { minimumFractionDigits: 2 })} // Format the value
                                                name="goAmlLimit"
                                                fullWidth
                                            />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1}>
                                            <InputLabel htmlFor="fronturl-global">CBSL URL</InputLabel>
                                            <OutlinedInput
                                                disabled
                                                id="fronturl-global"
                                                type="text"
                                                value={data?.goAMLCoreURL}
                                                name="frontURL"
                                                fullWidth

                                            />
                                        </Stack>
                                    </Grid>
                                </Grid>
                            </form>
                        )}
                    </Formik>
                    <Grid container spacing={4} >
                        <Grid item xs={4} md={12}>
                            <Stack direction="row" justifyContent="right">
                                {/* <Button component={Link} sx={{ my: 3, ml: 1 }} to={isDemo ? '/auth/dashboard/admin' : '/dashboard/admin'}>
                                    Back
                                </Button> */}
                            </Stack>
                        </Grid>
                    </Grid>
                </>
            </MainCard>
        </Grid>
    );
};

export default CreateEditView;