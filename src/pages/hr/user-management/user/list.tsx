import { Fragment, MouseEvent, useCallback, useEffect, useMemo, useState } from 'react';

// material-ui
import {
    Button,
    Dialog,
    IconButton,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Tooltip,
    Typography,
    alpha,
    useMediaQuery,
    useTheme
} from '@mui/material';

// third-party
import { HeaderSort, SortingSelect, TablePagination, TableRowSelection } from 'components/third-party/ReactTable';
import { Cell, Column, HeaderGroup, Row, useExpanded, useFilters, useGlobalFilter, usePagination, useRowSelect, useSortBy, useTable } from 'react-table';

// project import
import { PopupTransition } from 'components/@extended/Transitions';
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import AlertUserDelete from 'sections/hr/user-management/user/AlertUserDelete';
import {
    GlobalFilter,
    renderFilterTypes
} from 'utils/react-table';

// assets
import { CloseOutlined, EditTwoTone, EyeTwoTone, PlusOutlined, UserDeleteOutlined } from '@ant-design/icons';

//types 
import Dot from 'components/@extended/Dot';
import AddEditUser from 'sections/hr/user-management/user/AddEditUser';
import { useDispatch, useSelector } from 'store';
import { openSnackbar } from 'store/reducers/snackbar';
import { fetchUsers, toInitialState } from 'store/reducers/users';
import { ColorProps } from 'types/extended';
import { listParametersType } from 'types/users';
import { ReactTableProps, TableParamsType, dataProps, userProps } from './types/types';
import { AddActivityLog } from 'store/reducers/note-log';
import { toResetIsActionSuccessState } from 'store/reducers/user';
import useAuth from 'hooks/useAuth';
import ViewUser from 'sections/hr/user-management/user/ViewUser';

interface Props {
    status: boolean;
}

const IsActiveStatus = ({ status }: Props) => {
    let color: ColorProps;
    let title: string;

    switch (status) {
        case true:
            color = 'success';
            title = 'Active';
            break;
        case false:
            color = 'error';
            title = 'In-Active';
            break;
        default:
            color = 'primary';
            title = 'None';
    }

    return (
        <Stack direction="row" spacing={1} alignItems="center">
            <Dot color={color} />
            <Typography>{title}</Typography>
        </Stack>
    );
};

// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, handleAddEdit, getHeaderProps, renderRowSubComponent }: ReactTableProps) {
    const theme = useTheme();

    const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));
    const sortBy = { id: 'id', desc: false };

    const filterTypes = useMemo(() => renderFilterTypes, []);

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        allColumns,
        rows,
        page,
        gotoPage,
        setPageSize,
        visibleColumns,
        state: { globalFilter, selectedRowIds, pageIndex, pageSize, expanded },
        preGlobalFilteredRows,
        setGlobalFilter,
        setSortBy,
    } = useTable(
        {
            columns,
            data,
            filterTypes,
            initialState: { pageIndex: 0, pageSize: 10, hiddenColumns: ['avatar', 'email'], sortBy: [sortBy] }
        },
        useGlobalFilter,
        useFilters,
        useSortBy,
        useExpanded,
        usePagination,
        useRowSelect
    );

    return (
        <>
            <TableRowSelection selected={Object.keys(selectedRowIds).length} />
            <Stack spacing={3}>
                <Stack
                    direction={matchDownSM ? 'column' : 'row'}
                    spacing={1}
                    justifyContent="space-between"
                    alignItems="center"
                    sx={{ p: 3, pb: 0 }}
                >
                    <GlobalFilter
                        preGlobalFilteredRows={preGlobalFilteredRows}
                        globalFilter={globalFilter}
                        setGlobalFilter={setGlobalFilter}
                        size="small"
                    />
                    <Stack direction={matchDownSM ? 'column' : 'row'} alignItems="center" spacing={1}>
                        <SortingSelect sortBy={sortBy.id} setSortBy={setSortBy} allColumns={allColumns} />
                        <Button variant="contained" startIcon={<PlusOutlined />} onClick={handleAddEdit} size="small">
                            Add
                        </Button>

                    </Stack>
                </Stack>
                <Table {...getTableProps()}>
                    <TableHead>
                        {headerGroups.map((headerGroup: HeaderGroup<{}>) => (
                            <TableRow {...headerGroup.getHeaderGroupProps()} sx={{ '& > th:first-of-type': { width: '58px' } }}>
                                {headerGroup.headers.map((column: HeaderGroup) => (
                                    <TableCell {...column.getHeaderProps([{ className: column.className }, getHeaderProps(column)])}>
                                        <HeaderSort column={column} />
                                    </TableCell>
                                ))}
                            </TableRow>
                        ))}
                    </TableHead>
                    <TableBody {...getTableBodyProps()}>
                        {page.map((row: Row, i: number) => {
                            prepareRow(row);
                            const rowProps = row.getRowProps();

                            return (
                                <Fragment key={i}>
                                    <TableRow
                                        {...row.getRowProps()}
                                        onClick={() => {
                                            row.toggleRowSelected();
                                        }}
                                        sx={{ cursor: 'pointer', bgcolor: row.isSelected ? alpha(theme.palette.primary.lighter, 0.35) : 'inherit' }}
                                    >
                                        {row.cells.map((cell: Cell) => (
                                            <TableCell {...cell.getCellProps([{ className: cell.column.className }])}>{cell.render('Cell')}</TableCell>
                                        ))}
                                    </TableRow>
                                    {row.isExpanded && renderRowSubComponent({ row, rowProps, visibleColumns, expanded })}
                                </Fragment>
                            );
                        })}
                        <TableRow>
                            <TableCell sx={{ p: 2 }} colSpan={12}>
                                <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </Stack>
        </>
    );
}

// ==============================|| List ||============================== //

const List = () => {
    const theme = useTheme();
    const dispatch = useDispatch();
    const { userList, error, success, isActionSuccess } = useSelector(state => state.user);
    const { user } = useAuth();

    // table
    const [data, setData] = useState<dataProps[]>([])

    const columns = useMemo(
        () =>
            [
                {
                    Header: '',
                    accessor: 'id',
                    className: 'cell-center',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.id === undefined || row.id === null || row.id === '') {
                            return <>-</>
                        }
                        if (typeof row.id === 'string') {
                            return <>{(parseInt(row.id) + 1).toString()}</>;
                        }
                        if (typeof row.id === 'number') {
                            return <>{row.id + 1}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'User Id',
                    accessor: 'userName',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.userName === undefined || row.values.userName === null || row.values.userName === '') {
                            return <>-</>
                        }
                        if (typeof row.values.userName === 'string') {
                            return <>{row.values.userName}</>;
                        }
                        if (typeof row.values.userName === 'number') {
                            return <>{row.values.userName}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'User Role',
                    accessor: 'userRoleName',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.userRoleName === undefined || row.values.userRoleName === null || row.values.userRoleName === '') {
                            return <>-</>
                        }
                        if (typeof row.values.userRoleName === 'string') {
                            return <>{row.values.userRoleName}</>;
                        }
                        if (typeof row.values.userRoleName === 'number') {
                            return <>{row.values.userRoleName}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'User Type',
                    accessor: 'userTypeName',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.userTypeName === undefined || row.values.userTypeName === null || row.values.userTypeName === '') {
                            return <>-</>
                        }
                        if (typeof row.values.userTypeName === 'string') {
                            return <>{row.values.userTypeName}</>;
                        }
                        if (typeof row.values.userTypeName === 'number') {
                            return <>{row.values.userTypeName}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Email',
                    accessor: 'empEmail',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.empEmail === undefined || row.values.empEmail === null || row.values.empEmail === '') {
                            return <>-</>
                        }
                        if (typeof row.values.empEmail === 'string') {
                            return <>{row.values.empEmail}</>;
                        }
                        if (typeof row.values.empEmail === 'number') {
                            return <>{row.values.empEmail}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'NIC',
                    accessor: 'nic',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.nic === undefined || row.values.nic === null || row.values.nic === '') {
                            return <>-</>
                        }
                        if (typeof row.values.nic === 'string') {
                            return <>{row.values.nic}</>;
                        }
                        if (typeof row.values.nic === 'number') {
                            return <>{row.values.nic}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Contact No.',
                    accessor: 'empMobileNumber',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.empMobileNumber === undefined || row.values.empMobileNumber === null || row.values.empMobileNumber === '') {
                            return <>-</>
                        }
                        if (typeof row.values.empMobileNumber === 'string') {
                            return <>{row.values.empMobileNumber}</>;
                        }
                        if (typeof row.values.empMobileNumber === 'number') {
                            return <>{row.values.empMobileNumber}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Is Active',
                    accessor: 'isActive',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.isActive === undefined || row.values.isActive === null) {
                            return <>-</>;
                        }
                        if (typeof row.values.isActive === 'boolean') {
                            return (
                                <>
                                    <IsActiveStatus status={row.values.isActive} />{' '}
                                </>
                            );
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Status',
                    accessor: 'statusName',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.statusName === 'RESTRICTED') {
                            return (
                                <Stack direction="row" alignItems="center" spacing={1}>
                                    <Dot color="error" />
                                    <Typography>{row.values.statusName}</Typography>
                                </Stack>
                            );
                        }
                        else if (row.values.statusName === 'NEW') {
                            return (
                                <Stack direction="row" alignItems="center" spacing={1}>
                                    <Dot color="primary" />
                                    <Typography>{row.values.statusName}</Typography>
                                </Stack>
                            );
                        }
                        else {
                            return (
                                <Stack direction="row" alignItems="center" spacing={1}>
                                    <Dot color="success" />
                                    <Typography>{row.values.statusName}</Typography>
                                </Stack>
                            );
                        }
                    },
                },
                {
                    id: "actions",
                    Header: 'Actions',
                    accessor: 'actions',
                    className: 'cell-center',
                    Cell: ({ row }: { row: Row }) => {
                        const collapseIcon = row.isExpanded ? (
                            <CloseOutlined style={{ color: theme.palette.error.main }} />
                        ) : (
                            <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                        );
                        return (
                            <>
                                <Stack direction="row" alignItems="center" justifyContent="center" spacing={0}>
                                    <Tooltip title="View">
                                        <IconButton
                                            color="secondary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                                row.toggleRowExpanded();
                                            }}
                                        >
                                            {collapseIcon}
                                        </IconButton>
                                    </Tooltip>
                                    <Tooltip title="Edit">
                                        <IconButton
                                            color="primary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                //@ts-ignore
                                                const data: Users = {
                                                    ...row.original,
                                                    email: row?.values?.empEmail,
                                                    mobileNo: row?.values?.empMobileNumber
                                                };
                                                e.stopPropagation();
                                                setUser({ ...data });
                                                handleAddEdit();
                                            }}
                                        >
                                            <EditTwoTone twoToneColor={theme.palette.primary.main} />
                                        </IconButton>
                                    </Tooltip>
                                    {/* <Tooltip title="Delete">
                                        <IconButton
                                            color="error"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                                setUserId(data.userId!)
                                                setOpenAlert(true)
                                            }}
                                        >
                                            <DeleteTwoTone twoToneColor={theme.palette.error.main} />
                                        </IconButton>
                                    </Tooltip> */}

                                    <Tooltip title="In-Active">
                                        <IconButton
                                            disabled={row.values.statusName === 'RESTRICTED' ? true : false}
                                            color="error"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                //@ts-ignore
                                                let data: userProps = row.original;  // Ensure correct user data type
                                                e.stopPropagation();
                                                setUserId(data.userId!);   // Save the userId
                                                setUser(data);             // Save the user data including username
                                                setOpenAlert(true);        // Open the alert
                                            }}
                                        >
                                            <UserDeleteOutlined twoToneColor={theme.palette.error.main} />
                                        </IconButton>
                                    </Tooltip>
                                </Stack>
                            </>
                        )
                    }
                }
            ] as Column[],
        []
    );

    const renderRowSubComponent = useCallback(({ row }: { row: Row<{}> }) => <ViewUser data={data[Number(row.id)]} />, [data]);

    //dialog model 
    const [addEdit, setAddEdit] = useState<boolean>(false);
    const [users, setUser] = useState<userProps>();

    const handleAddEdit = () => {
        setAddEdit(!addEdit);
        if (users && !addEdit) setUser(undefined);
    };

    //alert model
    const [openAlert, setOpenAlert] = useState(false);
    const [userId, setUserId] = useState<number | null>(null)

    const handleAlertClose = () => {
        setOpenAlert(!openAlert);
    };

    // ==============================|| API CONFIG ||============================== //

    const [page, setPage] = useState<number>(0);
    const [perPage, setPerPage] = useState<number>(1000);
    const [direction, setDirection] = useState<"asc" | "desc">("desc");
    const [search, setSearch] = useState<string>("");
    const [sort, setSort] = useState<string>("userId"); //CHANGE ID

    const tableParams: TableParamsType = {
        page,
        setPage,
        perPage,
        setPerPage,
        direction,
        setDirection,
        sort,
        setSort,
        search,
        setSearch
    }

    useEffect(() => {
        const listParameters: listParametersType = {
            page: page,
            per_page: perPage,
            direction: direction,
            search: search,
            sort: sort
        };
        dispatch(fetchUsers(listParameters));
    }, [dispatch, success, page, perPage, direction, search, sort]);

    useEffect(() => {
        setData(userList?.result! || []);
    }, [userList])



    //  handel error 
    // useEffect(() => {
    //     if (error != null) {
    //         let errorMessage = "Default error message";

    //         //@ts-ignore
    //         const errorExp = error as Template1Error;
    //         //@ts-ignore
    //         const errorExp1 = error as Template2Error;

    //         if (errorExp.StatusCode && errorExp.Message) {
    //             errorMessage = errorExp.Message;
    //         }
    //         else if (errorExp1.title && errorExp1.status) {
    //             errorMessage = errorExp1.title;
    //         }

    //         dispatch(
    //             openSnackbar({
    //                 open: true,
    //                 message: errorMessage,
    //                 variant: 'alert',
    //                 alert: {
    //                     color: 'error'
    //                 },
    //                 close: true
    //             })
    //         );
    //         dispatch(toInitialState());
    //     }
    // }, [error]);



    useEffect(() => {
        if (error != null) {
            let defaultErrorMessage = "ERROR";
            // @ts-ignore
            const errorExp = error as Template1Error
            if (errorExp.message) {
                defaultErrorMessage = errorExp.message
            }
            dispatch(
                openSnackbar({
                    open: true,
                    message: defaultErrorMessage,
                    variant: 'alert',
                    alert: {
                        color: 'error'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [error]);

    //  handel success
    useEffect(() => {
        if (success != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: success,
                    variant: 'alert',
                    alert: {
                        color: 'success'
                    },
                    close: true
                })
            );
            dispatch(toInitialState())
        }
    }, [success]

    )
    useEffect(() => {
        if (isActionSuccess != null) {
            let actionId, actionName, keyValueId, keyValue, description;

            switch (isActionSuccess) {
                case 'LIST':
                    actionId = 2;
                    actionName = 'LIST';
                    keyValueId = 0;
                    keyValue = 'N/A';
                    description = 'Get All Users List'
                    break;
                case 'CREATE':
                    actionId = 1;
                    actionName = 'CREATE';
                    keyValueId = 0;
                    keyValue = 'N/A';
                    description = 'Create a User'
                    break;
                case 'UPDATE':
                    actionId = 3;
                    actionName = 'UPDATE';
                    keyValueId = users?.userId! || 0;
                    keyValue = users?.userName || 'N/A';
                    description = `Edit User Details : ${users?.userName} `
                    break;
                case 'INACTIVE':
                    actionId = 12;
                    actionName = 'INACTIVE';
                    keyValueId = userId || 0;
                    keyValue = users?.userName || 'N/A';
                    description = `Delete User: -${users?.userName}`

                    break;
                default:
                    return; // Exit early if no valid action is found
            }
            dispatch(AddActivityLog({
                actionId: actionId,
                actionName: actionName,
                branchId: user?.branchList?.[0]?.branchId ?? undefined,
                companyId: user?.companyId,
                deptId: user?.departmentList?.[0]?.departmentId ?? undefined,
                description: description,
                keyValue: keyValue,
                keyValueId: keyValueId,
                menuId: 2,
                menuName: "Users",
                deptName: user?.departmentList?.[0]?.deptName ?? undefined
            }));

            dispatch(toResetIsActionSuccessState());
        }

    }, [isActionSuccess]);


    // if (isLoading) {
    //     return <div>Loading...</div>;
    // }



    return (
        <>
            <MainCard content={false}>
                <ScrollX>
                    <ReactTable columns={columns} data={data} handleAddEdit={handleAddEdit} tableParams={tableParams}
                        getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()}
                        renderRowSubComponent={renderRowSubComponent} />
                </ScrollX>
                {/* add / edit user dialog */}
                <Dialog
                    maxWidth="sm"
                    TransitionComponent={PopupTransition}
                    keepMounted
                    fullWidth
                    onClose={handleAddEdit}
                    open={addEdit}
                    sx={{ '& .MuiDialog-paper': { p: 0 }, transition: 'transform 225ms' }}
                    aria-describedby="alert-dialog-slide-description"
                >
                    <AddEditUser user={users} onCancel={handleAddEdit} />
                </Dialog>
                {/* alert model */}
                {userId && <AlertUserDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={userId} />}
            </MainCard>
        </>
    );
}

export default List;
