import { Dispatch, FC, SetStateAction } from 'react';
import { Column, HeaderGroup } from 'react-table';
import { UsersType } from 'types/users';

export interface dataProps extends UsersType { }

export interface ReactTableProps {
    columns: Column[]
    data: dataProps[]
    handleAddEdit: () => void
    tableParams: TableParamsType
    getHeaderProps: (column: HeaderGroup) => {};
    renderRowSubComponent: FC<any>;
}

export interface userProps {
    description?: string,
    empEmail?: string,
    empMobileNumber?: string,
    firstName?: string,
    lastName?: string,
    userRoleId?: number,
    statusId?: number,
    userId?: number,
    userName?: string,
    isActive?: boolean,
    nic?: string,
    referenceNo?: string,
    departmentId?: number,
    branchId?: number,
    statusName?: string,
    userRoleName?: string,
    branchName?: string,
    departmentName?: string,
    userTypeId?: number,
    userTypeName?: string,
    companyId?: number,
    companyName?: string
}

export interface TableParamsType {
    page: number;
    setPage: Dispatch<SetStateAction<number>>;
    perPage: number;
    setPerPage: Dispatch<SetStateAction<number>>;
    direction: "asc" | "desc";
    setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
    sort: string;
    setSort: Dispatch<SetStateAction<string>>;
    search: string;
    setSearch: Dispatch<SetStateAction<string>>;
}
