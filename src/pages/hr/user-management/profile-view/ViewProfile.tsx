import {
    Grid,
    InputLabel,
    OutlinedInput,
    Stack
} from '@mui/material';

// third party

// project import
import MainCard from 'components/MainCard';
import useAuth from 'hooks/useAuth';

const TabProfile = ({ isDemo = false }: { isDemo?: boolean }) => {

    const { user } = useAuth()

    return (

        <Grid item xs={12} sm={6} spacing={2}>
            <MainCard title="View Profile Details">
                <>
                    <Grid container spacing={3}>
                        <Grid item xs={6}>
                            <Stack spacing={1}>
                                <InputLabel htmlFor="companyname-global">User ID</InputLabel>
                                <OutlinedInput
                                    disabled
                                    id="companyname-global"
                                    type="text"
                                    value={user?.userId}
                                    name="companyname"
                                    fullWidth
                                />
                            </Stack>
                        </Grid>
                        <Grid item xs={6}>
                            <Stack spacing={1}>
                                <InputLabel htmlFor="companyname-global">User Name</InputLabel>
                                <OutlinedInput
                                    disabled
                                    id="companyname-global"
                                    type="text"
                                    value={user?.userName}
                                    name="companyname"
                                    fullWidth
                                />
                            </Stack>
                        </Grid>
                        <Grid item xs={6}>
                            <Stack spacing={1}>
                                <InputLabel htmlFor="companyname-global">First Name</InputLabel>
                                <OutlinedInput
                                    disabled
                                    id="companyname-global"
                                    type="text"
                                    value={user?.firstName}
                                    name="companyname"
                                    fullWidth
                                />
                            </Stack>
                        </Grid>
                        <Grid item xs={6}>
                            <Stack spacing={1}>
                                <InputLabel htmlFor="versionnum-global">Last Name</InputLabel>
                                <OutlinedInput
                                    disabled
                                    id="versionnum-global"
                                    type="text"
                                    value={user?.lastName}
                                    name="versionnum"
                                    fullWidth

                                />
                            </Stack>
                        </Grid>
                        <Grid item xs={6}>
                            <Stack spacing={1}>
                                <InputLabel htmlFor="fronturl-global">Mobile No</InputLabel>
                                <OutlinedInput
                                    disabled
                                    id="fronturl-global"
                                    type="text"
                                    value={user?.mobile}
                                    name="frontURL"
                                    fullWidth

                                />
                            </Stack>
                        </Grid>
                        <Grid item xs={6}>
                            <Stack spacing={1}>
                                <InputLabel htmlFor="fronturl-global">Email Address</InputLabel>
                                <OutlinedInput
                                    disabled
                                    id="coreurl-global"
                                    type="text"
                                    value={user?.email}
                                    name="coreurl"
                                    fullWidth

                                />
                            </Stack>
                        </Grid>
                        <Grid item xs={6}>
                            <Stack spacing={1}>
                                <InputLabel htmlFor="footertitle-global">User Role Name</InputLabel>
                                <OutlinedInput
                                    disabled
                                    id="footertitle-global"
                                    type="text"
                                    value={user?.userRoleName}
                                    name="footertitle"
                                    fullWidth

                                />

                            </Stack>
                        </Grid>
                        <Grid item xs={6}>
                            <Stack spacing={1}>
                                <InputLabel htmlFor="domaindescription-global">Password Expire Date Count</InputLabel>
                                <OutlinedInput
                                    disabled
                                    id="domaindescription-global"
                                    type="text"
                                    value={user?.passwordResetDateCount}
                                    name="domaindescription"
                                    fullWidth
                                />
                            </Stack>
                        </Grid>
                        <Grid item xs={6}>
                            <Stack spacing={1}>
                                <InputLabel htmlFor="companyname-global">Branch</InputLabel>
                                <OutlinedInput
                                    disabled
                                    id="companyname-global"
                                    type="text"
                                    value={user?.branchList && user.branchList.length > 0 ? user.branchList[0]?.branchLocation : ''}
                                    name="companyname"
                                    fullWidth
                                />
                            </Stack>
                        </Grid>
                    </Grid>
                    <Grid container spacing={4} >
                        <Grid item xs={4} md={12}>
                            <Stack direction="row" justifyContent="right">
                            </Stack>
                        </Grid>
                    </Grid>
                </>
            </MainCard>
        </Grid>
    );
};

export default TabProfile;