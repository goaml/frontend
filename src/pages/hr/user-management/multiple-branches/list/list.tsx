import { MouseEvent, useEffect, useMemo, useState } from 'react';

// project import
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import { IndeterminateCheckbox, TablePagination, TableRowSelection } from 'components/third-party/ReactTable';
import TableHeader from 'sections/hr/user-management/multiple-branches/TableHeader';
import { GlobalFilter } from 'utils/react-table';
import { TableProps } from './types/types';
// assets
import { DeleteTwoTone, EditTwoTone, PlusOutlined } from '@ant-design/icons';

//material-ui
import { Autocomplete, Button, Dialog, Grid, IconButton, InputLabel, Stack, Table, TableBody, TableCell, TableRow, TextField, Tooltip } from '@mui/material';
import { useTheme } from '@mui/material/styles';

// third-party
import {
    Cell,
    Column,
    HeaderGroup,
    Row,
    useFilters,
    useGlobalFilter,
    usePagination,
    useRowSelect,
    useSortBy,
    useTable
} from 'react-table';

// types
import { PopupTransition } from 'components/@extended/Transitions';
import AddEditMultipleBranches from 'sections/hr/user-management/multiple-branches/AddEditMultipleBranches';
import AlertBranchDelete from 'sections/hr/user-management/multiple-branches/AlertMultipleBranchesDelete';
import { dispatch, useSelector } from 'store';
import { fetchGetAllBranchesOfUserForMultipleBranches, toInitialState } from 'store/reducers/branch';
import { openSnackbar } from 'store/reducers/snackbar';
import { fetchGetAllUserSuccess, getDefaultBranchUserById } from 'store/reducers/users';

import useAuth from 'hooks/useAuth';
import { AddActivityLog } from 'store/reducers/note-log';
import { toResetIsActionSuccessState } from 'store/reducers/user';
import { ThemeDirection } from 'types/config';
import { Users, listParametersType } from 'types/users';
import { userProps } from './types/types';


// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, getHeaderProps, selectedData }: TableProps) {
    const theme = useTheme();

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        page,
        prepareRow,
        gotoPage,
        setPageSize,
        state,
        preGlobalFilteredRows,
        setGlobalFilter,
        state: { selectedRowIds, pageIndex, pageSize }
    } = useTable(
        {
            columns,
            data,
            getHeaderProps,
            initialState: { pageIndex: 0, pageSize: 10 },
            sortBy: [
                {
                    id: 'id',
                    desc: false
                }
            ]
        },
        useFilters,
        useGlobalFilter,
        useSortBy,
        usePagination,
        useFilters,
        useRowSelect,
        (hooks) => {
            hooks.allColumns.push((columns: Column[]) => [
                {
                    id: 'row-selection-chk',
                    accessor: 'Selection',
                    disableSortBy: true,
                    Header: ({ getToggleAllPageRowsSelectedProps }) => (
                        <IndeterminateCheckbox indeterminate {...getToggleAllPageRowsSelectedProps()} />
                    ),
                    Cell: ({ row }: any) => <IndeterminateCheckbox {...row.getToggleRowSelectedProps()} />
                },
                ...columns
            ]);
        }

    );

    const sortingRow = rows.slice(0, 9);
    let sortedData = sortingRow.map((d: Row) => d.original);
    Object.keys(sortedData).forEach((key: string) => sortedData[Number(key)] === undefined && delete sortedData[Number(key)]);

    return (
        <ScrollX>
            <TableRowSelection selected={Object.keys(selectedRowIds).length} />

            <Stack
                spacing={3}
                sx={{
                    ...(theme.direction === ThemeDirection.RTL && {
                        '.MuiTable-root': { width: { xs: '930px', sm: 'inherit' } },
                        pre: { width: { xs: '930px', sm: 'inherit' }, overflowX: 'unset' }
                    })
                }}
            >
                <Stack direction="row" spacing={2} justifyContent="space-between" sx={{ padding: 2 }}>
                    <GlobalFilter preGlobalFilteredRows={preGlobalFilteredRows} globalFilter={state.globalFilter} setGlobalFilter={setGlobalFilter} />
                </Stack>

                <Table {...getTableProps()}>

                    <TableHeader headerGroups={headerGroups} />

                    <TableBody {...getTableBodyProps()}>

                        {page.map((row: Row) => {
                            prepareRow(row);
                            return (
                                <TableRow {...row.getRowProps()}>
                                    {row.cells.map((cell: Cell) => (
                                        <TableCell {...cell.getCellProps([{ className: cell.column.className }])}>{cell.render('Cell')}</TableCell>
                                    ))}
                                </TableRow>
                            );
                        })}

                        <TableRow>
                            <TableCell sx={{ p: 2 }} colSpan={12}>
                                <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </Stack>
        </ScrollX>

    );
}

// ==============================|| USER MULTIPLE BRANCHES LIST - MAIN ||============================== //

const List = () => {
    const [userId, setUserId] = useState<any>(undefined)
    const [users, setUsers] = useState<any>()
    const [branchId, setBranchId] = useState<string>("")
    const { user } = useAuth();

    const theme = useTheme();

    // const formatDate = (dateArray: number[]): string => {
    //     const [year, month, day] = dateArray;
    //     return `${year}.${String(month).padStart(2, '0')}.${String(day).padStart(2, '0')}`;
    // };

    const columns1 = useMemo(
        () => [

            {
                Header: 'Branch Name',
                accessor: "branchName",
            },
            {
                Header: 'Department Name',
                accessor: 'deptName',
            },
            {
                Header: 'From Date',
                accessor: 'from',
                Cell: ({ row }: { row: Row }) => {
                    const value = row.values.from;
                    if (value === undefined || value === null || value === '') return <>-</>;
                    return <>{new Date(value).toLocaleDateString()}</>; // Using toLocaleDateString() to remove the timestamp
                }
            },
            {
                Header: 'To Date',
                accessor: 'to',
                Cell: ({ row }: { row: Row }) => {
                    const value = row.values.to;
                    if (value === undefined || value === null || value === '') return <>-</>;
                    return <>{new Date(value).toLocaleDateString()}</>; // Using toLocaleDateString() to remove the timestamp
                }
            },
            {
                id: "actions",
                Header: 'Actions',
                accessor: 'actions',
                className: 'cell-left',
                Cell: ({ row }: { row: Row }) => {
                    return (
                        <>
                            <Stack direction="row" spacing={0}>
                                <Tooltip title="Edit">
                                    <IconButton
                                        color="primary"
                                        onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                            //@ts-ignore
                                            const data: MultipleBranches = row.original;
                                            e.stopPropagation();
                                            setmultipleBranches({ ...data });
                                            handleAddEdit();
                                        }}
                                    >
                                        <EditTwoTone twoToneColor={theme.palette.primary.main} />
                                    </IconButton>
                                </Tooltip>

                                <Tooltip title="Delete">
                                    <IconButton
                                        color="error"
                                        onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                            //@ts-ignore
                                            let data: MultipleBranches = row.original;
                                            e.stopPropagation();
                                            setbrachDeptId(data.employeeBranchId!)
                                            setOpenAlert(true)
                                        }}
                                    >
                                        <DeleteTwoTone twoToneColor={theme.palette.error.main} />
                                    </IconButton>
                                </Tooltip>
                            </Stack>
                        </>
                    )
                }
            }
        ],
        [theme, userId, branchId]
    );

    //dialog model  - add edit
    const [addEdit, setAddEdit] = useState<boolean>(false);
    const [multipleBranches, setmultipleBranches] = useState<userProps>();

    const handleAddEdit = () => {
        setAddEdit(!addEdit);
        if (multipleBranches && !addEdit) setmultipleBranches(undefined);
    };

    //alert model- delete
    const [openAlert, setOpenAlert] = useState(false);
    const [brachDeptId, setbrachDeptId] = useState<number | null>(null)

    const handleAlertClose = () => {
        setOpenAlert(!openAlert);
    };

    // user drop down
    const { selectUsers, userDefaultBranchById } = useSelector((state) => state.user);

    // user drop down useeffect
    useEffect(() => {
        const queryParams: listParametersType = {
            direction: "asc",
            page: 0,
            per_page: 1000,
            search: "",
            sort: "userId"
        }

        dispatch(fetchGetAllUserSuccess(queryParams))
    }, [])



    // set Branch Table Data
    const { isLoading, success, error, MultipleBranches, isActionSuccess } = useSelector((state) => state.branch);
    // Get branch for User
    useEffect(() => {
        if (typeof userId == "undefined") return
        dispatch(fetchGetAllBranchesOfUserForMultipleBranches(userId!))
    }, [success, userId])

    // set Branch table data
    const [DefaultBranchTable, setDefaultBranchTable] = useState<any>([])
    useEffect(() => {
        if (!MultipleBranches) {
            setDefaultBranchTable([])
            return
        }
        if (MultipleBranches == null) {
            setDefaultBranchTable([])
            return
        }
        setDefaultBranchTable(MultipleBranches?.result!)
    }, [MultipleBranches])

    useEffect(() => {
        if (success != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: success,
                    variant: 'alert',
                    alert: {
                        color: 'success'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [success])

    useEffect(() => {
        if (isActionSuccess != null) {
            let actionId, actionName, keyValue, description;

            switch (isActionSuccess) {
                case 'LIST':
                    actionId = 2;
                    actionName = 'LIST';
                    keyValue = 0;
                    description = `Get Assigned branch List of ${users}`
                    break;
                case 'CREATE':
                    actionId = 1;
                    actionName = 'CREATE';
                    keyValue = 0;
                    description = `Assign a Branch to ${users}`
                    break;
                case 'UPDATE':
                    actionId = 3;
                    actionName = 'UPDATE';
                    keyValue = userId! || 0;
                    description = `Edit User Branch Details : ${multipleBranches?.brachDeptId} `
                    break;
                case 'INACTIVE':
                    actionId = 12;
                    actionName = 'INACTIVE';
                    keyValue = userId || 0;
                    description = `Remove Assigned Branch: -${brachDeptId}`

                    break;
                default:
                    return; // Exit early if no valid action is found
            }
            dispatch(AddActivityLog({
                actionId: actionId,
                actionName: actionName,
                branchId: user?.branchList?.[0]?.branchId ?? undefined,
                companyId: user?.companyId,
                deptId: user?.departmentList?.[0]?.departmentId ?? undefined,
                description: description,
                keyValue: "userId",
                keyValueId: keyValue,
                menuId: 8,
                menuName: "Assign Multiple Branch",
                deptName: user?.departmentList?.[0]?.deptName ?? undefined
            }));

            dispatch(toResetIsActionSuccessState());
        }

    }, [isActionSuccess]);


    useEffect(() => {
        if (error != null) {

            let defaultErrorMessage = "ERROR";
            // @ts-ignore
            const errorExp = error as Template1Error
            if (errorExp.message) {
                defaultErrorMessage = errorExp.message
            }
            dispatch(
                openSnackbar({
                    open: true,
                    message: defaultErrorMessage,
                    variant: 'alert',
                    alert: {
                        color: 'error'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [error]);


    // Default Branch Set API
    useEffect(() => {
        if (typeof userId == "undefined") return
        dispatch(getDefaultBranchUserById(userId!))
    }, [userId])

    useEffect(() => {
        if (!userDefaultBranchById) {
            setBranchId("")
            return
        }
        if (userDefaultBranchById == null) {
            setBranchId("")
            return
        }
        setBranchId(userDefaultBranchById.defaultBranch!)
    }, [userDefaultBranchById])


    if (isLoading) {
        return <>Loading...</>
    }

    return (
        <>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={12}>

                    <MainCard>
                        <Grid container spacing={1} direction="column">
                            <Grid container spacing={6}>
                                <Grid item xs={12} sm={6}>
                                    <InputLabel htmlFor="userId">Login ID</InputLabel>
                                    <Autocomplete
                                        fullWidth
                                        id="userId"
                                        value={selectUsers?.find((option: Users) => option?.userId === userId) || null}
                                        onChange={(event: any, newValue: Users | null) => {
                                            setUserId(newValue?.userId);
                                            setUsers(newValue?.userName)
                                        }}
                                        options={selectUsers || []}
                                        getOptionLabel={(item) => `${item.userName}`}
                                        renderInput={(params) => (
                                            <TextField
                                                {...params}
                                                placeholder="Select Login ID"
                                                sx={{ '& .MuiAutocomplete-input.Mui-disabled': { WebkitTextFillColor: theme.palette.text.primary } }}
                                            />
                                        )}
                                    />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <InputLabel htmlFor="userRoleDesc">Default Branch</InputLabel>
                                    <TextField
                                        disabled
                                        fullWidth
                                        id="userRoleDesc"
                                        value={branchId}
                                        placeholder="Default Branch"
                                    />
                                </Grid>
                            </Grid>
                        </Grid>
                    </MainCard>
                </Grid>

                <Grid item xs={12} sm={12}>
                    <MainCard title="Branches Details" content={false} secondary={
                        <Button
                            variant="contained"
                            startIcon={<PlusOutlined />}
                            size="small"
                            onClick={handleAddEdit}
                        >
                            Add New
                        </Button>
                    }>

                        <ScrollX>
                            <ReactTable columns={columns1} data={DefaultBranchTable} getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()} />
                        </ScrollX>

                        <Dialog
                            maxWidth="sm"
                            TransitionComponent={PopupTransition}
                            keepMounted
                            fullWidth
                            onClose={handleAddEdit}
                            open={addEdit}
                            sx={{ '& .MuiDialog-paper': { p: 0 }, transition: 'transform 225ms' }}
                            aria-describedby="alert-dialog-slide-description"
                        >
                            <AddEditMultipleBranches userId={userId} onCancel={handleAddEdit} multipleBranch={multipleBranches!} />
                        </Dialog>
                        <AlertBranchDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={brachDeptId} />
                    </MainCard>

                </Grid>

            </Grid>
        </>
    );
};

export default List;
