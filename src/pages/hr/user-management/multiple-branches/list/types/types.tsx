import { Column } from 'react-table';
import { HeaderGroup } from 'react-table';
import { BranchesNotAssignedToUser, queryParamsProps } from 'types/multiple-branch';
export interface TableProps {
    columns: Column[];
    data: dataProps[];

    selectedData?: React.Dispatch<React.SetStateAction<number[]>>
    getHeaderProps: (column: HeaderGroup) => {};

}

export interface TableHeaderProps {
    headerGroups: HeaderGroup[];

}

export interface userRqueryParamsProps extends queryParamsProps { }

export interface dataProps extends BranchesNotAssignedToUser { }

export interface ReactTableProps {
    columns: Column[]
    data: dataProps[]
    handleAddEdit: () => void
    getHeaderProps: (column: HeaderGroup) => {};
}

export interface userProps extends BranchesNotAssignedToUser { }

