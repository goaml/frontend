// import { Dispatch, SetStateAction } from 'react';
//import { Dispatch, SetStateAction } from 'react';
import { Column, HeaderGroup } from 'react-table';
import { MyClientPerson } from 'types/person-my-client';

export interface dataProps extends MyClientPerson {}


export interface ReactTableProps {
    columns: Column[]
    data: dataProps[]
    getHeaderProps: (column: HeaderGroup) => {};
}

export interface tPersonMyclientProps  extends MyClientPerson{}

// export interface TableParamsType {
//   page: number;
//   setPage: Dispatch<SetStateAction<number>>;
//   perPage: number;
//   setPerPage: Dispatch<SetStateAction<number>>;
//   direction: "asc" | "desc";
//   setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
//   sort: string;
//   setSort: Dispatch<SetStateAction<string>>;

// }
