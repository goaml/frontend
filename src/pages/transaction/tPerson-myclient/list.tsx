import { Fragment, MouseEvent, useEffect, useMemo, useState } from 'react';

// material-ui
import {
  alpha,
  Box,
  Dialog,
  IconButton,
  LinearProgress,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
  Typography,
  useMediaQuery,
} from '@mui/material';
import { useTheme } from '@mui/material/styles';

// third-party
import { Cell, Column, HeaderGroup, Row, useFilters, useGlobalFilter, usePagination, useSortBy, useTable } from 'react-table';

// project import

import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import { HeaderSort, TablePagination } from 'components/third-party/ReactTable';

// utils
import {
  GlobalFilter,
  renderFilterTypes
} from 'utils/react-table';

// sections

// assets
import { ArrowLeftOutlined, EyeTwoTone } from '@ant-design/icons';

//types 

import { PopupTransition } from 'components/@extended/Transitions';
import { useLocation, useNavigate } from 'react-router';
import AddEditTPersonMyclient from 'sections/transactions/tPerson-myclient/AddEditTPersonMyclient';
import { useDispatch, useSelector } from 'store';
import { fetchMyClientPersonTable, toInitialState } from 'store/reducers/person-my-client';
import { openSnackbar } from 'store/reducers/snackbar';
import { queryParamsProps } from 'types/person-my-client';
import { dataProps, ReactTableProps, tPersonMyclientProps } from './types/types';
// import theme from 'themes/theme';


// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, getHeaderProps }: ReactTableProps) {
  const theme = useTheme();
  const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));
  const sortBy = { id: 'id', desc: false };
  const filterTypes = useMemo(() => renderFilterTypes, []);

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    preGlobalFilteredRows,
    setGlobalFilter,
    page,
    gotoPage,
    setPageSize,
    state: { globalFilter, pageIndex, pageSize }
  } = useTable(
    {
      columns,
      data,
      filterTypes,
      initialState: { pageIndex: 0, pageSize: 10, hiddenColumns: ['avatar'], sortBy: [sortBy] }
    },
    useGlobalFilter,
    useFilters,
    useSortBy,
    usePagination
  );

  return (
    <Box sx={{ width: '100%', overflowX: 'auto' }}>
      <Stack spacing={3}>
        <Stack
          direction={matchDownSM ? 'column' : 'row'}
          spacing={2}
          justifyContent="space-between"
          alignItems={matchDownSM ? 'stretch' : 'center'}
          sx={{ p: 3, pb: 0 }}
        >
          <Box sx={{ width: matchDownSM ? '100%' : 'auto' }}>
            <GlobalFilter
              preGlobalFilteredRows={preGlobalFilteredRows}
              globalFilter={globalFilter}
              setGlobalFilter={setGlobalFilter}
              size="small"
            />
          </Box>
        </Stack>

        <Box sx={{ 
          width: '100%',
          overflowX: 'auto',
          '& .MuiTable-root': {
            minWidth: 750,
            borderCollapse: 'separate',
            borderSpacing: '0 8px'
          }
        }}>
          <Table {...getTableProps()}>
            <TableHead>
              {headerGroups.map((headerGroup: HeaderGroup<{}>, index: number) => (
                <TableRow {...headerGroup.getHeaderGroupProps()}>
                  {headerGroup.headers.map((column: HeaderGroup, colIndex: number) => (
                    <TableCell 
                      {...column.getHeaderProps([{ className: column.className, key: colIndex }, getHeaderProps(column)])}
                      sx={{ 
                        whiteSpace: 'nowrap',
                        px: 2,
                        background: theme.palette.background.paper,
                        position: 'sticky',
                        top: 0,
                        zIndex: 1
                      }}
                    >
                      <HeaderSort column={column} />
                    </TableCell>
                  ))}
                </TableRow>
              ))}
            </TableHead>
            <TableBody {...getTableBodyProps()}>
              {page.length === 0 ? (
                <TableRow>
                  <TableCell colSpan={columns.length} align="center">
                    No data
                  </TableCell>
                </TableRow>
              ) : (
                page.map((row: Row, i: number) => {
                  prepareRow(row);
                  return (
                    <Fragment key={i}>
                      <TableRow
                        {...row.getRowProps()}
                        sx={{
                          cursor: 'pointer',
                          bgcolor: row.isSelected ? alpha(theme.palette.primary.lighter, 0.35) : theme.palette.background.paper,
                          '&:hover': {
                            bgcolor: alpha(theme.palette.primary.lighter, 0.2)
                          }
                        }}
                      >
                        {row.cells.map((cell: Cell, cellIndex: number) => (
                          <TableCell 
                            {...cell.getCellProps([{ className: cell.column.className, key: cellIndex }])}
                            sx={{ 
                              whiteSpace: 'nowrap',
                              overflow: 'hidden',
                              textOverflow: 'ellipsis',
                              maxWidth: '200px',
                              px: 2
                            }}
                          >
                            {cell.render('Cell')}
                          </TableCell>
                        ))}
                      </TableRow>
                    </Fragment>
                  );
                })
              )}
            </TableBody>
          </Table>
        </Box>

        <Box sx={{ p: 2, borderTop: `1px solid ${theme.palette.divider}` }}>
          <TablePagination
            gotoPage={gotoPage}
            rows={rows}
            setPageSize={setPageSize}
            pageIndex={pageIndex}
            pageSize={pageSize}
          />
        </Box>
      </Stack>
    </Box>
  );
}

// ==============================|| List ||============================== //

const List = () => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const { MyClientPersonList, error, success, isLoading } = useSelector(state => state.personMyClient);

  const navigate = useNavigate();
  const location = useLocation();

  // Extract trxId from state
  const MyClientId = location.state?.MyClientId;
  // table
  const [data, setData] = useState<dataProps[]>([])


  const columns = useMemo(
    () =>
      [
        {
          Header: '#',
          accessor: 'id',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            if (row.id === undefined || row.id === null || row.id === '') {
              return <>-</>
            }
            if (typeof row.id === 'string') {
              return <>{(parseInt(row.id) + 1).toString()}</>;
            }
            if (typeof row.id === 'number') {
              return <>{row.id + 1}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          Header: 'Person My Client ID',
          accessor: 'personMyClientId',

        },

        {
          Header: 'Title',
          accessor: 'title',

        },
        {
          Header: 'First Name',
          accessor: 'firstName',

        },
        {
          Header: 'Last Name',
          accessor: 'lastName',

        },
        {
          Header: 'DOB',
          accessor: 'birthdate',
          Cell: ({ row }) => {
            const value = row.values.birthdate;
            if (!value) return <>-</>;
            const formattedDate = new Date(value).toISOString().split('T')[0];
            return <>{formattedDate}</>;
          },
        },
        {
          Header: 'Birth Place',
          accessor: 'birthPlace',
          
        },
        {
          Header: 'Gender Type ID',
          accessor: 'genderTypeId',

        },
        {
          Header: 'Mother\'s Name',
          accessor: 'mothersName',

        },
        {
          Header: 'Alias',
          accessor: 'alias',

        },
        {
          Header: 'SSN',
          accessor: 'ssn',

        },
        {
          Header: 'Passport Number',
          accessor: 'passportNumber',

        },
        {
          Header: 'Passport Country Code ID',
          accessor: 'passportCountryCodeId',

        },
        {
          Header: 'NIC',
          accessor: 'idNumber',

        },
        {
          Header: 'Nationality 1 Country Code ID',
          accessor: 'nationalityCountryId',

        },
        {
          Header: 'Nationality 2 Country Code ID',
          accessor: 'nationalityCountry2Id',

        },
        {
          Header: 'Nationality 3 Country Code ID',
          accessor: 'nationalityCountry3Id',

        },
        {
          Header: 'Residence Country Code ID',
          accessor: 'residenceCountryId',

        },
        {
          Header: 'Mobile Number',
          accessor: 'phones',

        },
        {
          Header: 'Addresses',
          accessor: 'addresses',

        },
        {
          Header: 'Email',
          accessor: 'email',

        },
        {
          Header: 'Occupation',
          accessor: 'occupation',

        },
        {
          Header: 'Comments',
          accessor: 'comments',

        },
        {
          Header: 'Status',
          accessor: 'isActive',
          Cell: ({ row }: { row: Row }) => {
            if (row.values.isActive === undefined || row.values.isActive === null || row.values.isActive === '') {
              return <>-</>
            }
            if (typeof row.values.isActive === 'string') {
              return <>{row.values.isActive}</>;
            }
            if (typeof row.values.isActive === 'number') {
              return <>{row.values.isActive}</>;
            }
            if (typeof row.values.isActive === 'boolean') {
              return <>{row.values.isActive ? "ACTIVE" : "IN-ACTIVE"}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          id: "actions",
          Header: 'Actions',
          accessor: 'actions',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            const data: dataProps = row.original
            return (
              <>
                <Stack direction="row" alignItems="center" justifyContent="center" spacing={0}>
                  {/* <Tooltip title="View">
                                        <IconButton
                                            color="secondary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                            }}
                                        >
                                            <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                                        </IconButton>
                                    </Tooltip> */}
                  <Tooltip title="Edit">
                    <IconButton
                      color="primary"
                      onClick={(e: MouseEvent<HTMLButtonElement>) => {
                        e.stopPropagation();
                        handleAddEdit()
                        settPersonMyclient({
                          ...data
                        })
                      }}
                    >
                      <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                    </IconButton>
                  </Tooltip>
                </Stack>
              </>
            )
          }
        }
      ] as Column[],
    []
  );

  //dialog model 
  const [addEdit, setAddEdit] = useState<boolean>(false);
  const [tPersonMyclientCode, settPersonMyclient] = useState<tPersonMyclientProps>();

  const handleAddEdit = () => {
    setAddEdit(!addEdit);
    if (tPersonMyclientCode && !addEdit) settPersonMyclient(undefined);
  };

  // //================================API CONFIG=============================//

  useEffect(() => {
    const listParameters: queryParamsProps = {
      page: 0,
      per_page: 1000,
      direction: 'asc',
      sort: 'personMyClientId',
      search: '',
      personMyClientId: MyClientId
    };
    console.log('Fetching data with params:', listParameters);
    dispatch(fetchMyClientPersonTable(listParameters));
  }, [dispatch]);

  useEffect(() => {
    if (MyClientPersonList?.result) {
      const filteredData = MyClientId ? MyClientPersonList.result.filter((item) => item.personMyClientId === MyClientId) : MyClientPersonList.result;
      console.log('Filtered Data:', filteredData);
      setData(filteredData);
    }
  }, [MyClientPersonList, MyClientId]);

  useEffect(() => {
    if (error != null && typeof error === 'object' && 'message' in error) {
      console.log('Error:', error);
      dispatch(
        openSnackbar({
          open: true,
          message: (error as { message: string }).message,
          variant: 'alert',
          alert: {
            color: 'error'
          },
          close: true
        })
      );
      dispatch(toInitialState());
    }
  }, [error]);

  useEffect(() => {
    if (success != null) {
      console.log('Success:', success);
      dispatch(
        openSnackbar({
          open: true,
          message: success,
          variant: 'alert',
          alert: {
            color: 'success'
          },
          close: true
        })
      );
      dispatch(toInitialState())
    }
  }, [success])

  if (isLoading) {
    return (
      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
        <Box sx={{ width: '50%' }}>
          <LinearProgress />
        </Box>
      </div>
    );
  }

  return (
    <>
    <Box sx={{ width: '100%', height: '100%' }}>
      <MainCard content={false} title={
        <Stack direction="row" alignItems="center" spacing={1}>
          <IconButton onClick={() => {
            navigate(-1)
 
          }}>
            <ArrowLeftOutlined />
          </IconButton>
          <Typography sx={{ fontWeight: 'bold', fontSize: '1.5rem' }}>
            Transactions - Transaction Person
          </Typography>
        </Stack>
      }>
        <ScrollX>
          <ReactTable columns={columns} data={data} getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()} />
        </ScrollX>
        {/* add / edit submissionCode dialog */}
        <Dialog
          maxWidth="sm"
          TransitionComponent={PopupTransition}
          keepMounted
          fullWidth
          onClose={handleAddEdit}
          open={addEdit}
          sx={{ '& .MuiDialog-paper': { p: 0 }, transition: 'transform 225ms' }}
          aria-describedby="alert-dialog-slide-description"
        >
          <AddEditTPersonMyclient TPersonMyclientAddEditTPersonMyclient={tPersonMyclientCode} onCancel={handleAddEdit} />
        </Dialog>
        {/* alert model */}
        {/* {transFromId && <AlertStatusTypeDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={transFromId} />} */}
      </MainCard>
      </Box>
    </>
  );
}

export default List;
