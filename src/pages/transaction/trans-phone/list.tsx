import { Fragment, MouseEvent, useEffect, useMemo, useState } from 'react';

// material-ui
import {
  alpha,
  Dialog,
  IconButton,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
  useMediaQuery,
  useTheme
} from '@mui/material';

// third-party
import { Cell, Column, HeaderGroup, Row, useFilters, useGlobalFilter, usePagination, useSortBy, useTable } from 'react-table';

// project import
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import { HeaderSort, TablePagination } from 'components/third-party/ReactTable';



// utils
import {
  GlobalFilter,
  renderFilterTypes
} from 'utils/react-table';

// sections

// assets
import { EyeTwoTone } from '@ant-design/icons';

//types
import { useDispatch, useSelector } from 'store';
import { openSnackbar } from 'store/reducers/snackbar';
import { fetchTransPhonesTable, toInitialState } from 'store/reducers/trans-phone';
import { queryParamsProps } from 'types/trans-phone';
import { dataProps, ReactTableProps, transPhoneProps } from './types/types';
import { PopupTransition } from 'components/@extended/Transitions';
import AddEditTransPhone from 'sections/transactions/trans-phone/AddEditTransPhone';


// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, getHeaderProps }: ReactTableProps) {
  const theme = useTheme();

  const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));
  const sortBy = { id: 'id', desc: false };

  const filterTypes = useMemo(() => renderFilterTypes, []);

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    rows,
    gotoPage,
    setPageSize,
    state: { globalFilter, pageIndex, pageSize },
    preGlobalFilteredRows,
    setGlobalFilter,
  } = useTable(
    {
      columns,
      data,
      filterTypes,
      initialState: { pageIndex: 0, pageSize: 10, hiddenColumns: ['avatar'], sortBy: [sortBy] }
    },
    useGlobalFilter,
    useFilters,
    useSortBy, 
    usePagination
  );

  return (
    <>
      <Stack spacing={3}>
        <Stack
          direction={matchDownSM ? 'column' : 'row'}
          spacing={1}
          justifyContent="space-between"
          alignItems="center"
          sx={{ p: 3, pb: 0 }}
        >
          <GlobalFilter
            preGlobalFilteredRows={preGlobalFilteredRows}
            globalFilter={globalFilter}
            setGlobalFilter={setGlobalFilter}
            size="small"
          />
        </Stack>
      </Stack>

      <Stack>
        <Table {...getTableProps()}>
          <TableHead>
            {headerGroups.map((headerGroup: HeaderGroup<{}>) => (
              <TableRow {...headerGroup.getHeaderGroupProps()} sx={{ '& > th:first-of-type': { width: '58px' } }}>
                {headerGroup.headers.map((column: HeaderGroup) => (
                  <TableCell {...column.getHeaderProps([{ className: column.className }, getHeaderProps(column)])}
                  // sx={{ textAlign: column.isNumeric ? 'right' : 'left' }}
                  >
                    <HeaderSort column={column} />
                  </TableCell>
                ))}
              </TableRow>
            ))}
          </TableHead>
          <TableBody {...getTableBodyProps()}>
            {page.map((row: Row, i: number) => {
              prepareRow(row);
              return (
                <Fragment key={i}>
                  <TableRow
                    {...row.getRowProps()}
                    onClick={() => {
                      row.toggleRowSelected();
                    }}
                    sx={{ cursor: 'pointer', bgcolor: row.isSelected ? alpha(theme.palette.primary.lighter, 0.35) : 'inherit' }}
                  >
                    {row.cells.map((cell: Cell) => (
                      <TableCell {...cell.getCellProps([{ className: cell.column.className }])}
                      sx={{ textAlign: cell.column.isNumeric ? 'left' : 'right' }}
                      >{cell.render('Cell')}</TableCell>
                    ))}
                  </TableRow>
                </Fragment>
              );
            })}
            <TableRow>
              <TableCell sx={{ p: 2 }} colSpan={12}>
                <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
              </TableCell>
            </TableRow>
          </TableBody>
        </Table >
      </Stack>
    </>
  );
}

// ==============================|| List ||============================== //

const List = () => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const { transPhoneList, error, success, isLoading } = useSelector(state => state.transPhone);


  // table
  const [data, setData] = useState<dataProps[]>([])


  const columns = useMemo(
    () =>
      [
        {
          Header: '#',
          accessor: 'id',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            if (row.id === undefined || row.id === null || row.id === '') {
              return <>-</>
            }
            if (typeof row.id === 'string') {
              return <>{(parseInt(row.id) + 1).toString()}</>;
            }
            if (typeof row.id === 'number') {
              return <>{row.id + 1}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          Header: 'Communication Type ID',
          accessor: 'communicationTypeId',
          Cell: ({ row }) => {
            const value = row.values.communicationTypeId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          },
        },
        {
          Header: 'Country Prefix',
          accessor: 'tphCountryPrefix',
          Cell: ({ row }) => {
            const value = row.values.tphCountryPrefix;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          },
        },
        {
          Header: 'Phone Number',
          accessor: 'tphNumber',
          Cell: ({ row }) => {
            const value = row.values.tphNumber;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          },
        },
        {
          Header: 'Extension',
          accessor: 'tphExtension',
          Cell: ({ row }) => {
            const value = row.values.tphExtension;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          },
        },
        {
          Header: 'Comments',
          accessor: 'comments',
          Cell: ({ row }) => {
            const value = row.values.comments;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          },
        },

        {
          Header: 'Status',
          accessor: 'isActive',
          Cell: ({ row }: { row: Row }) => {
            if (row.values.isActive === undefined || row.values.isActive === null || row.values.isActive === '') {
              return <>-</>
            }
            if (typeof row.values.isActive === 'string') {
              return <>{row.values.isActive}</>;
            }
            if (typeof row.values.isActive === 'number') {
              return <>{row.values.isActive}</>;
            }
            if (typeof row.values.isActive === 'boolean') {
              return <>{row.values.isActive ? "ACTIVE" : "IN-ACTIVE"}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          id: "actions",
          Header: 'Actions',
          accessor: 'actions',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            const data: dataProps = row.original
            return (
              <>
                <Stack direction="row" alignItems="center" justifyContent="center" spacing={0}>
                  <Tooltip title="Edit">
                    <IconButton
                      color="primary"
                      onClick={(e: MouseEvent<HTMLButtonElement>) => {
                        e.stopPropagation();
                        handleAddEdit()
                        settransPhoneType({
                          ...data
                        })
                      }}
                    >
                      <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                    </IconButton>
                  </Tooltip>
                </Stack>
              </>
            )
          }
        }
      ] as Column[],
    []
  );

  //dialog model 
  const [addEdit, setAddEdit] = useState<boolean>(false);
  const [transPhoneTypeCode, settransPhoneType] = useState<transPhoneProps>();

  const handleAddEdit = () => {
    setAddEdit(!addEdit);
    if (transPhoneTypeCode && !addEdit) settransPhoneType(undefined);
  };

  // //================================API CONFIG=============================//

  const [page] = useState<number>(0);
  const [perPage] = useState<number>(10);
  const [direction] = useState<"asc" | "desc">("asc");
  const [sort] = useState<string>("phoneId"); //CHANGE ID


  useEffect(() => {
    const listParameters: queryParamsProps = {
      page: page,
      per_page: perPage,
      direction: direction,
      sort: sort,
      search: ''
    };
    dispatch(fetchTransPhonesTable(listParameters));
  }, [dispatch, success, page, perPage, direction, sort]);

  useEffect(() => {
    setData(transPhoneList?.result! || []);
  }, [transPhoneList])

  //  handel error 
  useEffect(() => {
    if (error != null && typeof error === 'object' && 'message' in error) {
      dispatch(
        openSnackbar({
          open: true,
          message: (error as { message: string }).message, // Type assertion
          variant: 'alert',
          alert: {
            color: 'error'
          },
          close: true
        })
      );
      dispatch(toInitialState());
    }
  }, [error]);

  //  handel success
  useEffect(() => {
    if (success != null) {
      dispatch(
        openSnackbar({
          open: true,
          message: success,
          variant: 'alert',
          alert: {
            color: 'success'
          },
          close: true
        })
      );
      dispatch(toInitialState())
    }
  }, [success])


  if (isLoading) {
    return <div>Loading...</div>;
  }


  return (
    <>
      <MainCard content={false}>
        <ScrollX>
          <ReactTable columns={columns} data={data}  getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()} />
        </ScrollX>
        {/* add / edit submissionCode dialog */}
        <Dialog
          maxWidth="sm"
          TransitionComponent={PopupTransition}
          keepMounted
          fullWidth
          onClose={handleAddEdit}
          open={addEdit}
          sx={{ '& .MuiDialog-paper': { p: 0 }, transition: 'transform 225ms' }}
          aria-describedby="alert-dialog-slide-description"
        >
          <AddEditTransPhone transPhone={transPhoneTypeCode} onCancel={handleAddEdit} />
        </Dialog>
      </MainCard>
    </>
  );
}

export default List;
