// import { Dispatch, SetStateAction } from 'react';
import { Dispatch, SetStateAction } from 'react';
import { Column, HeaderGroup } from 'react-table';
import { transFromMyClient } from 'types/trans-from-myclient';

export interface dataProps extends transFromMyClient{}


export interface ReactTableProps {
  columns: Column[]
  data: dataProps[]
  getHeaderProps: (column: HeaderGroup) => {};
}

export interface transFromMyClientProps  extends transFromMyClient{}

export interface TableParamsType {
  page: number;
  setPage: Dispatch<SetStateAction<number>>;
  perPage: number;
  setPerPage: Dispatch<SetStateAction<number>>;
  direction: "asc" | "desc";
  setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
  sort: string;
  setSort: Dispatch<SetStateAction<string>>;

}
