import { Fragment, useEffect, useMemo, useState } from 'react';

// material-ui
import {
  alpha,
  Box,
  Button,
  Grid,
  IconButton,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
  useMediaQuery,
  useTheme,
} from '@mui/material';

// third-party
import {
  Cell, Column, HeaderGroup, Row, useFilters, useGlobalFilter, usePagination, useSortBy, useTable
} from 'react-table';

// project import

import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import { CSVExport, HeaderSort, TablePagination } from 'components/third-party/ReactTable';

// utils
import {
  GlobalFilter,
  renderFilterTypes
} from 'utils/react-table';

// sections

// assets
import { ArrowLeftOutlined, FilterTwoTone } from '@ant-design/icons';

//types 
import { Typography } from '@mui/material';
import LinearProgress from '@mui/material/LinearProgress';
import FilterDialog from 'pages/transaction/trans-transaction/FilterDialog';
import { useNavigate, useParams } from 'react-router';
import { dispatch, useDispatch, useSelector } from 'store';
import { openSnackbar } from 'store/reducers/snackbar';
import { fetchTransTransactionsTable, toInitialState } from 'store/reducers/trans-transaction';
import { queryParamsProps } from 'types/trans-transaction';
import { dataProps, ReactTableProps, TableParamsType } from './types/types';


// ==============================|| REACT TABLE ||============================== //
function ReactTable({ columns, data, getHeaderProps, tableParams }: ReactTableProps) {
  const theme = useTheme();
  const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));
  const filterTypes = useMemo(() => renderFilterTypes, []);
  const { reportId } = useParams();
  const [filterDialogOpen, setFilterDialogOpen] = useState(false);
  const [filteredData, setFilteredData] = useState(data);

  useEffect(() => {
    setFilteredData(data);
  }, [data]);

  const handleFilterDialogOpen = () => setFilterDialogOpen(true);
  const handleFilterDialogClose = () => setFilterDialogOpen(false);

  const handleApplyFilters = (filters: { [key: string]: string }) => {
    console.log(filters, 'filters');

    setFilterDialogOpen(false);
    tableParams.setPage(0);
    tableParams.setTrxNo(parseInt(filters.trxNo) || undefined);
    tableParams.setRptCode(filters.rptCode);
    tableParams.setDateFrom(filters.dateFrom);
    tableParams.setDateTo(filters.dateTo);
    dispatch(fetchTransTransactionsTable({ ...filters, page: 0, per_page: 10, direction: 'asc', sort: 'trxNo', search: '', reportId:Number(reportId!) })); // Apply filters and reset pagination
  };

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    preGlobalFilteredRows,
    setGlobalFilter,
    page,
    gotoPage,
    setPageSize,
    state: { globalFilter, pageIndex, pageSize }
  } = useTable(
    {
      columns,
      data: filteredData,
      filterTypes,
      initialState: { pageIndex: 0, pageSize: 10, hiddenColumns: ['avatar'], sortBy: [{ id: 'id', desc: false }] }
    },
    useGlobalFilter,
    useFilters,
    useSortBy,
    usePagination
  );

  return (
    <>
      <Stack spacing={2}>
        <Stack
          direction={matchDownSM ? 'column' : 'row'}
          spacing={1}
          justifyContent="space-between"
          alignItems="center"
          sx={{ p: 3, pb: 0 }}
        >
          <GlobalFilter
            preGlobalFilteredRows={preGlobalFilteredRows}
            globalFilter={globalFilter}
            setGlobalFilter={setGlobalFilter}
            size="small"
          />

          <Stack direction="row" spacing={1} justifyContent="flex-end" alignItems="center">
            <Tooltip title="Filter Columns">
              <IconButton onClick={handleFilterDialogOpen}>
                <FilterTwoTone />
              </IconButton>
            </Tooltip>

            <CSVExport
              data={rows.map((d: Row) => d.original)}
              filename={'TransactionList.csv'}
            />
          </Stack>
        </Stack>
      </Stack>

      <Stack>
        <Table {...getTableProps()}>
          <TableHead>
            {headerGroups.map((headerGroup: HeaderGroup<{}>) => (
              <TableRow {...headerGroup.getHeaderGroupProps()} sx={{ '& > th:first-of-type': { width: '58px' } }}>
                {headerGroup.headers.map((column: HeaderGroup) => (
                  <TableCell {...column.getHeaderProps([{ className: column.className }, getHeaderProps(column)])}>
                    <HeaderSort column={column} />
                  </TableCell>
                ))}
              </TableRow>
            ))}
          </TableHead>
          <TableBody {...getTableBodyProps()}>
            {page.map((row: Row, i: number) => {
              prepareRow(row);
              return (
                <Fragment key={i}>
                  <TableRow
                    {...row.getRowProps()}
                    sx={{ cursor: 'pointer', bgcolor: row.isSelected ? alpha(theme.palette.primary.lighter, 0.35) : 'inherit' }}
                  >
                    {row.cells.map((cell: Cell) => (
                      <TableCell
                        {...cell.getCellProps([{ className: cell.column.className }])}
                        sx={{ textAlign: cell.column.isNumeric ? 'left' : 'right' }}
                      >
                        {cell.render('Cell')}
                      </TableCell>
                    ))}
                  </TableRow>
                </Fragment>
              );
            })}
            <TableRow>
              <TableCell sx={{ p: 2 }} colSpan={12}>
                <TablePagination
                  gotoPage={gotoPage}
                  rows={rows}
                  setPageSize={setPageSize}
                  pageIndex={pageIndex}
                  pageSize={pageSize}
                />
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>

        <FilterDialog
          open={filterDialogOpen}
          onClose={handleFilterDialogClose}
          onApplyFilters={handleApplyFilters}
        />
      </Stack>
    </>
  );

}

// ==============================|| List ||============================== //

const List = () => {
  //const theme = useTheme();
  const dispatch = useDispatch();
  const { transTransactionList, error, success, isLoading } = useSelector(state => state.transTransaction);
  const { reportId } = useParams();
  const navigate = useNavigate();

  // table
  const [data, setData] = useState<dataProps[]>([])

  const columns = useMemo(
    () =>
      [
        {
          Header: '#',
          accessor: 'id',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            if (row.id === undefined || row.id === null || row.id === '') {
              return <>-</>
            }
            if (typeof row.id === 'string') {
              return <>{(parseInt(row.id) + 1).toString()}</>;
            }
            if (typeof row.id === 'number') {
              return <>{row.id + 1}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          Header: () => (
            <>
            Trx <br />Number
            </>
          ),
          accessor: 'transactionNumber',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.transactionNumber;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: () => (
            <>
             Internal <br />Ref <br />Number
            </>
          ),
          accessor: 'internalRefNumber',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.internalRefNumber;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: () => (
            <>
            Trx <br />Location
            </>
          ),
          accessor: 'transactionLocation',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.transactionLocation;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Trx Description  ',
          accessor: 'transactionDescription',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.transactionDescription;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Trx Date',
          accessor: 'dateTransaction',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.dateTransaction;
            if (!value || !Array.isArray(value) || value.length !== 3) return <>-</>;

            // Extract the year, month, and day from the array
            const [day, month, year] = value;

            // Create a Date object using the extracted values
            const date = new Date(day, month - 1, year); // Month is 0-indexed in JS Date

            // Format the date to 'DD/MM/YYYY'
            const formattedDate = date.toLocaleDateString('en-GB', {
              day: '2-digit',
              month: '2-digit',
              year: 'numeric'
            });

            return <>{formattedDate}</>;
          }
        },
        {
          Header: 'GoAML Code',
          accessor: 'trxNo',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.trxNo;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'RPT Code',
          accessor: 'rptCode',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.rptCode;
            if (value === undefined || value === null) return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Date Posting',
          accessor: 'datePosting',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.datePosting;
            if (value === undefined || value === null) return <>-</>;
            const date = new Date(value);
            const formattedDate = date.toLocaleDateString('en-GB', {
              day: '2-digit',
              month: '2-digit',
              year: 'numeric'
            });
            return <>{formattedDate}</>;
          }
        },
        {
          Header: () => (
            <>
              Credit/<br />Debit
            </>
          ),
          accessor: 'transmodeComment',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.transmodeComment;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: () => (
            <span style={{ whiteSpace: 'nowrap' }}>Amount Local</span>
          ),
          accessor: 'amountLocal',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.amountLocal;
            if (value === undefined || value === null) return <>-</>;
            // Adjust formatting based on how you want to display the amount
            const formattedAmount = value.toLocaleString('en-US', { minimumFractionDigits: 2 });
            return <>{formattedAmount}</>;
          }
        },
        {
          Header: 'Comments',
          accessor: 'comments',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.comments;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },


        {
          Header: 'Status',
          accessor: 'isActive',
          Cell: ({ row }: { row: Row }) => {
            if (row.values.isActive === undefined || row.values.isActive === null || row.values.isActive === '') {
              return <>-</>
            }
            if (typeof row.values.isActive === 'string') {
              return <>{row.values.isActive}</>;
            }
            if (typeof row.values.isActive === 'number') {
              return <>{row.values.isActive}</>;
            }
            if (typeof row.values.isActive === 'boolean') {
              return <>{row.values.isActive ? "ACTIVE" : "IN-ACTIVE"}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          id: "actions",
          Header: 'Actions',
          accessor: 'actions',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            const data: dataProps = row.original
            return (
              <>
                <Stack direction="row" alignItems="center" justifyContent="center" spacing={1}>
                  <Stack spacing={1}>
                    <Grid container spacing={1} width={"225px"}>
                      <Grid item xs={3}>
                        <Tooltip title="Transactions From">
                          <Button
                            color="primary"
                            // sx={{ width: '100%', height: '30px', maxWidth: '120px' }}
                            variant="contained"
                            onClick={() => {
                              navigate('/transaction-management/trans-from', { state: { trxId: data.transactionId } });
                            }}
                          >
                            From
                          </Button>
                        </Tooltip>
                      </Grid>
                      <Grid item xs={9}>
                        <Tooltip title="Transactions From Client">
                          <Button
                            color="primary"
                            sx={{ width: '100%', height: '30px', maxWidth: '120px' }}
                            variant="contained"
                            onClick={() => {
                              navigate('/transaction-management/trans-from-myClient', { state: { trxId: data.transactionId } });
                            }}
                          >
                            From Client
                          </Button>
                        </Tooltip>
                      </Grid>
                      <Grid item xs={3}>
                        <Tooltip title="Transactions To">
                          <Button
                            color="primary"
                            sx={{ width: '100%', height: '30px', maxWidth: '120px' }}
                            variant="contained"
                            onClick={() => {
                              navigate('/transaction-management/trans-to', { state: { trxId: data.transactionId } });
                            }}
                          >
                            To
                          </Button>
                        </Tooltip>
                      </Grid>
                      <Grid item xs={9}>
                        <Tooltip title="Transactions To Client">
                          <Button
                            color="primary"
                            sx={{ width: '100%', height: '30px', maxWidth: '120px' }}
                            variant="contained"
                            onClick={() => {
                              navigate('/transaction-management/trans-to-myClient', { state: { trxId: data.transactionId } });
                            }}
                          >
                            To Client
                          </Button>
                        </Tooltip>
                      </Grid>
                    </Grid>
                  </Stack>
                </Stack>
              </>
            )
          }
        }
      ] as Column[],
    []
  );

  
  //================================API CONFIG=============================//

  const [page, setPage] = useState<number>(0);
  const [perPage, setPerPage] = useState<number>(1000);
  const [direction, setDirection] = useState<"asc" | "desc">("asc");
  const [sort, setSort] = useState<string>("transactionId"); //CHANGE ID
  const [dateFrom, setDateFrom] = useState<string>("");
  const [dateTo, setDateTo] = useState<string>("");
  const [trxNo, setTrxNo] = useState<number>();
  const [rptCode, setRptCode] = useState<string>("");



  const tableParams: TableParamsType = {
    page,
    setPage,
    perPage,
    setPerPage,
    direction,
    setDirection,
    sort,
    setSort,
    dateFrom,
    setDateFrom,
    dateTo,
    setDateTo,
    trxNo,
    setTrxNo,
    rptCode,
    setRptCode,
    
  }

  useEffect(() => {
    const listParameters: queryParamsProps = {
      page: page,
      per_page: perPage,
      direction: direction,
      sort: sort,
      search: '',
      reportId: parseInt(reportId!)
    };
    dispatch(fetchTransTransactionsTable(listParameters));
  }, [dispatch, success, page, perPage, direction, sort]);

  useEffect(() => {
    setData(transTransactionList?.result! || []);
  }, [transTransactionList])

  //  handel error 
  useEffect(() => {
    if (error != null && typeof error === 'object' && 'message' in error) {
      dispatch(
        openSnackbar({
          open: true,
          message: (error as { message: string }).message, // Type assertion
          variant: 'alert',
          alert: {
            color: 'error'
          },
          close: true
        })
      );
      dispatch(toInitialState());
    }
  }, [error]);

  //  handel success
  useEffect(() => {
    if (success != null) {
      dispatch(
        openSnackbar({
          open: true,
          message: success,
          variant: 'alert',
          alert: {
            color: 'success'
          },
          close: true
        })
      );
      dispatch(toInitialState())
    }
  }, [success])

  if (isLoading) {
    return (
      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
        <Box sx={{ width: '50%' }}>
          <LinearProgress />
        </Box>
      </div>
    );
  }


  return (
    <>

      <MainCard content={false} title={
        <Stack direction="row" alignItems="center" spacing={1}>
          <IconButton onClick={() => {
            navigate(-1)

          }}>
            <ArrowLeftOutlined />
          </IconButton>
          <Typography sx={{ fontWeight: 'bold', fontSize: '1.5rem' }}>
            Transactions - List
          </Typography>
        </Stack>
      } sx={{ '& .MuiTableCell-root:first-of-type': { pl: 2.5 }, '& .MuiTableCell-root:last-of-type': { pr: 2.5 } }}>
        {isLoading ? (
          <LinearProgress color="primary" sx={{ borderRadius: '0 0 4px 4px' }} />
        ) : (
          <ScrollX>
            <ReactTable columns={columns} data={data} getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()} tableParams={tableParams} />
          </ScrollX>
        )}
      </MainCard>

    </>
  );
}

export default List;
