import {
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControlLabel,
  Grid,
  InputLabel,
  Stack,
  TextField
} from '@mui/material';
import React, { useState } from 'react';
//import { DemoContainer } from '@mui/x-date-pickers/internals/demo/DemoContainer';
import { format } from 'date-fns';

interface FilterDialogProps {
  open: boolean;
  onClose: () => void;
  onApplyFilters: (filters: { [key: string]: string }) => void;
}

const FilterDialog: React.FC<FilterDialogProps> = ({ open, onClose, onApplyFilters }) => {
  const [filters, setFilters] = useState<{ [key: string]: string }>({});
  const [selectedColumns, setSelectedColumns] = useState<{ [key: string]: boolean }>({
    dateTransaction: false,
    transactionNumber: false,
    rptCode: false,
    datePosting: false,
  });

  const handleInputChange = (columnId: string) => (event: React.ChangeEvent<HTMLInputElement>) => {
    const newFilters = { ...filters, [columnId]: event.target.value };
    setFilters(newFilters);
  };

  const handleApplyFilters = () => {
    onApplyFilters(filters);
    onClose();
  };



  const handleClearFilters = () => {
    setFilters({});
    setSelectedColumns({
      dateTransaction: false,
      transactionNumber: false,
      rptCode: false,
      datePosting: false,

    });
    onApplyFilters({});
    onClose();
  };

  const handleColumnSelectionChange = (columnId: string) => (event: React.ChangeEvent<HTMLInputElement>) => {
    const newSelectedColumns = { ...selectedColumns, [columnId]: event.target.checked };
    setSelectedColumns(newSelectedColumns);
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle><b>Filter Columns</b> <hr /></DialogTitle>
      <DialogContent>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={3}>
            <FormControlLabel
              control={
                <Checkbox
                  checked={selectedColumns.dateTransaction}
                  onChange={handleColumnSelectionChange('dateTransaction')}
                  name="dateTransaction"
                  color="primary"
                />
              }
              label="TRX Date"
            />
          </Grid>

          <Grid item xs={12} sm={3}>
            <FormControlLabel
              control={
                <Checkbox
                  checked={selectedColumns.trxNo}
                  onChange={handleColumnSelectionChange('trxNo')}
                  name="trxNo"
                  color="primary"
                />
              }
              label="GoAML Code"
            />
          </Grid>
          <Grid item xs={12} sm={3}>
            <FormControlLabel
              control={
                <Checkbox
                  checked={selectedColumns.rptCode}
                  onChange={handleColumnSelectionChange('rptCode')}
                  name="rptCode"
                  color="primary"
                />
              }
              label="RPT Code"
            />
          </Grid>


          <Grid container spacing={2} mt={2}>
            {selectedColumns.trxNo && (
              <Grid item xs={12}>
                <Stack spacing={1.25}>
                  <InputLabel htmlFor="trxNo">
                    GoAML Code <span style={{ color: 'red' }}>*</span>
                  </InputLabel>
                  <TextField
                    fullWidth
                    id="trxNo"
                    name="trxNo"
                    value={filters.trxNo || ''}
                    onChange={handleInputChange('trxNo')}
                    error={false}
                    helperText={''}
                  />
                </Stack>
              </Grid>
            )}

            {selectedColumns.dateTransaction && (
              <>
                <Grid item xs={12} sm={6}>
                  <Stack spacing={1.25}>
                    <InputLabel htmlFor="dateFrom">
                      Date From <span style={{ color: 'red' }}>*</span>
                    </InputLabel>
                    <TextField
                      type="datetime-local"
                      value={
                        filters.dateFrom && !isNaN(new Date(filters.dateFrom).getTime())
                          ? format(new Date(filters.dateFrom), "yyyy-MM-dd'T'HH:mm:ss")
                          : format(new Date().setHours(0, 0, 0, 0), "yyyy-MM-dd'T'HH:mm:ss") // Defaults to today at 00:00 AM
                      }
                      onChange={(event) => {
                        const newValue = new Date(event.target.value);
                        if (!isNaN(newValue.getTime())) {
                          const formattedValue = format(newValue, "yyyy-MM-dd'T'HH:mm:ss");
                          setFilters({ ...filters, dateFrom: formattedValue });
                        } else {
                          console.error('Invalid date selected');
                        }
                      }}
                      error={false}
                      helperText={''}
                      fullWidth
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                  </Stack>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <Stack spacing={1.25}>
                    <InputLabel htmlFor="dateTo">
                      Date To <span style={{ color: 'red' }}>*</span>
                    </InputLabel>
                    <TextField
                      type="datetime-local"
                      value={
                        filters.dateTo && !isNaN(new Date(filters.dateTo).getTime())
                          ? format(new Date(filters.dateTo), "yyyy-MM-dd'T'HH:mm:ss")
                          : format(new Date().setHours(23, 59, 59, 0), "yyyy-MM-dd'T'HH:mm:ss") // Defaults to today at 11:59 PM
                      }
                      onChange={(event) => {
                        const newValue = new Date(event.target.value);
                        if (!isNaN(newValue.getTime())) {
                          const formattedValue = format(newValue, "yyyy-MM-dd'T'HH:mm:ss");
                          setFilters({ ...filters, dateTo: formattedValue });
                        } else {
                          console.error('Invalid date selected');
                        }
                      }}
                      error={false}
                      helperText={''}
                      fullWidth
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                  </Stack>
                </Grid>
              </>
            )}

            {selectedColumns.rptCode && (
              <Grid item xs={12}>
                <Stack spacing={1.25}>
                  <InputLabel htmlFor="rptCode">
                    RPT Code <span style={{ color: 'red' }}>*</span>
                  </InputLabel>
                  <TextField
                    fullWidth
                    id="rptCode"
                    name="rptCode"
                    value={filters.rptCode || ''}
                    onChange={handleInputChange('rptCode')}
                    error={false}
                    helperText={''}
                  />
                </Stack>
              </Grid>
            )}
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose}>Cancel</Button>
        <Button onClick={handleClearFilters}>Clear Filters</Button>
        <Button onClick={handleApplyFilters} color="primary" variant="contained">Apply</Button>
      </DialogActions>
    </Dialog>
  );
};

export default FilterDialog;
