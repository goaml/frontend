// import { Dispatch, SetStateAction } from 'react';
import { Dispatch, SetStateAction } from 'react';
import { Column, HeaderGroup } from 'react-table';
import { transTransaction } from 'types/trans-transaction';

export interface dataProps extends transTransaction { }


// export interface ReactTableProps {
//   columns: Column[]
//   data: dataProps[]
//   handleView: () => void
//   tableParams: TableParamsType
// }
export interface ReactTableProps {
  columns: Column[]
  data: dataProps[]
  getHeaderProps: (column: HeaderGroup) => {};
  // handleAddEdit: () => void
  tableParams: TableParamsType
}

export interface tTransProps extends transTransaction { }

export interface TableParamsType {
  page: number;
  setPage: Dispatch<SetStateAction<number>>;
  perPage: number;
  setPerPage: Dispatch<SetStateAction<number>>;
  direction: "asc" | "desc";
  setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
  sort: string;
  setSort: Dispatch<SetStateAction<string>>;
  dateFrom: string;
  setDateFrom: Dispatch<SetStateAction<string>>;
  dateTo: string;
  setDateTo: Dispatch<SetStateAction<string>>;
  trxNo?: number
  setTrxNo: Dispatch<SetStateAction<number | undefined>>;
  rptCode: string;
  setRptCode: Dispatch<SetStateAction<string>>;
}
