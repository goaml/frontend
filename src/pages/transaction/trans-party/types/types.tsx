import { Dispatch, SetStateAction } from 'react';
import { Column } from 'react-table';
import { transParty } from 'types/trans-party';

export interface dataProps extends transParty{}


export interface ReactTableProps {
  columns: Column[]
  data: dataProps[]
  handleAddEdit: () => void
  tableParams: TableParamsType
}

export interface transPartyProps  extends transParty{}

export interface TableParamsType {
  page: number;
  setPage: Dispatch<SetStateAction<number>>;
  perPage: number;
  setPerPage: Dispatch<SetStateAction<number>>;
  direction: "asc" | "desc";
  setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
  sort: string;
  setSort: Dispatch<SetStateAction<string>>;

}
