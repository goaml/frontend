import { MouseEvent, useMemo, useState, useEffect } from 'react';

// material-ui
import {
  //Button,
  Dialog,
  //Dialog,
  IconButton,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
  useTheme,
} from '@mui/material';

// third-party
import { Cell, Column, HeaderGroup, Row, useFilters, useGlobalFilter, usePagination, useTable } from 'react-table';

// project import
// import { PopupTransition } from 'components/@extended/Transitions';
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import { EmptyTable, TablePagination } from 'components/third-party/ReactTable';

// utils
import {
  DefaultColumnFilter,
  GlobalFilter,
  renderFilterTypes
} from 'utils/react-table';

// sections

// assets
import { EyeTwoTone } from '@ant-design/icons';

//types 
// import { useDispatch, useSelector } from 'store';
// import { fetchtransPartys, toInitialState } from 'store/reducers/branch-para';
// import { openSnackbar } from 'store/reducers/snackbar';
// import { listParametersType } from 'types/branch-para';
import { ReactTableProps, TableParamsType, dataProps, transPartyProps } from './types/types';
import { PopupTransition } from 'components/@extended/Transitions';
import AddEditTransParty from 'sections/transactions/trans-party/AddEditTransToParty';
import { useDispatch, useSelector } from 'store';
import { queryParamsProps } from 'types/trans-party';
import { fetchTransPartysTable, toInitialState } from 'store/reducers/trans-party';
import { openSnackbar } from 'store/reducers/snackbar';
// import AlertStatusTypeDelete from 'sections/parameter-management/status-code/DeleteStatusType';


// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, handleAddEdit }: ReactTableProps) {
  const filterTypes = useMemo(() => renderFilterTypes, []);
  const defaultColumn = useMemo(() => ({ Filter: DefaultColumnFilter }), []);

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    preGlobalFilteredRows,
    setGlobalFilter,
    globalFilter,
    page,
    gotoPage,
    setPageSize,
    state: { pageIndex, pageSize }
  } = useTable(
    {
      columns,
      data,
      defaultColumn,
      filterTypes,

      initialState: { pageIndex: 0, pageSize: 10 }
    },
    useGlobalFilter,
    useFilters,
    usePagination
  );

  return (
    <>
      <Stack direction="row" spacing={2} justifyContent="space-between" sx={{ padding: 2 }}>
        <GlobalFilter preGlobalFilteredRows={preGlobalFilteredRows} globalFilter={globalFilter} setGlobalFilter={setGlobalFilter} />
        <Stack direction="row" alignItems="center" spacing={1}>
          {/* <CSVExport data={rows.map((d: Row) => d.original)} filename={'filtering-table.csv'} /> */}
          {/* <Button variant="contained" startIcon={<PlusOutlined />} onClick={handleAddEdit}>
            Add New
          </Button> */}
        </Stack>
      </Stack>

      <Table {...getTableProps()}>
        <TableHead sx={{ borderTopWidth: 2 }}>
          {headerGroups.map((headerGroup) => (
            <TableRow {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column: HeaderGroup) => (
                <TableCell
                  {...column.getHeaderProps([{ className: column.className }])}
                  >
                  {column.render('Header')}
                </TableCell>
              ))}
            </TableRow>
          ))}
        </TableHead>
        <TableBody {...getTableBodyProps()}>
          {page.length > 0 ? (
            page.map((row, i) => {
              prepareRow(row);
              return (
                <TableRow {...row.getRowProps()}>
                  {row.cells.map((cell: Cell) => (
                    <TableCell
                      {...cell.getCellProps([{ className: cell.column.className }])}
                      // sx={{ textAlign: cell.column.isNumeric ? 'left' : 'right' }} 
                    >
                      {cell.render('Cell')}
                    </TableCell>
                  ))}
                </TableRow>
              );
            })
          ) : (
            <EmptyTable msg="No Data" colSpan={12} />
          )}
        </TableBody>
      {/* </Table>

      <Table> */}
        <TableRow>
          <TableCell sx={{ p: 2 }} colSpan={12}>
            <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
          </TableCell>
        </TableRow>
      </Table>


    </>
  );
}

// ==============================|| List ||============================== //

const List = () => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const { transPartyList, error, success } = useSelector(state => state.transParty);


  // table
  const [data, setData] = useState<dataProps[]>([])



  // Add more example data as needed


  const columns = useMemo(
    () =>
      [
        {
          Header: 'Party ID',
          accessor: 'partyId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.partyId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },

        {
          Header: 'Person My Client  ',
          accessor: 'personMyClientId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.personMyClientId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },

        {
          Header: 'Account My Client  ',
          accessor: 'accountMyClientId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.accountMyClientId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },

        {
          Header: 'Entity My Client  ',
          accessor: 'entityMyClientId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.entityMyClientId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Fund Type ID (Funds Code)',
          accessor: 'fundTypeId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.fundTypeId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Funds Comment',
          accessor: 'fundsComment',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.fundsComment;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Foreign Currency ID ',
          accessor: 'foreignCurrencyId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.foreignCurrencyId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Country',
          accessor: 'country',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.country;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Country Code ID ',
          accessor: 'countryCodeId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.countryCodeId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Comments',
          accessor: 'comments',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.comments;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },

        {
          Header: 'Status',
          accessor: 'isActive',
          Cell: ({ row }: { row: Row }) => {
            if (row.values.isActive === undefined || row.values.isActive === null || row.values.isActive === '') {
              return <>-</>
            }
            if (typeof row.values.isActive === 'string') {
              return <>{row.values.isActive}</>;
            }
            if (typeof row.values.isActive === 'number') {
              return <>{row.values.isActive}</>;
            }
            if (typeof row.values.isActive === 'boolean') {
              return <>{row.values.isActive ? "ACTIVE" : "IN-ACTIVE"}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          Header: 'Transaction ID',
          accessor: 'ttransaction',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.ttransaction?.transactionId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },



        // {
        //   Header: 'Branch Status',
        //   accessor: 'usRStatusDetail',
        //   Cell: ({ row }: { row: Row }) => {
        //     if (row.values.usRStatusDetail.statusDesc === undefined || row.values.usRStatusDetail.statusDesc === null || row.values.usRStatusDetail.statusDesc === '') {
        //       return <>-</>
        //     }
        //     if (typeof row.values.usRStatusDetail.statusDesc === 'string') {
        //       return <>{row.values.usRStatusDetail.statusDesc}</>;
        //     }
        //     if (typeof row.values.usRStatusDetail.statusDesc === 'number') {
        //       return <>{row.values.usRStatusDetail.statusDesc}</>;
        //     }
        //     // Handle any other data types if necessary
        //     return <>-</>;
        //   }
        // },
        {
          id: "actions",
          Header: 'Actions',
          accessor: 'actions',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            const data: dataProps = row.original
            return (
              <>
                <Stack direction="row" alignItems="center" justifyContent="center" spacing={0}>
                  {/* <Tooltip title="View">
                                        <IconButton
                                            color="secondary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                            }}
                                        >
                                            <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                                        </IconButton>
                                    </Tooltip> */}
                  <Tooltip title="Edit">
                    <IconButton
                      color="primary"
                      onClick={(e: MouseEvent<HTMLButtonElement>) => {
                        e.stopPropagation();
                        handleAddEdit()
                        settransParty({
                          ...data
                        })
                      }}
                    >
                      <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                    </IconButton>
                  </Tooltip>
                  {/* <Tooltip title="Delete">
                    <IconButton
                      color="error"
                      onClick={(e: MouseEvent<HTMLButtonElement>) => {
                        e.stopPropagation();
                        setTransPartyId(data.toId!)
                        setOpenAlert(true)
                      }}
                    >
                      <DeleteTwoTone twoToneColor={theme.palette.error.main} />
                    </IconButton>
                  </Tooltip> */}
                </Stack>
              </>
            )
          }
        }
      ] as Column[],
    []
  );

  //dialog model 
  const [addEdit, setAddEdit] = useState<boolean>(false);
  const [transPartyCode, settransParty] = useState<transPartyProps>();

  const handleAddEdit = () => {
    setAddEdit(!addEdit);
    if (transPartyCode && !addEdit) settransParty(undefined);
  };

  // //alert model
  // const [openAlert, setOpenAlert] = useState(false);
  // const [TransPartyId, settransFromId] = useState<number | null>(null)

  // const handleAlertClose = () => {
  //   setOpenAlert(!openAlert);
  // };
  //================================API CONFIG=============================//

  const [page, setPage] = useState<number>(0);
  const [perPage, setPerPage] = useState<number>(10);
  const [direction, setDirection] = useState<"asc" | "desc">("asc");
  const [sort, setSort] = useState<string>("partyId"); //CHANGE ID

  const tableParams: TableParamsType = {
    page,
    setPage,
    perPage,
    setPerPage,
    direction,
    setDirection,
    sort,
    setSort
  }

  useEffect(() => {
    const listParameters: queryParamsProps = {
      page: page,
      per_page: perPage,
      direction: direction,
      sort: sort,
      search: ''
    };
    dispatch(fetchTransPartysTable(listParameters));
  }, [dispatch, success, page, perPage, direction, sort]);

  useEffect(() => {
    setData(transPartyList?.result! || []);
  }, [transPartyList])

  //  handel error 
  useEffect(() => {
    if (error != null && typeof error === 'object' && 'message' in error) {
      dispatch(
        openSnackbar({
          open: true,
          message: (error as { message: string }).message, // Type assertion
          variant: 'alert',
          alert: {
            color: 'error'
          },
          close: true
        })
      );
      dispatch(toInitialState());
    }
  }, [error]);

  //  handel success
  useEffect(() => {
    if (success != null) {
      dispatch(
        openSnackbar({
          open: true,
          message: success,
          variant: 'alert',
          alert: {
            color: 'success'
          },
          close: true
        })
      );
      dispatch(toInitialState())
    }
  }, [success])

  // if (isLoading) {
  //     return <div>Loading...</div>;
  // }


  return (
    <>
      <MainCard content={false}>
        <ScrollX>
          <ReactTable columns={columns} data={data} handleAddEdit={handleAddEdit} tableParams={tableParams} />
        </ScrollX>
        {/* add / edit submissionCode dialog */}
        <Dialog
          maxWidth="sm"
          TransitionComponent={PopupTransition}
          keepMounted
          fullWidth
          onClose={handleAddEdit}
          open={addEdit}
          sx={{ '& .MuiDialog-paper': { p: 0 }, transition: 'transform 225ms' }}
          aria-describedby="alert-dialog-slide-description"
        >
          <AddEditTransParty transParty={transPartyCode} onCancel={handleAddEdit} />
        </Dialog>
        {/* alert model */}
        {/* {transFromId && <AlertStatusTypeDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={transFromId} />} */}
      </MainCard>
    </>
  );
}

export default List;
