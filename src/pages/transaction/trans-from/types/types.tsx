// import { Dispatch, SetStateAction } from 'react';
import { Dispatch, SetStateAction } from 'react';
import { Column, HeaderGroup } from 'react-table';
import { transFrom } from 'types/trans-from';

export interface dataProps extends transFrom{}


export interface ReactTableProps {
  columns: Column[]
  data: dataProps[]
  getHeaderProps: (column: HeaderGroup) => {};
}

export interface transFromProps  extends transFrom{}

export interface TableParamsType {
  page: number;
  setPage: Dispatch<SetStateAction<number>>;
  perPage: number;
  setPerPage: Dispatch<SetStateAction<number>>;
  direction: "asc" | "desc";
  setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
  sort: string;
  setSort: Dispatch<SetStateAction<string>>;

  

}
