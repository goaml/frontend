import { MouseEvent, useMemo, useState, useEffect } from 'react';

// material-ui
import {
  Dialog,
  IconButton,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
  useTheme,
} from '@mui/material';

// third-party
import { Cell, Column, HeaderGroup, Row, useFilters, useGlobalFilter, usePagination, useTable } from 'react-table';

// project import
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import { EmptyTable, TablePagination } from 'components/third-party/ReactTable';

// utils
import {
  DefaultColumnFilter,
  GlobalFilter,
  renderFilterTypes
} from 'utils/react-table';

// assets
import { EyeTwoTone } from '@ant-design/icons';

// types
import { ReactTableProps, TableParamsType, dataProps, transAddressProps } from './types/types';
import { PopupTransition } from 'components/@extended/Transitions';
import AddEditTransAddress from 'sections/transactions/trans-address/AddEditTransAddress';
import { useDispatch, useSelector } from 'store';
import { queryParamsProps } from 'types/trans-address';
import { fetchTransAddresssTable, toInitialState } from 'store/reducers/trans-address';
import { openSnackbar } from 'store/reducers/snackbar';

// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, handleAddEdit }: ReactTableProps) {
  const filterTypes = useMemo(() => renderFilterTypes, []);
  const defaultColumn = useMemo(() => ({ Filter: DefaultColumnFilter }), []);

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    preGlobalFilteredRows,
    setGlobalFilter,
    globalFilter,
    page,
    gotoPage,
    setPageSize,
    state: { pageIndex, pageSize }
  } = useTable(
    {
      columns,
      data,
      defaultColumn,
      filterTypes,

      initialState: { pageIndex: 0, pageSize: 10 }
    },
    useGlobalFilter,
    useFilters,
    usePagination
  );

  return (
    <>
      <Stack direction="row" spacing={2} justifyContent="space-between" sx={{ padding: 2 }}>
        <GlobalFilter preGlobalFilteredRows={preGlobalFilteredRows} globalFilter={globalFilter} setGlobalFilter={setGlobalFilter} />
        <Stack direction="row" alignItems="center" spacing={1}></Stack>
      </Stack>

      <Table {...getTableProps()}>
        <TableHead sx={{ borderTopWidth: 2 }}>
          {headerGroups.map((headerGroup) => (
            <TableRow {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column: HeaderGroup) => (
                <TableCell {...column.getHeaderProps([{ className: column.className }])}>{column.render('Header')}</TableCell>
              ))}
            </TableRow>
          ))}
        </TableHead>
        <TableBody {...getTableBodyProps()}>
          {page.length > 0 ? (
            page.map((row, i) => {
              prepareRow(row);
              return (
                <TableRow {...row.getRowProps()}>
                  {row.cells.map((cell: Cell) => (
                    <TableCell
                      {...cell.getCellProps([{ className: cell.column.className }])}
                      sx={{ textAlign: cell.column.isNumeric ? 'right' : 'left' }}
                    >
                      {cell.render('Cell')}
                    </TableCell>
                  ))}
                </TableRow>
              );
            })
          ) : (
            <EmptyTable msg="No Data" colSpan={12} />
          )}
          <TableRow>
            <TableCell sx={{ p: 2 }} colSpan={12}>
              <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </>
  );
}

// ==============================|| List ||============================== //

const List = () => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const { transAddressList, error, success } = useSelector(state => state.transAddress);

  // table
  const [data, setData] = useState<dataProps[]>([]);

  const columns = useMemo(
    () =>
      [
        {
          Header: '#',
          accessor: 'id',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            if (row.id === undefined || row.id === null || row.id === '') {
              return <>-</>
            }
            if (typeof row.id === 'string') {
              return <>{(parseInt(row.id) + 1).toString()}</>;
            }
            if (typeof row.id === 'number') {
              return <>{row.id + 1}</>;
            }
            return <>-</>;
          }
        },
        {
          Header: 'Address',
          accessor: 'address',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.address;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          },
        },
        {
          Header: 'Town',
          accessor: 'town',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.town;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          },
        },
        {
          Header: 'City ID',
          accessor: 'cityId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.cityId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          },
          isNumeric: true
        },
        {
          Header: 'ZIP',
          accessor: 'zip',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.zip;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          },
          isNumeric: true
        },
        {
          Header: 'Country Code ID',
          accessor: 'countryCodeId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.countryCodeId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          },
          isNumeric: true
        },
        {
          Header: 'State',
          accessor: 'state',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.state;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          },
        },
        {
          Header: 'Comments',
          accessor: 'comments',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.comments;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          },
        },
        {
          Header: 'Status',
          accessor: 'isActive',
          Cell: ({ row }: { row: Row }) => {
            if (row.values.isActive === undefined || row.values.isActive === null || row.values.isActive === '') {
              return <>-</>
            }
            if (typeof row.values.isActive === 'string') {
              return <>{row.values.isActive}</>;
            }
            if (typeof row.values.isActive === 'number') {
              return <>{row.values.isActive}</>;
            }
            if (typeof row.values.isActive === 'boolean') {
              return <>{row.values.isActive ? "ACTIVE" : "IN-ACTIVE"}</>;
            }
            return <>-</>;
          }
        },
        {
          id: "actions",
          Header: 'Actions',
          accessor: 'actions',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            const data: dataProps = row.original;
            return (
              <>
                <Stack direction="row" alignItems="center" justifyContent="center" spacing={0}>
                  <Tooltip title="Edit">
                    <IconButton
                      color="primary"
                      onClick={(e: MouseEvent<HTMLButtonElement>) => {
                        e.stopPropagation();
                        handleAddEdit();
                        settransAddressType({
                          ...data
                        });
                      }}
                    >
                      <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                    </IconButton>
                  </Tooltip>
                </Stack>
              </>
            );
          }
        }
      ] as Column[],
    []
  );

  //dialog model
  const [addEdit, setAddEdit] = useState<boolean>(false);
  const [transAddressTypeCode, settransAddressType] = useState<transAddressProps>();

  const handleAddEdit = () => {
    setAddEdit(!addEdit);
    if (transAddressTypeCode && !addEdit) settransAddressType(undefined);
  };

  const [page, setPage] = useState<number>(0);
  const [perPage, setPerPage] = useState<number>(1000);
  const [direction, setDirection] = useState<"asc" | "desc">("asc");
  const [sort, setSort] = useState<string>("addressId");

  const tableParams: TableParamsType = {
    page,
    setPage,
    perPage,
    setPerPage,
    direction,
    setDirection,
    sort,
    setSort
  };

  useEffect(() => {
    const listParameters: queryParamsProps = {
      page: page,
      per_page: perPage,
      direction: direction,
      sort: sort,
      search: ''
    };
    dispatch(fetchTransAddresssTable(listParameters));
  }, [dispatch, success, page, perPage, direction, sort]);

  useEffect(() => {
    setData(transAddressList?.result! || []);
  }, [transAddressList]);

  useEffect(() => {
    if (error != null && typeof error === 'object' && 'message' in error) {
      dispatch(
        openSnackbar({
          open: true,
          message: (error as { message: string }).message,
          variant: 'alert',
          alert: {
            color: 'error'
          },
          close: true
        })
      );
      dispatch(toInitialState());
    }
  }, [error]);

  useEffect(() => {
    if (success != null) {
      dispatch(
        openSnackbar({
          open: true,
          message: success,
          variant: 'alert',
          alert: {
            color: 'success'
          },
          close: true
        })
      );
      dispatch(toInitialState());
    }
  }, [success]);

  return (
    <>
      <MainCard content={false}>
        <ScrollX>
          <ReactTable columns={columns} data={data} handleAddEdit={handleAddEdit} tableParams={tableParams} />
        </ScrollX>
        <Dialog
          maxWidth="sm"
          TransitionComponent={PopupTransition}
          keepMounted
          fullWidth
          onClose={handleAddEdit}
          open={addEdit}
          sx={{ '& .MuiDialog-paper': { p: 0 }, transition: 'transform 225ms' }}
          aria-describedby="alert-dialog-slide-description"
        >
          <AddEditTransAddress transAddress={transAddressTypeCode} onCancel={handleAddEdit} />
        </Dialog>
      </MainCard>
    </>
  );
};

export default List;
