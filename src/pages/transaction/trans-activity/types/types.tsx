// import { Dispatch, SetStateAction } from 'react';
import { Column, HeaderGroup } from 'react-table';

export interface dataProps {
  activityId?: number | undefined;
  reportParties?: string | undefined;
  reportParty?: string | undefined;
  accountMyClientId?: number | undefined;
  personMyClientId?: number | undefined;
  entityMyClientId?: number | undefined;
  significance?: number | undefined;
  reason?: string | undefined;
  comments?: string | undefined;
  statusId?: number | undefined;
  isActive?: boolean | undefined;
  reportId?: number | undefined;
}


export interface ReactTableProps {
  columns: Column[]
  data: dataProps[]
  getHeaderProps: (column: HeaderGroup) => {};
}

export interface transActivityProps  { 
  activityId?: number | undefined;
  reportParties?: string | undefined;
  reportParty?: string | undefined;
  accountMyClientId?: number | undefined;
  personMyClientId?: number | undefined;
  entityMyClientId?: number | undefined;
  significance?: number | undefined;
  reason?: string | undefined;
  comments?: string | undefined;
  statusId?: number | undefined;
  isActive?: boolean | undefined;
  reportId?: number | undefined;
}

// export interface TableParamsType {
//   page: number;
//   setPage: Dispatch<SetStateAction<number>>;
//   perPage: number;
//   setPerPage: Dispatch<SetStateAction<number>>;
//   direction: "asc" | "desc";
//   setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
//   sort: string;
//   setSort: Dispatch<SetStateAction<string>>;

// }
