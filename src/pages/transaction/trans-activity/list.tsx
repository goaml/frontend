import { Fragment, MouseEvent, useMemo, useState, useEffect } from 'react';

// material-ui
import {
  //Button,
  alpha,
  Dialog,
  IconButton,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
  useMediaQuery,
} from '@mui/material';
import { useTheme } from '@mui/material/styles';

// third-party
import { Cell, Column, HeaderGroup, Row, useFilters, useGlobalFilter, usePagination,useSortBy, useTable } from 'react-table';

// project import
// import { PopupTransition } from 'components/@extended/Transitions';
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import {  HeaderSort, TablePagination } from 'components/third-party/ReactTable';

// utils
import {
  GlobalFilter,
  renderFilterTypes
} from 'utils/react-table';

// sections

// assets
import {   EyeTwoTone } from '@ant-design/icons';

//types 
// import { useDispatch, useSelector } from 'store';
// import { fetchBranchCodes, toInitialState } from 'store/reducers/branch-para';
// import { openSnackbar } from 'store/reducers/snackbar';
// import { listParametersType } from 'types/branch-para';
import { ReactTableProps, dataProps, transActivityProps } from './types/types';
import { PopupTransition } from 'components/@extended/Transitions';
import AddEditTransActivity from 'sections/transactions/trans-activity/AddEditTransActivity';
// import AlertStatusTypeDelete from 'sections/parameter-management/status-code/DeleteStatusType';


// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, getHeaderProps }: ReactTableProps) {
  const theme = useTheme();

  const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));
  const sortBy = { id: 'id', desc: false };

  const filterTypes = useMemo(() => renderFilterTypes, []);
  
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    page,
    gotoPage,
    setPageSize,
    state: { globalFilter, pageIndex, pageSize }
  } = useTable(
    {
      columns,
      data,
      filterTypes,

      initialState: { pageIndex: 0, pageSize: 10, hiddenColumns: ['avatar'], sortBy: [sortBy] }
    },
    useGlobalFilter,
    useFilters,
    useSortBy,
    usePagination
  );

  return (
    <>
      <Stack spacing={3} >
      <Stack
          direction={matchDownSM ? 'column' : 'row'}
          spacing={1}
          justifyContent="space-between"
          alignItems="center"
          sx={{ p: 3, pb: 0 }}
        >
          <GlobalFilter
            preGlobalFilteredRows={rows}
            globalFilter={globalFilter}
            setGlobalFilter={globalFilter}
            size="small"
          />
        </Stack>
      </Stack>

      <Stack>
        <Table {...getTableProps()}>
        <TableHead>
          {headerGroups.map((headerGroup: HeaderGroup<{}>) => (
            <TableRow {...headerGroup.getHeaderGroupProps()} sx={{ '& > th:first-of-type': { width: '58px' } }}>
            {headerGroup.headers.map((column: HeaderGroup) => (
              <TableCell {...column.getHeaderProps([{ className: column.className }, getHeaderProps(column)])}
              // sx={{ textAlign: column.isNumeric ? 'right' : 'left' }}
              >
              <HeaderSort column={column} />
            </TableCell>
            ))}
          </TableRow>
          ))}
        </TableHead>
        <TableBody {...getTableBodyProps()}>
          {page.map((row: Row, i: number) => {
              prepareRow(row);
              return (
                <Fragment key={i}>
                  <TableRow
                    {...row.getRowProps()}
                    onClick={() => {
                      row.toggleRowSelected();
                    }}
                    sx={{ cursor: 'pointer', bgcolor: row.isSelected ? alpha(theme.palette.primary.lighter, 0.35) : 'inherit' }}
                  >
                    {row.cells.map((cell: Cell) => (
                      <TableCell {...cell.getCellProps([{ className: cell.column.className }])}
                      sx={{ textAlign: cell.column.isNumeric ? 'left' : 'right' }}
                      >{cell.render('Cell')}</TableCell>
                    ))}
                  </TableRow>
                </Fragment> 
              );
            }
          )}
          <TableRow>
            <TableCell sx={{ p: 2 }} colSpan={12}>
              <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
      </Stack>
      
    </>
  );
}

// ==============================|| List ||============================== //

const List = () => {
  const theme = useTheme();
  // const dispatch = useDispatch();
  // const { branchCodeList, error, success } = useSelector(state => state.branchPara);


  // table
  const [data, setData] = useState<dataProps[]>([])

  const exampleData: dataProps[] = [
    {
      activityId: 1,
      reportParties: "Party A, Party B",
      reportParty: "Party A",
      accountMyClientId: 1001,
      personMyClientId: 2002,
      entityMyClientId: 3003,
      significance: 5,
      reason: "Suspicious activity detected",
      comments: "Transaction details need further investigation",
      statusId: 2,
      isActive: true,
      reportId: 5001,
  },
  {
    activityId: 2,
    reportParties: "Party C, Party D",
    reportParty: "Party D",
    accountMyClientId: 1500,
    personMyClientId: 2500,
    entityMyClientId: 3500,
    significance: 3,
    reason: "Large cash deposit without explanation",
    comments: "Customer provided insufficient information",
    statusId: 1,
    isActive: false,
    reportId: 6002,
  }
    // Add more example data as needed
  ];
  
  // Set example data to state
  useEffect(() => {
    // Set example data when the component mounts
    setData(exampleData);
  }, []);

  const columns = useMemo(
    () =>
      [
        {
          Header: '#',
          accessor: 'id',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            if (row.id === undefined || row.id === null || row.id === '') {
              return <>-</>
            }
            if (typeof row.id === 'string') {
              return <>{(parseInt(row.id) + 1).toString()}</>;
            }
            if (typeof row.id === 'number') {
              return <>{row.id + 1}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          Header: 'Activity ID',
          accessor: 'activityId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.activityId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Report Parties',
          accessor: 'reportParties',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.reportParties;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Report Party',
          accessor: 'reportParty',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.reportParty;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Account My Client ID',
          accessor: 'accountMyClientId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.accountMyClientId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Person My Client ID',
          accessor: 'personMyClientId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.personMyClientId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Entity My Client ID',
          accessor: 'entityMyClientId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.entityMyClientId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Significance',
          accessor: 'significance',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.significance;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Reason',
          accessor: 'reason',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.reason;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Comments',
          accessor: 'comments',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.comments;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },

        {
          Header: 'Status',
          accessor: 'isActive',
          Cell: ({ row }: { row: Row }) => {
            if (row.values.isActive === undefined || row.values.isActive === null || row.values.isActive === '') {
                return <>-</>
            }
            if (typeof row.values.isActive === 'string') {
                return <>{row.values.isActive}</>;
            }
            if (typeof row.values.isActive === 'number') {
                return <>{row.values.isActive}</>;
            }
            if (typeof row.values.isActive === 'boolean') {
                return <>{row.values.isActive ? "ACTIVE" : "IN-ACTIVE"}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
        }
        },
        {
          Header: 'Report ID',
          accessor: 'reportId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.reportId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        


          
        // {
        //   Header: 'Branch Status',
        //   accessor: 'usRStatusDetail',
        //   Cell: ({ row }: { row: Row }) => {
        //     if (row.values.usRStatusDetail.statusDesc === undefined || row.values.usRStatusDetail.statusDesc === null || row.values.usRStatusDetail.statusDesc === '') {
        //       return <>-</>
        //     }
        //     if (typeof row.values.usRStatusDetail.statusDesc === 'string') {
        //       return <>{row.values.usRStatusDetail.statusDesc}</>;
        //     }
        //     if (typeof row.values.usRStatusDetail.statusDesc === 'number') {
        //       return <>{row.values.usRStatusDetail.statusDesc}</>;
        //     }
        //     // Handle any other data types if necessary
        //     return <>-</>;
        //   }
        // },
        {
          id: "actions",
          Header: 'Actions',
          accessor: 'actions',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            const data: dataProps = row.original
            return (
              <>
                <Stack direction="row" alignItems="center" justifyContent="center" spacing={0}>
                  {/* <Tooltip title="View">
                                        <IconButton
                                            color="secondary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                            }}
                                        >
                                            <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                                        </IconButton>
                                    </Tooltip> */}
                  <Tooltip title="Edit">
                    <IconButton
                      color="primary"
                      onClick={(e: MouseEvent<HTMLButtonElement>) => {
                        e.stopPropagation();
                        handleAddEdit()
                        settransActivityType({
                          ...data
                        })
                      }}
                    >
                      <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                    </IconButton>
                  </Tooltip>
                  {/* <Tooltip title="Delete">
                    <IconButton
                      color="error"
                      onClick={(e: MouseEvent<HTMLButtonElement>) => {
                        e.stopPropagation();
                        settransActivityId(data.toId!)
                        setOpenAlert(true)
                      }}
                    >
                      <DeleteTwoTone twoToneColor={theme.palette.error.main} />
                    </IconButton>
                  </Tooltip> */}
                </Stack>
              </>
            )
          }
        }
      ] as Column[],
    []
  );

  //dialog model 
  const [addEdit, setAddEdit] = useState<boolean>(false);
  const [transActivityTypeCode, settransActivityType] = useState<transActivityProps>();

  const handleAddEdit = () => {
    setAddEdit(!addEdit);
    if (transActivityTypeCode && !addEdit) settransActivityType(undefined);
  };

  // //alert model
  // const [openAlert, setOpenAlert] = useState(false);
  // const [transActivityId, settransFromId] = useState<number | null>(null)

  // const handleAlertClose = () => {
  //   setOpenAlert(!openAlert);
  // };
  // //================================API CONFIG=============================//

  // const [page, setPage] = useState<number>(0);
  // const [perPage, setPerPage] = useState<number>(10);
  // const [direction, setDirection] = useState<"asc" | "desc">("asc");
  // const [sort, setSort] = useState<string>("transFromTypeId"); //CHANGE ID

  // const tableParams: TableParamsType = {
  //   page,
  //   setPage,
  //   perPage,
  //   setPerPage,
  //   direction,
  //   setDirection,
  //   sort,
  //   setSort
  // }

  // useEffect(() => {
  //   const listParameters: listParametersType = {
  //     page: page,
  //     per_page: perPage,
  //     direction: direction,
  //     sort: sort
  //   };
  //   dispatch(fetchBranchCodes(listParameters));
  // }, [dispatch, success, page, perPage, direction, sort]);

  // useEffect(() => {
  //   setData(branchCodeList?.result! || []);
  // }, [branchCodeList])

  // //  handel error 
  // useEffect(() => {
  //   if (error != null && typeof error === 'object' && 'message' in error) {
  //     dispatch(
  //       openSnackbar({
  //         open: true,
  //         message: (error as { message: string }).message, // Type assertion
  //         variant: 'alert',
  //         alert: {
  //           color: 'error'
  //         },
  //         close: true
  //       })
  //     );
  //     dispatch(toInitialState());
  //   }
  // }, [error]);

  // //  handel success
  // useEffect(() => {
  //   if (success != null) {
  //     dispatch(
  //       openSnackbar({
  //         open: true,
  //         message: success,
  //         variant: 'alert',
  //         alert: {
  //           color: 'success'
  //         },
  //         close: true
  //       })
  //     );
  //     dispatch(toInitialState())
  //   }
  // }, [success])

  // if (isLoading) {
  //     return <div>Loading...</div>;
  // }


  return (
    <>
      <MainCard content={false}>
        <ScrollX>
          <ReactTable columns={columns} data={data} getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()}  />
        </ScrollX>
        {/* add / edit submissionCode dialog */}
        <Dialog
          maxWidth="sm"
          TransitionComponent={PopupTransition}
          keepMounted
          fullWidth
          onClose={handleAddEdit}
          open={addEdit}
          sx={{ '& .MuiDialog-paper': { p: 0 }, transition: 'transform 225ms' }}
          aria-describedby="alert-dialog-slide-description"
        >
          <AddEditTransActivity transActivity={transActivityTypeCode} onCancel={handleAddEdit} />
        </Dialog>
        {/* alert model */}
        {/* {transFromId && <AlertStatusTypeDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={transFromId} />} */}
      </MainCard>
    </>
  );
}

export default List;
