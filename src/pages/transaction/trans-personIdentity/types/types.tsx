// import { Dispatch, SetStateAction } from 'react';
import { Dispatch, SetStateAction } from 'react';
import { Column, HeaderGroup } from 'react-table';
import { transPersonIdentification } from 'types/trans-person-identification';

export interface dataProps extends transPersonIdentification{}


export interface ReactTableProps {
  columns: Column[]
  data: dataProps[]
  getHeaderProps: (column: HeaderGroup) => {};
}

export interface transPersonIdProps  extends transPersonIdentification{}

export interface TableParamsType {
  page: number;
  setPage: Dispatch<SetStateAction<number>>;
  perPage: number;
  setPerPage: Dispatch<SetStateAction<number>>;
  direction: "asc" | "desc";
  setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
  sort: string;
  setSort: Dispatch<SetStateAction<string>>;

}
