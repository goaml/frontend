import { Fragment, MouseEvent, useMemo, useState, useEffect } from 'react';

// material-ui
import {
  alpha,
  Box,
  Button,
  //Button,
  Dialog,
  Grid,
  //Dialog,
  IconButton,
  LinearProgress,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
  Typography,
  useMediaQuery,
  useTheme,
} from '@mui/material';

// third-party
import { Cell, Column, HeaderGroup, Row, useFilters, useGlobalFilter, usePagination,useSortBy, useTable } from 'react-table';

// project import
// import { PopupTransition } from 'components/@extended/Transitions';
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import {  HeaderSort, TablePagination } from 'components/third-party/ReactTable';

// utils
import {
  GlobalFilter,
  renderFilterTypes
} from 'utils/react-table';

// sections

// assets
import { ArrowLeftOutlined, EyeTwoTone } from '@ant-design/icons';

//types 
import { ReactTableProps, dataProps, transToProps } from './types/types';
import { PopupTransition } from 'components/@extended/Transitions';
import AddEditTransTo from 'sections/transactions/trans-to/AddEditTransTo';
import { useLocation, useNavigate } from 'react-router';
import { useDispatch, useSelector } from 'store';
import { queryParamsProps } from 'types/trans-to';
import { fetchTransTosTable, toInitialState } from 'store/reducers/trans-to';
import { openSnackbar } from 'store/reducers/snackbar';


// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, getHeaderProps }: ReactTableProps) {
  const theme = useTheme();
  const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));

  const filterTypes = useMemo(() => renderFilterTypes, []);

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    preGlobalFilteredRows,
    setGlobalFilter,
    page,
    gotoPage,
    setPageSize,
    state: { globalFilter, pageIndex, pageSize }
  } = useTable(
    {
      columns,
      data,
      filterTypes,

      initialState: { pageIndex: 0, pageSize: 10 }
    },
    useGlobalFilter,
    useFilters,
    useSortBy,
    usePagination
  );

  return (
    <>
      <Stack spacing={2}> 
        <Stack
          direction={matchDownSM ? 'column' : 'row'}
          spacing={1}
          justifyContent="space-between"
          alignItems="center"
          sx={{ p: 3, pb: 0 }}
        >
          <GlobalFilter
          preGlobalFilteredRows={preGlobalFilteredRows}
          globalFilter={globalFilter}
          setGlobalFilter={setGlobalFilter}
          size="small"
        />
        </Stack>
      </Stack>

      <Stack>
      <Table {...getTableProps()}>
      <TableHead>
          {headerGroups.map((headerGroup: HeaderGroup<{}>) => (
            <TableRow {...headerGroup.getHeaderGroupProps()} sx={{ '& > th:first-of-type': { width: '58px' } }}>
              {headerGroup.headers.map((column: HeaderGroup) => (
                <TableCell {...column.getHeaderProps([{ className: column.className }, getHeaderProps(column)])}
                >
                <HeaderSort column={column} />
              </TableCell>
              ))}
            </TableRow>
          ))}
        </TableHead>
        <TableBody {...getTableBodyProps()}>
          {page.map((row: Row, i: number) => {
              prepareRow(row);
              return (
                <Fragment key={i}>
                  <TableRow
                    {...row.getRowProps()}
                    sx={{ cursor: 'pointer', bgcolor: row.isSelected ? alpha(theme.palette.primary.lighter, 0.35) : 'inherit' }}
                  >
                    {row.cells.map((cell: Cell) => (
                      <TableCell {...cell.getCellProps([{ className: cell.column.className }])}
                      sx={{ textAlign: cell.column.isNumeric ? 'left' : 'right' }}
                      >{cell.render('Cell')}</TableCell>
                    ))}
                  </TableRow>
                </Fragment> 
              );
            }
          )}
          <TableRow>
            <TableCell sx={{ p: 2 }} colSpan={12}>
              <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
      </Stack>
    </>
  );
}

// ==============================|| List ||============================== //

const List = () => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const { transToList, error, success ,isLoading} = useSelector(state => state.transTo);
  const navigate = useNavigate();

  const location = useLocation();
  
  // Extract trxId from state
  const trxId = location.state?.trxId;
  // table
  const [data, setData] = useState<dataProps[]>([])




  const columns = useMemo(
    () =>
      [
        {
          Header: '#',
          accessor: 'id',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            if (row.id === undefined || row.id === null || row.id === '') {
              return <>-</>
            }
            if (typeof row.id === 'string') {
              return <>{(parseInt(row.id) + 1).toString()}</>;
            }
            if (typeof row.id === 'number') {
              return <>{row.id + 1}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          Header: 'Transaction ID',
          accessor: 'transactionId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.transactionId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'To Funds Code',
          accessor: 'rfundsType',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.rfundsType.fundTypeCode;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },

        {
          Header: 'To Foreign Currency',
          accessor: 'toForeignCurrencyId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.toForeignCurrencyId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'T Conductor',
          accessor: 'conductorPersonId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.conductorPersonId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'To Account',
          accessor: 'toAccountId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.toAccountId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'To Person',
          accessor: 'toPersonId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.toPersonId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'To Entity',
          accessor: 'toEntityId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.toEntityId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'To Country',
          accessor: 'rcountryCodes',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.rcountryCodes.description;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'To Funds Comment',
          accessor: 'toFundsComment',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.toFundsComment;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },



        // {
        //   Header: 'Branch Status',
        //   accessor: 'usRStatusDetail',
        //   Cell: ({ row }: { row: Row }) => {
        //     if (row.values.usRStatusDetail.statusDesc === undefined || row.values.usRStatusDetail.statusDesc === null || row.values.usRStatusDetail.statusDesc === '') {
        //       return <>-</>
        //     }
        //     if (typeof row.values.usRStatusDetail.statusDesc === 'string') {
        //       return <>{row.values.usRStatusDetail.statusDesc}</>;
        //     }
        //     if (typeof row.values.usRStatusDetail.statusDesc === 'number') {
        //       return <>{row.values.usRStatusDetail.statusDesc}</>;
        //     }
        //     // Handle any other data types if necessary
        //     return <>-</>;
        //   }
        // },
        {
          id: "actions",
          Header: 'Actions',
          accessor: 'actions',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            const data: dataProps = row.original
            return (
              <>
                <Stack direction="row" alignItems="center" justifyContent="center" spacing={0}>
                  {/* <Tooltip title="View">
                                        <IconButton
                                            color="secondary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                            }}
                                        >
                                            <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                                        </IconButton>
                                    </Tooltip> */}
                  <Tooltip title="Edit">
                    <IconButton
                      color="primary"
                      onClick={(e: MouseEvent<HTMLButtonElement>) => {
                        e.stopPropagation();
                        handleAddEdit()
                        settranstoType({
                          ...data
                        })
                      }}
                    >
                      <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                    </IconButton>
                  </Tooltip>
                  <Stack spacing={1}>
                  <Grid container spacing={1} width={"300px"}>
                    <Grid item xs={3}>
                      <Tooltip title="Transactions From">
                        <Button
                          color="primary"
                          sx={{ width: '100%', height: '30px', maxWidth: '120px' }}
                          variant="contained"
                          onClick={() => {
                            navigate('/transaction-management/tp-myclient', { state: { MyClientId: data.toPersonId } });
                          }}
                        >
                          Person
                        </Button>
                      </Tooltip>
                    </Grid>
                    <Grid item xs={3}>
                      <Tooltip title="Transactions From Client">
                        <Button
                          color="primary"
                          sx={{ width: '100%', height: '30px', maxWidth: '120px' }}
                          variant="contained"
                          onClick={() => {
                            navigate('/transaction-management/te-myClient', { state: { MyClientEntity: data.toEntityId } });
                          }}
                        >
                          Entity
                        </Button>
                      </Tooltip>
                    </Grid>
                    <Grid item xs={3}>
                      <Tooltip title="Transactions To">
                        <Button
                          color="primary"
                          sx={{ width: '100%', height: '30px', maxWidth: '120px' }}
                          variant="contained"
                          onClick={() => {
                            navigate('/transaction-management/ta-myclient', { state: { MyClientAccId: data.toAccountId } });
                          }}
                        >
                          Account
                        </Button>
                      </Tooltip>
                    </Grid>
                    </Grid>
                    </Stack>
                  {/* <Tooltip title="Delete">
                    <IconButton
                      color="error"
                      onClick={(e: MouseEvent<HTMLButtonElement>) => {
                        e.stopPropagation();
                        settranstoId(data.toId!)
                        setOpenAlert(true)
                      }}
                    >
                      <DeleteTwoTone twoToneColor={theme.palette.error.main} />
                    </IconButton>
                  </Tooltip> */}
                </Stack>
              </>
            )
          }
        }
      ] as Column[],
    []
  );

  //dialog model 
  const [addEdit, setAddEdit] = useState<boolean>(false);
  const [transtoTypeCode, settranstoType] = useState<transToProps>();

  const handleAddEdit = () => {
    setAddEdit(!addEdit);
    if (transtoTypeCode && !addEdit) settranstoType(undefined);
  };

  // //alert model
  // const [openAlert, setOpenAlert] = useState(false);
  // const [transtoId, settransFromId] = useState<number | null>(null)

  // const handleAlertClose = () => {
  //   setOpenAlert(!openAlert);
  // };
  // //================================API CONFIG=============================//

  const [page] = useState<number>(0);
  const [perPage] = useState<number>(10);
  const [direction] = useState<"asc" | "desc">("asc");
  const [sort] = useState<string>("toId"); //CHANGE ID


  useEffect(() => {
    const listParameters: queryParamsProps = {
      page: page,
      per_page: perPage,
      direction: direction,
      sort: sort,
      search:'',
      trxId: trxId
    };
    dispatch(fetchTransTosTable(listParameters));
  }, [dispatch, success, page, perPage, direction, sort]);

  useEffect(() => {
    if (transToList?.result) {
      // const filteredData = trxId ? transToList.result.filter((item) => item.transactionId === trxId) : transToList.result;
      setData(transToList?.result);
    }
  }, [transToList, trxId]);


  //  handel error 
  useEffect(() => {
    if (error != null && typeof error === 'object' && 'message' in error) {
      dispatch(
        openSnackbar({
          open: true,
          message: (error as { message: string }).message, // Type assertion
          variant: 'alert',
          alert: {
            color: 'error'
          },
          close: true
        })
      );
      dispatch(toInitialState());
    }
  }, [error]);

  //  handel success
  useEffect(() => {
    if (success != null) {
      dispatch(
        openSnackbar({
          open: true,
          message: success,
          variant: 'alert',
          alert: {
            color: 'success'
          },
          close: true
        })
      );
      dispatch(toInitialState())
    }
  }, [success])

  if (isLoading) {
    return (
      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
        <Box sx={{ width: '50%' }}>
          <LinearProgress />
        </Box>
      </div>
    );
  }

  return (
    <>
      <MainCard content={false}title={
        <Stack direction="row" alignItems="center" spacing={1}>
          <IconButton onClick={() => {
            navigate(-1)

          }}>
            <ArrowLeftOutlined />
          </IconButton>
          <Typography sx={{ fontWeight: 'bold', fontSize: '1.5rem' }}>
            Transactions - To
          </Typography>
        </Stack>
      }
      >
        <ScrollX>
          <ReactTable columns={columns} data={data} getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()} />
        </ScrollX>
        {/* add / edit submissionCode dialog */}
        <Dialog
          maxWidth="sm"
          TransitionComponent={PopupTransition}
          keepMounted
          fullWidth
          onClose={handleAddEdit}
          open={addEdit}
          sx={{ '& .MuiDialog-paper': { p: 0 }, transition: 'transform 225ms' }}
          aria-describedby="alert-dialog-slide-description"
        >
          <AddEditTransTo transTo={transtoTypeCode} onCancel={handleAddEdit} />
        </Dialog>
        {/* alert model */}
        {/* {transFromId && <AlertStatusTypeDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={transFromId} />} */}
      </MainCard>
    </>
  );
}

export default List;
