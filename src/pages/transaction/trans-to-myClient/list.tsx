import { MouseEvent, useMemo, useState, useEffect } from 'react';

// material-ui
import {
  Box,
  Button,
  //Button,
  Dialog,
  Grid,
  //Dialog,
  IconButton,
  LinearProgress,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
  Typography,
  useTheme,
} from '@mui/material';

// third-party
import { Cell, Column, HeaderGroup, Row, useFilters, useGlobalFilter, usePagination, useTable } from 'react-table';

// project import
// import { PopupTransition } from 'components/@extended/Transitions';
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import {  EmptyTable, TablePagination } from 'components/third-party/ReactTable';

// utils
import {
  DefaultColumnFilter,
  GlobalFilter,
  renderFilterTypes
} from 'utils/react-table';

// sections

// assets
import { ArrowLeftOutlined, EyeTwoTone } from '@ant-design/icons';

//types 
// import { useDispatch, useSelector } from 'store';
// import { fetchBranchCodes, toInitialState } from 'store/reducers/branch-para';
// import { openSnackbar } from 'store/reducers/snackbar';
// import { listParametersType } from 'types/branch-para';
import { ReactTableProps, TableParamsType, dataProps, transToMyClientProps } from './types/types';
import { PopupTransition } from 'components/@extended/Transitions';
import AddEditTransToMyClient from 'sections/transactions/trans-to-myClient/AddEditTransToMyClient';
import { useLocation, useNavigate } from 'react-router';
import { useDispatch, useSelector } from 'store';
import { queryParamsProps } from 'types/trans-to-myclient';
import { fetchTransToMyClientsTable, toInitialState } from 'store/reducers/trans-to-myclient';
import { openSnackbar } from 'store/reducers/snackbar';
// import AlertStatusTypeDelete from 'sections/parameter-management/status-code/DeleteStatusType';


// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, handleAddEdit }: ReactTableProps) {
  const filterTypes = useMemo(() => renderFilterTypes, []);
  const defaultColumn = useMemo(() => ({ Filter: DefaultColumnFilter }), []);

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    preGlobalFilteredRows,
    setGlobalFilter,
    globalFilter,
    page,
    gotoPage,
    setPageSize,
    state: { pageIndex, pageSize }
  } = useTable(
    {
      columns,
      data,
      defaultColumn,
      filterTypes,

      initialState: { pageIndex: 0, pageSize: 10 }
    },
    useGlobalFilter,
    useFilters,
    usePagination
  );

  return (
    <>
      <Stack direction="row" spacing={2} justifyContent="space-between" sx={{ padding: 2 }}>
        <GlobalFilter preGlobalFilteredRows={preGlobalFilteredRows} globalFilter={globalFilter} setGlobalFilter={setGlobalFilter} />
        <Stack direction="row" alignItems="center" spacing={1}>
          {/* <CSVExport data={rows.map((d: Row) => d.original)} filename={'filtering-table.csv'} /> */}
          {/* <Button variant="contained" startIcon={<PlusOutlined />} onClick={handleAddEdit}>
            Add New
          </Button> */}
        </Stack>
      </Stack>

      <Table {...getTableProps()}>
        <TableHead sx={{ borderTopWidth: 2 }}>
          {headerGroups.map((headerGroup) => (
            <TableRow {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column: HeaderGroup) => (
                <TableCell {...column.getHeaderProps([{ className: column.className }])}>{column.render('Header')}</TableCell>
              ))}
            </TableRow>
          ))}
        </TableHead>
        <TableBody {...getTableBodyProps()}>
          {page.length > 0 ? (
            page.map((row, i) => {
              prepareRow(row);
              return (
                <TableRow {...row.getRowProps()}>
                  {row.cells.map((cell: Cell) => (
                    <TableCell {...cell.getCellProps([{ className: cell.column.className }])}>{cell.render('Cell')}</TableCell>
                  ))}
                </TableRow>
              );
            })
          ) : (
            <EmptyTable msg="No Data" colSpan={12} />
          )}
          <TableRow>
            <TableCell sx={{ p: 2 }} colSpan={12}>
              <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </>
  );
}

// ==============================|| List ||============================== //

const List = () => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const { transToMyClientList, error, success, isLoading } = useSelector(state => state.transToMyclient);
  const navigate = useNavigate();
  const location = useLocation();
  
  // Extract trxId from state
  const trxId = location.state?.trxId;

  // table
  const [data, setData] = useState<dataProps[]>([])



  const columns = useMemo(
    () =>
      [
        {
          Header: '#',
          accessor: 'id',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            if (row.id === undefined || row.id === null || row.id === '') {
              return <>-</>
            }
            if (typeof row.id === 'string') {
              return <>{(parseInt(row.id) + 1).toString()}</>;
            }
            if (typeof row.id === 'number') {
              return <>{row.id + 1}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          Header: 'To My Client ID',
          accessor: 'toMyClientId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.toMyClientId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        // {
        //   Header: 'Transaction ID',
        //   accessor: 'transactionId',
        //   Cell: ({ row }: { row: Row }) => {
        //     const value = row.values.transactionId;
        //     if (value === undefined || value === null || value === '') return <>-</>;
        //     return <>{value}</>;
        //   }
        // },
        {
          Header: 'To Funds Code',
          accessor: 'rfundsType',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.rfundsType.fundTypeCode;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },

        {
          Header: 'To Foreign Currency',
          accessor: 'toForeignCurrencyId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.toForeignCurrencyId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'T Conductor',
          accessor: 'conductorPersonMyClientId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.tConductor;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'To Account',
          accessor: 'toAccountMyClientId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.toAccountMyClientId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'To Person',
          accessor: 'toPersonMyClientId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.toPersonMyClientId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'To Entity',
          accessor: 'toEntityMyClientId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.toEntityMyClientId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'To Country',
          accessor: 'rcountryCodes',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.rcountryCodes.description;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'To Funds Comment',
          accessor: 'toFundsComment',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.toFundsComment;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },



        // {
        //   Header: 'Branch Status',
        //   accessor: 'usRStatusDetail',
        //   Cell: ({ row }: { row: Row }) => {
        //     if (row.values.usRStatusDetail.statusDesc === undefined || row.values.usRStatusDetail.statusDesc === null || row.values.usRStatusDetail.statusDesc === '') {
        //       return <>-</>
        //     }
        //     if (typeof row.values.usRStatusDetail.statusDesc === 'string') {
        //       return <>{row.values.usRStatusDetail.statusDesc}</>;
        //     }
        //     if (typeof row.values.usRStatusDetail.statusDesc === 'number') {
        //       return <>{row.values.usRStatusDetail.statusDesc}</>;
        //     }
        //     // Handle any other data types if necessary
        //     return <>-</>;
        //   }
        // },
        {
          id: "actions",
          Header: 'Actions',
          accessor: 'actions',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            const data: dataProps = row.original
            return (
              <>
                <Stack direction="row" alignItems="center" justifyContent="center" spacing={0}>
                  {/* <Tooltip title="View">
                                        <IconButton
                                            color="secondary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                            }}
                                        >
                                            <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                                        </IconButton>
                                    </Tooltip> */}
                  <Tooltip title="Edit">
                    <IconButton
                      color="primary"
                      onClick={(e: MouseEvent<HTMLButtonElement>) => {
                        e.stopPropagation();
                        handleAddEdit()
                        settranstoMyClient({
                          ...data
                        })
                      }}
                    >
                      <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                    </IconButton>
                  </Tooltip>
                  <Stack spacing={1}>
                  <Grid container spacing={1} width={"300px"}>
                    <Grid item xs={3}>
                      <Tooltip title="Transactions From">
                        <Button
                          color="primary"
                          sx={{ width: '100%', height: '30px', maxWidth: '120px' }}
                          variant="contained"
                          onClick={() => {
                            navigate('/transaction-management/tp-myclient', { state: { MyClientId: data.toPersonMyClientId } });
                          }}
                        >
                          Person
                        </Button>
                      </Tooltip>
                    </Grid>
                    <Grid item xs={3}>
                      <Tooltip title="Transactions From Client">
                        <Button
                          color="primary"
                          sx={{ width: '100%', height: '30px', maxWidth: '120px' }}
                          variant="contained"
                          onClick={() => {
                            navigate('/transaction-management/te-myClient', { state: { MyClientEntity: data.toEntityMyClientId } });
                          }}
                        >
                          Entity
                        </Button>
                      </Tooltip>
                    </Grid>
                    <Grid item xs={3}>
                      <Tooltip title="Transactions To">
                        <Button
                          color="primary"
                          sx={{ width: '100%', height: '30px', maxWidth: '120px' }}
                          variant="contained"
                          onClick={() => {
                            navigate('/transaction-management/ta-myclient', { state: { MyClientAccId: data.toAccountMyClientId } });
                          }}
                        >
                          Account
                        </Button>
                      </Tooltip>
                    </Grid>
                    </Grid>
                    </Stack>
                  {/* <Tooltip title="Delete">
                    <IconButton
                      color="error"
                      onClick={(e: MouseEvent<HTMLButtonElement>) => {
                        e.stopPropagation();
                        settranstoId(data.toId!)
                        setOpenAlert(true)
                      }}
                    >
                      <DeleteTwoTone twoToneColor={theme.palette.error.main} />
                    </IconButton>
                  </Tooltip> */}
                </Stack>
              </>
            )
          }
        }
      ] as Column[],
    []
  );

  //dialog model 
  const [addEdit, setAddEdit] = useState<boolean>(false);
  const [transtoMyClientCode, settranstoMyClient] = useState<transToMyClientProps>();

  const handleAddEdit = () => {
    setAddEdit(!addEdit);
    if (transtoMyClientCode && !addEdit) settranstoMyClient(undefined);
  };

  // //alert model
  // const [openAlert, setOpenAlert] = useState(false);
  // const [transtoId, settransFromId] = useState<number | null>(null)

  // const handleAlertClose = () => {
  //   setOpenAlert(!openAlert);
  // };
  // //================================API CONFIG=============================//

  const [page, setPage] = useState<number>(0);
  const [perPage, setPerPage] = useState<number>(10);
  const [direction, setDirection] = useState<"asc" | "desc">("asc");
  const [sort, setSort] = useState<string>("toMyClientId"); //CHANGE ID

  const tableParams: TableParamsType = {
    page,
    setPage,
    perPage,
    setPerPage,
    direction,
    setDirection,
    sort,
    setSort
  }

  useEffect(() => {
    const listParameters: queryParamsProps = {
      page: page,
      per_page: perPage,
      direction: direction,
      sort: sort,
      search:'',
      trxId: trxId
    };
    dispatch(fetchTransToMyClientsTable(listParameters));
  }, [dispatch, success, page, perPage, direction, sort]);

  useEffect(() => {
    if (transToMyClientList?.result) {
      // const filteredData = trxId ? transToMyClientList.result.filter((item) => item.transactionId === trxId) : transToMyClientList.result;
      setData(transToMyClientList?.result);
    }
  }, [transToMyClientList, trxId]);

  //  handel error 
  useEffect(() => {
    if (error != null && typeof error === 'object' && 'message' in error) {
      dispatch(
        openSnackbar({
          open: true,
          message: (error as { message: string }).message, // Type assertion
          variant: 'alert',
          alert: {
            color: 'error'
          },
          close: true
        })
      );
      dispatch(toInitialState());
    }
  }, [error]);

  //  handel success
  useEffect(() => {
    if (success != null) {
      dispatch(
        openSnackbar({
          open: true,
          message: success,
          variant: 'alert',
          alert: {
            color: 'success'
          },
          close: true
        })
      );
      dispatch(toInitialState())
    }
  }, [success])

  if (isLoading) {
    return (
      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
        <Box sx={{ width: '50%' }}>
          <LinearProgress />
        </Box>
      </div>
    );
  }


  return (
    <>
      <MainCard content={false}title={
        <Stack direction="row" alignItems="center" spacing={1}>
          <IconButton onClick={() => {
            navigate(-1)

          }}>
            <ArrowLeftOutlined />
          </IconButton>
          <Typography sx={{ fontWeight: 'bold', fontSize: '1.5rem' }}>
            Transactions - To My Client
          </Typography>
        </Stack>
      }
      >
        <ScrollX>
          <ReactTable columns={columns} data={data} handleAddEdit={handleAddEdit} tableParams={tableParams} />
        </ScrollX>
        {/* add / edit submissionCode dialog */}
        <Dialog
          maxWidth="sm"
          TransitionComponent={PopupTransition}
          keepMounted
          fullWidth
          onClose={handleAddEdit}
          open={addEdit}
          sx={{ '& .MuiDialog-paper': { p: 0 }, transition: 'transform 225ms' }}
          aria-describedby="alert-dialog-slide-description"
        >
          <AddEditTransToMyClient transToMyClient={transtoMyClientCode} onCancel={handleAddEdit} />
        </Dialog>
        {/* alert model */}
        {/* {transFromId && <AlertStatusTypeDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={transFromId} />} */}
      </MainCard>
    </>
  );
}

export default List;
