import { Fragment, useEffect, useMemo, useState } from 'react';

// material-ui
import {
  alpha,
  Box,
  Dialog,
  IconButton,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
  useMediaQuery,
  useTheme,
} from '@mui/material';

// third-party
import { Cell, Column, HeaderGroup, Row, useFilters, useGlobalFilter, usePagination, useSortBy, useTable } from 'react-table';

// project import
import MainCard from 'components/MainCard';
import { HeaderSort, TablePagination } from 'components/third-party/ReactTable';

// utils
import {
  GlobalFilter,
  renderFilterTypes
} from 'utils/react-table';

// sections

// assets
import { EyeTwoTone } from '@ant-design/icons';

//types 
import { PopupTransition } from 'components/@extended/Transitions';
import { useNavigate } from 'react-router';
import ViewTransReport from 'sections/transactions/trans-report/ViewTransReport';
import { useDispatch, useSelector } from 'store';
import { openSnackbar } from 'store/reducers/snackbar';
import { fetchTransReportsTable, toInitialState } from 'store/reducers/trans-report';
import { queryParamsProps } from 'types/trans-report';
import { dataProps, ReactTableProps, transReportProps } from './types/types';

// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, getHeaderProps }: ReactTableProps) {
  const theme = useTheme();
  const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));
  const sortBy = { id: 'id', desc: false };
  const filterTypes = useMemo(() => renderFilterTypes, []);

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    preGlobalFilteredRows,
    setGlobalFilter,
    page,
    gotoPage,
    setPageSize,
    state: { globalFilter, pageIndex, pageSize }
  } = useTable(
    {
      columns,
      data,
      filterTypes,
      initialState: { pageIndex: 0, pageSize: 10, hiddenColumns: ['avatar'], sortBy: [sortBy] }
    },
    useGlobalFilter,
    useFilters,
    useSortBy,
    usePagination
  );

  return (
    <Box sx={{ width: '100%', overflowX: 'auto' }}>
      <Stack spacing={3}>
        <Stack
          direction={matchDownSM ? 'column' : 'row'}
          spacing={2}
          justifyContent="space-between"
          alignItems={matchDownSM ? 'stretch' : 'center'}
          sx={{ p: 3, pb: 0 }}
        >
          <Box sx={{ width: matchDownSM ? '100%' : 'auto' }}>
            <GlobalFilter
              preGlobalFilteredRows={preGlobalFilteredRows}
              globalFilter={globalFilter}
              setGlobalFilter={setGlobalFilter}
              size="small"
            />
          </Box>
        </Stack>

        <Box sx={{ 
          width: '100%',
          overflowX: 'auto',
          '& .MuiTable-root': {
            minWidth: 750,
            borderCollapse: 'separate',
            borderSpacing: '0 8px'
          }
        }}>
          <Table {...getTableProps()}>
            <TableHead>
              {headerGroups.map((headerGroup: HeaderGroup<{}>, index: number) => (
                <TableRow {...headerGroup.getHeaderGroupProps()}>
                  {headerGroup.headers.map((column: HeaderGroup, colIndex: number) => (
                    <TableCell 
                      {...column.getHeaderProps([{ className: column.className }, getHeaderProps(column)])}
                      key={colIndex}
                      sx={{ 
                        whiteSpace: 'nowrap',
                        px: 2,
                        background: theme.palette.background.paper,
                        position: 'sticky',
                        top: 0,
                        zIndex: 1
                      }}
                    >
                      <HeaderSort column={column} />
                    </TableCell>
                  ))}
                </TableRow>
              ))}
            </TableHead>
            <TableBody {...getTableBodyProps()}>
              {page.length === 0 ? (
                <TableRow>
                  <TableCell colSpan={columns.length} align="center">
                    No data
                  </TableCell>
                </TableRow>
              ) : (
                page.map((row: Row, i: number) => {
                  prepareRow(row);
                  return (
                    <Fragment key={i}>
                      <TableRow
                        {...row.getRowProps()}
                        sx={{
                          cursor: 'pointer',
                          bgcolor: row.isSelected ? alpha(theme.palette.primary.lighter, 0.35) : theme.palette.background.paper,
                          '&:hover': {
                            bgcolor: alpha(theme.palette.primary.lighter, 0.2)
                          }
                        }}
                      >
                        {row.cells.map((cell: Cell, cellIndex: number) => (
                          <TableCell 
                            {...cell.getCellProps([{ className: cell.column.className }])}
                            sx={{ 
                              whiteSpace: 'nowrap',
                              overflow: 'hidden',
                              textOverflow: 'ellipsis',
                              maxWidth: '200px',
                              px: 2
                            }}
                          >
                            {cell.render('Cell')}
                          </TableCell>
                        ))}
                      </TableRow>
                    </Fragment>
                  );
                })
              )}
            </TableBody>
          </Table>
        </Box>

        <Box sx={{ p: 2, borderTop: `1px solid ${theme.palette.divider}` }}>
          <TablePagination
            gotoPage={gotoPage}
            rows={rows}
            setPageSize={setPageSize}
            pageIndex={pageIndex}
            pageSize={pageSize}
          />
        </Box>
      </Stack>
    </Box>
  );
}

// ==============================|| List ||============================== //

const List = () => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const { transReportList, error, success, isLoading } = useSelector(state => state.transReport);
  const navigate = useNavigate();

  console.log('transReportList', transReportList);

  // table
  const [data, setData] = useState<dataProps[]>([]);
  const [View, setView] = useState<boolean>(false);
  const [transReportTypeCode, setTransReportType] = useState<transReportProps>();
  
  const columns = useMemo(
    () =>
      [
        {
          Header: '#',
          accessor: 'id',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            if (row.id === undefined || row.id === null || row.id === '') {
              return <>-</>
            }
            if (typeof row.id === 'string') {
              return <>{(parseInt(row.id) + 1).toString()}</>;
            }
            if (typeof row.id === 'number') {
              return <>{row.id + 1}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          Header: 'Report ID',
          accessor: 'reportId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.reportId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Rentity ID',
          accessor: 'rentityId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.rentityId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Rentity Branch',
          accessor: 'rentityBranch',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.rentityBranch;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Submission Code',
          accessor: 'submissionCode',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.submissionCode;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Report Code',
          accessor: 'reportCode',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.reportCode;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Entity Reference',
          accessor: 'entityReference',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.entityReference;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'FIU Ref Number',
          accessor: 'fiuRefNumber',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.fiuRefNumber;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Submission Date',
          accessor: 'submissionDate',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.submissionDate;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{new Date(value).toLocaleString()}</>;
          }
        },
        {
          Header: 'Currency Code Local',
          accessor: 'currencyCodeLocal',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.currencyCodeLocal;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Reporting Person',
          accessor: 'reportingPerson',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.reportingPerson;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Location',
          accessor: 'location',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.location;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Reason',
          accessor: 'reason',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.reason;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Action',
          accessor: 'action',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.action;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'System Date',
          accessor: 'systemDate',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.systemDate;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{new Date(value).toLocaleString()}</>;
          }
        },
        {
          Header: 'User ID',
          accessor: 'userId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.userId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Number of Transactions',
          accessor: 'noOfTransactions',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.noOfTransactions;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Last Updated Date',
          accessor: 'lastTrxUpdatedDate',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.lastTrxUpdatedDate;
            if (value === undefined || value === null || value === '') return <>-</>;

            // Convert the timestamp to a date
            const date = new Date(value);
            const formattedDate = date.toLocaleDateString(); // This will format the date as MM/DD/YYYY

            return <>{formattedDate}</>;
          }
        },
        {
          Header: 'Status',
          accessor: 'isActive',
          Cell: ({ row }: { row: Row }) => {
            if (row.values.isActive === undefined || row.values.isActive === null || row.values.isActive === '') {
              return <>-</>
            }
            if (typeof row.values.isActive === 'string') {
              return <>{row.values.isActive}</>;
            }
            if (typeof row.values.isActive === 'number') {
              return <>{row.values.isActive}</>;
            }
            if (typeof row.values.isActive === 'boolean') {
              return <>{row.values.isActive ? "ACTIVE" : "IN-ACTIVE"}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          id: "actions",
          Header: 'Actions',
          accessor: 'actions',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            const data: dataProps = row.original
            return (
              <>
                <Stack direction="row" alignItems="center" justifyContent="center" spacing={0}>
                  <Tooltip title="View Transactions">
                    <IconButton
                      color="primary"
                      onClick={() => {
                        navigate(`/transaction-management/trans/${data.reportId}`)


                      }}
                    >
                      <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                    </IconButton>
                  </Tooltip>
                </Stack>
              </>
            )
          }
        }
      ] as Column[],
    []
  );

  const handleView = () => {
    setView(!View);
    if (transReportTypeCode && !View) setTransReportType(undefined);
  };

  //================================API CONFIG=============================//

  const [page] = useState<number>(0);
  const [perPage] = useState<number>(10);
  const [direction] = useState<"asc" | "desc">("asc");
  const [sort] = useState<string>("reportId");

  useEffect(() => {
    const listParameters: queryParamsProps = {
      page: page,
      per_page: perPage,
      direction: direction,
      sort: sort,
      search: ""
    };
    dispatch(fetchTransReportsTable(listParameters));
  }, [dispatch, success, page, perPage, direction, sort]);

  useEffect(() => {
    if (!transReportList) {
      setData([]);
      return;
    }
    if (transReportList == null) {
      setData([]);
      return;
    }
    setData(transReportList!.result!);
  }, [transReportList]);

  //  handel error 
  useEffect(() => {
    if (error != null && typeof error === 'object' && 'message' in error) {
      dispatch(
        openSnackbar({
          open: true,
          message: (error as { message: string }).message, // Type assertion
          variant: 'alert',
          alert: {
            color: 'error'
          },
          close: true
        })
      );
      dispatch(toInitialState());
    }
  }, [error]);

  //  handel success
  useEffect(() => {
    if (success != null) {
      dispatch(
        openSnackbar({
          open: true,
          message: success,
          variant: 'alert',
          alert: {
            color: 'success'
          },
          close: true
        })
      );
      dispatch(toInitialState())
    }
  }, [success])

  if (isLoading) {
    return <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>Loading...</Box>;
  }

  return (
    <Box sx={{ width: '100%', height: '100%' }}>
      <MainCard 
        content={false} 
        sx={{ 
          height: '100%',
          '& .MuiCardContent-root': { 
            height: '100%',
            p: 0 
          }
        }}
      >
        <Box sx={{ 
          height: '100%',
          display: 'flex',
          flexDirection: 'column',
          overflow: 'hidden'
        }}>
          <ReactTable 
            columns={columns} 
            data={data} 
            getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()} 
          />
        </Box>

        <Dialog
          maxWidth="sm"
          TransitionComponent={PopupTransition}
          keepMounted
          fullWidth
          onClose={handleView}
          open={View}
          sx={{ '& .MuiDialog-paper': { p: 0 } }}
        >
          <ViewTransReport transReport={transReportTypeCode} onCancel={handleView} />
        </Dialog>
      </MainCard>
    </Box>
  );
};

export default List;
