// import { Dispatch, SetStateAction } from 'react';
import { Dispatch, SetStateAction } from 'react';
import { Column, HeaderGroup } from 'react-table';
import { transReport } from 'types/trans-report';

export interface dataProps extends transReport {}


export interface ReactTableProps {
  columns: Column[]
  data: dataProps[]
  getHeaderProps: (column: HeaderGroup) => {};
  // handleAddEdit: () => void
  //tableParams?: TableParamsType
}

export interface transReportProps extends transReport { }

export interface TableParamsType {
  page: number;
  setPage: Dispatch<SetStateAction<number>>;
  perPage: number;
  setPerPage: Dispatch<SetStateAction<number>>;
  direction: "asc" | "desc";
  setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
  sort: string;
  setSort: Dispatch<SetStateAction<string>>;

}
