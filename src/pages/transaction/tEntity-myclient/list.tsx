import { MouseEvent, useMemo, useState, useEffect, Fragment } from 'react';

// material-ui
import {
  alpha,
  Button,
  Dialog,
  IconButton,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
  Typography,
  useMediaQuery,
  useTheme,
} from '@mui/material';

// third-party
import { Cell, Column, HeaderGroup, Row, useFilters, useGlobalFilter, usePagination, useTable, useSortBy, useRowSelect, useExpanded } from 'react-table';

// project import
import { PopupTransition } from 'components/@extended/Transitions';
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import {  HeaderSort, SortingSelect, TablePagination, TableRowSelection } from 'components/third-party/ReactTable';

// utils
import {
  GlobalFilter,
  renderFilterTypes
} from 'utils/react-table';

// sections

// assets
import { ArrowLeftOutlined, EyeTwoTone, PlusOutlined } from '@ant-design/icons';



//types
import { ReactTableProps, TableParamsType, dataProps, tEntityMyclientProps } from './types/types';
import AddEditTEntityMyclient from 'sections/transactions/tEntity-myclient/AddEditTEntityMyclient';
import { useDispatch, useSelector } from 'store';
import { queryParamsProps } from 'types/entity-my-client';
import { fetchMyClientEntityTable, toInitialState } from 'store/reducers/entity-my-client';
import { openSnackbar } from 'store/reducers/snackbar';
import { useLocation, useNavigate } from 'react-router';


// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, handleAddEdit, getHeaderProps }: ReactTableProps) {
  const filterTypes = useMemo(() => renderFilterTypes, []);
  const theme = useTheme();
  const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));
  const sortBy = { id: 'id', desc: false };


  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    allColumns,
    prepareRow,
    preGlobalFilteredRows,
    setGlobalFilter,
    page,
    gotoPage,
    setPageSize,
    setSortBy,
    state: { pageIndex, pageSize, selectedRowIds, globalFilter }
  }  = useTable(
    {
        columns,
        data,
        filterTypes,
        initialState: { pageIndex: 0, pageSize: 10, hiddenColumns: ['avatar'], sortBy: [sortBy] }
    },
    useGlobalFilter,
    useFilters,
    useSortBy,
    useExpanded,
    usePagination,
    useRowSelect
);

   

  return (
    <>
      <TableRowSelection selected={Object.keys(selectedRowIds).length} />

      <Stack spacing={3}>
                <Stack
                    direction={matchDownSM ? 'column' : 'row'}
                    spacing={4}
                    justifyContent="space-between"
                    alignItems="center"
                    sx={{ p: 3, pb: 0 }}
                >
                    <GlobalFilter
                        preGlobalFilteredRows={preGlobalFilteredRows}
                        globalFilter={globalFilter}
                        setGlobalFilter={setGlobalFilter}
                        size="small"
                    />
                    <Stack direction={matchDownSM ? 'column' : 'row'} alignItems="center" spacing={1}>
                        <SortingSelect sortBy={sortBy.id} setSortBy={setSortBy} allColumns={allColumns}  />
                        <Button variant="contained" startIcon={<PlusOutlined />} onClick={handleAddEdit} size="small">
                            Add
                        </Button>

                    </Stack>
                </Stack>
                <Table {...getTableProps()}>
                    <TableHead>
                        {headerGroups.map((headerGroup: HeaderGroup<{}>) => (
                            <TableRow {...headerGroup.getHeaderGroupProps()} sx={{ '& > th:first-of-type': { width: '58px' } }}>
                                {headerGroup.headers.map((column: HeaderGroup) => (
                                    <TableCell {...column.getHeaderProps([{ className: column.className }, getHeaderProps(column)])}>
                                        <HeaderSort column={column} />
                                    </TableCell>
                                ))}
                            </TableRow>
                        ))}
                    </TableHead>
                    <TableBody {...getTableBodyProps()}>
                      {page.length === 0 ? (
                      <TableRow>
                        <TableCell colSpan={12} align="center">
                          No data 
                        </TableCell>
                      </TableRow>
                      ) : (
                      page.map((row: Row, i: number) => {
                      prepareRow(row);
                      return (
                        <Fragment key={i}>
                          <TableRow
                            {...row.getRowProps()}
                            onClick={() => {
                            row.toggleRowSelected();
                            }}
                            sx={{
                            cursor: 'pointer',
                            bgcolor: row.isSelected
                            ? alpha(theme.palette.primary.lighter, 0.35)
                            : 'inherit',
                          }}
                          >
                          {row.cells.map((cell: Cell) => (
                          <TableCell {...cell.getCellProps([{ className: cell.column.className }])}
                          sx={{ textAlign: cell.column.isNumeric ? 'left' : 'right' }}
                          >
                          {cell.render('Cell')}
                          </TableCell>
                          ))}
                          </TableRow>
                        </Fragment>
                );
                        })
                        )}
                        <TableRow>
                            <TableCell sx={{ p: 2 }} colSpan={15}>
                                <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </Stack>
    </>
  );
}
// ==============================|| List ||============================== //

const List = () => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const { MyClientEntityList, error, success } = useSelector(state => state.entityMyClient);
  const location = useLocation();
  const navigate = useNavigate();

  // Extract trxId from state
  const MyClientEntityId = location.state?.MyClientEntity;

  // table
  const [data, setData] = useState<dataProps[]>([])



  const columns = useMemo(
    () =>
      [
        {
          Header: '#',
          accessor: 'id',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            if (row.id === undefined || row.id === null || row.id === '') {
              return <>-</>
            }
            if (typeof row.id === 'string') {
              return <>{(parseInt(row.id) + 1).toString()}</>;
            }
            if (typeof row.id === 'number') {
              return <>{row.id + 1}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          Header: 'Entity Client ID',
          accessor: 'entityMyClientId',
          Cell: ({ row }: { row: any }) => {
            const value = row.values.entityMyClientId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Name',
          accessor: 'name',
          Cell: ({ row }: { row: any }) => {
            const value = row.values.name;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Commercial Name',
          accessor: 'commercialName',
          Cell: ({ row }: { row: any }) => {
            const value = row.values.commercialName;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Entity Legal Form Type',
          accessor: 'entityLegalFormTypeId',
          Cell: ({ row }: { row: any }) => {
            const value = row.values.entityLegalFormTypeId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Incorporation Number',
          accessor: 'incorporationNumber',
          Cell: ({ row }: { row: any }) => {
            const value = row.values.incorporationNumber;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Business',
          accessor: 'business',
          Cell: ({ row }: { row: any }) => {
            const value = row.values.business;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Mobile Number',
          accessor: 'phones',
          Cell: ({ row }: { row: any }) => {
            const value = row.values.phones;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Addresses',
          accessor: 'addresses',
          Cell: ({ row }: { row: any }) => {
            const value = row.values.addresses;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Email',
          accessor: 'email',
          Cell: ({ row }: { row: any }) => {
            const value = row.values.email;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'URL',
          accessor: 'url',
          Cell: ({ row }: { row: any }) => {
            const value = row.values.url;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Incorporation State',
          accessor: 'incorporationState',
          Cell: ({ row }: { row: any }) => {
            const value = row.values.incorporationState;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Country Code',
          accessor: 'countryCodeId',
          Cell: ({ row }: { row: any }) => {
            const value = row.values.countryCodeId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Person My Client',
          accessor: 'personMyClientId',
          Cell: ({ row }: { row: any }) => {
            const value = row.values.personMyClientId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Entity Person Role Type',
          accessor: 'entityPersonRoleTypeId',
          Cell: ({ row }: { row: any }) => {
            const value = row.values.entityPersonRoleTypeId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Incorporation Date',
          accessor: 'incorporationDate',
          Cell: ({ row }: { row: any }) => {
            const value = row.values.incorporationDate;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Business Closed',
          accessor: 'businessClosed',
          Cell: ({ row }: { row: any }) => {
            const value = row.values.businessClosed;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value ? 'Yes' : 'No'}</>;
          }
        },
        {
          Header: 'Date Business Closed',
          accessor: 'dateBusinessClosed',
          Cell: ({ row }: { row: any }) => {
            const value = row.values.dateBusinessClosed;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Tax Number',
          accessor: 'taxNumber',
          Cell: ({ row }: { row: any }) => {
            const value = row.values.taxNumber;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Tax Reg Number',
          accessor: 'taxRegNumber',
          Cell: ({ row }: { row: any }) => {
            const value = row.values.taxRegNumber;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Comments',
          accessor: 'comments',
          Cell: ({ row }: { row: any }) => {
            const value = row.values.comments;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Status',
          accessor: 'isActive',
          Cell: ({ row }: { row: Row }) => {
            if (row.values.isActive === undefined || row.values.isActive === null || row.values.isActive === '') {
              return <>-</>
            }
            if (typeof row.values.isActive === 'string') {
              return <>{row.values.isActive}</>;
            }
            if (typeof row.values.isActive === 'number') {
              return <>{row.values.isActive}</>;
            }
            if (typeof row.values.isActive === 'boolean') {
              return <>{row.values.isActive ? "ACTIVE" : "IN-ACTIVE"}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          id: "actions",
          Header: 'Actions',
          accessor: 'actions',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            const data: dataProps = row.original
            return (
              <>
                <Stack direction="row" alignItems="center" justifyContent="center" spacing={0}>
                  <Tooltip title="Edit">
                    <IconButton
                      color="primary"
                      onClick={(e: MouseEvent<HTMLButtonElement>) => {
                        e.stopPropagation();
                        handleAddEdit()
                        settEntityMyclient({
                          ...data
                        })
                      }}
                    >
                      <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                    </IconButton>
                  </Tooltip>
                  </Stack>
              </>
            )
          }
        }
      ] as Column[],
    []
  );

  //dialog model 
  const [addEdit, setAddEdit] = useState<boolean>(false);
  const [tEntityMyclientCode, settEntityMyclient] = useState<tEntityMyclientProps>();

  const handleAddEdit = () => {
    setAddEdit(!addEdit);
    if (tEntityMyclientCode && !addEdit) settEntityMyclient(undefined);
  };

  // //================================API CONFIG=============================//

  const [page, setPage] = useState<number>(0);
  const [perPage, setPerPage] = useState<number>(10);
  const [direction, setDirection] = useState<"asc" | "desc">("asc");
  const [sort, setSort] = useState<string>("entityMyClientId"); //CHANGE ID

  const tableParams: TableParamsType = {
    page,
    setPage,
    perPage,
    setPerPage,
    direction,
    setDirection,
    sort,
    setSort
  }

  useEffect(() => {
    const listParameters: queryParamsProps = {
      page: 0,
      per_page: 1000,
      direction: 'asc',
      sort: 'entityMyClientId',
      search: '',
      entityMyClientId: MyClientEntityId
    };
    dispatch(fetchMyClientEntityTable(listParameters));
  }, [dispatch, success, page, perPage, direction, sort]);

  useEffect(() => {
    if (MyClientEntityList?.result) {
      const filteredData = MyClientEntityId ? MyClientEntityList.result.filter((item) => item.entityMyClientId === MyClientEntityId) : MyClientEntityList.result;
      setData(filteredData);
    }
  }, [MyClientEntityList, MyClientEntityId]);

  //  handel error 
  useEffect(() => {
    if (error != null && typeof error === 'object' && 'message' in error) {
      dispatch(
        openSnackbar({
          open: true,
          message: (error as { message: string }).message, // Type assertion
          variant: 'alert',
          alert: {
            color: 'error'
          },
          close: true
        })
      );
      dispatch(toInitialState());
    }
  }, [error]);

  //  handel success
  useEffect(() => {
    if (success != null) {
      dispatch(
        openSnackbar({
          open: true,
          message: success,
          variant: 'alert',
          alert: {
            color: 'success'
          },
          close: true
        })
      );
      dispatch(toInitialState())
    }
  }, [success])

  return (
    <>
      <MainCard content={false} title={
        <Stack direction="row" alignItems="center" spacing={1}>
          <IconButton onClick={() => {
            navigate(-1)

          }}>
            <ArrowLeftOutlined />
          </IconButton>
          <Typography sx={{ fontWeight: 'bold', fontSize: '1.5rem' }}>
            Transactions - Transaction Entity
          </Typography>
        </Stack>
      }>
        <ScrollX>
          <ReactTable columns={columns} data={data} handleAddEdit={handleAddEdit} tableParams={tableParams} getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()} />
        </ScrollX>
        {/* add / edit submissionCode dialog */}
        <Dialog
          maxWidth="sm"
          TransitionComponent={PopupTransition}
          keepMounted
          fullWidth
          onClose={handleAddEdit}
          open={addEdit}
          sx={{ '& .MuiDialog-paper': { p: 0 }, transition: 'transform 225ms' }}
          aria-describedby="alert-dialog-slide-description"
        >
          <AddEditTEntityMyclient TEntityMyclient={tEntityMyclientCode} onCancel={handleAddEdit} />
        </Dialog>
      </MainCard>
    </>
  );
}

export default List;
