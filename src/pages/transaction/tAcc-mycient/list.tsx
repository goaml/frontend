import { Fragment, MouseEvent, useEffect, useMemo, useState } from 'react';

// material-ui
import {
  alpha,
  Box,
  Dialog,
  IconButton,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
  Typography,
  useMediaQuery,
  useTheme
} from '@mui/material';

// third-party
import { Cell, Column, HeaderGroup, Row, useFilters, useGlobalFilter, usePagination, useSortBy, useTable } from 'react-table';

// project import
import { PopupTransition } from 'components/@extended/Transitions';
import MainCard from 'components/MainCard';
import { HeaderSort, TablePagination } from 'components/third-party/ReactTable';

// utils
import {
  GlobalFilter,
  renderFilterTypes
} from 'utils/react-table';

// sections

// assets
import { ArrowLeftOutlined, EyeTwoTone } from '@ant-design/icons';

//types 
import { useLocation, useNavigate } from 'react-router';
import AddEditTAccMyclient from 'sections/transactions/tAcc-myclient/AddEditTAccMyclient';
import { useDispatch, useSelector } from 'store';
import { fetchMyClientAccountTable, toInitialState } from 'store/reducers/account-my-client';
import { openSnackbar } from 'store/reducers/snackbar';
import { queryParamsProps } from 'types/account-my-client';
import { dataProps, ReactTableProps, TableParamsType, tAccMyclientProps } from './types/types';

// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, getHeaderProps }: ReactTableProps) {
  const theme = useTheme();
  const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));
  const sortBy = { id: 'id', desc: false };
  const filterTypes = useMemo(() => renderFilterTypes, []);

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    preGlobalFilteredRows,
    setGlobalFilter,
    page,
    gotoPage,
    setPageSize,
    state: { globalFilter, pageIndex, pageSize }
  } = useTable(
    {
      columns,
      data,
      filterTypes,
      initialState: { pageIndex: 0, pageSize: 10, hiddenColumns: ['avatar'], sortBy: [sortBy] }
    },
    useGlobalFilter,
    useFilters,
    useSortBy,
    usePagination
  );

  return (
    <Box sx={{ width: '100%', overflowX: 'auto' }}>
      <Stack spacing={3}>
        <Stack
          direction={matchDownSM ? 'column' : 'row'}
          spacing={2}
          justifyContent="space-between"
          alignItems={matchDownSM ? 'stretch' : 'center'}
          sx={{ p: 3, pb: 0 }}
        >
          <Box sx={{ width: matchDownSM ? '100%' : 'auto' }}>
            <GlobalFilter
              preGlobalFilteredRows={preGlobalFilteredRows}
              globalFilter={globalFilter}
              setGlobalFilter={setGlobalFilter}
              size="small"
            />
          </Box>
        </Stack>

        <Box sx={{ 
          width: '100%',
          overflowX: 'auto',
          '& .MuiTable-root': {
            minWidth: 750,
            borderCollapse: 'separate',
            borderSpacing: '0 8px'
          }
        }}>
          <Table {...getTableProps()}>
            <TableHead>
              {headerGroups.map((headerGroup: HeaderGroup<{}>, index: number) => (
                <TableRow {...headerGroup.getHeaderGroupProps()}>
                  {headerGroup.headers.map((column: HeaderGroup, colIndex: number) => (
                    <TableCell 
                      {...column.getHeaderProps([{ className: column.className, key: colIndex }, getHeaderProps(column)])}
                      sx={{ 
                        whiteSpace: 'nowrap',
                        px: 2,
                        background: theme.palette.background.paper,
                        position: 'sticky',
                        top: 0,
                        zIndex: 1
                      }}
                    >
                      <HeaderSort column={column} />
                    </TableCell>
                  ))}
                </TableRow>
              ))}
            </TableHead>
            <TableBody {...getTableBodyProps()}>
              {page.length === 0 ? (
                <TableRow>
                  <TableCell colSpan={columns.length} align="center">
                    No data
                  </TableCell>
                </TableRow>
              ) : (
                page.map((row: Row, i: number) => {
                  prepareRow(row);
                  return (
                    <Fragment key={i}>
                      <TableRow
                        {...row.getRowProps()}
                        sx={{
                          cursor: 'pointer',
                          bgcolor: row.isSelected ? alpha(theme.palette.primary.lighter, 0.35) : theme.palette.background.paper,
                          '&:hover': {
                            bgcolor: alpha(theme.palette.primary.lighter, 0.2)
                          }
                        }}
                      >
                        {row.cells.map((cell: Cell, cellIndex: number) => (
                          <TableCell 
                            {...cell.getCellProps([{ className: cell.column.className, key: cellIndex }])}
                            sx={{ 
                              whiteSpace: 'nowrap',
                              overflow: 'hidden',
                              textOverflow: 'ellipsis',
                              maxWidth: '200px',
                              px: 2
                            }}
                          >
                            {cell.render('Cell')}
                          </TableCell>
                        ))}
                      </TableRow>
                    </Fragment>
                  );
                })
              )}
            </TableBody>
          </Table>
        </Box>

        <Box sx={{ p: 2, borderTop: `1px solid ${theme.palette.divider}` }}>
          <TablePagination
            gotoPage={gotoPage}
            rows={rows}
            setPageSize={setPageSize}
            pageIndex={pageIndex}
            pageSize={pageSize}
          />
        </Box>
      </Stack>
    </Box>
  );
}

// ==============================|| List ||============================== //

const List = () => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const { MyClientAccountList, error, success } = useSelector(state => state.accountMyClient);

  const navigate = useNavigate()
  const location = useLocation();

  // Extract trxId from state
  const MyClientAccId = location.state?.MyClientAccId;
  // table
  const [data, setData] = useState<dataProps[]>([])


  const columns = useMemo(
    () =>
      [
        {
          Header: '#',
          accessor: 'id',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            if (row.id === undefined || row.id === null || row.id === '') {
              return <>-</>
            }
            if (typeof row.id === 'string') {
              return <>{(parseInt(row.id) + 1).toString()}</>;
            }
            if (typeof row.id === 'number') {
              return <>{row.id + 1}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          Header: 'Client Account ID',
          accessor: 'accountMyClientId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.accountMyClientId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Institution Name',
          accessor: 'institutionName',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.institutionName;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Institution Code',
          accessor: 'institutionCode',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.institutionCode;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'SWIFT',
          accessor: 'swift',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.swift;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Non Bank Institution',
          accessor: 'nonBankInstitution',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.nonBankInstitution;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value ? 'Yes' : 'No'}</>;
          }
        },
        {
          Header: 'Branch',
          accessor: 'branch',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.branch;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Account',
          accessor: 'account',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.account;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Account Name',
          accessor: 'accountName',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.accountName;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Currencies ID',
          accessor: 'currenciesId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.currenciesId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },

        {
          Header: 'IBAN',
          accessor: 'iban',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.iban;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Client Number',
          accessor: 'clientNumber',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.clientNumber;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Account Type ID',
          accessor: 'accountTypeId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.accountTypeId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Entity Client ID',
          accessor: 'entityMyClientId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.entityMyClientId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Person My Client ID',
          accessor: 'personMyClientId',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.personMyClientId;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Opened',
          accessor: 'opened',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.opened;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Closed',
          accessor: 'closed',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.closed;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Balance',
          accessor: 'balance',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.balance;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Date Balance',
          accessor: 'dateBalance',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.dateBalance;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Beneficiary',
          accessor: 'beneficiary',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.beneficiary;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Beneficiary Comment',
          accessor: 'beneficiaryComment',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.beneficiaryComment;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },
        {
          Header: 'Comments',
          accessor: 'comments',
          Cell: ({ row }: { row: Row }) => {
            const value = row.values.comments;
            if (value === undefined || value === null || value === '') return <>-</>;
            return <>{value}</>;
          }
        },

        {
          Header: 'Status',
          accessor: 'isActive',
          Cell: ({ row }: { row: Row }) => {
            if (row.values.isActive === undefined || row.values.isActive === null || row.values.isActive === '') {
              return <>-</>
            }
            if (typeof row.values.isActive === 'string') {
              return <>{row.values.isActive}</>;
            }
            if (typeof row.values.isActive === 'number') {
              return <>{row.values.isActive}</>;
            }
            if (typeof row.values.isActive === 'boolean') {
              return <>{row.values.isActive ? "ACTIVE" : "IN-ACTIVE"}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          id: "actions",
          Header: 'Actions',
          accessor: 'actions',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            const data: dataProps = row.original
            return (
              <>
                <Stack direction="row" alignItems="center" justifyContent="center" spacing={0}>
                  {/* <Tooltip title="View">
                                        <IconButton
                                            color="secondary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                            }}
                                        >
                                            <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                                        </IconButton>
                                    </Tooltip> */}
                  <Tooltip title="Edit">
                    <IconButton
                      color="primary"
                      onClick={(e: MouseEvent<HTMLButtonElement>) => {
                        e.stopPropagation();
                        handleAddEdit()
                        settAccMyclient({
                          ...data
                        })
                      }}
                    >
                      <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                    </IconButton>
                  </Tooltip>
                  {/* <Tooltip title="Delete">
                    <IconButton
                      color="error"
                      onClick={(e: MouseEvent<HTMLButtonElement>) => {
                        e.stopPropagation();
                        settranstoId(data.toId!)
                        setOpenAlert(true)
                      }}
                    >
                      <DeleteTwoTone twoToneColor={theme.palette.error.main} />
                    </IconButton>
                  </Tooltip> */}
                </Stack>
              </>
            )
          }
        }
      ] as Column[],
    []
  );

  //dialog model 
  const [addEdit, setAddEdit] = useState<boolean>(false);
  const [tAccMyclientCode, settAccMyclient] = useState<tAccMyclientProps>();

  const handleAddEdit = () => {
    setAddEdit(!addEdit);
    if (tAccMyclientCode && !addEdit) settAccMyclient(undefined);
  };

  // //alert model
  // const [openAlert, setOpenAlert] = useState(false);
  // const [transtoId, settransFromId] = useState<number | null>(null)

  // const handleAlertClose = () => {
  //   setOpenAlert(!openAlert);
  // };
  // //================================API CONFIG=============================//

  const [page, setPage] = useState<number>(0);
  const [perPage, setPerPage] = useState<number>(10);
  const [direction, setDirection] = useState<"asc" | "desc">("asc");
  const [sort, setSort] = useState<string>("accountMyClientId"); //CHANGE ID

  const tableParams: TableParamsType = {
    page,
    setPage,
    perPage,
    setPerPage,
    direction,
    setDirection,
    sort,
    setSort
  }

  useEffect(() => {
    const listParameters: queryParamsProps = {
      page: 0,
      per_page: 1000,
      direction: 'asc',
      sort: 'accountMyClientId',
      search: '',
      accountMyClientId: MyClientAccId
    };
    dispatch(fetchMyClientAccountTable(listParameters));
  }, [dispatch, success, page, perPage, direction, sort]);

  useEffect(() => {
    if (MyClientAccountList?.result) {
      const filteredData = MyClientAccId ? MyClientAccountList.result.filter((item) => item.accountMyClientId === MyClientAccId) : MyClientAccountList.result;
      setData(filteredData);
    }
  }, [MyClientAccountList, MyClientAccId]);

  //  handel error 
  useEffect(() => {
    if (error != null && typeof error === 'object' && 'message' in error) {
      dispatch(
        openSnackbar({
          open: true,
          message: (error as { message: string }).message, // Type assertion
          variant: 'alert',
          alert: {
            color: 'error'
          },
          close: true
        })
      );
      dispatch(toInitialState());
    }
  }, [error]);

  //  handel success
  useEffect(() => {
    if (success != null) {
      dispatch(
        openSnackbar({
          open: true,
          message: success,
          variant: 'alert',
          alert: {
            color: 'success'
          },
          close: true
        })
      );
      dispatch(toInitialState())
    }
  }, [success])

  // if (isLoading) {
  //     return <div>Loading...</div>;
  // }


  return (
    <Box sx={{ width: '100%', height: '100%' }}>
      <MainCard 
        content={false} 
        title={
          <Stack direction="row" alignItems="center" spacing={1}>
            <IconButton onClick={() => navigate(-1)}>
              <ArrowLeftOutlined />
            </IconButton>
            <Typography variant="h4">
              Transactions - Transaction Account
            </Typography>
          </Stack>
        }
        sx={{ height: '100%' }}
      >
        <Box sx={{ width: '100%', height: '100%', overflow: 'hidden' }}>
          <ReactTable 
            columns={columns}
            data={data}
            handleAddEdit={handleAddEdit}
            getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()} tableParams={tableParams}          />
        </Box>

        <Dialog
          maxWidth="sm"
          TransitionComponent={PopupTransition}
          keepMounted
          fullWidth
          onClose={handleAddEdit}
          open={addEdit}
          sx={{ '& .MuiDialog-paper': { p: 0 } }}
        >
          <AddEditTAccMyclient tAccMyclient={tAccMyclientCode} onCancel={handleAddEdit} />
        </Dialog>
      </MainCard>
    </Box>
  );
};


export default List;
