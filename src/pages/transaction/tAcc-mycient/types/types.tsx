// import { Dispatch, SetStateAction } from 'react';
import { Dispatch, SetStateAction } from 'react';
import { Column, HeaderGroup } from 'react-table';
import { MyClientAccount } from 'types/account-my-client';

export interface dataProps extends MyClientAccount {}


export interface ReactTableProps {
  columns: Column[]
  data: dataProps[]
  handleAddEdit: () => void
  getHeaderProps: (column: HeaderGroup) => {}
  tableParams: TableParamsType
}

export interface tAccMyclientProps  extends MyClientAccount {}

export interface TableParamsType {
  page: number;
  setPage: Dispatch<SetStateAction<number>>;
  perPage: number;
  setPerPage: Dispatch<SetStateAction<number>>;
  direction: "asc" | "desc";
  setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
  sort: string;
  setSort: Dispatch<SetStateAction<string>>;

}
