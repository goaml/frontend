import { Dispatch, SetStateAction } from 'react';
import { Column } from 'react-table';
import { DeptCodeResType, DeptCodeType } from 'types/department';

export interface dataProps extends DeptCodeResType { }

export interface ReactTableProps {
  columns: Column[]
  data: dataProps[]
  handleAddEdit: () => void
  tableParams: TableParamsType
}

export interface departmentProps extends DeptCodeType { }

export interface TableParamsType {
  page: number;
  setPage: Dispatch<SetStateAction<number>>;
  perPage: number;
  setPerPage: Dispatch<SetStateAction<number>>;
  direction: "asc" | "desc";
  setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
  sort: string;
  setSort: Dispatch<SetStateAction<string>>;

}
