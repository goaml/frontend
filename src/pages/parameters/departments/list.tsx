import { MouseEvent, useEffect, useMemo, useState } from 'react';

// material-ui
import {
  Button,
  Dialog,
  IconButton,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
  useTheme,
} from '@mui/material';

// third-party
import { Cell, Column, HeaderGroup, Row, useFilters, useGlobalFilter, usePagination, useTable } from 'react-table';

// project import
import { PopupTransition } from 'components/@extended/Transitions';
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import { CSVExport, EmptyTable, TablePagination } from 'components/third-party/ReactTable';

// utils
import {
  DefaultColumnFilter,
  GlobalFilter,
  renderFilterTypes
} from 'utils/react-table';

// sections

// assets
import { DeleteTwoTone, EditTwoTone, PlusOutlined } from '@ant-design/icons';

//types 
import { useDispatch, useSelector } from 'store';
import { fetchDepartmentCodes, toInitialState } from 'store/reducers/department';
import { openSnackbar } from 'store/reducers/snackbar';
import { listParametersType } from 'types/department';
import { ReactTableProps, TableParamsType, departmentProps, dataProps } from './types/types';
import AddEditDepartment from 'sections/parameter/department/AddEditDepartment';
import AlertDepartmentDelete from 'sections/parameter/department/DeleteDepartment';

// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, handleAddEdit }: ReactTableProps) {
  const filterTypes = useMemo(() => renderFilterTypes, []);
  const defaultColumn = useMemo(() => ({ Filter: DefaultColumnFilter }), []);

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    preGlobalFilteredRows,
    setGlobalFilter,
    globalFilter,
    page,
    gotoPage,
    setPageSize,
    state: { pageIndex, pageSize }
  } = useTable(
    {
      columns,
      data,
      defaultColumn,
      filterTypes,

      initialState: { pageIndex: 0, pageSize: 10 }
    },
    useGlobalFilter,
    useFilters,
    usePagination
  );

  return (
    <>
      <Stack direction="row" spacing={2} justifyContent="space-between" sx={{ padding: 2 }}>
        <GlobalFilter preGlobalFilteredRows={preGlobalFilteredRows} globalFilter={globalFilter} setGlobalFilter={setGlobalFilter} />
        <Stack direction="row" alignItems="center" spacing={1}>
          <CSVExport data={rows.map((d: Row) => d.original)} filename={'filtering-table.csv'} />
          <Button variant="contained" startIcon={<PlusOutlined />} onClick={handleAddEdit}>
            Add New
          </Button>
        </Stack>
      </Stack>

      <Table {...getTableProps()}>
        <TableHead sx={{ borderTopWidth: 2 }}>
          {headerGroups.map((headerGroup) => (
            <TableRow {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column: HeaderGroup) => (
                <TableCell {...column.getHeaderProps([{ className: column.className }])}>{column.render('Header')}</TableCell>
              ))}
            </TableRow>
          ))}
        </TableHead>
        <TableBody {...getTableBodyProps()}>
          {page.length > 0 ? (
            page.map((row, i) => {
              prepareRow(row);
              return (
                <TableRow {...row.getRowProps()}>
                  {row.cells.map((cell: Cell) => (
                    <TableCell {...cell.getCellProps([{ className: cell.column.className }])}>{cell.render('Cell')}</TableCell>
                  ))}
                </TableRow>
              );
            })
          ) : (
            <EmptyTable msg="No Data" colSpan={12} />
          )}
          <TableRow>
            <TableCell sx={{ p: 2 }} colSpan={12}>
              <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </>
  );
}

// ==============================|| List ||============================== //

const List = () => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const { departmentCodeList, error, deptSuccess } = useSelector(state => state.department);


  // table
  const [data, setData] = useState<dataProps[]>([])

  const columns = useMemo(
    () =>
      [
        {
          Header: '#',
          accessor: 'id',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            if (row.id === undefined || row.id === null || row.id === '') {
              return <>-</>
            }
            if (typeof row.id === 'string') {
              return <>{(parseInt(row.id) + 1).toString()}</>;
            }
            if (typeof row.id === 'number') {
              return <>{row.id + 1}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          Header: 'Department Code',
          accessor: 'deptCode',
          Cell: ({ row }: { row: Row }) => {
            if (row.values.deptCode === undefined || row.values.deptCode === null || row.values.deptCode === '') {
              return <>-</>
            }
            if (typeof row.values.deptCode === 'string') {
              return <>{row.values.deptCode}</>;
            }
            if (typeof row.values.deptCode === 'number') {
              return <>{row.values.deptCode}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          Header: 'Department Location',
          accessor: 'deptLocation',
          Cell: ({ row }: { row: Row }) => {
            if (row.values.deptLocation === undefined || row.values.deptLocation === null || row.values.deptLocation === '') {
              return <>-</>
            }
            if (typeof row.values.deptLocation === 'string') {
              return <>{row.values.deptLocation}</>;
            }
            if (typeof row.values.deptLocation === 'number') {
              return <>{row.values.deptLocation}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        // {
        //   Header: 'Branch Status',
        //   accessor: 'usRStatusDetail',
        //   Cell: ({ row }: { row: Row }) => {
        //     if (row.values.usRStatusDetail.statusDesc === undefined || row.values.usRStatusDetail.statusDesc === null || row.values.usRStatusDetail.statusDesc === '') {
        //       return <>-</>
        //     }
        //     if (typeof row.values.usRStatusDetail.statusDesc === 'string') {
        //       return <>{row.values.usRStatusDetail.statusDesc}</>;
        //     }
        //     if (typeof row.values.usRStatusDetail.statusDesc === 'number') {
        //       return <>{row.values.usRStatusDetail.statusDesc}</>;
        //     }
        //     // Handle any other data types if necessary
        //     return <>-</>;
        //   }
        // },
        {
          id: "actions",
          Header: 'Actions',
          accessor: 'actions',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            const data: dataProps = row.original
            return (
              <>
                <Stack direction="row" alignItems="center" justifyContent="center" spacing={0}>
                  {/* <Tooltip title="View">
                                        <IconButton
                                            color="secondary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                            }}
                                        >
                                            <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                                        </IconButton>
                                    </Tooltip> */}
                  <Tooltip title="Edit">
                    <IconButton
                      color="primary"
                      onClick={(e: MouseEvent<HTMLButtonElement>) => {
                        e.stopPropagation();
                        handleAddEdit()
                        setBranch({
                          ...data
                        })
                      }}
                    >
                      <EditTwoTone twoToneColor={theme.palette.primary.main} />
                    </IconButton>
                  </Tooltip>
                  <Tooltip title="Delete">
                    <IconButton
                      color="error"
                      onClick={(e: MouseEvent<HTMLButtonElement>) => {
                        e.stopPropagation();
                        setdeptId(data.deptId!)
                        setOpenAlert(true)
                      }}
                    >
                      <DeleteTwoTone twoToneColor={theme.palette.error.main} />
                    </IconButton>
                  </Tooltip>
                </Stack>
              </>
            )
          }
        }
      ] as Column[],
    []
  );

  //dialog model 
  const [addEdit, setAddEdit] = useState<boolean>(false);
  const [department, setBranch] = useState<departmentProps>();

  const handleAddEdit = () => {
    setAddEdit(!addEdit);
    if (department && !addEdit) setBranch(undefined);
  };

  //alert model
  const [openAlert, setOpenAlert] = useState(false);
  const [deptId, setdeptId] = useState<number | null>(null)

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
  };
  //================================API CONFIG=============================//

  const [page, setPage] = useState<number>(0);
  const [perPage, setPerPage] = useState<number>(10);
  const [direction, setDirection] = useState<"asc" | "desc">("asc");
  const [sort, setSort] = useState<string>("deptId"); //CHANGE ID

  const tableParams: TableParamsType = {
    page,
    setPage,
    perPage,
    setPerPage,
    direction,
    setDirection,
    sort,
    setSort
  }

  useEffect(() => {
    const listParameters: listParametersType = {
      page: page,
      per_page: perPage,
      direction: direction,
      sort: sort
    };
    dispatch(fetchDepartmentCodes(listParameters));
  }, [dispatch, deptSuccess, page, perPage, direction, sort]);

  useEffect(() => {
    setData(departmentCodeList?.result! || []);
  }, [departmentCodeList])

  //  handel error 
  useEffect(() => {
    if (error != null && typeof error === 'object' && 'message' in error) {
      dispatch(
        openSnackbar({
          open: true,
          message: (error as { message: string }).message, // Type assertion
          variant: 'alert',
          alert: {
            color: 'error'
          },
          close: true
        })
      );
      dispatch(toInitialState());
    }
  }, [error]);

  //  handel success
  useEffect(() => {
    if (deptSuccess != null) {
      dispatch(
        openSnackbar({
          open: true,
          message: deptSuccess,
          variant: 'alert',
          alert: {
            color: 'success'
          },
          close: true
        })
      );
      dispatch(toInitialState())
    }
  }, [deptSuccess])

  // if (isLoading) {
  //     return <div>Loading...</div>;
  // }


  return (
    <>
      <MainCard content={false}>
        <ScrollX>
          <ReactTable columns={columns} data={data} handleAddEdit={handleAddEdit} tableParams={tableParams} />
        </ScrollX>
        {/* add / edit department dialog */}
        <Dialog
          maxWidth="sm"
          TransitionComponent={PopupTransition}
          keepMounted
          fullWidth
          onClose={handleAddEdit}
          open={addEdit}
          sx={{ '& .MuiDialog-paper': { p: 0 }, transition: 'transform 225ms' }}
          aria-describedby="alert-dialog-slide-description"
        >
          <AddEditDepartment department={department} onCancel={handleAddEdit} />
        </Dialog>
        {/* alert model */}
        {deptId && <AlertDepartmentDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={deptId} />}
      </MainCard>
    </>
  );
}

export default List;
