import { Dispatch, SetStateAction } from 'react';
import { Column } from 'react-table';
import { BranchCodeResType, BranchCodeType } from 'types/branch';

export interface dataProps extends BranchCodeResType { }

export interface ReactTableProps {
  columns: Column[]
  data: dataProps[]
  handleAddEdit: () => void
  tableParams: TableParamsType
}

export interface branchProps extends BranchCodeType { }

export interface TableParamsType {
  page: number;
  setPage: Dispatch<SetStateAction<number>>;
  perPage: number;
  setPerPage: Dispatch<SetStateAction<number>>;
  direction: "asc" | "desc";
  setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
  sort: string;
  setSort: Dispatch<SetStateAction<string>>;

}
