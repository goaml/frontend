import { Button, CircularProgress, Typography } from '@mui/material';
import MainCard from "components/MainCard";
import { useEffect, useState } from "react";
import { dispatch, useSelector } from "store";
import { fetchGlobalDetailsSuccess } from "store/reducers/global-details";

// ==============================|| CBSL URL ||============================== //

const CBSL = () => {
    const { globalDetails, isLoading, error } = useSelector(state => state.globalDetails);
    const [isButtonLoading, setIsButtonLoading] = useState(false);

    useEffect(() => {
        if (!globalDetails) {
            dispatch(fetchGlobalDetailsSuccess());
        }
    }, [globalDetails, dispatch]);

    // Function to handle the button click
    const handleButtonClick = () => {
        setIsButtonLoading(true);
        if (globalDetails?.result?.goAMLCoreURL) {
            window.location.href = globalDetails.result.goAMLCoreURL;
        }
    };

    if (isLoading) {
        return (
            <MainCard>
                <CircularProgress />
                <Typography>Loading global details...</Typography>
            </MainCard>
        );
    }

    if (error) {
        return (
            <MainCard>
                <Typography color="error">Error loading global details. Please try again later.</Typography>
            </MainCard>
        );
    }

    return (
        <MainCard>
            <Typography variant="h4" align="center" gutterBottom>
                CBSL URL
            </Typography>
            <Button
                variant="contained"
                color="primary"
                onClick={handleButtonClick}
                disabled={isLoading || isButtonLoading || !globalDetails?.result?.goAMLCoreURL}
                fullWidth
                sx={{ padding: '16px', fontSize: '16px' }}
            >
                {isButtonLoading ? <CircularProgress size={24} color="inherit" /> : 'Go AML CBSL'}
            </Button>

            {/* <Typography variant="body1" align="center" sx={{ marginTop: '16px' }}>
                {globalDetails?.result?.goAMLCoreURL || 'No additional details available'}
            </Typography> */}
        </MainCard>
    );
};

export default CBSL;
