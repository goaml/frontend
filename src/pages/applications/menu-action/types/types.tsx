import { Column } from 'react-table';
import { HeaderGroup } from 'react-table';
import { MenuActions } from 'types/menu-action';

export interface dataProps extends MenuActions { }

export interface ReactTableProps {
    columns: Column[]
    data: dataProps[]
    getHeaderProps: (column: HeaderGroup) => {};
}

export interface TableHeaderProps {
    headerGroups: HeaderGroup[];

}

export interface dataProps extends MenuActions { }

export interface menuActionsProps extends MenuActions { }





