import { Fragment, useEffect, useMemo, useState } from 'react';

// material-ui
import {
    alpha,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    useMediaQuery,
    useTheme
} from '@mui/material';

// third-party
import { Cell, Column, HeaderGroup, Row, useFilters, useGlobalFilter, useSortBy, usePagination, useTable } from 'react-table';

// project import
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import { TablePagination } from 'components/third-party/ReactTable';
import {
    GlobalFilter,
    renderFilterTypes
} from 'utils/react-table';
import { dataProps, ReactTableProps } from './types/types';

// assets

import { HeaderSort } from 'components/third-party/ReactTable2.0';
import useAuth from 'hooks/useAuth';
import { useDispatch, useSelector } from 'store';
import { fetchMenuActions, toInitialState, toResetIsActionSuccessState } from 'store/reducers/menu-action';
import { AddActivityLog } from 'store/reducers/note-log';
import { openSnackbar } from 'store/reducers/snackbar';
import { queryParamsProps } from 'types/menu-action';

// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, getHeaderProps }: ReactTableProps) {
    const theme = useTheme();

    const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));
    const sortBy = { id: 'id', desc: false };
    
    const filterTypes = useMemo(() => renderFilterTypes, []);

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        rows,
        page,
        gotoPage,
        setPageSize,
        state: { globalFilter, pageIndex, pageSize },
        preGlobalFilteredRows,
        setGlobalFilter,
    } = useTable(
        {
            columns,
            data,
            filterTypes,
            initialState: { pageIndex: 0, pageSize: 10, hiddenColumns: ['avatar'], sortBy: [sortBy] }
        },
        useGlobalFilter,
        useFilters,
        useSortBy,
        usePagination,
    );

    return (
        <>
            <Stack spacing={3}>
                <Stack
                    direction={matchDownSM ? 'column' : 'row'}
                    spacing={1}
                    justifyContent="space-between"
                    alignItems="center"
                    sx={{ p: 3, pb: 0 }}
                >
                    <GlobalFilter
                        preGlobalFilteredRows={preGlobalFilteredRows}
                        globalFilter={globalFilter}
                        setGlobalFilter={setGlobalFilter}
                        size="small"
                    />
                    
                </Stack>
                <Table {...getTableProps()}>
                    <TableHead>
                        {headerGroups.map((headerGroup: HeaderGroup<{}>) => (
                            <TableRow {...headerGroup.getHeaderGroupProps()} sx={{ '& > th:first-of-type': { width: '58px' } }}>
                                {headerGroup.headers.map((column: HeaderGroup) => (
                                    <TableCell {...column.getHeaderProps([{ className: column.className }, getHeaderProps(column)])}>
                                        <HeaderSort column={column} />
                                    </TableCell>
                                ))}
                            </TableRow>
                        ))}
                    </TableHead>
                    <TableBody {...getTableBodyProps()}>
                        {page.map((row: Row, i: number) => {
                            prepareRow(row);
                            return (
                                <Fragment key={i}>
                                    <TableRow
                                        {...row.getRowProps()}
                                        onClick={() => {
                                            row.toggleRowSelected();
                                        }}
                                        sx={{ cursor: 'pointer', bgcolor: row.isSelected ? alpha(theme.palette.primary.lighter, 0.35) : 'inherit' }}
                                    >
                                        {row.cells.map((cell: Cell) => (
                                            <TableCell {...cell.getCellProps([{ className: cell.column.className }])}>{cell.render('Cell')}</TableCell>
                                        ))}
                                    </TableRow>
                                    {/* {row.isExpanded && renderRowSubComponent({ row, rowProps, visibleColumns, expanded })} */}
                                </Fragment>
                            );
                        })}
                        <TableRow>
                            <TableCell sx={{ p: 2 }} colSpan={12}>
                                <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </Stack>
        </>
    );
}


// ==============================|| Menus ||============================== //

const Menus = () => {

    const dispatch = useDispatch();
    const { menuActions, isLoading, error, success,isActionSuccess } = useSelector(state => state.menuActions)
    const { user } = useAuth();
    // table
    const [data, setData] = useState<dataProps[]>([])

    const columns = useMemo(
        () =>
            [
                {
                    Header: '#',
                    accessor: 'id',
                    className: 'cell-center',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.id === undefined || row.id === null || row.id === '') {
                            return <>-</>
                        }
                        if (typeof row.id === 'string') {
                            return <>{(parseInt(row.id) + 1).toString()}</>;
                        }
                        if (typeof row.id === 'number') {
                            return <>{row.id + 1}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Module Name',
                    accessor: 'moduleName',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.moduleName === undefined || row.values.moduleName === null || row.values.moduleName === '') {
                            return <>-</>
                        }
                        if (typeof row.values.moduleName === 'string') {
                            return <>{row.values.moduleName}</>;
                        }
                        if (typeof row.values.moduleName === 'number') {
                            return <>{row.values.moduleName}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Menu Name',
                    accessor: 'name',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.name === undefined || row.values.name === null || row.values.name === '') {
                            return <>-</>
                        }
                        if (typeof row.values.name === 'string') {
                            return <>{row.values.name}</>;
                        }
                        if (typeof row.values.name === 'number') {
                            return <>{row.values.name}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Menu Action',
                    accessor: 'actionName',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.actionName === undefined || row.values.actionName === null || row.values.actionName === '') {
                            return <>-</>
                        }
                        if (typeof row.values.actionName === 'string') {
                            return <>{row.values.actionName}</>;
                        }
                        if (typeof row.values.actionName === 'number') {
                            return <>{row.values.actionName}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'URL',
                    accessor: 'url',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.url === undefined || row.values.url === null || row.values.url === '') {
                            return <>-</>
                        }
                        if (typeof row.values.url === 'string') {
                            return <>{row.values.url}</>;
                        }
                        if (typeof row.values.url === 'number') {
                            return <>{row.values.url}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
            ] as Column[],
        []
    );


    //API CALL - MENU ACTION LIST 

    useEffect(() => {
        const queryParams: queryParamsProps = {
            direction: "asc",
            page: 0,
            per_page: 1000,
            search: "",
            sort: "menuActionId",
            statusId: 3,
        }

        dispatch(fetchMenuActions(queryParams))
    }, [])

    useEffect(() => {
        if (!menuActions) {
            setData([])
            return
        }
        if (menuActions == null) {
            setData([])
            return
        }
        setData(menuActions.result!)
    }, [menuActions])

    useEffect(() => {
        if (error != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: "Error",
                    variant: 'alert',
                    alert: {
                        color: 'error'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [error])

    useEffect(() => {
        if (success != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: success,
                    variant: 'alert',
                    alert: {
                        color: 'success'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [success])

    useEffect(()=>{
        if (isActionSuccess != null) {
            let actionId, actionName ,keyValue,description;
    
            switch (isActionSuccess) {
                case 'LIST':
                    actionId = 2;
                    actionName = 'LIST';
                    keyValue = 0 ;
                    description='View Menu List'
                    break;
                default:
                    return; // Exit early if no valid action is found
            }
            dispatch(AddActivityLog({
                actionId: actionId,
                actionName: actionName,
                branchId: user?.branchList?.[0]?.branchId ?? undefined,
                companyId: user?.companyId,
                deptId: user?.departmentList?.[0]?.departmentId ?? undefined,
                description: description,
                keyValue: "menuActionId",
                keyValueId: keyValue,
                menuId: 11,
                menuName: "Menu Activity List",
                deptName: user?.departmentList?.[0]?.deptName ?? undefined
            }));
    
            dispatch(toResetIsActionSuccessState());
        }
    
    },[isActionSuccess]);

    if (isLoading) {
        return <>Loading...</>
    }

    return (
        <>
            <MainCard content={false}>
                <ScrollX>
                    <ReactTable columns={columns} data={data} getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()} />
                </ScrollX>
            </MainCard>
        </>
    );
}

export default Menus;
