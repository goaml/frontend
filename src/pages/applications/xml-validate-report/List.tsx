import { useState, useEffect } from 'react';
import {
    Button,
    Stack,
    TextField,
    InputLabel,
    Grid,
    Autocomplete,
    Box
} from '@mui/material';
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import { useDispatch, useSelector } from 'store';
import { reportValidate, fetchReportCodeListForValidate, toInitialState } from 'store/reducers/report-type-validate';
import { openSnackbar } from 'store/reducers/snackbar';
import LinearProgress from '@mui/material/LinearProgress';

const List = () => {
    const dispatch = useDispatch();
    const { selectreportCode, isLoading, Rsuccess, error } = useSelector((state) => state.reportTypeValidate);
    const [reportCodeValue, setReportCodeValue] = useState<string>('');

    const handleValidateXMLReport = () => {
        dispatch(reportValidate(reportCodeValue));
    };

    useEffect(() => {
        dispatch(fetchReportCodeListForValidate());
    }, [dispatch]);
    // Check for error in the response

    useEffect(() => {
        if (Rsuccess != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: Rsuccess,
                    variant: 'alert',
                    alert: {
                        color: 'success',
                    },
                    close: true,
                })
            );
            dispatch(toInitialState());
        }
    }, [Rsuccess])

    useEffect(() => {
        if (error != null && typeof error === 'object' && 'message' in error) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: (error as { message: string }).message,
                    variant: 'alert',
                    alert: {
                        color: 'error',
                    },
                    close: true,
                })
            );
            dispatch(toInitialState());
        }
    }, [error]);



    // // dispatch(
    // //     openSnackbar({
    // //         open: true,
    // //         message: 'Error validating XML Report',
    // //         variant: 'alert',
    // //         alert: {
    // //             color: 'error',
    // //         },
    // //         close: true,
    // //     })
    // // );

    if (isLoading) {
        return (
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
                <Box sx={{ width: '50%' }}>
                    <LinearProgress />
                </Box>
            </div>
        );
    }

    return (
        <MainCard>
            <ScrollX>
                <Grid container spacing={6}>
                    <Grid item xs={6}>
                        <Stack spacing={1.25}>
                            <InputLabel htmlFor="reportCodeId">
                                Generate XML for the Report Type <span style={{ color: 'red' }}>*</span>
                            </InputLabel>
                            <Autocomplete
                                fullWidth
                                id="reportCodeId"
                                value={selectreportCode?.find(
                                    (option) => option.reportCode === reportCodeValue
                                ) || null}
                                onChange={(event, newValue) => {
                                    setReportCodeValue(newValue?.reportCode || '');
                                }}
                                options={selectreportCode || []}
                                getOptionLabel={(item) => `${item.reportCode}`}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        placeholder="Select a Report Type"

                                    />
                                )}
                            />
                        </Stack>
                    </Grid>
                    <Grid item xs={12}>
                        <Stack spacing={1.25} direction="row" alignItems="center" sx={{ height: '100%' }}>
                            <Button
                                variant="contained"
                                onClick={handleValidateXMLReport}
                                size="small"
                                disabled={!reportCodeValue}
                                sx={{ height: '100%', ml: 1 }}
                            >
                                Validate XML Report
                            </Button>
                        </Stack>
                    </Grid>
                </Grid>
            </ScrollX>
        </MainCard>
    );
};

export default List;