/* eslint-disable prettier/prettier */
import { useEffect, useState } from 'react';

// material ui
import {
    Autocomplete,
    Box,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Grid,
    InputLabel,
    Stack,
    TextField,
    Typography
} from '@mui/material';

// project import
import LinearProgress from '@mui/material/LinearProgress';
import { PopupTransition } from 'components/@extended/Transitions';
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import useAuth from 'hooks/useAuth';
import { useDispatch, useSelector } from 'store';
import { AddActivityLog } from 'store/reducers/note-log';
import { fetchDownloadCsv, fetchIsValidatedXML, fetchReport, fetchReportCodeList, toInitialState, toResetIsActionSuccessState } from 'store/reducers/report-type'; // Updated import for report types
import { openSnackbar } from 'store/reducers/snackbar'; // Updated import for report types

// ==============================|| List ||============================== //
const options = [
    { id: 1, label: "Records Without Errors" },
    { id: 2, label: "Only Error Records" },
    { id: 3, label: "All Records" },

];
const List = () => {
    const dispatch = useDispatch();
    const { selectreportCode, error, Rsuccess, isLoading, isActionSuccess, xmlDataIsValidated } = useSelector((state) => state.reportType);
    const { user } = useAuth();
    const [reportCodeValue, setReportCodeValue] = useState<string>('');
    const [ErrorID, setErrorID] = useState<number>(1);

    const handleFetchReport = async () => {
        await dispatch(fetchReport(reportCodeValue, ErrorID));
    };

    const handleFetchValidate = async () => {
        await dispatch(fetchIsValidatedXML(reportCodeValue, ErrorID));
        setOpenDialog(true);
    };

    const [openDialog, setOpenDialog] = useState<boolean>(false);

    const handleOpenDialog = () => {
        setOpenDialog(!openDialog);
    };

    useEffect(() => {
        dispatch(fetchReportCodeList());
    }, [dispatch]);

    useEffect(() => {
        if (error != null && typeof error === 'object' && 'message' in error) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: (error as { message: string }).message,
                    variant: 'alert',
                    alert: {
                        color: 'error',
                    },
                    close: true,
                })
            );
            dispatch(toInitialState());
        }
    }, [dispatch, error]);

    useEffect(() => {
        if (Rsuccess != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: Rsuccess,
                    variant: 'alert',
                    alert: {
                        color: 'success',
                    },
                    close: true,
                })
            );
            dispatch(toInitialState());
        }
    }, [dispatch, Rsuccess]);

    useEffect(() => {
        if (isActionSuccess != null) {
            let actionId, actionName, keyValue, description;

            switch (isActionSuccess) {
                case 'VIEW':
                    actionId = 2;
                    actionName = 'VIEW';
                    keyValue = 0;
                    description = `Export XML report -${reportCodeValue}`
                    break;
                default:
                    return; // Exit early if no valid action is found
            }
            dispatch(AddActivityLog({
                actionId: actionId,
                actionName: actionName,
                branchId: user?.branchList?.[0]?.branchId ?? undefined,
                companyId: user?.companyId,
                deptId: user?.departmentList?.[0]?.departmentId ?? undefined,
                description: description,
                keyValue: reportCodeValue,
                keyValueId: keyValue,
                menuId: 14,
                menuName: "Generate XML Report",
                deptName: user?.departmentList?.[0]?.deptName ?? undefined
            }));

            dispatch(toResetIsActionSuccessState());
        }

    }, [isActionSuccess]);

    if (isLoading) {
        return (
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
                <Box sx={{ width: '50%' }}>
                    <LinearProgress />
                </Box>
            </div>
        );
    }

    return (
        <>
            <MainCard>
                <ScrollX>
                    <Grid container spacing={6}>
                        <Grid item xs={6}>
                            <Stack spacing={1.25}>
                                <InputLabel htmlFor="reportCodeId">
                                    Generate XML for the Report Type <span style={{ color: 'red' }}>*</span>
                                </InputLabel>
                                <Autocomplete
                                    fullWidth
                                    id="reportCodeId"
                                    value={
                                        selectreportCode?.find(
                                            (option) => option.reportCode === reportCodeValue
                                        ) || null
                                    }
                                    onChange={(event, newValue) => {
                                        setReportCodeValue(newValue?.reportCode || '');
                                    }}
                                    options={selectreportCode || []}
                                    getOptionLabel={(item) => `${item.reportCode}`}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            placeholder="Select a Report Type"
                                        />
                                    )}
                                />
                            </Stack>
                        </Grid>
                        <Grid item xs={6}>
                            <Stack spacing={1.25}>
                                <InputLabel htmlFor="reportTypeId">Include Error Record</InputLabel>
                                <Autocomplete
                                    fullWidth
                                    id="includeErrorRecord"
                                    value={options.find((option) => option.id === ErrorID) || null}
                                    onChange={(event, newValue) => {
                                        setErrorID(newValue?.id || 1);
                                    }}
                                    options={options}
                                    getOptionLabel={(item) => item.label}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            placeholder="Select Yes or No"
                                        />
                                    )}
                                />
                            </Stack>
                        </Grid>
                        <Grid item xs={12}>
                            <Stack spacing={1.25} direction="row" alignItems="center" sx={{ height: '100%' }}>
                                <Button
                                    variant="contained"
                                    onClick={() => { handleFetchValidate(); }}
                                    size="small"
                                    disabled={!reportCodeValue || !ErrorID}
                                    sx={{ height: '100%', ml: 1 }}
                                >
                                    Download XML
                                </Button>
                                <Button
                                    color="primary"
                                    variant="contained"
                                    size="small"
                                    onClick={ () => {
                                        dispatch(fetchDownloadCsv({ reportTypeId:ErrorID  , reportCode: reportCodeValue }));
                                    }}
                                    disabled={!reportCodeValue || !ErrorID}
                                    sx={{ height: '100%', ml: 1 }}
                                >
                                    Download CSV
                                </Button>
                            </Stack>
                        </Grid>
                    </Grid>
                </ScrollX>
            </MainCard>

            <Dialog
                maxWidth="sm"
                TransitionComponent={PopupTransition}
                keepMounted
                fullWidth
                onClose={handleOpenDialog}
                open={openDialog}
                sx={{ '& .MuiDialog-paper': { p: 0 }, transition: 'transform 225ms' }}
                aria-describedby="alert-dialog-slide-description"
            >

                <DialogTitle>Validate XML</DialogTitle>
                <DialogContent>
                    <MainCard>
                        <Typography>
                            {xmlDataIsValidated ? xmlDataIsValidated?.validationMessage : 'Null'}
                        </Typography>
                    </MainCard>
                </DialogContent>

                <DialogActions>
                    <Button onClick={handleOpenDialog}>Cancel</Button>
                    <Button type="submit" color="primary" variant="contained" onClick={() => { handleFetchReport(); handleOpenDialog(); }} >
                        Continue
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
};

export default List;
