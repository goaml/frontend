import React, { useState } from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  TextField,
  FormControlLabel,
  Checkbox
} from '@mui/material';

interface FilterDialogProps {
  open: boolean;
  onClose: () => void;
  onApplyFilters: (filters: { [key: string]: string }) => void;
}

const FilterDialog: React.FC<FilterDialogProps> = ({ open, onClose, onApplyFilters }) => {
  const [filters, setFilters] = useState<{ [key: string]: string }>({});
  const [selectedColumns, setSelectedColumns] = useState<{ [key: string]: boolean }>({
    rSubmissionDate: false,
    rReportId: false,
  });

  const handleInputChange = (columnId: string) => (event: React.ChangeEvent<HTMLInputElement>) => {
    const newFilters = { ...filters, [columnId]: event.target.value };
    setFilters(newFilters);
  };

  const handleApplyFilters = () => {
    onApplyFilters(filters);
    onClose();
  };

  const handleClearFilters = () => {
    setFilters({});
    setSelectedColumns({
      rSubmissionDate: false,
      rReportId: false,
    });
    onApplyFilters({});
    onClose();
  };

  const handleColumnSelectionChange = (columnId: string) => (event: React.ChangeEvent<HTMLInputElement>) => {
    const newSelectedColumns = { ...selectedColumns, [columnId]: event.target.checked };
    setSelectedColumns(newSelectedColumns);
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>Filter Columns</DialogTitle>
      <DialogContent>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={6}>
            <FormControlLabel
              control={
                <Checkbox
                  checked={selectedColumns.rSubmissionDate}
                  onChange={handleColumnSelectionChange('rSubmissionDate')}
                  name="rSubmissionDate"
                  color="primary"
                />
              }
              label="Report Submission Date"
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormControlLabel
              control={
                <Checkbox
                  checked={selectedColumns.rReportId}
                  onChange={handleColumnSelectionChange('rReportId')}
                  name="rReportId"
                  color="primary"
                />
              }
              label="Report Report ID"
            />
          </Grid>

          {selectedColumns.rSubmissionDate && (
            <Grid item xs={12} sm={6}>
              <TextField
                label="Report Submission Date"
                value={filters.rSubmissionDate || ''}
                onChange={handleInputChange('rSubmissionDate')}
                fullWidth
              />
            </Grid>
          )}

          {selectedColumns.rReportId && (
            <Grid item xs={12} sm={6}>
              <TextField
                label="Report Report ID"
                value={filters.rReportId || ''}
                onChange={handleInputChange('rReportId')}
                fullWidth
              />
            </Grid>
          )}

        </Grid>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose}>Cancel</Button>
        <Button onClick={handleClearFilters}>Clear Filters</Button>
        <Button onClick={handleApplyFilters} color="primary" variant="contained">Apply</Button>
      </DialogActions>
    </Dialog>
  );
};

export default FilterDialog;
