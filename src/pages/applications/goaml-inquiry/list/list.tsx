import { MouseEvent, useMemo, useState } from 'react';

// material-ui
import {
  Button,
  Dialog,
  IconButton,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,

  Tooltip,
  useMediaQuery,
  useTheme,
} from '@mui/material';

// third-party
import { Cell, Column, HeaderGroup, Row, useExpanded, useFilters, useGlobalFilter, usePagination, useRowSelect, useSortBy, useTable } from 'react-table';


// project import
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import { CSVExport, EmptyTable, TablePagination,TableRowSelection } from 'components/third-party/ReactTable';

// utils
import {
  
  GlobalFilter,
  renderFilterTypes
} from 'utils/react-table';

// sections

// assets
import {   EyeTwoTone, PlusOutlined,FilterTwoTone } from '@ant-design/icons';

//types 
import { ReactTableProps, dataProps, goAMLInquiryProps } from './types/types';
import { PopupTransition } from 'components/@extended/Transitions';
import AddEditGOAMLInquiry from 'sections/applications/goaml-Inquiry/AddEditGoAMLInquiry';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import FilterDialog from './FilterDialog';
import { data } from 'data/org-chart';


// ==============================|| REACT TABLE ||============================== //


function ReactTable({ columns, data, handleAddEdit }: ReactTableProps) {
  const filterTypes = useMemo(() => renderFilterTypes, []);
  const theme = useTheme();
  const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));

  const exampleData: dataProps[] = [
    {
      rReportId: 1,
      rRentityId: 100,
      rRentityBranch: 'Main Branch',
      rSubmissionCode: 'SUB123',
      rReportCode: 'REP456',
      rEntityReference: 'ENT789',
      rFiuRefNumber: 'FIU001122',
      rSubmissionDate: '2023-06-15T00:00:00Z',
      rCurrencyCodeLocal: 840,
      rReportingPerson: 'John Doe',
      rLocation: 'New York',
      rReason: 'Regular Transaction',
      rAction: 'Approved',
      rStatusId: 2,
      rIsActive: true,
      rSystemDate: '2023-06-16T00:00:00Z',
      rUserId: 'USR001',
      tTransactionId: 200,
      tTransactionNumber: 'TRX123456',
      tInternalRefNumber: 'INTREF001',
      tTransactionLocation: 'Online',
      tTransactionDescription: 'Payment for services',
      tDateTransaction: '2023-06-14T00:00:00Z',
      tTeller: 'Jane Smith',
      tAuthorized: 'Manager',
      tLateDeposit: false,
      tDatePosting: '2023-06-16T00:00:00Z',
      tValueDate: '2023-06-14T00:00:00Z',
      tConductionTypeId: 1, // TransmodeCode
      tTransmodeComment: 'Bank Transfer',
      tAmountLocal: 5000.75, // Decimal
      tGoodsServices: 'Consulting Services',
      tComments: 'No issues',
      tStatusId: 2,
      tIsActive: true,
      tReportId: 1,
  }
  
    // Add more example data as needed
  ];
  

  const [filteredData, setFilteredData] = useState<dataProps[]>(exampleData);
  const [filterDialogOpen, setFilterDialogOpen] = useState(false);

      const handleFilterDialogOpen = () => {
        setFilterDialogOpen(true);
      };
    
      const handleFilterDialogClose = () => {
        setFilterDialogOpen(false);
      };
    
    
      const handleApplyFilters = (filters: { [key: string]: string }) => {
        const filtered = exampleData.filter((row) => {
          return Object.keys(filters).every((column) => {
            // Ensure column is keyof dataProps for type safety
            const rowValue = row[column as keyof dataProps] as string | undefined;
            if (!filters[column]) return true;
            return rowValue && rowValue.toLowerCase().includes(filters[column].toLowerCase());
          });
        });
        setFilteredData(filtered);
        setFilterDialogOpen(false);
      };
      
    const {
      getTableProps,
      getTableBodyProps,
      headerGroups,
      prepareRow,
      rows,
      page,
      gotoPage,
      setPageSize,
      state: { globalFilter, selectedRowIds, pageIndex, pageSize },
      preGlobalFilteredRows,
      setGlobalFilter,
    
  } = useTable(
      {
          columns,
          data:filteredData,
          filterTypes,
          initialState: { pageIndex: 0, pageSize: 10, hiddenColumns: ['avatar']}
      },
      useGlobalFilter,
      useFilters,
      useSortBy,
      useExpanded,
      usePagination,
      useRowSelect
  );
      
  


  return (
    <>
    <TableRowSelection selected={Object.keys(selectedRowIds).length} />
      <Stack direction={matchDownSM ? 'column' : 'row'} spacing={2} justifyContent="space-between" sx={{ padding: 2 }}>
        <GlobalFilter preGlobalFilteredRows={preGlobalFilteredRows} globalFilter={globalFilter} setGlobalFilter={setGlobalFilter} />
        <Stack direction={matchDownSM ? 'column' : 'row'} alignItems="center" spacing={1}>
          <LocalizationProvider dateAdapter={AdapterDateFns}>

          </LocalizationProvider>
          <CSVExport data={rows.map((d: Row) => d.original)} filename={'filtering-table.csv'} />
          <Button variant="contained" startIcon={<PlusOutlined />} onClick={handleAddEdit}>
            Add New
          </Button>
          <Tooltip title="Filter Columns">
                    <IconButton onClick={handleFilterDialogOpen}>
                        <FilterTwoTone />
                    </IconButton>
                </Tooltip>
        </Stack>
      </Stack>
      
      <Table {...getTableProps()}>
        <TableHead sx={{ borderTopWidth: 2 }}>
          {headerGroups.map((headerGroup) => (
            <TableRow {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column: HeaderGroup) => (
                <TableCell {...column.getHeaderProps([{ className: column.className }])}>
                  {column.render('Header')}
                </TableCell>
              ))}
            </TableRow>
          ))}
        </TableHead>
        <TableBody {...getTableBodyProps()}>
          {page.length > 0 ? (
            page.map((row, i) => {
              prepareRow(row);
              return (
                <TableRow {...row.getRowProps()}>
                  {row.cells.map((cell: Cell) => (
                    <TableCell {...cell.getCellProps([{ className: cell.column.className }])}>
                      {cell.render('Cell')}
                    </TableCell>
                  ))}
                </TableRow>
              );
            })
          ) : (
            <EmptyTable msg="No Data" colSpan={12} />
          )}
          <TableRow>
            <TableCell sx={{ p: 2 }} colSpan={12}>
              <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
      <FilterDialog
                    open={filterDialogOpen}
                    onClose={handleFilterDialogClose}
                    onApplyFilters={handleApplyFilters}
                />
    </>
  );
}

// ==============================|| List ||============================== //

const List = () => {
  const theme = useTheme();
  const columns = useMemo(
    () =>
      [
        {
          Header: '#',
          accessor: 'id',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            if (row.id === undefined || row.id === null || row.id === '') {
              return <>-</>
            }
            if (typeof row.id === 'string') {
              return <>{(parseInt(row.id) + 1).toString()}</>;
            }
            if (typeof row.id === 'number') {
              return <>{row.id + 1}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          Header: 'Report Report ID',
          accessor: 'rReportId',
          
        },
        {
          Header: 'Report Entity ID',
          accessor: 'rRentityId',
          
        },
        {
          Header: 'Report Entity Branch',
          accessor: 'rRentityBranch',
          
        },
        {
          Header: 'Report Submission Code',
          accessor: 'rSubmissionCode',
          
        },
        {
          Header: 'Report Report Code',
          accessor: 'rReportCode',
          
        },
        {
          Header: 'Report Entity Reference',
          accessor: 'rEntityReference',
          
        },
        {
          Header: 'Report FIU Ref Number',
          accessor: 'rFiuRefNumber',
          
        },
        {
          Header: 'Report Submission Date',
          accessor: 'rSubmissionDate',
          
        },
        {
          Header: 'Report Currency Code Local',
          accessor: 'rCurrencyCodeLocal',
          
        },
        {
          Header: 'Report Reporting Person',
          accessor: 'rReportingPerson',
          
        },
        {
          Header: 'Report Location',
          accessor: 'rLocation',
          
        },
        {
          Header: 'Report Reason',
          accessor: 'rReason',
          
        },
        {
          Header: 'Report Action',
          accessor: 'rAction',
          
        },
        {
          Header: 'Report Status ID',
          accessor: 'rStatusId',
          
        },
        {
          Header: 'Report Is Active',
          accessor: 'rIsActive',
          
        },
        {
          Header: 'Report System Date',
          accessor: 'rSystemDate',
          
        },
        {
          Header: 'Report User ID',
          accessor: 'rUserId',
          
        },
        {
          Header: 'Transaction ID',
          accessor: 'tTransactionId',
          
        },
        {
          Header: 'Transaction Number',
          accessor: 'tTransactionNumber',
          
        },
        {
          Header: 'TRX Internal Ref Number',
          accessor: 'tInternalRefNumber',
          
        },
        {
          Header: 'TRX Transaction Location',
          accessor: 'tTransactionLocation',
          
        },
        {
          Header: 'TRX Transaction Description',
          accessor: 'tTransactionDescription',
          
        },
        {
          Header: 'TRX Date of Transaction',
          accessor: 'tDateTransaction',
          
        },
        {
          Header: 'TRX Teller',
          accessor: 'tTeller',
          
        },
        {
          Header: 'TRX Authorized By',
          accessor: 'tAuthorized',

        },
        {
          Header: 'TRX Late Deposit',
          accessor: 'tLateDeposit',
          
        },
        {
          Header: 'TRX Date of Posting',
          accessor: 'tDatePosting',
          
        },
        {
          Header: 'TRX Value Date',
          accessor: 'tValueDate',
          
        },
        {
          Header: 'TRX Conduction Type ID',
          accessor: 'tConductionTypeId',
          
        },
        {
          Header: 'TRX Transmode Comment',
          accessor: 'tTransmodeComment',
          
        },
        {
          Header: 'TRX Amount Local',
          accessor: 'tAmountLocal',
          
        },
        {
          Header: 'TRX Goods/Services',
          accessor: 'tGoodsServices',
          
        },
        {
          Header: 'TRX Comments',
          accessor: 'tComments',
          
        },
        {
          Header: 'TRX Status ID',
          accessor: 'tStatusId',
          
        },
        {
          Header: 'TRX Is Active',
          accessor: 'tIsActive',
          
        },
        {
          Header: 'TRX Report ID',
          accessor: 'tReportId',
          
        },
        {
          id: "actions",
          Header: 'Actions',
          accessor: 'actions',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            const data: dataProps = row.original
            return (
              <>
                <Stack direction="row" alignItems="center" justifyContent="center" spacing={0}>
                  <Tooltip title="Edit">
                    <IconButton
                      color="primary"
                      onClick={(e: MouseEvent<HTMLButtonElement>) => {
                        e.stopPropagation();
                        handleAddEdit()
                        setgoAMLInquiryType({
                          ...data
                        })
                      }}
                    >
                      <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                    </IconButton>
                  </Tooltip>
                </Stack>
              </>
            )
          }
        }
      ] as Column[],
    []
  );

  //dialog model 
  const [addEdit, setAddEdit] = useState<boolean>(false);
  const [goAMLInquiryTypeCode, setgoAMLInquiryType] = useState<goAMLInquiryProps>();

  const handleAddEdit = () => {
    setAddEdit(!addEdit);
    if (goAMLInquiryTypeCode && !addEdit) setgoAMLInquiryType(undefined);
  };

  return (
    <>
      <MainCard content={false}>
        <ScrollX>
          <ReactTable columns={columns} getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()} data={data} handleAddEdit={handleAddEdit}  />
        </ScrollX>
        {/* add / edit submissionCode dialog */}
        <Dialog
          maxWidth="sm"
          TransitionComponent={PopupTransition}
          keepMounted
          fullWidth
          onClose={handleAddEdit}
          open={addEdit}
          sx={{ '& .MuiDialog-paper': { p: 0 }, transition: 'transform 225ms' }}
          aria-describedby="alert-dialog-slide-description"
        >
          <AddEditGOAMLInquiry goAMLInquiry={goAMLInquiryTypeCode} onCancel={handleAddEdit} />
        </Dialog>
      </MainCard>
    </>
  );
}

export default List;
