// import { Dispatch, SetStateAction } from 'react';
import { Column, HeaderGroup } from 'react-table';

export interface dataProps {
  rReportId?: number;
  rRentityId?: number;
  rRentityBranch?: string;
  rSubmissionCode?: string;
  rReportCode?: string;
  rEntityReference?: string;
  rFiuRefNumber?: string;
  rSubmissionDate?: string;
  rCurrencyCodeLocal?: number;
  rReportingPerson?: string;
  rLocation?: string;
  rReason?: string;
  rAction?: string;
  rStatusId?: number;
  rIsActive?: boolean;
  rSystemDate?: string;
  rUserId?: string;
  tTransactionId?: number;
  tTransactionNumber?: string;
  tInternalRefNumber?: string;
  tTransactionLocation?: string;
  tTransactionDescription?: string;
  tDateTransaction?: string;
  tTeller?: string;
  tAuthorized?: string;
  tLateDeposit?: boolean;
  tDatePosting?: string;
  tValueDate?: string;
  tConductionTypeId?: number; // TransmodeCode
  tTransmodeComment?: string;
  tAmountLocal?: number; // Decimal
  tGoodsServices?: string;
  tComments?: string;
  tStatusId?: number;
  tIsActive?: boolean;
  tReportId?: number;
  [key: string]: any;
}


export interface ReactTableProps {
  columns: Column[]
  data: dataProps[]
  handleAddEdit: () => void
  getHeaderProps: (column: HeaderGroup) => {};
  //tableParams: TableParamsType
}

export interface goAMLInquiryProps  { 
  rReportId?: number;
  rRentityId?: number;
  rRentityBranch?: string;
  rSubmissionCode?: string;
  rReportCode?: string;
  rEntityReference?: string;
  rFiuRefNumber?: string;
  rSubmissionDate?: string;
  rCurrencyCodeLocal?: number;
  rReportingPerson?: string;
  rLocation?: string;
  rReason?: string;
  rAction?: string;
  rStatusId?: number;
  rIsActive?: boolean;
  rSystemDate?: string;
  rUserId?: string;
  tTransactionId?: number;
  tTransactionNumber?: string;
  tInternalRefNumber?: string;
  tTransactionLocation?: string;
  tTransactionDescription?: string;
  tDateTransaction?: string;
  tTeller?: string;
  tAuthorized?: string;
  tLateDeposit?: boolean;
  tDatePosting?: string;
  tValueDate?: string;
  tConductionTypeId?: number; // TransmodeCode
  tTransmodeComment?: string;
  tAmountLocal?: number; // Decimal
  tGoodsServices?: string;
  tComments?: string;
  tStatusId?: number;
  tIsActive?: boolean;
  tReportId?: number;
}

// export interface TableParamsType {
//   page: number;
//   setPage: Dispatch<SetStateAction<number>>;
//   perPage: number;
//   setPerPage: Dispatch<SetStateAction<number>>;
//   direction: "asc" | "desc";
//   setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
//   sort: string;
//   setSort: Dispatch<SetStateAction<string>>;

// }
