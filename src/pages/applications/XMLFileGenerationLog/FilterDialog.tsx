import {
  Autocomplete,
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  FormControlLabel,
  Grid,
  InputLabel,
  Stack,
  TextField
} from '@mui/material';
import React, { useEffect, useState } from 'react';
import { format } from 'date-fns';
import { useFormik } from 'formik'; // Import Formik for form handling
import * as Yup from 'yup'; // Import Yup for validation
import { ReportCodeType } from 'types/report-type';
interface FilterValues {
  reportType?: string;
  processYear?: string;
  executeTimeFrom?: string;
  executeTimeTo?: string;
  processMonth?: string;
}

interface FilterDialogProps {
  open: boolean;
  onClose: () => void;
  onApplyFilters: (filters: { [key: string]: string }) => void;
  onReset: () => void;
  initialFilters: FilterValues;
  reportType: ReportCodeType[] | null;
}

// Define a simplified Yup validation schema
const validationSchema = Yup.object().shape({
  executeTimeFrom: Yup.date().nullable(),
  executeTimeTo: Yup.date()
    .nullable()
    .min(Yup.ref('executeTimeFrom'), 'End date can\'t be before start date'),
  processYear: Yup.number().nullable(),
  processMonth: Yup.string(),
  reportType: Yup.string(),
});

const FilterDialog: React.FC<FilterDialogProps> = ({
  open,
  onClose,
  onApplyFilters,
  onReset,
  initialFilters,
  reportType
}) => {
  const [selectedColumns, setSelectedColumns] = useState<string[]>([]);
  // Track multiple selected columns
  useEffect(() => {
    const initialSelectedColumns: string[] = [];
    if (initialFilters.executeTimeFrom || initialFilters.executeTimeTo) initialSelectedColumns.push('Date');
    if (initialFilters.processYear) initialSelectedColumns.push('processYear');
    if (initialFilters.processMonth) initialSelectedColumns.push('processMonth');
    if (initialFilters.reportType) initialSelectedColumns.push('reportType');

    setSelectedColumns(initialSelectedColumns);
  }, [initialFilters]);

  const formik = useFormik({
    initialValues: {
      executeTimeFrom: initialFilters?.executeTimeFrom || '',
      executeTimeTo: initialFilters?.executeTimeTo || '',
      processYear: initialFilters?.processYear,
      processMonth: initialFilters?.processMonth,
      reportType: initialFilters?.reportType
    },
    validationSchema,
    onSubmit: (values) => {
      let errors: { [key: string]: string } = {};

      // Validation for each selected field

      if (selectedColumns.includes('executeTime')) {
        if (!values.executeTimeFrom) errors.executeTimeFrom = 'Start execute time is required';
        if (!values.executeTimeTo) errors.executeTimeTo = 'End execute time is required'
      }

      if (selectedColumns.includes('processYear') && !values.processYear) {
        errors.processYear = 'Year is required';
      }


      if (selectedColumns.includes('processMonth') && !values.processMonth) {
        errors.processMonth = 'Month is required';
      }

      if (selectedColumns.includes('reportType') && !values.reportType) {
        errors.reportType = 'Report Type is required';
      }

      // If errors exist, set them and stop the form submission
      if (Object.keys(errors).length > 0) {
        formik.setErrors(errors);
        return;
      }

      const filtersToSend: any = { ...values };


      // Send the selected filters
      onApplyFilters(filtersToSend);
      onClose();
    },
  });

  // Handle the selection of multiple columns
  const handleColumnSelectionChange = (logId: string) => () => {
    setSelectedColumns((prevSelected) => {
      const isSelected = prevSelected.includes(logId);
      const updatedSelection = isSelected
        ? prevSelected.filter((col) => col !== logId)
        : [...prevSelected, logId];

      // Reset form fields when a column is deselected
      if (isSelected) {
        switch (logId) {
          case 'executeTime':
            formik.setFieldValue('executeTimeFrom', '');
            formik.setFieldValue('executeTimeTo', '');
            break;
          case 'processYear':
            formik.setFieldValue('processYear', '');
            break;
          case 'processMonth':
            formik.setFieldValue('processMonth', '');
            break;
          case 'reportType':
            formik.setFieldValue('reportType', '');
            break;
          default:
            break;
        }
      }

      return updatedSelection;
    });
  };

  const handleReset = () => {
    formik.resetForm();
    setSelectedColumns([]); // Clear selected columns
    onReset();
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <form onSubmit={formik.handleSubmit}>
        <DialogTitle>Filter Columns</DialogTitle>
        <Divider />
        <DialogContent>
          <Grid container spacing={2}>
            {/*  */}
            <Grid item xs={12} sm={4}>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={selectedColumns.includes('executeTime')}
                    onChange={handleColumnSelectionChange('executeTime')}
                    color="primary"
                  />
                }
                label="Execute Time"
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={selectedColumns.includes('processYear')}
                    onChange={handleColumnSelectionChange('processYear')}
                    color="primary"
                  />
                }
                label="Year"
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={selectedColumns.includes('processMonth')}
                    onChange={handleColumnSelectionChange('processMonth')}
                    color="primary"
                  />
                }
                label="Month"
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={selectedColumns.includes('reportType')}
                    onChange={handleColumnSelectionChange('reportType')}
                    color="primary"
                  />
                }
                label="Report Type"
              />
            </Grid>

            <Grid container spacing={2} mt={2}>
              {/* executeTime */}
              {selectedColumns.includes('executeTime') && (
                <>
                  <Grid item xs={12} sm={6}>
                    <Stack spacing={1.25}>
                      <InputLabel htmlFor="executeTimeFrom">
                        Execute Time From <span style={{ color: 'red' }}>*</span>
                      </InputLabel>
                      <TextField
                        type="datetime-local"
                        value={
                          formik.values.executeTimeFrom && !isNaN(new Date(formik.values.executeTimeFrom).getTime())
                            ? format(new Date(formik.values.executeTimeFrom), "yyyy-MM-dd'T'HH:mm:ss")
                            : format(new Date().setHours(0, 0, 0, 0), "yyyy-MM-dd'T'HH:mm:ss") // Defaults to today at 00:00 AM
                        }
                        onChange={(event) => {
                          const newValue = new Date(event.target.value);
                          if (!isNaN(newValue.getTime())) {
                            const formattedValue = format(newValue, "yyyy-MM-dd'T'HH:mm:ss");
                            formik.setFieldValue('executeTimeFrom', formattedValue);
                          } else {
                            console.error('Invalid date selected');
                          }
                        }}
                        error={formik.touched.executeTimeFrom && Boolean(formik.errors.executeTimeFrom)}
                        helperText={formik.touched.executeTimeFrom && formik.errors.executeTimeFrom}
                        fullWidth
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />
                    </Stack>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Stack spacing={1.25}>
                      <InputLabel htmlFor="executeTimeTo">
                        Execute Time To <span style={{ color: 'red' }}>*</span>
                      </InputLabel>
                      <TextField
                        type="datetime-local"
                        value={
                          formik.values.executeTimeTo && !isNaN(new Date(formik.values.executeTimeTo).getTime())
                            ? format(new Date(formik.values.executeTimeTo), "yyyy-MM-dd'T'HH:mm:ss")
                            : format(new Date().setHours(23, 59, 59, 0), "yyyy-MM-dd'T'HH:mm:ss") // Defaults to today at 11:59 PM
                        }
                        onChange={(event) => {
                          const newValue = new Date(event.target.value);
                          if (!isNaN(newValue.getTime())) {
                            const formattedValue = format(newValue, "yyyy-MM-dd'T'HH:mm:ss");
                            formik.setFieldValue('executeTimeTo', formattedValue);
                          } else {
                            console.error('Invalid date selected');
                          }
                        }}
                        error={formik.touched.executeTimeTo && Boolean(formik.errors.executeTimeTo)}
                        helperText={formik.touched.executeTimeTo && formik.errors.executeTimeTo}
                        fullWidth
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />
                    </Stack>
                  </Grid>
                </>
              )}

              {/* reportType */}
              {selectedColumns.includes('reportType') && (
                <Grid item xs={6}>
                  <Stack spacing={1.25}>
                    <InputLabel htmlFor="reportCodeId">
                      Report Type <span style={{ color: 'red' }}>*</span>
                    </InputLabel>
                    <Autocomplete
                      fullWidth
                      id="reportCodeId"
                      value={
                        reportType?.find(
                          (option) => option.reportCode === formik.values.reportType!
                        ) || null
                      }
                      onChange={(event, newValue) => {
                        formik.setFieldValue('reportType', newValue?.reportCode || '');
                      }}
                      options={reportType || []}
                      getOptionLabel={(item) => `${item.reportCode}`}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          placeholder="Select a Report Type"
                        />
                      )}
                    />
                  </Stack>
                </Grid>
              )}

              {/* process year */}
              {selectedColumns.includes('processYear') && (
                <Grid item xs={12} sm={6}>
                  <Stack spacing={1.25}>
                    <InputLabel htmlFor="processYear">
                      Process Year<span style={{ color: 'red' }}>*</span>
                    </InputLabel>
                    <TextField
                      {...formik.getFieldProps('processYear')}
                      error={formik.touched.processYear && Boolean(formik.errors.processYear)}
                      helperText={formik.touched.processYear && formik.errors.processYear}
                      fullWidth
                    />
                  </Stack>
                </Grid>
              )}

              {/* process month */}
              {selectedColumns.includes('processMonth') && (
                <Grid item xs={12} sm={6}>
                  <Stack spacing={1.25}>
                    <InputLabel htmlFor="processMonth">
                      Process Month <span style={{ color: 'red' }}>*</span>
                    </InputLabel>
                    <TextField
                      {...formik.getFieldProps('processMonth')}
                      error={formik.touched.processMonth && Boolean(formik.errors.processMonth)}
                      helperText={formik.touched.processMonth && formik.errors.processMonth}
                      fullWidth
                    />
                  </Stack>
                </Grid>
              )}
            </Grid>
          </Grid>
        </DialogContent>


        <DialogActions>
          <Button onClick={onClose}>Cancel</Button>
          <Button onClick={handleReset} color="secondary">
            Clear Filters
          </Button>
          <Button disabled={!selectedColumns} type="submit" color="primary" variant="contained">
            Apply
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

export default FilterDialog;
