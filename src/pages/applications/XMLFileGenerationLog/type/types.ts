//import { Dispatch, SetStateAction } from 'react';
import { Dispatch, SetStateAction } from 'react';
import { Column, HeaderGroup } from 'react-table';
import { ReportCodeType } from 'types/report-type';


export interface dataProps {
    transactionDate?: string;
    transactionId?: number;
    transactionType?: string;
    transactionSubType?: string;
    part_tran_type?: string;
    acid?: string;
    gl_sub_head_code?: string;
    cust_id?: string;
    module_id?: string;
    crncy_code?: string;
    branch_code?: string;
    bank_code?: string;
    rptCodeName?: string;
    [key: string]: any;
    solId?: string;
}

export interface ReactTableProps {
    columns: Column[];
    data: dataProps[];
    handleAddEdit: () => void;
    getHeaderProps: (column: HeaderGroup) => {};
    tableParams: TableParamsType;
    pagination: any;
    reportType:ReportCodeType[] |  null;

    
}
  
export interface TableHeaderProps {
    headerGroups: HeaderGroup[];
}

export interface transactionProps {
    transactionId: number;
    transactionType: string;
    transactionSubType: string;
    part_tran_type: string;
    acid: string;
    gl_sub_head_code: string;
    cust_id: string;
    module_id: string;
    crncy_code: string;
    branch_code: string;
    bank_code: string;
    rptCodeName: string;
}

export interface TableParamsType {
    page: number;
    setPage: Dispatch<SetStateAction<number>>;
    perPage: number;
    setPerPage: Dispatch<SetStateAction<number>>;
    direction: "asc" | "desc";
    setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
    sort: string;
    setSort: Dispatch<SetStateAction<string>>;
    search: string;
    setSearch: Dispatch<SetStateAction<string>>;
    logId?: number
    setLogId : Dispatch<SetStateAction<number | undefined>>;
    processMonth?: string
    setProcessMonth: Dispatch<SetStateAction<string>>;
    processYear?: string
    setProcessYear: Dispatch<SetStateAction<string>>;
    recordCount?: number
    setRecordCount : Dispatch<SetStateAction<number | undefined>>;
    reportType?: string
    setReportType: Dispatch<SetStateAction<string>>;
    status?: string
    setStatus: Dispatch<SetStateAction<string>>;
    executeTimeFrom: string;
    setExecuteTimeFrom: Dispatch<SetStateAction<string>>;
    executeTimeTo: string;
    setExecuteTimeTo: Dispatch<SetStateAction<string>>;
}
