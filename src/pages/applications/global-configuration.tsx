
// material-ui

import { AppstoreOutlined, ControlOutlined } from "@ant-design/icons";
import { Box, Button, Card, CardContent, Grid, Icon, Typography } from "@mui/material";
import { useNavigate } from "react-router";

// third-party

// project import

// assets

//types 
type GlobalConfigurationWidgetProps = {
    widgetIcon: any
    widgetTitle: string
    widgetOnClick: () => void
}

const GlobalConfigurationWidget = ({ widgetIcon, widgetTitle, widgetOnClick }: GlobalConfigurationWidgetProps) => {

    return (
        <>
            <Button fullWidth onClick={() => { widgetOnClick() }} sx={{ textDecoration: 'none', color: 'inherit' }}>
                <Card elevation={3} sx={{ textAlign: 'center', width: '100%' }} >
                    <CardContent>
                        <Box>
                            <Icon sx={{ height: '1.5em', fontSize: '45px' }}>
                                {widgetIcon}
                            </Icon>
                        </Box>
                        <Typography variant="h6" gutterBottom>
                            {widgetTitle}
                        </Typography>
                    </CardContent>
                </Card >
            </Button>
        </>
    )
}

// ==============================|| GlobalConfiguration ||============================== //

const GlobalConfiguration = () => {
    let navigate = useNavigate();

    return (
        <>
            <Grid container spacing={3}>
                <Grid item md={3} sm={6} xs={12}>
                    <GlobalConfigurationWidget
                        widgetIcon={<ControlOutlined />}
                        widgetTitle={"Application Settings"}
                        widgetOnClick={() => { navigate('/applications/global-configuration/application-settings') }}
                    />
                </Grid>
                <Grid item md={3} sm={6} xs={12}>
                    <GlobalConfigurationWidget
                        widgetIcon={<AppstoreOutlined />}
                        widgetTitle={"Theme Settings"}
                        widgetOnClick={() => { navigate('/applications/global-configuration/theme-settings') }}
                    />
                </Grid>
            </Grid>
        </>
    );
}

export default GlobalConfiguration;
