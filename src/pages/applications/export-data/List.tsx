/* eslint-disable prettier/prettier */
//import { useState, useEffect } from 'react';

// material ui
import {
    Button,
    Stack,
    //TextField,
    // InputLabel,
    Grid,
    Box,
    //CircularProgress
    //Autocomplete
    //useTheme,
} from '@mui/material';

// project import
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import { useDispatch, useSelector, } from 'store';
// import { fetchReport, toInitialState, fetchReportTypeList } from 'store/reducers/report-type'; // Updated import for report types
// import { openSnackbar } from 'store/reducers/snackbar'; // Updated import for report types
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import React, { useEffect } from 'react';
import dayjs, { Dayjs } from 'dayjs';
import { addTransactionData, toInitialState, toResetIsActionSuccessState } from 'store/reducers/goaml-trx-code';
import { openSnackbar } from 'store/reducers/snackbar';
import LinearProgress from '@mui/material/LinearProgress';
import useAuth from 'hooks/useAuth';
import { AddActivityLog } from 'store/reducers/note-log';
import { exportData } from 'types/goaml-trx-code';
// ==============================|| List ||============================== //


const List = () => {
    //const theme = useTheme();
    const dispatch = useDispatch();
 
    const { error, trsuccess, isLoading, isActionSuccess } = useSelector((state) => state.transaction);
    const { user } = useAuth();
    const [startDate, setStartDate] = React.useState<Dayjs | null>(dayjs());
    const [enddate, setEndDate] = React.useState<Dayjs | null>(dayjs());


    const handleExportData = () => {
        const queryParams: exportData = {
            startDate: startDate ? startDate.format('YYYY-MM-DD') : '',
            endDate: enddate ? enddate.format('YYYY-MM-DD') : '',
            executeType: 'run',
        };
        dispatch(addTransactionData(queryParams));
    };


    useEffect(() => {
        if (error != null && typeof error === 'object' && 'message' in error) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: (error as { message: string }).message,
                    variant: 'alert',
                    alert: {
                        color: 'error',
                    },
                    close: true,
                })
            );
            dispatch(toInitialState());
        }
    }, [dispatch, error]);

    // useEffect(() => {
    //     if (Rsuccess != null) {
    //         dispatch(
    //             openSnackbar({
    //                 open: true,
    //                 message: Rsuccess,
    //                 variant: 'alert',
    //                 alert: {
    //                     color: 'success',
    //                 },
    //                 close: true,
    //             })
    //         );
    //         dispatch(toInitialState());
    //     }
    // }, [dispatch, Rsuccess]);

    // Handle success
    React.useEffect(() => {
        if (trsuccess != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: trsuccess,
                    variant: 'alert',
                    alert: {
                        color: 'success'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [trsuccess, dispatch]);

    useEffect(() => {
        if (isActionSuccess != null) {
            let actionId, actionName, keyValue, description;

            switch (isActionSuccess) {
                case 'CREATE':
                    actionId = 1;
                    actionName = 'CREATE';
                    keyValue = 0;
                    description = `Export Data :- ${startDate}-${enddate}`
                    break;


                default:
                    return; // Exit early if no valid action is found
            }
            dispatch(AddActivityLog({
                actionId: actionId,
                actionName: actionName,
                branchId: user?.branchList?.[0]?.branchId ?? undefined,
                companyId: user?.companyId,
                deptId: user?.departmentList?.[0]?.departmentId ?? undefined,
                description: description,
                keyValue: "",
                keyValueId: keyValue,
                menuId: 15,
                menuName: "Generate GoAML Data",
                deptName: user?.departmentList?.[0]?.deptName ?? undefined
            }));

            dispatch(toResetIsActionSuccessState());
        }

    }, [isActionSuccess]);



    if (isLoading) {
        return (
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
                <Box sx={{ width: '50%' }}>
                    <LinearProgress />
                </Box>
            </div>
        );
    }

    return (
        <>
            <MainCard>
                <ScrollX>
                    <Grid container spacing={3}> 
                        <Grid item xs={4}>
                            <Stack spacing={1}>
                                <LocalizationProvider dateAdapter={AdapterDayjs}>
                                    <DemoContainer components={['DatePicker']}>
                                        <DatePicker
                                            label="Start Date"
                                            value={startDate}
                                            onChange={(newdate) => setStartDate(newdate)}
                                        />
                                    </DemoContainer>
                                </LocalizationProvider>
                            </Stack>
                        </Grid>
                        <Grid item xs={4}>
                            <Stack spacing={1}>
                                <LocalizationProvider dateAdapter={AdapterDayjs}>
                                    <DemoContainer components={['DatePicker']}>
                                        <DatePicker
                                            label="End Date"
                                            value={enddate}
                                            onChange={(newdate) => setEndDate(newdate)}
                                        />
                                    </DemoContainer>
                                </LocalizationProvider>
                            </Stack>
                        </Grid>
                        <Grid item xs={12}>
                            <Stack spacing={1.25} direction="row" alignItems="center" sx={{ height: '100%' }}>
                                <Button
                                    variant="contained"
                                    onClick={handleExportData}
                                    size="small"
                                    sx={{ height: '100%', ml: 1 }} // Adjust margin or padding as needed
                                >
                                    Generate Data
                                </Button>
                            </Stack>
                        </Grid>
                    </Grid>
                </ScrollX>
            </MainCard>
        </>
    );
};

export default List;
