import { Fragment, useEffect, useMemo, useState } from 'react';

// material-ui
import {
    alpha,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Typography,
    useMediaQuery,
    useTheme
} from '@mui/material';

// third-party
import { Cell, Column, HeaderGroup, Row, useFilters, useGlobalFilter, usePagination, useSortBy, useTable } from 'react-table';

// project import
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import { HeaderSort, TablePagination } from 'components/third-party/ReactTable';

import Dot from 'components/@extended/Dot';
import { useDispatch, useSelector } from 'store';
import { fetchNoteLogs, toInitialState } from 'store/reducers/note-log';
import { openSnackbar } from 'store/reducers/snackbar';
import { ColorProps } from 'types/extended';
import { queryParamsProps } from 'types/note-log';
import {
    GlobalFilter,
    renderFilterTypes
} from 'utils/react-table';
import { dataProps, ReactTableProps } from './types/types';


// ==============================|| REACT TABLE ||============================== //
interface Props {
    status: boolean;
}

const IsActiveStatus = ({ status }: Props) => {
    let color: ColorProps;
    let title: string;

    switch (status) {
        case true:
            color = 'success';
            title = 'Active';
            break;
        case false:
            color = 'error';
            title = 'In-Active';
            break;
        default:
            color = 'primary';
            title = 'None';
    }

    return (
        <Stack direction="row" spacing={1} alignItems="center">
            <Dot color={color} />
            <Typography>{title}</Typography>
        </Stack>
    );
};

function ReactTable({ columns, data, getHeaderProps }: ReactTableProps) {
    const theme = useTheme();

    const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));
    const sortBy = { id: 'id', desc: false };
    
    const filterTypes = useMemo(() => renderFilterTypes, []);

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        rows,
        page,
        gotoPage,
        setPageSize,
        state: { globalFilter, pageIndex, pageSize },
        preGlobalFilteredRows,
        setGlobalFilter,
    } = useTable(
        {
            columns,
            data,
            filterTypes,
            initialState: { pageIndex: 0, pageSize: 10, hiddenColumns: ['avatar'], sortBy: [sortBy] }
        },
        useGlobalFilter,
        useFilters,
        useSortBy,
        usePagination,
    );

    return (
        <>
            <Stack spacing={3}>
                <Stack
                    direction={matchDownSM ? 'column' : 'row'}
                    spacing={1}
                    justifyContent="space-between"
                    alignItems="center"
                    sx={{ p: 3, pb: 0 }}
                >
                    <GlobalFilter
                        preGlobalFilteredRows={preGlobalFilteredRows}
                        globalFilter={globalFilter}
                        setGlobalFilter={setGlobalFilter}
                        size="small"
                    />
                    
                </Stack>
                <Table {...getTableProps()}>
                    <TableHead>
                        {headerGroups.map((headerGroup: HeaderGroup<{}>) => (
                            <TableRow {...headerGroup.getHeaderGroupProps()} sx={{ '& > th:first-of-type': { width: '58px' } }}>
                                {headerGroup.headers.map((column: HeaderGroup) => (
                                    <TableCell {...column.getHeaderProps([{ className: column.className }, getHeaderProps(column)])}>
                                        <HeaderSort column={column} />
                                    </TableCell>
                                ))}
                            </TableRow>
                        ))}
                    </TableHead>
                    <TableBody {...getTableBodyProps()}>
                        {page.map((row: Row, i: number) => {
                            prepareRow(row);
                            return (
                                <Fragment key={i}>
                                    <TableRow
                                        {...row.getRowProps()}
                                        onClick={() => {
                                            row.toggleRowSelected();
                                        }}
                                        sx={{ cursor: 'pointer', bgcolor: row.isSelected ? alpha(theme.palette.primary.lighter, 0.35) : 'inherit' }}
                                    >
                                        {row.cells.map((cell: Cell) => (
                                            <TableCell {...cell.getCellProps([{ className: cell.column.className }])}>{cell.render('Cell')}</TableCell>
                                        ))}
                                    </TableRow>
                                    {/* {row.isExpanded && renderRowSubComponent({ row, rowProps, visibleColumns, expanded })} */}
                                </Fragment>
                            );
                        })}
                        <TableRow>
                            <TableCell sx={{ p: 2 }} colSpan={12}>
                                <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </Stack>
        </>
    );
}


// ==============================|| Logs ||============================== //

const Logs = () => {

    const dispatch = useDispatch();
    const { noteLogs, isLoading, error, success } = useSelector(state => state.noteLog)

    // table 
    const [data, setData] = useState<dataProps[]>([])

    const columns = useMemo(
        () =>
            [
                {
                    Header: '#',
                    accessor: 'id',
                    className: 'cell-center',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.id === undefined || row.id === null || row.id === '') {
                            return <>-</>
                        }
                        if (typeof row.id === 'string') {
                            return <>{(parseInt(row.id) + 1).toString()}</>;
                        }
                        if (typeof row.id === 'number') {
                            return <>{row.id + 1}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Menu',
                    accessor: 'menuName',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.menuName === undefined || row.values.menuName === null || row.values.menuName === '') {
                            return <>-</>
                        }
                        if (typeof row.values.menuName === 'string') {
                            return <>{row.values.menuName}</>;
                        }
                        if (typeof row.values.menuName === 'number') {
                            return <>{row.values.menuName}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Action',
                    accessor: 'actionName',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.actionName === undefined || row.values.actionName === null || row.values.actionName === '') {
                            return <>-</>
                        }
                        if (typeof row.values.actionName === 'string') {
                            return <>{row.values.actionName}</>;
                        }
                        if (typeof row.values.actionName === 'number') {
                            return <>{row.values.actionName}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Key-Value',
                    accessor: 'keyValue',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.keyValue === undefined || row.values.keyValue === null || row.values.keyValue === '') {
                            return <>-</>
                        }
                        if (typeof row.values.keyValue === 'string') {
                            return <>{row.values.keyValue}</>;
                        }
                        if (typeof row.values.keyValue === 'number') {
                            return <>{row.values.keyValue}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Description',
                    accessor: 'description',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.description === undefined || row.values.description === null || row.values.description === '') {
                            return <>-</>
                        }
                        if (typeof row.values.description === 'string') {
                            return <>{row.values.description}</>;
                        }
                        if (typeof row.values.description === 'number') {
                            return <>{row.values.description}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'User',
                    accessor: 'userName',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.userName === undefined || row.values.userName === null || row.values.userName === '') {
                            return <>-</>
                        }
                        if (typeof row.values.userName === 'string') {
                            return <>{row.values.userName}</>;
                        }
                        if (typeof row.values.userName === 'number') {
                            return <>{row.values.userName}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Branch',
                    accessor: 'branchName',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.branchName === undefined || row.values.branchName === null || row.values.branchName === '') {
                            return <>-</>
                        }
                        if (typeof row.values.branchName === 'string') {
                            return <>{row.values.branchName}</>;
                        }
                        if (typeof row.values.branchName === 'number') {
                            return <>{row.values.branchName}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Department',
                    accessor: 'deptName',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.deptName === undefined || row.values.deptName === null || row.values.deptName === '') {
                            return <>-</>
                        }
                        if (typeof row.values.deptName === 'string') {
                            return <>{row.values.deptName}</>;
                        }
                        if (typeof row.values.deptName === 'number') {
                            return <>{row.values.deptName}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Date',
                    accessor: 'systemDate',
                    Cell: ({ row }: { row: Row }) => {
                        const value = row.values.systemDate;
                        if (!value || !Array.isArray(value) || value.length !== 3) return <>-</>;
                    
                        // Extract the year, month, and day from the array
                        const [year, month, day] = value;
                        
                        // Create a Date object using the extracted values
                        const date = new Date(year, month - 1, day); // Month is 0-indexed in JS Date
                    
                        // Format the date to 'DD/MM/YYYY'
                        const formattedDate = date.toLocaleDateString('en-GB', {
                        day: '2-digit',
                        month: '2-digit',
                        year: 'numeric'
                        });
                    
                        return <>{formattedDate}</>;
                    }
                },
                {
                    Header: 'Time',
                    accessor: 'serverTime',
                    Cell: ({ row }: { row: Row }) => {
                        const value = row.values.serverTime;
                        if (!value || !Array.isArray(value) || value.length !== 3) return <>-</>;
                
                        // Extract the hours, minutes, and seconds from the array
                        const [hours, minutes, seconds] = value;
                
                        // Ensure all values are numbers and within valid ranges
                        const isValidTime = 
                            typeof hours === 'number' && hours >= 0 && hours < 24 &&
                            typeof minutes === 'number' && minutes >= 0 && minutes < 60 &&
                            typeof seconds === 'number' && seconds >= 0 && seconds < 60;
                
                        if (!isValidTime) return <>-</>;
                
                        // Format the time to 'HH:MM:SS'
                        const formattedTime = new Date(0, 0, 0, hours, minutes, seconds).toLocaleTimeString('en-GB', {
                            hour: '2-digit',
                            minute: '2-digit',
                            second: '2-digit'
                        });
                
                        return <>{formattedTime}</>;
                    }
                },
                
                {
                    Header: 'Is Active',
                    accessor: 'isActive',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.isActive === undefined || row.values.isActive === null) {
                            return <>-</>;
                        }
                        if (typeof row.values.isActive === 'boolean') {
                            return (
                                <>
                                    <IsActiveStatus status={row.values.isActive} />{' '}
                                </>
                            );
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
            ] as Column[],
        []
    );

    //API CALL - Activity Note Log LIST 

    useEffect(() => {
        const queryParams: queryParamsProps = {
            direction: "desc",
            page: 0,
            per_page: 1000,
            search: "",
            sort: "activityId"
        }

        dispatch(fetchNoteLogs(queryParams))
    }, [])

    useEffect(() => {
        if (!noteLogs) {
            setData([])
            return
        }
        if (noteLogs == null) {
            setData([])
            return
        }
        setData(noteLogs.result!)
    }, [noteLogs])

    useEffect(() => {
        if (error != null) {
            let defaultErrorMessage = "ERROR";
            // @ts-ignore
            const errorExp = error as Template1Error
            if (errorExp.message) {
                defaultErrorMessage = errorExp.message
            }
            dispatch(
                openSnackbar({
                    open: true,
                    message: defaultErrorMessage,
                    variant: 'alert',
                    alert: {
                        color: 'error'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [error]);

    useEffect(() => {
        if (success != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: success,
                    variant: 'alert',
                    alert: {
                        color: 'success'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [success])

    if (isLoading) {
        return <>Loading...</>
    }

    return (
        <>
            <MainCard content={false}>
                <ScrollX>
                    <ReactTable columns={columns} data={data} getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()} />
                </ScrollX>
            </MainCard>
        </>
    );
}

export default Logs;
