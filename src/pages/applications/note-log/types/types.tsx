import { Column } from 'react-table';
import { HeaderGroup } from 'react-table';
import { NoteLogs } from 'types/note-log';

export interface dataProps extends NoteLogs { }
export interface ReactTableProps {
    columns: Column[]
    data: dataProps[]
    getHeaderProps: (column: HeaderGroup) => {};
}

export interface TableHeaderProps {
    headerGroups: HeaderGroup[];

}

export interface dataProps extends NoteLogs { }

export interface noteLogsProps extends NoteLogs { }





