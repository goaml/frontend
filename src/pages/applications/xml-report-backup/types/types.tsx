import { Column } from 'react-table';
import { HeaderGroup } from 'react-table';
import { xmlReport } from 'types/xml-report-backup';

export interface dataProps extends xmlReport { }
export interface ReactTableProps {
    columns: Column[]
    data: dataProps[]
    getHeaderProps: (column: HeaderGroup) => {};
}

export interface TableHeaderProps {
    headerGroups: HeaderGroup[];

}

export interface dataProps extends xmlReport { }

export interface xmlReportsProps extends xmlReport { }
