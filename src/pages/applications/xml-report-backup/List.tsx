import { Fragment, useEffect, useMemo, useState } from 'react';

// material-ui
import {
    alpha,
    Box,
    Button,
    Grid,
    LinearProgress,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Tooltip,
    useMediaQuery,
    useTheme
} from '@mui/material';

// third-party
import { Cell, Column, HeaderGroup, Row, useFilters, useGlobalFilter, usePagination, useSortBy, useTable } from 'react-table';

// project import
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import { HeaderSort, TablePagination } from 'components/third-party/ReactTable';

import { format } from 'date-fns';
import { useDispatch, useSelector } from 'store';
import { openSnackbar } from 'store/reducers/snackbar';
import { fetchdownloadBackupXml, fetchXmlReport, toInitialState } from 'store/reducers/xml-report-backup';
import { queryParamsProps } from 'types/xml-report-backup';
import {
    GlobalFilter,
    renderFilterTypes
} from 'utils/react-table';
import { dataProps, ReactTableProps } from './types/types';

// ==============================|| REACT TABLE ||============================== //
function ReactTable({ columns, data, getHeaderProps }: ReactTableProps) {
    const theme = useTheme();

    const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));
    const sortBy = { id: 'reportLogId', desc: false };

    const filterTypes = useMemo(() => renderFilterTypes, []);

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        rows,
        page,
        gotoPage,
        setPageSize,
        state: { globalFilter, pageIndex, pageSize },
        preGlobalFilteredRows,
        setGlobalFilter,
    } = useTable(
        {
            columns,
            data,
            filterTypes,
            initialState: { pageIndex: 0, pageSize: 10, hiddenColumns: ['avatar'], sortBy: [sortBy] }
        },
        useGlobalFilter,
        useFilters,
        useSortBy,
        usePagination,
    );

    return (
        <>
            <Stack spacing={3}>
                <Stack
                    direction={matchDownSM ? 'column' : 'row'}
                    spacing={1}
                    justifyContent="space-between"
                    alignItems="center"
                    sx={{ p: 3, pb: 0 }}
                >
                    <GlobalFilter
                        preGlobalFilteredRows={preGlobalFilteredRows}
                        globalFilter={globalFilter}
                        setGlobalFilter={setGlobalFilter}
                        size="small"
                    />

                </Stack>
                <Table {...getTableProps()}>
                    <TableHead>
                        {headerGroups.map((headerGroup: HeaderGroup<{}>) => (
                            <TableRow {...headerGroup.getHeaderGroupProps()} sx={{ '& > th:first-of-type': { width: '58px' } }}>
                                {headerGroup.headers.map((column: HeaderGroup) => (
                                    <TableCell {...column.getHeaderProps([{ className: column.className }, getHeaderProps(column)])}>
                                        <HeaderSort column={column} />
                                    </TableCell>
                                ))}
                            </TableRow>
                        ))}
                    </TableHead>
                    <TableBody {...getTableBodyProps()}>
                        {page.map((row: Row, i: number) => {
                            prepareRow(row);
                            return (
                                <Fragment key={i}>
                                    <TableRow
                                        {...row.getRowProps()}
                                        sx={{ cursor: 'pointer', bgcolor: row.isSelected ? alpha(theme.palette.primary.lighter, 0.35) : 'inherit' }}
                                    >
                                        {row.cells.map((cell: Cell) => (
                                            <TableCell {...cell.getCellProps([{ className: cell.column.className }])}>{cell.render('Cell')}</TableCell>
                                        ))}
                                    </TableRow>
                                    {/* {row.isExpanded && renderRowSubComponent({ row, rowProps, visibleColumns, expanded })} */}
                                </Fragment>
                            );
                        })}
                        <TableRow>
                            <TableCell sx={{ p: 2 }} colSpan={12}>
                                <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </Stack>
        </>
    );
}


// ==============================|| XMLReport ||============================== //

const XMLReport = () => {

    const dispatch = useDispatch();
    const { xmlReports, isLoading, error, success } = useSelector(state => state.xmlBackupReport)

    // table 
    const [data, setData] = useState<dataProps[]>([])


    const columns = useMemo(
        () =>
            [
                {
                    Header: '#',
                    accessor: 'id',
                    className: 'cell-center',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.id === undefined || row.id === null || row.id === '') {
                            return <>-</>
                        }
                        if (typeof row.id === 'string') {
                            return <>{(parseInt(row.id) + 1).toString()}</>;
                        }
                        if (typeof row.id === 'number') {
                            return <>{row.id + 1}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Report Code',
                    accessor: 'reportCode',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.reportCode === undefined || row.values.reportCode === null || row.values.reportCode === '') {
                            return <>-</>
                        }
                        if (typeof row.values.reportCode === 'string') {
                            return <>{row.values.reportCode}</>;
                        }
                        if (typeof row.values.reportCode === 'number') {
                            return <>{row.values.reportCode}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Create Date',
                    accessor: 'createdOn',
                    Cell: ({ row }: { row: Row }) => {
                        const formatDate = (dateString: string) => {
                            try {
                                const date = new Date(dateString);
                                return format(date, 'yyyy/MM/dd HH:mm:ss'); // Desired format
                            } catch (error) {
                                console.error('Error formatting date:', error);
                                return '-';
                            }
                        };

                        if (!row.values.createdOn) {
                            return <>-</>;
                        }

                        if (typeof row.values.createdOn === 'string') {
                            return <>{formatDate(row.values.createdOn)}</>;
                        }

                        if (typeof row.values.createdOn === 'number') {
                            const dateFromNumber = new Date(row.values.createdOn).toISOString();
                            return <>{formatDate(dateFromNumber)}</>;
                        }

                        return <>-</>;
                    }
                },
                {
                    Header: 'Report Type',
                    accessor: 'reportType',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.reportType === undefined || row.values.reportType === null || row.values.reportType === '') {
                            return <>-</>
                        }
                        if (typeof row.values.reportType === 'string') {
                            return <>{row.values.reportType}</>;
                        }
                        if (typeof row.values.reportType === 'number') {
                            return <>{row.values.reportType}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'File Name',
                    accessor: 'fileName',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.fileName === undefined || row.values.fileName === null || row.values.fileName === '') {
                            return <>-</>
                        }
                        if (typeof row.values.fileName === 'string') {
                            return <>{row.values.fileName}</>;
                        }
                        if (typeof row.values.fileName === 'number') {
                            return <>{row.values.fileName}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    id: "actions",
                    Header: 'Actions',
                    accessor: 'actions',
                    className: 'cell-center',
                    Cell: ({ row }: { row: Row }) => {
                        return (
                            <>
                                <Stack spacing={2}>
                                    <Grid container spacing={1} width={"300px"}>
                                        <Grid item xs={5}>
                                            <Tooltip title="XML Backup Report">

                                                <Button
                                                    color="primary"
                                                    sx={{ width: '100%', height: '30px', maxWidth: '120px' }}
                                                    variant="contained"
                                                    onClick={async () => {
                                                        const data: dataProps = row.original;
                                                        await dispatch(fetchdownloadBackupXml({ reportLogId: data.reportLogId }))
                                                    }}
                                                >
                                                    Download
                                                </Button>
                                            </Tooltip>
                                        </Grid>
                                    </Grid>
                                </Stack>
                            </>
                        )
                    }
                }
            ] as Column[],
        []
    );

    //API CALL - Activity XML Report Log LIST 

    useEffect(() => {
        const queryParams: queryParamsProps = {
            direction: "asc",
            page: 0,
            per_page: 1000,
            search: "",
            sort: "reportLogId"
        }

        dispatch(fetchXmlReport(queryParams))
    }, [])

    useEffect(() => {
        if (!xmlReports) {
            setData([])
            return
        }
        if (xmlReports == null) {
            setData([])
            return
        }
        setData(xmlReports.result!)
    }, [xmlReports])

    useEffect(() => {
        if (error != null) {
            let defaultErrorMessage = "ERROR";
            // @ts-ignore
            const errorExp = error as Template1Error
            if (errorExp.message) {
                defaultErrorMessage = errorExp.message
            }
            dispatch(
                openSnackbar({
                    open: true,
                    message: defaultErrorMessage,
                    variant: 'alert',
                    alert: {
                        color: 'error'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [error]);

    useEffect(() => {
        if (success != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: success,
                    variant: 'alert',
                    alert: {
                        color: 'success'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [success])

    if (isLoading) {
        return (
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
                <Box sx={{ width: '50%' }}>
                    <LinearProgress />
                </Box>
            </div>
        );
    }

    return (
        <>
            <MainCard content={false}>
                <ScrollX>
                    <ReactTable columns={columns} data={data} getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()} />
                </ScrollX>
            </MainCard>
        </>
    );
}

export default XMLReport;
