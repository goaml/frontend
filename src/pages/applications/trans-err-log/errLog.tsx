/* eslint-disable prettier/prettier */
import { EyeTwoTone, FilterTwoTone } from '@ant-design/icons';
// material ui
import {
    Dialog,
    IconButton,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Tooltip,
    useMediaQuery,
    useTheme
} from '@mui/material';

import { PopupTransition } from 'components/@extended/Transitions';
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
// third-party
import { EmptyTable, HeaderSort, TableRowSelection } from 'components/third-party/ReactTable';


import { Box } from '@mui/system';
import { TablePagination } from 'components/third-party/ReactTable2.0';
import { MouseEvent, useEffect, useMemo, useState } from 'react';
import { Cell, Column, HeaderGroup, Row, useExpanded, useFilters, useGlobalFilter, usePagination, useRowSelect, useSortBy, useTable } from 'react-table';

import { dispatch, useSelector } from 'store';
import { fetchTransactionCodeList } from 'store/reducers/goaml-trx-code';
import { fetchRPTList } from 'store/reducers/rpt-code';
import { openSnackbar } from 'store/reducers/snackbar';
import { fetchTransactionErrorLog, toInitialState } from 'store/reducers/trans-err-log';
import { listParametersType } from 'types/trans-err-log';
import {
    GlobalFilter
} from 'utils/react-table';
import FilterDialog from './FilterDialog';
import { dataProps, ReactTableProps, TableParamsType } from './type/types';
import { fetchReportCodeList } from 'store/reducers/report-type';
import AddEditTransactionsErr from 'sections/parameter-management/trans-err-log/AddEditTransactionsErr';



// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, handleAddEdit, getHeaderProps, pagination, tableParams, rptCode, goAMLRPTData }: ReactTableProps) {
    const theme = useTheme();

    const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));
    const sortBy = { id: 'logId', desc: true };

    const [filterDialogOpen, setFilterDialogOpen] = useState(false);

    const handleFilterDialogOpen = () => {
        setFilterDialogOpen(true);
    };

    const handleFilterDialogClose = () => {
        setFilterDialogOpen(false);
    };


    const handleApplyFilters = (filters: { [key: string]: string }) => {
        console.log(filters, 'filters');

        setFilterDialogOpen(false);
        tableParams.setPage(0);
        tableParams.setAcid(filters.acid);
        tableParams.setForacid(filters.foracid);
        tableParams.setAmountLocal(parseInt(filters.amountLocal) || undefined);
        tableParams.setCustId(filters.custId);
        tableParams.setTransactionDateFrom(filters.transactionDateFrom);
        tableParams.setTransactionDateTo(filters.transactionDateTo);
        tableParams.setFailReason(filters.failReason);
        tableParams.setIsValidated(filters.isValidated === 'true' ? true : filters.isValidated === 'false' ? false : undefined);
        tableParams.setLogId(parseInt(filters.logId) || undefined);
        tableParams.setProcessDate(filters.processDate);
        tableParams.setRptCode(filters.rptCode);
        tableParams.setTransactionNumber(filters.transactionNumber);
        tableParams.setTrxNo(filters.trxNo);
        dispatch(fetchTransactionErrorLog({ ...filters, page: 0, per_page: 10, direction: 'asc', sort: 'logId' })); // Apply filters and reset pagination
    };

    const onReset = () => {
        // tableParams.setExecuteTime("");
        tableParams.setLogId(undefined);
        tableParams.setAcid("");
        tableParams.setForacid("");
        tableParams.setAmountLocal(undefined);
        tableParams.setCustId("");
        tableParams.setPage(0);
        tableParams.setTransactionDateFrom("");
        tableParams.setTransactionDateTo("");
        tableParams.setFailReason("");
        tableParams.setIsValidated(undefined);
        tableParams.setProcessDate("");
        tableParams.setRptCode("");
        tableParams.setTransactionNumber("");
        tableParams.setTrxNo("");

        dispatch(fetchTransactionErrorLog({ page: 0, per_page: 10, direction: 'asc', sort: 'logId' }));
    }

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        rows,
        page,
        gotoPage,
        setPageSize,
        state: { globalFilter, selectedRowIds, pageIndex, pageSize },
        preGlobalFilteredRows,
        setGlobalFilter,
    } = useTable(
        {
            columns,
            data,
            initialState: { pageIndex: tableParams.page, pageSize: 10, hiddenColumns: ['avatar'], sortBy: [sortBy] },
            manualPagination: true, // Enable manual pagination
            pageCount: Math.ceil(pagination.total / tableParams.perPage),
        },
        useGlobalFilter,
        useFilters,
        useSortBy,
        useExpanded,
        usePagination,
        useRowSelect
    );


    return (
        <>
            <TableRowSelection selected={Object.keys(selectedRowIds).length} />
            <Stack spacing={3}>
                <Stack
                    direction={matchDownSM ? 'column' : 'row'}
                    spacing={1}
                    justifyContent="space-between"
                    alignItems="center"
                    sx={{ p: 3, pb: 0 }}
                >
                    <GlobalFilter
                        preGlobalFilteredRows={preGlobalFilteredRows}
                        globalFilter={globalFilter}
                        setGlobalFilter={setGlobalFilter}
                        size="small"
                    />
                    <Stack direction={matchDownSM ? 'column' : 'row'} alignItems="center" spacing={1}>
                        <Tooltip title="Filter Columns">
                            <IconButton onClick={handleFilterDialogOpen}>
                                <FilterTwoTone />
                            </IconButton>
                        </Tooltip>
                    </Stack>
                </Stack>
                <Box sx={{ display: 'flex', flexDirection: 'column', height: '100%', width: '100%', justifyContent: 'space-between' }}>
                    <Box sx={{ flex: '1 1 auto', overflowY: 'auto' }}>
                        <Table {...getTableProps()}>
                            <TableHead>
                                {headerGroups.map((headerGroup: HeaderGroup<{}>) => (
                                    <TableRow
                                        {...headerGroup.getHeaderGroupProps()}
                                        sx={{ '& > th:first-of-type': { width: '58px' } }}
                                    >
                                        {headerGroup.headers.map((column: HeaderGroup) => (
                                            <TableCell
                                                {...column.getHeaderProps([{ className: column.className }, getHeaderProps(column)])}
                                            >
                                                <HeaderSort column={column} />
                                            </TableCell>
                                        ))}
                                    </TableRow>
                                ))}
                            </TableHead>
                            <TableBody {...getTableBodyProps()}>
                                {page.length > 0 ? (
                                    page.map((row, i) => {
                                        prepareRow(row);
                                        return (
                                            <TableRow {...row.getRowProps()}>
                                                {row.cells.map((cell: Cell) => (
                                                    <TableCell
                                                        {...cell.getCellProps([{ className: cell.column.className }])}
                                                    >
                                                        {cell.render('Cell')}
                                                    </TableCell>
                                                ))}
                                            </TableRow>
                                        );
                                    })
                                ) : (
                                    <EmptyTable msg="No Data" colSpan={12} />
                                )}
                            </TableBody>
                        </Table>
                    </Box>
                </Box>
                <Box sx={{ p: 1 }}>
                    <TablePagination
                        pageIndex={pageIndex}
                        pageSize={pageSize}
                        setPageSize={setPageSize}
                        gotoPage={gotoPage}
                        rows={rows}
                        totalRows={pagination.total}
                        tableParams={tableParams}
                    />
                </Box>

            </Stack>
            <FilterDialog
                open={filterDialogOpen}
                onClose={handleFilterDialogClose}
                onApplyFilters={handleApplyFilters}
                onReset={onReset}
                rptCode={rptCode}
                goAMLRPTData={goAMLRPTData}
                initialFilters={{
                    rptCode: tableParams.rptCode,
                    transactionNumber: tableParams.transactionNumber,
                    custId: tableParams.custId,
                    acid: tableParams.acid,
                    transactionDateFrom : tableParams.transactionDateFrom,
                    transactionDateTo : tableParams.transactionDateTo,

                }}
            />

        </>
    );
}

// ==============================|| List ||============================== //
const DateFormatter = ({ value }: { value: any[] }) => {
    if (!value || !Array.isArray(value)) {
        return <div>-</div>;
    }

    try {
        const [year, month, date, hours, minutes, seconds] = value;
        
        // Format date as YYYY/MM/DD
        const formattedDate = `${year.toString().padStart(4, '0')}/${month.toString().padStart(2, '0')}/${date.toString().padStart(2, '0')}`;
        
        // Format time as HH:mm:ss
        const formattedTime = `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
        
        return (
            <div>
                {formattedDate} {formattedTime}
            </div>
        );
    } catch (error) {
        console.error('Error formatting date:', error);
        return <div>-</div>;
    }
};

const ErrorLog = () => {
    const theme = useTheme();

    const { transErrLogList, error, success, isLoading } = useSelector(state => state.transactionErrorLog);
    // const { selectTransactionCodeType } = useSelector((state) => state.transaction);
    const { selectRPTCodeType } = useSelector((state) => state.rptCode);

    const [data, setData] = useState<dataProps[]>([])


    // ==============================|| Pagination-Config ||============================== //

    const [Pagination, setPagination] = useState<any>([])
    const [page, setPage] = useState<number>(0);
    const [perPage, setPerPage] = useState<number>(10);
    const [direction, setDirection] = useState<"asc" | "desc">("asc");
    const [search, setSearch] = useState<string>("");
    const [sort, setSort] = useState<string>("logId");

    const [transactionDateFrom, setTransactionDateFrom] = useState<string>("");
    const [transactionDateTo, setTransactionDateTo] = useState<string>("");
    const [logId, setLogId] = useState<number>();
    const [acid, setAcid] = useState<string>("");
    const [custId, setCustId] = useState<string>("");
    const [amountLocal, setAmountLocal] = useState<number>();
    const [failReason, setFailReason] = useState<string>("");
    const [foracid, setForacid] = useState<string>("");
    const [isValidated, setIsValidated] = useState<boolean | undefined>();
    const [rptCode, setRptCode] = useState<string>("");
    const [transactionNumber, setTransactionNumber] = useState<string>("");
    const [trxNo, setTrxNo] = useState<string>("");
    const [processDate, setProcessDate] = useState<string>("");

    const tableParams: TableParamsType = {
        page,
        setPage,
        perPage,
        setPerPage,
        direction,
        setDirection,
        sort,
        setSort,
        search,
        setSearch,
        acid,
        setAcid,
        amountLocal,
        setAmountLocal,
        custId,
        setCustId,
        transactionDateFrom,
        setTransactionDateFrom,
        transactionDateTo,
        setTransactionDateTo,
        failReason,
        setFailReason,
        foracid,
        setForacid,
        isValidated,
        setIsValidated,
        logId,
        setLogId,
        processDate,
        setProcessDate,
        rptCode,
        setRptCode,
        transactionNumber,
        setTransactionNumber,
        trxNo,
        setTrxNo
    }

    const columns = useMemo(
        () =>
            [
                {
                    Header: '#',
                    accessor: 'logId',
                    id: 'logId',
                    Cell: ({ value }) => (value ? value : '-')
                },
                {
                    Header: 'Process Date',
                    accessor: 'processDate',
                    id: 'processDate',
                    Cell: ({ value }) => {
                        if (!value) return '-';
                        const date = new Date(value);
                        return `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()}`; 
                    }
                },
                {
                    Header: 'Trx number',
                    accessor: 'transactionNumber',
                    id: 'transactionNumber',
                    Cell: ({ value }) => (value ? value : '-')
                },
                {
                    Header: 'Trx Date',
                    accessor: 'dateTransaction',
                    id: 'dateTransaction',
                    Cell: ({ value }) => <DateFormatter value={value} />
                },
                {
                    Header: 'Amount',
                    accessor: 'amountLocal',
                    id: 'amountLocal',
                    Cell: ({ value }) => {
                        if (!value) return '-';
                        return parseFloat(value).toLocaleString('en-US');
                    }
                },
                {
                    Header: 'Cust ID',
                    accessor: 'custId',
                    id: 'custId',
                    Cell: ({ value }) => (value ? value : '-')
                },
                {
                    Header: 'Acid',
                    accessor: 'acid',
                    id: 'acid',
                    Cell: ({ value }) => (value ? value : '-')
                },
                {
                    Header: 'RPT Code',
                    accessor: 'rptCode',
                    id: 'rptCode',
                    Cell: ({ value }) => (value ? value : '-')
                },
                {
                    Header: 'GoAML Code',
                    accessor: 'trxNo',
                    id: 'trxNo',
                    Cell: ({ value }) => (value ? value : '-')
                },
                {
                    Header: 'Error message',
                    accessor: 'failReason',
                    id: 'failReason',
                    Cell: ({ value }) => (value ? value : '-')
                },
                {
                    id: "actions",
                    Header: 'Actions',
                    accessor: 'actions',
                    className: 'cell-center',
                    Cell: ({ row }: { row: Row }) => {
                        const data: dataProps = row.original;
                        return (
                            <>
                                <Stack direction="row" alignItems="center" justifyContent="center" spacing={0}>
                                    <Tooltip title="View">
                                        <IconButton
                                            color="secondary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                                handleAddEdit()
                                                setUser({
                                                    ...data
                                                })
                                                console.log(data, 'data')
                                            }}
                                        >
                                            <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                                        </IconButton>
                                    </Tooltip>
                                </Stack>
                            </>
                        )
                    }
                }

            ] as Column[],
        [tableParams.page, tableParams.perPage]
    );

    const [addEdit, setAddEdit] = useState<boolean>(false);
    const [user, setUser] = useState<dataProps>();

    const handleAddEdit = () => {
        setAddEdit(!addEdit);
        if (user && !addEdit) setUser(undefined);

    };

    // ==============================|| API-Config ||============================== //

    useEffect(() => {
        dispatch(fetchTransactionCodeList());
        dispatch(fetchRPTList())
    }, [])


    useEffect(() => {
        const listParameters: listParametersType = {
            page: page,
            per_page: perPage,
            direction: 'desc',
            search: search,
            sort: 'logId',
            acid: acid,
            amountLocal: amountLocal,
            custId: custId,
            transactionDateFrom: transactionDateFrom,
            transactionDateTo: transactionDateTo,
            failReason: failReason,
            foracid: foracid,
            isValidated: isValidated,
            logId: logId,
            processDate: processDate,
            rptCode: rptCode,
            transactionNumber: transactionNumber,
            trxNo: trxNo
        };
        dispatch(fetchTransactionErrorLog(listParameters));
    }, [dispatch, success, page, perPage, direction, search, sort]);

    useEffect(() => {
        setData(transErrLogList?.result! || []);
        setPagination(transErrLogList?.pagination! || {})
    }, [transErrLogList])

    useEffect(() => {
        if (error != null && typeof error === 'object' && 'message' in error) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: (error as { message: string }).message, // Type assertion
                    variant: 'alert',
                    alert: {
                        color: 'error'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [error]);

    //  handel success
    useEffect(() => {
        if (success != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: success,
                    variant: 'alert',
                    alert: {
                        color: 'success'
                    },
                    close: true
                })
            );
            dispatch(toInitialState())
        }
    }, [success])

    const { selectTransErrLog } = useSelector((state) => state.transactionErrorLog);

    useEffect(() => {
        dispatch(fetchReportCodeList()); // Fetch report types on component mount
    }, [dispatch]);


    if (isLoading) {
        return <div>Loading...</div>;
    }



    return (
        <>
            <MainCard content={false}>
                <ScrollX>
                    <ReactTable columns={columns}
                        getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()}
                        data={data} handleAddEdit={handleAddEdit}
                        tableParams={tableParams} pagination={Pagination}
                        rptCode ={selectTransErrLog!}
                        goAMLRPTData={selectRPTCodeType!}
                    />
                </ScrollX>
                {/* add / edit user dialog */}
                <Dialog
                    maxWidth="sm"
                    TransitionComponent={PopupTransition}
                    keepMounted
                    fullWidth
                    onClose={handleAddEdit}
                    open={addEdit}
                    sx={{ '& .MuiDialog-paper': { p: 0 }, transition: 'transform 225ms' }}
                    aria-describedby="alert-dialog-slide-description"
                >
                    <AddEditTransactionsErr user={user} onCancel={handleAddEdit} />
                </Dialog>
            </MainCard>
        </>
    )


};



export default ErrorLog;

