import {
  Autocomplete,
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  FormControl,
  FormControlLabel,
  FormHelperText,
  Grid,
  InputLabel,
  Stack,
  TextField
} from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { transErrLogListFilterType } from 'types/trans-err-log';
import { RPTCodeType } from 'types/rpt-code';
import { format } from 'date-fns';

interface FilterValues {
  rptCode?: string;
  transactionNumber?: string;
  custId?: string;
  acid?: string;
  transactionDateFrom: string;
  transactionDateTo: string;
}

interface FilterDialogProps {
  open: boolean;
  onClose: () => void;
  onApplyFilters: (filters: { [key: string]: string }) => void;
  onReset: () => void;
  initialFilters: FilterValues;
  rptCode: transErrLogListFilterType[] | null;
  goAMLRPTData: RPTCodeType[] | null;
}

const validationSchema = Yup.object().shape({
  rptCode: Yup.string(),
  transactionNumber: Yup.string(),
  custId: Yup.string(),
  acid: Yup.string(),
  transactionDateFrom: Yup.date().nullable(),
  transactionDateTo: Yup.date()
    .nullable()
    .min(Yup.ref('transactionDateFrom'), 'To date must be later than from date'),
});

const FilterDialog: React.FC<FilterDialogProps> = ({
  open,
  onClose,
  onApplyFilters,
  onReset,
  initialFilters,
  rptCode,
  goAMLRPTData
}) => {
  const [selectedColumns, setSelectedColumns] = useState<string[]>([]);

  useEffect(() => {
    const initialSelectedColumns: string[] = [];
    if (initialFilters.rptCode) initialSelectedColumns.push('rptCode');
    if (initialFilters.transactionNumber) initialSelectedColumns.push('transactionNumber');
    if (initialFilters.custId) initialSelectedColumns.push('custId');
    if (initialFilters.acid) initialSelectedColumns.push('acid');
    if (initialFilters.transactionDateFrom || initialFilters.transactionDateTo) initialSelectedColumns.push('dateTransaction');

    setSelectedColumns(initialSelectedColumns);
  }, [initialFilters]);

  const formik = useFormik({
    initialValues: {
      rptCode: initialFilters?.rptCode || '',
      transactionNumber: initialFilters?.transactionNumber || '',
      custId: initialFilters?.custId || '',
      acid: initialFilters?.acid || '',
      transactionDateFrom: initialFilters?.transactionDateFrom || '',
      transactionDateTo: initialFilters?.transactionDateTo || '',
    },
    validationSchema,
    onSubmit: (values) => {
      let errors: { [key: string]: string } = {};

      if (selectedColumns.includes('rptCode') && !values.rptCode) {
        errors.rptCode = 'Rpt Type is required';
      }

      if (selectedColumns.includes('transactionNumber') && !values.transactionNumber) {
        errors.transactionNumber = 'Transaction Number is required';
      }

      if (selectedColumns.includes('custId') && !values.custId) {
        errors.custId = 'Customer ID is required';
      }

      if (selectedColumns.includes('acid') && !values.acid) {
        errors.acid = 'Account ID is required';
      }

      if (selectedColumns.includes('dateTransaction')) {
        if (!values.transactionDateFrom)
          errors.transactionDateFrom = 'From date is required';
        if (!values.transactionDateTo)
          errors.transactionDateTo = 'To date is required';
      }

      // If errors exist, set them and stop the form submission
      if (Object.keys(errors).length > 0) {
        formik.setErrors(errors);
        return;
      }

      onApplyFilters(values);
      onClose();
    },
  });

  const handleColumnSelectionChange = (logId: string) => () => {
    setSelectedColumns((prevSelected) => {
      const isSelected = prevSelected.includes(logId);
      const updatedSelection = isSelected
        ? prevSelected.filter((col) => col !== logId)
        : [...prevSelected, logId];

      if (isSelected) {
        switch (logId) {
          case 'rptCode':
            formik.setFieldValue('rptCode', '');
            break;
          case 'transactionNumber':
            formik.setFieldValue('transactionNumber', '');
            break;
          case 'custId':
            formik.setFieldValue('custId', '');
            break;
          case 'acid':
            formik.setFieldValue('acid', '');
            break;
          case 'dateTransaction':
            formik.setFieldValue('transactionDateFrom', '');
            formik.setFieldValue('transactionDateTo', '');
            break
          default:
            break;
        }
      }

      return updatedSelection;
    });
  };

  const handleReset = () => {
    formik.resetForm();
    setSelectedColumns([]);
    onReset();
  };

  return (
    <Dialog open={open} onClose={onClose} maxWidth="md" fullWidth>
      <form onSubmit={formik.handleSubmit}>
        <DialogTitle>Filter Columns</DialogTitle>
        <Divider />
        <DialogContent>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Grid container spacing={2}>
                <Grid item xs={3}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={selectedColumns.includes('rptCode')}
                        onChange={handleColumnSelectionChange('rptCode')}
                        color="primary"
                      />
                    }
                    label="RPT Type"
                  />
                </Grid>
                <Grid item xs={3}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={selectedColumns.includes('transactionNumber')}
                        onChange={handleColumnSelectionChange('transactionNumber')}
                        color="primary"
                      />
                    }
                    label="Transaction Number"
                  />
                </Grid>
                <Grid item xs={3}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={selectedColumns.includes('custId')}
                        onChange={handleColumnSelectionChange('custId')}
                        color="primary"
                      />
                    }
                    label="Customer ID"
                  />
                </Grid>
                <Grid item xs={3}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={selectedColumns.includes('acid')}
                        onChange={handleColumnSelectionChange('acid')}
                        color="primary"
                      />
                    }
                    label="Account ID"
                  />
                </Grid>
                <Grid item xs={12} sm={4}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={selectedColumns.includes('dateTransaction')}
                        onChange={handleColumnSelectionChange('dateTransaction')}
                        color="primary"
                      />
                    }
                    label="Transaction Time"
                  />
                </Grid>
              </Grid>
            </Grid>

            <Grid item xs={12}>
              <Grid container spacing={2}>
                {selectedColumns.includes('rptCode') && (
                  <Grid item xs={12} sm={6}>
                    <FormControl fullWidth>
                      <InputLabel shrink>RPT Code</InputLabel>
                      {<Autocomplete
                        fullWidth
                        id="reportCodeId"
                        value={
                          goAMLRPTData?.find(
                            (option) => option.rptCodeName === formik.values.rptCode
                          ) || null
                        }
                        onChange={(event, newValue) => {
                          formik.setFieldValue('rptCode', newValue?.rptCodeName || '');
                        }}
                        options={goAMLRPTData || []}
                        getOptionLabel={(item) => `${item.rptCodeName}`}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            InputLabelProps={{ shrink: true }}
                          />
                        )}
                      />}
                      {formik.touched.rptCode && formik.errors.rptCode && (
                        <FormHelperText error>{formik.errors.rptCode}</FormHelperText>
                      )}
                    </FormControl>
                  </Grid>
                )}

                {selectedColumns.includes('transactionNumber') && (
                  <Grid item xs={12}>
                    <Stack spacing={1.25}>
                      <InputLabel htmlFor="transactionNumber">
                        Transaction Number <span style={{ color: 'red' }}>*</span>
                      </InputLabel>
                      <TextField
                        fullWidth
                        id="transactionNumber"
                        name="transactionNumber"
                        value={formik.values.transactionNumber}
                        onChange={formik.handleChange}
                        error={formik.touched.transactionNumber && Boolean(formik.errors.transactionNumber)}
                        helperText={formik.touched.transactionNumber && formik.errors.transactionNumber}
                      />
                    </Stack>
                  </Grid>
                )}

                {selectedColumns.includes('custId') && (
                  <Grid item xs={12}>
                    <Stack spacing={1.25}>
                      <InputLabel htmlFor="custId">
                        Customer ID <span style={{ color: 'red' }}>*</span>
                      </InputLabel>
                      <TextField
                        fullWidth
                        id="custId"
                        name="custId"
                        value={formik.values.custId}
                        onChange={formik.handleChange}
                        error={formik.touched.custId && Boolean(formik.errors.custId)}
                        helperText={formik.touched.custId && formik.errors.custId}
                      />
                    </Stack>
                  </Grid>
                )}

                {selectedColumns.includes('acid') && (
                  <Grid item xs={12}>
                    <Stack spacing={1.25}>
                      <InputLabel htmlFor="acid">
                        Account ID <span style={{ color: 'red' }}>*</span>
                      </InputLabel>
                      <TextField
                        fullWidth
                        id="acid"
                        name="acid"
                        value={formik.values.acid}
                        onChange={formik.handleChange}
                        error={formik.touched.acid && Boolean(formik.errors.acid)}
                        helperText={formik.touched.acid && formik.errors.acid}
                      />
                    </Stack>
                  </Grid>
                )}

                {selectedColumns.includes('dateTransaction') && (
                  <>
                    <Grid item xs={12} sm={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="transactionDateFrom">
                          Transaction Time From <span style={{ color: 'red' }}>*</span>
                        </InputLabel>
                        <TextField
                          type="datetime-local"
                          value={
                            formik.values.transactionDateFrom && !isNaN(new Date(formik.values.transactionDateFrom).getTime())
                              ? format(new Date(formik.values.transactionDateFrom), "yyyy-MM-dd'T'HH:mm:ss")
                              : format(new Date().setHours(0, 0, 0, 0), "yyyy-MM-dd'T'HH:mm:ss") // Defaults to today at 00:00 AM
                          }
                          onChange={(event) => {
                            const newValue = new Date(event.target.value);
                            if (!isNaN(newValue.getTime())) {
                              const formattedValue = format(newValue, "yyyy-MM-dd'T'HH:mm:ss");
                              formik.setFieldValue('transactionDateFrom', formattedValue);
                            } else {
                              console.error('Invalid date selected');
                            }
                          }}
                          error={formik.touched.transactionDateFrom && Boolean(formik.errors.transactionDateFrom)}
                          helperText={formik.touched.transactionDateFrom && formik.errors.transactionDateFrom}
                          fullWidth
                          InputLabelProps={{
                            shrink: true,
                          }}
                        />
                      </Stack>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="transactionDateTo">
                          Transaction Time To <span style={{ color: 'red' }}>*</span>
                        </InputLabel>
                        <TextField
                          type="datetime-local"
                          value={
                            formik.values.transactionDateTo && !isNaN(new Date(formik.values.transactionDateTo).getTime())
                              ? format(new Date(formik.values.transactionDateTo), "yyyy-MM-dd'T'HH:mm:ss")
                              : format(new Date().setHours(23, 59, 59, 0), "yyyy-MM-dd'T'HH:mm:ss") // Defaults to today at 11:59 PM
                          }
                          onChange={(event) => {
                            const newValue = new Date(event.target.value);
                            if (!isNaN(newValue.getTime())) {
                              const formattedValue = format(newValue, "yyyy-MM-dd'T'HH:mm:ss");
                              formik.setFieldValue('transactionDateTo', formattedValue);
                            } else {
                              console.error('Invalid date selected');
                            }
                          }}
                          error={formik.touched.transactionDateTo && Boolean(formik.errors.transactionDateTo)}
                          helperText={formik.touched.transactionDateTo && formik.errors.transactionDateTo}
                          fullWidth
                          InputLabelProps={{
                            shrink: true,
                          }}
                        />
                      </Stack>
                    </Grid>
                  </>
                )}

              </Grid>
            </Grid>
          </Grid>
        </DialogContent>

        <DialogActions>
          <Button onClick={onClose}>Cancel</Button>
          <Button onClick={handleReset} color="secondary">
            Clear Filters
          </Button>
          <Button
            disabled={selectedColumns.length === 0}
            type="submit"
            color="primary"
            variant="contained"
          >
            Apply
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

export default FilterDialog;