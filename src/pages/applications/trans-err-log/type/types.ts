//import { Dispatch, SetStateAction } from 'react';
import { Dispatch, SetStateAction } from 'react';
import { Column, HeaderGroup } from 'react-table';
import { transErrLogListFilterType } from 'types/trans-err-log';
import { RPTCodeType } from 'types/rpt-code';

export interface dataProps {
    acid?: string;
    amountLocal?: number;
    custId?: string;
    transactionDateFrom ?: string;
    transactionDateTo ?: string;
    failReason?: string;
    foracid?: string;
    isValidated?: boolean;
    logId?: number;
    processDate?: string;
    rptCode?: string;
    transactionNumber?: string;
    trxNo?: string;
}

export interface ReactTableProps {
    columns: Column[];
    data: dataProps[];
    handleAddEdit: () => void;
    getHeaderProps: (column: HeaderGroup) => {};
    tableParams: TableParamsType;
    pagination: any;
    rptCode:transErrLogListFilterType[] |  null;
    goAMLRPTData: RPTCodeType [] |null;
}
  
export interface TableHeaderProps {
    headerGroups: HeaderGroup[];
}

export interface transactionErrProps {
    acid?: string;
    amountLocal?: number;
    custId?: string;
    transactionDateFrom ?: string;
    transactionDateTo ?: string;
    failReason?: string;
    foracid?: string;
    isValidated?: boolean;
    logId?: number;
    processDate?: string;
    rptCode?: string;
    transactionNumber?: string;
    trxNo?: string;
}

export interface TableParamsType {
    page: number;
    setPage: Dispatch<SetStateAction<number>>;
    perPage: number;
    setPerPage: Dispatch<SetStateAction<number>>;
    direction: "asc" | "desc";
    setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
    sort: string;
    setSort: Dispatch<SetStateAction<string>>;
    search: string;
    setSearch: Dispatch<SetStateAction<string>>;
    logId?: number
    setLogId : Dispatch<SetStateAction<number | undefined>>;
    acid?: string
    setAcid: Dispatch<SetStateAction<string>>;
    amountLocal?: number
    setAmountLocal : Dispatch<SetStateAction<number | undefined>>;
    foracid?: string
    setForacid: Dispatch<SetStateAction<string>>;
    custId: string;
    setCustId: Dispatch<SetStateAction<string>>;
    failReason?: string
    setFailReason: Dispatch<SetStateAction<string>>;
    isValidated: boolean | undefined;
    setIsValidated: Dispatch<SetStateAction<boolean | undefined>>;
    trxNo: string | undefined;
    setTrxNo: Dispatch<SetStateAction<string>>;
    transactionDateFrom: string;
    setTransactionDateFrom: Dispatch<SetStateAction<string>>;
    transactionDateTo: string;
    setTransactionDateTo: Dispatch<SetStateAction<string>>;
    processDate: string;
    setProcessDate: Dispatch<SetStateAction<string>>;
    rptCode: string;
    setRptCode: Dispatch<SetStateAction<string>>;
    transactionNumber: string;
    setTransactionNumber: Dispatch<SetStateAction<string>>;
}
