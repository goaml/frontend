import { Dispatch, SetStateAction } from 'react';
import { Column, HeaderGroup } from 'react-table';
import { uploadData } from 'types/upload-data';


export interface dataProps extends uploadData {
}

export interface ReactTableProps {
  columns: Column[]
  data: dataProps[]
  getHeaderProps: (column: HeaderGroup) => {};
  // handleAddEdit: () => void
  //tableParams?: TableParamsType
}

export interface cityProps {
  id?: number;
  fileName?: string;
  fileType?: string;
  fileSize?: string;
  lastUpdatedDate?: string;
}


export interface TableParamsType {
  page: number;
  setPage: Dispatch<SetStateAction<number>>;
  perPage: number;
  setPerPage: Dispatch<SetStateAction<number>>;
  direction: "asc" | "desc";
  setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
  sort: string;
  setSort: Dispatch<SetStateAction<string>>;

}
