import { Fragment, useEffect, useMemo, useState } from 'react';

// material-ui
import {
  alpha,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  useMediaQuery,
  useTheme
} from '@mui/material';

// third-party
import { Cell, Column, HeaderGroup, Row, useFilters, useGlobalFilter, usePagination, useSortBy, useTable } from 'react-table';

// project import
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import { HeaderSort, TablePagination } from 'components/third-party/ReactTable';

// utils
import {
  GlobalFilter,
  renderFilterTypes
} from 'utils/react-table';

// sections

// assets

//types 

import { dataProps, ReactTableProps } from './types/types';

import { useDispatch, useSelector } from 'store';
import { fetchUploadDataTable, toInitialState } from 'store/reducers/upload-data';

import { openSnackbar } from 'store/reducers/snackbar';

// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, getHeaderProps }: ReactTableProps) {
  const theme = useTheme();

  const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));
  const sortBy = { id: 'id', desc: false };

  const filterTypes = useMemo(() => renderFilterTypes, []);

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    rows,
    page,
    gotoPage,
    setPageSize,
    state: { globalFilter, pageIndex, pageSize },
    preGlobalFilteredRows,
    setGlobalFilter,
  } = useTable(
    {
      columns,
      data,
      filterTypes,
      initialState: { pageIndex: 0, pageSize: 10, hiddenColumns: ['avatar'], sortBy: [sortBy] }
    },
    useGlobalFilter,
    useFilters,
    useSortBy,
    usePagination,
  );

  return (
    <>
      <Stack spacing={3}>
        <Stack
          direction={matchDownSM ? 'column' : 'row'}
          spacing={1}
          justifyContent="space-between"
          alignItems="center"
          sx={{ p: 3, pb: 0 }}
        >
          <GlobalFilter
            preGlobalFilteredRows={preGlobalFilteredRows}
            globalFilter={globalFilter}
            setGlobalFilter={setGlobalFilter}
            size="small"
          />

        </Stack>
        <Table {...getTableProps()}>
          <TableHead>
            {headerGroups.map((headerGroup: HeaderGroup<{}>) => (
              <TableRow {...headerGroup.getHeaderGroupProps()} sx={{ '& > th:first-of-type': { width: '58px' } }}>
                {headerGroup.headers.map((column: HeaderGroup) => (
                  <TableCell {...column.getHeaderProps([{ className: column.className }, getHeaderProps(column)])}>
                    <HeaderSort column={column} />
                  </TableCell>
                ))}
              </TableRow>
            ))}
          </TableHead>
          <TableBody {...getTableBodyProps()}>
            {page.map((row: Row, i: number) => {
              prepareRow(row);
              return (
                <Fragment key={i}>
                  <TableRow
                    {...row.getRowProps()}
                    onClick={() => {
                      row.toggleRowSelected();
                    }}
                    sx={{ cursor: 'pointer', bgcolor: row.isSelected ? alpha(theme.palette.primary.lighter, 0.35) : 'inherit' }}
                  >
                    {row.cells.map((cell: Cell) => (
                      <TableCell {...cell.getCellProps([{ className: cell.column.className }])}>{cell.render('Cell')}</TableCell>
                    ))}
                  </TableRow>
                  {/* {row.isExpanded && renderRowSubComponent({ row, rowProps, visibleColumns, expanded })} */}
                </Fragment>
              );
            })}
            <TableRow>
              <TableCell sx={{ p: 2 }} colSpan={12}>
                <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </Stack>
    </>
  );
}

// ==============================|| List ||============================== //

const List = () => {
  // const theme = useTheme();
  const dispatch = useDispatch();
  const { UploadDataList, error, success, isLoading } = useSelector(state => state.uploadData);


  // table
  const [data, setData] = useState<dataProps[]>([])

  const columns = useMemo(
    () =>
      [
        {
          Header: '#',
          accessor: 'id',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            if (row.id === undefined || row.id === null || row.id === '') {
              return <>-</>
            }
            if (typeof row.id === 'string') {
              return <>{(parseInt(row.id) + 1).toString()}</>;
            }
            if (typeof row.id === 'number') {
              return <>{row.id + 1}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          Header: 'File Name',
          accessor: 'fileName',
          Cell: ({ row }: { row: Row }) => {
            if (row.values.fileName === undefined || row.values.fileName === null || row.values.fileName === '') {
              return <>-</>
            }
            if (typeof row.values.fileName === 'string') {
              return <>{row.values.fileName}</>;
            }
            if (typeof row.values.fileName === 'number') {
              return <>{row.values.fileName}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          Header: 'File Type',
          accessor: 'fileType',
          Cell: ({ row }: { row: Row }) => {
            if (row.values.fileType === undefined || row.values.fileType === null || row.values.fileType === '') {
              return <>-</>
            }
            if (typeof row.values.fileType === 'string') {
              return <>{row.values.fileType}</>;
            }
            if (typeof row.values.fileType === 'number') {
              return <>{row.values.fileType}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }


        },
        {
          Header: 'File Size',
          accessor: 'fileSize',
          Cell: ({ row }: { row: Row }) => {
            if (row.values.fileSize === undefined || row.values.fileSize === null || row.values.fileSize === '') {
              return <>-</>
            }
            if (typeof row.values.fileSize === 'string') {
              return <>{row.values.fileSize}</>;
            }
            if (typeof row.values.fileSize === 'number') {
              return <>{row.values.fileSize}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }


        },
        {
          Header: 'Updated Date',
          accessor: 'lastUpdatedDate',
          Cell: ({ row }: { row: Row }) => {
            if (row.values.lastUpdatedDate === undefined || row.values.lastUpdatedDate === null || row.values.lastUpdatedDate === '') {
              return <>-</>
            }
            if (typeof row.values.lastUpdatedDate === 'string') {
              return <>{row.values.lastUpdatedDate}</>;
            }
            if (typeof row.values.lastUpdatedDate === 'number') {
              return <>{row.values.lastUpdatedDate}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }


        }
      ] as Column[],
    []
  );


  useEffect(() => {

    dispatch(fetchUploadDataTable());
  }, [dispatch, success]);

  useEffect(() => {
    setData(UploadDataList! || []);
  }, [UploadDataList])

  //  handel error 
  useEffect(() => {
    if (error != null && typeof error === 'object' && 'message' in error) {
      dispatch(
        openSnackbar({
          open: true,
          message: (error as { message: string }).message, // Type assertion
          variant: 'alert',
          alert: {
            color: 'error'
          },
          close: true
        })
      );
      dispatch(toInitialState());
    }
  }, [error]);

  //  handel success
  useEffect(() => {
    if (success != null) {
      dispatch(
        openSnackbar({
          open: true,
          message: success,
          variant: 'alert',
          alert: {
            color: 'success'
          },
          close: true
        })
      );
      dispatch(toInitialState())
    }
  }, [success])

  if (isLoading) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <MainCard content={false}>
        <ScrollX>
          <ReactTable columns={columns} data={data} getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()} />
        </ScrollX>

      </MainCard>
    </>
  );
}

export default List;
