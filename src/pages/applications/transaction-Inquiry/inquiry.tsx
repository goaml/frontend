/* eslint-disable prettier/prettier */
import { EyeTwoTone, FilterTwoTone } from '@ant-design/icons';
// material ui
import {
    Dialog,
    IconButton, Stack,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,



    Tooltip, useMediaQuery,
    useTheme
} from '@mui/material';
import { PopupTransition } from 'components/@extended/Transitions';
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
// third-party
import { EmptyTable, HeaderSort, TableRowSelection } from 'components/third-party/ReactTable';

import { TablePagination } from 'components/third-party/ReactTable2.0';
import { MouseEvent, useEffect, useMemo, useState } from 'react';
import { Cell, Column, HeaderGroup, Row, useExpanded, useFilters, useGlobalFilter, usePagination, useRowSelect, useSortBy, useTable } from 'react-table';
import AddEditTransactionsInq from 'sections/parameter-management/transacttion-inquiry/AddEditTransactionsInq';
import { dispatch, useSelector } from 'store';
import { fetchTransactionCodeList } from 'store/reducers/goaml-trx-code';
import { fetchRPTList } from 'store/reducers/rpt-code';
import { openSnackbar } from 'store/reducers/snackbar';
import { fetchTransactionInquires, toInitialState } from 'store/reducers/transaction-Inquiry';
import { listParametersType } from 'types/transaction-inquiry';
import {
    GlobalFilter
} from 'utils/react-table';
import FilterDialog from './FilterDialog';
import { dataProps, ReactTableProps, TableParamsType } from './type/types';
import { Box } from '@mui/system';

// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, handleAddEdit, getHeaderProps, pagination, tableParams, goAMLTrxData, goAMLRPTData }: ReactTableProps) {
    const theme = useTheme();

    const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));
    const sortBy = { id: 'id', desc: false };

    const [filterDialogOpen, setFilterDialogOpen] = useState(false);

    const handleFilterDialogOpen = () => {
        setFilterDialogOpen(true);
    };

    const handleFilterDialogClose = () => {
        setFilterDialogOpen(false);
    };


    const handleApplyFilters = (filters: { [key: string]: string }) => {
        console.log(filters, 'filters');

        setFilterDialogOpen(false);
        tableParams.setPage(0);
        tableParams.setAmountFrom(parseInt(filters.amountFrom) || undefined);
        tableParams.setAmountTo(parseInt(filters.amountTo) || undefined);
        tableParams.setCustId(filters.custId);
        tableParams.setDateFrom(filters.dateFrom);
        tableParams.setDateTo(filters.dateTo);
        tableParams.setGoAmlTrxCode(filters.goAmlTrxCode);
        tableParams.setRptCode(filters.rptCode);
        tableParams.setTranId(filters.tranId);
        dispatch(fetchTransactionInquires({ ...filters, page: 0, per_page: 10, direction: 'asc', sort: 'id' })); // Apply filters and reset pagination
    };

    const onReset = () => {
        tableParams.setAmountFrom(undefined);
        tableParams.setAmountTo(undefined);
        tableParams.setCustId("");
        tableParams.setDateFrom("");
        tableParams.setDateTo("");
        tableParams.setGoAmlTrxCode("");
        tableParams.setRptCode("");
        tableParams.setTranId("");
        tableParams.setPage(0);

        dispatch(fetchTransactionInquires({ page: 0, per_page: 10, direction: 'asc', sort: 'id' }));
    }

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        rows,
        page,
        gotoPage,
        setPageSize,
        state: { globalFilter, selectedRowIds, pageIndex, pageSize },
        preGlobalFilteredRows,
        setGlobalFilter,
    } = useTable(
        {
            columns,
            data,
            initialState: { pageIndex: tableParams.page, pageSize: 10, hiddenColumns: ['avatar'], sortBy: [sortBy] },
            manualPagination: true, // Enable manual pagination
            pageCount: Math.ceil(pagination.total / tableParams.perPage),
        },
        useGlobalFilter,
        useFilters,
        useSortBy,
        useExpanded,
        usePagination,
        useRowSelect
    );


    return (
        <>
            <TableRowSelection selected={Object.keys(selectedRowIds).length} />
            <Stack spacing={3}>
                <Stack
                    direction={matchDownSM ? 'column' : 'row'}
                    spacing={1}
                    justifyContent="space-between"
                    alignItems="center"
                    sx={{ p: 3, pb: 0 }}
                >
                    <GlobalFilter
                        preGlobalFilteredRows={preGlobalFilteredRows}
                        globalFilter={globalFilter}
                        setGlobalFilter={setGlobalFilter}
                        size="small"
                    />
                    <Stack direction={matchDownSM ? 'column' : 'row'} alignItems="center" spacing={1}>
                        <Tooltip title="Filter Columns">
                            <IconButton onClick={handleFilterDialogOpen}>
                                <FilterTwoTone />
                            </IconButton>
                        </Tooltip>
                    </Stack>
                </Stack>
                <Box sx={{ display: 'flex', flexDirection: 'column', height: '100%', width: '100%', justifyContent: 'space-between' }}>
                    <Box sx={{ flex: '1 1 auto', overflowY: 'auto' }}>
                        <Table {...getTableProps()}>
                            <TableHead>
                                {headerGroups.map((headerGroup: HeaderGroup<{}>) => (
                                    <TableRow
                                        {...headerGroup.getHeaderGroupProps()}
                                        sx={{ '& > th:first-of-type': { width: '58px' } }}
                                    >
                                        {headerGroup.headers.map((column: HeaderGroup) => (
                                            <TableCell
                                                {...column.getHeaderProps([{ className: column.className }, getHeaderProps(column)])}
                                            >
                                                <HeaderSort column={column} />
                                            </TableCell>
                                        ))}
                                    </TableRow>
                                ))}
                            </TableHead>
                            <TableBody {...getTableBodyProps()}>
                                {page.length > 0 ? (
                                    page.map((row, i) => {
                                        prepareRow(row);
                                        return (
                                            <TableRow {...row.getRowProps()}>
                                                {row.cells.map((cell: Cell) => (
                                                    <TableCell
                                                        {...cell.getCellProps([{ className: cell.column.className }])}
                                                        sx={{
                                                            textAlign:
                                                                cell.column.isNumeric ? 'left' : 'right',
                                                        }}
                                                    >
                                                        {cell.render('Cell')}
                                                    </TableCell>
                                                ))}
                                            </TableRow>
                                        );
                                    })
                                ) : (
                                    <EmptyTable msg="No Data" colSpan={12} />
                                )}
                            </TableBody>
                        </Table>
                    </Box>
                </Box>
                <Box sx={{ p: 1 }}>
                    <TablePagination
                        pageIndex={pageIndex}
                        pageSize={pageSize}
                        setPageSize={setPageSize}
                        gotoPage={gotoPage}
                        rows={rows}
                        totalRows={pagination.total}
                        tableParams={tableParams}
                    />
                </Box>

            </Stack>
            <FilterDialog
                open={filterDialogOpen}
                onClose={handleFilterDialogClose}
                onApplyFilters={handleApplyFilters}
                goAMLTrxData={goAMLTrxData}
                goAMLRPTData={goAMLRPTData}
                onReset={onReset}
                initialFilters={{
                    amountFrom: tableParams.amountFrom,
                    amountTo: tableParams.amountTo,
                    custId: tableParams.custId,
                    dateFrom: tableParams.dateFrom,
                    dateTo: tableParams.dateTo,
                    goAmlTrxCode: tableParams.goAmlTrxCode,
                    rptCode: tableParams.rptCode,
                    tranId: tableParams.tranId
                }}
            />

        </>
    );
}

// ==============================|| List ||============================== //

const Inquiry = () => {
    const theme = useTheme();

    const { transactionInquiryList, error, success, isLoading } = useSelector(state => state.transactionInquiry);
    const { selectTransactionCodeType } = useSelector((state) => state.transaction);
    const { selectRPTCodeType } = useSelector((state) => state.rptCode);

    const [data, setData] = useState<dataProps[]>([])


    // ==============================|| Pagination-Config ||============================== //
    const [Pagination, setPagination] = useState<any>([])
    const [page, setPage] = useState<number>(0);
    const [perPage, setPerPage] = useState<number>(10);
    const [direction, setDirection] = useState<"asc" | "desc">("asc");
    const [search, setSearch] = useState<string>("");
    const [sort, setSort] = useState<string>("id");
    const [amountFrom, setAmountFrom] = useState<number>();
    const [amountTo, setAmountTo] = useState<number>();
    const [custId, setCustId] = useState<string>("");
    const [dateFrom, setDateFrom] = useState<string>("");
    const [dateTo, setDateTo] = useState<string>("");
    const [goAmlTrxCode, setGoAmlTrxCode] = useState<string>("");
    const [rptCode, setRptCode] = useState<string>("");
    const [tranId, setTranId] = useState<string>("");


    const tableParams: TableParamsType = {
        page,
        setPage,
        perPage,
        setPerPage,
        direction,
        setDirection,
        sort,
        setSort,
        search,
        setSearch,
        amountFrom,
        setAmountFrom,
        amountTo,
        setAmountTo,
        custId,
        setCustId,
        dateFrom,
        setDateFrom,
        dateTo,
        setDateTo,
        goAmlTrxCode,
        setGoAmlTrxCode,
        rptCode,
        setRptCode,
        tranId,
        setTranId
    }

    const columns = useMemo(
        () =>
            [
                {
                    Header: '#',
                    accessor: 'id',
                    className: 'cell-center',
                    Cell: ({ row }: { row: Row }) => {
                        const dynamicIndex: number = (tableParams.page * tableParams.perPage) + row.index + 1; // Calculate index
                        return <>{dynamicIndex}</>;
                    }
                },
                {
                    Header: 'TRX Date',
                    accessor: 'tranDate',
                    id: 'tranDate',
                    Cell: ({ value }) => (value ? value : '-')
                },
                {
                    Header: 'TRX Id',
                    accessor: 'tranId',
                    id: 'tranId',
                    Cell: ({ value }) => (value ? value : '-')
                },
                {
                    Header: 'TRX Type',
                    accessor: 'tranType',
                    id: 'tranType',
                    Cell: ({ value }) => (value ? value : '-')
                },
                {
                    Header: 'Sub Type',
                    accessor: 'tranSubType',
                    id: 'tranSubType',
                    Cell: ({ value }) => (value ? value : '-')
                },

                {
                    Header: 'Account No',
                    accessor: 'acid',
                    id: 'acid',
                    Cell: ({ value }) => (value ? value : '-')
                },
                {
                    Header: 'GL header',
                    accessor: 'glSubHeadCode',
                    id: 'glSubHeadCode',
                    Cell: ({ value }) => (value ? value : '-')
                },
                {
                    Header: 'Customer ID',
                    accessor: 'custId',
                    id: 'custId',
                    Cell: ({ value }) => (value ? value : '-')
                },
                {
                    Header: 'Narration',
                    accessor: 'tranParticular',
                    id: 'tranParticular',
                    Cell: ({ value }) => (value ? value : '-')
                },
                {
                    Header: 'Trx Amount',
                    accessor: 'tranAmt',
                    id: 'tranAmt',
                    Cell: ({ value }) => {
                        if (value === undefined || value === null) return '-';
                        return value.toLocaleString('en-US'); // Add commas for thousands
                    }
                },
                {
                    Header: 'DR/CR',
                    accessor: 'partTranType',
                    id: 'partTranType',
                    Cell: ({ value }) => (value ? value : '-')
                },
                {
                    Header: 'RPT Code',
                    accessor: 'rptCode',
                    id: 'rptCode',
                    Cell: ({ value }) => (value ? value : '-')
                },
                {
                    Header: 'Currency Code',
                    accessor: 'crncyCode',
                    id: 'crncyCode',
                    Cell: ({ value }) => (value ? value : '-')
                },

                {
                    id: "actions",
                    Header: 'Actions',
                    accessor: 'actions',
                    className: 'cell-center',
                    Cell: ({ row }: { row: Row }) => {
                        const data: dataProps = row.original;
                        return (
                            <>
                                <Stack direction="row" alignItems="center" justifyContent="center" spacing={0}>
                                    <Tooltip title="View">
                                        <IconButton
                                            color="secondary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                                handleAddEdit()
                                                setUser({
                                                    ...data
                                                })
                                                console.log(data, 'data')
                                            }}
                                        >
                                            <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                                        </IconButton>
                                    </Tooltip>
                                </Stack>
                            </>
                        )
                    }
                }

            ] as Column[],
        [tableParams.page, tableParams.perPage]
    );

    const [addEdit, setAddEdit] = useState<boolean>(false);
    const [user, setUser] = useState<dataProps>();

    const handleAddEdit = () => {
        setAddEdit(!addEdit);
        if (user && !addEdit) setUser(undefined);

    };

    // ==============================|| API-Config ||============================== //
    useEffect(() => {
        dispatch(fetchTransactionCodeList());
        dispatch(fetchRPTList())
    }, [])


    useEffect(() => {
        const listParameters: listParametersType = {
            page: page,
            per_page: perPage,
            direction: direction,
            search: search,
            sort: sort,
            tranId: tranId,
            amountFrom: amountFrom,
            amountTo: amountTo,
            dateFrom: dateFrom,
            dateTo: dateTo,
            goAmlTrxCode: goAmlTrxCode,
            rptCode: rptCode,
            custId: custId


        };
        dispatch(fetchTransactionInquires(listParameters));
    }, [dispatch, success, page, perPage, direction, search, sort]);

    useEffect(() => {
        setData(transactionInquiryList?.result! || []);
        setPagination(transactionInquiryList?.pagination! || {})
    }, [transactionInquiryList])

    useEffect(() => {
        if (error != null && typeof error === 'object' && 'message' in error) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: (error as { message: string }).message, // Type assertion
                    variant: 'alert',
                    alert: {
                        color: 'error'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [error]);

    //  handel success
    useEffect(() => {
        if (success != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: success,
                    variant: 'alert',
                    alert: {
                        color: 'success'
                    },
                    close: true
                })
            );
            dispatch(toInitialState())
        }
    }, [success])

    if (isLoading) {
        return <div>Loading...</div>;
    }



    return (
        <>
            <MainCard content={false}>
                <ScrollX>
                    <ReactTable columns={columns}
                        getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()}
                        data={data} handleAddEdit={handleAddEdit}
                        tableParams={tableParams} pagination={Pagination}
                        goAMLTrxData={selectTransactionCodeType!}
                        goAMLRPTData={selectRPTCodeType!}
                    />
                </ScrollX>
                {/* add / edit user dialog */}
                <Dialog
                    maxWidth="sm"
                    TransitionComponent={PopupTransition}
                    keepMounted
                    fullWidth
                    onClose={handleAddEdit}
                    open={addEdit}
                    sx={{ '& .MuiDialog-paper': { p: 0 }, transition: 'transform 225ms' }}
                    aria-describedby="alert-dialog-slide-description"
                >
                    <AddEditTransactionsInq user={user} onCancel={handleAddEdit} />
                </Dialog>
            </MainCard>
        </>
    )


};



export default Inquiry;
