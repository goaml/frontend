//import { Dispatch, SetStateAction } from 'react';
import { Dispatch, SetStateAction } from 'react';
import { Column, HeaderGroup } from 'react-table';
import { TransactionType } from 'types/goaml-trx-code';
import { RPTCodeType } from 'types/rpt-code';


export interface dataProps {
    transactionDate?: string;
    transactionId?: number;
    transactionType?: string;
    transactionSubType?: string;
    part_tran_type?: string;
    acid?: string;
    gl_sub_head_code?: string;
    cust_id?: string;
    module_id?: string;
    crncy_code?: string;
    branch_code?: string;
    bank_code?: string;
    rptCodeName?: string;
    [key: string]: any;
    solId?: string;
}

export interface ReactTableProps {
    columns: Column[];
    data: dataProps[];
    handleAddEdit: () => void;
    getHeaderProps: (column: HeaderGroup) => {};
    tableParams: TableParamsType;
    pagination: any;
    goAMLTrxData: TransactionType[] | null;
    goAMLRPTData: RPTCodeType [] |null;
}
  
export interface TableHeaderProps {
    headerGroups: HeaderGroup[];
}

export interface transactionProps {
    transactionId: number;
    transactionType: string;
    transactionSubType: string;
    part_tran_type: string;
    acid: string;
    gl_sub_head_code: string;
    cust_id: string;
    module_id: string;
    crncy_code: string;
    branch_code: string;
    bank_code: string;
    rptCodeName: string;
}

export interface TableParamsType {
    page: number;
    setPage: Dispatch<SetStateAction<number>>;
    perPage: number;
    setPerPage: Dispatch<SetStateAction<number>>;
    direction: "asc" | "desc";
    setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
    sort: string;
    setSort: Dispatch<SetStateAction<string>>;
    search: string;
    setSearch: Dispatch<SetStateAction<string>>;
    amountTo: number | undefined;
    setAmountTo: Dispatch<SetStateAction<number | undefined>>;
    amountFrom: number | undefined;
    setAmountFrom: Dispatch<SetStateAction<number | undefined>>;
    dateFrom: string;
    setDateFrom: Dispatch<SetStateAction<string>>;
    dateTo: string;
    setDateTo: Dispatch<SetStateAction<string>>;
    rptCode: string;
    setRptCode: Dispatch<SetStateAction<string>>;
    goAmlTrxCode: string;
    setGoAmlTrxCode: Dispatch<SetStateAction<string>>;
    tranId: string;
    setTranId: Dispatch<SetStateAction<string>>;
    custId: string;
    setCustId: Dispatch<SetStateAction<string>>;
}
