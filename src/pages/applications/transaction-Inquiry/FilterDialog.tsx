import {
  Autocomplete,
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  FormControl,
  FormControlLabel,
  FormHelperText,
  Grid,
  InputLabel,
  TextField
} from '@mui/material';
import React, { useEffect, useState } from 'react';
// Import DatePicker
import { useFormik } from 'formik'; // Import Formik for form handling
import { TransactionType } from 'types/goaml-trx-code';
import * as Yup from 'yup'; // Import Yup for validation
import { RPTCodeType } from 'types/rpt-code';
interface FilterValues {
  amountFrom?: number;
  amountTo?: number;
  custId?: string;
  dateFrom?: string;
  dateTo?: string;
  goAmlTrxCode?: string;
  rptCode?: string;
  tranId?: string;
}

interface FilterDialogProps {
  open: boolean;
  onClose: () => void;
  onApplyFilters: (filters: { [key: string]: string }) => void;
  onReset: () => void;
  initialFilters: FilterValues;
  goAMLTrxData: TransactionType[] | null;
  goAMLRPTData: RPTCodeType[] | null;
}

// Define a simplified Yup validation schema
const validationSchema = Yup.object().shape({
  dateFrom: Yup.date().nullable(),
  dateTo: Yup.date()
    .nullable()
    .min(Yup.ref('dateFrom'), 'End date can\'t be before start date'),
  tranId: Yup.string(),
  amountFrom: Yup.number().nullable(),
  amountTo: Yup.number()
    .nullable()
    .min(Yup.ref('amountFrom'), 'Amount To must be greater than Amount From'),
  goAmlTrxCode: Yup.string(),
  custId: Yup.string(),
  rptCode: Yup.string(),
});

const FilterDialog: React.FC<FilterDialogProps> = ({
  open,
  onClose,
  onApplyFilters,
  onReset,
  initialFilters,
  goAMLTrxData,
  goAMLRPTData
}) => {
  const [selectedColumns, setSelectedColumns] = useState<string[]>([]);
  // Track multiple selected columns
  useEffect(() => {
    const initialSelectedColumns: string[] = [];
    if (initialFilters.dateFrom || initialFilters.dateTo) initialSelectedColumns.push('Date');
    if (initialFilters.tranId) initialSelectedColumns.push('tranId');
    if (initialFilters.amountFrom || initialFilters.amountTo) initialSelectedColumns.push('amount');
    if (initialFilters.goAmlTrxCode) initialSelectedColumns.push('goAmlTrxCode');
    if (initialFilters.custId) initialSelectedColumns.push('custId');
    if (initialFilters.rptCode) initialSelectedColumns.push('rptCode');

    setSelectedColumns(initialSelectedColumns);
  }, [initialFilters]);

  const formik = useFormik({
    initialValues: {
      dateFrom: initialFilters?.dateFrom || null,
      dateTo: initialFilters?.dateTo || null,
      tranId: initialFilters?.tranId || '',
      amountFrom: initialFilters?.amountFrom || '',
      amountTo: initialFilters?.amountTo || '',
      goAmlTrxCode: initialFilters?.goAmlTrxCode || '',
      custId: initialFilters?.custId || '',
      rptCode: initialFilters?.rptCode || '',
    },
    validationSchema,
    onSubmit: (values) => {
      let errors: { [key: string]: string } = {};

      // Validation for each selected field
      if (selectedColumns.includes('Date')) {
        if (!values.dateFrom) errors.dateFrom = 'Start date is required';
        if (!values.dateTo) errors.dateTo = 'End date is required';
      }

      if (selectedColumns.includes('tranId') && !values.tranId) {
        errors.tranId = 'TRX ID is required';
      }

      if (selectedColumns.includes('amount') && (!values.amountFrom || !values.amountTo)) {
        if (!values.amountFrom) errors.amountFrom = 'Amount From is required';
        if (!values.amountTo) errors.amountTo = 'Amount To is required';
      }

      if (selectedColumns.includes('goAmlTrxCode') && !values.goAmlTrxCode) {
        errors.goAmlTrxCode = 'GOAML TRX Code is required';
      }

      if (selectedColumns.includes('custId') && !values.custId) {
        errors.custId = 'Customer ID is required';
      }

      if (selectedColumns.includes('rptCode') && !values.rptCode) {
        errors.rptCode = 'RPT Code is required';
      }

      // If errors exist, set them and stop the form submission
      if (Object.keys(errors).length > 0) {
        formik.setErrors(errors);
        return;
      }

      const filtersToSend: any = { ...values };

      // Format date fields
      if (values.dateFrom) {
        filtersToSend.dateFrom = new Date(values.dateFrom).toISOString().split('T')[0];
      }
      if (values.dateTo) {
        filtersToSend.dateTo = new Date(values.dateTo).toISOString().split('T')[0];
      }

      // Send the selected filters
      onApplyFilters(filtersToSend);
      onClose();
    },
  });

  // Handle the selection of multiple columns
  const handleColumnSelectionChange = (columnId: string) => () => {
    setSelectedColumns((prevSelected) => {
      const isSelected = prevSelected.includes(columnId);
      const updatedSelection = isSelected
        ? prevSelected.filter((col) => col !== columnId)
        : [...prevSelected, columnId];

      // Reset form fields when a column is deselected
      if (isSelected) {
        switch (columnId) {
          case 'Date':
            formik.setFieldValue('dateFrom', '');
            formik.setFieldValue('dateTo', '');
            break;
          case 'tranId':
            formik.setFieldValue('tranId', '');
            break;
          case 'amount':
            formik.setFieldValue('amountFrom', undefined);
            formik.setFieldValue('amountTo', undefined);
            break;
          case 'goAmlTrxCode':
            formik.setFieldValue('goAmlTrxCode', '');
            break;
          case 'custId':
            formik.setFieldValue('custId', '');
            break;
          case 'rptCode':
            formik.setFieldValue('rptCode', '');
            break;
          default:
            break;
        }
      }

      return updatedSelection;
    });
  };

  const handleReset = () => {
    formik.resetForm();
    setSelectedColumns([]); // Clear selected columns
    onReset();
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <form onSubmit={formik.handleSubmit}>
        <DialogTitle>Filter Columns</DialogTitle>
        <Divider />
        <DialogContent>
          <Grid container spacing={2}>
            {/* TRX Date */}
            <Grid item xs={12} sm={4}>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={selectedColumns.includes('Date')}
                    onChange={handleColumnSelectionChange('Date')}
                    color="primary"
                  />
                }
                label="TRX Date"
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={selectedColumns.includes('tranId')}
                    onChange={handleColumnSelectionChange('tranId')}
                    color="primary"
                  />
                }
                label="TRX ID"
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={selectedColumns.includes('amount')}
                    onChange={handleColumnSelectionChange('amount')}
                    color="primary"
                  />
                }
                label="TRX Amount"
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={selectedColumns.includes('goAmlTrxCode')}
                    onChange={handleColumnSelectionChange('goAmlTrxCode')}
                    color="primary"
                  />
                }
                label="GOAML TRX Code"
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={selectedColumns.includes('custId')}
                    onChange={handleColumnSelectionChange('custId')}
                    color="primary"
                  />
                }
                label="Customer ID"
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={selectedColumns.includes('rptCode')}
                    onChange={handleColumnSelectionChange('rptCode')}
                    color="primary"
                  />
                }
                label="RPT Code"
              />
            </Grid>

            {/* Date Fields */}
            {selectedColumns.includes('Date') && (
              <>
                <Grid item xs={12} sm={6}>
                  <TextField
                    label="TRX Date From"
                    type="date"
                    value={formik.values.dateFrom ? formik.values.dateFrom : ''}
                    onChange={(event) => formik.setFieldValue('dateFrom', event.target.value)}
                    error={formik.touched.dateFrom && Boolean(formik.errors.dateFrom)}
                    helperText={formik.touched.dateFrom && formik.errors.dateFrom}
                    fullWidth
                    InputLabelProps={{
                      shrink: true,
                    }}
                    inputProps={{
                      max: new Date().toISOString().split('T')[0],
                    }}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    label="TRX Date To"
                    type="date"
                    value={formik.values.dateTo ? formik.values.dateTo : ''}
                    onChange={(event) => formik.setFieldValue('dateTo', event.target.value)}
                    error={formik.touched.dateTo && Boolean(formik.errors.dateTo)}
                    helperText={formik.touched.dateTo && formik.errors.dateTo}
                    fullWidth
                    InputLabelProps={{
                      shrink: true,
                    }}
                    inputProps={{
                      max: new Date().toISOString().split('T')[0],
                    }}
                  />
                </Grid>
              </>
            )}

            {/* TRX Id */}
            {selectedColumns.includes('tranId') && (
              <Grid item xs={12} sm={6}>
                <TextField
                  label="TRX ID"
                  {...formik.getFieldProps('tranId')}
                  error={formik.touched.tranId && Boolean(formik.errors.tranId)}
                  helperText={formik.touched.tranId && formik.errors.tranId}
                  fullWidth
                />
              </Grid>
            )}

            {/* RPT Code Dropdown */}
            {selectedColumns.includes('rptCode') && (
              <Grid item xs={12} sm={6}>
                <FormControl fullWidth>
                  <InputLabel shrink>RPT Code</InputLabel>
                  {<Autocomplete
                    fullWidth
                    id="reportCodeId"
                    value={
                      goAMLRPTData?.find(
                        (option) => option.rptCodeName === formik.values.rptCode
                      ) || null
                    }
                    onChange={(event, newValue) => {
                      formik.setFieldValue('rptCode', newValue?.rptCodeName || '');
                    }}
                    options={goAMLRPTData || []}
                    getOptionLabel={(item) => `${item.rptCodeName}`}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        InputLabelProps={{ shrink: true }}
                      />
                    )}
                  />}
                  {formik.touched.rptCode && formik.errors.rptCode && (
                    <FormHelperText error>{formik.errors.rptCode}</FormHelperText>
                  )}
                </FormControl>
              </Grid>
            )}

            {/* TRX Amount (From and To in One Line) */}
            {selectedColumns.includes('amount') && (
              <Grid item xs={12}>
                <Grid container spacing={2}>
                  <Grid item xs={6}>
                    <TextField
                      label="TRX Amount From"
                      {...formik.getFieldProps('amountFrom')}
                      error={formik.touched.amountFrom && Boolean(formik.errors.amountFrom)}
                      helperText={formik.touched.amountFrom && formik.errors.amountFrom}
                      fullWidth
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <TextField
                      label="TRX Amount To"
                      {...formik.getFieldProps('amountTo')}
                      error={formik.touched.amountTo && Boolean(formik.errors.amountTo)}
                      helperText={formik.touched.amountTo && formik.errors.amountTo}
                      fullWidth
                    />
                  </Grid>
                </Grid>
              </Grid>
            )}

            {/* GOAML TRX Code Dropdown */}
            {selectedColumns.includes('goAmlTrxCode') && (
              <Grid item xs={12} sm={6}>
                <FormControl fullWidth>
                  <InputLabel shrink>GoAML TRX Code</InputLabel>
                  <Autocomplete
                    fullWidth
                    id="reportCodeId"
                    value={
                      goAMLTrxData?.find(
                        (option) => option.transactionCodeName === formik.values.goAmlTrxCode
                      ) || null
                    }
                    onChange={(event, newValue) => {
                      formik.setFieldValue('goAmlTrxCode', newValue?.transactionCodeName || '');
                    }}
                    options={goAMLTrxData || []}
                    getOptionLabel={(item) => `${item.transactionCodeName}`}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        InputLabelProps={{ shrink: true }}
                      />
                    )}
                  />
                  {formik.touched.goAmlTrxCode && formik.errors.goAmlTrxCode && (
                    <FormHelperText error>{formik.errors.goAmlTrxCode}</FormHelperText>
                  )}
                </FormControl>
              </Grid>
            )}

            {/* Customer ID */}
            {selectedColumns.includes('custId') && (
              <Grid item xs={12} sm={6}>
                <TextField
                  label="Customer ID"
                  {...formik.getFieldProps('custId')}
                  error={formik.touched.custId && Boolean(formik.errors.custId)}
                  helperText={formik.touched.custId && formik.errors.custId}
                  fullWidth
                />
              </Grid>
            )}


          </Grid>
        </DialogContent>


        <DialogActions>
          <Button onClick={onClose}>Cancel</Button>
          <Button onClick={handleReset} color="secondary">
            Clear Filters
          </Button>
          <Button disabled={
            !selectedColumns ||
            (selectedColumns.includes('Date') && (!formik.values.dateFrom || !formik.values.dateTo))
          } type="submit" color="primary" variant="contained">
            Apply
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

export default FilterDialog;
