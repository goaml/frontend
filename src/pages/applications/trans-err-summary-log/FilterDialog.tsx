import {
    Button,
    Checkbox,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    FormControlLabel,
    Grid,
    InputLabel,
    Stack,
    TextField
} from '@mui/material';
import { format } from 'date-fns';
import { useFormik } from 'formik';
import React, { useEffect, useState } from 'react';
import * as Yup from 'yup';

interface FilterValues {
    processDateFrom: string;
    processDateTo: string;
}

interface FilterDialogProps {
    open: boolean;
    onClose: () => void;
    onApplyFilters: (filters: { [key: string]: string }) => void;
    onReset: () => void;
    initialFilters: FilterValues;
}

const validationSchema = Yup.object().shape({
    processDateFrom: Yup.date().nullable(),
    processDateTo: Yup.date()
        .nullable()
        .min(Yup.ref('processDateFrom'), 'To date must be later than from date'),
});

const FilterDialog: React.FC<FilterDialogProps> = ({
    open,
    onClose,
    onApplyFilters,
    onReset,
    initialFilters,
}) => {
    const [selectedColumns, setSelectedColumns] = useState<string[]>([]);

    useEffect(() => {
        const initialSelectedColumns: string[] = [];
        if (initialFilters.processDateFrom || initialFilters.processDateTo) initialSelectedColumns.push('processDate');
        setSelectedColumns(initialSelectedColumns);
    }, [initialFilters]);

    const formik = useFormik({
        initialValues: {
            processDateFrom: initialFilters?.processDateFrom || '',
            processDateTo: initialFilters?.processDateTo || '',
        },
        validationSchema,
        onSubmit: (values) => {
            let errors: { [key: string]: string } = {};
            if (selectedColumns.includes('processDate')) {
                if (!values.processDateFrom)
                    errors.processDateFrom = 'From date is required';
                if (!values.processDateTo)
                    errors.processDateTo = 'To date is required';
            }

            // If errors exist, set them and stop the form submission
            if (Object.keys(errors).length > 0) {
                formik.setErrors(errors);
                return;
            }

            onApplyFilters(values);
            onClose();
        },
    });

    const handleColumnSelectionChange = (logId: string) => () => {
        setSelectedColumns((prevSelected) => {
            const isSelected = prevSelected.includes(logId);
            const updatedSelection = isSelected
                ? prevSelected.filter((col) => col !== logId)
                : [...prevSelected, logId];

            if (isSelected) {
                switch (logId) {
                    case 'processDate':
                        formik.setFieldValue('processDateFrom', '');
                        formik.setFieldValue('processDateTo', '');
                        break
                    default:
                        break;
                }
            }

            return updatedSelection;
        });
    };

    const handleReset = () => {
        formik.resetForm();
        setSelectedColumns([]);
        onReset();
    };

    return (
        <Dialog open={open} onClose={onClose} maxWidth="md" fullWidth>
            <form onSubmit={formik.handleSubmit}>
                <DialogTitle>Filter Columns</DialogTitle>
                <Divider />
                <DialogContent>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <Grid container spacing={2}>
                                <Grid item xs={12} sm={4}>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                checked={selectedColumns.includes('processDate')}
                                                onChange={handleColumnSelectionChange('processDate')}
                                                color="primary"
                                            />
                                        }
                                        label="Process Date"
                                    />
                                </Grid>
                            </Grid>
                        </Grid>

                        <Grid item xs={6}>
                            <Grid container spacing={2}>
                                {selectedColumns.includes('processDate') && (
                                    <>
                                        <Grid item xs={12} sm={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="processDateFrom">
                                                    Process Date From <span style={{ color: 'red' }}>*</span>
                                                </InputLabel>
                                                <TextField
                                                    type="date"
                                                    value={
                                                        formik.values.processDateFrom && !isNaN(new Date(formik.values.processDateFrom).getTime())
                                                            ? format(new Date(formik.values.processDateFrom), "yyyy-MM-dd")
                                                            : format(new Date().setHours(0, 0, 0, 0), "yyyy-MM-dd") // Defaults to today
                                                    }
                                                    onChange={(event) => {
                                                        const newValue = new Date(event.target.value);
                                                        if (!isNaN(newValue.getTime())) {
                                                            const formattedValue = format(newValue, "yyyy-MM-dd");
                                                            formik.setFieldValue('processDateFrom', formattedValue);
                                                        } else {
                                                            console.error('Invalid date selected');
                                                        }
                                                    }}
                                                    error={formik.touched.processDateFrom && Boolean(formik.errors.processDateFrom)}
                                                    helperText={formik.touched.processDateFrom && formik.errors.processDateFrom}
                                                    fullWidth
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="processDateTo">
                                                    Process Date To <span style={{ color: 'red' }}>*</span>
                                                </InputLabel>
                                                <TextField
                                                    type="date"
                                                    value={
                                                        formik.values.processDateTo && !isNaN(new Date(formik.values.processDateTo).getTime())
                                                            ? format(new Date(formik.values.processDateTo), "yyyy-MM-dd")
                                                            : format(new Date().setHours(0, 0, 0, 0), "yyyy-MM-dd") // Defaults to today
                                                    }
                                                    onChange={(event) => {
                                                        const newValue = new Date(event.target.value);
                                                        if (!isNaN(newValue.getTime())) {
                                                            const formattedValue = format(newValue, "yyyy-MM-dd");
                                                            formik.setFieldValue('processDateTo', formattedValue);
                                                        } else {
                                                            console.error('Invalid date selected');
                                                        }
                                                    }}
                                                    error={formik.touched.processDateTo && Boolean(formik.errors.processDateTo)}
                                                    helperText={formik.touched.processDateTo && formik.errors.processDateTo}
                                                    fullWidth
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                />
                                            </Stack>
                                        </Grid>
                                    </>
                                )}
                            </Grid>
                        </Grid>
                    </Grid>
                </DialogContent>

                <DialogActions>
                    <Button onClick={onClose}>Cancel</Button>
                    <Button onClick={handleReset} color="secondary">
                        Clear Filters
                    </Button>
                    <Button
                        disabled={selectedColumns.length === 0}
                        type="submit"
                        color="primary"
                        variant="contained"
                    >
                        Apply
                    </Button>
                </DialogActions>
            </form>
        </Dialog>
    );
};

export default FilterDialog;