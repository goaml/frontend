/* eslint-disable prettier/prettier */
import { EyeTwoTone, FilterTwoTone } from '@ant-design/icons';
// material ui
import {
    Dialog,
    IconButton,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Tooltip,
    useMediaQuery,
    useTheme
} from '@mui/material';

import { PopupTransition } from 'components/@extended/Transitions';
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
// third-party
import { EmptyTable, HeaderSort, TableRowSelection } from 'components/third-party/ReactTable';


import { Box } from '@mui/system';
import { TablePagination } from 'components/third-party/ReactTable2.0';
import { MouseEvent, useEffect, useMemo, useState } from 'react';
import { Cell, Column, HeaderGroup, Row, useExpanded, useFilters, useGlobalFilter, usePagination, useRowSelect, useSortBy, useTable } from 'react-table';

import AddEditTransactionsErrSummary from 'sections/parameter-management/trans-err-summary-log/AddEditTrxErrSummary';
import { dispatch, useSelector } from 'store';
import { openSnackbar } from 'store/reducers/snackbar';
import { fetchTransactionErrorSummaryLog, toInitialState } from 'store/reducers/trans-err-summary-log';
import { listParametersType } from 'types/trans-err-summary-log';
import {
    GlobalFilter
} from 'utils/react-table';
import FilterDialog from './FilterDialog';
import { dataProps, ReactTableProps, TableParamsType } from './type/types';



// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, handleAddEdit, getHeaderProps, pagination, tableParams }: ReactTableProps) {
    const theme = useTheme();

    const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));
    const sortBy = { id: 'id', desc: false };

    const [filterDialogOpen, setFilterDialogOpen] = useState(false);

    const handleFilterDialogOpen = () => {
        setFilterDialogOpen(true);
    };

    const handleFilterDialogClose = () => {
        setFilterDialogOpen(false);
    };


    const handleApplyFilters = (filters: { [key: string]: string }) => {
        console.log(filters, 'filters');

        setFilterDialogOpen(false);
        tableParams.setPage(0);
        tableParams.setProcessDateFrom(filters.processDateFrom);
        tableParams.setProcessDateTo(filters.processDateTo);
        tableParams.setFailReason(filters.failReason);
        tableParams.setLogId(parseInt(filters.logId) || undefined);
        tableParams.setProcessDate(filters.processDate);
        tableParams.setAttemptCode(filters.attemptCode);
        tableParams.setCount(parseInt(filters.count) || undefined);
        dispatch(fetchTransactionErrorSummaryLog({ ...filters, page: 0, per_page: 10, direction: 'asc', sort: 'logId' })); // Apply filters and reset pagination
    };

    const onReset = () => {
        tableParams.setLogId(undefined);
        tableParams.setPage(0);
        tableParams.setProcessDateFrom("");
        tableParams.setProcessDateTo("");
        tableParams.setFailReason("");
        tableParams.setProcessDate("");
        tableParams.setAttemptCode("");
        tableParams.setCount(undefined);
        dispatch(fetchTransactionErrorSummaryLog({ page: 0, per_page: 10, direction: 'asc', sort: 'logId' }));
    }

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        rows,
        page,
        gotoPage,
        setPageSize,
        state: { globalFilter, selectedRowIds, pageIndex, pageSize },
        preGlobalFilteredRows,
        setGlobalFilter,
    } = useTable(
        {
            columns,
            data,
            initialState: { pageIndex: tableParams.page, pageSize: 10, hiddenColumns: ['avatar'], sortBy: [sortBy] },
            manualPagination: true, // Enable manual pagination
            pageCount: Math.ceil(pagination.total / tableParams.perPage),
        },
        useGlobalFilter,
        useFilters,
        useSortBy,
        useExpanded,
        usePagination,
        useRowSelect
    );


    return (
        <>
            <TableRowSelection selected={Object.keys(selectedRowIds).length} />
            <Stack spacing={3}>
                <Stack
                    direction={matchDownSM ? 'column' : 'row'}
                    spacing={1}
                    justifyContent="space-between"
                    alignItems="center"
                    sx={{ p: 3, pb: 0 }}
                >
                    <GlobalFilter
                        preGlobalFilteredRows={preGlobalFilteredRows}
                        globalFilter={globalFilter}
                        setGlobalFilter={setGlobalFilter}
                        size="small"
                    />
                    <Stack direction={matchDownSM ? 'column' : 'row'} alignItems="center" spacing={1}>
                        <Tooltip title="Filter Columns">
                            <IconButton onClick={handleFilterDialogOpen}>
                                <FilterTwoTone />
                            </IconButton>
                        </Tooltip>
                    </Stack>
                </Stack>
                <Box sx={{ display: 'flex', flexDirection: 'column', height: '100%', width: '100%', justifyContent: 'space-between' }}>
                    <Box sx={{ flex: '1 1 auto', overflowY: 'auto' }}>
                        <Table {...getTableProps()}>
                            <TableHead>
                                {headerGroups.map((headerGroup: HeaderGroup<{}>) => (
                                    <TableRow
                                        {...headerGroup.getHeaderGroupProps()}
                                        sx={{ '& > th:first-of-type': { width: '58px' } }}
                                    >
                                        {headerGroup.headers.map((column: HeaderGroup) => (
                                            <TableCell
                                                {...column.getHeaderProps([{ className: column.className }, getHeaderProps(column)])}
                                            >
                                                <HeaderSort column={column} />
                                            </TableCell>
                                        ))}
                                    </TableRow>
                                ))}
                            </TableHead>
                            <TableBody {...getTableBodyProps()}>
                                {page.length > 0 ? (
                                    page.map((row, i) => {
                                        prepareRow(row);
                                        return (
                                            <TableRow {...row.getRowProps()}>
                                                {row.cells.map((cell: Cell) => (
                                                    <TableCell
                                                        {...cell.getCellProps([{ className: cell.column.className }])}
                                                    >
                                                        {cell.render('Cell')}
                                                    </TableCell>
                                                ))}
                                            </TableRow>
                                        );
                                    })
                                ) : (
                                    <EmptyTable msg="No Data" colSpan={12} />
                                )}
                            </TableBody>
                        </Table>
                    </Box>
                </Box>
                <Box sx={{ p: 1 }}>
                    <TablePagination
                        pageIndex={pageIndex}
                        pageSize={pageSize}
                        setPageSize={setPageSize}
                        gotoPage={gotoPage}
                        rows={rows}
                        totalRows={pagination.total}
                        tableParams={tableParams}
                    />
                </Box>

            </Stack>
            <FilterDialog
                open={filterDialogOpen}
                onClose={handleFilterDialogClose}
                onApplyFilters={handleApplyFilters}
                onReset={onReset}
                initialFilters={{
                    processDateFrom: tableParams.processDateFrom,
                    processDateTo: tableParams.processDateTo,

                }}
            />

        </>
    );
}

// ==============================|| List ||============================== //

const ErrorLog = () => {
    const theme = useTheme();

    const { transErrSummaryLogList, error, success, isLoading } = useSelector(state => state.transactionErrorSummaryLog);

    const [data, setData] = useState<dataProps[]>([])


    // ==============================|| Pagination-Config ||============================== //

    const [Pagination, setPagination] = useState<any>([])
    const [page, setPage] = useState<number>(0);
    const [perPage, setPerPage] = useState<number>(10);
    const [direction, setDirection] = useState<"asc" | "desc">("asc");
    const [search, setSearch] = useState<string>("");
    const [sort, setSort] = useState<string>("");

    const [processDateFrom, setProcessDateFrom] = useState<string>("");
    const [processDateTo, setProcessDateTo] = useState<string>("");
    const [logId, setLogId] = useState<number>();
    const [failReason, setFailReason] = useState<string>("");
    const [attemptCode, setAttemptCode] = useState<string>("");
    const [count, setCount] = useState<number>();
    const [processDate, setProcessDate] = useState<string>("");

    const tableParams: TableParamsType = {
        page,
        setPage,
        perPage,
        setPerPage,
        direction,
        setDirection,
        sort,
        setSort,
        search,
        setSearch,
        processDateFrom,
        setProcessDateFrom,
        processDateTo,
        setProcessDateTo,
        failReason,
        setFailReason,
        logId,
        setLogId,
        processDate,
        setProcessDate,
        attemptCode,
        setAttemptCode,
        count,
        setCount
    }

    const columns = useMemo(
        () =>
            [
                {
                    Header: '#',
                    accessor: 'id',
                    className: 'cell-center',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.id === undefined || row.id === null || row.id === '') {
                            return <>-</>
                        }
                        if (typeof row.id === 'string') {
                            return <>{(parseInt(row.id) + 1).toString()}</>;
                        }
                        if (typeof row.id === 'number') {
                            return <>{row.id + 1}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Attempt Code',
                    accessor: 'attemptCode',
                    id: 'attemptCode',
                    Cell: ({ value }) => (value ? value : '-')
                },
                {
                    Header: 'Process Date',
                    accessor: 'processDate',
                    id: 'processDate',
                    Cell: ({ value }) => {
                        if (!value) return '-';
                
                        const date = new Date(value);
                
                        // Format to YYYY-MM-DD
                        const formattedDate = `${date.getFullYear()}-${String(date.getMonth() + 1).padStart(2, '0')}-${String(date.getDate()).padStart(2, '0')}`;
                
                        return formattedDate;
                    }
                },                
                {
                    Header: 'Failed Description',
                    accessor: 'failReason',
                    id: 'failReason',
                    Cell: ({ value }) => (value ? value : '-')
                },
                {
                    Header: 'Failed Count',
                    accessor: 'count',
                    id: 'count',
                    Cell: ({ value }) => (value ? value : '-')
                },
                {
                    id: "actions",
                    Header: 'Actions',
                    accessor: 'actions',
                    className: 'cell-center',
                    Cell: ({ row }: { row: Row }) => {
                        const data: dataProps = row.original;
                        return (
                            <>
                                <Stack direction="row" alignItems="center" justifyContent="center" spacing={0}>
                                    <Tooltip title="View">
                                        <IconButton
                                            color="secondary"
                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                e.stopPropagation();
                                                handleAddEdit()
                                                setUser({
                                                    ...data
                                                })
                                                console.log(data, 'data')
                                            }}
                                        >
                                            <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
                                        </IconButton>
                                    </Tooltip>
                                </Stack>
                            </>
                        )
                    }
                }

            ] as Column[],
        [tableParams.page, tableParams.perPage]
    );

    const [addEdit, setAddEdit] = useState<boolean>(false);
    const [user, setUser] = useState<dataProps>();

    const handleAddEdit = () => {
        setAddEdit(!addEdit);
        if (user && !addEdit) setUser(undefined);

    };

    // ==============================|| API-Config ||============================== //


    useEffect(() => {
        const listParameters: listParametersType = {
            page: page,
            per_page: perPage,
            direction: 'asc',
            search: search,
            sort: "logId",
            processDateFrom: processDateFrom,
            processDateTo: processDateTo,
            logId: logId,
        };
        dispatch(fetchTransactionErrorSummaryLog(listParameters));
    }, [dispatch, success, page, perPage, direction, search, sort]);

    useEffect(() => {
        setData(transErrSummaryLogList?.result! || []);
        setPagination(transErrSummaryLogList?.pagination! || {})
    }, [transErrSummaryLogList])

    useEffect(() => {
        if (error != null && typeof error === 'object' && 'message' in error) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: (error as { message: string }).message, // Type assertion
                    variant: 'alert',
                    alert: {
                        color: 'error'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [error]);

    //  handel success
    useEffect(() => {
        if (success != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: success,
                    variant: 'alert',
                    alert: {
                        color: 'success'
                    },
                    close: true
                })
            );
            dispatch(toInitialState())
        }
    }, [success])

    if (isLoading) {
        return <div>Loading...</div>;
    }



    return (
        <>
            <MainCard content={false}>
                <ScrollX>
                    <ReactTable columns={columns}
                        getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()}
                        data={data} handleAddEdit={handleAddEdit}
                        tableParams={tableParams} pagination={Pagination}
                    />
                </ScrollX>
                {/* add / edit user dialog */}
                <Dialog
                    maxWidth="sm"
                    TransitionComponent={PopupTransition}
                    keepMounted
                    fullWidth
                    onClose={handleAddEdit}
                    open={addEdit}
                    sx={{ '& .MuiDialog-paper': { p: 0 }, transition: 'transform 225ms' }}
                    aria-describedby="alert-dialog-slide-description"
                >
                    <AddEditTransactionsErrSummary user={user} onCancel={handleAddEdit} />
                </Dialog>
            </MainCard>
        </>
    )


};



export default ErrorLog;

