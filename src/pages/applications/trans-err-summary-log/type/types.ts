import { Dispatch, SetStateAction } from 'react';
import { Column, HeaderGroup } from 'react-table';

export interface dataProps {
    processDate?: string,
    failReason?: string,
    attemptCode?: string
    count?: number | undefined
}

export interface ReactTableProps {
    columns: Column[];
    data: dataProps[];
    handleAddEdit: () => void;
    getHeaderProps: (column: HeaderGroup) => {};
    tableParams: TableParamsType;
    pagination: any;
}

export interface TableHeaderProps {
    headerGroups: HeaderGroup[];
}

export interface transactionErrSummaryProps {
    processDate?: string,
    failReason?: string,
    attemptCode?: string,
    count?: number| undefined
}

export interface TableParamsType {
    page: number;
    setPage: Dispatch<SetStateAction<number>>;
    perPage: number;
    setPerPage: Dispatch<SetStateAction<number>>;
    direction: "asc" | "desc";
    setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
    sort: string;
    setSort: Dispatch<SetStateAction<string>>;
    search: string;
    setSearch: Dispatch<SetStateAction<string>>;
    logId?: number
    setLogId: Dispatch<SetStateAction<number | undefined>>;
    processDateFrom: string;
    setProcessDateFrom: Dispatch<SetStateAction<string>>;
    processDateTo: string;
    setProcessDateTo: Dispatch<SetStateAction<string>>;
    processDate: string;
    setProcessDate: Dispatch<SetStateAction<string>>;
    failReason: string,
    setFailReason: Dispatch<SetStateAction<string>>;
    attemptCode: string,
    setAttemptCode:Dispatch<SetStateAction<string>>;
    count: number | undefined,
    setCount: Dispatch<SetStateAction<number | undefined>>;
}
