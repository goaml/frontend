import { useEffect, useMemo, useState } from 'react';

// material-ui
import {
  Box,
  IconButton,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
  useMediaQuery,
  Typography,
  Link,
  Breadcrumbs,
  useTheme
} from '@mui/material';

// third-party
import { Cell, Column, HeaderGroup, Row, useFilters, useGlobalFilter, usePagination, useSortBy, useTable } from 'react-table';

// project import
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import { HeaderSort, EmptyTable, TablePagination } from 'components/third-party/ReactTable2.0';

// utils
import {
  GlobalFilter
} from 'utils/react-table';

// sections

// assets

//types 
import { FilterTwoTone } from '@ant-design/icons';
import { dispatch, useDispatch, useSelector } from 'store';
import { fetchtransactioncode_summary_monthlyList, toInitialState } from 'store/reducers/transactioncode_summary_monthly';
import { openSnackbar } from 'store/reducers/snackbar';
import { listParametersType } from 'types/transactioncode_summary_monthly';
import FilterDialog from './FilterDialog';
import { ReactTableProps, TableParamsType, dataProps } from './types/types';

// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, tableParams, pagination, getHeaderProps }: ReactTableProps) {

  const theme = useTheme();

  const [filterDialogOpen, setFilterDialogOpen] = useState(false);

  const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));

  const sortBy = { id: 'id', desc: false };


  const handleFilterDialogOpen = () => {
    setFilterDialogOpen(true);
  };

  const handleFilterDialogClose = () => {
    setFilterDialogOpen(false);
  };


  const handleApplyFilters = (filters: { [key: string]: string }) => {
    console.log(filters, 'filters');

    setFilterDialogOpen(false);
    tableParams.setPage(0);
    tableParams.setToYear(filters.toYear);
    tableParams.setFromYear(filters.fromYear);
    tableParams.setCustId(filters.custId);
    tableParams.setToMonth(filters.toMonth);
    tableParams.setFromMonth(filters.fromMonth);

    dispatch(fetchtransactioncode_summary_monthlyList({ ...filters, page: 0, per_page: 10, direction: 'asc', sort: 'id' })); // Apply filters and reset pagination
  };

  const onReset = () => {
    tableParams.setToYear("");
    tableParams.setFromYear("");
    tableParams.setCustId("");
    tableParams.setToMonth("");
    tableParams.setFromMonth("");
    tableParams.setPage(0);

    dispatch(fetchtransactioncode_summary_monthlyList({ page: 0, per_page: 10, direction: 'asc', sort: 'id' }));
  }

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    preGlobalFilteredRows,
    setGlobalFilter,
    globalFilter,
    page,
    gotoPage,
    setPageSize,
    state: { pageIndex, pageSize }
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: tableParams.page, pageSize: 10, hiddenColumns: ['avatar'], sortBy: [sortBy] },
      manualPagination: true, // Enable manual pagination
      pageCount: Math.ceil(pagination.total / tableParams.perPage),
    },
    useGlobalFilter,
    useFilters,
    useSortBy,
    usePagination
  );

  return (

    <>
          <Stack spacing={1} sx={{ padding: 2 }}>
        <Breadcrumbs aria-label="breadcrumb">
          <Link underline="hover" color="inherit" href="/">
            Home
          </Link>
          <Typography fontWeight="bold" color="text.primary">
            Transaction Code Summary Monthly
          </Typography>
        </Breadcrumbs>

        <Typography variant="h4" fontWeight="bold">
        Transaction Code Summary Monthly
        </Typography>
      </Stack>

      <Stack direction="row" spacing={2} justifyContent="space-between" sx={{ padding: 2 }}>
        <GlobalFilter preGlobalFilteredRows={preGlobalFilteredRows} globalFilter={globalFilter} setGlobalFilter={setGlobalFilter} />
        <Stack direction="row" alignItems="center" spacing={1}>

          <Stack direction={matchDownSM ? 'column' : 'row'} alignItems="center" spacing={1}>
            <Tooltip title="Filter Columns">
              <IconButton onClick={handleFilterDialogOpen}>
                <FilterTwoTone />
              </IconButton>
            </Tooltip>
          </Stack>
        </Stack>
      </Stack>

      <Table {...getTableProps()}>
        <TableHead sx={{ borderTopWidth: 2 }}>
          {headerGroups.map((headerGroup) => (
            <TableRow {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column: HeaderGroup) => (
                <TableCell {...column.getHeaderProps([{ className: column.className }, getHeaderProps(column)])}>
                  <HeaderSort column={column} /> {/* Sorting icons added only once */}
                </TableCell>
              ))}
            </TableRow>
          ))}
        </TableHead>
        <TableBody {...getTableBodyProps()}>
          {page.length > 0 ? (
            page.map((row, i) => {
              prepareRow(row);
              return (
                <TableRow {...row.getRowProps()}>
                  {row.cells.map((cell: Cell) => (
                    <TableCell {...cell.getCellProps([{ className: cell.column.className }])}>{cell.render('Cell')}</TableCell>
                  ))}
                </TableRow>
              );
            })
          ) : (
            <EmptyTable msg="No Data" colSpan={12} />
          )}
          <TableRow>
            <TableCell sx={{ p: 2 }} colSpan={12}>
              <Box sx={{ p: 1 }}>
                <TablePagination
                  pageIndex={pageIndex}
                  pageSize={pageSize}
                  setPageSize={setPageSize}
                  gotoPage={gotoPage}
                  rows={rows}
                  totalRows={pagination.total}
                  tableParams={tableParams}
                />
              </Box>
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>

      <FilterDialog
        open={filterDialogOpen}
        onClose={handleFilterDialogClose}
        onApplyFilters={handleApplyFilters}
        onReset={onReset}
        initialFilters={{
          toYear: tableParams.toYear,
          fromYear: tableParams.fromYear,
          custId: tableParams.custId,
          toMonth: tableParams.toMonth,
          fromMonth: tableParams.fromMonth,
        }}
      />
    </>
  );
}

// ==============================|| List ||============================== //

const List = () => {
  const dispatch = useDispatch();
  const { transactioncode_summary_monthlyList, isLoading, error, success } = useSelector(state => state.transactioncode_summary_monthly);


  // table
  const [data, setData] = useState<dataProps[]>([])

  const columns = useMemo(
    () =>
      [
        {
          Header: '#',
          accessor: 'id',
          className: 'cell-center',
          Cell: ({ row }: { row: Row }) => {
            if (row.id === undefined || row.id === null || row.id === '') {
              return <>-</>
            }
            if (typeof row.id === 'string') {
              return <>{(parseInt(row.id) + 1).toString()}</>;
            }
            if (typeof row.id === 'number') {
              return <>{row.id + 1}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },
        {
          Header: 'Customer ID',
          accessor: 'custId',
          Cell: ({ row }: { row: Row }) => {
            if (row.values.custId === undefined || row.values.custId === null || row.values.custId === '') {
              return <>-</>
            }
            if (typeof row.values.custId === 'string') {
              return <>{row.values.custId}</>;
            }
            if (typeof row.values.custId === 'number') {
              return <>{row.values.custId}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },

        {
          Header: 'Month',
          accessor: 'month',
          id: 'month',
          Cell: ({ value }) => {
            if (!value) return '-';
            const monthNames = [
              'January', 'February', 'March', 'April', 'May', 'June',
              'July', 'August', 'September', 'October', 'November', 'December'
            ];
            return monthNames[parseInt(value, 10) - 1] || '-';
          },
        },

        {
          Header: 'Year',
          accessor: 'year',
          Cell: ({ row }: { row: Row }) => {
            if (row.values.year === undefined || row.values.year === null || row.values.year === '') {
              return <>-</>
            }
            if (typeof row.values.year === 'string') {
              return <>{row.values.year}</>;
            }
            if (typeof row.values.year === 'number') {
              return <>{row.values.year}</>;
            }
            // Handle any other data types if necessary
            return <>-</>;
          }
        },

        {
        Header: 'Trx code (RPT CODE)',
        accessor: 'rptCode',
        Cell: ({ row }: { row: Row }) => {
          if (row.values.rptCode === undefined || row.values.rptCode === null || row.values.rptCode === '') {
            return <>-</>
          }
          if (typeof row.values.rptCode === 'string') {
            return <>{row.values.rptCode}</>;
          }
          if (typeof row.values.rptCode === 'number') {
            return <>{row.values.rptCode}</>;
          }
          // Handle any other data types if necessary
          return <>-</>;
        }
      },

      {
        Header: 'Total Debit Amount',
        accessor: 'totalDebitAmt',

        Cell: ({ row }: { row: Row }) => {
          const formatNumber = (value: number) => {
            return value.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 });
          };

          if (row.values.totalDebitAmt === undefined || row.values.totalDebitAmt === null || row.values.totalDebitAmt === '') {
            return <div style={{ textAlign: 'right' }}>-</div>;
          }

          if (typeof row.values.totalDebitAmt === 'number') {
            return <div style={{ textAlign: 'right' }}>{formatNumber(row.values.totalDebitAmt)}</div>;
          }

          if (typeof row.values.totalDebitAmt === 'string') {
            const numValue = parseFloat(row.values.totalDebitAmt);
            if (!isNaN(numValue)) {
              return <div style={{ textAlign: 'right' }}>{formatNumber(numValue)}</div>;
            }
            return <div style={{ textAlign: 'right' }}>{row.values.totalDebitAmt}</div>;
          }

          return <div style={{ textAlign: 'right' }}>-</div>;
        }
      },

      {
        Header: 'Total Credit Amount',
        accessor: 'totalCreditAmt',

        Cell: ({ row }: { row: Row }) => {
          const formatNumber = (value: number) => {
            return value.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 });
          };

          if (row.values.totalCreditAmt === undefined || row.values.totalCreditAmt === null || row.values.totalCreditAmt === '') {
            return <div style={{ textAlign: 'right' }}>-</div>;
          }

          if (typeof row.values.totalCreditAmt === 'number') {
            return <div style={{ textAlign: 'right' }}>{formatNumber(row.values.totalCreditAmt)}</div>;
          }

          if (typeof row.values.totalCreditAmt === 'string') {
            const numValue = parseFloat(row.values.totalCreditAmt);
            if (!isNaN(numValue)) {
              return <div style={{ textAlign: 'right' }}>{formatNumber(numValue)}</div>;
            }
            return <div style={{ textAlign: 'right' }}>{row.values.totalCreditAmt}</div>;
          }

          return <div style={{ textAlign: 'right' }}>-</div>;
        }
      },


        {
            Header: 'Total Net Amount',
            accessor: 'totNetAmt',
  
            Cell: ({ row }: { row: Row }) => {
              const formatNumber = (value: number) => {
                return value.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 });
              };
  
              if (row.values.totNetAmt === undefined || row.values.totNetAmt === null || row.values.totNetAmt === '') {
                return <div style={{ textAlign: 'right' }}>-</div>;
              }
  
              if (typeof row.values.totNetAmt === 'number') {
                return <div style={{ textAlign: 'right' }}>{formatNumber(row.values.totNetAmt)}</div>;
              }
  
              if (typeof row.values.totNetAmt === 'string') {
                const numValue = parseFloat(row.values.totNetAmt);
                if (!isNaN(numValue)) {
                  return <div style={{ textAlign: 'right' }}>{formatNumber(numValue)}</div>;
                }
                return <div style={{ textAlign: 'right' }}>{row.values.totNetAmt}</div>;
              }
  
              return <div style={{ textAlign: 'right' }}>-</div>;
            }
          },
  
          {
            Header: 'Debit Average',
            accessor: 'debitAverage',
  
            Cell: ({ row }: { row: Row }) => {
              const formatNumber = (value: number) => {
                return value.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 });
              };
  
              if (row.values.debitAverage === undefined || row.values.debitAverage === null || row.values.debitAverage === '') {
                return <div style={{ textAlign: 'right' }}>-</div>;
              }
  
              if (typeof row.values.debitAverage === 'number') {
                return <div style={{ textAlign: 'right' }}>{formatNumber(row.values.debitAverage)}</div>;
              }
  
              if (typeof row.values.debitAverage === 'string') {
                const numValue = parseFloat(row.values.debitAverage);
                if (!isNaN(numValue)) {
                  return <div style={{ textAlign: 'right' }}>{formatNumber(numValue)}</div>;
                }
                return <div style={{ textAlign: 'right' }}>{row.values.debitAverage}</div>;
              }
  
              return <div style={{ textAlign: 'right' }}>-</div>;
            }
          },

          {
            Header: 'Credit Average',
            accessor: 'creditAverage',
  
            Cell: ({ row }: { row: Row }) => {
              const formatNumber = (value: number) => {
                return value.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 });
              };
  
              if (row.values.creditAverage === undefined || row.values.creditAverage === null || row.values.creditAverage === '') {
                return <div style={{ textAlign: 'right' }}>-</div>;
              }
  
              if (typeof row.values.creditAverage === 'number') {
                return <div style={{ textAlign: 'right' }}>{formatNumber(row.values.creditAverage)}</div>;
              }
  
              if (typeof row.values.creditAverage === 'string') {
                const numValue = parseFloat(row.values.creditAverage);
                if (!isNaN(numValue)) {
                  return <div style={{ textAlign: 'right' }}>{formatNumber(numValue)}</div>;
                }
                return <div style={{ textAlign: 'right' }}>{row.values.creditAverage}</div>;
              }
  
              return <div style={{ textAlign: 'right' }}>-</div>;
            }
          },

          {
            Header: 'Max Debit Amount',
            accessor: 'maxDebitAmt',
  
            Cell: ({ row }: { row: Row }) => {
              const formatNumber = (value: number) => {
                return value.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 });
              };
  
              if (row.values.maxDebitAmt === undefined || row.values.maxDebitAmt === null || row.values.maxDebitAmt === '') {
                return <div style={{ textAlign: 'right' }}>-</div>;
              }
  
              if (typeof row.values.maxDebitAmt === 'number') {
                return <div style={{ textAlign: 'right' }}>{formatNumber(row.values.maxDebitAmt)}</div>;
              }
  
              if (typeof row.values.maxDebitAmt === 'string') {
                const numValue = parseFloat(row.values.maxDebitAmt);
                if (!isNaN(numValue)) {
                  return <div style={{ textAlign: 'right' }}>{formatNumber(numValue)}</div>;
                }
                return <div style={{ textAlign: 'right' }}>{row.values.maxDebitAmt}</div>;
              }
  
              return <div style={{ textAlign: 'right' }}>-</div>;
            }
          },

          {
            Header: 'Max Credit Amount',
            accessor: 'maxCreditAmt',
  
            Cell: ({ row }: { row: Row }) => {
              const formatNumber = (value: number) => {
                return value.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 });
              };
  
              if (row.values.maxCreditAmt === undefined || row.values.maxCreditAmt === null || row.values.maxCreditAmt === '') {
                return <div style={{ textAlign: 'right' }}>-</div>;
              }
  
              if (typeof row.values.maxCreditAmt === 'number') {
                return <div style={{ textAlign: 'right' }}>{formatNumber(row.values.maxCreditAmt)}</div>;
              }
  
              if (typeof row.values.maxCreditAmt === 'string') {
                const numValue = parseFloat(row.values.maxCreditAmt);
                if (!isNaN(numValue)) {
                  return <div style={{ textAlign: 'right' }}>{formatNumber(numValue)}</div>;
                }
                return <div style={{ textAlign: 'right' }}>{row.values.maxCreditAmt}</div>;
              }
  
              return <div style={{ textAlign: 'right' }}>-</div>;
            }
          },

          {
            Header: 'Min Debit Amount',
            accessor: 'minDebitAmt',
  
            Cell: ({ row }: { row: Row }) => {
              const formatNumber = (value: number) => {
                return value.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 });
              };
  
              if (row.values.minDebitAmt === undefined || row.values.minDebitAmt === null || row.values.minDebitAmt === '') {
                return <div style={{ textAlign: 'right' }}>-</div>;
              }
  
              if (typeof row.values.minDebitAmt === 'number') {
                return <div style={{ textAlign: 'right' }}>{formatNumber(row.values.minDebitAmt)}</div>;
              }
  
              if (typeof row.values.minDebitAmt === 'string') {
                const numValue = parseFloat(row.values.minDebitAmt);
                if (!isNaN(numValue)) {
                  return <div style={{ textAlign: 'right' }}>{formatNumber(numValue)}</div>;
                }
                return <div style={{ textAlign: 'right' }}>{row.values.minDebitAmt}</div>;
              }
  
              return <div style={{ textAlign: 'right' }}>-</div>;
            }
          },

          {
            Header: 'Min Credit Amount',
            accessor: 'minCreditAmt',
  
            Cell: ({ row }: { row: Row }) => {
              const formatNumber = (value: number) => {
                return value.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 });
              };
  
              if (row.values.minCreditAmt === undefined || row.values.minCreditAmt === null || row.values.minCreditAmt === '') {
                return <div style={{ textAlign: 'right' }}>-</div>;
              }
  
              if (typeof row.values.minCreditAmt === 'number') {
                return <div style={{ textAlign: 'right' }}>{formatNumber(row.values.minCreditAmt)}</div>;
              }
  
              if (typeof row.values.minCreditAmt === 'string') {
                const numValue = parseFloat(row.values.minCreditAmt);
                if (!isNaN(numValue)) {
                  return <div style={{ textAlign: 'right' }}>{formatNumber(numValue)}</div>;
                }
                return <div style={{ textAlign: 'right' }}>{row.values.minCreditAmt}</div>;
              }
  
              return <div style={{ textAlign: 'right' }}>-</div>;
            }
          },

        {
          Header: 'No of Debit Transactions',
          accessor: 'noOfDebitTransactions',

          Cell: ({ row }: { row: Row }) => {
            const formatNumber = (value: number) => {
              return value.toLocaleString(undefined, { minimumFractionDigits: 0, maximumFractionDigits: 0 });
            };

            if (row.values.noOfDebitTransactions === undefined || row.values.noOfDebitTransactions === null || row.values.noOfDebitTransactions === '') {
              return <div style={{ textAlign: 'right' }}>-</div>;
            }

            if (typeof row.values.noOfDebitTransactions === 'number') {
              return <div style={{ textAlign: 'right' }}>{formatNumber(row.values.noOfDebitTransactions)}</div>;
            }

            if (typeof row.values.noOfDebitTransactions === 'string') {
              const numValue = parseFloat(row.values.noOfDebitTransactions);
              if (!isNaN(numValue)) {
                return <div style={{ textAlign: 'right' }}>{formatNumber(numValue)}</div>;
              }
              return <div style={{ textAlign: 'right' }}>{row.values.noOfDebitTransactions}</div>;
            }

            return <div style={{ textAlign: 'right' }}>-</div>;
          }
        },

        {
          Header: 'No of Credit Transactions',
          accessor: 'noOfCreditTransactions',

          Cell: ({ row }: { row: Row }) => {
            const formatNumber = (value: number) => {
              return value.toLocaleString(undefined, { minimumFractionDigits: 0, maximumFractionDigits: 0 });
            };

            if (row.values.totNetAmt === undefined || row.values.totNetAmt === null || row.values.totNetAmt === '') {
              return <div style={{ textAlign: 'right' }}>-</div>;
            }

            if (typeof row.values.totNetAmt === 'number') {
              return <div style={{ textAlign: 'right' }}>{formatNumber(row.values.totNetAmt)}</div>;
            }

            if (typeof row.values.totNetAmt === 'string') {
              const numValue = parseFloat(row.values.totNetAmt);
              if (!isNaN(numValue)) {
                return <div style={{ textAlign: 'right' }}>{formatNumber(numValue)}</div>;
              }
              return <div style={{ textAlign: 'right' }}>{row.values.totNetAmt}</div>;
            }

            return <div style={{ textAlign: 'right' }}>-</div>;
          }
        },

        {
          Header: 'No of Transactions',
          accessor: 'noOfTransactions',

          Cell: ({ row }: { row: Row }) => {
            const formatNumber = (value: number) => {
              return value.toLocaleString(undefined, { minimumFractionDigits: 0, maximumFractionDigits: 0 });
            };

            if (row.values.noOfTransactions === undefined || row.values.noOfTransactions === null || row.values.noOfTransactions === '') {
              return <div style={{ textAlign: 'right' }}>-</div>;
            }

            if (typeof row.values.noOfTransactions === 'number') {
              return <div style={{ textAlign: 'right' }}>{formatNumber(row.values.noOfTransactions)}</div>;
            }

            if (typeof row.values.noOfTransactions === 'string') {
              const numValue = parseFloat(row.values.noOfTransactions);
              if (!isNaN(numValue)) {
                return <div style={{ textAlign: 'right' }}>{formatNumber(numValue)}</div>;
              }
              return <div style={{ textAlign: 'right' }}>{row.values.noOfTransactions}</div>;
            }

            return <div style={{ textAlign: 'right' }}>-</div>;
          }
        },

        // {
        //   id: "actions",
        //   Header: 'Actions',
        //   accessor: 'actions',
        //   className: 'cell-center',
        //   Cell: ({ row }: { row: Row }) => {

        //     return (
        //       <>
        //         <Stack direction="row" alignItems="center" justifyContent="center" spacing={0}>
        //           {/* <Tooltip title="View">
        //                                 <IconButton
        //                                     color="secondary"
        //                                     onClick={(e: MouseEvent<HTMLButtonElement>) => {
        //                                         e.stopPropagation();
        //                                     }}
        //                                 >
        //                                     <EyeTwoTone twoToneColor={theme.palette.secondary.main} />
        //                                 </IconButton>
        //                             </Tooltip> */}

        //         </Stack>
        //       </>
        //     )
        //   }
        // }
      ] as Column[],
    []
  );

  //================================API CONFIG=============================//

  const [page, setPage] = useState<number>(0);
  const [perPage, setPerPage] = useState<number>(10);
  const [direction, setDirection] = useState<"asc" | "desc">("asc");
  const [sort, setSort] = useState<string>("summaryLogId"); //CHANGE ID

  const [toYear, setToYear] = useState<string>("");
  const [fromYear, setFromYear] = useState<string>("");
  const [custId, setCustId] = useState<string>("");
  const [toMonth, setToMonth] = useState<string>("");
  const [fromMonth, setFromMonth] = useState<string>("");
  const [Pagination, setPagination] = useState<any>([])
  const [search, setSearch] = useState<string>("");
  const [rptCode,setRptCode] = useState<string>("");

  const tableParams: TableParamsType = {
    page,
    setPage,
    perPage,
    setPerPage,
    direction,
    setDirection,
    sort,
    setSort,
    search,
    setSearch,
    toYear,
    setToYear,
    fromYear,
    setFromYear,
    custId,
    setCustId,
    toMonth,
    setToMonth,
    fromMonth,
    setFromMonth,
    rptCode,
    setRptCode
  }

  useEffect(() => {
    const listParameters: listParametersType = {
      page: page,
      per_page: perPage,
      direction: direction,
      sort: sort,
      search: search,
      toYear: toYear,
      fromYear: fromYear,
      toMonth: toMonth,
      fromMonth: fromMonth,
      custId: custId,
      rptCode: rptCode
    };
    dispatch(fetchtransactioncode_summary_monthlyList(listParameters));
  }, [dispatch, success, page, perPage, direction, sort]);

  useEffect(() => {
    setData(transactioncode_summary_monthlyList?.result! || []);
    setPagination(transactioncode_summary_monthlyList?.pagination! || {})
  }, [transactioncode_summary_monthlyList])

  //  handel error 
  useEffect(() => {
    if (error != null && typeof error === 'object' && 'message' in error) {
      dispatch(
        openSnackbar({
          open: true,
          message: (error as { message: string }).message, // Type assertion
          variant: 'alert',
          alert: {
            color: 'error'
          },
          close: true
        })
      );
      dispatch(toInitialState());
    }
  }, [error]);

  //  handel success
  useEffect(() => {
    if (success != null) {
      dispatch(
        openSnackbar({
          open: true,
          message: success,
          variant: 'alert',
          alert: {
            color: 'success'
          },
          close: true
        })
      );
      dispatch(toInitialState())
    }
  }, [success])

  if (isLoading) {
    return <div>Loading...</div>;
  }


  return (
    <>
      <MainCard content={false}>
        <ScrollX>
          <ReactTable columns={columns} data={data} tableParams={tableParams} pagination={Pagination} getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()} />
        </ScrollX>

      </MainCard>
    </>
  );
}

export default List;
