import {
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  FormControlLabel,
  Grid,
  TextField
} from '@mui/material';
import React, { useEffect, useState } from 'react';
// Import DatePicker
import { useFormik } from 'formik'; // Import Formik for form handling
import * as Yup from 'yup'; // Import Yup for validation

interface FilterValues {
  fromYear?: string;
  toYear?: string;
  custId?: string;
  fromMonth?: string;
  toMonth?: string;

}

interface FilterDialogProps {
  open: boolean;
  onClose: () => void;
  onApplyFilters: (filters: { [key: string]: string }) => void;
  onReset: () => void;
  initialFilters: FilterValues;
}

// Define a simplified Yup validation schema
const validationSchema = Yup.object().shape({
  fromMonth: Yup.date().nullable(),
  toMonth: Yup.date()
    .nullable()
    .min(Yup.ref('fromMonth'), 'To Month can\'t be before from Month'),
  fromYear: Yup.date().nullable(),
  toYear: Yup.number()
    .nullable()
    .min(Yup.ref('fromYear'), 'To Year can\'t be before from Year'),
  custId: Yup.string(),
});

const FilterDialog: React.FC<FilterDialogProps> = ({
  open,
  onClose,
  onApplyFilters,
  onReset,
  initialFilters
}) => {
  const [selectedColumns, setSelectedColumns] = useState<string[]>([]);
  // Track multiple selected columns
  useEffect(() => {
    const initialSelectedColumns: string[] = [];
    if (initialFilters.fromMonth || initialFilters.toMonth) initialSelectedColumns.push('Date');
    if (initialFilters.fromYear || initialFilters.toYear) initialSelectedColumns.push('amount');
    if (initialFilters.custId) initialSelectedColumns.push('custId');

    setSelectedColumns(initialSelectedColumns);
  }, [initialFilters]);

  const formik = useFormik({
    initialValues: {
      fromMonth: initialFilters?.fromMonth || null,
      toMonth: initialFilters?.toMonth || null,
      fromYear: initialFilters?.fromYear || '',
      toYear: initialFilters?.toYear || '',
      custId: initialFilters?.custId || '',
    },
    validationSchema,
    onSubmit: (values) => {
      let errors: { [key: string]: string } = {};

      // Validation for each selected field
      if (selectedColumns.includes('Month')) {
        if (!values.fromMonth) errors.fromMonth = 'From Month is required';
        if (!values.toMonth) errors.toMonth = 'To Month is required';
      }

      if (selectedColumns.includes('year') && (!values.fromYear || !values.toYear)) {
        if (!values.fromYear) errors.fromYear = 'Amount From is required';
        if (!values.toYear) errors.toYear = 'Amount To is required';
      }


      if (selectedColumns.includes('custId') && !values.custId) {
        errors.custId = 'Customer ID is required';
      }

      // If errors exist, set them and stop the form submission
      if (Object.keys(errors).length > 0) {
        formik.setErrors(errors);
        return;
      }

      const filtersToSend: any = { ...values };

      // Format date fields
      if (values.fromMonth) {
        filtersToSend.fromMonth = new Date(values.fromMonth).getMonth();
      }
      if (values.toMonth) {
        filtersToSend.toMonth = new Date(values.toMonth).getMonth();
      }

      if (values.fromYear) {
        filtersToSend.fromYear = new Date(values.fromYear).getFullYear();
      }
      if (values.toYear) {
        filtersToSend.toYear = new Date(values.toYear).getFullYear();
      }

      // Send the selected filters
      onApplyFilters(filtersToSend);
      onClose();
    },
  });

  // Handle the selection of multiple columns
  const handleColumnSelectionChange = (columnId: string) => () => {
    setSelectedColumns((prevSelected) => {
      const isSelected = prevSelected.includes(columnId);
      const updatedSelection = isSelected
        ? prevSelected.filter((col) => col !== columnId)
        : [...prevSelected, columnId];

      // Reset form fields when a column is deselected
      if (isSelected) {
        switch (columnId) {
          case 'Month':
            formik.setFieldValue('fromMonth', '');
            formik.setFieldValue('toMonth', '');
            break;
        
          case 'Year':
            formik.setFieldValue('fromYear', '');
            formik.setFieldValue('toYear', '');
            break;
          case 'custId':
            formik.setFieldValue('custId', '');
            break;
          default:
            break;
        }
      }

      return updatedSelection;
    });
  };

  const handleReset = () => {
    formik.resetForm();
    setSelectedColumns([]); // Clear selected columns
    onReset();
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <form onSubmit={formik.handleSubmit}>
        <DialogTitle>Filter Columns</DialogTitle>
        <Divider />
        <DialogContent>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={4}>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={selectedColumns.includes('Month')}
                    onChange={handleColumnSelectionChange('Month')}
                    color="primary"
                  />
                }
                label="Month"
              />
            </Grid>
            
            <Grid item xs={12} sm={4}>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={selectedColumns.includes('year')}
                    onChange={handleColumnSelectionChange('year')}
                    color="primary"
                  />
                }
                label="Year"
              />
            </Grid>

            <Grid item xs={12} sm={4}>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={selectedColumns.includes('custId')}
                    onChange={handleColumnSelectionChange('custId')}
                    color="primary"
                  />
                }
                label="Customer ID"
              />
            </Grid>


            {/* Date Fields */}
            {selectedColumns.includes('Month') && (
              <>
                <Grid item xs={12} sm={6}>
                  <TextField
                    label="Month From"
                    type="date"
                    value={formik.values.fromMonth ? formik.values.fromMonth : ''}
                    onChange={(event) => formik.setFieldValue('fromMonth', event.target.value)}
                    error={formik.touched.fromMonth && Boolean(formik.errors.fromMonth)}
                    helperText={formik.touched.fromMonth && formik.errors.fromMonth}
                    fullWidth
                    InputLabelProps={{
                      shrink: true,
                    }}
                    inputProps={{
                      max: new Date().getMonth(),
                    }}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    label="Month To"
                    type="date"
                    value={formik.values.toMonth ? formik.values.toMonth : ''}
                    onChange={(event) => formik.setFieldValue('toMonth', event.target.value)}
                    error={formik.touched.toMonth && Boolean(formik.errors.toMonth)}
                    helperText={formik.touched.toMonth && formik.errors.toMonth}
                    fullWidth
                    InputLabelProps={{
                      shrink: true,
                    }}
                    inputProps={{
                      max: new Date().getMonth(),
                    }}
                  />
                </Grid>
              </>
            )}

            {selectedColumns.includes('year') && (
              <Grid item xs={12}>
                <Grid container spacing={2}>
                  <Grid item xs={6}>
                    <TextField
                    label="To Year"
                    type="date"
                    value={formik.values.toYear ? formik.values.toYear : ''}
                    onChange={(event) => formik.setFieldValue('toYear', event.target.value)}
                    error={formik.touched.toYear && Boolean(formik.errors.toYear)}
                    helperText={formik.touched.toYear && formik.errors.toYear}
                    fullWidth
                    InputLabelProps={{
                      shrink: true,
                    }}
                    inputProps={{
                      max: new Date().getFullYear(),
                    }}
                  />
                  </Grid>
                  <Grid item xs={6}>
                  <TextField
                    label="From Year"
                    type="date"
                    value={formik.values.fromYear ? formik.values.fromYear : ''}
                    onChange={(event) => formik.setFieldValue('fromYear', event.target.value)}
                    error={formik.touched.fromYear && Boolean(formik.errors.fromYear)}
                    helperText={formik.touched.fromYear && formik.errors.fromYear}
                    fullWidth
                    InputLabelProps={{
                      shrink: true,
                    }}
                    inputProps={{
                      max: new Date().getFullYear(),
                    }}
                  />
                  </Grid>
                </Grid>
              </Grid>
            )}

            {/* Customer ID */}
            {selectedColumns.includes('custId') && (
              <Grid item xs={12} sm={6}>
                <TextField
                  label="Customer ID"
                  {...formik.getFieldProps('custId')}
                  error={formik.touched.custId && Boolean(formik.errors.custId)}
                  helperText={formik.touched.custId && formik.errors.custId}
                  fullWidth
                />
              </Grid>
            )}


          </Grid>
        </DialogContent>


        <DialogActions>
          <Button onClick={onClose}>Cancel</Button>
          <Button onClick={handleReset} color="secondary">
            Clear Filters
          </Button>
          <Button disabled={
            !selectedColumns ||
            (selectedColumns.includes('Month') && (!formik.values.fromMonth || !formik.values.toMonth))
            ||(selectedColumns.includes('Year') && (!formik.values.fromYear || !formik.values.toYear))

          } type="submit" color="primary" variant="contained">
            Apply
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

export default FilterDialog;
