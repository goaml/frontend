import { Dispatch, SetStateAction } from 'react';
import { Column } from 'react-table';
import { HeaderGroup } from 'react-table';
import { customer_summary_monthly_trxcode_wiseType } from 'types/customer_summary_monthly_trxcode_wise';

export interface dataProps extends customer_summary_monthly_trxcode_wiseType { }

export interface ReactTableProps {
  columns: Column[]
  data: dataProps[]
  tableParams: TableParamsType
  pagination: any
  getHeaderProps: (column: HeaderGroup) => {};
}

export interface TableHeaderProps {
  headerGroups: HeaderGroup[];
}

export interface TableParamsType {
  page: number;
  setPage: Dispatch<SetStateAction<number>>;
  perPage: number;
  setPerPage: Dispatch<SetStateAction<number>>;
  direction: "asc" | "desc";
  setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
  sort: string;
  setSort: Dispatch<SetStateAction<string>>;
  search: string;
  setSearch: Dispatch<SetStateAction<string>>;
  toYear: string;
  setToYear: Dispatch<SetStateAction<string>>;
  fromYear: string;
  setFromYear: Dispatch<SetStateAction<string>>;
  fromMonth: string;
  setFromMonth: Dispatch<SetStateAction<string>>;
  toMonth: string;
  setToMonth: Dispatch<SetStateAction<string>>;
  custId: string;
  setCustId: Dispatch<SetStateAction<string>>;
  rptCode: string;
  setRptCode: Dispatch<SetStateAction<string>>;
}
