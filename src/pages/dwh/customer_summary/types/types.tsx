import { Dispatch, SetStateAction } from 'react';
import { Column, HeaderGroup } from 'react-table';
import { customerSummaryType } from 'types/customer_summary_monthly';


export interface dataProps extends customerSummaryType { }

export interface ReactTableProps {
  columns: Column[]
  data: dataProps[]
  tableParams: TableParamsType
  pagination: any
  getHeaderProps: (column: HeaderGroup) => {};
}

export interface TableHeaderProps {
  headerGroups: HeaderGroup[];

}

export interface TableParamsType {
  page: number;
  setPage: Dispatch<SetStateAction<number>>;
  perPage: number;
  setPerPage: Dispatch<SetStateAction<number>>;
  direction: "asc" | "desc";
  setDirection: Dispatch<SetStateAction<"asc" | "desc">>;
  sort: string;
  setSort: Dispatch<SetStateAction<string>>;
  search: string;
  setSearch: Dispatch<SetStateAction<string>>;
  toYear: string;
  setToYear: Dispatch<SetStateAction<string>>;
  fromYear: string;
  setFromYear: Dispatch<SetStateAction<string>>;
  fromMonth: string;
  setFromMonth: Dispatch<SetStateAction<string>>;
  toMonth: string;
  setToMonth: Dispatch<SetStateAction<string>>;
  custId: string;
  setCustId: Dispatch<SetStateAction<string>>;

}
