import React from 'react';
import ReactApexChart from 'react-apexcharts';

type ChartProps = {
	chartData: any[];
	chartOptions: any;
};

type ChartState = {
	chartData: any[];
	chartOptions: any;
};

class PieChart extends React.Component<ChartProps, ChartState> {
	constructor(props: ChartProps) {
		super(props);

		this.state = {
			chartData: [],
			chartOptions: {}
		};
	}

	componentDidMount() {
		this.setState({
			chartData: this.props.chartData,
			chartOptions: this.props.chartOptions
		});
	}

	render() {
		return (
			<div>
				<ReactApexChart
					options={this.state.chartOptions}
					series={this.state.chartData}
					type="pie"
					width="100%"
					height="100%"
				/>
				<div style={{ marginTop: '20px' }}>
					<h3>Chart Options:</h3>
					<pre style={{ background: '#f4f4f4', padding: '10px', borderRadius: '5px' }}>
						{JSON.stringify(this.state.chartOptions, null, 2)}
					</pre>
				</div>
			</div>
		);
	}
}

export default PieChart;
