// CustomCard.tsx
import { Card as MuiCard, styled } from '@mui/material';

const CustomCard = styled(MuiCard)(({ theme }) => ({
  padding: '10px',
  borderRadius: '12px',
  boxShadow: '0px 18px 20px rgba(112, 144, 176, 0.12)',
  backgroundColor: theme.palette.background.paper,
  transition: 'transform 0.3s ease-in-out',
  '&:hover': {
    transform: 'scale(1.05)',
  },
  '& .MuiCardContent-root': {
    padding: '10px',
  },
}));

export default CustomCard;
