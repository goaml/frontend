// material-ui

// project import

// assets
import MainCard from "components/MainCard";
import useAuth from "hooks/useAuth";
import { useEffect, useState } from "react";
import AlertUserPasswordEx from "sections/auth/auth-forms/AlertUserPasswordEx";
import ColumnChart from "sections/dashboard/ColumnChart";
import XmlFileSummary from "sections/dashboard/XmlFileSummary/log";
import { dispatch, useSelector } from "store";
import { fetchGlobalDetailsSuccess } from "store/reducers/global-details";
import { fetchSummaryLogForDashboard } from "store/reducers/xml-file-log";
import { listParametersType } from 'types/xml-file-log';

// ==============================|| SAMPLE PAGE ||============================== //

const Dashboard = () => {
  const { user } = useAuth()

  useEffect(() => {
    console.log("user", user);
  }, [user])

  const { globalDetails, success } = useSelector(state => state.globalDetails)

  useEffect(() => {
    dispatch(fetchGlobalDetailsSuccess())
  }, [success])

  //alert model
  const [openAlert, setOpenAlert] = useState(false);

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
  };

  useEffect(() => {
    if (user) {
      if (user?.passwordResetDateCount! <= globalDetails?.result?.resetDateCount!) {
        //Alert Box
        setOpenAlert(true)
      }
    }
  }, [user, globalDetails]);

  useEffect(() => {
    const listParameters: listParametersType = {
      page: 0,
      per_page: 1000000,
      direction: 'desc',
      search: '',
      sort: 'logId',
      executeTimeFrom: '',
      executeTimeTo: '',
      logId: undefined,
      processMonth: '',
      processYear: '',
      recordCount: undefined,
      reportType: '',
      status: ''
    };
    dispatch(fetchSummaryLogForDashboard(listParameters));
  }, [dispatch, success]);

  const { xmlFileLogListForDashBoard } = useSelector((state) => state.xmlFileLog);

  return (
    <>
      <MainCard>
        <div style={{ display: 'flex', gap: '20px' }}>
          <div style={{ flex: 1 }}>
            <XmlFileSummary />
          </div>
          <div style={{ flex: 1 }}>
            <h3><i>Report Type Data Distribution</i></h3>
            <ColumnChart apiData={xmlFileLogListForDashBoard?.result!} />
          </div>
        </div>
      </MainCard>
      <br />
      {/* alert model */}
      {user && (
        <AlertUserPasswordEx
          title={""}
          open={openAlert}
          handleClose={handleAlertClose}
          passwordResetDateCount={user?.passwordResetDateCount}
        />
      )}
    </>
  );

}
export default Dashboard;
