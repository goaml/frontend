import React from 'react';
import ReactApexChart from 'react-apexcharts';
import { Card, CardContent, Typography, Grid, Button, Menu, MenuItem } from '@mui/material';

const StackedBarChart: React.FC = () => {
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);
    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    const chartData = {
        series: [
            { name: 'Rejected Companies', data: [30, 20, 10, 40, 50, 60, 30, 20, 30, 40, 50, 60] },
            { name: 'Accepted Companies', data: [60, 50, 30, 80, 100, 120, 60, 40, 60, 80, 100, 120] },
            { name: 'Pending Companies', data: [90, 70, 40, 100, 120, 150, 90, 60, 90, 120, 150, 180] },
        ],
        options: {
            chart: {
                type: 'bar',
                stacked: true,
                height: 350,
                stackType: 'normal',
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                },
            },
            xaxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            },
            legend: {
                position: 'bottom',
            },
            fill: {
                opacity: 1,
            },
            dataLabels: {
                enabled: false,
            },
            yaxis: {
                show: true,
            },
            colors: ['#8138a1', '#a11fa3', '#c96be3'],
        },
    };

    return (
        <Card>
            <CardContent>
                <Grid container justifyContent="space-between" alignItems="center">
                    <Grid item>
                        <Typography variant="h6">Total Growth</Typography>
                        <Typography variant="h4">40 Registrations</Typography>
                    </Grid>
                    <Grid item>
                        <Button
                            id="demo-positioned-button"
                            aria-controls={open ? 'demo-positioned-menu' : undefined}
                            aria-haspopup="true"
                            aria-expanded={open ? 'true' : undefined}
                            onClick={handleClick}
                        >
                            Today
                        </Button>
                        <Menu
                            id="demo-positioned-menu"
                            anchorEl={anchorEl}
                            open={open}
                            onClose={handleClose}
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'left',
                            }}
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'left',
                            }}
                        >
                            <MenuItem onClick={handleClose}>Today</MenuItem>
                            <MenuItem onClick={handleClose}>This Week</MenuItem>
                            <MenuItem onClick={handleClose}>This Month</MenuItem>
                        </Menu>
                    </Grid>
                </Grid>
                <ReactApexChart options={chartData.options} series={chartData.series} type="bar" height={350} />
            </CardContent>
        </Card>
    );
};

export default StackedBarChart;
