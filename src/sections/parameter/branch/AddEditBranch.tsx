import { useState } from 'react';

// material-ui
import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  InputLabel,
  Stack,
  TextField,
  Tooltip
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
import { branchProps } from 'pages/parameters/branch-code/types/types';
import AlertBranchDelete from './DeleteBranch';
import { addBranchCode, updateBranchCode } from 'store/reducers/branch';
import { dispatch } from 'store';

// types

// constant
const getInitialValues = (branch: FormikValues | null) => {

  const newBranch = {
    branchId: undefined,
    branchName: '',
    branchLocation: '',
    statusDesc: undefined,
    limitAmt: 0
  }

  if (branch) {
    return _.merge({}, newBranch, branch);
  }

  return newBranch;
};

// ==============================|| BRANCH ADD / EDIT ||============================== //

export interface Props {
  branch?: branchProps
  onCancel: () => void;
}

const AddEditBranch = ({ branch, onCancel }: Props) => {

  const isCreating = !branch;

  const BranchSchema = Yup.object().shape({
    branchName: Yup.string().required('Branch Name is required'),
    branchLocation: Yup.string().required('Branch Location is required'),
  });

  const formik = useFormik({
    initialValues: getInitialValues(branch!),
    validationSchema: BranchSchema,
    enableReinitialize: true,
    onSubmit: (values, { setSubmitting, resetForm }) => {
      try {
        if (branch) {
          // put
          dispatch(updateBranchCode(values))
        } else {
          // post
          dispatch(addBranchCode(values))
        }
        resetForm()
        setSubmitting(false);
        onCancel();
      } catch (error) {
        console.error(error);
      }
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  const [openAlert, setOpenAlert] = useState(false);

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
    onCancel();
  };
  return (
    <>
      <FormikProvider value={formik}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
            <DialogTitle>{branch ? 'Edit Branch' : ' Add New Branch'}</DialogTitle>
            <Divider />
            <DialogContent sx={{ p: 2.5 }}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={12}>
                  <Grid container spacing={3}>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="branchName">Branch Name <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="branchName"
                          placeholder="Enter Branch Name"
                          {...getFieldProps('branchName')}
                          error={Boolean(touched.branchName && errors.branchName)}
                          helperText={touched.branchName && errors.branchName}
                        />
                      </Stack>
                    </Grid>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="branchLocation">Branch Location <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="branchLocation"
                          placeholder="Enter Branch Location"
                          {...getFieldProps('branchLocation')}
                          error={Boolean(touched.branchLocation && errors.branchLocation)}
                          helperText={touched.branchLocation && errors.branchLocation}
                        />
                      </Stack>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <Divider />
            <DialogActions sx={{ p: 2.5 }}>
              <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                  {!isCreating && (
                    <Tooltip title="Delete user" placement="top">
                      <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                        <DeleteFilled />
                      </IconButton>
                    </Tooltip>
                  )}
                </Grid>
                <Grid item>
                  <Stack direction="row" spacing={2} alignItems="center">
                    <Button color="error" onClick={onCancel}>
                      Cancel
                    </Button>
                    <Button type="submit" variant="contained" disabled={isSubmitting}>
                      {branch ? 'Edit' : 'Add'}
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </DialogActions>
          </Form>
        </LocalizationProvider>
      </FormikProvider>
      {!isCreating && <AlertBranchDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={branch.branchId!} />}
    </>
  );
};

export default AddEditBranch;
