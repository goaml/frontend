import { useState } from 'react';

// material-ui
import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  InputLabel,
  Stack,
  TextField,
  Tooltip
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
import { departmentProps } from 'pages/parameters/departments/types/types';
import AlertDepartmentDelete from './DeleteDepartment';
import { addDepartmentCode, updateDepartmentCode } from 'store/reducers/department';
import { dispatch } from 'store';

// types

// constant
const getInitialValues = (department: FormikValues | null) => {

  const newDepartment = {
    deptId: undefined,
    deptName: '',
    deptLocation: '',
    statusDesc: undefined,
    limitAmt: 0
  }

  if (department) {
    return _.merge({}, newDepartment, department);
  }

  return newDepartment;
};

// ==============================|| DEPARTMENT ADD / EDIT ||============================== //

export interface Props {
  department?: departmentProps
  onCancel: () => void;
}

const AddEditDepartment = ({ department, onCancel }: Props) => {

  const isCreating = !department;

  const DepartmentSchema = Yup.object().shape({
    departmentName: Yup.string().required('Department Name is required'),
    departmentLocation: Yup.string().required('Department Location is required'),
  });

  const formik = useFormik({
    initialValues: getInitialValues(department!),
    validationSchema: DepartmentSchema,
    enableReinitialize: true,
    onSubmit: (values, { setSubmitting, resetForm }) => {
      try {
        if (department) {
          // put
          dispatch(updateDepartmentCode(values))
        } else {
          // post
          dispatch(addDepartmentCode(values))
        }
        resetForm()
        setSubmitting(false);
        onCancel();
      } catch (error) {
        console.error(error);
      }
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  const [openAlert, setOpenAlert] = useState(false);

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
    onCancel();
  };
  return (
    <>
      <FormikProvider value={formik}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
            <DialogTitle>{department ? 'Edit Department' : ' Add New Department'}</DialogTitle>
            <Divider />
            <DialogContent sx={{ p: 2.5 }}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={12}>
                  <Grid container spacing={3}>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="departmentName">Department Name <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="deptName"
                          placeholder="Enter Department Name"
                          {...getFieldProps('deptName')}
                          error={Boolean(touched.deptName && errors.deptName)}
                          helperText={touched.deptName && errors.deptName}
                        />
                      </Stack>
                    </Grid>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="departmentLocation">Department Location <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="departmentLocation"
                          placeholder="Enter Department Location"
                          {...getFieldProps('departmentLocation')}
                          error={Boolean(touched.deptLocation && errors.deptLocation)}
                          helperText={touched.deptLocation && errors.deptLocation}
                        />
                      </Stack>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <Divider />
            <DialogActions sx={{ p: 2.5 }}>
              <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                  {!isCreating && (
                    <Tooltip title="Delete user" placement="top">
                      <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                        <DeleteFilled />
                      </IconButton>
                    </Tooltip>
                  )}
                </Grid>
                <Grid item>
                  <Stack direction="row" spacing={2} alignItems="center">
                    <Button color="error" onClick={onCancel}>
                      Cancel
                    </Button>
                    <Button type="submit" variant="contained" disabled={isSubmitting}>
                      {department ? 'Edit' : 'Add'}
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </DialogActions>
          </Form>
        </LocalizationProvider>
      </FormikProvider>
      {!isCreating && <AlertDepartmentDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={department.deptId!} />}
    </>
  );
};

export default AddEditDepartment;
