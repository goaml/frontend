// material-ui
import {
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    Grid,
    InputLabel,
    Stack,
    TextField
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { FormikProvider, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';
import { dataProps } from 'pages/applications/trans-err-log/type/types';
import { format } from 'date-fns';

// types

export interface FormikValues {
    processDateFrom?: string;
    processDateTo?: string;
    processDate?: string,
    failReason?: string,
    attemptCode?: string
    count?: number
}



export interface Props {
    user?: dataProps;
    onCancel: () => void;
}

const getInitialValues = (user: FormikValues | null) => {
    const newUser: FormikValues = {
        processDateFrom: "",
        processDateTo: "",
        failReason: "",
        processDate: "",
        attemptCode: "",
        count: 0
    };

    if (user) {
        return _.merge({}, newUser, user);
    }

    return newUser;
};

const AddEditUser = ({ user, onCancel }: Props) => {
    const UserSchema = Yup.object().shape({

    });

    const formik = useFormik({
        initialValues: getInitialValues(user || null),
        validationSchema: UserSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (user) {
                    // put request logic
                } else {
                    // post request logic
                }
                resetForm();
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, getFieldProps } = formik;

    const getHelperText = (fieldError: any): string | undefined => {
        return typeof fieldError === 'string' ? fieldError : undefined;
    };

    const formattedDate = formik.values.processDate
    ? format(new Date(formik.values.processDate), 'yyyy-MM-dd') 
    : '';

    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{user ? 'View Transaction Details' : 'New Transaction'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                    <Grid container spacing={3}>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.2}>
                                                <InputLabel htmlFor="attemptCode">Attempt Code</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="attemptCode"
                                                    {...getFieldProps('attemptCode')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.attemptCode && errors.attemptCode)}
                                                    helperText={getHelperText(touched.attemptCode && errors.attemptCode)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="processDate">Process Date</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="processDate"
                                                    value={formattedDate}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.processDate && errors.processDate)}
                                                    helperText={getHelperText(touched.processDate && errors.processDate)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="failReason">Fail Reason</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="failReason"
                                                    {...getFieldProps('failReason')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.failReason && errors.failReason)}
                                                    helperText={getHelperText(touched.failReason && errors.failReason)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="count">Count</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="count"
                                                    {...getFieldProps('count')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.count && errors.count)}
                                                    helperText={getHelperText(touched.count && errors.count)}
                                                />
                                            </Stack>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 1.25 }}>

                            <Button color="error" onClick={onCancel}>
                                Cancel
                            </Button>
                        </DialogActions>
                    </form>
                </LocalizationProvider>
            </FormikProvider>
        </>
    );
};

export default AddEditUser;
