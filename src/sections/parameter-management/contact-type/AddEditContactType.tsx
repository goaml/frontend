import { useState } from 'react';

// material-ui
import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  InputLabel,
  Stack,
  TextField,
  Tooltip
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
import AlertContactTypeDelete from './DeleteContactType';
import { contactProps } from 'pages/parameter-management/contact-type/types/types';
import { dispatch } from 'store';
import { addContactType, updateContactType } from 'store/reducers/contact-type';


// types

// constant
const getInitialValues = (contactType: FormikValues | null) => {

  const newSubmissionCode = {
   contactTypeId: undefined,
   contactTypeCode: '',
    description: '',
    statusId: 1,
    //limitAmt: 0
  }

  if (contactType) {
    return _.merge({}, newSubmissionCode,contactType);
  }

  return newSubmissionCode;
};

// ==============================||contactType ADD / EDIT ||============================== //

export interface Props {
 contactType?: contactProps
  onCancel: () => void;
}

const AddEditContactType = ({contactType, onCancel }: Props) => {

  const isCreating = !contactType;

  const contactTypeSchema = Yup.object().shape({
   contactTypeCode: Yup.string().required('Contact Type Code is required'),
   description: Yup.string().required('Contact Type Description is required'),
  });

  const formik = useFormik({
    initialValues: getInitialValues(contactType!),
    validationSchema:contactTypeSchema,
    enableReinitialize: true,
    onSubmit: (values, { setSubmitting, resetForm }) => {
      try {
        if (contactType) {
          // put
          dispatch(updateContactType(values))

        } else {
          // post
          dispatch(addContactType(values))
      
        }
        resetForm()
        setSubmitting(false);
        onCancel();
      } catch (error) {
        console.error(error);
      }
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  const [openAlert, setOpenAlert] = useState(false);

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
    onCancel();
  };
  return (
    <>
      <FormikProvider value={formik}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
            <DialogTitle>{contactType ? 'Edit Contact Type' : ' Add Contact Type'}</DialogTitle>
            <Divider />
            <DialogContent sx={{ p: 2.5 }}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={12}>
                  <Grid container spacing={3}>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="contactTypeCode">Contact Type Code <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="contactTypeCode"
                          placeholder="Enter Contact Type Code "
                          {...getFieldProps('contactTypeCode')}
                          error={Boolean(touched.contactTypeCode && errors.contactTypeCode)}
                          helperText={touched.contactTypeCode && errors.contactTypeCode}
                        />
                      </Stack>
                    </Grid>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="description">Contact Type Description <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="description"
                          placeholder="Enter Contact Type Code Description"
                          {...getFieldProps('description')}
                          error={Boolean(touched.description && errors.description)}
                          helperText={touched.description && errors.description}
                        />
                      </Stack>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <Divider />
            <DialogActions sx={{ p: 2.5 }}>
              <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                  {!isCreating && (
                    <Tooltip title="Delete user" placement="top">
                      <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                        <DeleteFilled />
                      </IconButton>
                    </Tooltip>
                  )}
                </Grid>
                <Grid item>
                  <Stack direction="row" spacing={2} alignItems="center">
                    <Button color="error" onClick={onCancel}>
                      Cancel
                    </Button>
                    <Button type="submit" variant="contained" disabled={isSubmitting}>
                      {contactType ? 'Edit' : 'Add'}
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </DialogActions>
          </Form>
        </LocalizationProvider>
      </FormikProvider>
      {!isCreating && <AlertContactTypeDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={contactType.contactTypeId!} />}
    </>
  );
};

export default AddEditContactType;
