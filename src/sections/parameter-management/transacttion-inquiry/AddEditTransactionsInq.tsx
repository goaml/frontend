//import { useState } from 'react';

// material-ui
import {
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    // FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField
} from '@mui/material';
// import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { FormikProvider, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
// import IconButton from 'components/@extended/IconButton';

// // assets
// import { DeleteFilled } from '@ant-design/icons';
// import AlertTransactionsDelete from './AlertTransactionsDelete';
// import { fetchCategoryList } from 'store/reducers/category';
// import { dispatch, useSelector } from 'store';
// //import { CategoryType } from 'types/category';
// import { fetchFundTypeList } from 'store/reducers/fund-type';
// import { FundTypeType } from 'types/fund-type';
// import { fetchfundTypeDescriptionList } from 'store/reducers/fund-desc';
// import { FundTypesType } from 'types/fund-desc';
// import { fetchMultiPartyFundsCodeList } from 'store/reducers/multyParty-fundCode';
// import { fetchMultiPartyInvolvePartyList } from 'store/reducers/multiParty-Involve-party';
// import { fetchMultiPartyRoleList } from 'store/reducers/multiParty-Role';
// import { fetchTransactionCodeList } from 'store/reducers/transaction-code';
// import { fetchRPTList } from 'store/reducers/rpt-code';
// import { MultiPartyFundsCodeType } from 'types/multiParty-fundCode';
// import { MultiPartyInvolvePartyType } from 'types/multiParty-Involve-party';
// import { MultiPartyRoleType } from 'types/multiParty-Role';
// import { ReportTypeType } from 'types/report-type';
// import { RPTCodeType } from 'types/rpt-code';
// 
// import { TransactionCodeType } from 'types/transaction-code';
// import { fetchReportTypeList } from 'store/reducers/report-type';
// import { transactionProps } from 'pages/parameter-management/transactions/list/types/types';
//import { optionType } from 'data/options';

import { dataProps } from 'pages/applications/transaction-Inquiry/type/types';

// types

export interface FormikValues {
    isActive?: boolean;
    tranId?: number;
    tranType?: string;
    tranSubType?: string;
    partTranType?: string;
    acid?: string;
    glSubHeadCode?: string;
    custId?: string;
    moduleId?: string;
    crncyCode?: string;
    brCode?: string;
    bankCode?: string;
    rptCode?: string;
    tranDate?: string,
    solId?: string;
}



export interface Props {
    user?: dataProps;
    onCancel: () => void;
}

const getInitialValues = (user: FormikValues | null) => {
    const newUser: FormikValues = {
        isActive: true,
        tranId: 0,
        tranType: "",
        tranSubType: "",
        partTranType: "",
        acid: "",
        glSubHeadCode: "",
        custId: "",
        moduleId: "",
        crncyCode: "",
        brCode: "",
        bankCode: "",
        rptCode: "",
        tranDate: "",
        solId: "",
    };

    if (user) {
        return _.merge({}, newUser, user);
    }

    return newUser;
};

const AddEditUser = ({ user, onCancel }: Props) => {
    // const theme = useTheme();
    // const isCreating = !user;

    const UserSchema = Yup.object().shape({
       
    });

    const formik = useFormik({
        initialValues: getInitialValues(user || null),
        validationSchema: UserSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (user) {
                    // put request logic
                } else {
                    // post request logic
                }
                resetForm();
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, getFieldProps } = formik;

    // const [openAlert, setOpenAlert] = useState(false);

    // const handleAlertClose = () => {
    //     setOpenAlert(!openAlert);
    //     onCancel();
    // };

    const getHelperText = (fieldError: any): string | undefined => {
        return typeof fieldError === 'string' ? fieldError : undefined;
    };

    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{user ? 'View Transaction Details' : 'New Transaction'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                    <Grid container spacing={3}>

                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="tranDate">Transaction Date</InputLabel>
                                                <TextField
                                                    disabled
                                                    fullWidth
                                                    id="tranDate"
                                                    {...getFieldProps('tranDate')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.tranDate && errors.tranDate)}
                                                    helperText={getHelperText(touched.tranDate && errors.tranDate)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.2}>
                                                <InputLabel htmlFor="tranType">Transaction Type</InputLabel>
                                                <TextField
                                                    disabled
                                                    fullWidth
                                                    id="tranType"
                                                    {...getFieldProps('tranType')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.tranType && errors.tranType)}
                                                    helperText={getHelperText(touched.tranType && errors.tranType)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="tranSubType">Transaction Sub Type</InputLabel>
                                                <TextField
                                                    disabled
                                                    fullWidth
                                                    id="tranSubType"
                                                    {...getFieldProps('tranSubType')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.tranSubType && errors.tranSubType)}
                                                    helperText={getHelperText(touched.tranSubType && errors.tranSubType)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="partTranType">Part Transaction Type</InputLabel>
                                                <TextField
                                                    disabled
                                                    fullWidth
                                                    id="partTranType"
                                                    {...getFieldProps('partTranType')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.partTranType && errors.partTranType)}
                                                    helperText={getHelperText(touched.partTranType && errors.partTranType)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="glSubHeadCode">GL Header</InputLabel>
                                                <TextField
                                                    disabled
                                                    fullWidth
                                                    id="glSubHeadCode"
                                                    {...getFieldProps('glSubHeadCode')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.glSubHeadCode && errors.glSubHeadCode)}
                                                    helperText={getHelperText(touched.glSubHeadCode && errors.glSubHeadCode)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="acid">Account No</InputLabel>
                                                <TextField
                                                    disabled
                                                    fullWidth
                                                    id="acid"
                                                    {...getFieldProps('acid')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.acid && errors.acid)}
                                                    helperText={getHelperText(touched.acid && errors.acid)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="rptCode">RPT Code</InputLabel>
                                                <TextField
                                                    disabled
                                                    fullWidth
                                                    id="rptCode"
                                                    {...getFieldProps('rptCode')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.rptCode && errors.rptCode)}
                                                    helperText={getHelperText(touched.rptCode && errors.rptCode)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="custId">Customer Id</InputLabel>
                                                <TextField
                                                    disabled
                                                    fullWidth
                                                    id="custId"
                                                    {...getFieldProps('custId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.custId && errors.custId)}
                                                    helperText={getHelperText(touched.custId && errors.custId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="moduleId">Module Id</InputLabel>
                                                <TextField
                                                    disabled
                                                    fullWidth
                                                    id="moduleId"
                                                    {...getFieldProps('moduleId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    value={formik.values.moduleId || 'Not Available'}
                                                    error={Boolean(touched.moduleId && errors.moduleId)}
                                                    helperText={getHelperText(touched.moduleId && errors.moduleId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="crncyCode">Currency Code</InputLabel>
                                                <TextField
                                                    disabled
                                                    fullWidth
                                                    id="crncyCode"
                                                    {...getFieldProps('crncyCode')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.crncyCode && errors.crncyCode)}
                                                    helperText={getHelperText(touched.crncyCode && errors.crncyCode)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="solId">Sol Id</InputLabel>
                                                <TextField
                                                    disabled
                                                    fullWidth
                                                    id="solId"
                                                    {...getFieldProps('solId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.solId && errors.solId)}
                                                    helperText={getHelperText(touched.solId && errors.solId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="bankCode">Bank Code</InputLabel>
                                                <TextField
                                                    disabled
                                                    fullWidth
                                                    id="bankCode"
                                                    {...getFieldProps('bankCode')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.bankCode && errors.bankCode)}
                                                    helperText={getHelperText(touched.bankCode && errors.bankCode)}
                                                />
                                            </Stack>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 1.25 }}>

                            <Button color="error" onClick={onCancel}>
                                Cancel
                            </Button>
                        </DialogActions>
                    </form>
                </LocalizationProvider>
            </FormikProvider>
        </>
    );
};

export default AddEditUser;
