import { useEffect, useState } from 'react';

// material-ui
import {
    Autocomplete,
    // Autocomplete,
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    // FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField,
    Tooltip,
    useTheme
} from '@mui/material';
// import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
// import { optionType, options } from 'data/options';
import { dataProps } from 'pages/parameter-management/rpt-code/list/types/types';
import { dispatch, useSelector } from 'store';
import { fetchTransactionCodeList } from 'store/reducers/goaml-trx-code';
import { addRPTCode, updateRPTCode } from 'store/reducers/rpt-code';
import { TransactionReqType } from 'types/goaml-trx-code';

import { fetchReportForSelect } from 'store/reducers/trans-report';
import { transReport } from 'types/trans-report';
import AlertRptCodeDelete from './AlertRptCodeDelete';
// types

// constant
const getInitialValues = (rptCode: FormikValues | null) => {

    const newRPTCode = {
        rptCodeId: undefined,
        rptCodeName: '',
        rptCodeDescription: '',
        goamlTrxCodeId: undefined,
        isActive: true,
        transactionCodeName: '',
        reportId: undefined
    }

    if (rptCode) {
        return _.merge({}, newRPTCode, rptCode);
    }

    return newRPTCode;
};

// ==============================|| CUSTOMER ADD / EDIT ||============================== //

export interface Props {
    rptCode?: dataProps
    onCancel: () => void;
}

const AddEditRptCode = ({ rptCode, onCancel }: Props) => {
    const theme = useTheme();

    const isCreating = !rptCode;

    const rptCodeSchema = Yup.object().shape({

        rptCodeName: Yup.string().required('RPT Code is required'),
        rptCodeDescription: Yup.string().required('RPT Code Description  is required'),
        reportId: Yup.number().nullable().required('Report Type is required'),
        goamlTrxCodeId: Yup.number().nullable().required('Transaction Code is required'),

    });

    const formik = useFormik({
        initialValues: getInitialValues(rptCode!),
        validationSchema: rptCodeSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (rptCode) {
                    // put
                    dispatch(updateRPTCode(values))
                } else {
                    // post

                    dispatch(addRPTCode(values))
                }
                resetForm()
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

    const [openAlert, setOpenAlert] = useState(false);

    const handleAlertClose = () => {
        setOpenAlert(!openAlert);
        onCancel();
    };
    //SELECT Multi Part Funnd code DESC FROM DROPDOWN
    const { selectTransactionCodeType, trsuccess } = useSelector((state) => state.transaction);
    useEffect(() => {
        dispatch(fetchTransactionCodeList());
    }, [trsuccess, dispatch]);

    //SELECT Report type FROM DROPDOWN
    const { selectReportType, success } = useSelector((state) => state.transReport);
    useEffect(() => {
        dispatch(fetchReportForSelect());
    }, [success, dispatch]);



    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{rptCode ? 'Edit RPT Code' : 'Add RPT Code'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                    <Grid container spacing={3}>

                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="rptCodeName">RPT Code <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="rptCodeName"
                                                    placeholder="Enter RPT Code"
                                                    {...getFieldProps('rptCodeName')}
                                                    error={Boolean(touched.rptCodeName && errors.rptCodeName)}
                                                    helperText={touched.rptCodeName && errors.rptCodeName}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="rptCodeDescription">RPT Code Description <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="rptCodeDescription"
                                                    placeholder="Enter RPT Code Description"
                                                    {...getFieldProps('rptCodeDescription')}
                                                    error={Boolean(touched.rptCodeDescription && errors.rptCodeDescription)}
                                                    helperText={touched.rptCodeDescription && errors.rptCodeDescription}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="goamlTrxCodeId">Transaction Code <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <Autocomplete
                                                    fullWidth
                                                    id="goamlTrxCodeId"
                                                    value={selectTransactionCodeType?.find((option: TransactionReqType) => option.goamlTrxCodeId === formik.values.goamlTrxCodeId) || null}
                                                    onChange={(event: any, newValue: TransactionReqType | null) => {
                                                        formik.setFieldValue('goamlTrxCodeId', newValue? newValue.goamlTrxCodeId : null);
                                                       

                                                    }}
                                                    onBlur={() => {
                                                        formik.setFieldTouched('goamlTrxCodeId', true); // Mark field as touched on blur
                                                        formik.validateField('goamlTrxCodeId'); // Validate the field when clicking elsewhere
                                                    }}
                                                    options={selectTransactionCodeType || []}
                                                    getOptionLabel={(item) => `${item.transactionCodeName}-${item.description}`}
                                                    renderInput={(params) => {
                                                        return (
                                                            <TextField
                                                                {...params}
                                                                placeholder="Select a Transaction"
                                                                sx={{ '& .MuiAutocomplete-input.Mui-disabled': { WebkitTextFillColor: theme.palette.text.primary } }}
                                                                error={Boolean(touched.goamlTrxCodeId && errors.goamlTrxCodeId)}
                                                                helperText={touched.goamlTrxCodeId && errors.goamlTrxCodeId}
                                                            />
                                                        )
                                                    }}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="reportId">Report Type <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <Autocomplete
                                                    fullWidth
                                                    id="reportId"
                                                    value={selectReportType?.find((option: transReport) => option.reportId === formik.values.reportId) || null}
                                                    onChange={(event: any, newValue: transReport| null) => {
                                                        formik.setFieldValue('reportId', newValue ? newValue.reportId : null);
                                                    }}
                                                    onBlur={() => {
                                                        formik.setFieldTouched('reportId', true); // Mark field as touched on blur
                                                        formik.validateField('reportId'); // Validate the field when clicking elsewhere
                                                    }}
                                                    options={selectReportType || []}
                                                    getOptionLabel={(item) => `${item.reportCode}`}
                                                    renderInput={(params) => {
                                                        return (
                                                            <TextField
                                                                {...params}
                                                                placeholder="Select a Report Type "
                                                                sx={{ '& .MuiAutocomplete-input.Mui-disabled': { WebkitTextFillColor: theme.palette.text.primary } }}
                                                                error={Boolean(touched.reportId && errors.reportId)}
                                                                helperText={touched.reportId && errors.reportId}
                                                            />
                                                        )
                                                    }}
                                                />
                                            </Stack>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 2.5 }}>
                            <Grid container justifyContent="space-between" alignItems="center">
                                <Grid item>
                                    {!isCreating && (
                                        <Tooltip title="Delete RPT Code" placement="top">
                                            <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                                                <DeleteFilled />
                                            </IconButton>
                                        </Tooltip>
                                    )}
                                </Grid>
                                <Grid item>
                                    <Stack direction="row" spacing={2} alignItems="center">
                                        <Button color="error" onClick={onCancel}>
                                            Cancel
                                        </Button>
                                        <Button type="submit" variant="contained" disabled={isSubmitting}>
                                            {rptCode ? 'Edit' : 'Add'}
                                        </Button>
                                    </Stack>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </Form>
                </LocalizationProvider>
            </FormikProvider>
            {!isCreating && <AlertRptCodeDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={rptCode?.rptCodeId!} />}
        </>
    );
};

export default AddEditRptCode;
