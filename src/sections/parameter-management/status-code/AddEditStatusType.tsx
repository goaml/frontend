import { useState } from 'react';

// material-ui
import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  InputLabel,
  Stack,
  TextField,
  Tooltip
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
import AlertStatusTypeDelete from './DeleteStatusType';
import { statusProps } from 'pages/parameter-management/status-code/types/types';
import { dispatch } from 'store';
import { addStatus, updateStatus } from 'store/reducers/status-type';


// types

// constant
const getInitialValues = (statusType: FormikValues | null) => {

  const newStatusCode = {
    statusCode: '',
    description: '',
    isActive: true,
    
    //limitAmt: 0
  }

  if (statusType) {
    return _.merge({}, newStatusCode,statusType);
  }

  return newStatusCode;
};

// ==============================||currencyType ADD / EDIT ||============================== //

export interface Props {
  statusType?: statusProps
  onCancel: () => void;
}

const AddEditStatusType = ({statusType, onCancel }: Props) => {

  const isCreating = !statusType;

  const statusTypeSchema = Yup.object().shape({
   statusCode: Yup.string().required('Status Type Code is required'),
   description: Yup.string().required('Status Type Description is required'),
  });

  const formik = useFormik({
    initialValues: getInitialValues(statusType!),
    validationSchema:statusTypeSchema,
    enableReinitialize: true,
    onSubmit: (values, { setSubmitting, resetForm }) => {
      try {
        if (statusType) {
          // put
          dispatch(updateStatus(values))

        } else {
          // post
          dispatch(addStatus(values))
      
        }
        resetForm()
        setSubmitting(false);
        onCancel();
      } catch (error) {
        console.error(error);
      }
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  const [openAlert, setOpenAlert] = useState(false);

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
    onCancel();
  };
  return (
    <>
      <FormikProvider value={formik}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
            <DialogTitle>{statusType ? 'Edit Status Type' : ' Add Status Type'}</DialogTitle>
            <Divider />
            <DialogContent sx={{ p: 2.5 }}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={12}>
                  <Grid container spacing={3}>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="statusCode">Status Type Code <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="statusCode"
                          placeholder="Enter Status Type Code "
                          {...getFieldProps('statusCode')}
                          error={Boolean(touched.statusCode && errors.statusCode)}
                          helperText={touched.statusCode && errors.statusCode}
                        />
                      </Stack>
                    </Grid>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="description">Status Type Description <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="description"
                          placeholder="Enter Status Type Description"
                          {...getFieldProps('description')}
                          error={Boolean(touched.description && errors.description)}
                          helperText={touched.description && errors.description}
                        />
                      </Stack>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <Divider />
            <DialogActions sx={{ p: 2.5 }}>
              <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                  {!isCreating && (
                    <Tooltip title="Delete Status Type" placement="top">
                      <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                        <DeleteFilled />
                      </IconButton>
                    </Tooltip>
                  )}
                </Grid>
                <Grid item>
                  <Stack direction="row" spacing={2} alignItems="center">
                    <Button color="error" onClick={onCancel}>
                      Cancel
                    </Button>
                    <Button type="submit" variant="contained" disabled={isSubmitting}>
                      {statusType ? 'Edit' : 'Add'}
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </DialogActions>
          </Form>
        </LocalizationProvider>
      </FormikProvider>
      {!isCreating && <AlertStatusTypeDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={statusType.statusId!} />}
    </>
  );
};

export default AddEditStatusType;
