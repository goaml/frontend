import { useEffect, useState } from 'react';

// material-ui
import {
    Autocomplete,
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    // FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField,
    Tooltip,
    useTheme,
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
import AlertTransactionsDelete from './AlertTransactionsDelete';
import { dispatch, useSelector } from 'store';
// import { fetchTransactionCodeList } from 'store/reducers/transaction-code';
// import { fetchRPTList } from 'store/reducers/rpt-code';
// import { RPTCodeType } from 'types/rpt-code';
import { addTransaction, updateTransaction } from 'store/reducers/goaml-trx-code';
// import { TransactionCodeType } from 'types/transaction-code';
import { transactionProps } from 'pages/parameter-management/goaml-trx-code/list/types/types';
import { fetchFundTypeList } from 'store/reducers/fund-type';
import { FundTypeType } from 'types/fund-type';
import { fetchMultiPartyFundsCodeList } from 'store/reducers/multyParty-fundCode';
import { MultiPartyFundsCodesType } from 'types/multiParty-fundCode';
import { MultiPartyInvolvePartysType } from 'types/multiParty-Involve-party';
import { fetchMultiPartyInvolvePartyList } from 'store/reducers/multiParty-Involve-party';
import { fetchMultiPartyRoleList } from 'store/reducers/multiParty-Role';
import { MultiPartyRolesType } from 'types/multiParty-Role';
import { fetchReportCodeList } from 'store/reducers/report-type';
import { ReportCodesType } from 'types/report-type';

// types

// constant
const getInitialValues = (user: FormikValues | null) => {


    const newUser = {
        isActive:true,
       // goamlTrxCodeId:undefined,
        categoryName: '',
        description:'',
        fromFundTypeDescription: "",
        fromFundTypeDescriptionId: 0,
        fromFundTypeId: 0,
        goamlTrxCodeScenario: '',
        goamlTrxCodeComments: '',
        goamlTrxCodeExample: '',
        goamlTrxCodeToFundType: '',
        multiPartyFundsCodeId: 0,
        multiPartyInvolvePartyId: 0,
        multiPartyRoleId: 0,
        reportCodeId: 0,
        rptCodeId: 0,
        toFundTypeDescription: '',
        toFundTypeDescriptionId: 0,
        toFundTypeId: 0,
        transactionCodeId: 0,
        transactionCodeName:'',
        scenario:''
    }

    if (user) {
        return _.merge({}, newUser, user);
    }

    return newUser;
};

// ==============================|| CUSTOMER ADD / EDIT ||============================== //

export interface Props {
    user?: transactionProps
    onCancel: () => void;
}

const AddEditUser = ({ user, onCancel }: Props) => {
    const theme = useTheme();

    const isCreating = !user;

    const UserSchema = Yup.object().shape({
        // isActive: Yup.boolean().required('Category name is required'),
        // goamlTrxCodeId: Yup.number().required('Category name is required'),
        categoryName: Yup.string().required('Category name is required'),
        fromFundTypeDescription: Yup.string().required('From Fund Type description is required'),
        fromFundTypeId: Yup.number().required('From Fund Type is required'),
        goamlTrxCodeScenario: Yup.string().required('Scenario is required'),
        //goamlTrxCodeComments: Yup.string().required('GoAML transaction code comments is required'),
        description: Yup.string().required('Transaction Code Description is required'),
        transactionCodeName: Yup.string().required('Transaction Code  is required'),
        //goamlTrxCodeExample: Yup.string().required('GoAML transaction code example is required'),
        // // goamlTrxCodeToFundType: Yup.string().required('GoAML transaction code to fund type is required'),
        // multiPartyFundsCodeId: Yup.number().required('Multi-party funds code name is required'),
        // multiPartyInvolvePartyId: Yup.number().required('Multi-party involve party name is required'),
       //  multiPartyRoleId: Yup.number().required('Multi-party role name is required'),
        // reportCodeId: Yup.number().required('Report type name is required'),
        // rptCodeId: Yup.number().required('Rpt code name is required'),
        toFundTypeDescriptionId: Yup.string().required('To Fund Type description is required'),
        toFundTypeId: Yup.number().required('To Fund Type is required'),
        // transactionCodeId: Yup.number().required('Transaction code name is required'),
    });

    const formik = useFormik({
        initialValues: getInitialValues(user!),
        validationSchema: UserSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (user) {
                    // put
                    dispatch(updateTransaction(values))
                } else {
                    // post
                    dispatch(addTransaction(values))
                }
                resetForm()
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });


    //SELECT Multi Part Funnd code DESC FROM DROPDOWN
    const { selectMultiPartyFundsCode, MPFsuccess } = useSelector((state) => state.multiPartyFundCode);
    useEffect(() => {
        dispatch(fetchMultiPartyFundsCodeList());
    }, [MPFsuccess, dispatch]);

    //SELECT Multi Part Involve code DESC FROM DROPDOWN
    const { selectMultiPartyInvolveParty, MPIsuccess } = useSelector((state) => state.multiPartyInvolveParty);
    useEffect(() => {
        dispatch(fetchMultiPartyInvolvePartyList());
    }, [MPIsuccess, dispatch]);

    //SELECT Multi Part Role code DESC FROM DROPDOWN
    const { selectMultiPartyRole, MPRsuccess } = useSelector((state) => state.multiPartyRole);
    useEffect(() => {
        dispatch(fetchMultiPartyRoleList());
    }, [MPRsuccess, dispatch]);

    //SELECT Multi Part Role code DESC FROM DROPDOWN
    const { selectreportCode, Rsuccess } = useSelector((state) => state.reportType);
    useEffect(() => {
        dispatch(fetchReportCodeList());
    }, [Rsuccess, dispatch]);
      
    
    // //SELECT Multi Part Funnd code DESC FROM DROPDOWN
    // const { selectTransactionCodeType, TRCsuccess } = useSelector((state) => state.transactionCode);
    // useEffect(() => {
    //     dispatch(fetchTransactionCodeList());
    // }, [TRCsuccess, dispatch]);

      //SELECT Report type FROM DROPDOWN
      const { selectFundType, FTsuccess } = useSelector((state) => state.fundType);
      useEffect(() => {
          dispatch(fetchFundTypeList());
      }, [FTsuccess, dispatch]);




    const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

    const [openAlert, setOpenAlert] = useState(false);

    const handleAlertClose = () => {
        setOpenAlert(!openAlert);
        onCancel();
    };
    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{user ? 'Edit Transaction' : 'Add Transaction'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                    <Grid container spacing={3}>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="transactionCodeName">Transaction Code <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="transactionCodeName"
                                                    placeholder="Enter Transaction Code"
                                                    {...getFieldProps('transactionCodeName')}
                                                    error={Boolean(touched.transactionCodeName && errors.transactionCodeName)}
                                                    helperText={touched.transactionCodeName && errors.transactionCodeName}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="description">Transaction Code Description <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="description"
                                                    placeholder="Enter Transaction Code Description"
                                                    {...getFieldProps('description')}
                                                    error={Boolean(touched.description && errors.description)}
                                                    helperText={touched.description && errors.description}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="categoryName">Category <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="categoryName"
                                                    placeholder="Enter Category"
                                                    {...getFieldProps('categoryName')}
                                                    error={Boolean(touched.categoryName && errors.categoryName)}
                                                    helperText={touched.categoryName && errors.categoryName}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="goamlTrxCodeScenario"> Scenario <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="goamlTrxCodeScenario"
                                                    placeholder="Enter Scenario"
                                                    {...getFieldProps('goamlTrxCodeScenario')}
                                                    error={Boolean(touched.goamlTrxCodeScenario && errors.goamlTrxCodeScenario)}
                                                    helperText={touched.goamlTrxCodeScenario && errors.goamlTrxCodeScenario}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="fromFundTypeId">From Fund Type <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <Autocomplete
                                                    fullWidth
                                                    id="fromFundTypeId"
                                                    value={selectFundType?.find((option) => option.fundTypeId === formik.values.fromFundTypeId) || null}
                                                    onChange={(event: any, newValue: FundTypeType| null) => {
                                                        formik.setFieldValue('fromFundTypeId', newValue ? newValue.fundTypeId : null);
                                                    }}
                                                    onBlur={() => {
                                                        formik.setFieldTouched('fromFundTypeId', true); // Mark field as touched on blur
                                                        formik.validateField('fromFundTypeId'); // Validate the field when clicking elsewhere
                                                    }}
                                                    options={selectFundType || []}
                                                    getOptionLabel={(item) => `${item.fundTypeCode}`}
                                                    renderInput={(params) => {
                                                        return (
                                                            <TextField
                                                                {...params}
                                                                placeholder="Select From Fund Type "
                                                                sx={{ '& .MuiAutocomplete-input.Mui-disabled': { WebkitTextFillColor: theme.palette.text.primary } }}
                                                                error={Boolean(touched.fromFundTypeId && errors.fromFundTypeId)}
                                                                helperText={touched.fromFundTypeId && errors.fromFundTypeId}
                                                            />
                                                        )
                                                    }}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="fromFundTypeDescription">From Fund Type Description <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="fromFundTypeDescription"
                                                    placeholder="Enter From Fund Type Description"
                                                    {...getFieldProps('fromFundTypeDescription')}
                                                    error={Boolean(touched.fromFundTypeDescription && errors.fromFundTypeDescription)}
                                                    helperText={touched.fromFundTypeDescription && errors.fromFundTypeDescription}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="toFundTypeId">To Fund Type <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <Autocomplete
                                                    fullWidth
                                                    id="toFundTypeId"
                                                    value={selectFundType?.find((option) => option.fundTypeId === formik.values.toFundTypeId) || null}
                                                    onChange={(event: any, newValue: FundTypeType| null) => {
                                                        formik.setFieldValue('toFundTypeId', newValue ? newValue.fundTypeId : null);
                                                    }}
                                                    onBlur={() => {
                                                        formik.setFieldTouched('toFundTypeId', true); // Mark field as touched on blur
                                                        formik.validateField('toFundTypeId'); // Validate the field when clicking elsewhere
                                                    }}
                                                    options={selectFundType || []}
                                                    getOptionLabel={(item) => `${item.fundTypeCode}`}
                                                    renderInput={(params) => {
                                                        return (
                                                            <TextField
                                                                {...params}
                                                                placeholder="Select To Fund Type Code"
                                                                sx={{ '& .MuiAutocomplete-input.Mui-disabled': { WebkitTextFillColor: theme.palette.text.primary } }}
                                                                error={Boolean(touched.toFundTypeId && errors.toFundTypeId)}
                                                                helperText={touched.toFundTypeId && errors.toFundTypeId}
                                                            />
                                                        )
                                                    }}
                                                />
                                            </Stack>
                                        </Grid>

                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="toFundTypeDescription">To Fund Type Description <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="toFundTypeDescription"
                                                    placeholder="Enter To Fund Type Description"
                                                    {...getFieldProps('toFundTypeDescription')}
                                                    error={Boolean(touched.toFundTypeDescription && errors.toFundTypeDescription)}
                                                    helperText={touched.toFundTypeDescription && errors.toFundTypeDescription}
                                                />
                                            </Stack>
                                        </Grid>

                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="multiPartyFundsCodeId">Multi Party Funds Code </InputLabel>
                                                <Autocomplete
                                                    fullWidth
                                                    id="multiPartyFundsCodeId"
                                                    value={selectMultiPartyFundsCode?.find((option) => option.multiPartyFundsCodeId === formik.values.multiPartyFundsCodeId) || null}
                                                    onChange={(event: any, newValue: MultiPartyFundsCodesType| null) => {
                                                        formik.setFieldValue('multiPartyFundsCodeId', newValue ? newValue.multiPartyFundsCodeId : null);
                                                    }}
                                                    options={selectMultiPartyFundsCode || []}
                                                    getOptionLabel={(item) => `${item.multiPartyFundsCodeName}`}
                                                    renderInput={(params) => {
                                                        return (
                                                            <TextField
                                                                {...params}
                                                                placeholder="Select Multi Party Fund Code"
                                                                sx={{ '& .MuiAutocomplete-input.Mui-disabled': { WebkitTextFillColor: theme.palette.text.primary } }}
                                                                error={Boolean(touched.multiPartyFundsCodeId && errors.multiPartyFundsCodeId)}
                                                                helperText={touched.multiPartyFundsCodeId && errors.multiPartyFundsCodeId}
                                                            />
                                                        )
                                                    }}
                                                />
                                            </Stack>
                                        </Grid>

                                        
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="multiPartyInvolvePartyId">Multi Party Involve Code </InputLabel>
                                                <Autocomplete
                                                    fullWidth
                                                    id="multiPartyInvolvePartyId"
                                                    value={selectMultiPartyInvolveParty?.find((option) => option.multiPartyInvolvePartyId === formik.values.multiPartyInvolvePartyId) || null}
                                                    onChange={(event: any, newValue: MultiPartyInvolvePartysType| null) => {
                                                        formik.setFieldValue('multiPartyInvolvePartyId', newValue ? newValue.multiPartyInvolvePartyId : null);
                                                    }}
                                                    
                                                    options={selectMultiPartyInvolveParty || []}
                                                    getOptionLabel={(item) => `${item.multiPartyInvolvePartyName}`}
                                                    renderInput={(params) => {
                                                        return (
                                                            <TextField
                                                                {...params}
                                                                placeholder="Select Multi Party Involve Code"
                                                                sx={{ '& .MuiAutocomplete-input.Mui-disabled': { WebkitTextFillColor: theme.palette.text.primary } }}
                                                                error={Boolean(touched.multiPartyInvolvePartyId && errors.multiPartyInvolvePartyId)}
                                                                helperText={touched.multiPartyInvolvePartyId && errors.multiPartyInvolvePartyId}
                                                            />
                                                        )
                                                    }}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="multiPartyRoleId">Multi Party Role Code </InputLabel>
                                                <Autocomplete
                                                    fullWidth
                                                    id="multiPartyRoleId"
                                                    value={selectMultiPartyRole?.find((option) => option.multiPartyRoleId === formik.values.multiPartyRoleId) || null}
                                                    onChange={(event: any, newValue: MultiPartyRolesType| null) => {
                                                        formik.setFieldValue('multiPartyRoleId', newValue ? newValue.multiPartyRoleId : null);
                                                    }}
                                                    
                                                    options={selectMultiPartyRole || []}
                                                    getOptionLabel={(item) => `${item.multiPartyRoleName}`}
                                                    renderInput={(params) => {
                                                        return (
                                                            <TextField
                                                                {...params}
                                                                placeholder="Select Multi Party Role Code"
                                                                sx={{ '& .MuiAutocomplete-input.Mui-disabled': { WebkitTextFillColor: theme.palette.text.primary } }}
                                                                error={Boolean(touched.multiPartyRoleId && errors.multiPartyRoleId)}
                                                                helperText={touched.multiPartyRoleId && errors.multiPartyRoleId}
                                                            />
                                                        )
                                                    }}
                                                />
                                            </Stack>
                                        </Grid>

                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="reportCodeId">Report Code </InputLabel>
                                                <Autocomplete
                                                    fullWidth
                                                    id="reportCodeId"
                                                    value={selectreportCode?.find((option) => option.reportCodeId === formik.values.reportCodeId) || null}
                                                    onChange={(event: any, newValue: ReportCodesType| null) => {
                                                        formik.setFieldValue('reportCodeId', newValue ? newValue.reportCodeId : null);
                                                    }}
                                                   
                                                    options={selectreportCode || []}
                                                    getOptionLabel={(item) => `${item.reportCode}`}
                                                    renderInput={(params) => {
                                                        return (
                                                            <TextField
                                                                {...params}
                                                                placeholder="Select Report Code"
                                                                sx={{ '& .MuiAutocomplete-input.Mui-disabled': { WebkitTextFillColor: theme.palette.text.primary } }}
                                                                error={Boolean(touched.reportCodeId && errors.reportCodeId)}
                                                                helperText={touched.reportCodeId && errors.reportCodeId}
                                                            />
                                                        )
                                                    }}
                                                />
                                            </Stack>
                                        </Grid>
                                        
                                        <Grid item xs={12}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="goamlTrxCodeComments">Transaction Comments </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="goamlTrxCodeComments"
                                                    placeholder="Enter Transaction Comments"
                                                    {...getFieldProps('goamlTrxCodeComments')}
                                                    error={Boolean(touched.goamlTrxCodeComments && errors.goamlTrxCodeComments)}
                                                    helperText={touched.goamlTrxCodeComments && errors.goamlTrxCodeComments}
                                                />
                                            </Stack>
                                        </Grid>


                                        
                                    </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 2.5 }}>
                            <Grid container justifyContent="space-between" alignItems="center">
                                <Grid item>
                                    {!isCreating && (
                                        <Tooltip title="Delete user" placement="top">
                                            <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                                                <DeleteFilled />
                                            </IconButton>
                                        </Tooltip>
                                    )}
                                </Grid>
                                <Grid item>
                                    <Stack direction="row" spacing={2} alignItems="center">
                                        <Button color="error" onClick={onCancel}>
                                            Cancel
                                        </Button>
                                        <Button type="submit" variant="contained" disabled={isSubmitting}>
                                            {user ? 'Edit' : 'Add'}
                                        </Button>
                                    </Stack>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </Form>
                </LocalizationProvider>
            </FormikProvider>
            {!isCreating && <AlertTransactionsDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={user?.goamlTrxCodeId!} />}
        </>
    );
};

export default AddEditUser;
