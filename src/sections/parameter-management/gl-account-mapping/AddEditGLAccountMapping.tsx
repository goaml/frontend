import { useEffect, useState } from 'react';

// material-ui
import {
    Autocomplete,
    // Autocomplete,
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    // FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField,
    Tooltip,
    useTheme
} from '@mui/material';
// import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
// import { optionType, options } from 'data/options';
import { dispatch, useSelector } from 'store';
import { addGLAccount, updateGLAccount } from 'store/reducers/gl-account-mapping';

import { fetchRPTList } from 'store/reducers/rpt-code';
import { RPTCodeType } from 'types/rpt-code';
import AlertGLAccountDelete from './AlertGLAccountMappingDelete';
import { statusProps } from 'pages/parameter-management/status-code/types/types';
// types

// constant
const getInitialValues = (GLAccount: FormikValues | null) => {

    const newGLAccount = {
        GLAccountId: undefined,
        glAccount: '',
        mappingAccount: '',
        rptCode: undefined,
        institutionName: '',
        swift: '',
    }

    if (GLAccount) {
        return _.merge({}, newGLAccount, GLAccount);
    }

    return newGLAccount;
};

// ==============================|| CUSTOMER ADD / EDIT ||============================== //

export interface Props {
    rptCode?: statusProps
    onCancel: () => void;
}

const AddEditGLAccount = ({ rptCode, onCancel }: Props) => {
    const theme = useTheme();
    console.log(rptCode, 'This is the edit');

    const isCreating = !rptCode;

    const rptCodeSchema = Yup.object().shape({
        mappingAccount: Yup.string().required('Mapping Account is required'),
        institutionName: Yup.string().required('Institution Name  is required'),
        rptCode: Yup.string().required('RPT Code is required'),
        swift: Yup.string().required('Swift Code is required'),
    });

    const formik = useFormik({
        initialValues: getInitialValues(rptCode!),
        validationSchema: rptCodeSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (rptCode) {
                    // put
                    dispatch(updateGLAccount(values))
                } else {
                    // post

                    dispatch(addGLAccount(values))
                }
                resetForm()
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

    const [openAlert, setOpenAlert] = useState(false);

    const handleAlertClose = () => {
        setOpenAlert(!openAlert);
        onCancel();
    };

    //SELECT Report type FROM DROPDOWN
    const { selectRPTCodeType, RPTsuccess } = useSelector((state) => state.rptCode);
    useEffect(() => {
        dispatch(fetchRPTList());
    }, [RPTsuccess, dispatch]);



    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{rptCode ? 'Edit GL Account' : 'Add GL Account'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                    <Grid container spacing={3}>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="rptCode">RPT Code <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <Autocomplete
                                                    fullWidth
                                                    id="rptCode"
                                                    value={selectRPTCodeType?.find((option: RPTCodeType) => option.rptCodeName === formik.values.rptCode) || null}
                                                    onChange={(event: any, newValue: RPTCodeType | null) => {
                                                        formik.setFieldValue('rptCode', newValue ? newValue.rptCodeName : null);
                                                    }}
                                                    options={selectRPTCodeType || []}
                                                    getOptionLabel={(item) => `${item.rptCodeId}-${item.rptCodeName}`}
                                                    renderInput={(params) => {
                                                        return (
                                                            <TextField
                                                                {...params}
                                                                placeholder="Select a RPT Code "
                                                                sx={{ '& .MuiAutocomplete-input.Mui-disabled': { WebkitTextFillColor: theme.palette.text.primary } }}
                                                                error={Boolean(touched.rptCode && errors.rptCode)}
                                                                helperText={touched.rptCode && errors.rptCode}
                                                            />
                                                        )
                                                    }}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="mappingAccount">Mapping Account <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="mappingAccount"
                                                    placeholder="Enter Mapping Account"
                                                    {...getFieldProps('mappingAccount')}
                                                    error={Boolean(touched.mappingAccount && errors.mappingAccount)}
                                                    helperText={touched.mappingAccount && errors.mappingAccount}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="Institution Name">Institution Name <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="Institution Name"
                                                    placeholder="Enter Institution Name"
                                                    {...getFieldProps('institutionName')}
                                                    error={Boolean(touched.institutionName && errors.institutionName)}
                                                    helperText={touched.institutionName && errors.institutionName}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="swift">Swift Code <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="Swift Code"
                                                    placeholder="Enter Swift Code"
                                                    {...getFieldProps('swift')}
                                                    error={Boolean(touched.swift && errors.swift)}
                                                    helperText={touched.swift && errors.swift}
                                                />
                                            </Stack>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 2.5 }}>
                            <Grid container justifyContent="space-between" alignItems="center">
                                <Grid item>
                                    {!isCreating && (
                                        <Tooltip title="Delete GL Account" placement="top">
                                            <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                                                <DeleteFilled />
                                            </IconButton>
                                        </Tooltip>
                                    )}
                                </Grid>
                                <Grid item>
                                    <Stack direction="row" spacing={2} alignItems="center">
                                        <Button color="error" onClick={onCancel}>
                                            Cancel
                                        </Button>
                                        <Button type="submit" variant="contained" disabled={isSubmitting}>
                                            {rptCode ? 'Edit' : 'Add'}
                                        </Button>
                                    </Stack>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </Form>
                </LocalizationProvider>
            </FormikProvider>
            {!isCreating && <AlertGLAccountDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={rptCode?.statusId!} />}
        </>
    );
};

export default AddEditGLAccount;
