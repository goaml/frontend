import { useState } from 'react';

// material-ui
import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  InputLabel,
  Stack,
  TextField,
  Tooltip
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
import { departmentProps } from 'pages/parameter-management/departments/list/types/types';
import { dispatch } from 'store';
import { addDepartmentCode, updateDepartmentCode } from 'store/reducers/department-para';
import AlertDepartmentDelete from './DeleteDepartment';

// types

// constant
const getInitialValues = (department: FormikValues | null) => {

  const newDepartment = {
    departmentId: undefined,
    deptName: '',
    deptLocation: '',
    branchId: 0,
    desc: ''
    //statusDesc: undefined,
    // limitAmt: 0
  }

  if (department) {
    return _.merge({}, newDepartment, department);
  }

  return newDepartment;
};

// ==============================|| department ADD / EDIT ||============================== //

export interface Props {
  department?: departmentProps
  onCancel: () => void;
}



const AddEditDepartment = ({ department, onCancel }: Props) => {

  // useEffect(() => {
  //   console.log(department,'department');
    
     
  //  }, [department])
   
  // const theme = useTheme();
  const isCreating = !department;

  const DepartmentSchema = Yup.object().shape({
    deptName: Yup.string().required('Department Code is required'),
    deptLocation: Yup.string().required('Department Location is required'),
    desc: Yup.string().required('Description is required'),
  });

  const formik = useFormik({
    initialValues: getInitialValues(department!),
    validationSchema: DepartmentSchema,
    enableReinitialize: true,
    onSubmit: (values, { setSubmitting, resetForm }) => {
      try {
        if (department) {
          // put
          dispatch(updateDepartmentCode(values))
        } else {
          // post
          dispatch(addDepartmentCode(values))
        }
        resetForm()
        setSubmitting(false);
        onCancel();
      } catch (error) {
        console.error(error);
      }
    }
  });

  // //SELECT BRANCH FOR DROPDOWN
  // const { selectBranches, success } = useSelector((state) => state.branch);
  // useEffect(() => {
  //   dispatch(fetchBranchList());
  // }, [success, dispatch]);

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  const [openAlert, setOpenAlert] = useState(false);

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
    onCancel();
  };
  return (
    <>
      <FormikProvider value={formik}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
            <DialogTitle>{department ? 'Edit Department Code' : ' Add Department Code'}</DialogTitle>
            <Divider />
            <DialogContent sx={{ p: 2.5 }}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={12}>
                  



                  <Grid container spacing={3}>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="deptName">Department Code <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="deptName"
                          placeholder="Enter Department Code"
                          {...getFieldProps('deptName')}
                          error={Boolean(touched.deptName && errors.deptName)}
                          helperText={touched.deptName && errors.deptName}
                        />
                      </Stack>
                    </Grid>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="deptLocation">Department Location <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="deptLocation"
                          placeholder="Enter Department Location"
                          {...getFieldProps('deptLocation')}
                          error={Boolean(touched.deptLocation && errors.deptLocation)}
                          helperText={touched.deptLocation && errors.deptLocation}
                        />
                      </Stack>
                    </Grid>
                  </Grid>
                  <Grid container spacing={3} >
                    {/* <Grid item xs={6}>
                      <Stack spacing={1.25} sx={{ marginBottom: 3 }}>
                        <InputLabel htmlFor="branchId">Branch <span style={{ color: 'red' }}>*</span></InputLabel>
                        <Autocomplete
                          fullWidth
                          id="branchId"
                          value={selectBranches?.find((option1: Branches) => option1.branchId === formik.values.branchId) || null}
                          onChange={(event: any, newValue: Branches | null) => {
                            formik.setFieldValue('branchId', newValue?.branchId);
                          }}
                          options={selectBranches || []}
                          getOptionLabel={(item) => `${item.branchName}`}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              placeholder="Select User Type"
                              sx={{ '& .MuiAutocomplete-input.Mui-disabled': { WebkitTextFillColor: theme.palette.text.primary } }}
                            />
                          )}
                        />
                        {formik.touched.branchId && formik.errors.branchId && (
                          <FormHelperText error id="helper-text-branchId">
                            {formik.errors.branchId}
                          </FormHelperText>
                        )}
                      </Stack>
                    </Grid> */}
                    <Grid item xs={12}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="desc">Description <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="desc"
                          placeholder="Enter Description"
                          {...getFieldProps('desc')}
                          error={Boolean(touched.desc && errors.desc)}
                          helperText={touched.desc && errors.desc}
                        />
                      </Stack>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <Divider />
            <DialogActions sx={{ p: 2.5 }}>
              <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                  {!isCreating && (
                    <Tooltip title="Delete Department" placement="top">
                      <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                        <DeleteFilled />
                      </IconButton>
                    </Tooltip>
                  )}
                </Grid>
                <Grid item>
                  <Stack direction="row" spacing={2} alignItems="center">
                    <Button color="error" onClick={onCancel}>
                      Cancel
                    </Button>
                    <Button type="submit" variant="contained" disabled={isSubmitting}>
                      {department ? 'Edit' : 'Add'}
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </DialogActions>
          </Form>
        </LocalizationProvider>
      </FormikProvider>
      {!isCreating && <AlertDepartmentDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={department.departmentId!} />}
    </>
  );
};

export default AddEditDepartment;
