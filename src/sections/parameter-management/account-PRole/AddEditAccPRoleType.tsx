import { useState } from 'react';

// material-ui
import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  InputLabel,
  Stack,
  TextField,
  Tooltip
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
import AlertAccPRoleDelete from './DeleteAccPRoleType';
import { accountPRoleProps } from 'pages/parameter-management/account-PRole/types/types';
import { dispatch } from 'store';
import { addAccountPersonRoleTypeType, updateAccountPersonRoleTypeType } from 'store/reducers/account-person-role';


// types

// constant
const getInitialValues = (accountPRoleType: FormikValues | null) => {

  const newSubmissionCode = {
    accountPersonRoleTypeId: undefined,
    accountPersonRoleTypeCode: '',
    description: '',
    statusId: 1,
    //limitAmt: 0
  }

  if (accountPRoleType) {
    return _.merge({}, newSubmissionCode, accountPRoleType);
  }

  return newSubmissionCode;
};

// ==============================|| accountPRoleType ADD / EDIT ||============================== //

export interface Props {
  accountPRoleType?: accountPRoleProps
  onCancel: () => void;
}

const AddEditaccountPRoleTypeCode = ({ accountPRoleType, onCancel }: Props) => {

  const isCreating = !accountPRoleType;

  const accountPRoleTypeCodeSchema = Yup.object().shape({
    accountPersonRoleTypeCode: Yup.string().required('Account Person Role Type Code is required'),
    description: Yup.string().required('Account Person Role Type Description is required'),
  });

  const formik = useFormik({
    initialValues: getInitialValues(accountPRoleType!),
    validationSchema: accountPRoleTypeCodeSchema,
    enableReinitialize: true,
    onSubmit: (values, { setSubmitting, resetForm }) => {
      try {
        if (accountPRoleType) {
          // put
          dispatch(updateAccountPersonRoleTypeType(values))

        } else {
          // post
          dispatch(addAccountPersonRoleTypeType(values))
      
        }
        resetForm()
        setSubmitting(false);
        onCancel();
      } catch (error) {
        console.error(error);
      }
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  const [openAlert, setOpenAlert] = useState(false);

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
    onCancel();
  };
  return (
    <>
      <FormikProvider value={formik}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
            <DialogTitle>{accountPRoleType ? 'Edit Account Person Role Type ' : ' Add Account Person Role Type '}</DialogTitle>
            <Divider />
            <DialogContent sx={{ p: 2.5 }}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={12}>
                  <Grid container spacing={3}>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="accountPersonRoleTypeCode">Account Person Role Type Code <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="accountPersonRoleTypeCode"
                          placeholder="Enter Account Person Role Type Code "
                          {...getFieldProps('accountPersonRoleTypeCode')}
                          error={Boolean(touched.accountPersonRoleTypeCode && errors.accountPersonRoleTypeCode)}
                          helperText={touched.accountPersonRoleTypeCode && errors.accountPersonRoleTypeCode}
                        />
                      </Stack>
                    </Grid>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="description">Account Person Role Type Description <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="description"
                          placeholder="Enter Account Person Role Type Description"
                          {...getFieldProps('description')}
                          error={Boolean(touched.description && errors.description)}
                          helperText={touched.description && errors.description}
                        />
                      </Stack>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <Divider />
            <DialogActions sx={{ p: 2.5 }}>
              <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                  {!isCreating && (
                    <Tooltip title="Delete Account Person Role Type" placement="top">
                      <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                        <DeleteFilled />
                      </IconButton>
                    </Tooltip>
                  )}
                </Grid>
                <Grid item>
                  <Stack direction="row" spacing={2} alignItems="center">
                    <Button color="error" onClick={onCancel}>
                      Cancel
                    </Button>
                    <Button type="submit" variant="contained" disabled={isSubmitting}>
                      {accountPRoleType ? 'Edit' : 'Add'}
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </DialogActions>
          </Form>
        </LocalizationProvider>
      </FormikProvider>
      {!isCreating && <AlertAccPRoleDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={accountPRoleType.accountPersonRoleTypeId!} />}
    </>
  );
};

export default AddEditaccountPRoleTypeCode;
