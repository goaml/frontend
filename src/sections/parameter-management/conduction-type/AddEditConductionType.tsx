import { useState } from 'react';

// material-ui
import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  InputLabel,
  Stack,
  TextField,
  Tooltip
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
import AlertConductionTypeDelete from './DeleteConductionType';
import { conductionProps } from 'pages/parameter-management/conduction-type/types/types';
import { addConductionType, updateConductionType } from 'store/reducers/conduction-type';
import { dispatch } from 'store';


// types

// constant
const getInitialValues = (conductionType: FormikValues | null) => {

  const newSubmissionCode = {
   conductionTypeId: undefined,
   conductionTypeCode: '',
   description: '',
   statusId: 1
    //limitAmt: 0
  }

  if (conductionType) {
    return _.merge({}, newSubmissionCode,conductionType);
  }

  return newSubmissionCode;
};

// ==============================||conductionType ADD / EDIT ||============================== //

export interface Props {
 conductionType?: conductionProps
  onCancel: () => void;
}

const AddEditConductionType = ({conductionType, onCancel }: Props) => {

  const isCreating = !conductionType;

  const conductionTypeSchema = Yup.object().shape({
   conductionTypeCode: Yup.string().required('Conduction Type Code  is required'),
    description: Yup.string().required('Conduction Type Description is required'),
  });

  const formik = useFormik({
    initialValues: getInitialValues(conductionType!),
    validationSchema:conductionTypeSchema,
    enableReinitialize: true,
    onSubmit: (values, { setSubmitting, resetForm }) => {
      try {
        if (conductionType) {
          // put
          dispatch(updateConductionType(values))

        } else {
          // post
          dispatch(addConductionType(values))
      
        }
        resetForm()
        setSubmitting(false);
        onCancel();
      } catch (error) {
        console.error(error);
      }
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  const [openAlert, setOpenAlert] = useState(false);

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
    onCancel();
  };
  return (
    <>
      <FormikProvider value={formik}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
            <DialogTitle>{conductionType ? 'Edit Conduction Type' : ' Add Conduction Type'}</DialogTitle>
            <Divider />
            <DialogContent sx={{ p: 2.5 }}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={12}>
                  <Grid container spacing={3}>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="conductionTypeCode">Conduction Type Code <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="conductionTypeCode"
                          placeholder="Enter Conduction Type Code "
                          {...getFieldProps('conductionTypeCode')}
                          error={Boolean(touched.conductionTypeCode && errors.conductionTypeCode)}
                          helperText={touched.conductionTypeCode && errors.conductionTypeCode}
                        />
                      </Stack>
                    </Grid>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="description">Conduction Type Description <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="description"
                          placeholder="Enter Conduction Type Description"
                          {...getFieldProps('description')}
                          error={Boolean(touched.description && errors.description)}
                          helperText={touched.description && errors.description}
                        />
                      </Stack>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <Divider />
            <DialogActions sx={{ p: 2.5 }}>
              <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                  {!isCreating && (
                    <Tooltip title="Delete Conduction Type" placement="top">
                      <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                        <DeleteFilled />
                      </IconButton>
                    </Tooltip>
                  )}
                </Grid>
                <Grid item>
                  <Stack direction="row" spacing={2} alignItems="center">
                    <Button color="error" onClick={onCancel}>
                      Cancel
                    </Button>
                    <Button type="submit" variant="contained" disabled={isSubmitting}>
                      {conductionType ? 'Edit' : 'Add'}
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </DialogActions>
          </Form>
        </LocalizationProvider>
      </FormikProvider>
      {!isCreating && <AlertConductionTypeDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={conductionType.conductionTypeId!} />}
    </>
  );
};

export default AddEditConductionType;
