import { useState } from 'react';

// material-ui
import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  InputLabel,
  Stack,
  TextField,
  Tooltip
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
import AlertSubmissionDelete from './DeleteSubCode';
import { submissionProps } from 'pages/parameter-management/submission-type/types/types';
import { dispatch } from 'store';
import { addSubmissionType, updateSubmissionType } from 'store/reducers/submission-type';


// types

// constant
const getInitialValues = (submissionTypeCode: FormikValues | null) => {

  const newSubmission = {
    submissionTypeId: undefined,
    submissionTypeCode: '',
    description: '',
    statusId: 1,
    //limitAmt: 0
  }

  if (submissionTypeCode) {
    return _.merge({}, newSubmission, submissionTypeCode);
  }

  return newSubmission;
};

// ==============================|| Submission ADD / EDIT ||============================== //

export interface Props {
  submissionTypeCode?: submissionProps
  onCancel: () => void;
}

const AddEditSubmission = ({ submissionTypeCode, onCancel }: Props) => {

  const isCreating = !submissionTypeCode;

  const submissionTypeCodeSchema = Yup.object().shape({
    submissionTypeCode: Yup.string().required('Submission Code is required'),
    description: Yup.string().required('Submission Code Description is required'),
  });

  const formik = useFormik({
    initialValues: getInitialValues(submissionTypeCode!),
    validationSchema: submissionTypeCodeSchema,
    enableReinitialize: true,
    onSubmit: (values, { setSubmitting, resetForm }) => {
      try {
        if (submissionTypeCode) {
          // put
          dispatch(updateSubmissionType(values))

        } else {
          // post
          
          dispatch(addSubmissionType(values))
      
        }
        resetForm()
        setSubmitting(false);
        onCancel();
      } catch (error) {
        console.error(error);
      }
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  const [openAlert, setOpenAlert] = useState(false);

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
    onCancel();
  };
  return (
    <>
      <FormikProvider value={formik}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
            <DialogTitle>{submissionTypeCode ? 'Edit Submission Type' : ' Add Submission Type'}</DialogTitle>
            <Divider />
            <DialogContent sx={{ p: 2.5 }}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={12}>
                  <Grid container spacing={3}>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="submissionTypeCode">Submission Code <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="submissionTypeCode"
                          placeholder="Enter Submission Code"
                          {...getFieldProps('submissionTypeCode')}
                          error={Boolean(touched.submissionTypeCode && errors.submissionTypeCode)}
                          helperText={touched.submissionTypeCode && errors.submissionTypeCode}
                        />
                      </Stack>
                    </Grid>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="description">Submission Description <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="description"
                          placeholder="Enter Submission Code Description"
                          {...getFieldProps('description')}
                          error={Boolean(touched.description && errors.description)}
                          helperText={touched.description && errors.description}
                        />
                      </Stack>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <Divider />
            <DialogActions sx={{ p: 2.5 }}>
              <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                  {!isCreating && (
                    <Tooltip title="Delete user" placement="top">
                      <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                        <DeleteFilled />
                      </IconButton>
                    </Tooltip>
                  )}
                </Grid>
                <Grid item>
                  <Stack direction="row" spacing={2} alignItems="center">
                    <Button color="error" onClick={onCancel}>
                      Cancel
                    </Button>
                    <Button type="submit" variant="contained" disabled={isSubmitting}>
                      {submissionTypeCode ? 'Edit' : 'Add'}
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </DialogActions>
          </Form>
        </LocalizationProvider>
      </FormikProvider>
      {!isCreating && <AlertSubmissionDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={submissionTypeCode.submissionTypeId!} />}
    </>
  );
};

export default AddEditSubmission;
