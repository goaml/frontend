import { useState } from 'react';

// material-ui
import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  InputLabel,
  Stack,
  TextField,
  Tooltip
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
import { identifierProps } from 'pages/parameter-management/identifier-type/types/types';
import AlertIdentifierTypeDelete from './DeleteIdentifierType';
import { dispatch } from 'store';
import { addIdentifierType, updateIdentifierType } from 'store/reducers/identifier-type';


// types

// constant
const getInitialValues = (identifierType: FormikValues | null) => {

  const newIdentifierType = {
    identifierTypeId: undefined,
    identifierTypeCode: '',
    description: '',
    statusId: 1,
    //limitAmt: 0
  }

  if (identifierType) {
    return _.merge({}, newIdentifierType, identifierType);
  }

  return newIdentifierType;
};

// ==============================|| Identifier Type ADD / EDIT ||============================== //

export interface Props {
  identifierType?: identifierProps
  onCancel: () => void;
}

const AddEditIdentifierType = ({ identifierType, onCancel }: Props) => {

  const isCreating = !identifierType;

  const IdentifierTypeSchema = Yup.object().shape({
    identifierTypeCode: Yup.string().required('Identifier Type Code  is required'),
    description: Yup.string().required('Identifier Type Description is required'),
  });

  const formik = useFormik({
    initialValues: getInitialValues(identifierType!),
    validationSchema: IdentifierTypeSchema,
    enableReinitialize: true,
    onSubmit: (values, { setSubmitting, resetForm }) => {
      try {
        if (identifierType) {
          // put
          dispatch(updateIdentifierType(values))

        } else {
          // post
          dispatch(addIdentifierType(values))
      
        }
        resetForm()
        setSubmitting(false);
        onCancel();
      } catch (error) {
        console.error(error);
      }
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  const [openAlert, setOpenAlert] = useState(false);

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
    onCancel();
  };
  return (
    <>
      <FormikProvider value={formik}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
            <DialogTitle>{identifierType ? 'Edit Identifier Type' : ' Add Identifier Type'}</DialogTitle>
            <Divider />
            <DialogContent sx={{ p: 2.5 }}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={12}>
                  <Grid container spacing={3}>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="identifierTypeCode">Identifier Type Code <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="identifierTypeCode"
                          placeholder="Enter Identifier Type Code "
                          {...getFieldProps('identifierTypeCode')}
                          error={Boolean(touched.identifierTypeCode && errors.identifierTypeCode)}
                          helperText={touched.identifierTypeCode && errors.identifierTypeCode}
                        />
                      </Stack>
                    </Grid>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="description">Identifier Type Description <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="description"
                          placeholder="Enter Identifier Type  Description"
                          {...getFieldProps('description')}
                          error={Boolean(touched.description && errors.description)}
                          helperText={touched.description && errors.description}
                        />
                      </Stack>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <Divider />
            <DialogActions sx={{ p: 2.5 }}>
              <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                  {!isCreating && (
                    <Tooltip title="Delete Identifier Type" placement="top">
                      <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                        <DeleteFilled />
                      </IconButton>
                    </Tooltip>
                  )}
                </Grid>
                <Grid item>
                  <Stack direction="row" spacing={2} alignItems="center">
                    <Button color="error" onClick={onCancel}>
                      Cancel
                    </Button>
                    <Button type="submit" variant="contained" disabled={isSubmitting}>
                      {identifierType ? 'Edit' : 'Add'}
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </DialogActions>
          </Form>
        </LocalizationProvider>
      </FormikProvider>
      {!isCreating && <AlertIdentifierTypeDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={identifierType.identifierTypeId!} />}
    </>
  );
};

export default AddEditIdentifierType;
