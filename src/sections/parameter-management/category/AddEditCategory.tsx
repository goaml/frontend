import { useState } from 'react';

// material-ui
import {
    // Autocomplete,
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    // FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField,
    Tooltip
} from '@mui/material';
// import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
// import { optionType, options } from 'data/options';
import { dataProps } from 'pages/parameter-management/category/list/types/types';
import AlertCategoryDelete from './AlertCategoryDelete';
import { addCategory, updateCategory } from 'store/reducers/category';
import { dispatch } from 'store';

// types

// constant
const getInitialValues = (user: FormikValues | null) => {

    const newUser = {
        categoryName: '',
        categoryDescription: ''
        //categoryId:undefined,
        
    }

    if (user) {
        return _.merge({}, newUser, user);
    }

    return newUser;
};

// ==============================|| CUSTOMER ADD / EDIT ||============================== //

export interface Props {
    user?: dataProps
    onCancel: () => void;
}

const AddEditUser = ({ user, onCancel }: Props) => {
    // const theme = useTheme();

    const isCreating = !user;

    const UserSchema = Yup.object().shape({
        categoryName: Yup.string().required('Category Name is required'),
        categoryDescription: Yup.string().required('Category Description is required'),

    });

    const formik = useFormik({
        initialValues: getInitialValues(user!),
        validationSchema: UserSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (user) {
                    // put
                    dispatch(updateCategory(values))
                } else {
                    // post
                    dispatch(addCategory(values))
                }
                resetForm()
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

    const [openAlert, setOpenAlert] = useState(false);

    const handleAlertClose = () => {
        setOpenAlert(!openAlert);
        onCancel();
    };



    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{user ? 'Edit Category' : 'Add Category'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                    <Grid container spacing={3}>
                                        
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="categoryName">Category Name <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="categoryName"
                                                    placeholder="Enter Category Name"
                                                    {...getFieldProps('categoryName')}
                                                    error={Boolean(touched.categoryName && errors.categoryName)}
                                                    helperText={touched.categoryName && errors.categoryName}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="categoryDescription">Category Description <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="categoryDescription"
                                                    placeholder="Enter Category Description"
                                                    {...getFieldProps('categoryDescription')}
                                                    error={Boolean(touched.categoryDescription && errors.categoryDescription)}
                                                    helperText={touched.categoryDescription && errors.categoryDescription}
                                                />
                                            </Stack>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 2.5 }}>
                            <Grid container justifyContent="space-between" alignItems="center">
                                <Grid item>
                                    {!isCreating && (
                                        <Tooltip title="Delete user" placement="top">
                                            <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                                                <DeleteFilled />
                                            </IconButton>
                                        </Tooltip>
                                    )}
                                </Grid>
                                <Grid item>
                                    <Stack direction="row" spacing={2} alignItems="center">
                                        <Button color="error" onClick={onCancel}>
                                            Cancel
                                        </Button>
                                        <Button type="submit" variant="contained" disabled={isSubmitting}>
                                            {user ? 'Edit' : 'Add'}
                                        </Button>
                                    </Stack>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </Form>
                </LocalizationProvider>
            </FormikProvider>
            {!isCreating && <AlertCategoryDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={user?.categoryId!} />}
        </>
    );
};

export default AddEditUser;
