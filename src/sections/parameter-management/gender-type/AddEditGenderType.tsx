import { useState } from 'react';

// material-ui
import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  InputLabel,
  Stack,
  TextField,
  Tooltip
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
import AlertGenderTypeDelete from './DeleteGenderType';
import { genderProps } from 'pages/parameter-management/gender-type/types/types';
import { addGenderType, updateGenderType } from 'store/reducers/gender-type';
import { dispatch } from 'store';


// types

// constant
const getInitialValues = (genderType: FormikValues | null) => {

  const newGenderType = {
   genderTypeId: undefined,
   genderTypeCode: '',
    description: '',
    statusId: 1,
    //limitAmt: 0
  }

  if (genderType) {
    return _.merge({}, newGenderType,genderType);
  }

  return newGenderType;
};

// ==============================||genderType ADD / EDIT ||============================== //

export interface Props {
 genderType?: genderProps
  onCancel: () => void;
}

const AddEditGenderType = ({genderType, onCancel }: Props) => {

  const isCreating = !genderType;

  const genderTypeSchema = Yup.object().shape({
    genderTypeCode: Yup.string().required('Gender Type Code is required'),
    description: Yup.string().required('Gender Type Description is required'),
  });

  const formik = useFormik({
    initialValues: getInitialValues(genderType!),
    validationSchema:genderTypeSchema,
    enableReinitialize: true,
    onSubmit: (values, { setSubmitting, resetForm }) => {
      try {
        if (genderType) {
          // put
          dispatch(updateGenderType(values))

        } else {
          // post
          
          dispatch(addGenderType(values))
      
        }
        resetForm()
        setSubmitting(false);
        onCancel();
      } catch (error) {
        console.error(error);
      }
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  const [openAlert, setOpenAlert] = useState(false);

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
    onCancel();
  };
  return (
    <>
      <FormikProvider value={formik}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
            <DialogTitle>{genderType ? 'Edit Gender Type' : ' Add Gender Type'}</DialogTitle>
            <Divider />
            <DialogContent sx={{ p: 2.5 }}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={12}>
                  <Grid container spacing={3}>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="genderTypeCode">Gender Type Code <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="genderTypeCode"
                          placeholder="Enter Gender Type Code "
                          {...getFieldProps('genderTypeCode')}
                          error={Boolean(touched.genderTypeCode && errors.genderTypeCode)}
                          helperText={touched.genderTypeCode && errors.genderTypeCode}
                        />
                      </Stack>
                    </Grid>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="description">Gender Type Description <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="description"
                          placeholder="Enter Gender Type Description"
                          {...getFieldProps('description')}
                          error={Boolean(touched.description && errors.description)}
                          helperText={touched.description && errors.description}
                        />
                      </Stack>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <Divider />
            <DialogActions sx={{ p: 2.5 }}>
              <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                  {!isCreating && (
                    <Tooltip title="Delete user" placement="top">
                      <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                        <DeleteFilled />
                      </IconButton>
                    </Tooltip>
                  )}
                </Grid>
                <Grid item>
                  <Stack direction="row" spacing={2} alignItems="center">
                    <Button color="error" onClick={onCancel}>
                      Cancel
                    </Button>
                    <Button type="submit" variant="contained" disabled={isSubmitting}>
                      {genderType ? 'Edit' : 'Add'}
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </DialogActions>
          </Form>
        </LocalizationProvider>
      </FormikProvider>
      {!isCreating && <AlertGenderTypeDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={genderType.genderTypeId!} />}
    </>
  );
};

export default AddEditGenderType;
