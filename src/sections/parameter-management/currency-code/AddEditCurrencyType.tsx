import { useState } from 'react';

// material-ui
import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  InputLabel,
  Stack,
  TextField,
  Tooltip
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
import AlertCurrenciesDelete from './DeleteCurrencyType';
import { currencyProps } from 'pages/parameter-management/currency-code/types/types';
import { addCurrencies, updateCurrencies } from 'store/reducers/currency-code';
import { dispatch } from 'store';


// types

// constant
const getInitialValues = (currencies: FormikValues | null) => {

  const newSubmissionCode = {
   currenciesId: undefined,
   currenciesCode: '',
    description: '',
    statusId: 1,
    //limitAmt: 0
  }

  if (currencies) {
    return _.merge({}, newSubmissionCode,currencies);
  }

  return newSubmissionCode;
};

// ==============================||currencies ADD / EDIT ||============================== //

export interface Props {
 currencies?: currencyProps
  onCancel: () => void;
}

const AddEditCurrencies = ({currencies, onCancel }: Props) => {

  const isCreating = !currencies;

  const currenciesSchema = Yup.object().shape({
   currenciesCode: Yup.string().required('Currency Type Code is required'),
   description: Yup.string().required('Currency Type Description is required'),
  });

  const formik = useFormik({
    initialValues: getInitialValues(currencies!),
    validationSchema:currenciesSchema,
    enableReinitialize: true,
    onSubmit: (values, { setSubmitting, resetForm }) => {
      try {
        if (currencies) {
          // put
          dispatch(updateCurrencies(values))

        } else {
          // post
          dispatch(addCurrencies(values))
      
        }
        resetForm()
        setSubmitting(false);
        onCancel();
      } catch (error) {
        console.error(error);
      }
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  const [openAlert, setOpenAlert] = useState(false);

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
    onCancel();
  };
  return (
    <>
      <FormikProvider value={formik}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
            <DialogTitle>{currencies ? 'Edit Currency Type' : ' Add Currency Type'}</DialogTitle>
            <Divider />
            <DialogContent sx={{ p: 2.5 }}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={12}>
                  <Grid container spacing={3}>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="currenciesCode">Currency Type Code <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="currenciesCode"
                          placeholder="Enter Currency Type Code "
                          {...getFieldProps('currenciesCode')}
                          error={Boolean(touched.currenciesCode && errors.currenciesCode)}
                          helperText={touched.currenciesCode && errors.currenciesCode}
                        />
                      </Stack>
                    </Grid>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="description">Currency Type Description <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="description"
                          placeholder="Enter Currency Type Description"
                          {...getFieldProps('description')}
                          error={Boolean(touched.description && errors.description)}
                          helperText={touched.description && errors.description}
                        />
                      </Stack>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <Divider />
            <DialogActions sx={{ p: 2.5 }}>
              <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                  {!isCreating && (
                    <Tooltip title="Delete Currency Code" placement="top">
                      <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                        <DeleteFilled />
                      </IconButton>
                    </Tooltip>
                  )}
                </Grid>
                <Grid item>
                  <Stack direction="row" spacing={2} alignItems="center">
                    <Button color="error" onClick={onCancel}>
                      Cancel
                    </Button>
                    <Button type="submit" variant="contained" disabled={isSubmitting}>
                      {currencies ? 'Edit' : 'Add'}
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </DialogActions>
          </Form>
        </LocalizationProvider>
      </FormikProvider>
      {!isCreating && <AlertCurrenciesDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={currencies.currenciesId!} />}
    </>
  );
};

export default AddEditCurrencies;
