import { useState } from 'react';

// material-ui
import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  InputLabel,
  Stack,
  TextField,
  Tooltip
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
import AlertAccountStatusCodeDelete from './DeleteAccStatusType';
import { accountStatusProps } from 'pages/parameter-management/accoutStatus-type/types/types';
import { dispatch } from 'store';
import { addAccountStatusType, updateAccountStatusType } from 'store/reducers/account-status-type';


// types

// constant
const getInitialValues = (accountStatusCode: FormikValues | null) => {

  const newaccountStatusCode = {
    accountStatusTypeCodeId: undefined,
    accountStatusTypeCode: '',
    description: '',
    statusId: 1,
    //limitAmt: 0
  }

  if (accountStatusCode) {
    return _.merge({}, newaccountStatusCode, accountStatusCode);
  }

  return newaccountStatusCode;
};

// ==============================|| accountStatusCode ADD / EDIT ||============================== //

export interface Props {
  accountStatusCode?: accountStatusProps
  onCancel: () => void;
}

const AddEditAccountStatusCode = ({ accountStatusCode, onCancel }: Props) => {

  const isCreating = !accountStatusCode;

  const accountStatusCodeSchema = Yup.object().shape({
    accountStatusTypeCode: Yup.string().required('Account Status Code  is required'),
    description: Yup.string().required('Account Status Code Description is required'),
  });

  const formik = useFormik({
    initialValues: getInitialValues(accountStatusCode!),
    validationSchema: accountStatusCodeSchema,
    enableReinitialize: true,
    onSubmit: (values, { setSubmitting, resetForm }) => {
      try {
        if (accountStatusCode) {
          // put
          dispatch(updateAccountStatusType(values))

        } else {
          // post
          dispatch(addAccountStatusType(values))
        }
        resetForm()
        setSubmitting(false);
        onCancel();
      } catch (error) {
        console.error(error);
      }
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  const [openAlert, setOpenAlert] = useState(false);

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
    onCancel();
  };
  return (
    <>
      <FormikProvider value={formik}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
            <DialogTitle>{accountStatusCode ? 'Edit Account Status Type' : ' Add Account Status Type'}</DialogTitle>
            <Divider />
            <DialogContent sx={{ p: 2.5 }}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={12}>
                  <Grid container spacing={3}>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="accountStatusTypeCode">Account Status Code <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="accountStatusTypeCode"
                          placeholder="Enter Account Status Code "
                          {...getFieldProps('accountStatusTypeCode')}
                          error={Boolean(touched.accountStatusTypeCode && errors.accountStatusTypeCode)}
                          helperText={touched.accountStatusTypeCode && errors.accountStatusTypeCode}
                        />
                      </Stack>
                    </Grid>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="description">Account Status Code Description <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="description"
                          placeholder="Enter Account Status Code Description"
                          {...getFieldProps('description')}
                          error={Boolean(touched.description && errors.description)}
                          helperText={touched.description && errors.description}
                        />
                      </Stack>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <Divider />
            <DialogActions sx={{ p: 2.5 }}>
              <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                  {!isCreating && (
                    <Tooltip title="Delete user" placement="top">
                      <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                        <DeleteFilled />
                      </IconButton>
                    </Tooltip>
                  )}
                </Grid>
                <Grid item>
                  <Stack direction="row" spacing={2} alignItems="center">
                    <Button color="error" onClick={onCancel}>
                      Cancel
                    </Button>
                    <Button type="submit" variant="contained" disabled={isSubmitting}>
                      {accountStatusCode ? 'Edit' : 'Add'}
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </DialogActions>
          </Form>
        </LocalizationProvider>
      </FormikProvider>
      {!isCreating && <AlertAccountStatusCodeDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={accountStatusCode.accountStatusTypeId!} />}
    </>
  );
};

export default AddEditAccountStatusCode;
