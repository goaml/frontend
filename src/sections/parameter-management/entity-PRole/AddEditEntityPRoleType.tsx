import { useState } from 'react';

// material-ui
import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  InputLabel,
  Stack,
  TextField,
  Tooltip
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
import AlertEntityPRoleDelete from './DeleteEntityPRoleType';
import { entityPRoleProps } from 'pages/parameter-management/entity-PRole/types/types';
import { dispatch } from 'store';
import { addEntityPersonRoleTypeType, updateEntityPersonRoleTypeType } from 'store/reducers/entity-person-role';


// types

// constant
const getInitialValues = (entityPRoleType: FormikValues | null) => {

  const newSubmissionCode = {
    entityPersonRoleTypeCodeId: undefined,
    entityPersonRoleTypeCode: '',
    description: '',
    statusId: 1,
    //limitAmt: 0
  }

  if (entityPRoleType) {
    return _.merge({}, newSubmissionCode, entityPRoleType);
  }

  return newSubmissionCode;
};

// ==============================|| entityPRoleType ADD / EDIT ||============================== //

export interface Props {
  entityPRoleType?: entityPRoleProps
  onCancel: () => void;
}

const AddEditEntityPRoleTypeCode = ({ entityPRoleType, onCancel }: Props) => {

  const isCreating = !entityPRoleType;

  const entityPRoleTypeCodeSchema = Yup.object().shape({
    entityPersonRoleTypeCode: Yup.string().required('Entity Person Role Type Code is required'),
    description: Yup.string().required('Entity Person Role Type Description is required'),
  });

  const formik = useFormik({
    initialValues: getInitialValues(entityPRoleType!),
    validationSchema: entityPRoleTypeCodeSchema,
    enableReinitialize: true,
    onSubmit: (values, { setSubmitting, resetForm }) => {
      try {
        if (entityPRoleType) {
          // put
          dispatch(updateEntityPersonRoleTypeType(values))

        } else {
          // post
          dispatch(addEntityPersonRoleTypeType(values))
      
        }
        resetForm()
        setSubmitting(false);
        onCancel();
      } catch (error) {
        console.error(error);
      }
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  const [openAlert, setOpenAlert] = useState(false);

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
    onCancel();
  };
  return (
    <>
      <FormikProvider value={formik}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
            <DialogTitle>{entityPRoleType ? 'Edit Entity Person Role Type ' : ' Add Entity Person Role Type '}</DialogTitle>
            <Divider />
            <DialogContent sx={{ p: 2.5 }}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={12}>
                  <Grid container spacing={3}>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="entityPersonRoleTypeCode">Entity Person Role Type <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="entityPersonRoleTypeCode"
                          placeholder="Enter Entity Person Role Type "
                          {...getFieldProps('entityPersonRoleTypeCode')}
                          error={Boolean(touched.entityPersonRoleTypeCode && errors.entityPersonRoleTypeCode)}
                          helperText={touched.entityPersonRoleTypeCode && errors.entityPersonRoleTypeCode}
                        />
                      </Stack>
                    </Grid>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="description">Entity Person Role Type  Description <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="description"
                          placeholder="Enter Entity Person Role Type Description"
                          {...getFieldProps('description')}
                          error={Boolean(touched.description && errors.description)}
                          helperText={touched.description && errors.description}
                        />
                      </Stack>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <Divider />
            <DialogActions sx={{ p: 2.5 }}>
              <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                  {!isCreating && (
                    <Tooltip title="Delete Entity Person Role Type" placement="top">
                      <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                        <DeleteFilled />
                      </IconButton>
                    </Tooltip>
                  )}
                </Grid>
                <Grid item>
                  <Stack direction="row" spacing={2} alignItems="center">
                    <Button color="error" onClick={onCancel}>
                      Cancel
                    </Button>
                    <Button type="submit" variant="contained" disabled={isSubmitting}>
                      {entityPRoleType ? 'Edit' : 'Add'}
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </DialogActions>
          </Form>
        </LocalizationProvider>
      </FormikProvider>
      {!isCreating && <AlertEntityPRoleDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={entityPRoleType.entityPersonRoleTypeId!} />}
    </>
  );
};

export default AddEditEntityPRoleTypeCode;
