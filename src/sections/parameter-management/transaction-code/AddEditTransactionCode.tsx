import { useState } from 'react';

// material-ui
import {
    // Autocomplete,
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    // FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField,
    Tooltip
} from '@mui/material';
// import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
// import { optionType, options } from 'data/options';
import { userProps } from 'pages/parameter-management/transaction-code/list/types/types';
import AlertTransactionCodeDelete from './AlertTransactionCodeDelete';
import { addTransactionCode, updateTransactionCode } from 'store/reducers/transaction-code';
import { dispatch } from 'store';

// types

// constant
const getInitialValues = (user: FormikValues | null) => {

    const newUser = {
        transactionCodeName: '',
        transactionCodeDescription: '',
        
    }

    if (user) {
        return _.merge({}, newUser, user);
    }

    return newUser;
};

// ==============================|| CUSTOMER ADD / EDIT ||============================== //

export interface Props {
    user?: userProps
    onCancel: () => void;
}

const AddEditUser = ({ user, onCancel }: Props) => {
    // const theme = useTheme();

    const isCreating = !user;

    const UserSchema = Yup.object().shape({

        transactionCodeName: Yup.string().required('Transaction Code name is required'),
        transactionCodeDescription: Yup.string().required('Transaction Code Descriptions is required'),


    });

    const formik = useFormik({
        initialValues: getInitialValues(user!),
        validationSchema: UserSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (user) {
                    // put
                    dispatch(updateTransactionCode(values))
                } else {
                    // post
                    dispatch(addTransactionCode(values))
                }
                resetForm()
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

    const [openAlert, setOpenAlert] = useState(false);

    const handleAlertClose = () => {
        setOpenAlert(!openAlert);
        onCancel();
    };



    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{user ? 'Edit Transaction Code' : 'New Transaction Code'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                    <Grid container spacing={3}>
                                        
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="transactionCodeName">Transaction Code Name <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="transactionCodeName"
                                                    placeholder="Enter Transaction Code Name"
                                                    {...getFieldProps('transactionCodeName')}
                                                    error={Boolean(touched.transactionCodeName && errors.transactionCodeName)}
                                                    helperText={touched.transactionCodeName && errors.transactionCodeName}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="transactionCodeDescription">Transaction Code Description <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="transactionCodeDescription"
                                                    placeholder="Enter Transaction Code Description"
                                                    {...getFieldProps('transactionCodeDescription')}
                                                    error={Boolean(touched.transactionCodeDescription && errors.transactionCodeDescription)}
                                                    helperText={touched.transactionCodeDescription && errors.transactionCodeDescription}
                                                />
                                            </Stack>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 2.5 }}>
                            <Grid container justifyContent="space-between" alignItems="center">
                                <Grid item>
                                    {!isCreating && (
                                        <Tooltip title="Delete user" placement="top">
                                            <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                                                <DeleteFilled />
                                            </IconButton>
                                        </Tooltip>
                                    )}
                                </Grid>
                                <Grid item>
                                    <Stack direction="row" spacing={2} alignItems="center">
                                        <Button color="error" onClick={onCancel}>
                                            Cancel
                                        </Button>
                                        <Button type="submit" variant="contained" disabled={isSubmitting}>
                                            {user ? 'Edit' : 'Add'}
                                        </Button>
                                    </Stack>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </Form>
                </LocalizationProvider>
            </FormikProvider>
            {!isCreating && <AlertTransactionCodeDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={user?.transactionCodeId!} />}
        </>
    );
};

export default AddEditUser;
