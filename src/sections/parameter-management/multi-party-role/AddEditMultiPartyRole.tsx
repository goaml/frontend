import { useState } from 'react';

// material-ui
import {
    // Autocomplete,
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    // FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField,
    Tooltip
} from '@mui/material';
// import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
// import { optionType, options } from 'data/options';
import { userProps } from 'pages/parameter-management/multi-party-role/list/types/types';
import AlertMultiPartyRoleDelete from './AlertMultiPartyRoleDelete';
import { addMultiPartyRole, updateMultiPartyRole } from 'store/reducers/multiParty-Role';
import { dispatch } from 'store';

// types

// constant
const getInitialValues = (user: FormikValues | null) => {

    const newUser = {
        multiPartyRoleName: '',
        multiPartyRoleDescription: '',
        isActive:true
        
    }

    if (user) {
        return _.merge({}, newUser, user);
    }

    return newUser;
};

// ==============================|| CUSTOMER ADD / EDIT ||============================== //

export interface Props {
    user?: userProps
    onCancel: () => void;
}

const AddEditUser = ({ user, onCancel }: Props) => {
    // const theme = useTheme();

    const isCreating = !user;

    const UserSchema = Yup.object().shape({
        multiPartyRoleName: Yup.string().required('Multi Party Role Name is required'),
        multiPartyRoleDescription: Yup.string().required('Multi Party Role Description is required'),

    });

    const formik = useFormik({
        initialValues: getInitialValues(user!),
        validationSchema: UserSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (user) {
                    // put
                    dispatch(updateMultiPartyRole(values))
                } else {
                    // post
                    dispatch(addMultiPartyRole(values))
                }
                resetForm()
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

    const [openAlert, setOpenAlert] = useState(false);

    const handleAlertClose = () => {
        setOpenAlert(!openAlert);
        onCancel();
    };



    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{user ? 'Edit Multi Party Role' : 'Add Multi Party Role'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                    <Grid container spacing={3}>
                                        
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="multiPartyRoleName">Multi Party Role Name <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="multiPartyRoleName"
                                                    placeholder="Enter Multi Party Role Name"
                                                    {...getFieldProps('multiPartyRoleName')}
                                                    error={Boolean(touched.multiPartyRoleName && errors.multiPartyRoleName)}
                                                    helperText={touched.multiPartyRoleName && errors.multiPartyRoleName}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="multiPartyRoleDescription">Multi Party Role Description <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="multiPartyRoleDescription"
                                                    placeholder="Enter Multi Party Role Description"
                                                    {...getFieldProps('multiPartyRoleDescription')}
                                                    error={Boolean(touched.multiPartyRoleDescription && errors.multiPartyRoleDescription)}
                                                    helperText={touched.multiPartyRoleDescription && errors.multiPartyRoleDescription}
                                                />
                                            </Stack>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 2.5 }}>
                            <Grid container justifyContent="space-between" alignItems="center">
                                <Grid item>
                                    {!isCreating && (
                                        <Tooltip title="Delete Multi Party Role" placement="top">
                                            <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                                                <DeleteFilled />
                                            </IconButton>
                                        </Tooltip>
                                    )}
                                </Grid>
                                <Grid item>
                                    <Stack direction="row" spacing={2} alignItems="center">
                                        <Button color="error" onClick={onCancel}>
                                            Cancel
                                        </Button>
                                        <Button type="submit" variant="contained" disabled={isSubmitting}>
                                            {user ? 'Edit' : 'Add'}
                                        </Button>
                                    </Stack>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </Form>
                </LocalizationProvider>
            </FormikProvider>
            {!isCreating && <AlertMultiPartyRoleDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={user?.multiPartyRoleId!} />}
        </>
    );
};

export default AddEditUser;
