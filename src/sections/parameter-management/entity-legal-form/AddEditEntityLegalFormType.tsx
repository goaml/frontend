import { useState } from 'react';

// material-ui
import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  InputLabel,
  Stack,
  TextField,
  Tooltip
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
import AlertEntityLegalFormTypeDelete from './DeleteEntityLegalFormType';
import { entityLegalFormProps } from 'pages/parameter-management/entity-legal-form/types/types';
import { dispatch } from 'store';
import { addEntityLegalFormType, updateEntityLegalFormType } from 'store/reducers/entity-legal-form-type';


// types

// constant
const getInitialValues = (entityLegalFormType: FormikValues | null) => {

  const newSubmissionCode = {
   entityLegalFormTypeId: undefined,
   entityLegalFormTypeCode: '',
    description: '',
    statusId: 1,
    //limitAmt: 0
  }

  if (entityLegalFormType) {
    return _.merge({}, newSubmissionCode,entityLegalFormType);
  }

  return newSubmissionCode;
};

// ==============================||entityLegalFormType ADD / EDIT ||============================== //

export interface Props {
 entityLegalFormType?: entityLegalFormProps
  onCancel: () => void;
}

const AddEditEntityLegalFormType = ({entityLegalFormType, onCancel }: Props) => {

  const isCreating = !entityLegalFormType;

  const entityLegalFormTypeSchema = Yup.object().shape({
   entityLegalFormTypeCode: Yup.string().required('Entity Legal Form Type Code is required'),
   description: Yup.string().required('Entity Legal Form Type Description is required'),
  });

  const formik = useFormik({
    initialValues: getInitialValues(entityLegalFormType!),
    validationSchema:entityLegalFormTypeSchema,
    enableReinitialize: true,
    onSubmit: (values, { setSubmitting, resetForm }) => {
      try {
        if (entityLegalFormType) {
          // put
          dispatch(updateEntityLegalFormType(values))

        } else {
          // post
          
          dispatch(addEntityLegalFormType(values))
      
        }
        resetForm()
        setSubmitting(false);
        onCancel();
      } catch (error) {
        console.error(error);
      }
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  const [openAlert, setOpenAlert] = useState(false);

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
    onCancel();
  };
  return (
    <>
      <FormikProvider value={formik}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
            <DialogTitle>{entityLegalFormType ? 'Edit Entity Legal Form Type' : ' Add Entity Legal Form Type'}</DialogTitle>
            <Divider />
            <DialogContent sx={{ p: 2.5 }}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={12}>
                  <Grid container spacing={3}>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="entityLegalFormTypeCode">Entity Legal Form Type Code <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="entityLegalFormTypeCode"
                          placeholder="Enter Entity Legal Form Type Code "
                          {...getFieldProps('entityLegalFormTypeCode')}
                          error={Boolean(touched.entityLegalFormTypeCode && errors.entityLegalFormTypeCode)}
                          helperText={touched.entityLegalFormTypeCode && errors.entityLegalFormTypeCode}
                        />
                      </Stack>
                    </Grid>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="description">Entity Legal Form Type Description <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="description"
                          placeholder="Enter Entity Legal Form Type Description"
                          {...getFieldProps('description')}
                          error={Boolean(touched.description && errors.description)}
                          helperText={touched.description && errors.description}
                        />
                      </Stack>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <Divider />
            <DialogActions sx={{ p: 2.5 }}>
              <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                  {!isCreating && (
                    <Tooltip title="Delete Entity Legal Form Type" placement="top">
                      <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                        <DeleteFilled />
                      </IconButton>
                    </Tooltip>
                  )}
                </Grid>
                <Grid item>
                  <Stack direction="row" spacing={2} alignItems="center">
                    <Button color="error" onClick={onCancel}>
                      Cancel
                    </Button>
                    <Button type="submit" variant="contained" disabled={isSubmitting}>
                      {entityLegalFormType ? 'Edit' : 'Add'}
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </DialogActions>
          </Form>
        </LocalizationProvider>
      </FormikProvider>
      {!isCreating && <AlertEntityLegalFormTypeDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={entityLegalFormType.entityLegalFormTypeId!} />}
    </>
  );
};

export default AddEditEntityLegalFormType;
