//import { useState } from 'react';

// material-ui
import {
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    // FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField
} from '@mui/material';
// import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { FormikProvider, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
// import IconButton from 'components/@extended/IconButton';

// // assets
// import { DeleteFilled } from '@ant-design/icons';
// import AlertTransactionsDelete from './AlertTransactionsDelete';
// import { fetchCategoryList } from 'store/reducers/category';
// import { dispatch, useSelector } from 'store';
// //import { CategoryType } from 'types/category';
// import { fetchFundTypeList } from 'store/reducers/fund-type';
// import { FundTypeType } from 'types/fund-type';
// import { fetchfundTypeDescriptionList } from 'store/reducers/fund-desc';
// import { FundTypesType } from 'types/fund-desc';
// import { fetchMultiPartyFundsCodeList } from 'store/reducers/multyParty-fundCode';
// import { fetchMultiPartyInvolvePartyList } from 'store/reducers/multiParty-Involve-party';
// import { fetchMultiPartyRoleList } from 'store/reducers/multiParty-Role';
// import { fetchTransactionCodeList } from 'store/reducers/transaction-code';
// import { fetchRPTList } from 'store/reducers/rpt-code';
// import { MultiPartyFundsCodeType } from 'types/multiParty-fundCode';
// import { MultiPartyInvolvePartyType } from 'types/multiParty-Involve-party';
// import { MultiPartyRoleType } from 'types/multiParty-Role';
// import { ReportTypeType } from 'types/report-type';
// import { RPTCodeType } from 'types/rpt-code';
// 
// import { TransactionCodeType } from 'types/transaction-code';
// import { fetchReportTypeList } from 'store/reducers/report-type';
// import { transactionProps } from 'pages/parameter-management/transactions/list/types/types';
//import { optionType } from 'data/options';

import { dataProps } from 'pages/applications/trans-err-log/type/types';

// types

export interface FormikValues {
    acid?: string;
    amountLocal?: number;
    custId?: string;
    transactionDateFrom ?: string;
    transactionDateTo ?: string;
    failReason?: string;
    foracid?: string;
    isValidated?: boolean;
    logId?: number;
    processDate?: string;
    rptCode?: string;
    transactionNumber?: string;
    trxNo?: string;
}



export interface Props {
    user?: dataProps;
    onCancel: () => void;
}

const getInitialValues = (user: FormikValues | null) => {
    const newUser: FormikValues = {
    acid: "",
    amountLocal: 0,
    custId: "",
    transactionDateFrom : "",
    transactionDateTo : "",
    failReason: "",
    foracid: "",
    isValidated: true,
    logId: 0,
    processDate: "",
    rptCode: "",
    transactionNumber: "",
    trxNo: "",
    };

    if (user) {
        return _.merge({}, newUser, user);
    }

    return newUser;
};

const AddEditUser = ({ user, onCancel }: Props) => {
    // const theme = useTheme();
    // const isCreating = !user;

    const UserSchema = Yup.object().shape({
       
    });

    const formik = useFormik({
        initialValues: getInitialValues(user || null),
        validationSchema: UserSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (user) {
                    // put request logic
                } else {
                    // post request logic
                }
                resetForm();
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, getFieldProps } = formik;

    // const [openAlert, setOpenAlert] = useState(false);

    // const handleAlertClose = () => {
    //     setOpenAlert(!openAlert);
    //     onCancel();
    // };

    const getHelperText = (fieldError: any): string | undefined => {
        return typeof fieldError === 'string' ? fieldError : undefined;
    };

    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{user ? 'View Transaction Details' : 'New Transaction'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                    <Grid container spacing={3}>
                                    <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="processDate">Process Date</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="processDate"
                                                    {...getFieldProps('processDate')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.processDate && errors.processDate)}
                                                    helperText={getHelperText(touched.processDate && errors.processDate)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.2}>
                                                <InputLabel htmlFor="transactionNumber">Transaction Number</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="transactionNumber"
                                                    {...getFieldProps('transactionNumber')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.transactionNumber && errors.transactionNumber)}
                                                    helperText={getHelperText(touched.transactionNumber && errors.transactionNumber)}
                                                />
                                            </Stack>
                                        </Grid>
                                        {/* <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="dateTransaction">Transaction Date</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="dateTransaction"
                                                    {...getFieldProps('dateTransaction')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    value={formik.values.dateTransaction || 'Not Available'}
                                                    error={Boolean(touched.dateTransaction && errors.dateTransaction)}
                                                    helperText={getHelperText(touched.dateTransaction && errors.dateTransaction)}
                                                />
                                            </Stack>
                                        </Grid> */}
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="amountLocal">Amount</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="amountLocal"
                                                    {...getFieldProps('amountLocal')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.amountLocal && errors.amountLocal)}
                                                    helperText={getHelperText(touched.amountLocal && errors.amountLocal)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="isValidated">Validated</InputLabel>
                                                <TextField   
                                                    fullWidth
                                                    id="isValidated"
                                                    {...getFieldProps('isValidated')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.isValidated && errors.isValidated)}
                                                    helperText={getHelperText(touched.isValidated && errors.isValidated)}
                                                />
                                            </Stack>
                                        </Grid>
                                        
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="custId">Cust ID</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="custId"
                                                    {...getFieldProps('custId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.custId && errors.custId)}
                                                    helperText={getHelperText(touched.custId && errors.custId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="acid">Account No</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="acid"
                                                    {...getFieldProps('acid')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.acid && errors.acid)}
                                                    helperText={getHelperText(touched.acid && errors.acid)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="rptCode">RPT Code</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="rptCode"
                                                    {...getFieldProps('rptCode')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.rptCode && errors.rptCode)}
                                                    helperText={getHelperText(touched.rptCode && errors.rptCode)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="trxNo">GoAML Code</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="trxNo"
                                                    {...getFieldProps('trxNo')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.trxNo && errors.trxNo)}
                                                    helperText={getHelperText(touched.trxNo && errors.trxNo)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="failReason">Error Message</InputLabel>
                                                <TextField 
                                                    fullWidth
                                                    id="failReason"
                                                    {...getFieldProps('failReason')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    value={formik.values.failReason || 'Not Available'}
                                                    error={Boolean(touched.failReason && errors.failReason)}
                                                    helperText={getHelperText(touched.failReason && errors.failReason)}
                                                />
                                            </Stack>
                                            </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 1.25 }}>

                            <Button color="error" onClick={onCancel}>
                                Cancel
                            </Button>
                        </DialogActions>
                    </form>
                </LocalizationProvider>
            </FormikProvider>
        </>
    );
};

export default AddEditUser;
