import { useState } from 'react';

// material-ui
import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  InputLabel,
  Stack,
  TextField,
  Tooltip
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
import { BusinessProps } from 'pages/parameter-management/business-nature/list/types/types';
import AlertBusinessNatureDelete from './DeleteBusinessNature';


// types

// constant
const getInitialValues = (businessNature: FormikValues | null) => {

  const newBusinessNature = {
    BusinessId: 0,
    BusinessNatureCode: undefined,
    desc: undefined
  }

  if (businessNature) {
    return _.merge({}, newBusinessNature, businessNature);
  }

  return newBusinessNature;
};

// ==============================|| Business-Nature ADD / EDIT ||============================== //

export interface Props {
  businessNature?: BusinessProps
  onCancel: () => void;
}

const AddEditBusinessNature = ({ businessNature, onCancel }: Props) => {

  const isCreating = !businessNature;

  const BusinessNatureSchema = Yup.object().shape({
    BusinessNatureCode: Yup.string().required('Business Nature Name is required'),
    desc: Yup.string().required('Business Nature Location is required'),
  });

  const formik = useFormik({
    initialValues: getInitialValues(businessNature!),
    validationSchema: BusinessNatureSchema,
    enableReinitialize: true,
    onSubmit: (values, { setSubmitting, resetForm }) => {
      try {
        if (businessNature) {
          // put
          
        } else {
          // post
          
        }
        resetForm()
        setSubmitting(false);
        onCancel();
      } catch (error) {
        console.error(error);
      }
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  const [openAlert, setOpenAlert] = useState(false);

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
    onCancel();
  };
  return (
    <>
      <FormikProvider value={formik}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
            <DialogTitle>{businessNature ? 'Edit Business Nature Code' : ' Add New Business Nature Code'}</DialogTitle>
            <Divider />
            <DialogContent sx={{ p: 2.5 }}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={12}>
                  <Grid container spacing={3}>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="BusinessNatureCode">Business Nature Code <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="BusinessNatureCode"
                          placeholder="Enter Business Nature Code"
                          {...getFieldProps('BusinessNatureCode')}
                          error={Boolean(touched.BusinessNatureCode && errors.BusinessNatureCode)}
                          helperText={touched.BusinessNatureCode && errors.BusinessNatureCode}
                        />
                      </Stack>
                    </Grid>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="desc">Business Nature Description <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="desc"
                          placeholder="Enter BusinessNature Description"
                          {...getFieldProps('desc')}
                          error={Boolean(touched.desc && errors.desc)}
                          helperText={touched.desc && errors.desc}
                        />
                      </Stack>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <Divider />
            <DialogActions sx={{ p: 2.5 }}>
              <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                  {!isCreating && (
                    <Tooltip title="Delete user" placement="top">
                      <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                        <DeleteFilled />
                      </IconButton>
                    </Tooltip>
                  )}
                </Grid>
                <Grid item>
                  <Stack direction="row" spacing={2} alignItems="center">
                    <Button color="error" onClick={onCancel}>
                      Cancel
                    </Button>
                    <Button type="submit" variant="contained" disabled={isSubmitting}>
                      {businessNature ? 'Edit' : 'Add'}
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </DialogActions>
          </Form>
        </LocalizationProvider>
      </FormikProvider>
      {!isCreating && <AlertBusinessNatureDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={businessNature.BusinessId!} />}
    </>
  );
};

export default AddEditBusinessNature;
