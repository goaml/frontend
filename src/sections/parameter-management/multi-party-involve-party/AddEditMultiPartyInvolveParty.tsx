import { useState } from 'react';

// material-ui
import {
    // Autocomplete,
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    // FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField,
    Tooltip
} from '@mui/material';
// import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
// import { optionType, options } from 'data/options';
import { userProps } from 'pages/parameter-management/multi-party-involve-party/list/types/types';
import AlertMultiPartyInvolvePartyDelete from './AlertMultiPartyInvolvePartyDelete';
import { dispatch } from 'store';
import { addMultiPartyInvolveParty, updateMultiPartyInvolveParty } from 'store/reducers/multiParty-Involve-party';

// types

// constant
const getInitialValues = (user: FormikValues | null) => {

    const newUser = {
        multiPartyInvolvePartyName: '',
        multiPartyInvolvePartyDescription: '',
        
    }

    if (user) {
        return _.merge({}, newUser, user);
    }

    return newUser;
};

// ==============================|| CUSTOMER ADD / EDIT ||============================== //

export interface Props {
    user?: userProps
    onCancel: () => void;
}

const AddEditUser = ({ user, onCancel }: Props) => {
    // const theme = useTheme();

    const isCreating = !user;

    const UserSchema = Yup.object().shape({

        multiPartyInvolvePartyName: Yup.string().required('Invlove Party name is required'),
        multiPartyInvolvePartyDescription: Yup.string().required('Invlove Party name is required'),


    });

    const formik = useFormik({
        initialValues: getInitialValues(user!),
        validationSchema: UserSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (user) {
                    // put
                    dispatch(updateMultiPartyInvolveParty(values))
                } else {
                    // post
                    dispatch(addMultiPartyInvolveParty(values))
                }
                resetForm()
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

    const [openAlert, setOpenAlert] = useState(false);

    const handleAlertClose = () => {
        setOpenAlert(!openAlert);
        onCancel();
    };



    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{user ? 'Edit Multi Party Fund Code' : 'New Multi Party Fund Code'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                    <Grid container spacing={3}>
                                        
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="multiPartyInvolvePartyName">Multi Party Involve Party Name <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="multiPartyInvolvePartyName"
                                                    placeholder="Enter Multi Party Involve Party Name"
                                                    {...getFieldProps('multiPartyInvolvePartyName')}
                                                    error={Boolean(touched.multiPartyInvolvePartyName && errors.multiPartyInvolvePartyName)}
                                                    helperText={touched.multiPartyInvolvePartyName && errors.multiPartyInvolvePartyName}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="multiPartyInvolvePartyDescription">Multi Party Involve Party Description <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="multiPartyInvolvePartyDescription"
                                                    placeholder="Enter Multi Party Involve Party Description"
                                                    {...getFieldProps('multiPartyInvolvePartyDescription')}
                                                    error={Boolean(touched.multiPartyInvolvePartyDescription && errors.multiPartyInvolvePartyDescription)}
                                                    helperText={touched.multiPartyInvolvePartyDescription && errors.multiPartyInvolvePartyDescription}
                                                />
                                            </Stack>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 2.5 }}>
                            <Grid container justifyContent="space-between" alignItems="center">
                                <Grid item>
                                    {!isCreating && (
                                        <Tooltip title="Delete user" placement="top">
                                            <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                                                <DeleteFilled />
                                            </IconButton>
                                        </Tooltip>
                                    )}
                                </Grid>
                                <Grid item>
                                    <Stack direction="row" spacing={2} alignItems="center">
                                        <Button color="error" onClick={onCancel}>
                                            Cancel
                                        </Button>
                                        <Button type="submit" variant="contained" disabled={isSubmitting}>
                                            {user ? 'Edit' : 'Add'}
                                        </Button>
                                    </Stack>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </Form>
                </LocalizationProvider>
            </FormikProvider>
            {!isCreating && <AlertMultiPartyInvolvePartyDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={user?.multiPartyInvolvePartyId!} />}
        </>
    );
};

export default AddEditUser;
