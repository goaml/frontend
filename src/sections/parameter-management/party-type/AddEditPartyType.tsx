import { useState } from 'react';

// material-ui
import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  InputLabel,
  Stack,
  TextField,
  Tooltip
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
import AlertPartyTypeDelete from './DeletePartyType';
import { partyProps } from 'pages/parameter-management/party-type/types/types';
import { dispatch } from 'store';
import { addPartyType, updatePartyType } from 'store/reducers/party-type';


// types

// constant
const getInitialValues = (partyType: FormikValues | null) => {

  const newPartyType = {
   partyTypeId: undefined,
   partyTypeCode: '',
    description: '',
    statusId: 1,
    //limitAmt: 0
  }

  if (partyType) {
    return _.merge({}, newPartyType,partyType);
  }

  return newPartyType;
};

// ==============================||PartyType ADD / EDIT ||============================== //

export interface Props {
  partyType?: partyProps
  onCancel: () => void;
}

const AddEditPartyType = ({partyType, onCancel }: Props) => {

  const isCreating = !partyType;

  const partyTypeSchema = Yup.object().shape({
    partyTypeCode: Yup.string().required('Party Type Code is required'),
    description: Yup.string().required('Party Type Description is required'),
  });

  const formik = useFormik({
    initialValues: getInitialValues(partyType!),
    validationSchema:partyTypeSchema,
    enableReinitialize: true,
    onSubmit: (values, { setSubmitting, resetForm }) => {
      try {
        if (partyType) {
          // put
          dispatch(updatePartyType(values))

        } else {
          // post
          dispatch(addPartyType(values))
      
        }
        resetForm()
        setSubmitting(false);
        onCancel();
      } catch (error) {
        console.error(error);
      }
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  const [openAlert, setOpenAlert] = useState(false);

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
    onCancel();
  };
  return (
    <>
      <FormikProvider value={formik}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
            <DialogTitle>{partyType ? 'Edit Party Type' : ' Add Party Type'}</DialogTitle>
            <Divider />
            <DialogContent sx={{ p: 2.5 }}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={12}>
                  <Grid container spacing={3}>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="partyTypeCode">Party Type Code <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="partyTypeCode"
                          placeholder="Enter Party Type Code "
                          {...getFieldProps('partyTypeCode')}
                          error={Boolean(touched.partyTypeCode && errors.partyTypeCode)}
                          helperText={touched.partyTypeCode && errors.partyTypeCode}
                        />
                      </Stack>
                    </Grid>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="description">Party Type Description <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="description"
                          placeholder="Enter Party Type Description"
                          {...getFieldProps('description')}
                          error={Boolean(touched.description && errors.description)}
                          helperText={touched.description && errors.description}
                        />
                      </Stack>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <Divider />
            <DialogActions sx={{ p: 2.5 }}>
              <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                  {!isCreating && (
                    <Tooltip title="Delete Party Type" placement="top">
                      <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                        <DeleteFilled />
                      </IconButton>
                    </Tooltip>
                  )}
                </Grid>
                <Grid item>
                  <Stack direction="row" spacing={2} alignItems="center">
                    <Button color="error" onClick={onCancel}>
                      Cancel
                    </Button>
                    <Button type="submit" variant="contained" disabled={isSubmitting}>
                      {partyType ? 'Edit' : 'Add'}
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </DialogActions>
          </Form>
        </LocalizationProvider>
      </FormikProvider>
      {!isCreating && <AlertPartyTypeDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={partyType.partyTypeId!} />}
    </>
  );
};

export default AddEditPartyType;
