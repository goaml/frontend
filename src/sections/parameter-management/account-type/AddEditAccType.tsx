import { useState } from 'react';

// material-ui
import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  InputLabel,
  Stack,
  TextField,
  Tooltip
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
import AlertSubmissionCodeDelete from './DeleteAccType';
import { accountProps } from 'pages/parameter-management/account-type/types/types';
import { dispatch } from 'store';
import { addAccountType, updateAccountType } from 'store/reducers/account-type';


// types

// constant
const getInitialValues = (accountType: FormikValues | null) => {

  const newSubmissionCode = {
    accountTypeCodeId: undefined,
    accountTypeCode: '',
    description: '',
    statusId: 1,
    //limitAmt: 0
  }

  if (accountType) {
    return _.merge({}, newSubmissionCode, accountType);
  }

  return newSubmissionCode;
};

// ==============================|| accountType ADD / EDIT ||============================== //

export interface Props {
  accountType?: accountProps
  onCancel: () => void;
}

const AddEditaccountTypeCode = ({ accountType, onCancel }: Props) => {

  const isCreating = !accountType;

  const accountTypeCodeSchema = Yup.object().shape({
    accountTypeCode: Yup.string().required('Account Type Code is required'),
    description: Yup.string().required('Account Type Description is required'),
  });

  const formik = useFormik({
    initialValues: getInitialValues(accountType!),
    validationSchema: accountTypeCodeSchema,
    enableReinitialize: true,
    onSubmit: (values, { setSubmitting, resetForm }) => {
      try {
        if (accountType) {
          // put
          dispatch(updateAccountType(values))

        } else {
          // post
          dispatch(addAccountType(values))
      
        }
        resetForm()
        setSubmitting(false);
        onCancel();
      } catch (error) {
        console.error(error);
      }
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  const [openAlert, setOpenAlert] = useState(false);

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
    onCancel();
  };
  return (
    <>
      <FormikProvider value={formik}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
            <DialogTitle>{accountType ? 'Edit Account Type ' : ' Add Account Type '}</DialogTitle>
            <Divider />
            <DialogContent sx={{ p: 2.5 }}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={12}>
                  <Grid container spacing={3}>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="accountTypeCode">Account Type <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="accountTypeCode"
                          placeholder="Enter Account Type "
                          {...getFieldProps('accountTypeCode')}
                          error={Boolean(touched.accountTypeCode && errors.accountTypeCode)}
                          helperText={touched.accountTypeCode && errors.accountTypeCode}
                        />
                      </Stack>
                    </Grid>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="description">Account Type  Description <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="description"
                          placeholder="Enter Account Type Description"
                          {...getFieldProps('description')}
                          error={Boolean(touched.description && errors.description)}
                          helperText={touched.description && errors.description}
                        />
                      </Stack>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <Divider />
            <DialogActions sx={{ p: 2.5 }}>
              <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                  {!isCreating && (
                    <Tooltip title="Delete user" placement="top">
                      <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                        <DeleteFilled />
                      </IconButton>
                    </Tooltip>
                  )}
                </Grid>
                <Grid item>
                  <Stack direction="row" spacing={2} alignItems="center">
                    <Button color="error" onClick={onCancel}>
                      Cancel
                    </Button>
                    <Button type="submit" variant="contained" disabled={isSubmitting}>
                      {accountType ? 'Edit' : 'Add'}
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </DialogActions>
          </Form>
        </LocalizationProvider>
      </FormikProvider>
      {!isCreating && <AlertSubmissionCodeDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={accountType.accountTypeId!} />}
    </>
  );
};

export default AddEditaccountTypeCode;
