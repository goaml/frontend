import { useState } from 'react';

// material-ui
import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  InputLabel,
  Stack,
  TextField,
  Tooltip
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
import AlertCityCodeDelete from './DeleteCityType';
import { statusProps } from 'pages/parameter-management/status-code/types/types';
import { dispatch } from 'store';
import { addCity, updateCity } from 'store/reducers/city-code';


// types

// constant
const getInitialValues = (cityCode: FormikValues | null) => {

  const newStatusCode = {
   cityId: undefined,
   cityCode: '',
    description: '',
    statusId: 1,
    //limitAmt: 0
  }

  if (cityCode) {
    return _.merge({}, newStatusCode,cityCode);
  }

  return newStatusCode;
};

// ==============================||currencyType ADD / EDIT ||============================== //

export interface Props {
  cityCode?: statusProps
  onCancel: () => void;
}

const AddEditcityCode = ({cityCode, onCancel }: Props) => {

  const isCreating = !cityCode;

  const cityCodeSchema = Yup.object().shape({
   cityCode: Yup.string().required('City Code is required'),
   description: Yup.string().required('City Description is required'),
  });

  const formik = useFormik({
    initialValues: getInitialValues(cityCode!),
    validationSchema:cityCodeSchema,
    enableReinitialize: true,
    onSubmit: (values, { setSubmitting, resetForm }) => {
      try {
        if (cityCode) {
          // put
          dispatch(updateCity(values))

        } else {
          // post
          dispatch(addCity(values))
      
        }
        resetForm()
        setSubmitting(false);
        onCancel();
      } catch (error) {
        console.error(error);
      }
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  const [openAlert, setOpenAlert] = useState(false);

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
    onCancel();
  };
  return (
    <>
      <FormikProvider value={formik}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
            <DialogTitle>{cityCode ? 'Edit City Code' : ' Add City Code'}</DialogTitle>
            <Divider />
            <DialogContent sx={{ p: 2.5 }}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={12}>
                  <Grid container spacing={3}>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="cityCode">City Code <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="cityCode"
                          placeholder="Enter City Code "
                          {...getFieldProps('cityCode')}
                          error={Boolean(touched.cityCode && errors.cityCode)}
                          helperText={touched.cityCode && errors.cityCode}
                        />
                      </Stack>
                    </Grid>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="description">City Description <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="description"
                          placeholder="Enter City Code Description"
                          {...getFieldProps('description')}
                          error={Boolean(touched.description && errors.description)}
                          helperText={touched.description && errors.description}
                        />
                      </Stack>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <Divider />
            <DialogActions sx={{ p: 2.5 }}>
              <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                  {!isCreating && (
                    <Tooltip title="Delete City" placement="top">
                      <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                        <DeleteFilled />
                      </IconButton>
                    </Tooltip>
                  )}
                </Grid>
                <Grid item>
                  <Stack direction="row" spacing={2} alignItems="center">
                    <Button color="error" onClick={onCancel}>
                      Cancel
                    </Button>
                    <Button type="submit" variant="contained" disabled={isSubmitting}>
                      {cityCode ? 'Edit' : 'Add'}
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </DialogActions>
          </Form>
        </LocalizationProvider>
      </FormikProvider>
      {!isCreating && <AlertCityCodeDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={cityCode.statusId!} />}
    </>
  );
};

export default AddEditcityCode;
