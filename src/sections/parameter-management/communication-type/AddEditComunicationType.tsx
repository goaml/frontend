import { useState } from 'react';

// material-ui
import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  InputLabel,
  Stack,
  TextField,
  Tooltip
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
import AlertCommunicationTypeDelete from './DeleteComunicationType';
import { communicationProps } from 'pages/parameter-management/communication-type/types/types';
import { dispatch } from 'store';
import { addCommunicationType, updateCommunicationType } from 'store/reducers/communication-type';


// types

// constant
const getInitialValues = (communicationType: FormikValues | null) => {

  const newSubmissionCode = {
   communicationTypeId: undefined,
   communicationTypeCode: '',
    description: '',
    statusId: 1,
    //limitAmt: 0
  }

  if (communicationType) {
    return _.merge({}, newSubmissionCode,communicationType);
  }

  return newSubmissionCode;
};

// ==============================||communicationType ADD / EDIT ||============================== //

export interface Props {
  communicationType?: communicationProps
  onCancel: () => void;
}

const AddEditCommunicationType = ({communicationType, onCancel }: Props) => {

  const isCreating = !communicationType;

  const communicationTypeSchema = Yup.object().shape({
    communicationTypeCode: Yup.string().required('Communication Type Code  is required'),
    description: Yup.string().required('Communication Type Description is required'),
  });

  const formik = useFormik({
    initialValues: getInitialValues(communicationType!),
    validationSchema:communicationTypeSchema,
    enableReinitialize: true,
    onSubmit: (values, { setSubmitting, resetForm }) => {
      try {
        if (communicationType) {
          // put
          dispatch(updateCommunicationType(values))

        } else {
          // post
          dispatch(addCommunicationType(values))
      
        }
        resetForm()
        setSubmitting(false);
        onCancel();
      } catch (error) {
        console.error(error);
      }
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  const [openAlert, setOpenAlert] = useState(false);

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
    onCancel();
  };
  return (
    <>
      <FormikProvider value={formik}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
            <DialogTitle>{communicationType ? 'Edit Communication Type' : ' Add Communication Type'}</DialogTitle>
            <Divider />
            <DialogContent sx={{ p: 2.5 }}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={12}>
                  <Grid container spacing={3}>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="communicationTypeCode">Communication Type Code <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="communicationTypeCode"
                          placeholder="Enter Communication Type Code "
                          {...getFieldProps('communicationTypeCode')}
                          error={Boolean(touched.communicationTypeCode && errors.communicationTypeCode)}
                          helperText={touched.communicationTypeCode && errors.communicationTypeCode}
                        />
                      </Stack>
                    </Grid>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="description">Communication Type Description <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="description"
                          placeholder="Enter Communication Type Description"
                          {...getFieldProps('description')}
                          error={Boolean(touched.description && errors.description)}
                          helperText={touched.description && errors.description}
                        />
                      </Stack>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <Divider />
            <DialogActions sx={{ p: 2.5 }}>
              <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                  {!isCreating && (
                    <Tooltip title="Delete user" placement="top">
                      <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                        <DeleteFilled />
                      </IconButton>
                    </Tooltip>
                  )}
                </Grid>
                <Grid item>
                  <Stack direction="row" spacing={2} alignItems="center">
                    <Button color="error" onClick={onCancel}>
                      Cancel
                    </Button>
                    <Button type="submit" variant="contained" disabled={isSubmitting}>
                      {communicationType ? 'Edit' : 'Add'}
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </DialogActions>
          </Form>
        </LocalizationProvider>
      </FormikProvider>
      {!isCreating && <AlertCommunicationTypeDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={communicationType.communicationTypeId!} />}
    </>
  );
};

export default AddEditCommunicationType;
