import { useState } from 'react';

// material-ui
import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  InputLabel,
  Stack,
  TextField,
  Tooltip
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-ReportIndicator
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
import AlertReportIndicatorTypeDelete from './DeleteReportIndicatorType';
import { reportIndicatorProps } from 'pages/parameter-management/report-indicator/types/types';
import { addReportIndicatorType, updateReportIndicatorType } from 'store/reducers/report-indicator-type';
import { dispatch } from 'store';


// types

// constant
const getInitialValues = (reportIndicatorType: FormikValues | null) => {

  const newReportIndicatorType = {
   reportIndicatorTypeId: undefined,
   reportIndicatorTypeCode: '',
    description: '',
    statusId: 1,
    //limitAmt: 0
  }

  if (reportIndicatorType) {
    return _.merge({}, newReportIndicatorType,reportIndicatorType);
  }

  return newReportIndicatorType;
};

// ==============================||PartyType ADD / EDIT ||============================== //

export interface Props {
  reportIndicatorType?: reportIndicatorProps
  onCancel: () => void;
}

const AddEditReportIndicatorType = ({reportIndicatorType, onCancel }: Props) => {

  const isCreating = !reportIndicatorType;

  const ReportIndicatorTypeSchema = Yup.object().shape({
    reportIndicatorTypeCode: Yup.string().required('Report Indicator Type Code is required'),
    description: Yup.string().required('Report Indicator Type Description is required'),
  });

  const formik = useFormik({
    initialValues: getInitialValues(reportIndicatorType!),
    validationSchema:ReportIndicatorTypeSchema,
    enableReinitialize: true,
    onSubmit: (values, { setSubmitting, resetForm }) => {
      try {
        if (reportIndicatorType) {
          // put
          dispatch(updateReportIndicatorType(values))

        } else {
          // post
          dispatch(addReportIndicatorType(values))
      
        }
        resetForm()
        setSubmitting(false);
        onCancel();
      } catch (error) {
        console.error(error);
      }
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  const [openAlert, setOpenAlert] = useState(false);

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
    onCancel();
  };
  return (
    <>
      <FormikProvider value={formik}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
            <DialogTitle>{reportIndicatorType ? 'Edit Report Indicator Type' : ' Add Report Indicator Type'}</DialogTitle>
            <Divider />
            <DialogContent sx={{ p: 2.5 }}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={12}>
                  <Grid container spacing={3}>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="reportIndicatorTypeCode">Report Indicator Type Code <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="reportIndicatorTypeCode"
                          placeholder="Enter Report Indicator Type Code "
                          {...getFieldProps('reportIndicatorTypeCode')}
                          error={Boolean(touched.reportIndicatorTypeCode && errors.reportIndicatorTypeCode)}
                          helperText={touched.reportIndicatorTypeCode && errors.reportIndicatorTypeCode}
                        />
                      </Stack>
                    </Grid>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="description">Report Indicator Type Description <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="description"
                          placeholder="Enter Report Indicator Type Description"
                          {...getFieldProps('description')}
                          error={Boolean(touched.description && errors.description)}
                          helperText={touched.description && errors.description}
                        />
                      </Stack>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <Divider />
            <DialogActions sx={{ p: 2.5 }}>
              <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                  {!isCreating && (
                    <Tooltip title="Delete user" placement="top">
                      <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                        <DeleteFilled />
                      </IconButton>
                    </Tooltip>
                  )}
                </Grid>
                <Grid item>
                  <Stack direction="row" spacing={2} alignItems="center">
                    <Button color="error" onClick={onCancel}>
                      Cancel
                    </Button>
                    <Button type="submit" variant="contained" disabled={isSubmitting}>
                      {reportIndicatorType ? 'Edit' : 'Add'}
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </DialogActions>
          </Form>
        </LocalizationProvider>
      </FormikProvider>
      {!isCreating && <AlertReportIndicatorTypeDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={reportIndicatorType.reportIndicatorTypeId!} />}
    </>
  );
};

export default AddEditReportIndicatorType;
