import { useState } from 'react';

// material-ui
import {
  Autocomplete,
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  InputLabel,
  Stack,
  TextField,
  Tooltip,
  useTheme
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
import { SubBusinessProps } from 'pages/parameter-management/sub-business-nature/list/types/types';
import AlertBusinessNatureDelete from './DeleteSubBusinessNature';
import { optionType, options } from 'data/options';


// types

// constant
const getInitialValues = (businessNature: FormikValues | null) => {

  const newBusinessNature = {
    SubBusinessId: 0,
    BusinessNatureCode: undefined,
    SubBusinessNatureCode: undefined,
    desc: undefined
  }

  if (businessNature) {
    return _.merge({}, newBusinessNature, businessNature);
  }

  return newBusinessNature;
};

// ==============================|| Business-Nature ADD / EDIT ||============================== //

export interface Props {
  businessNature?: SubBusinessProps
  onCancel: () => void;
}

const AddEditSubBusinessNature = ({ businessNature, onCancel }: Props) => {
  const theme = useTheme();
  const isCreating = !businessNature;

  const BusinessNatureSchema = Yup.object().shape({
    BusinessNatureCode: Yup.string().required('Business Sub Nature Name is required'),
    desc: Yup.string().required('Business Sub Nature Location is required'),
  });

  const formik = useFormik({
    initialValues: getInitialValues(businessNature!),
    validationSchema: BusinessNatureSchema,
    enableReinitialize: true,
    onSubmit: (values, { setSubmitting, resetForm }) => {
      try {
        if (businessNature) {
          // put
          
        } else {
          // post
          
        }
        resetForm()
        setSubmitting(false);
        onCancel();
      } catch (error) {
        console.error(error);
      }
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  const [openAlert, setOpenAlert] = useState(false);

  const handleAlertClose = () => {
    setOpenAlert(!openAlert);
    onCancel();
  };
  return (
    <>
      <FormikProvider value={formik}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
            <DialogTitle>{businessNature ? 'Edit Sub Business Nature Code' : ' Add New Sub Business Nature Code'}</DialogTitle>
            <Divider />
            <DialogContent sx={{ p: 2.5 }}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={12}>
                  <Grid container spacing={3}>
                  <Grid item xs={6}>
                  <Stack spacing={1.25}>
                      <InputLabel htmlFor="BusinessNatureCode">Buiness Nature Code <span style={{ color: 'red' }}>*</span></InputLabel>
                      <Autocomplete
                          fullWidth
                          id="BusinessNatureCode"
                          value={options?.find((option) => option.key === formik.values.BusinessNatureCode) || null}
                          onChange={(event: any, newValue: optionType | null) => {
                              formik.setFieldValue('BusinessNatureCode', newValue?.key);
                          }}
                          options={options}
                          getOptionLabel={(item) => `${item.value}`}
                          renderInput={(params) => {
                              return (
                                  <TextField
                                      {...params}
                                      placeholder="Select a Business Nature Code"
                                      sx={{ '& .MuiAutocomplete-input.Mui-disabled': { WebkitTextFillColor: theme.palette.text.primary } }}
                                      error={Boolean(touched.BusinessNatureCode && errors.BusinessNatureCode)}
                                      helperText={touched.BusinessNatureCode && errors.BusinessNatureCode}
                                  />
                              )
                          }}
                      />
                  </Stack>
                   </Grid>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="SubBusinessNatureCode">Business Sub Nature Code <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="SubBusinessNatureCode"
                          placeholder="Enter Business Sub Nature Code"
                          {...getFieldProps('SubBusinessNatureCode')}
                          error={Boolean(touched.SubBusinessNatureCode && errors.SubBusinessNatureCode)}
                          helperText={touched.SubBusinessNatureCode && errors.SubBusinessNatureCode}
                        />
                      </Stack>
                    </Grid>
                    <Grid item xs={6}>
                      <Stack spacing={1.25}>
                        <InputLabel htmlFor="desc">Business Nature Description <span style={{ color: 'red' }}>*</span></InputLabel>
                        <TextField
                          fullWidth
                          id="desc"
                          placeholder="Enter Business Sub Nature Description"
                          {...getFieldProps('desc')}
                          error={Boolean(touched.desc && errors.desc)}
                          helperText={touched.desc && errors.desc}
                        />
                      </Stack>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>
            <Divider />
            <DialogActions sx={{ p: 2.5 }}>
              <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                  {!isCreating && (
                    <Tooltip title="Delete user" placement="top">
                      <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                        <DeleteFilled />
                      </IconButton>
                    </Tooltip>
                  )}
                </Grid>
                <Grid item>
                  <Stack direction="row" spacing={2} alignItems="center">
                    <Button color="error" onClick={onCancel}>
                      Cancel
                    </Button>
                    <Button type="submit" variant="contained" disabled={isSubmitting}>
                      {businessNature ? 'Edit' : 'Add'}
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </DialogActions>
          </Form>
        </LocalizationProvider>
      </FormikProvider>
      {!isCreating && <AlertBusinessNatureDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={businessNature.SubBusinessId!} />}
    </>
  );
};

export default AddEditSubBusinessNature;
