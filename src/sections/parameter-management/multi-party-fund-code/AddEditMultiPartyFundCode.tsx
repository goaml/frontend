import { useState } from 'react';

// material-ui
import {
    // Autocomplete,
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    // FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField,
    Tooltip
} from '@mui/material';
// import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';

// assets
import { DeleteFilled } from '@ant-design/icons';
// import { optionType, options } from 'data/options';
import { userProps } from 'pages/parameter-management/multi-party-fund-code/list/types/types';
import AlertUserDelete from './AlertMultiPartyFundCodeDelete';
import { addMultiPartyFundCodeParty, updateMultiPartyFundCodeParty } from 'store/reducers/multyParty-fundCode';
import { dispatch } from 'store';

// types

// constant
const getInitialValues = (user: FormikValues | null) => {

    const newUser = {
        multiPartyFundsCodeName: '',
        multiPartyFundsCodeDescription: '',
        
    }

    if (user) {
        return _.merge({}, newUser, user);
    }

    return newUser;
};

// ==============================|| CUSTOMER ADD / EDIT ||============================== //

export interface Props {
    user?: userProps
    onCancel: () => void;
}

const AddEditUser = ({ user, onCancel }: Props) => {
    // const theme = useTheme();

    const isCreating = !user;

    const UserSchema = Yup.object().shape({
        multiPartyFundsCodeName: Yup.string().required('Multi Party Name is required'),
        multiPartyFundsCodeDescription: Yup.string().required('Multi Party Description is required'),

    });

    const formik = useFormik({
        initialValues: getInitialValues(user!),
        validationSchema: UserSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (user) {
                    // put
                    dispatch(updateMultiPartyFundCodeParty(values))
                } else {
                    // post
                    dispatch(addMultiPartyFundCodeParty(values))
                }
                resetForm()
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

    const [openAlert, setOpenAlert] = useState(false);

    const handleAlertClose = () => {
        setOpenAlert(!openAlert);
        onCancel();
    };



    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{user ? 'Edit Multi Party FundsCode' : 'New Multi Party FundsCode'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                    <Grid container spacing={3}>
                                        
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="multiPartyFundsCodeName">Multi Party FundsCode Name <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="multiPartyFundsCodeName"
                                                    placeholder="Enter Multi Party FundsCode Name"
                                                    {...getFieldProps('multiPartyFundsCodeName')}
                                                    error={Boolean(touched.multiPartyFundsCodeName && errors.multiPartyFundsCodeName)}
                                                    helperText={touched.multiPartyFundsCodeName && errors.multiPartyFundsCodeName}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="multiPartyFundsCodeDescription">Multi Party FundsCode Description <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="multiPartyFundsCodeDescription"
                                                    placeholder="Enter Multi Party FundsCode Description"
                                                    {...getFieldProps('multiPartyFundsCodeDescription')}
                                                    error={Boolean(touched.multiPartyFundsCodeDescription && errors.multiPartyFundsCodeDescription)}
                                                    helperText={touched.multiPartyFundsCodeDescription && errors.multiPartyFundsCodeDescription}
                                                />
                                            </Stack>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 2.5 }}>
                            <Grid container justifyContent="space-between" alignItems="center">
                                <Grid item>
                                    {!isCreating && (
                                        <Tooltip title="Delete user" placement="top">
                                            <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                                                <DeleteFilled />
                                            </IconButton>
                                        </Tooltip>
                                    )}
                                </Grid>
                                <Grid item>
                                    <Stack direction="row" spacing={2} alignItems="center">
                                        <Button color="error" onClick={onCancel}>
                                            Cancel
                                        </Button>
                                        <Button type="submit" variant="contained" disabled={isSubmitting}>
                                            {user ? 'Edit' : 'Add'}
                                        </Button>
                                    </Stack>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </Form>
                </LocalizationProvider>
            </FormikProvider>
            {!isCreating && <AlertUserDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={user?.multiPartyFundsCodeId!} />}
        </>
    );
};

export default AddEditUser;
