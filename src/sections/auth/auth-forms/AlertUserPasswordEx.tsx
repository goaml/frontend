// material-ui
import { LockOutlined } from '@ant-design/icons';
import { Button, Dialog, DialogContent, Stack, Typography } from '@mui/material';

// project import
import Avatar from 'components/@extended/Avatar';
import { PopupTransition } from 'components/@extended/Transitions';

// types
interface Props {
  title: string;
  open: boolean;
  handleClose: (status: boolean) => void;
  passwordResetDateCount?: number
}

export default function AlertUserPasswordEx({ title, open, handleClose, passwordResetDateCount }: Props) {

  return (
    <Dialog
      open={open}
      onClose={() => handleClose(false)}
      keepMounted
      TransitionComponent={PopupTransition}
      maxWidth="xs"
      aria-labelledby="column-delete-title"
      aria-describedby="column-delete-description"
    >
      <DialogContent sx={{ mt: 2, my: 1 }}>
        <Stack alignItems="center" spacing={3.5}>
          <Avatar color="error" sx={{ width: 72, height: 72, fontSize: '1.75rem' }}>
            <LockOutlined />
          </Avatar>
          <Stack spacing={2}>
            <Typography variant="h4" align="center">
              Your password will expire in {passwordResetDateCount} days. Would you like to change it now?
            </Typography>
          </Stack>

          <Stack direction="row" spacing={2} sx={{ width: 1 }}>
            <Button fullWidth onClick={() => { handleClose(false) }} color="secondary" variant="outlined">
              No
            </Button>
            <Button fullWidth color="error" variant="contained" onClick={() => {
              window.location.replace('/resources/system-user-management/password-change')
              handleClose(true)
            }} autoFocus>
              Yes
            </Button>
          </Stack>
        </Stack>
      </DialogContent>
    </Dialog >
  );
}
