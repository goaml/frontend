// material-ui
import {
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    Grid,
    InputLabel,
    Stack,
    TextField,
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { FormikProvider, useFormik } from 'formik';
import _ from 'lodash';
import { dataProps } from 'pages/transaction/trans-from/types/types';
import * as Yup from 'yup';

// types
export interface FormikValues {
    fromId?:number;
    fromFundsComment?:string;
    fromForeignCurrencyId?:number;
    conductorPersonId?:number;
    fromAccountId?:number;
    fromPersonId?:number;
    fromEntityId?:number;
    transactionId?:number;
    isActive?:boolean;
    fundTypeCode?: string;
    rfundsType?:{
        fundTypeCode?: string;
    };
    rcountryCodes?:{
        description?:string;
    }
    rstatus?:{
        statusId?:number;
        isActive?:boolean;
        statusCode?:string;
    }

}

export interface Props {
    transFrom?: dataProps;
    onCancel: () => void;
}

const getInitialValues = (transFrom: FormikValues | null) => {
    const newtransFrom: FormikValues = {
        fromId:0,
        fromFundsComment:'',
        fromForeignCurrencyId:0,
        conductorPersonId:0,
        fromAccountId:0,
        fromPersonId:0,
        fromEntityId:0,
        transactionId:0,
        isActive:true,
        rfundsType:{
            fundTypeCode: '',
        },
        rcountryCodes:{
            description:'',
        },
        rstatus:{
            statusId:0,
            isActive:true,
            statusCode:'',
        }
    
    };

    if (transFrom) {
        return _.merge({}, newtransFrom, transFrom);
    }

    return newtransFrom;
};

const AddEdittransFrom = ({ transFrom, onCancel }: Props) => {
    const transFromSchema = Yup.object().shape({
        rfundsType: Yup.object({
            fundTypeCode: Yup.string().required('Fund Type Code is required'),
          }),
    });

    const formik = useFormik({
        initialValues: getInitialValues(transFrom || null),
        validationSchema: transFromSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (transFrom) {
                    // put request logic
                } else {
                    // post request logic
                }
                resetForm();
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, getFieldProps } = formik;

    const getHelperText = (fieldError: any): string | undefined => {
        return typeof fieldError === 'string' ? fieldError : undefined;
    };

    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{transFrom ? 'View Transaction Sender Details' : 'New transFrom'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                    <Grid container spacing={3}>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="fromId">From ID</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="fromId"
                                                    {...getFieldProps('fromId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.fromId && errors.fromId)}
                                                    helperText={getHelperText(touched.fromId && errors.fromId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                            <InputLabel htmlFor="rfundsType.fundTypeCode">
                                            From Funds Code
                                            </InputLabel>
                                            <TextField
                                            fullWidth
                                            id="rfundsType.fundTypeCode"
                                            {...getFieldProps('rfundsType.fundTypeCode')}
                                            InputProps={{
                                                readOnly: true,
                                            }}
                                            error={Boolean(touched.fundTypeCode && errors.fundTypeCode)}
                                            helperText={getHelperText(touched.fundTypeCode && errors.fundTypeCode)}
                                            />
                                        </Stack>
                                        </Grid>

                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="fromFundsComment">From Funds Comment </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="fromFundsComment"
                                                    {...getFieldProps('fromFundsComment')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.fromFundsComment && errors.fromFundsComment)}
                                                    helperText={getHelperText(touched.fromFundsComment && errors.fromFundsComment)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="fromForeignCurrencyId">From Foreign Currency </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="fromForeignCurrencyId"
                                                    {...getFieldProps('fromForeignCurrencyId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.fromForeignCurrencyId && errors.fromForeignCurrencyId)}
                                                    helperText={getHelperText(touched.fromForeignCurrencyId && errors.fromForeignCurrencyId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="conductorPersonId">T Conductor</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="conductorPersonId"
                                                    {...getFieldProps('conductorPersonId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.conductorPersonId && errors.conductorPersonId)}
                                                    helperText={getHelperText(touched.conductorPersonId && errors.conductorPersonId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="fromAccountId">From Account </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="fromAccountId"
                                                    {...getFieldProps('fromAccountId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.fromAccountId && errors.fromAccountId)}
                                                    helperText={getHelperText(touched.fromAccountId && errors.fromAccountId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="fromPersonId">From Person </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="fromPersonId"
                                                    {...getFieldProps('fromPersonId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.fromPersonId && errors.fromPersonId)}
                                                    helperText={getHelperText(touched.fromPersonId && errors.fromPersonId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="fromEntityId">From Entity </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="fromEntityId"
                                                    {...getFieldProps('fromEntityId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.fromEntityId && errors.fromEntityId)}
                                                    helperText={getHelperText(touched.fromEntityId && errors.fromEntityId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="rcountryCodes.description">From Country </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="rcountryCodes.description"
                                                    {...getFieldProps('rcountryCodes.description')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.rcountryCodes&& errors.rcountryCodes)}
                                                    helperText={getHelperText(touched.rcountryCodes&& errors.rcountryCodes)}
                                                />
                                            </Stack>
                                        </Grid>

                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="isActive">Is Active </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="isActive"
                                                    {...getFieldProps('isActive')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.isActive && errors.isActive)}
                                                    helperText={getHelperText(touched.isActive && errors.isActive)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="transactionId">Transaction ID </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="transactionId"
                                                    {...getFieldProps('transactionId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.transactionId && errors.transactionId)}
                                                    helperText={getHelperText(touched.transactionId && errors.transactionId)}
                                                />
                                            </Stack>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 1.25 }}>
                            <Button color="error" onClick={onCancel}>
                                Cancel
                            </Button>
                        </DialogActions>
                    </form>
                </LocalizationProvider>
            </FormikProvider>
        </>
    );
};

export default AddEdittransFrom;
