//import { useState } To 'react';

// material-ui
import {
    //Autocomplete,
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    // FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField,
    // Tooltip
} from '@mui/material';
// import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { FormikProvider, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// types
import { dataProps } from 'pages/transaction/trans-party/types/types';

export interface FormikValues {
    partyId?: number;
    partyTypeId?: number;
    personMyClientId?: number;
    accountMyClientId?: number;
    entityMyClientId?: number;
    fundTypeId?: string | null;
    fundsComment?: string | null;
    foreignCurrencyId?: number;
    country?: string | null;
    countryCodeId?: number;
    comments?: string | null;
    statusId?: number;
    isActive?: boolean;
    transactionId?: number;
}

export interface Props {
    transParty?: dataProps;
    onCancel: () => void;
}

const getInitialValues = (transParty: FormikValues | null) => {
    const newtransParty: FormikValues = {
        partyId: 0,
        partyTypeId: 0,
        personMyClientId: 0,
        accountMyClientId: 0,
        entityMyClientId: 0,
        fundTypeId: '',
        fundsComment: '',
        foreignCurrencyId: 0,
        country: '',
        countryCodeId: 0,
        comments: '',
        statusId: 0,
        isActive: true,
        transactionId: 0,
    };

    if (transParty) {
        return _.merge({}, newtransParty, transParty);
    }

    return newtransParty;
};

const AddEdittransParty = ({ transParty, onCancel }: Props) => {
    const transPartySchema = Yup.object().shape({
        toId: Yup.number().required('To ID is required'),
        toFundsCode: Yup.string().required('To Funds Code is required'),
        toFundsComment: Yup.string().required('To Funds Comment is required'),
        toForeignCurrency: Yup.string().required('To Foreign Currency is required'),
        tConductor: Yup.string().required('T Conductor is required'),
        toAccount: Yup.string().required('To Account is required'),
        toPerson: Yup.string().required('To Person is required'),
        toEntity: Yup.string().required('To Entity is required'),
        toCountry: Yup.string().required('To Country is required'),
        statusId: Yup.number().required('Status ID is required'),
        isActive: Yup.boolean().required('Is Active is required'),
        transactionId: Yup.number().required('Transaction ID is required'),
    });

    const formik = useFormik({
        initialValues: getInitialValues(transParty!),
        validationSchema: transPartySchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (transParty) {
                    // put request logic
                } else {
                    // post request logic
                }
                resetForm();
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, getFieldProps } = formik;

    const getHelperText = (fieldError: any): string | undefined => {
        return typeof fieldError === 'string' ? fieldError : undefined;
    };

    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{transParty ? 'View Transaction Party Details' : 'New transParty'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                    <Grid container spacing={3}>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="partyId">Party ID</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="partyId"
                                                    {...getFieldProps('partyId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.partyId && errors.partyId)}
                                                    helperText={getHelperText(touched.partyId && errors.partyId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="partyTypeId">Party Type ID (Role)</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="partyTypeId"
                                                    {...getFieldProps('partyTypeId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.partyTypeId && errors.partyTypeId)}
                                                    helperText={getHelperText(touched.partyTypeId && errors.partyTypeId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="personMyClientId">Person My Client ID (Person)</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="personMyClientId"
                                                    {...getFieldProps('personMyClientId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.personMyClientId && errors.personMyClientId)}
                                                    helperText={getHelperText(touched.personMyClientId && errors.personMyClientId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="accountMyClientId">Account My Client ID (Account)</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="accountMyClientId"
                                                    {...getFieldProps('accountMyClientId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.accountMyClientId && errors.accountMyClientId)}
                                                    helperText={getHelperText(touched.accountMyClientId && errors.accountMyClientId)}
                                                />
                                            </Stack>
                                        </Grid>

                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="entityMyClientId">Entity My Client ID (Entity)</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="entityMyClientId"
                                                    {...getFieldProps('entityMyClientId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.entityMyClientId && errors.entityMyClientId)}
                                                    helperText={getHelperText(touched.entityMyClientId && errors.entityMyClientId)}
                                                />
                                            </Stack>
                                        </Grid>

                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="fundTypeId">Fund Type ID (Funds Code)</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="fundTypeId"
                                                    {...getFieldProps('fundTypeId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.fundTypeId && errors.fundTypeId)}
                                                    helperText={getHelperText(touched.fundTypeId && errors.fundTypeId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="fundsComment">Funds Comment</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="fundsComment"
                                                    {...getFieldProps('fundsComment')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.fundsComment && errors.fundsComment)}
                                                    helperText={getHelperText(touched.fundsComment && errors.fundsComment)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="foreignCurrencyId">Foreign Currency ID (Foreign Currency)</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="foreignCurrencyId"
                                                    {...getFieldProps('foreignCurrencyId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.foreignCurrencyId && errors.foreignCurrencyId)}
                                                    helperText={getHelperText(touched.foreignCurrencyId && errors.foreignCurrencyId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="country">Country</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="country"
                                                    {...getFieldProps('country')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.country && errors.country)}
                                                    helperText={getHelperText(touched.country && errors.country)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="countryCodeId">Country Code ID (Significance)</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="countryCodeId"
                                                    {...getFieldProps('countryCodeId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.countryCodeId && errors.countryCodeId)}
                                                    helperText={getHelperText(touched.countryCodeId && errors.countryCodeId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="comments">Comments</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="comments"
                                                    {...getFieldProps('comments')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.comments && errors.comments)}
                                                    helperText={getHelperText(touched.comments && errors.comments)}
                                                />
                                            </Stack>
                                        </Grid>

                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="isActive">Is Active</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="isActive"
                                                    {...getFieldProps('isActive')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.isActive && errors.isActive)}
                                                    helperText={getHelperText(touched.isActive && errors.isActive)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="transactionId">Transaction ID</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="transactionId"
                                                    {...getFieldProps('transactionId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.transactionId && errors.transactionId)}
                                                    helperText={getHelperText(touched.transactionId && errors.transactionId)}
                                                />
                                            </Stack>
                                        </Grid>
                                    </Grid>

                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 1.25 }}>
                            <Button color="error" onClick={onCancel}>
                                Cancel
                            </Button>
                        </DialogActions>
                    </form>
                </LocalizationProvider>
            </FormikProvider>
        </>
    );
};

export default AddEdittransParty;
