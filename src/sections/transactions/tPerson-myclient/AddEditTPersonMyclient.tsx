// material-ui
import {
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    Grid,
    InputLabel,
    Stack,
    TextField,
} from '@mui/material';


import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { FormikProvider, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// types
import { dataProps } from 'pages/transaction/tPerson-myclient/types/types';

export interface FormikValues {
    personMyClientId?: number;
    genderTypeId?: number;
    title?: string;
    firstName?: string;
    lastName?: string;
    birthdate?: Date;
    birthPlace?: string;
    mothersName?: string;
    alias?: string;
    ssn?: string;
    passportNumber?: string;
    passportCountryCodeId?: number;
    idNumber?: string;
    nationalityCountryId?: number;
    nationalityCountry2Id?: number;
    nationalityCountry3Id?: number;
    residenceCountryCodeId?: number;
    phones?: string;
    addresses?: string;
    email?: string;
    occupation?: string;
    comments?: string;
    statusId?: number;
    isActive?: boolean;
}

export interface Props {
    TPersonMyclientAddEditTPersonMyclient?: dataProps;
    onCancel: () => void;
}

const getInitialValues = (TPersonMyclientAddEditTPersonMyclient: FormikValues | null) => {
    const newTPersonMyclientAddEditTPersonMyclient: FormikValues = {
        personMyClientId: 0,
        genderTypeId: 0,
        title: '',
        firstName: '',
        lastName: '',
        birthdate: new Date(),
        birthPlace: '',
        mothersName: '',
        alias: '',
        ssn: '',
        passportNumber: '',
        passportCountryCodeId: 0,
        idNumber: '',
        nationalityCountryId: 0,
        nationalityCountry2Id: 0,
        nationalityCountry3Id: 0,
        residenceCountryCodeId: 0,
        phones: '',
        addresses: '',
        email: '',
        occupation: '',
        comments: '',
        statusId: 0,
        isActive: true,
    };

    if (TPersonMyclientAddEditTPersonMyclient) {
        return _.merge({}, newTPersonMyclientAddEditTPersonMyclient, TPersonMyclientAddEditTPersonMyclient);
    }

    return newTPersonMyclientAddEditTPersonMyclient;
};

const AddEditTPersonMyclient = ({ TPersonMyclientAddEditTPersonMyclient, onCancel }: Props) => {
    const TPersonMyclientAddEditTPersonMyclientSchema = Yup.object().shape({
        toId: Yup.number().required('To ID is required'),
        toFundsCode: Yup.string().required('To Funds Code is required'),
        toFundsComment: Yup.string().required('To Funds Comment is required'),
        toForeignCurrency: Yup.string().required('To Foreign Currency is required'),
        tConductor: Yup.string().required('T Conductor is required'),
        toAccount: Yup.string().required('To Account is required'),
        toPerson: Yup.string().required('To Person is required'),
        toEntity: Yup.string().required('To Entity is required'),
        toCountry: Yup.string().required('To Country is required'),
        statusId: Yup.number().required('Status ID is required'),
        isActive: Yup.boolean().required('Is Active is required'),
        transactionId: Yup.number().required('Transaction ID is required'),
    });

    const formik = useFormik({
        initialValues: getInitialValues(TPersonMyclientAddEditTPersonMyclient!),
        validationSchema: TPersonMyclientAddEditTPersonMyclientSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (TPersonMyclientAddEditTPersonMyclient) {
                    // put request logic
                } else {
                    // post request logic
                }
                resetForm();
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, getFieldProps } = formik;

    const getHelperText = (fieldError: any): string | undefined => {
        return typeof fieldError === 'string' ? fieldError : undefined;
    };

    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{TPersonMyclientAddEditTPersonMyclient ? 'View Transaction Reciever Details' : 'New TPersonMyclientAddEditTPersonMyclient'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                    <Grid container spacing={3}>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="personMyClientId">Person My Client ID</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="personMyClientId"
                                                    {...getFieldProps('personMyClientId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.personMyClientId && errors.personMyClientId)}
                                                    helperText={getHelperText(touched.personMyClientId && errors.personMyClientId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="genderTypeId">Gender Type ID</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="genderTypeId"
                                                    {...getFieldProps('genderTypeId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.genderTypeId && errors.genderTypeId)}
                                                    helperText={getHelperText(touched.genderTypeId && errors.genderTypeId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="title">Title</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="title"
                                                    {...getFieldProps('title')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.title && errors.title)}
                                                    helperText={getHelperText(touched.title && errors.title)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="firstName">First Name</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="firstName"
                                                    {...getFieldProps('firstName')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.firstName && errors.firstName)}
                                                    helperText={getHelperText(touched.firstName && errors.firstName)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="lastName">Last Name</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="lastName"
                                                    {...getFieldProps('lastName')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.lastName && errors.lastName)}
                                                    helperText={getHelperText(touched.lastName && errors.lastName)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="birthdate">DOB</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="birthdate"
                                                    {...getFieldProps('birthdate')}
                                                    value={formik.values.birthdate
                                                        ? new Date(formik.values.birthdate).toISOString().split('T')[0]
                                                        : ''}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.birthdate && errors.birthdate)}
                                                    helperText={getHelperText(touched.birthdate && errors.birthdate)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="birthPlace">Birth Place</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="birthPlace"
                                                    {...getFieldProps('birthPlace')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.birthPlace && errors.birthPlace)}
                                                    helperText={getHelperText(touched.birthPlace && errors.birthPlace)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="mothersName">Mother's Name</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="mothersName"
                                                    {...getFieldProps('mothersName')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.mothersName && errors.mothersName)}
                                                    helperText={getHelperText(touched.mothersName && errors.mothersName)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="alias">Alias</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="alias"
                                                    {...getFieldProps('alias')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.alias && errors.alias)}
                                                    helperText={getHelperText(touched.alias && errors.alias)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="ssn">SSN</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="ssn"
                                                    {...getFieldProps('ssn')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.ssn && errors.ssn)}
                                                    helperText={getHelperText(touched.ssn && errors.ssn)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="passportNumber">Passport Number</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="passportNumber"
                                                    {...getFieldProps('passportNumber')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.passportNumber && errors.passportNumber)}
                                                    helperText={getHelperText(touched.passportNumber && errors.passportNumber)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="passportCountryCodeId">Passport Country Code ID</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="passportCountryCodeId"
                                                    {...getFieldProps('passportCountryCodeId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.passportCountryCodeId && errors.passportCountryCodeId)}
                                                    helperText={getHelperText(touched.passportCountryCodeId && errors.passportCountryCodeId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="idNumber">NIC</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="idNumber"
                                                    {...getFieldProps('idNumber')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.idNumber && errors.idNumber)}
                                                    helperText={getHelperText(touched.idNumber && errors.idNumber)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="nationalityCountryId">Nationality 1 Country Code ID</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="nationalityCountryId"
                                                    {...getFieldProps('nationalityCountryId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.nationalityCountryId && errors.nationalityCountryId)}
                                                    helperText={getHelperText(touched.nationalityCountryId && errors.nationalityCountryId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="nationalityCountry2Id">Nationality 2 Country Code ID</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="nationalityCountry2Id"
                                                    {...getFieldProps('nationalityCountry2Id')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.nationalityCountry2Id && errors.nationalityCountry2Id)}
                                                    helperText={getHelperText(touched.nationalityCountry2Id && errors.nationalityCountry2Id)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="nationalityCountry3Id">Nationality 3 Country Code ID</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="nationalityCountry3Id"
                                                    {...getFieldProps('nationalityCountry3Id')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.nationalityCountry3Id && errors.nationalityCountry3Id)}
                                                    helperText={getHelperText(touched.nationalityCountry3Id && errors.nationalityCountry3Id)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="residenceCountryCodeId">Residence Country Code ID</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="residenceCountryCodeId"
                                                    {...getFieldProps('residenceCountryCodeId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.residenceCountryCodeId && errors.residenceCountryCodeId)}
                                                    helperText={getHelperText(touched.residenceCountryCodeId && errors.residenceCountryCodeId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="phones">Mobile Number</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="phones"
                                                    {...getFieldProps('phones')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.phones && errors.phones)}
                                                    helperText={getHelperText(touched.phones && errors.phones)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="addresses">Addresses</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="addresses"
                                                    {...getFieldProps('addresses')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.addresses && errors.addresses)}
                                                    helperText={getHelperText(touched.addresses && errors.addresses)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="email">Email</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="email"
                                                    {...getFieldProps('email')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.email && errors.email)}
                                                    helperText={getHelperText(touched.email && errors.email)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="occupation">Occupation</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="occupation"
                                                    {...getFieldProps('occupation')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.occupation && errors.occupation)}
                                                    helperText={getHelperText(touched.occupation && errors.occupation)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="comments">Comments</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="comments"
                                                    {...getFieldProps('comments')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.comments && errors.comments)}
                                                    helperText={getHelperText(touched.comments && errors.comments)}
                                                />
                                            </Stack>
                                        </Grid>

                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="isActive">Is Active</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="isActive"
                                                    {...getFieldProps('isActive')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.isActive && errors.isActive)}
                                                    helperText={getHelperText(touched.isActive && errors.isActive)}
                                                />
                                            </Stack>
                                        </Grid>
                                    </Grid>

                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 1.25 }}>
                            <Button color="error" onClick={onCancel}>
                                Cancel
                            </Button>
                        </DialogActions>
                    </form>
                </LocalizationProvider>
            </FormikProvider>
        </>
    );
};

export default AddEditTPersonMyclient;
