//import { useState } To 'react';

// material-ui
import {
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    Grid,
    InputLabel,
    Stack,
    TextField,
} from '@mui/material';

import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { FormikProvider, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// types
import { dataProps } from 'pages/transaction/trans-phone/types/types';

export interface FormikValues {
    phoneId?: number;
    contactTypeId?: number;
    communicationTypeId?: number;
    tphCountryPrefix?: string;
    tphNumber?: string;
    tphExtension?: string;
    comments?: string;
    status_id?: number;
    is_active?: boolean;
}

export interface Props {
    transPhone?: dataProps;
    onCancel: () => void;
}

const getInitialValues = (transPhone: FormikValues | null) => {
    const newtransPhone: FormikValues = {
        phoneId: 0,
        contactTypeId: 0,
        communicationTypeId: 0,
        tphCountryPrefix: '',
        tphNumber: '',
        tphExtension: '',
        comments: '',
        status_id: 0,
        is_active: true,
    };

    if (transPhone) {
        return _.merge({}, newtransPhone, transPhone);
    }

    return newtransPhone;
};

const AddEdittransPhone = ({ transPhone, onCancel }: Props) => {
    const transPhoneSchema = Yup.object().shape({
        phoneId: Yup.number().required('Phone ID is required'),
        contactTypeId: Yup.number().required('Contact Type ID is required'),
        communicationTypeId: Yup.number().required('Communication Type ID is required'),
        tphCountryPrefix: Yup.string().required('Country Prefix is required'),
        tphNumber: Yup.string().required('Phone Number is required'),
        tphExtension: Yup.string(),
        comments: Yup.string(),
        status_id: Yup.number().required('Status ID is required'),
        is_active: Yup.boolean().required('Is Active is required'),
    });

    console.log('transPhone', transPhone);
    
    const formik = useFormik({
        initialValues: getInitialValues(transPhone!),
        validationSchema: transPhoneSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (transPhone) {
                    // put request logic
                } else {
                    // post request logic
                }
                resetForm();
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, getFieldProps } = formik;

    const getHelperText = (fieldError: any): string | undefined => {
        return typeof fieldError === 'string' ? fieldError : undefined;
    };

    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{transPhone ? 'View Transaction Contact Details' : 'New transPhone'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                <Grid container spacing={3}>
                                <Grid item xs={6}>
                                <Stack spacing={1.25}>
                                    <InputLabel htmlFor="phoneId">Phone ID</InputLabel>
                                    <TextField
                                    fullWidth
                                    id="phoneId"
                                    {...getFieldProps('phoneId')}
                                    InputProps={{
                                        readOnly: true,
                                    }}
                                    error={Boolean(touched.phoneId && errors.phoneId)}
                                    helperText={getHelperText(touched.phoneId && errors.phoneId)}
                                    />
                                </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                <Stack spacing={1.25}>
                                    <InputLabel htmlFor="contactTypeId">Contact Type ID</InputLabel>
                                    <TextField
                                    fullWidth
                                    id="contactTypeId"
                                    {...getFieldProps('contactTypeId')}
                                    InputProps={{
                                        readOnly: true,
                                    }}
                                    error={Boolean(touched.contactTypeId && errors.contactTypeId)}
                                    helperText={getHelperText(touched.contactTypeId && errors.contactTypeId)}
                                    />
                                </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                <Stack spacing={1.25}>
                                    <InputLabel htmlFor="communicationTypeId">Communication Type ID</InputLabel>
                                    <TextField
                                    fullWidth
                                    id="communicationTypeId"
                                    {...getFieldProps('communicationTypeId')}
                                    InputProps={{
                                        readOnly: true,
                                    }}
                                    error={Boolean(touched.communicationTypeId && errors.communicationTypeId)}
                                    helperText={getHelperText(touched.communicationTypeId && errors.communicationTypeId)}
                                    />
                                </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                <Stack spacing={1.25}>
                                    <InputLabel htmlFor="tphCountryPrefix">Country Prefix</InputLabel>
                                    <TextField
                                    fullWidth
                                    id="tphCountryPrefix"
                                    {...getFieldProps('tphCountryPrefix')}
                                    InputProps={{
                                        readOnly: true,
                                    }}
                                    error={Boolean(touched.tphCountryPrefix && errors.tphCountryPrefix)}
                                    helperText={getHelperText(touched.tphCountryPrefix && errors.tphCountryPrefix)}
                                    />
                                </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                <Stack spacing={1.25}>
                                    <InputLabel htmlFor="tphNumber">Phone Number</InputLabel>
                                    <TextField
                                    fullWidth
                                    id="tphNumber"
                                    {...getFieldProps('tphNumber')}
                                    InputProps={{
                                        readOnly: true,
                                    }}
                                    error={Boolean(touched.tphNumber && errors.tphNumber)}
                                    helperText={getHelperText(touched.tphNumber && errors.tphNumber)}
                                    />
                                </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                <Stack spacing={1.25}>
                                    <InputLabel htmlFor="tphExtension">Extension</InputLabel>
                                    <TextField
                                    fullWidth
                                    id="tphExtension"
                                    {...getFieldProps('tphExtension')}
                                    InputProps={{
                                        readOnly: true,
                                    }}
                                    error={Boolean(touched.tphExtension && errors.tphExtension)}
                                    helperText={getHelperText(touched.tphExtension && errors.tphExtension)}
                                    />
                                </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                <Stack spacing={1.25}>
                                    <InputLabel htmlFor="comments">comments</InputLabel>
                                    <TextField
                                    fullWidth
                                    id="comments"
                                    {...getFieldProps('comments')}
                                    InputProps={{
                                        readOnly: true,
                                    }}
                                    error={Boolean(touched.comments && errors.comments)}
                                    helperText={getHelperText(touched.comments && errors.comments)}
                                    />
                                </Stack>
                                </Grid>

                                <Grid item xs={6}>
                                <Stack spacing={1.25}>
                                    <InputLabel htmlFor="is_active">Is Active</InputLabel>
                                    <TextField
                                    fullWidth
                                    id="is_active"
                                    {...getFieldProps('is_active')}
                                    InputProps={{
                                        readOnly: true,
                                    }}
                                    error={Boolean(touched.is_active && errors.is_active)}
                                    helperText={getHelperText(touched.is_active && errors.is_active)}
                                    />
                                </Stack>
                                </Grid>
                            </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 1.25 }}>
                            <Button color="error" onClick={onCancel}>
                                Cancel
                            </Button>
                        </DialogActions>
                    </form>
                </LocalizationProvider>
            </FormikProvider>
        </>
    );
};

export default AddEdittransPhone;
