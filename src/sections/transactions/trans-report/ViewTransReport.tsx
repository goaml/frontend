//import { useState } To 'react';

// material-ui
import {
    //Autocomplete,
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    // FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField,
   // Tooltip
} from '@mui/material';
// import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { FormikProvider, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// types
// import { dataProps } from 'pages/transaction/trans-transaction/types/types';

export interface FormikValues {
    reportId?: number;
    rentityId?: number;
    rentityBranch?: string;
    submissionCode?: string;
    reportCode?: string;
    entityReference?: string;
    fiuRefNumber?: string;
    submissionDate?: Date;
    currencyCodeLocal?: number;
    reportingPerson?: string;
    location?: string;
    reason?: string;
    action?: string;
    statusId?: number;
    isActive?: boolean;
    systemDate?: Date;
    userId?: number;
}

export interface Props {
    transReport?: any;
    onCancel: () => void;
}

const getInitialValues = (transReport: FormikValues | null) => {
    const newtransReport: FormikValues = {
        reportId: 0,
        rentityId: 0,
        rentityBranch: "",
        submissionCode: "",
        reportCode: "",
        entityReference: "",
        fiuRefNumber: "",
        submissionDate: undefined,
        currencyCodeLocal: 0,
        reportingPerson: "",
        location: "",
        reason: "",
        action: "",
        statusId: 0,
        isActive: true,
        systemDate: undefined,
        userId: 0,
    };

    if (transReport) {
        return _.merge({}, newtransReport, transReport);
    }

    return newtransReport;
};

const ViewTransReport = ({ transReport, onCancel }: Props) => {
    const transReportSchema = Yup.object().shape({
        reportId: Yup.number().required('Report ID is required'),
        rentityId: Yup.number().required('Rentity ID is required'),
        rentityBranch: Yup.string().required('Rentity Branch is required'),
        submissionCode: Yup.string().required('Submission Code is required'),
        reportCode: Yup.string().required('Report Code is required'),
        entityReference: Yup.string().required('Entity Reference is required'),
        fiuRefNumber: Yup.string().required('FIU Ref Number is required'),
        submissionDate: Yup.date().required('Submission Date is required'),
        currencyCodeLocal: Yup.number().required('Currency Code Local is required'),
        reportingPerson: Yup.string().required('Reporting Person is required'),
        location: Yup.string().required('Location is required'),
        reason: Yup.string().required('Reason is required'),
        action: Yup.string().required('Action is required'),
        statusId: Yup.number().required('Status ID is required'),
        isActive: Yup.boolean().required('Is Active is required'),
        systemDate: Yup.date().required('System Date is required'),
        userId: Yup.number().required('User ID is required')
    });

    const formik = useFormik({
        initialValues: getInitialValues(transReport!),
        validationSchema: transReportSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (transReport) {
                    // put request logic
                } else {
                    // post request logic
                }
                resetForm();
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, getFieldProps } = formik;

    const getHelperText = (fieldError: any): string | undefined => {
        return typeof fieldError === 'string' ? fieldError : undefined;
    };

    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{transReport ? 'View transaction Report Details' : 'New Transaction Report'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                    <Grid container spacing={3}>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="reportId">Report ID</InputLabel>
                                        <TextField
                                            fullWidth
                                            disabled
                                            placeholder='No data found'
                                            id="reportId"
                                            {...getFieldProps('reportId')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.reportId && errors.reportId)}
                                            helperText={getHelperText(touched.reportId && errors.reportId)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="rentityId">Rentity ID</InputLabel>
                                        <TextField
                                            fullWidth
                                            disabled
                                            placeholder='No data found'
                                            id="rentityId"
                                            {...getFieldProps('rentityId')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.rentityId && errors.rentityId)}
                                            helperText={getHelperText(touched.rentityId && errors.rentityId)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="rentityBranch">Rentity Branch</InputLabel>
                                        <TextField
                                            fullWidth
                                            disabled
                                            placeholder='No data found'
                                            id="rentityBranch"
                                            {...getFieldProps('rentityBranch')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.rentityBranch && errors.rentityBranch)}
                                            helperText={getHelperText(touched.rentityBranch && errors.rentityBranch)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="submissionCode">Submission Code</InputLabel>
                                        <TextField
                                            fullWidth
                                            disabled
                                            placeholder='No data found'
                                            id="submissionCode"
                                            {...getFieldProps('submissionCode')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.submissionCode && errors.submissionCode)}
                                            helperText={getHelperText(touched.submissionCode && errors.submissionCode)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="reportCode">Report Code</InputLabel>
                                        <TextField
                                            fullWidth
                                            disabled
                                            placeholder='No data found'
                                            id="reportCode"
                                            {...getFieldProps('reportCode')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.reportCode && errors.reportCode)}
                                            helperText={getHelperText(touched.reportCode && errors.reportCode)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="entityReference">Entity Reference</InputLabel>
                                        <TextField
                                            fullWidth
                                            disabled
                                            placeholder='No data found'
                                            id="entityReference"
                                            {...getFieldProps('entityReference')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.entityReference && errors.entityReference)}
                                            helperText={getHelperText(touched.entityReference && errors.entityReference)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="fiuRefNumber">FIU Ref Number</InputLabel>
                                        <TextField
                                            fullWidth
                                            disabled
                                            placeholder='No data found'
                                            id="fiuRefNumber"
                                            {...getFieldProps('fiuRefNumber')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.fiuRefNumber && errors.fiuRefNumber)}
                                            helperText={getHelperText(touched.fiuRefNumber && errors.fiuRefNumber)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="submissionDate">Submission Date</InputLabel>
                                        <TextField
                                            fullWidth
                                            disabled
                                            placeholder='No data found'
                                            id="submissionDate"
                                            {...getFieldProps('submissionDate')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.submissionDate && errors.submissionDate)}
                                            helperText={getHelperText(touched.submissionDate && errors.submissionDate)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="currencyCodeLocal">Currency Code Local</InputLabel>
                                        <TextField
                                            fullWidth
                                            disabled
                                            placeholder='No data found'
                                            id="currencyCodeLocal"
                                            {...getFieldProps('currencyCodeLocal')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.currencyCodeLocal && errors.currencyCodeLocal)}
                                            helperText={getHelperText(touched.currencyCodeLocal && errors.currencyCodeLocal)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="reportingPerson">Reporting Person</InputLabel>
                                        <TextField
                                            fullWidth
                                            disabled
                                            placeholder='No data found'
                                            id="reportingPerson"
                                            {...getFieldProps('reportingPerson')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.reportingPerson && errors.reportingPerson)}
                                            helperText={getHelperText(touched.reportingPerson && errors.reportingPerson)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="location">Location</InputLabel>
                                        <TextField
                                            fullWidth
                                            disabled
                                            placeholder='No data found'
                                            id="location"
                                            {...getFieldProps('location')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.location && errors.location)}
                                            helperText={getHelperText(touched.location && errors.location)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="reason">Reason</InputLabel>
                                        <TextField
                                            fullWidth
                                            disabled
                                            placeholder='No data found'
                                            id="reason"
                                            {...getFieldProps('reason')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.reason && errors.reason)}
                                            helperText={getHelperText(touched.reason && errors.reason)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="action">Action</InputLabel>
                                        <TextField
                                            fullWidth
                                            disabled
                                            placeholder='No data found'
                                            id="action"
                                            {...getFieldProps('action')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.action && errors.action)}
                                            helperText={getHelperText(touched.action && errors.action)}
                                        />
                                        </Stack>
                                    </Grid>

                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="isActive">Is Active</InputLabel>
                                        <TextField
                                            fullWidth
                                            disabled
                                            placeholder='No data found'
                                            id="isActive"
                                            {...getFieldProps('isActive')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.isActive && errors.isActive)}
                                            helperText={getHelperText(touched.isActive && errors.isActive)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="systemDate">System Date</InputLabel>
                                        <TextField
                                            fullWidth
                                            disabled
                                            placeholder='No data found'
                                            id="systemDate"
                                            {...getFieldProps('systemDate')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.systemDate && errors.systemDate)}
                                            helperText={getHelperText(touched.systemDate && errors.systemDate)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="userId">User ID</InputLabel>
                                        <TextField
                                            fullWidth
                                            disabled
                                            placeholder='No data found'
                                            id="userId"
                                            {...getFieldProps('userId')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.userId && errors.userId)}
                                            helperText={getHelperText(touched.userId && errors.userId)}
                                        />
                                        </Stack>
                                    </Grid>
                                    </Grid>

                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 1.25 }}>
                            <Button color="error" onClick={onCancel}>
                                Cancel
                            </Button>
                        </DialogActions>
                    </form>
                </LocalizationProvider>
            </FormikProvider>
        </>
    );
};

export default ViewTransReport;
