//import { useState } To 'react';

// material-ui
import {
    //Autocomplete,
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    // FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField,
   // Tooltip
} from '@mui/material';
// import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { FormikProvider, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// types
import { dataProps } from 'pages/transaction/foreign-currency/types/types';

export interface FormikValues {
    foreignCurrencyId?: number;
    currenciesId?: number;
    foreignAmount?: number;
    foreignExchangeRate?: number;
    statusId?: number;
    isActive?: boolean;
}

export interface Props {
    FCurrencyAddEditFCurrency?: dataProps;
    onCancel: () => void;
}

const getInitialValues = (FCurrencyAddEditFCurrency: FormikValues | null) => {
    const newFCurrencyAddEditFCurrency: FormikValues = {
        foreignCurrencyId: 0,
        currenciesId: 0,
        foreignAmount: 0,
        foreignExchangeRate: 0,
        statusId: 0,
        isActive: true,
    };

    if (FCurrencyAddEditFCurrency) {
        return _.merge({}, newFCurrencyAddEditFCurrency, FCurrencyAddEditFCurrency);
    }

    return newFCurrencyAddEditFCurrency;
};

const AddEditFCurrency = ({ FCurrencyAddEditFCurrency, onCancel }: Props) => {
    const FCurrencyAddEditFCurrencySchema = Yup.object().shape({
        toId: Yup.number().required('To ID is required'),
        toFundsCode: Yup.string().required('To Funds Code is required'),
        toFundsComment: Yup.string().required('To Funds Comment is required'),
        toForeignCurrency: Yup.string().required('To Foreign Currency is required'),
        tConductor: Yup.string().required('T Conductor is required'),
        toAccount: Yup.string().required('To Account is required'),
        toPerson: Yup.string().required('To Person is required'),
        toEntity: Yup.string().required('To Entity is required'),
        toCountry: Yup.string().required('To Country is required'),
        statusId: Yup.number().required('Status ID is required'),
        isActive: Yup.boolean().required('Is Active is required'),
        transactionId: Yup.number().required('Transaction ID is required'),
    });

    const formik = useFormik({
        initialValues: getInitialValues(FCurrencyAddEditFCurrency!),
        validationSchema: FCurrencyAddEditFCurrencySchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (FCurrencyAddEditFCurrency) {
                    // put request logic
                } else {
                    // post request logic
                }
                resetForm();
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, getFieldProps } = formik;

    const getHelperText = (fieldError: any): string | undefined => {
        return typeof fieldError === 'string' ? fieldError : undefined;
    };

    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{FCurrencyAddEditFCurrency ? 'View Foreign Currency Details' : 'New FCurrencyAddEditFCurrency'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                <Grid container spacing={3}>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="foreignCurrencyId">Foreign Currency ID</InputLabel>
                                        <TextField
                                            fullWidth
                                            id="foreignCurrencyId"
                                            {...getFieldProps('foreignCurrencyId')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.foreignCurrencyId && errors.foreignCurrencyId)}
                                            helperText={getHelperText(touched.foreignCurrencyId && errors.foreignCurrencyId)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="currenciesId">Currencies ID</InputLabel>
                                        <TextField
                                            fullWidth
                                            id="currenciesId"
                                            {...getFieldProps('currenciesId')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.currenciesId && errors.currenciesId)}
                                            helperText={getHelperText(touched.currenciesId && errors.currenciesId)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="foreignAmount">Foreign Amount </InputLabel>
                                        <TextField
                                            fullWidth
                                            id="foreignAmount"
                                            {...getFieldProps('foreignAmount')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.foreignAmount && errors.foreignAmount)}
                                            helperText={getHelperText(touched.foreignAmount && errors.foreignAmount)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="foreignExchangeRate">Foreign Exchange Rate </InputLabel>
                                        <TextField
                                            fullWidth
                                            id="foreignExchangeRate"
                                            {...getFieldProps('foreignExchangeRate')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.foreignExchangeRate && errors.foreignExchangeRate)}
                                            helperText={getHelperText(touched.foreignExchangeRate && errors.foreignExchangeRate)}
                                        />
                                        </Stack>
                                    </Grid>

                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="isActive">Is Active </InputLabel>
                                        <TextField
                                            fullWidth
                                            id="isActive"
                                            {...getFieldProps('isActive')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.isActive && errors.isActive)}
                                            helperText={getHelperText(touched.isActive && errors.isActive)}
                                        />
                                        </Stack>
                                    </Grid>
                                    {/* Additional fields can be added similarly */}
                                    </Grid>

                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 1.25 }}>
                            <Button color="error" onClick={onCancel}>
                                Cancel
                            </Button>
                        </DialogActions>
                    </form>
                </LocalizationProvider>
            </FormikProvider>
        </>
    );
};

export default AddEditFCurrency;
