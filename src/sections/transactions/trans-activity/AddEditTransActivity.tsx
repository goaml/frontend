//import { useState } To 'react';

// material-ui
import {
    //Autocomplete,
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    // FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField,
   // Tooltip
} from '@mui/material';
// import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { FormikProvider, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// types
import { dataProps } from 'pages/transaction/trans-activity/types/types';

export interface FormikValues {
    activityId?: number;
    reportParties?: string; 
    reportParty?: string; 
    accountMyClientId?: number;
    personMyClientId?: number;
    entityMyClientId?: number;
    significance?: number;
    reason?: string;
    comments?: string;
    statusId?: number;
    isActive?: boolean;
    reportId?: number;
}

export interface Props {
    transActivity?: dataProps;
    onCancel: () => void;
}

const getInitialValues = (transActivity: FormikValues | null) => {
    const newtransActivity: FormikValues = {
        activityId: 0,
        reportParties: '', 
        reportParty: '', 
        accountMyClientId: 0,
        personMyClientId: 0,
        entityMyClientId: 0,
        significance: 0,
        reason: '',
        comments: '',
        statusId: 0,
        isActive: true,
        reportId: 0,
    };

    if (transActivity) {
        return _.merge({}, newtransActivity, transActivity);
    }

    return newtransActivity;
};

const AddEdittransActivity = ({ transActivity, onCancel }: Props) => {
    const transActivitySchema = Yup.object().shape({
        activityId: Yup.number().required('Activity ID is required'),
        reportParties: Yup.string().required('Report Parties is required'),
        reportParty: Yup.string().required('Report Party is required'),
        accountMyClientId: Yup.number().required('Account My Client ID is required'),
        personMyClientId: Yup.number().required('Person My Client ID is required'),
        entityMyClientId: Yup.number().required('Entity My Client ID is required'),
        significance: Yup.number().required('Significance is required'),
        reason: Yup.string().required('Reason is required'),
        comments: Yup.string().required('Comments is required'),
        statusId: Yup.number().required('Status ID is required'),
        isActive: Yup.boolean().required('Is Active is required'),
        reportId: Yup.number().required('Report ID is required'),
    });

    const formik = useFormik({
        initialValues: getInitialValues(transActivity!),
        validationSchema: transActivitySchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (transActivity) {
                    // put request logic
                } else {
                    // post request logic
                }
                resetForm();
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, getFieldProps } = formik;

    const getHelperText = (fieldError: any): string | undefined => {
        return typeof fieldError === 'string' ? fieldError : undefined;
    };

    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{transActivity ? 'View Transaction Reciever Details' : 'New transActivity'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                <Grid container spacing={3}>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="activityId">Activity ID</InputLabel>
                                        <TextField
                                            fullWidth
                                            id="activityId"
                                            {...getFieldProps('activityId')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.activityId && errors.activityId)}
                                            helperText={getHelperText(touched.activityId && errors.activityId)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="reportParties">Report Parties</InputLabel>
                                        <TextField
                                            fullWidth
                                            id="reportParties"
                                            {...getFieldProps('reportParties')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.reportParties && errors.reportParties)}
                                            helperText={getHelperText(touched.reportParties && errors.reportParties)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="reportParty">Report Party</InputLabel>
                                        <TextField
                                            fullWidth
                                            id="reportParty"
                                            {...getFieldProps('reportParty')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.reportParty && errors.reportParty)}
                                            helperText={getHelperText(touched.reportParty && errors.reportParty)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="accountMyClientId">Account My Client ID</InputLabel>
                                        <TextField
                                            fullWidth
                                            id="accountMyClientId"
                                            {...getFieldProps('accountMyClientId')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.accountMyClientId && errors.accountMyClientId)}
                                            helperText={getHelperText(touched.accountMyClientId && errors.accountMyClientId)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="personMyClientId">Person My Client ID</InputLabel>
                                        <TextField
                                            fullWidth
                                            id="personMyClientId"
                                            {...getFieldProps('personMyClientId')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.personMyClientId && errors.personMyClientId)}
                                            helperText={getHelperText(touched.personMyClientId && errors.personMyClientId)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="entityMyClientId">Entity My Client ID</InputLabel>
                                        <TextField
                                            fullWidth
                                            id="entityMyClientId"
                                            {...getFieldProps('entityMyClientId')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.entityMyClientId && errors.entityMyClientId)}
                                            helperText={getHelperText(touched.entityMyClientId && errors.entityMyClientId)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="significance">Significance</InputLabel>
                                        <TextField
                                            fullWidth
                                            id="significance"
                                            {...getFieldProps('significance')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.significance && errors.significance)}
                                            helperText={getHelperText(touched.significance && errors.significance)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="reason">Reason</InputLabel>
                                        <TextField
                                            fullWidth
                                            id="reason"
                                            {...getFieldProps('reason')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.reason && errors.reason)}
                                            helperText={getHelperText(touched.reason && errors.reason)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="comments">Comments</InputLabel>
                                        <TextField
                                            fullWidth
                                            id="comments"
                                            {...getFieldProps('comments')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.comments && errors.comments)}
                                            helperText={getHelperText(touched.comments && errors.comments)}
                                        />
                                        </Stack>
                                    </Grid>
 
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="isActive">Is Active</InputLabel>
                                        <TextField
                                            fullWidth
                                            id="isActive"
                                            {...getFieldProps('isActive')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.isActive && errors.isActive)}
                                            helperText={getHelperText(touched.isActive && errors.isActive)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="reportId">Report ID</InputLabel>
                                        <TextField
                                            fullWidth
                                            id="reportId"
                                            {...getFieldProps('reportId')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.reportId && errors.reportId)}
                                            helperText={getHelperText(touched.reportId && errors.reportId)}
                                        />
                                        </Stack>
                                    </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 1.25 }}>
                            <Button color="error" onClick={onCancel}>
                                Cancel
                            </Button>
                        </DialogActions>
                    </form>
                </LocalizationProvider>
            </FormikProvider>
        </>
    );
};

export default AddEdittransActivity;
