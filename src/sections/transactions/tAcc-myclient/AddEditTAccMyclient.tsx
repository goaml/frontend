// material-ui
import {
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    Grid,
    InputLabel,
    Stack,
    TextField,
} from '@mui/material';

import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { FormikProvider, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// types
import { dataProps } from 'pages/transaction/tAcc-mycient/types/types';

export interface FormikValues {
    accountMyClientId?: number | undefined;
    institutionName?: string | undefined;
    institutionCode?: string | undefined;
    swift?: string | undefined;
    nonBankInstitution?: boolean | undefined;
    branch?: string | undefined;
    account?: string | undefined;
    currenciesId?: number | undefined;
    accountName?: string | undefined;
    iban?: string | undefined;
    clientNumber?: string | undefined;
    accountTypeId?: number | undefined;
    entityMyClientId?: number | undefined;
    personMyClientId?: number | undefined;
    accountPersonRoleTypeId?: number | undefined;
    opened?: string | undefined;
    closed?: string | undefined;
    balance?: number | undefined; // Assuming Decimal translates to number
    dateBalance?: string | undefined;
    accountStatusTypeId?: number | undefined;
    beneficiary?: string | undefined;
    beneficiaryComment?: string | undefined;
    comments?: string | undefined;
    statusId?: number | undefined;
    isActive?: boolean | undefined;
}

export interface Props {
    tAccMyclient?: dataProps;
    onCancel: () => void;
}

const getInitialValues = (tAccMyclient: FormikValues | null) => {
    const newtAccMyclient: FormikValues = {
        accountMyClientId: 0 ,
        institutionName: '' ,
        institutionCode: '' ,
        swift: '' ,
        nonBankInstitution: true ,
        branch: '' ,
        account: '' ,
        currenciesId: 0 ,
        accountName: '' ,
        iban: '' ,
        clientNumber: '' ,
        accountTypeId: 0 ,
        entityMyClientId: 0 ,
        personMyClientId: 0 ,
        accountPersonRoleTypeId: 0 ,
        opened: '' ,
        closed: '' ,
        balance: 0 , // Assuming Decimal translates to 0
        dateBalance: '' ,
        accountStatusTypeId: 0 ,
        beneficiary: '' ,
        beneficiaryComment: '' ,
        comments: '' ,
        statusId: 0 ,
        isActive: true ,
    };

    if (tAccMyclient) {
        return _.merge({}, newtAccMyclient, tAccMyclient);
    }

    return newtAccMyclient;
};

const AddEdittAccMyclient = ({ tAccMyclient, onCancel }: Props) => {
    const tAccMyclientSchema = Yup.object().shape({
        toId: Yup.number().required('To ID is required'),
        toFundsCode: Yup.string().required('To Funds Code is required'),
        toFundsComment: Yup.string().required('To Funds Comment is required'),
        toForeignCurrency: Yup.string().required('To Foreign Currency is required'),
        tConductor: Yup.string().required('T Conductor is required'),
        toAccount: Yup.string().required('To Account is required'),
        toPerson: Yup.string().required('To Person is required'),
        toEntity: Yup.string().required('To Entity is required'),
        toCountry: Yup.string().required('To Country is required'),
        statusId: Yup.number().required('Status ID is required'),
        isActive: Yup.boolean().required('Is Active is required'),
        transactionId: Yup.number().required('Transaction ID is required'),
    });

    const formik = useFormik({
        initialValues: getInitialValues(tAccMyclient!),
        validationSchema: tAccMyclientSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (tAccMyclient) {
                    // put request logic
                } else {
                    // post request logic
                }
                resetForm();
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, getFieldProps } = formik;

    const getHelperText = (fieldError: any): string | undefined => {
        return typeof fieldError === 'string' ? fieldError : undefined;
    };

    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{tAccMyclient ? 'View Transaction Reciever Details' : 'New tAccMyclient'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                <Grid container spacing={3}>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="accountMyClientId">Account My Client ID</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="accountMyClientId"
                                        {...getFieldProps('accountMyClientId')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.accountMyClientId && errors.accountMyClientId)}
                                        helperText={getHelperText(touched.accountMyClientId && errors.accountMyClientId)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="institutionName">Institution Name</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="institutionName"
                                        {...getFieldProps('institutionName')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.institutionName && errors.institutionName)}
                                        helperText={getHelperText(touched.institutionName && errors.institutionName)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="institutionCode">Institution Code</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="institutionCode"
                                        {...getFieldProps('institutionCode')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.institutionCode && errors.institutionCode)}
                                        helperText={getHelperText(touched.institutionCode && errors.institutionCode)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="swift">SWIFT</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="swift"
                                        {...getFieldProps('swift')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.swift && errors.swift)}
                                        helperText={getHelperText(touched.swift && errors.swift)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="nonBankInstitution">Non Bank Institution</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="nonBankInstitution"
                                        {...getFieldProps('nonBankInstitution')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.nonBankInstitution && errors.nonBankInstitution)}
                                        helperText={getHelperText(touched.nonBankInstitution && errors.nonBankInstitution)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="branch">Branch</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="branch"
                                        {...getFieldProps('branch')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.branch && errors.branch)}
                                        helperText={getHelperText(touched.branch && errors.branch)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="account">Account</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="account"
                                        {...getFieldProps('account')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.account && errors.account)}
                                        helperText={getHelperText(touched.account && errors.account)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="currenciesId">Currencies ID</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="currenciesId"
                                        {...getFieldProps('currenciesId')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.currenciesId && errors.currenciesId)}
                                        helperText={getHelperText(touched.currenciesId && errors.currenciesId)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="accountName">Account Name</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="accountName"
                                        {...getFieldProps('accountName')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.accountName && errors.accountName)}
                                        helperText={getHelperText(touched.accountName && errors.accountName)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="iban">IBAN</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="iban"
                                        {...getFieldProps('iban')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.iban && errors.iban)}
                                        helperText={getHelperText(touched.iban && errors.iban)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="clientNumber">Client Number</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="clientNumber"
                                        {...getFieldProps('clientNumber')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.clientNumber && errors.clientNumber)}
                                        helperText={getHelperText(touched.clientNumber && errors.clientNumber)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="accountTypeId">Account Type ID</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="accountTypeId"
                                        {...getFieldProps('accountTypeId')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.accountTypeId && errors.accountTypeId)}
                                        helperText={getHelperText(touched.accountTypeId && errors.accountTypeId)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="entityMyClientId">Entity My Client ID</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="entityMyClientId"
                                        {...getFieldProps('entityMyClientId')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.entityMyClientId && errors.entityMyClientId)}
                                        helperText={getHelperText(touched.entityMyClientId && errors.entityMyClientId)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="personMyClientId">Person My Client ID</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="personMyClientId"
                                        {...getFieldProps('personMyClientId')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.personMyClientId && errors.personMyClientId)}
                                        helperText={getHelperText(touched.personMyClientId && errors.personMyClientId)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="accountPersonRoleTypeId">Account Person Role Type ID</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="accountPersonRoleTypeId"
                                        {...getFieldProps('accountPersonRoleTypeId')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.accountPersonRoleTypeId && errors.accountPersonRoleTypeId)}
                                        helperText={getHelperText(touched.accountPersonRoleTypeId && errors.accountPersonRoleTypeId)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="opened">Opened</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="opened"
                                        {...getFieldProps('opened')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.opened && errors.opened)}
                                        helperText={getHelperText(touched.opened && errors.opened)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="closed">Closed</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="closed"
                                        {...getFieldProps('closed')}
                                        InputProps={{
                                        readOnly: true,
                                        }}       error={Boolean(touched.closed && errors.closed)}
                                        helperText={getHelperText(touched.closed && errors.closed)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="balance">Balance</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="balance"
                                        {...getFieldProps('balance')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.balance && errors.balance)}
                                        helperText={getHelperText(touched.balance && errors.balance)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="dateBalance">Date Balance</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="dateBalance"
                                        {...getFieldProps('dateBalance')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.dateBalance && errors.dateBalance)}
                                        helperText={getHelperText(touched.dateBalance && errors.dateBalance)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="accountStatusTypeId">Account Status Type ID</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="accountStatusTypeId"
                                        {...getFieldProps('accountStatusTypeId')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.accountStatusTypeId && errors.accountStatusTypeId)}
                                        helperText={getHelperText(touched.accountStatusTypeId && errors.accountStatusTypeId)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="beneficiary">Beneficiary</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="beneficiary"
                                        {...getFieldProps('beneficiary')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.beneficiary && errors.beneficiary)}
                                        helperText={getHelperText(touched.beneficiary && errors.beneficiary)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="beneficiaryComment">Beneficiary Comment</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="beneficiaryComment"
                                        {...getFieldProps('beneficiaryComment')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.beneficiaryComment && errors.beneficiaryComment)}
                                        helperText={getHelperText(touched.beneficiaryComment && errors.beneficiaryComment)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="comments">Comments</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="comments"
                                        {...getFieldProps('comments')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.comments && errors.comments)}
                                        helperText={getHelperText(touched.comments && errors.comments)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="isActive">Is Active</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="isActive"
                                        {...getFieldProps('isActive')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.isActive && errors.isActive)}
                                        helperText={getHelperText(touched.isActive && errors.isActive)}
                                    />
                                    </Stack>
                                </Grid>
                                </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 1.25 }}>
                            <Button color="error" onClick={onCancel}>
                                Cancel
                            </Button>
                        </DialogActions>
                    </form>
                </LocalizationProvider>
            </FormikProvider>
        </>
    );
};

export default AddEdittAccMyclient;
