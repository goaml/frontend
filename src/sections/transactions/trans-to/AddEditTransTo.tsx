//import { useState } To 'react';

// material-ui
import {
    //Autocomplete,
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    // FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField,
   // Tooltip
} from '@mui/material';
// import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { FormikProvider, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// types
import { dataProps } from 'pages/transaction/trans-to/types/types';

export interface FormikValues {
    toId?:number;
    toFundsComment?:string;
    toForeignCurrencyId?:number;
    conductorPersonId?:number;
    toAccountId?:number;
    toPersonId?:number;
    toEntityId?:number;
    transactionId?:number;
    isActive?:boolean;
    fundTypeCode?: string;
    rfundsType?:{
        fundTypeCode?: string;
    };
    rcountryCodes?:{
        description?:string;
    }
    rstatus?:{
        statusId?:number;
        isActive?:boolean;
        statusCode?:string;
    }
}

export interface Props {
    transTo?: dataProps;
    onCancel: () => void;
}

const getInitialValues = (transTo: FormikValues | null) => {
    const newtransTo: FormikValues = {
        toId:0,
        toFundsComment:'',
        toForeignCurrencyId:0,
        conductorPersonId:0,
        toAccountId:0,
        toPersonId:0,
        toEntityId:0,
        transactionId:0,
        isActive:true,
        rfundsType:{
            fundTypeCode: '',
        },
        rcountryCodes:{
            description:'',
        },
        rstatus:{
            statusId:0,
            isActive:true,
            statusCode:'',
        }
    };

    if (transTo) {
        return _.merge({}, newtransTo, transTo);
    }

    return newtransTo;
};

const AddEdittransTo = ({ transTo, onCancel }: Props) => {
    const transToSchema = Yup.object().shape({
        toId: Yup.number().required('To ID is required'),
        toFundsCode: Yup.string().required('To Funds Code is required'),
        toFundsComment: Yup.string().required('To Funds Comment is required'),
        toForeignCurrency: Yup.string().required('To Foreign Currency is required'),
        tConductor: Yup.string().required('T Conductor is required'),
        toAccount: Yup.string().required('To Account is required'),
        toPerson: Yup.string().required('To Person is required'),
        toEntity: Yup.string().required('To Entity is required'),
        toCountry: Yup.string().required('To Country is required'),
        statusId: Yup.number().required('Status ID is required'),
        isActive: Yup.boolean().required('Is Active is required'),
        transactionId: Yup.number().required('Transaction ID is required'),
    });

    const formik = useFormik({
        initialValues: getInitialValues(transTo!),
        validationSchema: transToSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (transTo) {
                    // put request logic
                } else {
                    // post request logic
                }
                resetForm();
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, getFieldProps } = formik;

    const getHelperText = (fieldError: any): string | undefined => {
        return typeof fieldError === 'string' ? fieldError : undefined;
    };

    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{transTo ? 'View Transaction Reciever Details' : 'New transTo'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                    <Grid container spacing={3}>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="toId">To ID</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="toId"
                                                    {...getFieldProps('toId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.toId && errors.toId)}
                                                    helperText={getHelperText(touched.toId && errors.toId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="rFundsType.fundTypeCode">To Funds Code</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="rFundsType.fundTypeCode"
                                                    {...getFieldProps('rFundsType.fundTypeCode')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.fundTypeCode && errors.fundTypeCode)}
                                                    helperText={getHelperText(touched.fundTypeCode && errors.fundTypeCode)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="toFundsComment">To Funds Comment </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="toFundsComment"
                                                    {...getFieldProps('toFundsComment')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.toFundsComment && errors.toFundsComment)}
                                                    helperText={getHelperText(touched.toFundsComment && errors.toFundsComment)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="toForeignCurrencyId">To Foreign Currency </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="toForeignCurrencyId"
                                                    {...getFieldProps('toForeignCurrencyId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.toForeignCurrencyId && errors.toForeignCurrencyId)}
                                                    helperText={getHelperText(touched.toForeignCurrencyId && errors.toForeignCurrencyId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="conductorPersonId">T Conductor </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="conductorPersonId"
                                                    {...getFieldProps('conductorPersonId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.conductorPersonId && errors.conductorPersonId)}
                                                    helperText={getHelperText(touched.conductorPersonId && errors.conductorPersonId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="toAccountId">To Account </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="toAccountId"
                                                    {...getFieldProps('toAccountId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.toAccountId && errors.toAccountId)}
                                                    helperText={getHelperText(touched.toAccountId && errors.toAccountId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="toPersonId">To Person </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="toPersonId"
                                                    {...getFieldProps('toPersonId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.toPersonId && errors.toPersonId)}
                                                    helperText={getHelperText(touched.toPersonId && errors.toPersonId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="toEntityId">To Entity </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="toEntityId"
                                                    {...getFieldProps('toEntityId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.toEntityId && errors.toEntityId)}
                                                    helperText={getHelperText(touched.toEntityId && errors.toEntityId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="rcountryCodes.description">To Country </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="rcountryCodes.description"
                                                    {...getFieldProps('rcountryCodes.description')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.rcountryCodes && errors.rcountryCodes)}
                                                    helperText={getHelperText(touched.rcountryCodes && errors.rcountryCodes)}
                                                />
                                            </Stack>
                                        </Grid>

                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="isActive">Is Active </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="isActive"
                                                    {...getFieldProps('isActive')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.isActive && errors.isActive)}
                                                    helperText={getHelperText(touched.isActive && errors.isActive)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="transactionId">Transaction ID </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="transactionId"
                                                    {...getFieldProps('transactionId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.transactionId && errors.transactionId)}
                                                    helperText={getHelperText(touched.transactionId && errors.transactionId)}
                                                />
                                            </Stack>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 1.25 }}>
                            <Button color="error" onClick={onCancel}>
                                Cancel
                            </Button>
                        </DialogActions>
                    </form>
                </LocalizationProvider>
            </FormikProvider>
        </>
    );
};

export default AddEdittransTo;
