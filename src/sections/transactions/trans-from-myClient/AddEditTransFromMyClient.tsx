//import { useState } from 'react';

// material-ui
import {
    //Autocomplete,
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    // FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField,
   // Tooltip
} from '@mui/material';
// import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { FormikProvider, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// types
import { dataProps } from 'pages/transaction/trans-from-myClient/types/types';

export interface FormikValues {
    fromMyClientId?:number,
    fromFundsComment?:string,
    fromForeignCurrencyId?:number,
    conductorPersonMyClientId?:number,
    fromAccountMyClientId?:number,
    fromPersonMyClientId?:number,
    fromEntityMyClientId?:number,
    transactionId?:number,
    isActive?:boolean,
    fundTypeCode?: string,
    rfundsType?:{
        fundTypeCode?: string,
    },
    rcountryCodes?:{
        description?:string,
    }
    rstatus?:{
        statusId?:number,
        isActive?:boolean,
        statusCode?:string,
    }

}

export interface Props {
    transFromMyClient?: dataProps;
    onCancel: () => void;
}

const getInitialValues = (transFromMyClient: FormikValues | null) => {
    const newtransFrom: FormikValues = {
        fromMyClientId:0,
        fromFundsComment:'',
        fromForeignCurrencyId:0,
        conductorPersonMyClientId:0,
        fromAccountMyClientId:0,
        fromPersonMyClientId:0,
        fromEntityMyClientId:0,
        transactionId:0,
        isActive:true,
        fundTypeCode: '',
        rfundsType:{
            fundTypeCode: '',
        },
        rcountryCodes:{
            description:'',
        },
        rstatus:{
            statusId:0,
            isActive:true,
            statusCode:'',
        }
    };

    if (transFromMyClient) {
        return _.merge({}, newtransFrom, transFromMyClient);
    }

    return newtransFrom;
};

const AddEdittransFromMyClient = ({ transFromMyClient, onCancel }: Props) => {
    const transFromSchema = Yup.object().shape({
        fromMyClientId: Yup.number().required('From ID is required'),
        fromFundsCode: Yup.string().required('From Funds Code is required'),
        fromFundsComment: Yup.string().required('From Funds Comment is required'),
        fromForeignCurrency: Yup.string().required('From Foreign Currency is required'),
        tConductor: Yup.string().required('T Conductor is required'),
        fromAccount: Yup.string().required('From Account is required'),
        fromPerson: Yup.string().required('From Person is required'),
        fromEntity: Yup.string().required('From Entity is required'),
        fromCountry: Yup.string().required('From Country is required'),
        statusId: Yup.number().required('Status ID is required'),
        isActive: Yup.boolean().required('Is Active is required'),
        transactionId: Yup.number().required('Transaction ID is required'),
    });

    const formik = useFormik({
        initialValues: getInitialValues(transFromMyClient || null),
        validationSchema: transFromSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (transFromMyClient) {
                    // put request logic
                } else {
                    // post request logic
                }
                resetForm();
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, getFieldProps } = formik;

    const getHelperText = (fieldError: any): string | undefined => {
        return typeof fieldError === 'string' ? fieldError : undefined;
    };

    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{transFromMyClient ? 'View transaction Sender Details' : 'New transFrom'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                    <Grid container spacing={3}>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="fromMyClientId">From ID</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="fromMyClientId"
                                                    {...getFieldProps('fromMyClientId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.fromMyClientId && errors.fromMyClientId)}
                                                    helperText={getHelperText(touched.fromMyClientId && errors.fromMyClientId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="rfundsType.fundTypeCode">From Funds Code</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="rfundsType.fundTypeCode"
                                                    {...getFieldProps('rfundsType.fundTypeCode')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.fundTypeCode && errors.fundTypeCode)}
                                                    helperText={getHelperText(touched.fundTypeCode && errors.fundTypeCode)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="fromFundsComment">From Funds Comment </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="fromFundsComment"
                                                    {...getFieldProps('fromFundsComment')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.fromFundsComment && errors.fromFundsComment)}
                                                    helperText={getHelperText(touched.fromFundsComment && errors.fromFundsComment)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="fromForeignCurrencyId">From Foreign Currency </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="fromForeignCurrencyId"
                                                    {...getFieldProps('fromForeignCurrencyId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.fromForeignCurrencyId && errors.fromForeignCurrencyId)}
                                                    helperText={getHelperText(touched.fromForeignCurrencyId && errors.fromForeignCurrencyId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="conductorPersonMyClientId">T Conductor </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="conductorPersonMyClientId"
                                                    {...getFieldProps('conductorPersonMyClientId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.conductorPersonMyClientId && errors.conductorPersonMyClientId)}
                                                    helperText={getHelperText(touched.conductorPersonMyClientId && errors.conductorPersonMyClientId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="fromAccountMyClientId">From Account </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="fromAccountMyClientId"
                                                    {...getFieldProps('fromAccountMyClientId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.fromAccountMyClientId && errors.fromAccountMyClientId)}
                                                    helperText={getHelperText(touched.fromAccountMyClientId && errors.fromAccountMyClientId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="fromPersonMyClientId">From Person </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="fromPersonMyClientId"
                                                    {...getFieldProps('fromPersonMyClientId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.fromPersonMyClientId && errors.fromPersonMyClientId)}
                                                    helperText={getHelperText(touched.fromPersonMyClientId && errors.fromPersonMyClientId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="fromEntityMyClientId">From Entity </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="fromEntityMyClientId"
                                                    {...getFieldProps('fromEntityMyClientId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.fromEntityMyClientId && errors.fromEntityMyClientId)}
                                                    helperText={getHelperText(touched.fromEntityMyClientId && errors.fromEntityMyClientId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="rcountryCodes.description">From Country </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="rcountryCodes.description"
                                                    {...getFieldProps('rcountryCodes.description')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.rcountryCodes && errors.rcountryCodes)}
                                                    helperText={getHelperText(touched.rcountryCodes && errors.rcountryCodes)}
                                                />
                                            </Stack>
                                        </Grid>

                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="isActive">Is Active </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="isActive"
                                                    {...getFieldProps('isActive')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.isActive && errors.isActive)}
                                                    helperText={getHelperText(touched.isActive && errors.isActive)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="transactionId">Transaction ID </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="transactionId"
                                                    {...getFieldProps('transactionId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.transactionId && errors.transactionId)}
                                                    helperText={getHelperText(touched.transactionId && errors.transactionId)}
                                                />
                                            </Stack>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 1.25 }}>
                            <Button color="error" onClick={onCancel}>
                                Cancel
                            </Button>
                        </DialogActions>
                    </form>
                </LocalizationProvider>
            </FormikProvider>
        </>
    );
};

export default AddEdittransFromMyClient;
