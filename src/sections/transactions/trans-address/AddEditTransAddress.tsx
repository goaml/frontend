//import { useState } To 'react';

// material-ui
import {
    //Autocomplete,
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    // FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField,
   // Tooltip
} from '@mui/material';
// import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { FormikProvider, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// types
import { dataProps } from 'pages/transaction/trans-address/types/types';

export interface FormikValues {
    address_id?: number;
    contact_type_id?: number;
    address?: string;
    town?: string;
    city_id?: number;
    zip?: string;
    country_code_id?: number;
    state?: string;
    comments?: string;
    status_id?: number;
    is_active?: boolean;
}

export interface Props {
    transAddress?: dataProps;
    onCancel: () => void;
}

const getInitialValues = (transAddress: FormikValues | null) => {
    const newtransAddress: FormikValues = {
        address_id: 0,
        contact_type_id: 0,
        address: '',
        town: '',
        city_id: 0,
        zip: '',
        country_code_id: 0,
        state: '',
        comments: '',
        status_id: 0,
        is_active: true,
    };

    if (transAddress) {
        return _.merge({}, newtransAddress, transAddress);
    }

    return newtransAddress;
};

const AddEdittransAddress = ({ transAddress, onCancel }: Props) => {
    const transAddressSchema = Yup.object().shape({
        toId: Yup.number().required('To ID is required'),
        toFundsCode: Yup.string().required('To Funds Code is required'),
        toFundsComment: Yup.string().required('To Funds Comment is required'),
        toForeignCurrency: Yup.string().required('To Foreign Currency is required'),
        tConductor: Yup.string().required('T Conductor is required'),
        toAccount: Yup.string().required('To Account is required'),
        toPerson: Yup.string().required('To Person is required'),
        toEntity: Yup.string().required('To Entity is required'),
        toCountry: Yup.string().required('To Country is required'),
        statusId: Yup.number().required('Status ID is required'),
        isActive: Yup.boolean().required('Is Active is required'),
        transactionId: Yup.number().required('Transaction ID is required'),
    });

    const formik = useFormik({
        initialValues: getInitialValues(transAddress!),
        validationSchema: transAddressSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (transAddress) {
                    // put request logic
                } else {
                    // post request logic
                }
                resetForm();
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, getFieldProps } = formik;

    const getHelperText = (fieldError: any): string | undefined => {
        return typeof fieldError === 'string' ? fieldError : undefined;
    };

    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{transAddress ? 'View Transaction Address Details' : 'New transAddress'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                <Grid container spacing={3}>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="address_id">
                                            Address ID
                                        </InputLabel>
                                        <TextField
                                            fullWidth
                                            id="address_id"
                                            {...getFieldProps('address_id')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.address_id && errors.address_id)}
                                            helperText={getHelperText(touched.address_id && errors.address_id)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="contact_type_id">
                                            Contact Type ID
                                        </InputLabel>
                                        <TextField
                                            fullWidth
                                            id="contact_type_id"
                                            {...getFieldProps('contact_type_id')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.contact_type_id && errors.contact_type_id)}
                                            helperText={getHelperText(touched.contact_type_id && errors.contact_type_id)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="address">
                                            Address
                                        </InputLabel>
                                        <TextField
                                            fullWidth
                                            id="address"
                                            {...getFieldProps('address')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.address && errors.address)}
                                            helperText={getHelperText(touched.address && errors.address)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="town">
                                            Town
                                        </InputLabel>
                                        <TextField
                                            fullWidth
                                            id="town"
                                            {...getFieldProps('town')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.town && errors.town)}
                                            helperText={getHelperText(touched.town && errors.town)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="city_id">
                                            City ID
                                        </InputLabel>
                                        <TextField
                                            fullWidth
                                            id="city_id"
                                            {...getFieldProps('city_id')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.city_id && errors.city_id)}
                                            helperText={getHelperText(touched.city_id && errors.city_id)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="zip">
                                            ZIP
                                        </InputLabel>
                                        <TextField
                                            fullWidth
                                            id="zip"
                                            {...getFieldProps('zip')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.zip && errors.zip)}
                                            helperText={getHelperText(touched.zip && errors.zip)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="country_code_id">
                                            Country Code ID
                                        </InputLabel>
                                        <TextField
                                            fullWidth
                                            id="country_code_id"
                                            {...getFieldProps('country_code_id')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.country_code_id && errors.country_code_id)}
                                            helperText={getHelperText(touched.country_code_id && errors.country_code_id)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="state">
                                            State
                                        </InputLabel>
                                        <TextField
                                            fullWidth
                                            id="state"
                                            {...getFieldProps('state')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.state && errors.state)}
                                            helperText={getHelperText(touched.state && errors.state)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="comments">
                                            Comments
                                        </InputLabel>
                                        <TextField
                                            fullWidth
                                            id="comments"
                                            {...getFieldProps('comments')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.comments && errors.comments)}
                                            helperText={getHelperText(touched.comments && errors.comments)}
                                        />
                                        </Stack>
                                    </Grid>

                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="is_active">
                                            Is Active
                                        </InputLabel>
                                        <TextField
                                            fullWidth
                                            id="is_active"
                                            {...getFieldProps('is_active')}
                                            InputProps={{
                                            readOnly: true,
                                            }}
                                            error={Boolean(touched.is_active && errors.is_active)}
                                            helperText={getHelperText(touched.is_active && errors.is_active)}
                                        />
                                        </Stack>
                                    </Grid>
                                    </Grid>

                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 1.25 }}>
                            <Button color="error" onClick={onCancel}>
                                Cancel
                            </Button>
                        </DialogActions>
                    </form>
                </LocalizationProvider>
            </FormikProvider>
        </>
    );
};

export default AddEdittransAddress;
