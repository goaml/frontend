//import { useState } To 'react';

// material-ui
import {
    //Autocomplete,
    Button,
    Checkbox,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    // FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField,
} from '@mui/material';
// import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { FormikProvider, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// types

export interface FormikValues {
    transactionId?: number;
    transactionNumber?: string;
    internalRefNumber?: string;
    transactionLocation?: string;
    transactionDescription?: string;
    dateTransaction?: Date;
    teller?: string;
    authorized?: boolean;
    lateDeposit?: boolean;
    datePosting?: Date;
    valueDate?: Date;
    conductionTypeId?: number; // transmode_code
    transmodeComment?: string;
    amountLocal?: number;
    goodsServices?: string;
    comments?: string;
    statusId?: number;
    isActive?: boolean;
    reportId?: number;
}

export interface Props {
    transTo?: any;
    onCancel: () => void;
}

const getInitialValues = (transTo: FormikValues | null) => {
    const newtransTo: FormikValues = {
        transactionId:undefined,
        transactionNumber: '',
        internalRefNumber: '',
        transactionLocation: '',
        transactionDescription: '',
        dateTransaction: undefined,
        teller: '',
        authorized: true,
        lateDeposit: true,
        datePosting: undefined,
        valueDate: undefined,
        conductionTypeId:undefined, // transmode_code
        transmodeComment: '',
        amountLocal:undefined,
        goodsServices: '',
        comments: '',
        statusId:undefined,
        isActive: true,
        reportId:undefined,
    };

    if (transTo) {
        return _.merge({}, newtransTo, transTo);
    }

    return newtransTo;
};

const AddEdittTrans = ({ transTo, onCancel }: Props) => {
    const transToSchema = Yup.object().shape({
        transactionId: Yup.number().required('Transaction ID is required'),
        transactionNumber: Yup.string().required('Transaction Number is required'),
        internalRefNumber: Yup.string().required('Internal Ref Number is required'),
        transactionLocation: Yup.string().required('Transaction Location is required'),
        transactionDescription: Yup.string().required('Transaction Description is required'),
        dateTransaction: Yup.date().required('Date Transaction is required'),
        teller: Yup.string().required('Teller is required'),
        authorized: Yup.boolean().required('Authorized is required'),
        lateDeposit: Yup.boolean().required('Late Deposit is required'),
        datePosting: Yup.date().required('Date Posting is required'),
        valueDate: Yup.date().required('Value Date is required'),
        conductionTypeId: Yup.number().required('Conduction Type ID (Transmode Code) is required'),
        transmodeComment: Yup.string().required('Transmode Comment is required'),
        amountLocal: Yup.number().required('Amount Local is required'),
        goodsServices: Yup.string().required('Goods/Services is required'),
        comments: Yup.string().required('Comments are required'),
        statusId: Yup.number().required('Status ID is required'),
        isActive: Yup.boolean().required('Is Active is required'),
        reportId: Yup.number().required('Report ID is required')
    });

    const formik = useFormik({
        initialValues: getInitialValues(transTo!),
        validationSchema: transToSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (transTo) {
                    // put request logic
                } else {
                    // post request logic
                }
                resetForm();
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, getFieldProps } = formik;

    const getHelperText = (fieldError: any): string | undefined => {
        return typeof fieldError === 'string' ? fieldError : undefined;
    };

    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{transTo ? 'View Transaction Details' : 'New transTo'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                    <Grid container spacing={3}>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="transactionId">Transaction ID<span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    disabled
                                                    placeholder='No data found'
                                                    id="transactionId"
                                                    {...getFieldProps('transactionId')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.transactionId && errors.transactionId)}
                                                    helperText={getHelperText(touched.transactionId && errors.transactionId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="transactionNumber">Transaction Number<span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    disabled
                                                    placeholder='No data found'
                                                    id="transactionNumber"
                                                    {...getFieldProps('transactionNumber')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.transactionNumber && errors.transactionNumber)}
                                                    helperText={getHelperText(touched.transactionNumber && errors.transactionNumber)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="internalRefNumber">Internal Ref Number<span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    disabled
                                                    placeholder='No data found'
                                                    id="internalRefNumber"
                                                    {...getFieldProps('internalRefNumber')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.internalRefNumber && errors.internalRefNumber)}
                                                    helperText={getHelperText(touched.internalRefNumber && errors.internalRefNumber)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="transactionLocation">Transaction Location<span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    disabled
                                                    placeholder='No data found'
                                                    id="transactionLocation"
                                                    {...getFieldProps('transactionLocation')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.transactionLocation && errors.transactionLocation)}
                                                    helperText={getHelperText(touched.transactionLocation && errors.transactionLocation)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="transactionDescription">Transaction Description<span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    disabled
                                                    placeholder='No data found'
                                                    id="transactionDescription"
                                                    {...getFieldProps('transactionDescription')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.transactionDescription && errors.transactionDescription)}
                                                    helperText={getHelperText(touched.transactionDescription && errors.transactionDescription)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="dateTransaction">Date Transaction<span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    disabled
                                                    placeholder='No data found'
                                                    id="dateTransaction"
                                                    type="date"
                                                    {...getFieldProps('dateTransaction')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.dateTransaction && errors.dateTransaction)}
                                                    helperText={getHelperText(touched.dateTransaction && errors.dateTransaction)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="teller">Teller<span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    disabled
                                                    placeholder='No data found'
                                                    id="teller"
                                                    {...getFieldProps('teller')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.teller && errors.teller)}
                                                    helperText={getHelperText(touched.teller && errors.teller)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="authorized">Authorized<span style={{ color: 'red' }}>*</span></InputLabel>
                                                <Checkbox
                                                    // fullWidth
                                                    disabled
                                                    placeholder='No data found'
                                                    id="authorized"
                                                    // type="checkbox"
                                                    {...getFieldProps('authorized')}
                                                    // InputProps={{
                                                    //     readOnly: false,
                                                    // }}
                                                    // error={Boolean(touched.authorized && errors.authorized)}
                                                    // helperText={getHelperText(touched.authorized && errors.authorized)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="lateDeposit">Late Deposit<span style={{ color: 'red' }}>*</span></InputLabel>
                                                <Checkbox
                                                    // fullWidth
                                                    disabled
                                                    placeholder='No data found'
                                                    id="lateDeposit"
                                                    // type="checkbox"
                                                    {...getFieldProps('lateDeposit')}
                                                    // InputProps={{
                                                    //     readOnly: false,
                                                    // }}
                                                    // error={Boolean(touched.lateDeposit && errors.lateDeposit)}
                                                    // helperText={getHelperText(touched.lateDeposit && errors.lateDeposit)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="datePosting">Date Posting<span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    disabled
                                                    placeholder='No data found'
                                                    id="datePosting"
                                                    type="date"
                                                    {...getFieldProps('datePosting')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.datePosting && errors.datePosting)}
                                                    helperText={getHelperText(touched.datePosting && errors.datePosting)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="valueDate">Value Date<span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    disabled
                                                    placeholder='No data found'
                                                    id="valueDate"
                                                    type="date"
                                                    {...getFieldProps('valueDate')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.valueDate && errors.valueDate)}
                                                    helperText={getHelperText(touched.valueDate && errors.valueDate)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="conductionTypeId">Conduction Type ID<span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    disabled
                                                    placeholder='No data found'
                                                    id="conductionTypeId"
                                                    {...getFieldProps('conductionTypeId')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.conductionTypeId && errors.conductionTypeId)}
                                                    helperText={getHelperText(touched.conductionTypeId && errors.conductionTypeId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="transmodeComment">Transmode Comment<span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    disabled
                                                    placeholder='No data found'
                                                    id="transmodeComment"
                                                    {...getFieldProps('transmodeComment')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.transmodeComment && errors.transmodeComment)}
                                                    helperText={getHelperText(touched.transmodeComment && errors.transmodeComment)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="amountLocal">Amount Local<span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    disabled
                                                    placeholder='No data found'
                                                    id="amountLocal"
                                                    type="number"
                                                    {...getFieldProps('amountLocal')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.amountLocal && errors.amountLocal)}
                                                    helperText={getHelperText(touched.amountLocal && errors.amountLocal)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="goodsServices">Goods Services<span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    disabled
                                                    placeholder='No data found'
                                                    id="goodsServices"
                                                    {...getFieldProps('goodsServices')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.goodsServices && errors.goodsServices)}
                                                    helperText={getHelperText(touched.goodsServices && errors.goodsServices)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="comments">Comments<span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    disabled
                                                    placeholder='No data found'
                                                    id="comments"
                                                    {...getFieldProps('comments')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.comments && errors.comments)}
                                                    helperText={getHelperText(touched.comments && errors.comments)}
                                                />
                                            </Stack>
                                        </Grid>

                                        <Grid item xs={6}>
                                            <Stack spacing={
                                                1.25}>
                                                <InputLabel htmlFor="isActive">Is Active<span style={{ color: 'red' }}>*</span></InputLabel>
                                                <Checkbox
                                                    // fullWidth
                                                    disabled
                                                    placeholder='No data found'
                                                    id="isActive"
                                                    // type="checkbox"
                                                    {...getFieldProps('isActive')}
                                                    // InputProps={{
                                                    //     readOnly: false,
                                                    // }}
                                                    // error={Boolean(touched.isActive && errors.isActive)}
                                                    // helperText={getHelperText(touched.isActive && errors.isActive)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="reportId">Report ID<span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    disabled
                                                    placeholder='No data found'
                                                    id="reportId"
                                                    {...getFieldProps('reportId')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.reportId && errors.reportId)}
                                                    helperText={getHelperText(touched.reportId && errors.reportId)}
                                                />
                                            </Stack>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 1.25 }}>
                            <Button color="error" onClick={onCancel}>
                                Cancel
                            </Button>
                        </DialogActions>
                    </form>
                </LocalizationProvider>
            </FormikProvider>
        </>
    );
};

export default AddEdittTrans;
