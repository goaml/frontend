//import { useState } To 'react';

// material-ui
import {
    //Autocomplete,
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    // FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField,
   // Tooltip
} from '@mui/material';
// import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { FormikProvider, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// types
import { dataProps } from 'pages/transaction/trans-to/types/types';

export interface FormikValues {
    entityMyClientId?: number;
    name?: string;
    commercialName?: string;
    entityLegalFormTypeId?: number;
    incorporationNumber?: string;
    business?: string;
    phones?: string;
    addresses?: string;
    email?: string;
    url?: string;
    incorporationState?: string;
    countryCodeId?: number;
    personMyClientId?: number;
    entityPersonRoleTypeId?: number;
    incorporationDate?: Date;
    businessClosed?: boolean;
    dateBusinessClosed?: Date;
    taxNumber?: string;
    taxRegNumber?: string;
    comments?: string;
    statusId?: number;
    isActive?: boolean;
}

export interface Props {
    TEntityMyclient?: dataProps;
    onCancel: () => void;
}

const getInitialValues = (TEntityMyclient: FormikValues | null) => {
    const newTEntityMyclient: FormikValues = {
        entityMyClientId: 0,
        name: '',
        commercialName: '',
        entityLegalFormTypeId: 0,
        incorporationNumber: '',
        business: '',
        phones: '',
        addresses: '',
        email: '',
        url: '',
        incorporationState: '',
        countryCodeId: 0,
        personMyClientId: 0,
        entityPersonRoleTypeId: 0,
        incorporationDate:new Date(),
        businessClosed: true,
        dateBusinessClosed:new Date(),
        taxNumber: '',
        taxRegNumber: '',
        comments: '',
        statusId: 0,
        isActive: true,
    };

    if (TEntityMyclient) {
        return _.merge({}, newTEntityMyclient, TEntityMyclient);
    }

    return newTEntityMyclient;
};

const AddEditTEntityMyclient = ({ TEntityMyclient, onCancel }: Props) => {
    const TEntityMyclientSchema = Yup.object().shape({
        toId: Yup.number().required('To ID is required'),
        toFundsCode: Yup.string().required('To Funds Code is required'),
        toFundsComment: Yup.string().required('To Funds Comment is required'),
        toForeignCurrency: Yup.string().required('To Foreign Currency is required'),
        tConductor: Yup.string().required('T Conductor is required'),
        toAccount: Yup.string().required('To Account is required'),
        toPerson: Yup.string().required('To Person is required'),
        toEntity: Yup.string().required('To Entity is required'),
        toCountry: Yup.string().required('To Country is required'),
        statusId: Yup.number().required('Status ID is required'),
        isActive: Yup.boolean().required('Is Active is required'),
        transactionId: Yup.number().required('Transaction ID is required'),
    });

    const formik = useFormik({
        initialValues: getInitialValues(TEntityMyclient!),
        validationSchema: TEntityMyclientSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (TEntityMyclient) {
                    // put request logic
                } else {
                    // post request logic
                }
                resetForm();
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, getFieldProps } = formik;

    const getHelperText = (fieldError: any): string | undefined => {
        return typeof fieldError === 'string' ? fieldError : undefined;
    };

    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{TEntityMyclient ? 'View Transaction Reciever Details' : 'New TEntityMyclient'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                <Grid container spacing={3}>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="entityMyClientId">Entity My Client ID</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="entityMyClientId"
                                        {...getFieldProps('entityMyClientId')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.entityMyClientId && errors.entityMyClientId)}
                                        helperText={getHelperText(touched.entityMyClientId && errors.entityMyClientId)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="name">Name</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="name"
                                        {...getFieldProps('name')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.name && errors.name)}
                                        helperText={getHelperText(touched.name && errors.name)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="commercialName">Commercial Name</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="commercialName"
                                        {...getFieldProps('commercialName')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.commercialName && errors.commercialName)}
                                        helperText={getHelperText(touched.commercialName && errors.commercialName)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="entityLegalFormTypeId">Entity Legal Form Type ID</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="entityLegalFormTypeId"
                                        {...getFieldProps('entityLegalFormTypeId')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.entityLegalFormTypeId && errors.entityLegalFormTypeId)}
                                        helperText={getHelperText(touched.entityLegalFormTypeId && errors.entityLegalFormTypeId)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="incorporationNumber">Incorporation Number</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="incorporationNumber"
                                        {...getFieldProps('incorporationNumber')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.incorporationNumber && errors.incorporationNumber)}
                                        helperText={getHelperText(touched.incorporationNumber && errors.incorporationNumber)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="business">Business</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="business"
                                        {...getFieldProps('business')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.business && errors.business)}
                                        helperText={getHelperText(touched.business && errors.business)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="phones">Mobile Number</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="phones"
                                        {...getFieldProps('phones')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.phones && errors.phones)}
                                        helperText={getHelperText(touched.phones && errors.phones)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="addresses">Addresses</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="addresses"
                                        {...getFieldProps('addresses')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.addresses && errors.addresses)}
                                        helperText={getHelperText(touched.addresses && errors.addresses)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="email">Email</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="email"
                                        {...getFieldProps('email')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.email && errors.email)}
                                        helperText={getHelperText(touched.email && errors.email)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="url">URL</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="url"
                                        {...getFieldProps('url')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.url && errors.url)}
                                        helperText={getHelperText(touched.url && errors.url)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="incorporationState">Incorporation State</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="incorporationState"
                                        {...getFieldProps('incorporationState')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.incorporationState && errors.incorporationState)}
                                        helperText={getHelperText(touched.incorporationState && errors.incorporationState)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="countryCodeId">Country Code ID</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="countryCodeId"
                                        {...getFieldProps('countryCodeId')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.countryCodeId && errors.countryCodeId)}
                                        helperText={getHelperText(touched.countryCodeId && errors.countryCodeId)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="personMyClientId">Person My Client ID</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="personMyClientId"
                                        {...getFieldProps('personMyClientId')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.personMyClientId && errors.personMyClientId)}
                                        helperText={getHelperText(touched.personMyClientId && errors.personMyClientId)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="entityPersonRoleTypeId">Entity Person Role Type ID</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="entityPersonRoleTypeId"
                                        {...getFieldProps('entityPersonRoleTypeId')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.entityPersonRoleTypeId && errors.entityPersonRoleTypeId)}
                                        helperText={getHelperText(touched.entityPersonRoleTypeId && errors.entityPersonRoleTypeId)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="incorporationDate">Incorporation Date</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="incorporationDate"
                                        {...getFieldProps('incorporationDate')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.incorporationDate && errors.incorporationDate)}
                                        helperText={getHelperText(touched.incorporationDate && errors.incorporationDate)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="businessClosed">Business Closed</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="businessClosed"
                                        {...getFieldProps('businessClosed')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.businessClosed && errors.businessClosed)}
                                        helperText={getHelperText(touched.businessClosed && errors.businessClosed)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="dateBusinessClosed">Date Business Closed</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="dateBusinessClosed"
                                        {...getFieldProps('dateBusinessClosed')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.dateBusinessClosed && errors.dateBusinessClosed)}
                                        helperText={getHelperText(touched.dateBusinessClosed && errors.dateBusinessClosed)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="taxNumber">Tax Number</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="taxNumber"
                                        {...getFieldProps('taxNumber')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.taxNumber && errors.taxNumber)}
                                        helperText={getHelperText(touched.taxNumber && errors.taxNumber)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="taxRegNumber">Tax Registration Number</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="taxRegNumber"
                                        {...getFieldProps('taxRegNumber')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.taxRegNumber && errors.taxRegNumber)}
                                        helperText={getHelperText(touched.taxRegNumber && errors.taxRegNumber)}
                                    />
                                    </Stack>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="comments">Comments</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="comments"
                                        {...getFieldProps('comments')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.comments && errors.comments)}
                                        helperText={getHelperText(touched.comments && errors.comments)}
                                    />
                                    </Stack>
                                </Grid>

                                <Grid item xs={6}>
                                    <Stack spacing={1.25}>
                                    <InputLabel htmlFor="isActive">Is Active</InputLabel>
                                    <TextField
                                        fullWidth
                                        id="isActive"
                                        {...getFieldProps('isActive')}
                                        InputProps={{
                                        readOnly: true,
                                        }}
                                        error={Boolean(touched.isActive && errors.isActive)}
                                        helperText={getHelperText(touched.isActive && errors.isActive)}
                                    />
                                    </Stack>
                                </Grid>
                                </Grid>

                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 1.25 }}>
                            <Button color="error" onClick={onCancel}>
                                Cancel
                            </Button>
                        </DialogActions>
                    </form>
                </LocalizationProvider>
            </FormikProvider>
        </>
    );
};

export default AddEditTEntityMyclient;
