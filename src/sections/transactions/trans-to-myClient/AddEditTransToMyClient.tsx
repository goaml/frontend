//import { useState } To 'react';

// material-ui
import {
    //Autocomplete,
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    // FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField,
   // Tooltip
} from '@mui/material';
// import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { FormikProvider, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// types
import { dataProps } from 'pages/transaction/trans-to-myClient/types/types';

export interface FormikValues {
    toMyClientId?:number,
    toFundsComment?:string,
    toForeignCurrencyId?:number,
    conductorPersonMyClientId?:number,
    toAccountMyClientId?:number,
    toPersonMyClientId?:number,
    toEntityMyClientId?:number,
    transactionId?:number,
    isActive?:boolean,
    rfundsType?:{
        fundTypeCode?: string,
    },
    rcountryCodes?:{
        description?:string,
    }
    rstatus?:{
        statusId?:number,
        isActive?:boolean,
        statusCode?:string,
    }
}

export interface Props {
    transToMyClient?: dataProps;
    onCancel: () => void;
}

const getInitialValues = (transToMyClient: FormikValues | null) => {
    const newtransTo: FormikValues = {
        toMyClientId:0,
        toFundsComment:'',
        toForeignCurrencyId:0,
        conductorPersonMyClientId:0,
        toAccountMyClientId:0,
        toPersonMyClientId:0,
        toEntityMyClientId:0,
        transactionId:0,
        isActive:true,
        rfundsType:{
            fundTypeCode: '',
        },
        rcountryCodes:{
            description:'',
        },
        rstatus:{
            statusId:0,
            isActive:true,
            statusCode:'',
        }
    };

    if (transToMyClient) {
        return _.merge({}, newtransTo, transToMyClient);
    }

    return newtransTo;
};

const AddEdittransToMyClient = ({ transToMyClient, onCancel }: Props) => {
    const transToSchema = Yup.object().shape({
        toMyClientId: Yup.number().required('To ID is required'),
        toFundsCode: Yup.string().required('To Funds Code is required'),
        toFundsComment: Yup.string().required('To Funds Comment is required'),
        toForeignCurrency: Yup.string().required('To Foreign Currency is required'),
        tConductor: Yup.string().required('T Conductor is required'),
        toAccount: Yup.string().required('To Account is required'),
        toPerson: Yup.string().required('To Person is required'),
        toEntity: Yup.string().required('To Entity is required'),
        toCountry: Yup.string().required('To Country is required'),
        statusId: Yup.number().required('Status ID is required'),
        isActive: Yup.boolean().required('Is Active is required'),
        transactionId: Yup.number().required('Transaction ID is required'),
    });

    const formik = useFormik({
        initialValues: getInitialValues(transToMyClient!),
        validationSchema: transToSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (transToMyClient) {
                    // put request logic
                } else {
                    // post request logic
                }
                resetForm();
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, getFieldProps } = formik;

    const getHelperText = (fieldError: any): string | undefined => {
        return typeof fieldError === 'string' ? fieldError : undefined;
    };

    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{transToMyClient ? 'View transaction Reciever Details' : 'New transTo'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                    <Grid container spacing={3}>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="toMyClientId">To My Client ID</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="toMyClientId"
                                                    {...getFieldProps('toId')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.toMyClientId && errors.toMyClientId)}
                                                    helperText={getHelperText(touched.toMyClientId && errors.toMyClientId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="rfundsType.fundTypeCode">To Funds Code</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="rfundsType.fundTypeCode"
                                                    {...getFieldProps('rfundsType.fundTypeCode')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.rfundsType && errors.rfundsType)}
                                                    helperText={getHelperText(touched.rfundsType && errors.rfundsType)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="toFundsComment">To Funds Comment </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="toFundsComment"
                                                    {...getFieldProps('toFundsComment')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.toFundsComment && errors.toFundsComment)}
                                                    helperText={getHelperText(touched.toFundsComment && errors.toFundsComment)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="toForeignCurrencyId">To Foreign Currency </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="toForeignCurrencyId"
                                                    {...getFieldProps('toForeignCurrencyId')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.toForeignCurrencyId && errors.toForeignCurrencyId)}
                                                    helperText={getHelperText(touched.toForeignCurrencyId && errors.toForeignCurrencyId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="conductorPersonMyClientId">T Conductor </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="conductorPersonMyClientId"
                                                    {...getFieldProps('conductorPersonMyClientId')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.conductorPersonMyClientId && errors.conductorPersonMyClientId)}
                                                    helperText={getHelperText(touched.conductorPersonMyClientId && errors.conductorPersonMyClientId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="toAccountMyClientId">To Account </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="toAccountMyClientId"
                                                    {...getFieldProps('toAccountMyClientId')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.toAccountMyClientId && errors.toAccountMyClientId)}
                                                    helperText={getHelperText(touched.toAccountMyClientId && errors.toAccountMyClientId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="toPersonMyClientId">To Person </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="toPersonMyClientId"
                                                    {...getFieldProps('toPersonMyClientId')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.toPersonMyClientId && errors.toPersonMyClientId)}
                                                    helperText={getHelperText(touched.toPersonMyClientId && errors.toPersonMyClientId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="toEntityMyClientId">To Entity </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="toEntityMyClientId"
                                                    {...getFieldProps('toEntityMyClientId')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.toEntityMyClientId && errors.toEntityMyClientId)}
                                                    helperText={getHelperText(touched.toEntityMyClientId && errors.toEntityMyClientId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="rcountryCodes.description">To Country </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="rcountryCodes.description"
                                                    {...getFieldProps('rcountryCodes.description')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.rcountryCodes && errors.rcountryCodes)}
                                                    helperText={getHelperText(touched.rcountryCodes && errors.rcountryCodes)}
                                                />
                                            </Stack>
                                        </Grid>

                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="isActive">Is Active </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="isActive"
                                                    {...getFieldProps('isActive')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.isActive && errors.isActive)}
                                                    helperText={getHelperText(touched.isActive && errors.isActive)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="transactionId">Transaction ID </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="transactionId"
                                                    {...getFieldProps('transactionId')}
                                                    InputProps={{
                                                        readOnly: false,
                                                    }}
                                                    error={Boolean(touched.transactionId && errors.transactionId)}
                                                    helperText={getHelperText(touched.transactionId && errors.transactionId)}
                                                />
                                            </Stack>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 1.25 }}>
                            <Button color="error" onClick={onCancel}>
                                Cancel
                            </Button>
                        </DialogActions>
                    </form>
                </LocalizationProvider>
            </FormikProvider>
        </>
    );
};

export default AddEdittransToMyClient;
