// material-ui
import {
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    Grid,
    InputLabel,
    Stack,
    TextField,
    // Tooltip
} from '@mui/material';

import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { FormikProvider, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// types
import { dataProps } from 'pages/transaction/trans-personIdentity/types/types';

export interface FormikValues {
    personIdentificationId?: number;
    identifierTypeId?: number;
    number?: string;
    issueDate?: string;
    expiryDate?: string;
    issuedBy?: string;
    issueCountryCodeId?: number;
    comments?: string;
    status_id?: number;
    isActive?: boolean;
}

export interface Props {
    transPersonId?: dataProps;
    onCancel: () => void;
}

const getInitialValues = (transPersonId: FormikValues | null) => {
    const newtransPersonId: FormikValues = {
        personIdentificationId: 0,
        identifierTypeId: 0,
        number: '',
        issueDate: '',
        expiryDate: '',
        issuedBy: '',
        issueCountryCodeId: 0,
        comments: '',
        status_id: 0,
        isActive: true,
    };

    if (transPersonId) {
        return _.merge({}, newtransPersonId, transPersonId);
    }

    return newtransPersonId;
};

const AddEdittransPersonId = ({ transPersonId, onCancel }: Props) => {
    const transPersonIdSchema = Yup.object().shape({
    });

    console.log('transPersonId', transPersonId);

    const formik = useFormik({
        initialValues: getInitialValues(transPersonId!),
        validationSchema: transPersonIdSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (transPersonId) {
                    // put request logic
                } else {
                    // post request logic
                }
                resetForm();
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, getFieldProps } = formik;

    const getHelperText = (fieldError: any): string | undefined => {
        return typeof fieldError === 'string' ? fieldError : undefined;
    };

    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{transPersonId ? 'View Transaction Person Identification Details' : 'New transPersonId'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                    <Grid container spacing={3}>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="personIdentificationId">Person Identification ID</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="personIdentificationId"
                                                    {...getFieldProps('personIdentificationId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.personIdentificationId && errors.personIdentificationId)}
                                                    helperText={getHelperText(touched.personIdentificationId && errors.personIdentificationId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="identifierTypeId">Identifier Type ID</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="identifierTypeId"
                                                    {...getFieldProps('identifierTypeId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.identifierTypeId && errors.identifierTypeId)}
                                                    helperText={getHelperText(touched.identifierTypeId && errors.identifierTypeId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="number">Number</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="number"
                                                    {...getFieldProps('number')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.number && errors.number)}
                                                    helperText={getHelperText(touched.number && errors.number)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="issueDate">Issue Date</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="issueDate"
                                                    {...getFieldProps('issueDate')}
                                                    value={formik.values.issueDate
                                                        ? new Date(formik.values.issueDate).toISOString().split('T')[0]
                                                        : ''}
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.issueDate && errors.issueDate)}
                                                    helperText={getHelperText(touched.issueDate && errors.issueDate)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="expiryDate">Expiry Date</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="expiryDate"
                                                    {...getFieldProps('expiryDate')}
                                                    value={formik.values.expiryDate
                                                        ? new Date(formik.values.expiryDate).toISOString().split('T')[0]
                                                        : ''}
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.expiryDate && errors.expiryDate)}
                                                    helperText={getHelperText(touched.expiryDate && errors.expiryDate)}
                                                />
                                            </Stack>
                                        </Grid>

                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="issuedBy">Issued By</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="issuedBy"
                                                    {...getFieldProps('issuedBy')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.issuedBy && errors.issuedBy)}
                                                    helperText={getHelperText(touched.issuedBy && errors.issuedBy)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="issueCountryCodeId">Country Code ID</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="issueCountryCodeId"
                                                    {...getFieldProps('issueCountryCodeId')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.issueCountryCodeId && errors.issueCountryCodeId)}
                                                    helperText={getHelperText(touched.issueCountryCodeId && errors.issueCountryCodeId)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="comments">Comments</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="comments"
                                                    {...getFieldProps('comments')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.comments && errors.comments)}
                                                    helperText={getHelperText(touched.comments && errors.comments)}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="isActive">Is Active</InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="isActive"
                                                    {...getFieldProps('isActive')}
                                                    InputProps={{
                                                        readOnly: true,
                                                    }}
                                                    error={Boolean(touched.isActive && errors.isActive)}
                                                    helperText={getHelperText(touched.isActive && errors.isActive)}
                                                />
                                            </Stack>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 1.25 }}>
                            <Button color="error" onClick={onCancel}>
                                Cancel
                            </Button>
                        </DialogActions>
                    </form>
                </LocalizationProvider>
            </FormikProvider>
        </>
    );
};

export default AddEdittransPersonId;
