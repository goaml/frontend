import { Box } from '@mui/material';
import { useTheme } from '@mui/material/styles';
import useConfig from '../../hooks/useConfig';
import { useEffect, useState } from 'react';
import ReactApexChart from 'react-apexcharts';
import { ApexOptions } from 'apexcharts';
import { XmlFileLog } from 'types/xml-file-log';

const monthNames = [
  'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
  'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
];


const processData = (data: XmlFileLog[]) => {
  const groupedData: Record<string, Record<string, XmlFileLog>> = {};

  data.forEach((item) => {
    const { processYear, processMonth, reportType } = item;
    const yearMonthKey = `${processYear}-${processMonth}`;

    if (!groupedData[yearMonthKey]) groupedData[yearMonthKey] = {};

    if (
      !groupedData[yearMonthKey][reportType] ||
      groupedData[yearMonthKey][reportType].logId < item.logId
    ) {
      groupedData[yearMonthKey][reportType] = item;
    }
  });

  const allYearMonthKeys = Object.keys(groupedData).sort();
  const seriesData: Record<string, { x: string; y: number; recordCount: number }[]> = {};

  allYearMonthKeys.forEach((yearMonth) => {
    Object.entries(groupedData[yearMonth]).forEach(([reportType, item]) => {
      if (!seriesData[reportType]) seriesData[reportType] = [];
      const [year, month] = yearMonth.split('-');
      seriesData[reportType].push({
        x: `${monthNames[parseInt(month, 10) - 1]} ${year}`, // Convert month to name
        y: item.totalTranAmt,
        recordCount: item.recordCount,
      });
    });
  });

  const allMonthsWithNames = allYearMonthKeys.map((key) => {
    const [year, month] = key.split('-');
    return `${monthNames[parseInt(month, 10) - 1]} ${year}`;
  });

  const series = Object.keys(seriesData).map((key) => ({
    name: key,
    data: allMonthsWithNames.map((monthWithName) => {
      const dataPoint = seriesData[key].find((d) => d.x === monthWithName);
      return dataPoint || { x: monthWithName, y: 0, recordCount: 0 };
    }),
  }));

  return { months: allMonthsWithNames, series };
};

const ApexColumnChart = ({ apiData }: { apiData: XmlFileLog[] }) => {
  const theme = useTheme();
  const { mode } = useConfig();

  const { primary } = theme.palette.text;
  const line = theme.palette.divider;
  const grey200 = theme.palette.grey[200];

  const secondary = theme.palette.primary[700];
  const primaryMain = theme.palette.primary.main;
  const successDark = theme.palette.success.main;

  const [chartOptions, setChartOptions] = useState<ApexOptions>({});
  const [chartSeries, setChartSeries] = useState<{ name: string; data: { x: string; y: number; recordCount: number }[] }[]>([]);

  useEffect(() => {
    if (apiData && Array.isArray(apiData)) {
      const { months, series } = processData(apiData);
 

      setChartSeries(series);
      setChartOptions({
        chart: {
          type: 'bar',
          height: 350,
          stacked: false,
        },
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: '80%',
            barHeight: 'full',
            dataLabels: {
              position: 'top',
              orientation: 'horizontal'
            }
          },
        },
        xaxis: {
          categories: months, // Use formatted months with year
          labels: {
            style: {
              colors: Array(months.length).fill(primary),
            },
          },
        },
        yaxis: {
          title: {
            text: 'Total Transaction Amount',
          },
          labels: {
            style: {
              colors: [primary], // Replace 'primary' with the actual color value or variable
            },
            formatter: function (value) {
              if (value >= 1_000_000_000) {
                return (value / 1_000_000_000).toFixed(2) + 'B'; // Display in billions
              } else if (value >= 1_000_000) {
                return (value / 1_000_000).toFixed(2) + 'M'; // Display in millions
              }
              return value.toFixed(2); // Display as is for smaller numbers
            },
          },
        },
        dataLabels: {
          enabled: true,
          offsetX: 0,
          offsetY: -15, // Adjust this value to control the position above the bar
          style: {
            colors: [mode === 'dark' ? '#FFFF00' : '#FF6500'], // Yellow for dark mode, Bright Green for light mode
            fontSize: '12px',
            fontWeight: 'bold',
          },
          formatter: function (value) {
            if (typeof value === 'number') {
              if (value >= 1_000_000_000) {
                return (value / 1_000_000_000).toFixed(2) + 'B'; // In billions
              } else if (value >= 1_000_000) {
                return (value / 1_000_000).toFixed(2) + 'M'; // In millions
              }
              return value.toFixed(2); // Less than a million
            }
            return value.toString(); // Ensure non-number values are returned as strings
          },
        },
        tooltip: {
          shared: false,
          intersect: true,
          custom: function ({
            series,
            seriesIndex,
            dataPointIndex,
            w,
          }) {
            const recordCount =
              w.config.series[seriesIndex].data[dataPointIndex].recordCount;
            const totalTranAmt = series[seriesIndex][dataPointIndex];
            const formattedTotalTranAmt = new Intl.NumberFormat('en-US', {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
            }).format(totalTranAmt);
            return `
              <div style="padding: 10px;">
                <strong>Report Type:</strong> ${w.globals.seriesNames[seriesIndex]}<br/>
                <strong>Total Tran Amt:</strong> ${formattedTotalTranAmt}<br/>
                <strong>Record Count:</strong> ${recordCount}
              </div>
            `;
          },
        },
        fill: {
          opacity: 1,
        },
        legend: {
          show: true,
          position: 'bottom',
          labels: {
            colors: 'grey.500',
          },
          markers: {
            width: 16,
            height: 16,
            radius: 5,
          },
          itemMargin: {
            horizontal: 15,
            vertical: 8,
          },
        },
        colors: [secondary, primaryMain, successDark, '#FFA500', '##FFFF00', "#7b0000", '#4CAF50'], // Add a unique color for CTR

        grid: {
          borderColor: line,
        },
        theme: {
          mode: mode === 'dark' ? 'dark' : 'light',
        },
      });
    } else {
      console.error("apiData is invalid or not an array:", apiData);
    }
  }, [apiData, mode, primary, line, grey200, secondary, primaryMain, successDark]);

  return (
    <Box id="chart" sx={{ bgcolor: 'transparent' }}>
      <ReactApexChart options={chartOptions} series={chartSeries} type="bar" height={350} />
    </Box>
  );
};

export default ApexColumnChart;
