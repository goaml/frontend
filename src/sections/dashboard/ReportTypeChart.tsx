import { useEffect, useState } from 'react';
import { Box } from '@mui/material';
import { useTheme } from '@mui/material/styles';
import ReactApexChart, { Props as ChartProps } from 'react-apexcharts';
import { useSelector } from 'react-redux';
import useConfig from 'hooks/useConfig';
import { ThemeMode } from 'types/config';

const columnChartOptions = {
  chart: {
    type: 'bar',
    height: 350
  },
  plotOptions: {
    bar: {
      horizontal: false,
      columnWidth: '35%',
      endingShape: 'rounded',
      borderRadius: 2,
      distributed: true  // Enable different colors for each bar
    }
  },
  dataLabels: {
    enabled: false
  },
  stroke: {
    show: true,
    width: 0.5,
    colors: ['transparent']
  },
  grid: {
    borderColor: '#e0e0e0',
    strokeDashArray: 3,
    xaxis: {
      lines: {
        show: true,
        lineWidth: 0.5
      }
    },
    yaxis: {
      lines: {
        show: true,
        lineWidth: 0.5
      }
    }
  },
  xaxis: {
    title: {
      text: 'Report Type',
    },
    categories: [],
    axisBorder: {
      show: true,
      height: 0.5
    },
    axisTicks: {
      show: true,
      height: 3,
      width: 0.5
    }
  },
  yaxis: {
    title: {
      text: 'Count',
    },
    fill: {
      opacity: 1
    },
    axisBorder: {
      show: true,
      width: 0.5
    },
    axisTicks: {
      show: true,
      width: 0.5
    }
  },
  fill: {
    opacity: 0.9
  },
  tooltip: {
    y: {
      formatter(val: number) {
        return `${val} counts`;
      }
    }
  },
  legend: {
    show: false  // Hide legend since we're using distributed colors
  },
  responsive: [
    {
      breakpoint: 600,
      options: {
        yaxis: {
          show: false
        }
      }
    }
  ]
};

const ReportTypeBarChart = () => {
  const theme = useTheme();
  const { mode } = useConfig();
  const { xmlFileLogList } = useSelector((state: { xmlFileLog: { xmlFileLogList: any } }) => state.xmlFileLog);

  const { primary } = theme.palette.text;
  const line = theme.palette.divider;

  const [series, setSeries] = useState<{ name: string; data: number[] }[]>([]);
  const [options, setOptions] = useState<ChartProps>(columnChartOptions);

  useEffect(() => {
    if (xmlFileLogList?.result) {
      const allItems = Array.isArray(xmlFileLogList.result) 
        ? xmlFileLogList.result 
        : xmlFileLogList.result.items || xmlFileLogList.result;

      const reportTypeCounts = allItems.reduce((acc: Record<string, number>, item: any) => {
        const reportType = item.reportType || 'Unknown';
        acc[reportType] = (acc[reportType] || 0) + 1;
        return acc;
      }, {});

      const categories = Object.keys(reportTypeCounts);
      const data = Object.values(reportTypeCounts).map(value => value as number);

      setSeries([
        {
          name: 'Report Types',
          data
        }
      ]);

      setOptions((prevState) => ({
        ...prevState,
        colors: [
          '#FF4560',  // Red for CTR
          '#00E396',  // Green for IFT
          '#008FFB',  // Blue for EFT
          '#775DD0'   // Purple for any other type
        ],
        xaxis: {
          ...prevState.xaxis,
          categories,
          labels: {
            style: {
              colors: categories.map(() => primary),
              fontSize: '11px'
            }
          }
        },
        grid: {
          borderColor: line,
          strokeDashArray: 3,
          xaxis: {
            lines: {
              show: true,
              lineWidth: 0.5
            }
          },
          yaxis: {
            lines: {
              show: true,
              lineWidth: 0.5
            }
          }
        },
        theme: {
          mode: mode === ThemeMode.DARK ? 'dark' : 'light'
        }
      }));
    }
  }, [xmlFileLogList, mode, primary, line]);

  return (
    <Box id="chart" sx={{ bgcolor: 'transparent' }}>
      <ReactApexChart options={options} series={series} type="bar" height={350} />
    </Box>
  );
};

export default ReportTypeBarChart;