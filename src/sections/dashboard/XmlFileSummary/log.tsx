/* eslint-disable prettier/prettier */
import { FilterTwoTone } from '@ant-design/icons';
// material ui
import {
    Dialog,
    IconButton, Stack,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Tooltip, useMediaQuery,
    useTheme
} from '@mui/material';

import { PopupTransition } from 'components/@extended/Transitions';
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
// third-party
import { EmptyTable, HeaderSort, TableRowSelection } from 'components/third-party/ReactTable';

import { Box } from '@mui/system';
import { TablePagination } from 'components/third-party/ReactTable2.0';
import { useEffect, useMemo, useState } from 'react';
import { Cell, Column, HeaderGroup, useExpanded, useFilters, useGlobalFilter, usePagination, useRowSelect, useSortBy, useTable } from 'react-table';
import AddEditTransactionsInq from 'sections/parameter-management/transacttion-inquiry/AddEditTransactionsInq';
import { dispatch, useSelector } from 'store';
import { fetchTransactionCodeList } from 'store/reducers/goaml-trx-code';
import { fetchRPTList } from 'store/reducers/rpt-code';
import { openSnackbar } from 'store/reducers/snackbar';
import { fetchSummaryLog, toInitialState } from 'store/reducers/xml-file-log';
import { listParametersType } from 'types/xml-file-log';
import {
    GlobalFilter
} from 'utils/react-table';
import FilterDialog from '../XmlFileSummary/FilterDialog';
import { dataProps, ReactTableProps, TableParamsType } from './type/types';
import { fetchReportCodeList } from 'store/reducers/report-type';

// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, handleAddEdit, getHeaderProps, pagination, tableParams, reportType }: ReactTableProps) {
    const theme = useTheme();

    const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));
    const sortBy = { id: 'logId', desc: true };

    const [filterDialogOpen, setFilterDialogOpen] = useState(false);

    const handleFilterDialogOpen = () => {
        setFilterDialogOpen(true);
    };

    const handleFilterDialogClose = () => {
        setFilterDialogOpen(false);
    };


    const handleApplyFilters = (filters: { [key: string]: string }) => {
        console.log(filters, 'filters');

        setFilterDialogOpen(false);
        tableParams.setPage(0);
        // tableParams.setExecuteTime(filters.executeTime);
        tableParams.setLogId(parseInt(filters.logId) || undefined);
        tableParams.setProcessMonth(filters.processMonth);
        tableParams.setProcessYear(filters.processYear);
        tableParams.setRecordCount(parseInt(filters.recordCount) || undefined);
        tableParams.setReportType(filters.reportType);
        tableParams.setExecuteTimeFrom(filters.executeTimeFrom);
        tableParams.setExecuteTimeTo(filters.executeTimeTo);
        dispatch(fetchSummaryLog({ ...filters, page: 0, per_page: 10, direction: 'asc', sort: 'logId' })); // Apply filters and reset pagination
    };

    const onReset = () => {
        // tableParams.setExecuteTime("");
        tableParams.setLogId(undefined);
        tableParams.setProcessMonth("");
        tableParams.setProcessYear("");
        tableParams.setRecordCount(undefined);
        tableParams.setReportType("");
        tableParams.setPage(0);
        tableParams.setExecuteTimeFrom("");
        tableParams.setExecuteTimeTo("");

        dispatch(fetchSummaryLog({ page: 0, per_page: 10, direction: 'asc', sort: 'logId' }));
    }

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        rows,
        page,
        gotoPage,
        setPageSize,
        state: { globalFilter, selectedRowIds, pageIndex, pageSize },
        preGlobalFilteredRows,
        setGlobalFilter,
    } = useTable(
        {
            columns,
            data,
            initialState: { pageIndex: tableParams.page, pageSize: 10, hiddenColumns: ['avatar'], sortBy: [sortBy] },
            manualPagination: true, // Enable manual pagination
            pageCount: Math.ceil(pagination.total / tableParams.perPage),
        },
        useGlobalFilter,
        useFilters,
        useSortBy,
        useExpanded,
        usePagination,
        useRowSelect
    );


    return (
        <>
            <TableRowSelection selected={Object.keys(selectedRowIds).length} />
            <Stack spacing={3}>
                <Stack
                    direction={matchDownSM ? 'column' : 'row'}
                    spacing={1}
                    justifyContent="space-between"
                    alignItems="center"
                    sx={{ p: 3, pb: 0 }}
                >
                    <GlobalFilter
                        preGlobalFilteredRows={preGlobalFilteredRows}
                        globalFilter={globalFilter}
                        setGlobalFilter={setGlobalFilter}
                        size="small"
                    />
                    <Stack direction={matchDownSM ? 'column' : 'row'} alignItems="center" spacing={1}>
                        <Tooltip title="Filter Columns">
                            <IconButton onClick={handleFilterDialogOpen}>
                                <FilterTwoTone />
                            </IconButton>
                        </Tooltip>
                    </Stack>
                </Stack>
                <Box sx={{ display: 'flex', flexDirection: 'column', height: '100%', width: '100%', justifyContent: 'space-between' }}>
                    <Box sx={{ flex: '1 1 auto', overflowY: 'auto' }}>
                        <Table {...getTableProps()}>
                            <TableHead>
                                {headerGroups.map((headerGroup: HeaderGroup<{}>) => (
                                    <TableRow
                                        {...headerGroup.getHeaderGroupProps()}
                                        sx={{ '& > th:first-of-type': { width: '58px' } }}
                                    >
                                        {headerGroup.headers.map((column: HeaderGroup) => (
                                            <TableCell
                                                {...column.getHeaderProps([{ className: column.className }, getHeaderProps(column)])}
                                            >
                                                <HeaderSort column={column} />
                                            </TableCell>
                                        ))}
                                    </TableRow>
                                ))}
                            </TableHead>
                            <TableBody {...getTableBodyProps()}>
                                {page.length > 0 ? (
                                    page.map((row, i) => {
                                        prepareRow(row);
                                        return (
                                            <TableRow {...row.getRowProps()}>
                                                {row.cells.map((cell: Cell) => (
                                                    <TableCell
                                                        {...cell.getCellProps([{ className: cell.column.className }])}
                                                    >
                                                        {cell.render('Cell')}
                                                    </TableCell>
                                                ))}
                                            </TableRow>
                                        );
                                    })
                                ) : (
                                    <EmptyTable msg="No Data" colSpan={12} />
                                )}
                            </TableBody>
                        </Table>
                    </Box>
                </Box>
                <Box sx={{ p: 1 }}>
                    <TablePagination
                        pageIndex={pageIndex}
                        pageSize={pageSize}
                        setPageSize={setPageSize}
                        gotoPage={gotoPage}
                        rows={rows}
                        totalRows={pagination.total}
                        tableParams={tableParams}
                    />
                </Box>

            </Stack>
            <FilterDialog
                open={filterDialogOpen}
                onClose={handleFilterDialogClose}
                onApplyFilters={handleApplyFilters}
                onReset={onReset}
                reportType={reportType}
                initialFilters={{
                    executeTimeFrom: tableParams.executeTimeFrom,
                    executeTimeTo: tableParams.executeTimeTo,
                    processYear: tableParams.processYear,
                    processMonth: tableParams.processMonth,
                    reportType: tableParams.reportType
                }}
            />

        </>
    );
}

// ==============================|| List ||============================== //

const XmlFileSummary = () => {
    // const theme = useTheme();

    const { xmlFileLogList, error, success, isLoading } = useSelector(state => state.xmlFileLog);

    const [data, setData] = useState<dataProps[]>([])


    // ==============================|| Pagination-Config ||============================== //
    const [Pagination, setPagination] = useState<any>([])
    const [page, setPage] = useState<number>(0);
    const [perPage, setPerPage] = useState<number>(10);
    const [direction, setDirection] = useState<"asc" | "desc">("asc");
    const [search, setSearch] = useState<string>("");
    const [sort, setSort] = useState<string>("logId");

    const [executeTimeFrom, setExecuteTimeFrom] = useState<string>("");
    const [executeTimeTo, setExecuteTimeTo] = useState<string>("");
    const [logId, setLogId] = useState<number>();
    const [processMonth, setProcessMonth] = useState<string>("");
    const [processYear, setProcessYear] = useState<string>("");
    const [recordCount, setRecordCount] = useState<number>();
    const [reportType, setReportType] = useState<string>("");
    const [status, setStatus] = useState<string>("");


    const tableParams: TableParamsType = {
        page,
        setPage,
        perPage,
        setPerPage,
        direction,
        setDirection,
        sort,
        setSort,
        search,
        setSearch,
        executeTimeFrom,
        setExecuteTimeFrom,
        executeTimeTo,
        setExecuteTimeTo,
        logId,
        setLogId,
        processMonth,
        setProcessMonth,
        processYear,
        setProcessYear,
        recordCount,
        setRecordCount,
        reportType,
        setReportType,
        status,
        setStatus
    }

    const columns = useMemo(
        () =>
            [
                {
                    Header: '#',
                    accessor: 'logId',
                    id: 'logId',
                    Cell: ({ value }) => (value ? value : '-')
                },
                {
                    Header: 'Year',
                    accessor: 'processYear',
                    id: 'processYear',
                    Cell: ({ value }) => (value ? value : '-')
                },
                {
                    Header: 'Month',
                    accessor: 'processMonth',
                    id: 'processMonth',
                    Cell: ({ value }) => {
                        if (!value) return '-';
                        const monthNames = [
                            'January', 'February', 'March', 'April', 'May', 'June',
                            'July', 'August', 'September', 'October', 'November', 'December'
                        ];
                        return monthNames[parseInt(value, 10) - 1] || '-';
                    },
                },
                {
                    Header: 'Report Type',
                    accessor: 'reportType',
                    id: 'reportType',
                    Cell: ({ value }) => (value ? value : '-')
                },
                {
                    Header: 'Record Count',
                    accessor: 'recordCount',
                    id: 'recordCount',
                    Cell: ({ value }) => {
                        if (value) {
                            const formattedAmount = value.toLocaleString('en-US');
                            return <div style={{ textAlign: 'right' }}>{formattedAmount}</div>;
                        }
                        return <div style={{ textAlign: 'right' }}>-</div>;
                    },
                },
                {
                    Header: 'Value',
                    accessor: 'totalTranAmt',
                    id: 'totalTranAmt',
                    Cell: ({ value }) => {
                        if (value) {
                            const formattedAmount = value.toLocaleString('en-US', { minimumFractionDigits: 2 });
                            return <div style={{ textAlign: 'right' }}>{formattedAmount}</div>;
                        }
                        return <div style={{ textAlign: 'right' }}>-</div>;
                    },
                },
            ] as Column[],
        [tableParams.page, tableParams.perPage]
    );

    const [addEdit, setAddEdit] = useState<boolean>(false);
    const [user, setUser] = useState<dataProps>();

    const handleAddEdit = () => {
        setAddEdit(!addEdit);
        if (user && !addEdit) setUser(undefined);

    };

    // ==============================|| API-Config ||============================== //
    useEffect(() => {
        dispatch(fetchTransactionCodeList());
        dispatch(fetchRPTList())
    }, [])


    useEffect(() => {
        const listParameters: listParametersType = {
            page: page,
            per_page: perPage,
            direction: 'desc',
            search: search,
            sort: 'logId',
            executeTimeFrom: executeTimeFrom,
            executeTimeTo: executeTimeTo,
            logId: logId,
            processMonth: processMonth,
            processYear: processYear,
            recordCount: recordCount,
            reportType: reportType,
            status: status
        };
        dispatch(fetchSummaryLog(listParameters));
    }, [dispatch, success, page, perPage, direction, search, sort]);

    useEffect(() => {
        setData(xmlFileLogList?.result! || []);
        setPagination(xmlFileLogList?.pagination! || {})
    }, [xmlFileLogList])

    useEffect(() => {
        if (error != null && typeof error === 'object' && 'message' in error) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: (error as { message: string }).message, // Type assertion
                    variant: 'alert',
                    alert: {
                        color: 'error'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [error]);

    //  handel success
    useEffect(() => {
        if (success != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: success,
                    variant: 'alert',
                    alert: {
                        color: 'success'
                    },
                    close: true
                })
            );
            dispatch(toInitialState())
        }
    }, [success])

    const { selectreportCode } = useSelector((state) => state.reportType);

    useEffect(() => {
        dispatch(fetchReportCodeList()); // Fetch report types on component mount
    }, [dispatch]);

    if (isLoading) {
        return <div>Loading...</div>;
    }



    return (
        <>
            <MainCard content={false}>
                <ScrollX>
                    <ReactTable columns={columns}
                        getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()}
                        data={data} handleAddEdit={handleAddEdit}
                        tableParams={tableParams} pagination={Pagination}
                        reportType={selectreportCode!}

                    />
                </ScrollX>
                {/* add / edit user dialog */}
                <Dialog
                    maxWidth="sm"
                    TransitionComponent={PopupTransition}
                    keepMounted
                    fullWidth
                    onClose={handleAddEdit}
                    open={addEdit}
                    sx={{ '& .MuiDialog-paper': { p: 0 }, transition: 'transform 225ms' }}
                    aria-describedby="alert-dialog-slide-description"
                >
                    <AddEditTransactionsInq user={user} onCancel={handleAddEdit} />
                </Dialog>
            </MainCard>
        </>
    )


};



export default XmlFileSummary
