import {
  Autocomplete,
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  FormControlLabel,
  Grid,
  InputLabel,
  Stack,
  TextField
} from '@mui/material';
import { useFormik } from 'formik'; // Import Formik for form handling
import React, { useEffect, useState } from 'react';
import { ReportCodeType } from 'types/report-type';
import * as Yup from 'yup'; // Import Yup for validation
interface FilterValues {
  reportType?: string;
  processYear?: string;
  executeTimeFrom?: string;
  executeTimeTo?: string;
  processMonth?: string;
}

interface FilterDialogProps {
  open: boolean;
  onClose: () => void;
  onApplyFilters: (filters: { [key: string]: string }) => void;
  onReset: () => void;
  initialFilters: FilterValues;
  reportType: ReportCodeType[] | null;
}

// Define a simplified Yup validation schema
const validationSchema = Yup.object().shape({
  executeTimeFrom: Yup.date().nullable(),
  executeTimeTo: Yup.date()
    .nullable()
    .min(Yup.ref('executeTimeFrom'), 'End date can\'t be before start date'),
  processYear: Yup.number().nullable(),
  processMonth: Yup.string(),
  reportType: Yup.string(),
});

const FilterDialog: React.FC<FilterDialogProps> = ({
  open,
  onClose,
  onApplyFilters,
  onReset,
  initialFilters,
  reportType
}) => {
  const [selectedColumns, setSelectedColumns] = useState<string[]>([]);
  // Track multiple selected columns
  useEffect(() => {
    const initialSelectedColumns: string[] = [];
    if (initialFilters.executeTimeFrom || initialFilters.executeTimeTo) initialSelectedColumns.push('Date');
    if (initialFilters.processYear) initialSelectedColumns.push('processYear');
    if (initialFilters.processMonth) initialSelectedColumns.push('processMonth');
    if (initialFilters.reportType) initialSelectedColumns.push('reportType');

    setSelectedColumns(initialSelectedColumns);
  }, [initialFilters]);

  const formik = useFormik({
    initialValues: {
      executeTimeFrom: initialFilters?.executeTimeFrom || '',
      executeTimeTo: initialFilters?.executeTimeTo || '',
      processYear: initialFilters?.processYear,
      processMonth: initialFilters?.processMonth,
      reportType: initialFilters?.reportType
    },
    validationSchema,
    onSubmit: (values) => {
      let errors: { [key: string]: string } = {};

      // Validation for each selected field

      if (selectedColumns.includes('executeTime')) {
        if (!values.executeTimeFrom) errors.executeTimeFrom = 'Start execute time is required';
        if (!values.executeTimeTo) errors.executeTimeTo = 'End execute time is required'
      }

      if (selectedColumns.includes('processYear') && !values.processYear) {
        errors.processYear = 'Year is required';
      }


      if (selectedColumns.includes('processMonth') && !values.processMonth) {
        errors.processMonth = 'Month is required';
      }

      if (selectedColumns.includes('reportType') && !values.reportType) {
        errors.reportType = 'Report Type is required';
      }

      // If errors exist, set them and stop the form submission
      if (Object.keys(errors).length > 0) {
        formik.setErrors(errors);
        return;
      }

      const filtersToSend: any = { ...values };


      // Send the selected filters
      onApplyFilters(filtersToSend);
      onClose();
    },
  });

  // Handle the selection of multiple columns
  const handleColumnSelectionChange = (logId: string) => () => {
    setSelectedColumns((prevSelected) => {
      const isSelected = prevSelected.includes(logId);
      const updatedSelection = isSelected
        ? prevSelected.filter((col) => col !== logId)
        : [...prevSelected, logId];

      // Reset form fields when a column is deselected
      if (isSelected) {
        switch (logId) {
          case 'executeTime':
            formik.setFieldValue('executeTimeFrom', '');
            formik.setFieldValue('executeTimeTo', '');
            break;
          case 'processYear':
            formik.setFieldValue('processYear', '');
            break;
          case 'processMonth':
            formik.setFieldValue('processMonth', '');
            break;
          case 'reportType':
            formik.setFieldValue('reportType', '');
            break;
          default:
            break;
        }
      }

      return updatedSelection;
    });
  };

  const handleReset = () => {
    formik.resetForm();
    setSelectedColumns([]); // Clear selected columns
    onReset();
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <form onSubmit={formik.handleSubmit}>
        <DialogTitle>Filter Columns</DialogTitle>
        <Divider />
        <DialogContent>
          <Grid container spacing={0.5}>
            {/*  */}

            <Grid item xs={12} sm={4}>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={selectedColumns.includes('processYear')}
                    onChange={handleColumnSelectionChange('processYear')}
                    color="primary"
                  />
                }
                label="Year"
              />
            </Grid>

            <Grid item xs={12} sm={4}>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={selectedColumns.includes('reportType')}
                    onChange={handleColumnSelectionChange('reportType')}
                    color="primary"
                  />
                }
                label="Report Type"
              />
            </Grid>

            <Grid container spacing={2} mt={2}>
              {/* reportType */}
              {selectedColumns.includes('reportType') && (
                <Grid item xs={12} sm={6}>
                  <Stack spacing={1.25}>
                    <InputLabel htmlFor="processYear">
                      Report Type<span style={{ color: 'red' }}>*</span>
                    </InputLabel>
                    <Autocomplete
                      fullWidth
                      id="reportCodeId"
                      value={
                        reportType?.find(
                          (option) => option.reportCode === formik.values.reportType!
                        ) || null
                      }
                      onChange={(event, newValue) => {
                        formik.setFieldValue('reportType', newValue?.reportCode || '');
                      }}
                      options={reportType || []}
                      getOptionLabel={(item) => `${item.reportCode}`}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          placeholder="Select a Report Type"
                        />
                      )}
                    />
                  </Stack>
                </Grid>
              )}

              {/* process year */}
              {selectedColumns.includes('processYear') && (
                <Grid item xs={12} sm={6}>
                  <Stack spacing={1.25}>
                    <InputLabel htmlFor="processYear">
                      Process Year<span style={{ color: 'red' }}>*</span>
                    </InputLabel>
                    <TextField
                      {...formik.getFieldProps('processYear')}
                      error={formik.touched.processYear && Boolean(formik.errors.processYear)}
                      helperText={formik.touched.processYear && formik.errors.processYear}
                      fullWidth
                    />
                  </Stack>
                </Grid>
              )}

            </Grid>
          </Grid>
        </DialogContent>


        <DialogActions>
          <Button onClick={onClose}>Cancel</Button>
          <Button onClick={handleReset} color="secondary">
            Clear Filters
          </Button>
          <Button disabled={!selectedColumns} type="submit" color="primary" variant="contained">
            Apply
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

export default FilterDialog;
