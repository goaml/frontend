
// material-ui
import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Autocomplete,
    Box,
    CardActions,
    CardContent,
    FormControlLabel,
    FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField,
    Typography
} from '@mui/material';
import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, Formik } from 'formik';

// project imports
import { CalendarOutlined, InfoOutlined, LinkOutlined, LockOutlined, LogoutOutlined, NotificationOutlined, SecurityScanOutlined } from '@ant-design/icons';
import { Checkbox } from '@mui/material';
import IconButton from 'components/@extended/IconButton';
import MainCard from 'components/MainCard';
import useAuth from 'hooks/useAuth';
import useScriptRef from 'hooks/useScriptRef';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'store';
import { fetchGlobalDetailsSuccess, toInitialState } from 'store/reducers/global-details';
import { openSnackbar } from 'store/reducers/snackbar';
import { dataProps } from './types/types';


// ==============================|| ApplicationSettings ||============================== //

const ApplicationSettings = () => {
    const theme = useTheme();

    const { user } = useAuth()

    useEffect(() => { }, [user])

    const scriptedRef = useScriptRef();
    const dispatch = useDispatch();
    const { globalDetails, isLoading, error, success } = useSelector(state => state.globalDetails)

    const [data, setData] = useState<dataProps | null>(null)

    const formattedDate = data?.currentWorkingDate
  ? `${data.currentWorkingDate[0]}-${String(data.currentWorkingDate[1]).padStart(2, '0')}-${String(data.currentWorkingDate[2]).padStart(2, '0')}`
  : '';

    const formattedNextWorkingDate = data?.nextWorkingDate
    ? `${data.nextWorkingDate[0]}-${String(data.nextWorkingDate[1]).padStart(2, '0')}-${String(data.nextWorkingDate[2]).padStart(2, '0')}`
      : '';
    // ----------------------- | API Call - ApplicationSettings | ---------------------

    useEffect(() => {
        dispatch(fetchGlobalDetailsSuccess())
    }, [success])

    useEffect(() => {
        if (!globalDetails) {
            setData(null)
            return
        }
        if (globalDetails == null) {
            setData(null)
            return
        }
        setData(globalDetails.result!)
    }, [globalDetails])

    useEffect(() => {
        if (error != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: "Error",
                    variant: 'alert',
                    alert: {
                        color: 'error'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [error])

    useEffect(() => {
        if (success != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: success,
                    variant: 'alert',
                    alert: {
                        color: 'success'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [success])

    if (isLoading) {
        return <>Loading...</>
    }

    return (<>
        <MainCard
            sx={{
                overflow: 'visible',
                '& .MuiCardActions-root': {
                    color: 'background.paper',
                    bgcolor: 'primary.main',
                    '& .MuiTypography-root': { fontSize: '1rem' }
                }
            }}
            content={false}
        >
            <Formik
                initialValues={{
                    companyname: data?.companyName,
                    fronturl: data?.frontURL,
                    versionnum: data?.versionNumber,
                    coreurl: data?.coreURL,
                    footertitle: data?.footerTitle,
                    domaindescription: data?.domainDestription,
                    encryptKey: data?.encryptKey,
                    logopath: data?.logoPath,
                    usermalepath: data?.userMalePath,
                    userfemalepath: data?.userFemalePath,
                    loginattempt: data?.userLoginAttempts,
                    resetdate: data?.resetDateCount,
                    expirationdays: data?.userExpirationDays,
                    passwordexpiration: data?.passwordExpDate,
                    currentWorkingDate: data?.currentWorkingDate,
                    nextWorkingDate: data?.nextWorkingDate,
                    userNotificationMode: data?.userNotificationMode,
                    passwordMinLength: data?.passwordMinLength,
                    passwordMaxLength: data?.passwordMaxLength,
                    passwordNumOfSpecialCharacter: data?.passwordNumOfSpecialCharacter,
                    passwordNumOfCapitalLetters: data?.passwordNumOfCapitalLetters,
                    passwordNumOfSimpleLetters: data?.passwordNumOfSimpleLetters,
                    passwordNumOfDigits: data?.passwordNumOfDigits,
                    submit: null
                }}
                onSubmit={async (values, { setErrors, setStatus, setSubmitting }) => {
                    try {
                        if (scriptedRef.current) {
                            setStatus({ success: true });
                            setSubmitting(false);
                        }
                    } catch (err: any) {
                        console.error(err);
                        if (scriptedRef.current) {
                            setStatus({ success: false });
                            setErrors({ submit: err.message });
                            setSubmitting(false);
                        }
                    }
                }}
            >
                {(formik) => (
                    <LocalizationProvider dateAdapter={AdapterDateFns}>
                        <Form autoComplete="off" noValidate>
                            <CardActions
                                sx={{
                                    position: 'sticky',
                                    top: '60px',
                                    bgcolor: theme.palette.background.default,
                                    zIndex: 1,
                                    borderBottom: `1px solid ${theme.palette.divider}`
                                }}
                            >
                                <Stack direction="row" alignItems="center" justifyContent="space-between" sx={{ width: 1 }}>
                                    <Typography variant="h5" sx={{ m: 0, pl: 1.5 }}>
                                        Application Settings
                                    </Typography>
                                    <Stack direction="row" spacing={1} sx={{ px: 1.5, py: 0.75 }}>
                                        {/* <Button size="small" variant='contained' sx={{ border: "1px solid white", color: 'white' }}>
                                            Cancel
                                        </Button>
                                        <Button type='submit' variant="contained" size="small" sx={{ border: "1px solid white" }}>
                                            Save
                                        </Button> */}
                                    </Stack>
                                </Stack>
                            </CardActions>
                            <CardContent>
                                <Box
                                    sx={{
                                        marginBottom: '10px',
                                        '& .MuiAccordion-root': {
                                            borderColor: theme.palette.divider,
                                            '& .MuiAccordionSummary-root': {
                                                bgcolor: 'transparent',
                                                flexDirection: 'row',
                                                '&:focus-visible': {
                                                    bgcolor: 'primary.lighter'
                                                }
                                            },
                                            '& .MuiAccordionDetails-root': {
                                                borderColor: theme.palette.divider
                                            },
                                            '& .Mui-expanded': {
                                                color: theme.palette.primary.main
                                            }
                                        }
                                    }}
                                >
                                    <Accordion defaultExpanded={true}>
                                        <AccordionSummary aria-controls={`panel1d-content`} id={`panel1d-header`}>
                                            <Stack direction="row" spacing={1.5} alignItems="center">
                                                <IconButton
                                                    disableRipple
                                                    color="primary"
                                                    sx={{ bgcolor: 'primary.lighter' }}
                                                    aria-label="settings toggler"
                                                >
                                                    <InfoOutlined />
                                                </IconButton>
                                                <Typography variant="h6">General Information</Typography>
                                            </Stack>
                                        </AccordionSummary>
                                        <AccordionDetails>
                                            <Grid container spacing={3}>
                                                <Grid item xs={12} md={6}>
                                                    <Stack spacing={1.25}>
                                                        <InputLabel htmlFor="applicationName">Application Name</InputLabel>
                                                        <TextField
                                                            fullWidth
                                                            id="applicationName"
                                                            disabled
                                                            type="text"
                                                            value={data?.companyName}
                                                            name="companyname"
                                                        />
                                                    </Stack>
                                                </Grid>
                                                <Grid item xs={12} md={6}>
                                                    <Stack spacing={1.25}>
                                                        <InputLabel htmlFor="versionNumber">Version No.</InputLabel>
                                                        <TextField
                                                            fullWidth
                                                            id="versionNumber"
                                                            disabled
                                                            type="text"
                                                            value={data?.versionNumber}
                                                            name="companyname"

                                                        />
                                                    </Stack>
                                                </Grid>
                                            </Grid>
                                        </AccordionDetails>
                                    </Accordion>
                                </Box>
                                <Box
                                    sx={{
                                        marginBottom: '10px',
                                        '& .MuiAccordion-root': {
                                            borderColor: theme.palette.divider,
                                            '& .MuiAccordionSummary-root': {
                                                bgcolor: 'transparent',
                                                flexDirection: 'row',
                                                '&:focus-visible': {
                                                    bgcolor: 'primary.lighter'
                                                }
                                            },
                                            '& .MuiAccordionDetails-root': {
                                                borderColor: theme.palette.divider
                                            },
                                            '& .Mui-expanded': {
                                                color: theme.palette.primary.main
                                            }
                                        }
                                    }}
                                >
                                    <Accordion defaultExpanded={true}>
                                        <AccordionSummary aria-controls={`panel2d-content`} id={`panel2d-header`}>
                                            <Stack direction="row" spacing={1.5} alignItems="center">
                                                <IconButton
                                                    disableRipple
                                                    color="primary"
                                                    sx={{ bgcolor: 'primary.lighter' }}
                                                    aria-label="settings toggler"
                                                >
                                                    <LinkOutlined />
                                                </IconButton>
                                                <Typography variant="h6">URL Information</Typography>
                                            </Stack>
                                        </AccordionSummary>
                                        <AccordionDetails>
                                            <Grid container spacing={3}>
                                                <Grid item xs={12} md={12}>
                                                    <Stack spacing={1.25}>
                                                        <InputLabel htmlFor="homeUrl">Home URL</InputLabel>
                                                        <TextField
                                                            fullWidth
                                                            id="homeUrl"
                                                            disabled
                                                            type="text"
                                                            value={data?.frontURL}
                                                            name="frontURL"
                                                            InputProps={{
                                                                startAdornment: 'https://',
                                                                endAdornment: '.com'
                                                            }}

                                                        />
                                                    </Stack>
                                                </Grid>
                                                <Grid item xs={12} md={12}>
                                                    <Stack spacing={1.25}>
                                                        <InputLabel htmlFor="url">URL</InputLabel>
                                                        <TextField
                                                            fullWidth
                                                            id="url"
                                                            disabled
                                                            type="text"
                                                            value={data?.coreURL}
                                                            name="coreURL"
                                                            InputProps={{
                                                                startAdornment: 'https://',
                                                                endAdornment: '.com'
                                                            }}

                                                        />
                                                    </Stack>
                                                </Grid>
                                                <Grid item xs={12} md={12}>
                                                    <Stack spacing={1.25}>
                                                        <InputLabel htmlFor="domain">Domain</InputLabel>
                                                        <TextField
                                                            fullWidth
                                                            id="domain"
                                                            disabled
                                                            type="text"
                                                            value={data?.footerTitle}
                                                            name="footerTitle"

                                                        />
                                                    </Stack>
                                                </Grid>
                                            </Grid>
                                        </AccordionDetails>
                                    </Accordion>
                                </Box>
                                <Box
                                    sx={{
                                        marginBottom: '10px',
                                        '& .MuiAccordion-root': {
                                            borderColor: theme.palette.divider,
                                            '& .MuiAccordionSummary-root': {
                                                bgcolor: 'transparent',
                                                flexDirection: 'row',
                                                '&:focus-visible': {
                                                    bgcolor: 'primary.lighter'
                                                }
                                            },
                                            '& .MuiAccordionDetails-root': {
                                                borderColor: theme.palette.divider
                                            },
                                            '& .Mui-expanded': {
                                                color: theme.palette.primary.main
                                            }
                                        }
                                    }}
                                >
                                    <Accordion defaultExpanded={false}>
                                        <AccordionSummary aria-controls={`panel3d-content`} id={`panel3d-header`}>
                                            <Stack direction="row" spacing={1.5} alignItems="center">
                                                <IconButton
                                                    disableRipple
                                                    color="primary"
                                                    sx={{ bgcolor: 'primary.lighter' }}
                                                    aria-label="settings toggler"
                                                >
                                                    <SecurityScanOutlined />
                                                </IconButton>
                                                <Typography variant="h6">Security Settings</Typography>
                                            </Stack>
                                        </AccordionSummary>
                                        <AccordionDetails>
                                            <Grid container spacing={3}>
                                                <Grid item xs={12} md={12}>
                                                    <Stack spacing={1.25}>
                                                        <InputLabel htmlFor="encryptedKey">Encrypted Key</InputLabel>
                                                        <TextField
                                                            fullWidth
                                                            id="encryptedKey"
                                                            disabled
                                                            type="text"
                                                            value={data?.encryptKey}
                                                            name="encryptKey"
                                                        />
                                                    </Stack>
                                                </Grid>
                                                <Grid item xs={12} md={3}>
                                                    <Stack spacing={1.25}>
                                                        <InputLabel htmlFor="userLoginAttempts">User Login Attempts</InputLabel>
                                                        <TextField
                                                            type='number'
                                                            fullWidth
                                                            id="userLoginAttempts"
                                                            disabled
                                                            value={data?.userLoginAttempts}
                                                            name="userLoginAttempts"
                                                        />
                                                    </Stack>
                                                </Grid>
                                                <Grid item xs={12} md={3}>
                                                    <Stack spacing={1.25}>
                                                        <InputLabel htmlFor="resetDateCount">Reset Date Count</InputLabel>
                                                        <TextField
                                                            type='number'
                                                            fullWidth
                                                            id="resetDateCount"
                                                            disabled
                                                            value={data?.resetDateCount}
                                                            name="resetDateCount"

                                                        />
                                                    </Stack>
                                                </Grid>
                                                <Grid item xs={12} md={3}>
                                                    <Stack spacing={1.25}>
                                                        <InputLabel htmlFor="userExpirationDays">User Expiration Days</InputLabel>
                                                        <TextField
                                                            type='number'
                                                            fullWidth
                                                            id="userExpirationDays"
                                                            disabled
                                                            value={data?.userExpirationDays}
                                                            name="userExpirationDays"

                                                        />
                                                    </Stack>
                                                </Grid>
                                                <Grid item xs={12} md={3}>
                                                    <Stack spacing={1.25}>
                                                        <InputLabel htmlFor="passwordExpireDays">Password Expire Days</InputLabel>
                                                        <TextField
                                                            type='number'
                                                            fullWidth
                                                            id="passwordExpireDays"
                                                            disabled
                                                            value={data?.passwordExpDate}
                                                            name="passwordExpDate"

                                                        />
                                                    </Stack>
                                                </Grid>
                                            </Grid>
                                        </AccordionDetails>
                                    </Accordion>
                                </Box>
                                <Box
                                    sx={{
                                        marginBottom: '10px',
                                        '& .MuiAccordion-root': {
                                            borderColor: theme.palette.divider,
                                            '& .MuiAccordionSummary-root': {
                                                bgcolor: 'transparent',
                                                flexDirection: 'row',
                                                '&:focus-visible': {
                                                    bgcolor: 'primary.lighter'
                                                }
                                            },
                                            '& .MuiAccordionDetails-root': {
                                                borderColor: theme.palette.divider
                                            },
                                            '& .Mui-expanded': {
                                                color: theme.palette.primary.main
                                            }
                                        }
                                    }}
                                >
                                    <Accordion defaultExpanded={false}>
                                        <AccordionSummary aria-controls={`panel4d-content`} id={`panel4d-header`}>
                                            <Stack direction="row" spacing={1.5} alignItems="center">
                                                <IconButton
                                                    disableRipple
                                                    color="primary"
                                                    sx={{ bgcolor: 'primary.lighter' }}
                                                    aria-label="settings toggler"
                                                >
                                                    <CalendarOutlined />
                                                </IconButton>
                                                <Typography variant="h6">Date Information</Typography>
                                            </Stack>
                                        </AccordionSummary>
                                        <AccordionDetails>
                                            <Grid container spacing={3}>
                                                <Grid item xs={12} md={6}>
                                                    <Stack spacing={1.25}>
                                                        <InputLabel htmlFor="currentWorkingDate">Current Working Date</InputLabel>
                                                        <TextField
                                                            type="date"
                                                            fullWidth
                                                            id="currentWorkingDate"
                                                            disabled
                                                            // value={
                                                            //     data?.currentWorkingDate!.join('-')
                                                            // }
                                                            value={formattedDate}
                                                            name="currentWorkingDate"
                                                        />
                                                    </Stack>
                                                </Grid>
                                                <Grid item xs={12} md={6}>
                                                    <Stack spacing={1.25}>
                                                        <InputLabel htmlFor="nextWorkingDate">Next Working Date</InputLabel>
                                                        <TextField
                                                            type='date'
                                                            fullWidth
                                                            id="nextWorkingDate"
                                                            disabled
                                                            // value={
                                                            //     data?.nextWorkingDate!.join('-')
                                                            // }
                                                            value={formattedNextWorkingDate}
                                                            name="nextWorkingDate"

                                                        />
                                                    </Stack>
                                                </Grid>
                                            </Grid>
                                        </AccordionDetails>
                                    </Accordion>
                                </Box>
                                <Box
                                    sx={{
                                        marginBottom: '10px',
                                        '& .MuiAccordion-root': {
                                            borderColor: theme.palette.divider,
                                            '& .MuiAccordionSummary-root': {
                                                bgcolor: 'transparent',
                                                flexDirection: 'row',
                                                '&:focus-visible': {
                                                    bgcolor: 'primary.lighter'
                                                }
                                            },
                                            '& .MuiAccordionDetails-root': {
                                                borderColor: theme.palette.divider
                                            },
                                            '& .Mui-expanded': {
                                                color: theme.palette.primary.main
                                            }
                                        }
                                    }}
                                >
                                    <Accordion defaultExpanded={false}>
                                        <AccordionSummary aria-controls={`panel5d-content`} id={`panel5d-header`}>
                                            <Stack direction="row" spacing={1.5} alignItems="center">
                                                <IconButton
                                                    disableRipple
                                                    color="primary"
                                                    sx={{ bgcolor: 'primary.lighter' }}
                                                    aria-label="settings toggler"
                                                >
                                                    <NotificationOutlined />
                                                </IconButton>
                                                <Typography variant="h6">Notification Settings</Typography>
                                            </Stack>
                                        </AccordionSummary>
                                        <AccordionDetails>
                                            <Grid container spacing={3}>
                                                <Grid item xs={12} md={4}>
                                                    <Stack spacing={1.25}>
                                                        <InputLabel htmlFor="userNotificationMode">User Notification Mode</InputLabel>
                                                        <Autocomplete
                                                            fullWidth
                                                            id="userNotificationMode"
                                                            value={[{ id: 'Both', code: "Both", description: "Both" }, { id: 'sms', code: "SMS", description: "SMS" }, { id: 'email', code: "Email", description: "Email" }].find((option) => option.id === formik.values.userNotificationMode) || null}
                                                            onChange={(event: any, newValue: { id: string, code: string, description: string } | null) => {
                                                                formik.setFieldValue('userNotificationMode', newValue?.id);
                                                            }}
                                                            options={[{ id: 'Both', code: "Both", description: "Both" }, { id: 'sms', code: "SMS", description: "SMS" }, { id: 'email', code: "Email", description: "Email" }]}
                                                            getOptionLabel={(item) => `${item.description}`}
                                                            renderInput={(params) => {
                                                                return (
                                                                    <TextField
                                                                        {...params}
                                                                        disabled
                                                                        value={data?.userNotificationMode}
                                                                        name="userNotificationMode"
                                                                        sx={{ '& .MuiAutocomplete-input.Mui-disabled': { WebkitTextFillColor: theme.palette.text.primary } }}
                                                                    />
                                                                )
                                                            }}
                                                        />
                                                        {formik.touched.userNotificationMode && formik.errors.userNotificationMode && (
                                                            <FormHelperText error id="helper-text-userNotificationMode">
                                                                {formik.errors.userNotificationMode}
                                                            </FormHelperText>
                                                        )}
                                                    </Stack>
                                                </Grid>
                                                <Grid item xs={12} md={4}>
                                                    <FormControlLabel
                                                        control={
                                                            <Checkbox
                                                                color="primary"
                                                                name="userNotificationModeIsEmail"
                                                                //checked={formik.values.userNotificationModeIsEmail}
                                                                onChange={formik.handleChange}
                                                            />
                                                        }
                                                        label="User Notification Mode Is Email"
                                                        labelPlacement='start'
                                                        sx={{ marginLeft: '0px', marginTop: "30px" }}
                                                    />
                                                    {/* {formik.touched.userNotificationModeIsEmail && formik.errors.userNotificationModeIsEmail && (
                                                        <FormHelperText error>{formik.errors.userNotificationModeIsEmail}</FormHelperText>
                                                    )} */}
                                                </Grid>
                                                <Grid item xs={12} md={4}>
                                                    <FormControlLabel
                                                        control={
                                                            <Checkbox
                                                                color="primary"
                                                                name="userNotificationModeIsSMS"
                                                                //checked={formik.values.userNotificationModeIsSMS}
                                                                onChange={formik.handleChange}
                                                            />
                                                        }
                                                        label="User Notification Mode Is SMS"
                                                        labelPlacement='start'
                                                        sx={{ marginLeft: '0px', marginTop: "30px" }}
                                                    />
                                                    {/* {formik.touched.userNotificationModeIsSMS && formik.errors.userNotificationModeIsSMS && (
                                                        <FormHelperText error>{formik.errors.userNotificationModeIsSMS}</FormHelperText>
                                                    )} */}
                                                </Grid>
                                            </Grid>
                                        </AccordionDetails>
                                    </Accordion>
                                </Box>
                                <Box
                                    sx={{
                                        marginBottom: '10px',
                                        '& .MuiAccordion-root': {
                                            borderColor: theme.palette.divider,
                                            '& .MuiAccordionSummary-root': {
                                                bgcolor: 'transparent',
                                                flexDirection: 'row',
                                                '&:focus-visible': {
                                                    bgcolor: 'primary.lighter'
                                                }
                                            },
                                            '& .MuiAccordionDetails-root': {
                                                borderColor: theme.palette.divider
                                            },
                                            '& .Mui-expanded': {
                                                color: theme.palette.primary.main
                                            }
                                        }
                                    }}
                                >
                                    <Accordion defaultExpanded={false}>
                                        <AccordionSummary aria-controls={`panel6d-content`} id={`panel6d-header`}>
                                            <Stack direction="row" spacing={1.5} alignItems="center">
                                                <IconButton
                                                    disableRipple
                                                    color="primary"
                                                    sx={{ bgcolor: 'primary.lighter' }}
                                                    aria-label="settings toggler"
                                                >
                                                    <LogoutOutlined />
                                                </IconButton>
                                                <Typography variant="h6">Logo and Icons</Typography>
                                            </Stack>
                                        </AccordionSummary>
                                        <AccordionDetails>
                                            <Grid container spacing={3}>
                                                <Grid item xs={12} md={6}>
                                                    <Stack spacing={1.25}>
                                                        <InputLabel htmlFor="logoIcon">Logo Icon</InputLabel>
                                                        <TextField
                                                            fullWidth
                                                            id="logoIcon"
                                                            disabled
                                                            value={data?.logoPath}
                                                            name="logoPath"
                                                        // {...getFieldProps('logoIcon')}
                                                        // error={Boolean(touched.logoIcon && errors.logoIcon)}
                                                        // helperText={touched.logoIcon && errors.logoIcon}
                                                        />
                                                    </Stack>
                                                </Grid>
                                                <Grid item xs={12} md={6}>
                                                    <Stack spacing={1.25}>
                                                        <InputLabel htmlFor="logoMain">Logo Main</InputLabel>
                                                        <TextField
                                                            fullWidth
                                                            id="logoMain"
                                                            disabled
                                                            value={data?.logoPath}
                                                            name="logoPath"


                                                        />
                                                    </Stack>
                                                </Grid>
                                                <Grid item xs={12} md={6}>
                                                    <Stack spacing={1.25}>
                                                        <InputLabel htmlFor="maleIcon">Male Icon</InputLabel>
                                                        <TextField
                                                            fullWidth
                                                            id="maleIcon"
                                                            disabled
                                                            value={data?.userMalePath}
                                                            name="userMalePath"

                                                        />
                                                    </Stack>
                                                </Grid>
                                                <Grid item xs={12} md={6}>
                                                    <Stack spacing={1.25}>
                                                        <InputLabel htmlFor="femaleIcon">Female Icon</InputLabel>
                                                        <TextField
                                                            fullWidth
                                                            id="femaleIcon"
                                                            disabled
                                                            value={data?.userFemalePath}
                                                            name="userFemalePath"

                                                        />
                                                    </Stack>
                                                </Grid>
                                            </Grid>
                                        </AccordionDetails>
                                    </Accordion>
                                </Box>
                                <Box
                                    sx={{
                                        marginBottom: '10px',
                                        '& .MuiAccordion-root': {
                                            borderColor: theme.palette.divider,
                                            '& .MuiAccordionSummary-root': {
                                                bgcolor: 'transparent',
                                                flexDirection: 'row',
                                                '&:focus-visible': {
                                                    bgcolor: 'primary.lighter'
                                                }
                                            },
                                            '& .MuiAccordionDetails-root': {
                                                borderColor: theme.palette.divider
                                            },
                                            '& .Mui-expanded': {
                                                color: theme.palette.primary.main
                                            }
                                        }
                                    }}
                                >
                                    <Accordion defaultExpanded={false}>
                                        <AccordionSummary aria-controls={`panel7d-content`} id={`panel7d-header`}>
                                            <Stack direction="row" spacing={1.5} alignItems="center">
                                                <IconButton
                                                    disableRipple
                                                    color="primary"
                                                    sx={{ bgcolor: 'primary.lighter' }}
                                                    aria-label="settings toggler"
                                                >
                                                    <LockOutlined />
                                                </IconButton>
                                                <Typography variant="h6">Password Policy</Typography>
                                            </Stack>
                                        </AccordionSummary>
                                        <AccordionDetails>
                                            <Grid container spacing={3}>
                                                <Grid item xs={12} md={4}>
                                                    <Stack spacing={1.25}>
                                                        <InputLabel htmlFor="passwordMinimumLength">Password Minimum Length</InputLabel>
                                                        <TextField
                                                            type='number'
                                                            fullWidth
                                                            id="passwordMinimumLength"
                                                            disabled
                                                            value={data?.passwordMinLength}
                                                            name="passwordMinLength"

                                                        />
                                                    </Stack>
                                                </Grid>
                                                <Grid item xs={12} md={4}>
                                                    <Stack spacing={1.25}>
                                                        <InputLabel htmlFor="passwordMaximumLength">Password Maximum Length</InputLabel>
                                                        <TextField
                                                            type='number'
                                                            fullWidth
                                                            id="passwordMaximumLength"
                                                            disabled
                                                            value={data?.passwordMaxLength}
                                                            name="passwordMaxLength"

                                                        />
                                                    </Stack>
                                                </Grid>
                                                <Grid item xs={12} md={4}>
                                                    <Stack spacing={1.25}>
                                                        <InputLabel htmlFor="passwordNoOfSpecialCharacters">Password No. Of Special Characters</InputLabel>
                                                        <TextField
                                                            type='number'
                                                            fullWidth
                                                            id="passwordNoOfSpecialCharacters"
                                                            disabled
                                                            value={data?.passwordNumOfSpecialCharacter}
                                                            name="passwordNumOfSpecialCharacter"

                                                        />
                                                    </Stack>
                                                </Grid>
                                                <Grid item xs={12} md={4}>
                                                    <Stack spacing={1.25}>
                                                        <InputLabel htmlFor="passwordNoOfCapitalLetters">Password No. Of Capital Letters</InputLabel>
                                                        <TextField
                                                            type='number'
                                                            fullWidth
                                                            id="passwordNoOfCapitalLetters"
                                                            disabled
                                                            value={data?.passwordNumOfCapitalLetters}
                                                            name="passwordNumOfCapitalLetters"

                                                        />
                                                    </Stack>
                                                </Grid>
                                                <Grid item xs={12} md={4}>
                                                    <Stack spacing={1.25}>
                                                        <InputLabel htmlFor="passwordNoOfSimpleLetters">Password No. Of Simple Letters</InputLabel>
                                                        <TextField
                                                            type='number'
                                                            fullWidth
                                                            id="passwordNoOfSimpleLetters"
                                                            disabled
                                                            value={data?.passwordNumOfSimpleLetters}
                                                            name="passwordNumOfSimpleLetters"

                                                        />
                                                    </Stack>
                                                </Grid>
                                                <Grid item xs={12} md={4}>
                                                    <Stack spacing={1.25}>
                                                        <InputLabel htmlFor="passwordNoOfDigits">Password No. Of Digits</InputLabel>
                                                        <TextField
                                                            type='number'
                                                            fullWidth
                                                            id="passwordNoOfDigits"
                                                            disabled
                                                            value={data?.passwordNumOfDigits}
                                                            name="passwordNumOfDigits"

                                                        />
                                                    </Stack>
                                                </Grid>
                                            </Grid>
                                        </AccordionDetails>
                                    </Accordion>
                                </Box>
                            </CardContent>
                        </Form>
                    </LocalizationProvider>
                )}
            </Formik>
        </MainCard >
    </>);
}

export default ApplicationSettings;
