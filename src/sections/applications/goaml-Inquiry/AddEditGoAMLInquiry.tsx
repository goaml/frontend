//import { useState } To 'react';

// material-ui
import {
    //Autocomplete,
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    // FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField,
   // Tooltip
} from '@mui/material';
// import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { FormikProvider, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// types
import { dataProps } from 'pages/applications/goaml-inquiry/list/types/types';

export interface FormikValues {
    rReportId?: number;
    rRentityId?: number;
    rRentityBranch?: string;
    rSubmissionCode?: string;
    rReportCode?: string;
    rEntityReference?: string;
    rFiuRefNumber?: string;
    rSubmissionDate?: string;
    rCurrencyCodeLocal?: number;
    rReportingPerson?: string;
    rLocation?: string;
    rReason?: string;
    rAction?: string;
    rStatusId?: number;
    rIsActive?: boolean;
    rSystemDate?: string;
    rUserId?: string;
    tTransactionId?: number;
    tTransactionNumber?: string;
    tInternalRefNumber?: string;
    tTransactionLocation?: string;
    tTransactionDescription?: string;
    tDateTransaction?: string;
    tTeller?: string;
    tAuthorized?: string;
    tLateDeposit?: boolean;
    tDatePosting?: string;
    tValueDate?: string;
    tConductionTypeId?: number; // TransmodeCode
    tTransmodeComment?: string;
    tAmountLocal?: number; // Decimal
    tGoodsServices?: string;
    tComments?: string;
    tStatusId?: number;
    tIsActive?: boolean;
    tReportId?: number;
}

export interface Props {
    goAMLInquiry?: dataProps;
    onCancel: () => void;
}

const getInitialValues = (goAMLInquiry: FormikValues | null) => {
    const newgoAMLInquiry: FormikValues = {
        rReportId: 0,
        rRentityId: 0,
        rRentityBranch: '',
        rSubmissionCode: '',
        rReportCode: '',
        rEntityReference: '',
        rFiuRefNumber: '',
        rSubmissionDate: '',
        rCurrencyCodeLocal: 0,
        rReportingPerson: '',
        rLocation: '',
        rReason: '',
        rAction: '',
        rStatusId: 0,
        rIsActive: true,
        rSystemDate: '',
        rUserId: '',
        tTransactionId: 0,
        tTransactionNumber: '',
        tInternalRefNumber: '',
        tTransactionLocation: '',
        tTransactionDescription: '',
        tDateTransaction: '',
        tTeller: '',
        tAuthorized: '',
        tLateDeposit: true,
        tDatePosting: '',
        tValueDate: '',
        tConductionTypeId: 0, // TransmodeCode
        tTransmodeComment: '',
        tAmountLocal: 0, // Decimal
        tGoodsServices: '',
        tComments: '',
        tStatusId: 0,
        tIsActive: true,
        tReportId: 0,
    };

    if (goAMLInquiry) {
        return _.merge({}, newgoAMLInquiry, goAMLInquiry);
    }

    return newgoAMLInquiry;
};

const AddEditgoAMLInquiry = ({ goAMLInquiry, onCancel }: Props) => {
    const goAMLInquirySchema = Yup.object().shape({
        toId: Yup.number().required('To ID is required'),
        toFundsCode: Yup.string().required('To Funds Code is required'),
        toFundsComment: Yup.string().required('To Funds Comment is required'),
        toForeignCurrency: Yup.string().required('To Foreign Currency is required'),
        tConductor: Yup.string().required('T Conductor is required'),
        toAccount: Yup.string().required('To Account is required'),
        toPerson: Yup.string().required('To Person is required'),
        toEntity: Yup.string().required('To Entity is required'),
        toCountry: Yup.string().required('To Country is required'),
        statusId: Yup.number().required('Status ID is required'),
        isActive: Yup.boolean().required('Is Active is required'),
        transactionId: Yup.number().required('Transaction ID is required'),
    });

    const formik = useFormik({
        initialValues: getInitialValues(goAMLInquiry!),
        validationSchema: goAMLInquirySchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (goAMLInquiry) {
                    // put request logic
                } else {
                    // post request logic
                }
                resetForm();
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, getFieldProps } = formik;

    const getHelperText = (fieldError: any): string | undefined => {
        return typeof fieldError === 'string' ? fieldError : undefined;
    };

    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{goAMLInquiry ? 'View transaction Reciever Details' : 'New goAMLInquiry'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                <Grid container spacing={3}>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="rReportId">Report ID <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="rReportId"
                                            {...getFieldProps('rReportId')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.rReportId && errors.rReportId)}
                                            helperText={getHelperText(touched.rReportId && errors.rReportId)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="rRentityId">Entity ID <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="rRentityId"
                                            {...getFieldProps('rRentityId')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.rRentityId && errors.rRentityId)}
                                            helperText={getHelperText(touched.rRentityId && errors.rRentityId)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="rRentityBranch">Entity Branch <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="rRentityBranch"
                                            {...getFieldProps('rRentityBranch')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.rRentityBranch && errors.rRentityBranch)}
                                            helperText={getHelperText(touched.rRentityBranch && errors.rRentityBranch)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="rSubmissionCode">Submission Code <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="rSubmissionCode"
                                            {...getFieldProps('rSubmissionCode')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.rSubmissionCode && errors.rSubmissionCode)}
                                            helperText={getHelperText(touched.rSubmissionCode && errors.rSubmissionCode)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="rReportCode">Report Code <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="rReportCode"
                                            {...getFieldProps('rReportCode')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.rReportCode && errors.rReportCode)}
                                            helperText={getHelperText(touched.rReportCode && errors.rReportCode)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="rEntityReference">Entity Reference <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="rEntityReference"
                                            {...getFieldProps('rEntityReference')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.rEntityReference && errors.rEntityReference)}
                                            helperText={getHelperText(touched.rEntityReference && errors.rEntityReference)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="rFiuRefNumber">FIU Ref Number <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="rFiuRefNumber"
                                            {...getFieldProps('rFiuRefNumber')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.rFiuRefNumber && errors.rFiuRefNumber)}
                                            helperText={getHelperText(touched.rFiuRefNumber && errors.rFiuRefNumber)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="rSubmissionDate">Submission Date <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="rSubmissionDate"
                                            {...getFieldProps('rSubmissionDate')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.rSubmissionDate && errors.rSubmissionDate)}
                                            helperText={getHelperText(touched.rSubmissionDate && errors.rSubmissionDate)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="rCurrencyCodeLocal">Currency Code Local <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="rCurrencyCodeLocal"
                                            {...getFieldProps('rCurrencyCodeLocal')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.rCurrencyCodeLocal && errors.rCurrencyCodeLocal)}
                                            helperText={getHelperText(touched.rCurrencyCodeLocal && errors.rCurrencyCodeLocal)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="rReportingPerson">Reporting Person <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="rReportingPerson"
                                            {...getFieldProps('rReportingPerson')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.rReportingPerson && errors.rReportingPerson)}
                                            helperText={getHelperText(touched.rReportingPerson && errors.rReportingPerson)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="rLocation">Location <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="rLocation"
                                            {...getFieldProps('rLocation')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.rLocation && errors.rLocation)}
                                            helperText={getHelperText(touched.rLocation && errors.rLocation)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="rReason">Reason <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="rReason"
                                            {...getFieldProps('rReason')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.rReason && errors.rReason)}
                                            helperText={getHelperText(touched.rReason && errors.rReason)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="rAction">Action <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="rAction"
                                            {...getFieldProps('rAction')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.rAction && errors.rAction)}
                                            helperText={getHelperText(touched.rAction && errors.rAction)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="rStatusId">Status ID <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="rStatusId"
                                            {...getFieldProps('rStatusId')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.rStatusId && errors.rStatusId)}
                                            helperText={getHelperText(touched.rStatusId && errors.rStatusId)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="rIsActive">Is Active <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="rIsActive"
                                            {...getFieldProps('rIsActive')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.rIsActive && errors.rIsActive)}
                                            helperText={getHelperText(touched.rIsActive && errors.rIsActive)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="tTransactionId">Transaction ID <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="tTransactionId"
                                            {...getFieldProps('tTransactionId')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.tTransactionId && errors.tTransactionId)}
                                            helperText={getHelperText(touched.tTransactionId && errors.tTransactionId)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="tTransactionNumber">Transaction Number <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="tTransactionNumber"
                                            {...getFieldProps('tTransactionNumber')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.tTransactionNumber && errors.tTransactionNumber)}
                                            helperText={getHelperText(touched.tTransactionNumber && errors.tTransactionNumber)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="tInternalRefNumber">Internal Ref Number <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="tInternalRefNumber"
                                            {...getFieldProps('tInternalRefNumber')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.tInternalRefNumber && errors.tInternalRefNumber)}
                                            helperText={getHelperText(touched.tInternalRefNumber && errors.tInternalRefNumber)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="tTransactionLocation">Transaction Location <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="tTransactionLocation"
                                            {...getFieldProps('tTransactionLocation')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.tTransactionLocation && errors.tTransactionLocation)}
                                            helperText={getHelperText(touched.tTransactionLocation && errors.tTransactionLocation)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="tTransactionDescription">Transaction Description <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="tTransactionDescription"
                                            {...getFieldProps('tTransactionDescription')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.tTransactionDescription && errors.tTransactionDescription)}
                                            helperText={getHelperText(touched.tTransactionDescription && errors.tTransactionDescription)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="tDateTransaction">Date Transaction <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="tDateTransaction"
                                            {...getFieldProps('tDateTransaction')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.tDateTransaction && errors.tDateTransaction)}
                                            helperText={getHelperText(touched.tDateTransaction && errors.tDateTransaction)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="tTeller">Teller <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="tTeller"
                                            {...getFieldProps('tTeller')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.tTeller && errors.tTeller)}
                                            helperText={getHelperText(touched.tTeller && errors.tTeller)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="tAuthorized">Authorized <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="tAuthorized"
                                            {...getFieldProps('tAuthorized')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.tAuthorized && errors.tAuthorized)}
                                            helperText={getHelperText(touched.tAuthorized && errors.tAuthorized)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="tLateDeposit">Late Deposit <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="tLateDeposit"
                                            {...getFieldProps('tLateDeposit')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.tLateDeposit && errors.tLateDeposit)}
                                            helperText={getHelperText(touched.tLateDeposit && errors.tLateDeposit)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="tDatePosting">Date Posting <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="tDatePosting"
                                            {...getFieldProps('tDatePosting')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.tDatePosting && errors.tDatePosting)}
                                            helperText={getHelperText(touched.tDatePosting && errors.tDatePosting)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="tValueDate">Value Date <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="tValueDate"
                                            {...getFieldProps('tValueDate')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.tValueDate && errors.tValueDate)}
                                            helperText={getHelperText(touched.tValueDate && errors.tValueDate)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="tConductionTypeId">Conduction Type ID <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="tConductionTypeId"
                                            {...getFieldProps('tConductionTypeId')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.tConductionTypeId && errors.tConductionTypeId)}
                                            helperText={getHelperText(touched.tConductionTypeId && errors.tConductionTypeId)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="tTransmodeComment">Transmode Comment <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="tTransmodeComment"
                                            {...getFieldProps('tTransmodeComment')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.tTransmodeComment && errors.tTransmodeComment)}
                                            helperText={getHelperText(touched.tTransmodeComment && errors.tTransmodeComment)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="tAmountLocal">Amount Local <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="tAmountLocal"
                                            {...getFieldProps('tAmountLocal')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.tAmountLocal && errors.tAmountLocal)}
                                            helperText={getHelperText(touched.tAmountLocal && errors.tAmountLocal)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="tGoodsServices">Goods Services <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="tGoodsServices"
                                            {...getFieldProps('tGoodsServices')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.tGoodsServices && errors.tGoodsServices)}
                                            helperText={getHelperText(touched.tGoodsServices && errors.tGoodsServices)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="tComments">Comments <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="tComments"
                                            {...getFieldProps('tComments')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.tComments && errors.tComments)}
                                            helperText={getHelperText(touched.tComments && errors.tComments)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="tStatusId">Status ID <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="tStatusId"
                                            {...getFieldProps('tStatusId')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.tStatusId && errors.tStatusId)}
                                            helperText={getHelperText(touched.tStatusId && errors.tStatusId)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="tIsActive">Is Active <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="tIsActive"
                                            {...getFieldProps('tIsActive')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.tIsActive && errors.tIsActive)}
                                            helperText={getHelperText(touched.tIsActive && errors.tIsActive)}
                                        />
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack spacing={1.25}>
                                        <InputLabel htmlFor="tReportId">Report ID <span style={{ color: 'red' }}>*</span></InputLabel>
                                        <TextField
                                            fullWidth
                                            id="tReportId"
                                            {...getFieldProps('tReportId')}
                                            InputProps={{
                                            readOnly: false,
                                            }}
                                            error={Boolean(touched.tReportId && errors.tReportId)}
                                            helperText={getHelperText(touched.tReportId && errors.tReportId)}
                                        />
                                        </Stack>
                                    </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 1.25 }}>
                            <Button color="error" onClick={onCancel}>
                                Cancel
                            </Button>
                        </DialogActions>
                    </form>
                </LocalizationProvider>
            </FormikProvider>
        </>
    );
};

export default AddEditgoAMLInquiry;
