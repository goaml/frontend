import { useEffect, useState } from 'react';

// material-ui
import {
    Autocomplete,
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports

// assets
import { useTheme } from '@mui/material/styles';
import { dispatch, useSelector } from 'store';
import { addMultipleBranches, updateMultipleBranch } from 'store/reducers/branch';
import { getBranchesNotAssignedToUserSuccess } from 'store/reducers/multiple-branch';
import { BranchesForUsers } from 'types/branch';

// types

// constant
const getInitialValues = (multipleBranch: FormikValues | null) => {

    const newBranch = {
        brachDeptId: undefined,
        userId: undefined,
        branchId: undefined,
        from: new Date().toISOString().split('T')[0],   // Set today's date for "To Date",
        to: new Date().toISOString().split('T')[0],   // Set today's date for "To Date"
        //isActive: true,
    }

    if (multipleBranch) {
        return _.merge({}, newBranch, multipleBranch);
    }

    return newBranch;
};

// ==============================|| Multiple Branches ADD / EDIT ||============================== //

export interface Props {
    multipleBranch?: any;
    onCancel: () => void;
    userId?: number
}

const AddEditMultipleBranches = ({ multipleBranch, onCancel, userId }: Props) => {

    const BranchSchema = Yup.object().shape({
        brachDeptId: Yup.string().required('Branch is required'),
    });

    const formik = useFormik({
        initialValues: getInitialValues(multipleBranch!),
        validationSchema: BranchSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (multipleBranch) {
                    // PUT API 
                    dispatch(updateMultipleBranch({ ...values }))
                } else {
                    // POST API
                    dispatch(addMultipleBranches({
                        brachDeptId: values.brachDeptId,
                        from: values.from,
                        to: values.to,
                        userId: userId,
                    }))
                }
                resetForm()
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });
    const theme = useTheme();

    // =================================| Date- Format change|=============================
    const formatDate = (dateString: string) => {
        if (!dateString) {
            return ''; // Handle null or empty string dates
        }

        const date = new Date(dateString);
        const year = date.getFullYear();
        const month = `${date.getMonth() + 1}`.padStart(2, '0');
        const day = `${date.getDate()}`.padStart(2, '0');

        return `${year}-${month}-${day}`;
    };

    // SELECT BRANCHES FOR DROPDOWN
    const { NotAssignedBranchesForUsers } = useSelector((state) => state.multipleBranches);

    //console.log(NotAssignedBranchesForUsers);


    const [dropDownNotAssignedBranchesForUsers, setDropdownNotAssignedBranchesForUsers] = useState<any>([])
    // console.log(multipleBranch)
    useEffect(() => {
        if (typeof NotAssignedBranchesForUsers === "undefined" || NotAssignedBranchesForUsers === null || NotAssignedBranchesForUsers?.length === 0) return
        if (typeof multipleBranch === "undefined") {
            setDropdownNotAssignedBranchesForUsers([
                ...NotAssignedBranchesForUsers!
            ])

        } else {
            setDropdownNotAssignedBranchesForUsers([
                {
                    branchName: multipleBranch?.branchName,
                    brachDeptId: multipleBranch?.brachDeptId,
                    usMDepartment: {
                        deptName: multipleBranch?.deptName
                    }
                },
                ...NotAssignedBranchesForUsers!
            ])
        }
    }, [NotAssignedBranchesForUsers, multipleBranch])


    useEffect(() => {
        if (typeof userId === "undefined") return;
        dispatch(getBranchesNotAssignedToUserSuccess(userId!));

        // Set today's date for "From Date" and "To Date"
        const today = new Date().toISOString().split('T')[0];

        formik.setFieldValue('from', today);
        formik.setFieldValue('to', today);
    }, [userId]);


    const { handleSubmit, isSubmitting, getFieldProps } = formik;

    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{multipleBranch ? 'Edit Branch Details' : 'Add Branch Details'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                    <Grid container spacing={3}>
                                        <Grid item xs={12}>
                                            <InputLabel htmlFor="brachDeptId">Branch Department<span style={{ color: 'red' }}> *</span></InputLabel>
                                            <Autocomplete
                                                fullWidth
                                                id="brachDeptId"
                                                value={dropDownNotAssignedBranchesForUsers?.find((option: BranchesForUsers) => option.brachDeptId === formik.values.brachDeptId) || null}
                                                onChange={(event: any, newValue: BranchesForUsers | null) => {
                                                    formik.setFieldValue('brachDeptId', newValue?.brachDeptId || null); // Set to null if cleared
                                                    formik.setFieldTouched('brachDeptId', true); // Mark as touched
                                                }}
                                                options={dropDownNotAssignedBranchesForUsers || []}
                                                getOptionLabel={(item) => `${item.branchName} - ${item.usMDepartment?.deptName}`}
                                                renderInput={(params) => (
                                                    <TextField
                                                        {...params}
                                                        placeholder="Select Branch"
                                                        sx={{ '& .MuiAutocomplete-input.Mui-disabled': { WebkitTextFillColor: theme.palette.text.primary } }}
                                                    />
                                                )}
                                            />
                                            {formik.touched.brachDeptId && formik.errors.brachDeptId && (
                                                <FormHelperText error id="helper-text-brachDeptId">
                                                    {formik.errors.brachDeptId}
                                                </FormHelperText>
                                            )}
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Grid container spacing={3}>
                                                <Grid item xs={12} sm={6}>
                                                    <Stack spacing={1.25}>
                                                        <InputLabel htmlFor="from">From Date</InputLabel>
                                                        <TextField
                                                            id="from"
                                                            type="date"
                                                            InputLabelProps={{
                                                                shrink: true
                                                            }}
                                                            value={formatDate(getFieldProps('from').value)}
                                                            onChange={(e) => {
                                                                formik.setFieldValue('from', e.target.value);
                                                            }}
                                                            InputProps={{
                                                                // No restrictions, allow past, present, and future dates
                                                                inputProps: {}
                                                            }}
                                                        />

                                                    </Stack>
                                                </Grid>

                                                <Grid item xs={12} sm={6}>
                                                    <Stack spacing={1.25}>
                                                        <InputLabel htmlFor="to">To Date</InputLabel>
                                                        <TextField
                                                            id="to"
                                                            type="date"
                                                            InputLabelProps={{
                                                                shrink: true
                                                            }}
                                                            value={formatDate(getFieldProps('to').value)}
                                                            onChange={(e) => {
                                                                formik.setFieldValue('to', e.target.value);
                                                            }}
                                                            InputProps={{
                                                                // Remove the max restriction to allow selecting future dates
                                                                inputProps: {
                                                                    // You can optionally add a min date here as well if needed:
                                                                    min: formik.values.from // Optionally restrict to dates after "From Date"
                                                                }
                                                            }}
                                                        />

                                                    </Stack>
                                                </Grid>
                                            </Grid>
                                        </Grid>

                                    </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 2.5 }}>
                            <Grid container justifyContent="right" alignItems="center">
                                <Grid item>
                                    <Stack direction="row" spacing={2} alignItems="center">
                                        <Button color="error" onClick={onCancel}>
                                            Cancel
                                        </Button>
                                        <Button type="submit" variant="contained" disabled={isSubmitting}>
                                            {multipleBranch ? 'Edit' : 'Add'}
                                        </Button>
                                    </Stack>
                                </Grid>

                            </Grid>
                        </DialogActions>
                    </Form>
                </LocalizationProvider>
            </FormikProvider>
        </>
    );
};

export default AddEditMultipleBranches;
