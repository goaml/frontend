// material-ui
import { useTheme } from '@mui/material/styles';
import {
    useMediaQuery,
    Grid,
    List,
    ListItem,
    Stack,
    TableCell,
    TableRow,
    Typography
} from '@mui/material';

// third-party

// project import
import MainCard from 'components/MainCard';
import Transitions from 'components/@extended/Transitions';

// assets


// ==============================|| USER - VIEW ||============================== //

const ProvinceCodeView = ({ data }: any) => {
    const theme = useTheme();
    const matchDownMD = useMediaQuery(theme.breakpoints.down('md'));

    return (
        <TableRow sx={{ '&:hover': { bgcolor: `transparent !important` }, overflow: 'hidden' }}>
            <TableCell colSpan={19} sx={{ p: 2.5, overflow: 'hidden' }}>
                <Transitions type="slide" direction="down" in={true}>
                    <Grid container spacing={2.5} sx={{ pl: { xs: 0, sm: 1, md: 1, lg: 1, xl: 6 } }}>

                        <Grid item xs={12} sm={7} md={8} lg={8} xl={9}>
                            <Stack spacing={2.5}>
                                <MainCard title="View Applicant Details">
                                    <List sx={{ py: 0 }}>
                                        <ListItem divider={!matchDownMD}>
                                            <Grid container spacing={3}>
                                                <Grid item xs={12} md={6}>
                                                    <Stack spacing={0.5}>
                                                        <Typography color="secondary">User Id</Typography>
                                                        <Typography>{data.userName}</Typography>
                                                    </Stack>
                                                </Grid>
                                                <Grid item xs={12} md={6}>
                                                    <Stack spacing={0.5}>
                                                        <Typography color="secondary">Email</Typography>
                                                        <Typography>
                                                            {data.empEmail}
                                                        </Typography>
                                                    </Stack>
                                                </Grid>
                                            </Grid>
                                        </ListItem>
                                        <ListItem divider={!matchDownMD}>
                                            <Grid container spacing={3}>
                                                <Grid item xs={12} md={6}>
                                                    <Stack spacing={0.5}>
                                                        <Typography color="secondary">Employee Name</Typography>
                                                        <Typography>{data.firstName} {data.lastName}</Typography>
                                                    </Stack>
                                                </Grid>
                                                <Grid item xs={12} md={6}>
                                                    <Stack spacing={0.5}>
                                                        <Typography color="secondary">NIC</Typography>
                                                        <Typography>
                                                            {data.nic}
                                                        </Typography>
                                                    </Stack>
                                                </Grid>
                                            </Grid>
                                        </ListItem>
                                        <ListItem divider={!matchDownMD}>
                                            <Grid container spacing={3}>
                                                <Grid item xs={12} md={6}>
                                                    <Stack spacing={0.5}>
                                                        <Typography color="secondary">User Role</Typography>
                                                        <Typography>{data.userRoleName}</Typography>
                                                    </Stack>
                                                </Grid>
                                                <Grid item xs={12} md={6}>
                                                    <Stack spacing={0.5}>
                                                        <Typography color="secondary">Branch</Typography>
                                                        <Typography>
                                                            {data.branchName}
                                                        </Typography>
                                                    </Stack>
                                                </Grid>
                                            </Grid>
                                        </ListItem>
                                        <ListItem divider={!matchDownMD}>
                                            <Grid container spacing={3}>
                                                <Grid item xs={12} md={6}>
                                                    <Stack spacing={0.5}>
                                                        <Typography color="secondary">Contact Number</Typography>
                                                        <Typography>{data.empMobileNumber}</Typography>
                                                    </Stack>
                                                </Grid>
                                                <Grid item xs={12} md={6}>
                                                    <Stack spacing={0.5}>
                                                        <Typography color="secondary">User Type</Typography>
                                                        <Typography>{data.userTypeName}</Typography>
                                                    </Stack>
                                                </Grid>
                                            </Grid>
                                        </ListItem>
                                    </List>
                                </MainCard>
                            </Stack>
                        </Grid>
                    </Grid>
                </Transitions>
            </TableCell>
        </TableRow>
    );
};

export default ProvinceCodeView;
