import { useEffect, useState } from 'react';

// material-ui
import {
    Autocomplete,
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField
} from '@mui/material';
import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports

// assets
import { userProps } from 'pages/hr/user-management/user/types/types';
import { useDispatch, useSelector } from 'store';
import { fetchBranchList } from 'store/reducers/branch';
import { fetchDepartmentList } from 'store/reducers/department';
import { addUser, updateUser } from 'store/reducers/users';
import { fetchUserRoleList } from 'store/reducers/user-role';
import { fetchUserTypeList } from 'store/reducers/user-type';
import { Branches } from 'types/branch';
import { DeptType } from 'types/department';
import { UserRolesType } from 'types/user-role';
import { UserType } from 'types/user-type';
import AlertUserDelete from './AlertUserDelete';

// types

// constant
const getInitialValues = (user: FormikValues | null) => {

    const newUser = {
        branchId: undefined,
        comId: 1,
        departmentId: undefined,
        description: '',
        empEmail: '',
        firstName: '',
        isExistingCompany: true,
        lastName: '',
        empMobileNumber: '',
        nic: '',
        referenceNo: '',
        userRoleId: undefined,
        userTypeId: undefined
    }

    if (user) {
        return _.merge({}, newUser, user);
    }

    return newUser;
};

// ==============================|| CUSTOMER ADD / EDIT ||============================== //

export interface Props {
    user?: userProps
    onCancel: () => void;
}



const AddEditUser = ({ user, onCancel }: Props) => {
    const theme = useTheme();



    //  MAP USER_ROLES

    const [loginTypeId, setLoginTypeId] = useState<number | null>(null);

    const isCreating = !user;

    const UserSchema = Yup.object().shape({
        userTypeId: Yup.string().required('User type ID is required'),
        userRoleId: Yup.number().required('User role ID is required'),
        referenceNo: Yup.string(),
        nic: loginTypeId === 3 ? Yup.string()
            .test('nic', 'Invalid NIC', (value) => {
                return /^[0-9]{9}[vVxX]$/.test(value!) || /^[0-9]{12}$/.test(value!);
            })
            .required('NIC is required') : Yup.string(),
        empEmail: loginTypeId === 3 ? Yup.string().email('Invalid Email address').required('Email is required') : Yup.string().email('Invalid Email address'),
        empMobileNumber: loginTypeId === 3 ? Yup.string()
            .matches(/^(07|08)\d{8}$|^\+947\d{8}$/, 'Invalid mobile number')
            .required('Mobile number is required') : Yup.string().matches(/^(07|08)\d{8}$|^\+947\d{8}$/, 'Invalid mobile number'),
        firstName: Yup.string()
            .matches(/^[A-Za-z]+$/, 'First name must contain only letters')
            .required('First name is required'),
        lastName: Yup.string()
            .matches(/^[A-Za-z]+$/, 'Last name must contain only letters')
            .required('Last name is required'),
        branchId: Yup.number().required('Branch ID is required'),
        departmentId: Yup.number().required('Department ID is required'),

    });

    const formik = useFormik({
        initialValues: getInitialValues(user!),
        validationSchema: UserSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (user) {
                    // put
                    dispatch(updateUser({
                        ...values,
                        mobileNo: values.empMobileNumber,
                        email: values.empEmail
                    }))
                } else {
                    // post
                    dispatch(addUser({
                        branchId: values.branchId,
                        comId: 1,
                        departmentId: values.departmentId,
                        email: values.empEmail,
                        firstName: values.firstName,
                        isExistingCompany: true,
                        lastName: values.lastName,
                        mobileNo: values.empMobileNumber,
                        nic: values.nic,
                        referenceNo: values.referenceNo,
                        userRoleId: values.userRoleId,
                        userTypeId: values.userTypeId
                    }))
                }
                resetForm()
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    //USER ROLE DROP DOWN
    const dispatch = useDispatch();



    //SELECT ROLES FOR DROPDOWN
    const { selectUserRoles, Rolesuccess } = useSelector((state) => state.userRole);
    useEffect(() => {
        dispatch(fetchUserRoleList());
    }, [Rolesuccess, dispatch]);

    //USER Type DROP DOWN



    //SELECT TYPE FOR DROPDOWN
    const { selectUserTypes, typesuccess } = useSelector((state) => state.userType);
    useEffect(() => {
        dispatch(fetchUserTypeList());
    }, [typesuccess, dispatch]);

    useEffect(() => {
        const selectedUserType = selectUserTypes?.find((option: UserType) => option.userTypeId === formik.values.userTypeId);
        if (selectedUserType) {
            setLoginTypeId(selectedUserType.loginTypeId!); // Assuming loginTypeId is a part of the UserType object
        }
    }, [formik.values.userTypeId, selectUserTypes]);


    //SELECT BRANCH FOR DROPDOWN
    const { selectBranches, success } = useSelector((state) => state.branch);
    useEffect(() => {
        dispatch(fetchBranchList());
    }, [success, dispatch]);

    //SELECT DEPARTMENT FOR DROPDOWN
    const { selectDepartments, deptSuccess } = useSelector((state) => state.department);
    useEffect(() => {
        dispatch(fetchDepartmentList());
    }, [deptSuccess, dispatch]);



    const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

    const [openAlert, setOpenAlert] = useState(false);

    const handleAlertClose = () => {
        setOpenAlert(!openAlert);
        onCancel();
    };
    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{user ? 'Edit User' : 'Add New User'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                    <Grid container spacing={3}>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="userTypeId">User Type <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <Autocomplete
                                                    fullWidth
                                                    id="userTypeId"
                                                    value={selectUserTypes?.find((option1: UserType) => option1.userTypeId === formik.values.userTypeId) || null}
                                                    onChange={(event: any, newValue: UserType | null) => {
                                                        formik.setFieldValue('userTypeId', newValue?.userTypeId);
                                                        setLoginTypeId(newValue?.loginTypeId || null); // Update loginTypeId state
                                                    }}
                                                    options={selectUserTypes || []}
                                                    getOptionLabel={(item) => `${item.userCategory}`}
                                                    renderInput={(params) => (
                                                        <TextField
                                                            {...params}
                                                            placeholder="Select User Type"
                                                            sx={{ '& .MuiAutocomplete-input.Mui-disabled': { WebkitTextFillColor: theme.palette.text.primary } }}
                                                        />

                                                    )}
                                                />
                                                {formik.touched.userTypeId && formik.errors.userTypeId && (
                                                    <FormHelperText error id="helper-text-userTypeId">
                                                        {formik.errors.userTypeId}
                                                    </FormHelperText>
                                                )}
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="userRoleId">User Role <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <Autocomplete
                                                    fullWidth
                                                    id="userRoleId"
                                                    value={selectUserRoles?.find((option: UserRolesType) => option.userRoleId === formik.values.userRoleId) || null}
                                                    onChange={(event: any, newValue: UserRolesType | null) => {
                                                        formik.setFieldValue('userRoleId', newValue?.userRoleId);
                                                    }}
                                                    options={selectUserRoles || []}
                                                    getOptionLabel={(item) => `${item.userRoleName}`}
                                                    renderInput={(params) => (
                                                        <TextField
                                                            {...params}
                                                            placeholder="Select User Role"
                                                            sx={{ '& .MuiAutocomplete-input.Mui-disabled': { WebkitTextFillColor: theme.palette.text.primary } }}
                                                        />
                                                    )}
                                                />
                                                {formik.touched.userRoleId && formik.errors.userRoleId && (
                                                    <FormHelperText error id="helper-text-userRoleId">
                                                        {formik.errors.userRoleId}
                                                    </FormHelperText>
                                                )}
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="referenceNo">Reference No / Employee No </InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="referenceNo"
                                                    placeholder="Enter Reference No / Employee No"
                                                    {...getFieldProps('referenceNo')}
                                                    error={Boolean(touched.referenceNo && errors.referenceNo)}
                                                    helperText={touched.referenceNo && errors.referenceNo}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="nic">NIC <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="nic"
                                                    placeholder="Enter NIC"
                                                    {...getFieldProps('nic')}
                                                    error={Boolean(touched.nic && errors.nic)}
                                                    helperText={touched.nic && errors.nic}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="empEmail">  Email <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    type='email'
                                                    fullWidth
                                                    id="empEmail"
                                                    placeholder="Enter Email"
                                                    {...getFieldProps('empEmail')}
                                                    error={Boolean(touched.empEmail && errors.empEmail)}
                                                    helperText={touched.empEmail && errors.empEmail}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="empMobileNumber">Mobile Number <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="empMobileNumber"
                                                    placeholder="07XXXXXXXX"
                                                    {...getFieldProps('empMobileNumber')}
                                                    error={Boolean(touched.empMobileNumber && errors.empMobileNumber)}
                                                    helperText={touched.empMobileNumber && errors.empMobileNumber}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="firstName">First Name <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="firstName"
                                                    placeholder="Enter First Name"
                                                    {...getFieldProps('firstName')}
                                                    error={Boolean(touched.firstName && errors.firstName)}
                                                    helperText={touched.firstName && errors.firstName}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="lastName">Last Name <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="lastName"
                                                    placeholder="Enter Last Name"
                                                    {...getFieldProps('lastName')}
                                                    error={Boolean(touched.lastName && errors.lastName)}
                                                    helperText={touched.lastName && errors.lastName}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="branchId">Branch <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <Autocomplete
                                                    fullWidth
                                                    id="branchId"
                                                    value={selectBranches?.find((option1: Branches) => option1.branchId === formik.values.branchId) || null}
                                                    onChange={(event: any, newValue: Branches | null) => {
                                                        formik.setFieldValue('branchId', newValue?.branchId);
                                                    }}
                                                    options={selectBranches || []}
                                                    getOptionLabel={(item) => `${item.branchName}`}
                                                    renderInput={(params) => (
                                                        <TextField
                                                            {...params}
                                                            placeholder="Select User Type"
                                                            sx={{ '& .MuiAutocomplete-input.Mui-disabled': { WebkitTextFillColor: theme.palette.text.primary } }}
                                                        />
                                                    )}
                                                />
                                                {formik.touched.branchId && formik.errors.branchId && (
                                                    <FormHelperText error id="helper-text-branchId">
                                                        {formik.errors.branchId}
                                                    </FormHelperText>
                                                )}
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="departmentId">Department <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <Autocomplete
                                                    fullWidth
                                                    id="departmentId"
                                                    value={selectDepartments?.find((option1: DeptType) => option1.departmentId === formik.values.departmentId) || null}
                                                    onChange={(event: any, newValue: DeptType | null) => {
                                                        formik.setFieldValue('departmentId', newValue?.departmentId);
                                                    }}
                                                    options={selectDepartments || []}
                                                    getOptionLabel={(item) => `${item.deptName}`}
                                                    renderInput={(params) => (
                                                        <TextField
                                                            {...params}
                                                            placeholder="Select Department"
                                                            sx={{ '& .MuiAutocomplete-input.Mui-disabled': { WebkitTextFillColor: theme.palette.text.primary } }}
                                                        />
                                                    )}
                                                />

                                                {formik.touched.departmentId && formik.errors.departmentId && (
                                                    <FormHelperText error id="helper-text-departmentId">
                                                        {formik.errors.departmentId}
                                                    </FormHelperText>
                                                )}
                                            </Stack>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 2.5 }}>
                            <Grid container justifyContent="space-between" alignItems="center">
                                <Grid item>
                                </Grid>
                                <Grid item>
                                    <Stack direction="row" spacing={2} alignItems="center">
                                        <Button color="error" onClick={onCancel}>
                                            Cancel
                                        </Button>
                                        <Button type="submit" variant="contained" disabled={isSubmitting}>
                                            {user ? 'Edit' : 'Add'}
                                        </Button>
                                    </Stack>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </Form>
                </LocalizationProvider>
            </FormikProvider>
            {!isCreating && <AlertUserDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={user.userId!} />}
        </>
    );

}

export default AddEditUser;
