import { Column } from 'react-table';
import { HeaderGroup } from 'react-table';
import { RoleRestrictionListdata, RoleRestrictions } from 'types/role-restrictions';
export interface TableProps {
    columns: Column[];
    data: [];
    handleAddEdit: () => void;
    getHeaderProps: (column: HeaderGroup) => {};

}

export interface TableHeaderProps {
    headerGroups: HeaderGroup[];

}

export interface dataProps extends RoleRestrictionListdata { }

export interface ReactTableProps {
    columns: Column[]
    data: dataProps[]
    handleAddEdit: () => void
    selectedData?: React.Dispatch<React.SetStateAction<number[] | undefined>>
    selectdata?: number[]
    getHeaderProps: (column: HeaderGroup) => {};
}

export interface userProps extends RoleRestrictions { }

