import { useEffect, useMemo, useState } from 'react';

// material-ui
import {
    Button,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableRow
} from '@mui/material';
import { alpha, useTheme } from '@mui/material/styles';

// third-party
import { Cell, Column, HeaderGroup, Row, useFilters, useGlobalFilter, usePagination, useRowSelect, useSortBy, useTable } from 'react-table';

// project import
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import { IndeterminateCheckbox, TablePagination, TableRowSelection } from 'components/third-party/ReactTable';
import {
    GlobalFilter
} from 'utils/react-table';

// assets

//types  
import { Grid } from '@mui/material';
import { dispatch, useSelector } from 'store';
import { addRoleRestrictionSuccess, fetchRolesSuccess, toInitialState, toResetIsActionSuccessState } from 'store/reducers/role-restrictions';
import { openSnackbar } from 'store/reducers/snackbar';
import { ThemeDirection } from 'types/config';
import TableHeader from '../multiple-branches/TableHeader';
import { ReactTableProps, dataProps } from './types/types';
import { useNavigate } from 'react-router';
import { AddActivityLog } from 'store/reducers/note-log';
import useAuth from 'hooks/useAuth';

// ==============================|| REACT TABLE ||============================== //

function ReactTable({ columns, data, getHeaderProps, selectedData, selectdata }: ReactTableProps) {
    const theme = useTheme();

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        page,
        prepareRow,
        gotoPage,
        setPageSize,
        state,
        preGlobalFilteredRows,
        setGlobalFilter,
        state: { selectedRowIds, pageIndex, pageSize },
    } = useTable(
        {
            columns,
            data,
            getHeaderProps,
            initialState: { pageIndex: 0, pageSize: 10 },
            sortBy: [
                {
                    id: 'id',
                    desc: false
                }
            ]
        },
        useFilters,
        useGlobalFilter,
        useSortBy,
        usePagination,
        useFilters,
        useRowSelect,
        (hooks) => {
            hooks.allColumns.push((columns: Column[]) => [
                {
                    id: 'row-selection-chk',
                    accessor: 'Selection',
                    disableSortBy: true,
                    Header: ({ getToggleAllPageRowsSelectedProps }) => (
                        <IndeterminateCheckbox indeterminate {...getToggleAllPageRowsSelectedProps()} />
                    ),
                    Cell: ({ row }: any) => <IndeterminateCheckbox {...row.getToggleRowSelectedProps()} />
                },
                ...columns
            ]);
        }
    );

    const statedata = selectedRowIds

    const sortingRow = rows.slice(0, 9);
    let sortedData = sortingRow.map((d: Row) => d.original);
    Object.keys(sortedData).forEach((key: string) => sortedData[Number(key)] === undefined && delete sortedData[Number(key)]);

    //const [firstTrueKey, setKeyId] = useState<number>(0)
    const [resultArray, setResultArray] = useState<number[]>([]);

    useEffect(() => {

        const numbers = Object.keys(statedata).map(Number);

        // Update the result array
        setResultArray(numbers);

    }, [statedata]);

    useEffect(() => {

        let munuids = resultArray?.map((item) => (data[item]?.menuActionId!))

        if (typeof selectedData != "undefined") {
            selectedData(munuids!)
        }

    }, [resultArray])

    return (
        <ScrollX>

            <TableRowSelection selected={Object.keys(selectedRowIds).length} />

            <Stack
                spacing={3}
                sx={{
                    ...(theme.direction === ThemeDirection.RTL && {
                        '.MuiTable-root': { width: { xs: '930px', sm: 'inherit' } },
                        pre: { width: { xs: '930px', sm: 'inherit' }, overflowX: 'unset' }
                    })
                }}
            >
                <Stack direction="row" spacing={2} justifyContent="space-between" sx={{ padding: 2 }}>
                    <GlobalFilter preGlobalFilteredRows={preGlobalFilteredRows} globalFilter={state.globalFilter} setGlobalFilter={setGlobalFilter} />
                </Stack>
                <Table {...getTableProps()}>

                    <TableHeader headerGroups={headerGroups} />

                    <TableBody {...getTableBodyProps()}>

                        {page.map((row: Row, i: number) => {
                            prepareRow(row);
                            return (
                                <TableRow
                                    {...row.getRowProps()}
                                    onClick={() => {
                                        row.toggleRowSelected();
                                    }}
                                    sx={{ cursor: 'pointer', bgcolor: row.isSelected ? alpha(theme.palette.primary.lighter, 0.35) : 'inherit' }}
                                >
                                    {row.cells.map((cell: Cell) => (
                                        <TableCell {...cell.getCellProps([{ className: cell.column.className }])}>{cell.render('Cell')}</TableCell>
                                    ))}
                                </TableRow>
                            );
                        })}

                        <TableRow>
                            <TableCell sx={{ p: 2 }} colSpan={12}>
                                <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
                            </TableCell>

                        </TableRow>

                    </TableBody>
                </Table>
            </Stack>
        </ScrollX>
    );
}

// ==============================|| RestrictedRoleAccessTable ||============================== //

interface ListContainerProps {
    userRoleid?: number
}

const RestrictedRoleAccessTable = (containerProps: ListContainerProps) => {

    const navigate = useNavigate()
    const { user } = useAuth()
    const {
        userRoleid
    } = containerProps

    // table
    const [tabledata, settableData] = useState<dataProps[]>([])
    const { isLoading, rolAccessList, isPostSuccess, postsuccess, error, isActionSuccess } = useSelector((state) => state.roleRestriction)
    const [selectedData, setselectedData] = useState<number[]>()

    const columns2 = useMemo(
        () =>
            [
                {
                    Header: 'Menu',
                    accessor: 'menuName',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.menuName === undefined || row.values.menuName === null || row.values.menuName === '') {
                            return <>-</>
                        }
                        if (typeof row.values.menuName === 'string') {
                            return <>{row.values.menuName}</>;
                        }
                        if (typeof row.values.menuName === 'number') {
                            return <>{row.values.menuName}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'Action',
                    accessor: 'menuActionName',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.menuActionName === undefined || row.values.menuActionName === null || row.values.menuActionName === '') {
                            return <>-</>
                        }
                        if (typeof row.values.menuActionName === 'string') {
                            return <>{row.values.menuActionName}</>;
                        }
                        if (typeof row.values.menuActionName === 'number') {
                            return <>{row.values.menuActionName}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                },
                {
                    Header: 'URL',
                    accessor: 'url',
                    Cell: ({ row }: { row: Row }) => {
                        if (row.values.url === undefined || row.values.url === null || row.values.url === '') {
                            return <>-</>
                        }
                        if (typeof row.values.url === 'string') {
                            return <>{row.values.url}</>;
                        }
                        if (typeof row.values.url === 'number') {
                            return <>{row.values.url}</>;
                        }
                        // Handle any other data types if necessary
                        return <>-</>;
                    }
                }
            ] as Column[],
        []
    );

    // ----------------------- | API Call - Role Restriction | ---------------------
    const [selectedIdsString, setSelectedIds] = useState<string>()

    useEffect(() => {
        if (typeof userRoleid == "undefined") return
        dispatch(fetchRolesSuccess(userRoleid!))
    }, [userRoleid, isPostSuccess])

    const onSubmit = () => {
        if (!selectedData || selectedData.length === 0) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: 'There are no Role restrictions for this Role',
                    variant: 'alert',
                    alert: {
                        color: 'error'
                    },
                    close: true
                })
            );
        } else {
            console.log(userRoleid);

            let submitdata = selectedData?.map((item) => ({
                menuActionId: item!,
                userRoleId: userRoleid!
            }));
            setSelectedIds(selectedData.join(', ')); // Join IDs into a string


            dispatch(addRoleRestrictionSuccess(submitdata!));
        }
    };

    useEffect(() => {
        if (!rolAccessList) {
            settableData([])
            return
        }
        if (rolAccessList == null) {
            settableData([])
            return
        }
        settableData(rolAccessList.roleOptions!)
    }, [rolAccessList])

    useEffect(() => {
        if (error != null) {

            let defaultErrorMessage = "ERROR";
            // @ts-ignore
            const errorExp = error as Template1Error
            if (errorExp.message) {
                defaultErrorMessage = errorExp.message
            }
            dispatch(
                openSnackbar({
                    open: true,
                    message: defaultErrorMessage,
                    variant: 'alert',
                    alert: {
                        color: 'error'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [error]);

    useEffect(() => {
        if (postsuccess != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: postsuccess,
                    variant: 'alert',
                    alert: {
                        color: 'success'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [isPostSuccess])
    useEffect(() => {
        if (isActionSuccess != null) {
            let actionId, actionName, keyValueId, keyValue, description;

            switch (isActionSuccess) {
                case 'LIST':
                    actionId = 2;
                    actionName = 'LIST';
                    keyValueId = 0;
                    keyValue = 'N/A';
                    description = 'Get All Menu List'
                    break;
                case 'APPROVE':
                    actionId = 6;
                    actionName = 'APPROVE';
                    keyValueId = 0;
                    keyValue = 'N/A';
                    description = `Grant Permissions: ${selectedIdsString}`;
                    break;
                case 'UPDATE':
                    actionId = 3;
                    actionName = 'UPDATE';
                    keyValue = selectedIdsString || 'N/A';
                    description = `Edit User Details: ${selectedIdsString}`;
                    break;
                case 'REJECT':
                    actionId = 7;
                    actionName = 'REJECT';
                    keyValueId = userRoleid || 0;
                    keyValue = selectedIdsString || 'N/A';
                    description = `Restrict Actions: ${selectedIdsString}`;
                    break;
                default:
                    return; // Exit early if no valid action is found
            }
            dispatch(AddActivityLog({
                actionId: actionId,
                actionName: actionName,
                branchId: user?.branchList?.[0]?.branchId ?? undefined,
                companyId: user?.companyId,
                deptId: user?.departmentList?.[0]?.departmentId ?? undefined,
                description: description,
                keyValue: keyValue,
                keyValueId: keyValueId,
                menuId: 6,
                menuName: "Role Restriction",
                deptName: user?.departmentList?.[0]?.deptName ?? undefined
            }));

            dispatch(toResetIsActionSuccessState());
        }

    }, [isActionSuccess]);

    if (isLoading) {
        return <>Loading...</>
    }

    return (
        <>
            <MainCard content={false}>
                <ScrollX>
                    <ReactTable selectedData={setselectedData} selectdata={selectedData} handleAddEdit={() => { }} columns={columns2} data={tabledata} getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()} />
                </ScrollX>
            </MainCard>
            <Grid item xs={12} sm={12}>
                <Stack direction="row" spacing={2} justifyContent="right" alignItems="center" sx={{ p: 2.5, pb: 0 }}>
                    <Button color="error" onClick={() => { navigate(`/home/dashboard`) }}>
                        Cancel
                    </Button>
                    <Button variant="contained" sx={{ textTransform: 'none' }} onClick={() => { onSubmit() }}>
                        Grant Permission
                    </Button>
                </Stack>
            </Grid>
        </>
    );
}

export default RestrictedRoleAccessTable;
