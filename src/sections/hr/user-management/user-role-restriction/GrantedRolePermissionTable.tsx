import { useEffect, useMemo, useState } from 'react';

// material-ui
import {
    Chip,
    IconButton,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Tooltip,
    useTheme
} from '@mui/material';

// third-party
import { Cell, Column, HeaderGroup, Row, useFilters, useGlobalFilter, usePagination,useRowSelect, useSortBy, useTable } from 'react-table';

// project import
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import { EmptyTable, HeaderSort, TablePagination, TableRowSelection } from 'components/third-party/ReactTable';
import {
    DefaultColumnFilter,
    GlobalFilter,
    renderFilterTypes
} from 'utils/react-table';

// assets
import { CheckCircleTwoTone, CloseCircleTwoTone } from '@ant-design/icons';

//types  
import makeData from 'data/react-table';
import { dispatch, useSelector } from 'store';
import { fetchRolesSuccessTwo, restrictionDisable, toInitialState, toResetIsActionSuccessState } from 'store/reducers/role-restrictions';
import { openSnackbar } from 'store/reducers/snackbar';
import { ReactTableProps, dataProps } from './types/types';
import useAuth from 'hooks/useAuth';
import { AddActivityLog } from 'store/reducers/note-log';
import { RoleRestrictionListdata } from 'types/role-restrictions';

// ==============================|| REACT TABLE ||============================== //

interface ListContainerProps {
    userRoleid?: number
}

function ReactTable({ columns, data,getHeaderProps }: ReactTableProps) {
    const filterTypes = useMemo(() => renderFilterTypes, []);
    const defaultColumn = useMemo(() => ({ Filter: DefaultColumnFilter }), []);

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
        preGlobalFilteredRows,
        setGlobalFilter,
        globalFilter,
        page,
        gotoPage,
        setPageSize,
        state: { pageIndex, pageSize,selectedRowIds }
    } = useTable(
        {
            columns,
            data,
            defaultColumn,
            filterTypes,

            initialState: { pageIndex: 0, pageSize: 10 }
        },
        useGlobalFilter,
        useFilters,
        
        useSortBy,
        usePagination,
        useRowSelect
    );

    return (
        <>
        <TableRowSelection selected={Object.keys(selectedRowIds).length} />
            <Stack direction="row" spacing={2} justifyContent="space-between" sx={{ padding: 2 }}>
                <GlobalFilter preGlobalFilteredRows={preGlobalFilteredRows} globalFilter={globalFilter} setGlobalFilter={setGlobalFilter} />
                <Stack direction="row" alignItems="center" spacing={1}>

                </Stack>
            </Stack>

            <Table {...getTableProps()}>
                <TableHead sx={{ borderTopWidth: 2 }}>
                    {headerGroups.map((headerGroup) => (
                        <TableRow {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map((column: HeaderGroup) => (
                                <TableCell {...column.getHeaderProps([{ className: column.className },  getHeaderProps(column)])}>
                                    {/* {column.render('Header')} */}
                                    <HeaderSort column={column} />
                                    </TableCell>
                            ))}
                        </TableRow>
                    ))}
                </TableHead>
                <TableBody {...getTableBodyProps()}>
                    {page.length > 0 ? (
                        page.map((row, i) => {
                            prepareRow(row);
                            return (
                                <TableRow {...row.getRowProps()}>
                                    {row.cells.map((cell: Cell) => (
                                        <TableCell {...cell.getCellProps([{ className: cell.column.className }])}>{cell.render('Cell')}</TableCell>
                                    ))}
                                </TableRow>
                            );
                        })
                    ) : (
                        <EmptyTable msg="No Data" colSpan={12} />
                    )}
                    <TableRow>
                        <TableCell sx={{ p: 2 }} colSpan={12}>
                            <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
                        </TableCell>
                    </TableRow>
                </TableBody>
            </Table>
        </>
    );
}

// ==============================|| GrantedRolePermissionTable ||============================== //

const GrantedRolePermissionTable = (containerProps: ListContainerProps) => {

    const {
        userRoleid
    } = containerProps
    const { user } =useAuth()

    const theme = useTheme();
    const [statusData] = useState(makeData(200).map(() => ({ authorized: false, expanded: false })));

    // table
    const [tabledatatwo, settableDatatwo] = useState<dataProps[]>([])
    const { roleRestrictionList, isLoading, error, disablepostsuccess, isPostSuccess, success,isActionSuccess} = useSelector((state) => state.roleRestriction)

    const columns1 = useMemo(
        () =>
            [
                {
                    Header: 'Menu Name',
                    accessor: 'usTMenuAction.name',
                    Cell: ({ row }: { row: Row }) => {

                        const rowdata: dataProps = row?.original
                        return <>{rowdata?.usTMenuAction?.name}</>;
                    },
                },
                {
                    Header: 'Action Name',
                    accessor: 'usTMenuAction.usMAction.actionName',
                    Cell: ({ row }: { row: Row }) => {

                        const rowdata: dataProps = row?.original
                        return <>{rowdata?.usTMenuAction?.usMAction?.actionName}</>;
                    },
                },
                {
                    Header: 'URL',
                    accessor: 'usTMenuAction.url',
                    Cell: ({ row }: { row: Row }) => {

                        const rowdata: dataProps = row?.original
                        return <>{rowdata?.usTMenuAction?.url}</>;
                    },
                },
                {
                    Header: 'Status',
                    accessor: 'statusId',
                    Cell: ({ row }: { row: Row }) => (
                        <Chip
                            color={statusData[row.index].authorized ? 'primary' : 'info'}
                            label={row.values?.statusId == 4 ? 'Restricted' : 'Granted'}
                            size="small"
                            variant="light"
                        />
                    ),
                },
                {
                    id: "actions",
                    Header: 'Actions',
                    accessor: 'actions',
                    className: 'cell-left',
                    Cell: ({ row }: { row: Row }) => {

                        const isApproved = statusData[row.index].authorized;
                        const collapseIcon = row.values?.statusId == 4 ? (
                            <CheckCircleTwoTone twoToneColor={theme.palette.primary.main} />

                        ) : (
                            <CloseCircleTwoTone twoToneColor={theme.palette.success.main} />
                        );

                        const tooltipTitle = isApproved ? 'Approve' : 'Reject';

                        return (
                            <Stack direction="row" spacing={0}>

                                <Tooltip title={tooltipTitle}>
                                    <IconButton
                                        color="secondary"
                                        onClick={() => {
                                            //@ts-ignore
                                            let data: RoleRestrictionListdata = row.original;
                                            setMenu(data)
                                            onDisable(data?.usTMenuAction?.menuActionId, data?.usMUserRole?.userRoleId);
                                        }}
                                    >
                                        {collapseIcon}
                                    </IconButton>
                                </Tooltip>

                            </Stack>
                        );
                    }
                }
            ] as Column[],
        [theme, statusData, userRoleid]
    );
    // ----------------------- | API Call - Role Restriction| ---------------------
    const [menu, setMenu] = useState<RoleRestrictionListdata>();
    useEffect(() => {
        if (typeof userRoleid == "undefined") return
        dispatch(fetchRolesSuccessTwo(userRoleid!))
    }, [success, userRoleid, isPostSuccess])

    useEffect(() => {
        if (!roleRestrictionList) {
            settableDatatwo([])
            return
        }
        if (roleRestrictionList == null) {
            settableDatatwo([])
            return
        }

        settableDatatwo(roleRestrictionList.result!)
    }, [roleRestrictionList])

    useEffect(() => {
        if (disablepostsuccess != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: disablepostsuccess,
                    variant: 'alert',
                    alert: {
                        color: 'success'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [isPostSuccess])

    useEffect(() => {
        if (error != null) {

            let defaultErrorMessage = "ERROR";
            // @ts-ignore
            const errorExp = error as Template1Error
            if (errorExp.message) {
                defaultErrorMessage = errorExp.message
            }
            dispatch(
                openSnackbar({
                    open: true,
                    message: defaultErrorMessage,
                    variant: 'alert',
                    alert: {
                        color: 'error'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [error]);

    //Access Roles Disable Function
    const onDisable = (menuActionId: any, userRoleId: any) => {
        dispatch(restrictionDisable(menuActionId, userRoleId))
    }

    useEffect(()=>{
        if (isActionSuccess != null) {
            let actionId, actionName ,keyValueId,keyValue,description;
    
            switch (isActionSuccess) {
                case 'LIST':
                    actionId = 2;
                    actionName = 'LIST';
                    keyValueId = 0 ;
                    keyValue = 'N/A' ;
                    description='Get All Menu List'
                    break;
                case 'APPROVE':
                    actionId = 6;
                    actionName = 'APPROVE';
                    keyValueId = 0 ;
                    keyValue = 'N/A' ;
                    description='Grant Permission'
                    break;
                case 'UPDATE':
                    actionId = 3;
                    actionName = 'UPDATE';
                    keyValueId = user?.userRoleId! || 0 ;
                    keyValue = menu?.menuActionId||'N/A' ;
                    description=`Edit User Details : ${menu?.menuActionId} `
                    break;
                case 'REJECT':
                    actionId = 7;
                    actionName = 'REJECT';
                    keyValueId = userRoleid || 0;
                    keyValue = menu?.menuActionId||'N/A' ;
                    description=`Restrict Permission: ${menu?.usTMenuAction?.menuActionId}`
                    
                    break;
                default:
                    return; // Exit early if no valid action is found
            }
            dispatch(AddActivityLog({
                actionId: actionId,
                actionName: actionName,
                branchId: user?.branchList?.[0]?.branchId ?? undefined,
                companyId: user?.companyId,
                deptId: user?.departmentList?.[0]?.departmentId ?? undefined,
                description: description,
                keyValue: keyValue.toString(),
                keyValueId: keyValueId,
                menuId: 6,
                menuName: "Role Restriction",
                deptName: user?.departmentList?.[0]?.deptName ?? undefined
            }));
    
            dispatch(toResetIsActionSuccessState());
        }
    
    },[isActionSuccess]);

    if (isLoading) {
        return <>Loading...</>
    }

    return (
        <>
            <MainCard content={false}>
                <ScrollX>
                    <ReactTable
                        handleAddEdit={() => { }} columns={columns1} data={tabledatatwo} getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()}
                    />
                </ScrollX>
            </MainCard>
        </>
    );
}

export default GrantedRolePermissionTable;
