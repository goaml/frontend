import { useEffect, useMemo, useState } from 'react';

// material-ui
import {
    Chip,
    IconButton,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Tooltip,
    useTheme
} from '@mui/material';

// third-party
import { Cell, HeaderGroup, Row, useFilters, useGlobalFilter, usePagination, useTable } from 'react-table';

// project import
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import { EmptyTable, TablePagination } from 'components/third-party/ReactTable';
import {
    DefaultColumnFilter,
    GlobalFilter,
    renderFilterTypes
} from 'utils/react-table';

// assets
import { CheckCircleTwoTone, CloseCircleTwoTone } from '@ant-design/icons';

//types  
import makeData from 'data/react-table';
import { dispatch, useSelector } from 'store';
import { openSnackbar } from 'store/reducers/snackbar';
import { fetchRestrictionSuccessTwo, restrictionDisable, toInitialState, toResetIsActionSuccessState } from 'store/reducers/user-restrictions';
import { ReactTableProps, userRqueryParamsProps } from './types/types';
import useAuth from 'hooks/useAuth';
import { UserResPostReq } from 'types/user-restrictions';
import { AddActivityLog } from 'store/reducers/note-log';

// ==============================|| REACT TABLE ||============================== //
interface ListContainerProps {
    userId?: number,
    branchId?: number
}

function ReactTable({ columns, data }: ReactTableProps) {
    const filterTypes = useMemo(() => renderFilterTypes, []);
    const defaultColumn = useMemo(() => ({ Filter: DefaultColumnFilter }), []);

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
        preGlobalFilteredRows,
        setGlobalFilter,
        globalFilter,
        page,
        gotoPage,
        setPageSize,
        state: { pageIndex, pageSize }
    } = useTable(
        {
            columns,
            data,
            defaultColumn,
            filterTypes,

            initialState: { pageIndex: 0, pageSize: 10 }
        },
        useGlobalFilter,
        useFilters,
        usePagination
    );

    return (
        <>
            <Stack direction="row" spacing={2} justifyContent="space-between" sx={{ padding: 2 }}>
                <GlobalFilter preGlobalFilteredRows={preGlobalFilteredRows} globalFilter={globalFilter} setGlobalFilter={setGlobalFilter} />
                <Stack direction="row" alignItems="center" spacing={1}>

                </Stack>
            </Stack>

            <Table {...getTableProps()}>
                <TableHead sx={{ borderTopWidth: 2 }}>
                    {headerGroups.map((headerGroup) => (
                        <TableRow {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map((column: HeaderGroup) => (
                                <TableCell {...column.getHeaderProps([{ className: column.className }])}>{column.render('Header')}</TableCell>
                            ))}
                        </TableRow>
                    ))}
                </TableHead>
                <TableBody {...getTableBodyProps()}>
                    {page.length > 0 ? (
                        page.map((row, i) => {
                            prepareRow(row);
                            return (
                                <TableRow {...row.getRowProps()}>
                                    {row.cells.map((cell: Cell) => (
                                        <TableCell {...cell.getCellProps([{ className: cell.column.className }])}>{cell.render('Cell')}</TableCell>
                                    ))}
                                </TableRow>
                            );
                        })
                    ) : (
                        <EmptyTable msg="No Data" colSpan={12} />
                    )}
                    <TableRow>
                        <TableCell sx={{ p: 2 }} colSpan={12}>
                            <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
                        </TableCell>
                    </TableRow>
                </TableBody>
            </Table>
        </>
    );
}

// ==============================|| GrantedUserPermissionTable ||============================== //

const GrantedUserPermissionTable = (containerProps: ListContainerProps) => {

    const {
        userId, branchId
    } = containerProps

    const theme = useTheme();

    const { user } = useAuth()

    // table
    const [tabledatatwo, settableDatatwo] = useState<any>([])
    const { userRestrictionList, isLoading, isPostSuccess, disablepostsuccess, error ,isActionSuccess} = useSelector((state) => state.userRestriction)
    const [statusData] = useState(makeData(200).map(() => ({ authorized: false, expanded: false })));

    const columns1 = useMemo(
        () => [

            {
                Header: 'Module',
                accessor: 'moduleName'
            },
            {
                Header: 'Menu',
                accessor: 'menuName'
            },
            {
                Header: 'Action',
                accessor: 'actionName',
            },
            {
                Header: 'Status',
                accessor: 'statusId',
                Cell: ({ row }: { row: Row }) => (
                    <Chip
                        color={statusData[row.index].authorized ? 'primary' : 'info'}
                        label={row.values?.statusId == 4 ? 'Restricted' : 'Granted'}
                        size="small"
                        variant="light"
                    />
                ),
            },
            {
                Header: 'Actions',
                disableSortBy: true,
                Cell: ({ row }: { row: Row }) => {

                    const isApproved = statusData[row.index].authorized;
                    const collapseIcon = row.values?.statusId == 4 ? (
                        <CheckCircleTwoTone twoToneColor={theme.palette.primary.main} />

                    ) : (
                        <CloseCircleTwoTone twoToneColor={theme.palette.success.main} />
                    );

                    const tooltipTitle = isApproved ? 'Approve' : 'Reject';



                    return (
                        <Stack direction="row">
                            <Tooltip title={tooltipTitle}>
                                <IconButton
                                    color="secondary"
                                    onClick={() => {
                                        //@ts-ignore
                                        let data: UserResPostReq = row.original;
                                        setMenu(data)
                                        onDisable(data?.menuActionId, userId, branchId);
                                    }}
                                >
                                    {collapseIcon}
                                </IconButton>
                            </Tooltip>
                        </Stack>
                    );
                }
            }
        ],
        [theme, statusData, userId, branchId]
    );

    // ----------------------- | API Call - USER RESTRICTION | ---------------------
    const [menu, setMenu] = useState<UserResPostReq>();

    useEffect(() => {
        if (userId && branchId) {
            const queryParams: userRqueryParamsProps = {
                direction: "asc",
                page: 0,
                per_page: 1000,
                search: "",
                sort: "userId",
                branchId: branchId,
                userId: userId
            }
            dispatch(fetchRestrictionSuccessTwo(queryParams))
        }
    }, [userId, branchId, isPostSuccess])

    useEffect(() => {
        if (!userRestrictionList) {
            settableDatatwo([])
            return
        }
        if (userRestrictionList == null) {
            settableDatatwo([])
            return
        }
        settableDatatwo(userRestrictionList.result!)
    }, [userRestrictionList])

    useEffect(() => {
        if (error != null) {

            let defaultErrorMessage = "ERROR";
            // @ts-ignore
            const errorExp = error as Template1Error
            if (errorExp.message) {
                defaultErrorMessage = errorExp.message
            }
            dispatch(
                openSnackbar({
                    open: true,
                    message: defaultErrorMessage,
                    variant: 'alert',
                    alert: {
                        color: 'error'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [error]);

    useEffect(() => {
        if (disablepostsuccess != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: disablepostsuccess,
                    variant: 'alert',
                    alert: {
                        color: 'success'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [isPostSuccess])

    useEffect(()=>{
        if (isActionSuccess != null) {
            let actionId, actionName ,keyValueId,description,keyValue;
    
            switch (isActionSuccess) {
                case 'LIST':
                    actionId = 2;
                    actionName = 'LIST';
                    keyValueId = 0 ;
                    keyValue = 'N/A' ;
                    description='Get All Menu List'
                    break;
                case 'APPROVE':
                    actionId = 6;
                    actionName = 'APPROVE';
                    keyValueId = 0 ;
                    keyValue = 'N/A' ;
                    description='Grant Permission'
                    break;
                case 'UPDATE':
                    actionId = 3;
                    actionName = 'UPDATE';
                    keyValueId = menu?.userId! || 0 ;
                    keyValue = menu?.menuActionId||'N/A' ;
                    description=`Edit User Details : ${menu?.menuActionId} `
                    break;
                case 'REJECT':
                    actionId = 7;
                    actionName = 'REJECT';
                    keyValueId = userId || 0;
                    keyValue = menu?.menuActionId||'N/0A' ;
                    description=`Restrict Permission: -${menu?.menuActionId}`
                    
                    break;
                default:
                    return; // Exit early if no valid action is found
            }
            dispatch(AddActivityLog({
                actionId: actionId,
                actionName: actionName,
                branchId: user?.branchList?.[0]?.branchId ?? undefined,
                companyId: user?.companyId,
                deptId: user?.departmentList?.[0]?.departmentId ?? undefined,
                description: description,
                keyValue: keyValue.toString(),
                keyValueId: keyValueId,
                menuId: 4,
                menuName: "User Restriction",
                deptName: user?.departmentList?.[0]?.deptName ?? undefined
            }));
    
            dispatch(toResetIsActionSuccessState());
        }
    
    },[isActionSuccess]);
    //Access Roles Disable Function
    const onDisable = (menuActionId: any, userId: any, branchId: any) => {
        dispatch(restrictionDisable(menuActionId, userId, branchId,))
    }

    if (isLoading) {
        return <>Loading...</>
    }

    return (
        <>
            <MainCard content={false}>
                <ScrollX>
                    <ReactTable handleAddEdit={() => { }} columns={columns1} data={tabledatatwo} getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()} />
                </ScrollX>
            </MainCard>
        </>
    );
}

export default GrantedUserPermissionTable;
