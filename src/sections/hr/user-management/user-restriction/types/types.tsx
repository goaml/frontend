import { Column } from 'react-table';
import { HeaderGroup } from 'react-table';
import { UserRestrictions, queryParamsProps } from 'types/user-restrictions';
export interface TableProps {
    columns: Column[];
    data: dataProps[];
    handleAddEdit: () => void;
    selectedData?: React.Dispatch<React.SetStateAction<number[]>>
    getHeaderProps: (column: HeaderGroup) => {};

}

export interface TableHeaderProps {
    headerGroups: HeaderGroup[];

}

export interface userRqueryParamsProps extends queryParamsProps { }

export interface dataProps extends UserRestrictions { }

export interface ReactTableProps {
    columns: Column[]
    data: dataProps[]
    handleAddEdit: () => void
    getHeaderProps: (column: HeaderGroup) => {};
}

export interface userProps extends UserRestrictions { }

