import { useEffect, useMemo, useState } from 'react';

// material-ui
import {
    Button,
    Chip,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableRow
} from '@mui/material';
import { useTheme } from '@mui/material/styles';

// third-party
import { Cell, Column, HeaderGroup, Row, useFilters, useGlobalFilter, usePagination, useRowSelect, useSortBy, useTable } from 'react-table';

// project import
import MainCard from 'components/MainCard';
import ScrollX from 'components/ScrollX';
import { IndeterminateCheckbox, TablePagination, TableRowSelection } from 'components/third-party/ReactTable';
import {
    GlobalFilter
} from 'utils/react-table';

// assets

//types  
import { Grid } from '@mui/material';
import { ThemeDirection } from 'types/config';
import TableHeader from '../multiple-branches/TableHeader';
import { TableProps, userRqueryParamsProps } from './types/types';
import { addUserRestrictionSuccess, fetchRestrictionSuccess, toInitialState, toResetIsActionSuccessState } from 'store/reducers/user-restrictions';
import { dispatch, useSelector } from 'store';
import { openSnackbar } from 'store/reducers/snackbar';
import { useNavigate } from 'react-router';
import { AddActivityLog } from 'store/reducers/note-log';
import useAuth from 'hooks/useAuth';

// ==============================|| REACT TABLE ||============================== //


function ReactTable({ columns, data, getHeaderProps, selectedData }: TableProps) {
    const theme = useTheme();

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        page,
        prepareRow,
        gotoPage,
        setPageSize,
        state,
        preGlobalFilteredRows,
        setGlobalFilter,
        state: { selectedRowIds, pageIndex, pageSize }
    } = useTable(
        {
            columns,
            data,
            getHeaderProps,
            initialState: { pageIndex: 0, pageSize: 10 },
            sortBy: [
                {
                    id: 'id',
                    desc: false
                }
            ]
        },
        useFilters,
        useGlobalFilter,
        useSortBy,
        usePagination,
        useFilters,
        useRowSelect,
        (hooks) => {
            hooks.allColumns.push((columns: Column[]) => [
                {
                    id: 'row-selection-chk',
                    accessor: 'Selection',
                    disableSortBy: true,
                    Header: ({ getToggleAllPageRowsSelectedProps }) => (
                        <IndeterminateCheckbox indeterminate {...getToggleAllPageRowsSelectedProps()} />
                    ),
                    Cell: ({ row }: any) => <IndeterminateCheckbox {...row.getToggleRowSelectedProps()} />
                },
                ...columns
            ]);
        }

    );

    const statedata = selectedRowIds

    const sortingRow = rows.slice(0, 9);
    let sortedData = sortingRow.map((d: Row) => d.original);
    Object.keys(sortedData).forEach((key: string) => sortedData[Number(key)] === undefined && delete sortedData[Number(key)]);

    const [firstTrueKey, setKeyId] = useState<Array<string>>([])
    useEffect(() => {
        const keysArray = Object.keys(statedata);
        setKeyId(keysArray)
    }, [statedata])

    useEffect(() => {
        if (typeof firstTrueKey != "undefined") {
            if (typeof selectedData != "undefined") {
                selectedData(
                    firstTrueKey?.map((item) => (
                        data[Number(item)]?.menuActionId!
                    ))
                )
            }
        }
    }, [firstTrueKey, data, selectedData]);


    return (
        <ScrollX>
            <TableRowSelection selected={Object.keys(selectedRowIds).length} />

            <Stack
                spacing={3}
                sx={{
                    ...(theme.direction === ThemeDirection.RTL && {
                        '.MuiTable-root': { width: { xs: '930px', sm: 'inherit' } },
                        pre: { width: { xs: '930px', sm: 'inherit' }, overflowX: 'unset' }
                    })
                }}
            >
                <Stack direction="row" spacing={2} justifyContent="space-between" sx={{ padding: 2 }}>
                    <GlobalFilter preGlobalFilteredRows={preGlobalFilteredRows} globalFilter={state.globalFilter} setGlobalFilter={setGlobalFilter} />
                </Stack>

                <Table {...getTableProps()}>

                    <TableHeader headerGroups={headerGroups} />

                    <TableBody {...getTableBodyProps()}>

                        {page.map((row: Row) => {
                            prepareRow(row);
                            return (
                                <TableRow {...row.getRowProps()}>
                                    {row.cells.map((cell: Cell) => (
                                        <TableCell {...cell.getCellProps([{ className: cell.column.className }])}>{cell.render('Cell')}</TableCell>
                                    ))}
                                </TableRow>
                            );
                        })}

                        <TableRow>
                            <TableCell sx={{ p: 2 }} colSpan={12}>
                                <TablePagination gotoPage={gotoPage} rows={rows} setPageSize={setPageSize} pageIndex={pageIndex} pageSize={pageSize} />
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </Stack>
        </ScrollX>

    );
}

interface ListContainerProps {
    userId?: number,
    branchId?: number
}

// ==============================|| RestrictedUserAccessTable ||============================== //

const RestrictedUserAccessTable = (containerProps: ListContainerProps) => {

    const navigate = useNavigate()

    const {
        userId, branchId
    } = containerProps

    const { user } = useAuth()
    // table
    const [tabledata, settableData] = useState<any>([])
    const { isLoading, newuserRestrictionList, /*isPostSuccess, postsuccess,*/ error, success, isActionSuccess } = useSelector((state) => state.userRestriction)
    const [selectedData, setselectedData] = useState<Array<number>>([])

    const columns2 = useMemo(
        () => [
            {
                Header: 'Module',
                accessor: 'moduleName'
            },
            {
                Header: 'Menu',
                accessor: 'menuName'
            },
            {
                Header: 'Action',
                accessor: 'actionName',
            },
            {
                Header: 'Status',
                Cell: () => (
                    <Chip color='primary' label='Restricted' size="small" variant="light" />
                )
            },

        ],
        []
    );

    // ----------------------- | API Call - USER RESTRICTION | ---------------------

    useEffect(() => {
        if (typeof userId == "undefined" || typeof branchId == "undefined") return;
        const queryParams: userRqueryParamsProps = {
            direction: "asc",
            page: 0,
            per_page: 1000,
            search: "",
            sort: "userId",
            branchId: branchId,
            userId: userId
        }
        dispatch(fetchRestrictionSuccess(queryParams))
    }, [userId, branchId, success])

    const [selectedIdsString, setSelectedIds] = useState<string>()

    const onSubmit = () => {
        if (selectedData.length === 0) {
            // Show a snackbar alert if no data is selected
            dispatch(
                openSnackbar({
                    open: true,
                    message: 'There are no User restrictions selected for this User',
                    variant: 'alert',
                    alert: {
                        color: 'error'
                    },
                    close: true
                })
            );
        } else {
            // Dispatch an action with the selected data to add user restrictions
            const payload = selectedData?.map((item) => ({
                branchId: branchId!,
                menuActionId: item,  // map selected menuActionId from the selectedData state
                statusId: 3, // Example statusId (can be dynamic based on your case)
                userId: userId!
            }));

            // Display the selected menuActionIds in the description
            setSelectedIds(selectedData.join(', ')); // Join IDs into a string

            dispatch(addUserRestrictionSuccess({ userRestrictionList: payload }));

            // Optionally: Show success snackbar immediately (if you don't rely on the response)
            dispatch(
                openSnackbar({
                    open: true,
                    message: `Permissions granted successfully for MenuActionIds: ${selectedIdsString}`,
                    variant: 'alert',
                    alert: {
                        color: 'success'
                    },
                    close: true
                })
            );
        }
    };



    useEffect(() => {
        if (!newuserRestrictionList) {
            settableData([])
            return
        }
        if (newuserRestrictionList == null) {
            settableData([])
            return
        }
        settableData(newuserRestrictionList.result!)
    }, [newuserRestrictionList])

    useEffect(() => {
        if (error != null) {

            let defaultErrorMessage = "ERROR";
            // @ts-ignore
            const errorExp = error as Template1Error
            if (errorExp.message) {
                defaultErrorMessage = errorExp.message
            }
            dispatch(
                openSnackbar({
                    open: true,
                    message: defaultErrorMessage,
                    variant: 'alert',
                    alert: {
                        color: 'error'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [error]);


    useEffect(() => {
        if (success != null) {
            dispatch(
                openSnackbar({
                    open: true,
                    message: success,
                    variant: 'alert',
                    alert: {
                        color: 'success'
                    },
                    close: true
                })
            );
            dispatch(toInitialState());
        }
    }, [success])

    useEffect(() => {
        if (isActionSuccess != null) {
            let actionId, actionName, keyValue, keyValueId, description;

            switch (isActionSuccess) {
                case 'LIST':
                    actionId = 2;
                    actionName = 'LIST';
                    keyValueId = 0;
                    keyValue = 'N/A';
                    description = 'Get All Menu List'
                    break;
                case 'APPROVE':
                    actionId = 6;
                    actionName = 'APPROVE';
                    keyValueId = 0;
                    keyValue = 'N/A';
                    description = `Grant Permissions: ${selectedIdsString}`;
                    break;
                case 'UPDATE':
                    actionId = 3;
                    actionName = 'UPDATE';
                    keyValue = selectedIdsString || 'N/A';
                    description = `Edit User Details: ${selectedIdsString}`;
                    break;
                case 'REJECT':
                    actionId = 7;
                    actionName = 'REJECT';
                    keyValueId = userId || 0;
                    keyValue = selectedIdsString || 'N/A';
                    description = `Restrict Actions: ${selectedIdsString}`;
                    break;
                default:
                    return; // Exit early if no valid action is found
            }
            dispatch(AddActivityLog({
                actionId: actionId,
                actionName: actionName,
                branchId: user?.branchList?.[0]?.branchId ?? undefined,
                companyId: user?.companyId,
                deptId: user?.departmentList?.[0]?.departmentId ?? undefined,
                description: description,
                keyValue: keyValue,
                keyValueId: keyValueId,
                menuId: 4,
                menuName: "User Restriction",
                deptName: user?.departmentList?.[0]?.deptName ?? undefined
            }));

            dispatch(toResetIsActionSuccessState());
        }

    }, [isActionSuccess]);



    if (isLoading) {
        return <>Loading...</>
    }

    return (
        <>
            <MainCard content={false}>
                <ScrollX>
                    <ReactTable selectedData={setselectedData} handleAddEdit={() => { }} columns={columns2} data={tabledata} getHeaderProps={(column: HeaderGroup) => column.getSortByToggleProps()} />
                </ScrollX>
            </MainCard>
            <Grid item xs={12} sm={12}>
                <Stack direction="row" spacing={2} justifyContent="right" alignItems="center" sx={{ p: 2.5, pb: 0 }}>
                    <Button color="error" onClick={() => { navigate(`/home/dashboard`) }}>
                        Cancel
                    </Button>
                    <Button variant="contained" sx={{ textTransform: 'none' }} onClick={() => { onSubmit() }}>
                        Grant Permission
                    </Button>
                </Stack>
            </Grid>
        </>
    );
}

export default RestrictedUserAccessTable;
