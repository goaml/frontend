import { useState } from 'react';

// material-ui
import {
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    Grid,
    InputLabel,
    Stack,
    TextField,
    Tooltip
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';
import AlertRoleDelete from './AlertRoleDelete';

// assets
import { DeleteFilled } from '@ant-design/icons';
import { dispatch } from 'store';
import { addRoleSuccess, updateRoleSuccess } from 'store/reducers/role';
import { Roles } from 'types/role';

// types

// constant
const getInitialValues = (roles: FormikValues | null) => {

    const newRole = {
        userRoleId: undefined,
        userRoleName: "",
        userRoleDesc: "",
        //isActive: true,
    }

    if (roles) {
        return _.merge({}, newRole, roles);
    }

    return newRole;
};

// ==============================|| ROLES ADD / EDIT ||============================== //

export interface Props {
    roles?: Roles
    onCancel: () => void;
}

const AddEditRoleStatus = ({ roles, onCancel }: Props) => {
    const isCreating = !roles;

    const [openAlert, setOpenAlert] = useState(false);

    const handleAlertClose = () => {
        setOpenAlert(!openAlert);
        onCancel();
    };

    const RoleSchema = Yup.object().shape({
        userRoleName: Yup.string().required('User Role Name is required'),
        userRoleDesc: Yup.string().required('User Role Description is required')
    });

    const formik = useFormik({
        initialValues: getInitialValues(roles!),
        validationSchema: RoleSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (roles) {
                    // PUT API 
                    dispatch(updateRoleSuccess({ ...values }))
                } else {
                    // POST API
                    dispatch(addRoleSuccess({
                        userRoleDesc: values.userRoleDesc,
                        userRoleName: values.userRoleName
                    }))
                }
                resetForm()
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{roles ? 'Edit Role' : 'Add Role'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                    <Grid container spacing={3}>
                                        <Grid item xs={12}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="userRoleName">User Role Name<span style={{ color: 'red' }}> *</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="userRoleName"
                                                    placeholder="Enter User Role Name"
                                                    {...getFieldProps('userRoleName')}
                                                    error={Boolean(touched.userRoleName && errors.userRoleName)}
                                                    helperText={touched.userRoleName && errors.userRoleName}
                                                />
                                            </Stack>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="userRoleDesc">User Role Description<span style={{ color: 'red' }}> *</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="userRoleDesc"
                                                    placeholder="Enter User Role Description"
                                                    {...getFieldProps('userRoleDesc')}
                                                    error={Boolean(touched.userRoleDesc && errors.userRoleDesc)}
                                                    helperText={touched.userRoleDesc && errors.userRoleDesc}
                                                />
                                            </Stack>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 2.5 }}>
                            <Grid container justifyContent="space-between" alignItems="center">
                                <Grid item>
                                    {!isCreating && (
                                        <Tooltip title="Delete Role" placement="top">
                                            <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                                                <DeleteFilled />
                                            </IconButton>
                                        </Tooltip>
                                    )}
                                </Grid>
                                <Grid item>
                                    <Stack direction="row" spacing={2} alignItems="center">
                                        <Button color="error" onClick={onCancel}>
                                            Cancel
                                        </Button>
                                        <Button type="submit" variant="contained" disabled={isSubmitting}>
                                            {roles ? 'Edit' : 'Add'}
                                        </Button>
                                    </Stack>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </Form>
                </LocalizationProvider>
            </FormikProvider>
            {!isCreating && <AlertRoleDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={roles.userRoleId!} />}
        </>
    );
};

export default AddEditRoleStatus;
