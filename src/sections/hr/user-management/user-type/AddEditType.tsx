import { useEffect, useState } from 'react';

// material-ui
import {
    Autocomplete,
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    FormHelperText,
    Grid,
    InputLabel,
    Stack,
    TextField,
    Tooltip,
    useTheme
} from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

// third-party
import { Form, FormikProvider, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

// project imports
import IconButton from 'components/@extended/IconButton';
import AlertTypeDelete from './AlertTypeDelete';

// assets
import { DeleteFilled } from '@ant-design/icons';
import { dispatch, useSelector } from 'store';
import { addUserType, updateUserType } from 'store/reducers/user-type';
import { UserType } from 'types/user-type';
import { fetchUserLoginTypeList } from 'store/reducers/login-type';
import { UserLoginType } from 'types/login-type';

// types

// constant
const getInitialValues = (Types: FormikValues | null) => {

    const newType = {
        userTypeId: undefined,
        usRLoginType: {
            typeCode: "",
            typeDescription: "",
        },
        userCategory: "",
        loginTypeId: undefined
        //isActive: true,
    }

    if (Types) {
        return _.merge({}, newType, Types);
    }

    return newType;
};

// ==============================|| TypeS ADD / EDIT ||============================== //

export interface Props {
    Types?: UserType
    onCancel: () => void;
}

const AddEditTypeStatus = ({ Types, onCancel }: Props) => {
    const isCreating = !Types;
    const theme = useTheme();
    const [openAlert, setOpenAlert] = useState(false);

    const handleAlertClose = () => {
        setOpenAlert(!openAlert);
        onCancel();
    };

    const TypeSchema = Yup.object().shape({
        userCategory: Yup.string().required('User Type Name is required'),
        loginTypeId: Yup.string().required('Login type ID is required'),
    });

    const formik = useFormik({
        initialValues: getInitialValues(Types!),
        validationSchema: TypeSchema,
        enableReinitialize: true,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            try {
                if (Types) {
                    // PUT API 
                    dispatch(updateUserType({ ...values }))
                } else {
                    // POST API
                    dispatch(addUserType({
                        loginTypeId: values.loginTypeId,
                        userCategory: values.userCategory
                    }))
                }
                resetForm()
                setSubmitting(false);
                onCancel();
            } catch (error) {
                console.error(error);
            }
        }
    });

    //SELECT TYPE FOR DROPDOWN
    const { selectUserLoginTypes, typesuccess } = useSelector((state) => state.loginType);
    useEffect(() => {
        dispatch(fetchUserLoginTypeList());
    }, [typesuccess, dispatch]);

    const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

    return (
        <>
            <FormikProvider value={formik}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <DialogTitle>{Types ? 'Edit User Type' : 'Add User Type'}</DialogTitle>
                        <Divider />
                        <DialogContent sx={{ p: 2.5 }}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} md={12}>
                                    <Grid container spacing={3}>
                                        <Grid item xs={6}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="loginTypeId">User Login Type <span style={{ color: 'red' }}>*</span></InputLabel>
                                                <Autocomplete
                                                    fullWidth
                                                    id="loginTypeId"
                                                    value={selectUserLoginTypes?.find((option1: UserLoginType) => option1.loginTypeId === formik.values.loginTypeId) || null}
                                                    onChange={(event: any, newValue: UserLoginType | null) => {
                                                        formik.setFieldValue('loginTypeId', newValue?.loginTypeId);
                                                    }}
                                                    options={selectUserLoginTypes || []}
                                                    getOptionLabel={(item) => `${item.typeCode}`}
                                                    renderInput={(params) => (
                                                        <TextField
                                                            {...params}
                                                            placeholder="Select User Login Type"
                                                            sx={{ '& .MuiAutocomplete-input.Mui-disabled': { WebkitTextFillColor: theme.palette.text.primary } }}
                                                        />

                                                    )}
                                                />
                                            </Stack>
                                            {formik.touched.loginTypeId && formik.errors.loginTypeId && (
                                                <FormHelperText error id="helper-text-userTypeId">
                                                    {formik.errors.loginTypeId}
                                                </FormHelperText>
                                            )}
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Stack spacing={1.25}>
                                                <InputLabel htmlFor="userCategory">User Type Name<span style={{ color: 'red' }}> *</span></InputLabel>
                                                <TextField
                                                    fullWidth
                                                    id="userCategory"
                                                    placeholder="Enter User Type Name"
                                                    {...getFieldProps('userCategory')}
                                                    error={Boolean(touched.userCategory && errors.userCategory)}
                                                    helperText={touched.userCategory && errors.userCategory}
                                                />
                                            </Stack>

                                        </Grid>

                                    </Grid>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <Divider />
                        <DialogActions sx={{ p: 2.5 }}>
                            <Grid container justifyContent="space-between" alignItems="center">
                                <Grid item>
                                    {!isCreating && (
                                        <Tooltip title="Delete Type" placement="top">
                                            <IconButton onClick={() => setOpenAlert(true)} size="large" color="error">
                                                <DeleteFilled />
                                            </IconButton>
                                        </Tooltip>
                                    )}
                                </Grid>
                                <Grid item>
                                    <Stack direction="row" spacing={2} alignItems="center">
                                        <Button color="error" onClick={onCancel}>
                                            Cancel
                                        </Button>
                                        <Button type="submit" variant="contained" disabled={isSubmitting}>
                                            {Types ? 'Edit' : 'Add'}
                                        </Button>
                                    </Stack>
                                </Grid>
                            </Grid>
                        </DialogActions>
                    </Form>
                </LocalizationProvider>
            </FormikProvider>
            {!isCreating && <AlertTypeDelete title={""} open={openAlert} handleClose={handleAlertClose} deleteId={Types.userTypeId!} />}
        </>
    );
};

export default AddEditTypeStatus;
