type logProps = {
    id?: number
    menuName?: string
    actionName?: string
    keyValue?: string
    description?: string
    userName?: string
    branchName?: string
    departmentName?: string
    date?: string
}

export const logsMockData: logProps[] = [
    {
        id: 1,
        menuName: "Sales",
        actionName: "Generate Invoice",
        keyValue: "INV001",
        description: "Create an invoice for a new sale",
        userName: "John Doe",
        branchName: "Colombo",
        departmentName: "Finance",
        date: "2024-01-06"
    },
    {
        id: 2,
        menuName: "HR",
        actionName: "New Employee Onboarding",
        keyValue: "EMP002",
        description: "Onboard a new employee to the company",
        userName: "Jane Smith",
        branchName: "Kandy",
        departmentName: "Human Resources",
        date: "2024-01-07"
    },
    {
        id: 3,
        menuName: "Inventory",
        actionName: "Restock Products",
        keyValue: "PROD003",
        description: "Replenish stock for popular products",
        userName: "Sam Wilson",
        branchName: "Galle",
        departmentName: "Operations",
        date: "2024-01-08"
    },
    {
        id: 4,
        menuName: "Marketing",
        actionName: "Launch Campaign",
        keyValue: "CAM004",
        description: "Start a new marketing campaign",
        userName: "Ella Johnson",
        branchName: "Jaffna",
        departmentName: "Marketing",
        date: "2024-01-09"
    },
    {
        id: 5,
        menuName: "IT",
        actionName: "Software Update",
        keyValue: "SW005",
        description: "Apply the latest software updates to servers",
        userName: "Mike Brown",
        branchName: "Matara",
        departmentName: "Information Technology",
        date: "2024-01-10"
    }
];

