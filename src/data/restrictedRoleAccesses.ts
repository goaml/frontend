export type restrictedRoleAccessType = {
    menuActionId?: number
    status?: boolean
    menuActionName?: string
    menuName?: string
    url?: string
}

export const restrictedRoleAccesses: restrictedRoleAccessType[] = [ 
    {
        menuActionId: 3,
        status: false,
        menuActionName: 'Delete Documents',
        menuName: 'Documents',
        url: '/documents/delete',
    },
    {
        menuActionId: 4,
        status: false,
        menuActionName: 'Access Admin Panel',
        menuName: 'Admin',
        url: '/admin/settings',
    },
];
