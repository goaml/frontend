export type roleType = {
    userRoleId?: number,
    roleApprvReason?: string,
    roleInBussiness?: string,
    statusFlag?: string,
    userRoleDesc?: string,
    userRoleName?: string,
    statusId?: number,
    usRStatusDetail?: {
        statusId?: number,
        statusCode?: string,
        statusDesc?: string,
        statusName?: string
    },
    ccode?: number
}

export const roles: roleType[] = [
    {
      userRoleId: 1,
      roleApprvReason: "Approval Reason 1",
      roleInBussiness: "Business Role 1",
      statusFlag: "Active",
      userRoleDesc: "Description 1",
      userRoleName: "Role Name 1",
      statusId: 1,
      usRStatusDetail: {
        statusId: 1,
        statusCode: "STA001",
        statusDesc: "Active",
        statusName: "Active"
      },
      ccode: 123
    },
    {
      userRoleId: 2,
      roleApprvReason: "Approval Reason 2",
      roleInBussiness: "Business Role 2",
      statusFlag: "Inactive",
      userRoleDesc: "Description 2",
      userRoleName: "Role Name 2",
      statusId: 2,
      usRStatusDetail: {
        statusId: 2,
        statusCode: "STA002",
        statusDesc: "Inactive",
        statusName: "Inactive"
      },
      ccode: 456
    },
    // Add more roleType objects as needed
  ];
  