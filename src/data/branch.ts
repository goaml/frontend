export type branchType = {
    branchId?: number,
    branchName?:String,
    description?:String,
    branchMobileNo?:String,
    branchEmail?:String,
    branchAddress?:String,
    statusName?: string,
    isActive?:boolean
}

export const branchs : branchType[] = [
    {
        branchId: 1,
        branchName:"Gampaha",
        description:"gampaha",
        branchMobileNo:"0771123422",
        branchEmail:"gampaha@gmail.com",
        branchAddress:"colombo road,Gampaha",
        statusName: "Active",
        isActive:true
    }
]

