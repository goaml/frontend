export type optionType = {
    key: number,
    value: string
}

export const options: optionType[] = [
    {
        key: 1,
        value: "Option-1"
    },
    {
        key: 2,
        value: "Option-2"
    }
]