export type restrictedUserAccessType = {
    menuActionId?: number;
    menuActionName?: string;
    moduleId?: number;
    moduleName?: string;
    menuId?: number;
    menuName?: string;
    actionId?: number;
    actionName?: string;
    statusId?: number;
}

export const restrictedUserAccesses: restrictedUserAccessType[] = [
    {
        menuActionId: 3,
        menuActionName: 'Delete Documents',
        moduleId: 3,
        moduleName: 'Documents',
        menuId: 301,
        menuName: 'All Documents',
        actionId: 3001,
        actionName: 'Delete',
        statusId: 2,
    },
    {
        menuActionId: 4,
        menuActionName: 'Access Admin Panel',
        moduleId: 4,
        moduleName: 'Admin',
        menuId: 401,
        menuName: 'Settings',
        actionId: 4001,
        actionName: 'Access',
        statusId: 2,
    },
];
