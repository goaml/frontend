export type grantedUserPermissionType = {
    menuActionId?: number;
    menuActionName?: string;
    moduleId?: number;
    moduleName?: string;
    menuId?: number;
    menuName?: string;
    actionId?: number;
    actionName?: string;
    statusId?: number;
}

export const grantedUserPermissions: grantedUserPermissionType[] = [
    {
        menuActionId: 1,
        menuActionName: 'View Dashboard',
        moduleId: 1,
        moduleName: 'Dashboard',
        menuId: 101,
        menuName: 'Overview',
        actionId: 1001,
        actionName: 'View',
        statusId: 1,
    },
    {
        menuActionId: 2,
        menuActionName: 'Edit Profile',
        moduleId: 2,
        moduleName: 'Profile',
        menuId: 201,
        menuName: 'Settings',
        actionId: 2001,
        actionName: 'Edit',
        statusId: 1,
    },
];
