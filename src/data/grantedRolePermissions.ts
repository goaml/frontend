export type grantedRolePermissionsType = {
    menuActionId?: number
    status?: boolean
    menuActionName?: string
    menuName?: string
    url?: string
}

export const grantedRolePermissions: grantedRolePermissionsType[] = [
    {
        menuActionId: 1,
        status: true,
        menuActionName: 'View Dashboard',
        menuName: 'Dashboard',
        url: '/dashboard',
    },
    {
        menuActionId: 2,
        status: true,
        menuActionName: 'Edit Profile',
        menuName: 'Profile',
        url: '/profile/settings',
    },
];
