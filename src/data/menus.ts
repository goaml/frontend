type menuProps = {
    id?: number
    menuActionId: number,
    name: string,
    url: string,
    isMenu: boolean,
    moduleName: string,
    statusName: string,
    actionName: string,
    menuId: number,
    moduleId: number,
    statusId: number,
    actionId: number,
    usMAction: {
        created_by: string,
        created_on: string,
        updated_by: string,
        updated_on: string,
        actionId: number,
        actionCode: string,
        actionName: string,
        actionStatus: string,
        statusId: number,
        usRStatusDetail: {
            statusId: string,
            statusCode: string,
            statusDesc: string,
            statusName: string
        }
    },
    usMMenu: {
        created_by: string,
        created_on: string,
        updated_by: string,
        updated_on: string,
        menuId: number,
        menuDesc: string,
        menuHashcode: string,
        menuName: string,
        menuUrl: string,
        menuIcon: string,
        seqNo: number,
        statusId: number,
        subGroupId: number,
        usRStatusDetail: {
            statusId: number,
            statusCode: string,
            statusDesc: string,
            statusName: string
        },
        usMMenuSubGroup: {
            subGroupId: number,
            subGroupName: string,
            seqNo: string,
            menuGroupId: number,
            usMMenuGroup: {
                menuGroupId: number,
                menuGroupName: string,
                seqNo: number,
                moduleId: number,
                usMModule: {
                    created_by: string,
                    created_on: string,
                    updated_by: string,
                    updated_on: string,
                    moduleId: number,
                    moduleName: string
                }
            }
        }
    },
    usMModule: {
        created_by: string,
        created_on: string,
        updated_by: string,
        updated_on: string,
        moduleId: number,
        moduleName: string
    },
    usRStatusDetail: {
        statusId: string,
        statusCode: string,
        statusDesc: string,
        statusName: string
    }
}

export const menusMockData: menuProps[] = [
    {
        id: 1,
        menuActionId: 101,
        name: "Dashboard",
        url: "/dashboard",
        isMenu: true,
        moduleName: "Admin",
        statusName: "Active",
        actionName: "View",
        menuId: 201,
        moduleId: 301,
        statusId: 401,
        actionId: 501,
        usMAction: {
            created_by: "John Doe",
            created_on: "2024-01-06",
            updated_by: "Jane Smith",
            updated_on: "2024-01-07",
            actionId: 501,
            actionCode: "VIEW_DASHBOARD",
            actionName: "View",
            actionStatus: "Active",
            statusId: 401,
            usRStatusDetail: {
                statusId: "401",
                statusCode: "ACT",
                statusDesc: "Active",
                statusName: "Active",
            },
        },
        usMMenu: {
            created_by: "Jane Smith",
            created_on: "2024-01-08",
            updated_by: "Mike Brown",
            updated_on: "2024-01-09",
            menuId: 201,
            menuDesc: "Main Dashboard",
            menuHashcode: "HASH123",
            menuName: "Dashboard",
            menuUrl: "/dashboard",
            menuIcon: "dashboard-icon",
            seqNo: 1,
            statusId: 401,
            subGroupId: 501,
            usRStatusDetail: {
                statusId: 401,
                statusCode: "ACT",
                statusDesc: "Active",
                statusName: "Active",
            },
            usMMenuSubGroup: {
                subGroupId: 501,
                subGroupName: "Main",
                seqNo: "1",
                menuGroupId: 601,
                usMMenuGroup: {
                    menuGroupId: 601,
                    menuGroupName: "Main Group",
                    seqNo: 1,
                    moduleId: 301,
                    usMModule: {
                        created_by: "Mike Brown",
                        created_on: "2024-01-10",
                        updated_by: "Ella Johnson",
                        updated_on: "2024-01-11",
                        moduleId: 301,
                        moduleName: "Admin",
                    },
                },
            },
        },
        usMModule: {
            created_by: "Ella Johnson",
            created_on: "2024-01-12",
            updated_by: "Sam Wilson",
            updated_on: "2024-01-13",
            moduleId: 301,
            moduleName: "Admin",
        },
        usRStatusDetail: {
            statusId: "401",
            statusCode: "ACT",
            statusDesc: "Active",
            statusName: "Active",
        },
    }
];

