import React, { ReactNode, createContext, useContext, useState } from 'react';
import { NavItemType } from 'types/menu';


interface MenuContextProps {
  menuItems: { items: NavItemType[] };
  updateMenuItems: any
}

const MenuContext = createContext<MenuContextProps | undefined>(undefined);

interface MenuProviderProps {
  children: ReactNode;
}

export const MenuProvider: React.FC<MenuProviderProps> = ({ children }) => {
  const [menuItems, setMenuItems] = useState<{ items: NavItemType[] }>({ items: [] });

  const updateMenuItems = (transformedMenuItems: NavItemType[]) => {
    setMenuItems({ items: transformedMenuItems });
  };

  return (
    <MenuContext.Provider value={{ menuItems, updateMenuItems }}>
      {children}
    </MenuContext.Provider>
  );
};

export const useMenu = () => {
  const context = useContext(MenuContext);
  if (!context) {
    throw new Error('useMenu must be used within a MenuProvider');
  }
  return context;
};
