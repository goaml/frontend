// third-party
import { FormattedMessage } from 'react-intl';

// assets
import { SettingOutlined, FileOutlined, OrderedListOutlined,BranchesOutlined, BuildOutlined } from '@ant-design/icons';

// type
import { NavItemType } from 'types/menu';

// icons
const icons = { SettingOutlined, FileOutlined, OrderedListOutlined ,BranchesOutlined, BuildOutlined}


// ==============================|| MENU ITEMS - APPLICATION MANAGEMENT ||============================== //

const para: NavItemType = {
    id: 'para',
    title: <FormattedMessage id="para" />,
    type: 'group',
    children: [
        {
            id: 'parameter-management',
            title: <FormattedMessage id="parameter-management" />,
            type: 'collapse',
            icon: icons.SettingOutlined,
            children: [
                {
                    id: 'view-category',
                    title: <FormattedMessage id="view-category" />,
                    type: 'item',
                    url: '/parameter-management/view-category',
                    icon: icons.SettingOutlined,
                },
                {
                    id: 'multi-party-role',
                    title: <FormattedMessage id="multi-party-role" />,
                    type: 'item',
                    url: '/parameter-management/multi-party-role',
                    icon: icons.SettingOutlined,
                },
                {
                    id: 'report-type',
                    title: <FormattedMessage id="report-type" />,
                    type: 'item',
                    url: '/parameter-management/report-type',
                    icon: icons.SettingOutlined,
                },
                {
                    id: 'fund-type',
                    title: <FormattedMessage id="fund-type" />,
                    type: 'item',
                    url: '/parameter-management/fund-type',
                    icon: icons.SettingOutlined,
                },
                // {
                //     id: 'fund-type-description',
                //     title: <FormattedMessage id="fund-type-description" />,
                //     type: 'item',
                //     url: '/parameter-management/fund-type-description',
                //     icon: icons.SettingOutlined,
                // },
                {
                    id: 'rpt-code',
                    title: <FormattedMessage id="rpt-code" />,
                    type: 'item',
                    url: '/parameter-management/rpt-code',
                    icon: icons.SettingOutlined,
                },
                {
                    id: 'gl-account-mapping',
                    title: <FormattedMessage id="gl-account-mapping" />,
                    type: 'item',
                    url:'/parameter-management/gl-account-mapping',
                    icon: icons.SettingOutlined,
                },
                // {
                //     id: 'transaction-code',
                //     title: <FormattedMessage id="transaction-code" />,
                //     type: 'item',
                //     url: '/parameter-management/transaction-code',
                //     icon: icons.SettingOutlined,
                // },
                {
                    id: 'transaction-code',
                    title: <FormattedMessage id="transaction-code" />,
                    type: 'item',
                    url: '/parameter-management/goAMLTrxCode',
                    icon: icons.SettingOutlined,
                },
                {
                    id: 'Branch Code',
                    title: <FormattedMessage id="branch" />,
                    type: 'item',
                    icon: icons.BranchesOutlined,
                    url:'/parameter-management/branchCode'
                    
                },
                {
                    id: 'Department Code',
                    title: <FormattedMessage id="dept" />,
                    type: 'item',
                    icon: icons.BuildOutlined,
                    url:'/parameter-management/departmentCode'
                    
                },
                {
                    id: 'Submission Type',
                    title: <FormattedMessage id="submit-type" />,
                    type: 'item',
                    icon: icons.SettingOutlined,
                    url:'/parameter-management/submission-type'
                    
                },
                {
                    id: 'Account Type',
                    title: <FormattedMessage id="account-type" />,
                    type: 'item',
                    icon: icons.SettingOutlined,
                    url:'/parameter-management/account-type'
                    
                },
                {
                    id: 'Account Status Type',
                    title: <FormattedMessage id="accStatus-type" />,
                    type: 'item',
                    icon: icons.SettingOutlined,
                    url:'/parameter-management/accountStatus-type'
                    
                },
                {
                    id: 'Identifier Type',
                    title: <FormattedMessage id="identifier" />,
                    type: 'item',
                    icon: icons.SettingOutlined,
                    url:'/parameter-management/identifier-type'
                    
                },
                {
                    id: 'Conduction Type',
                    title: <FormattedMessage id="conduction" />,
                    type: 'item',
                    icon: icons.SettingOutlined,
                    url:'/parameter-management/conduction-type'
                    
                },
                {
                    id: 'Contact Type',
                    title: <FormattedMessage id="contact-type" />,
                    type: 'item',
                    icon: icons.SettingOutlined,
                    url:'/parameter-management/contact-type'
                    
                },
                {
                    id: 'Communication Type',
                    title: <FormattedMessage id="com-type" />,
                    type: 'item',
                    icon: icons.SettingOutlined,
                    url:'/parameter-management/com-type'
                    
                },
                {
                    id: 'Entity Legal Form Type',
                    title: <FormattedMessage id="entity-form" />,
                    type: 'item',
                    icon: icons.SettingOutlined,
                    url:'/parameter-management/entity-type'
                    
                },
                {
                    id: 'Currency Type',
                    title: <FormattedMessage id="currency" />,
                    type: 'item',
                    icon: icons.SettingOutlined,
                    url:'/parameter-management/currency-type'
                    
                },
                {
                    id: 'Account Person Role Type',
                    title: <FormattedMessage id="acc-PRtype" />,
                    type: 'item',
                    icon: icons.SettingOutlined,
                    url:'/parameter-management/accPRole-type'
                    
                },
                {
                    id: 'Entity Person Role Type',
                    title: <FormattedMessage id="entity-PRtype" />,
                    type: 'item',
                    icon: icons.SettingOutlined,
                    url:'/parameter-management/entityPRole-type'
                    
                },
                {
                    id: 'Gender Type',
                    title: <FormattedMessage id="gender-type" />,
                    type: 'item',
                    icon: icons.SettingOutlined,
                    url:'/parameter-management/gender-type'
                    
                },
                {
                    id: 'Party Type',
                    title: <FormattedMessage id="party-type" />,
                    type: 'item',
                    icon: icons.SettingOutlined,
                    url:'/parameter-management/party-type'
                    
                },
                {
                    id: 'Report Indicator Type',
                    title: <FormattedMessage id="report-indi-type" />,
                    type: 'item',
                    icon: icons.SettingOutlined,
                    url:'/parameter-management/report-indi-type'
                    
                },
                {
                    id: 'Status Type',
                    title: <FormattedMessage id="status-type" />,
                    type: 'item',
                    icon: icons.SettingOutlined,
                    url:'/parameter-management/status-type'
                    
                },
                {
                    id: 'City Code',
                    title: <FormattedMessage id="city-code" />,
                    type: 'item',
                    icon: icons.SettingOutlined,
                    url:'/parameter-management/city-code'
                    
                },
                {
                    id: 'Limit',
                    title: <FormattedMessage id="Limit" />,
                    type: 'item',
                    icon: icons.SettingOutlined,
                    url:'/parameter-management/limit'
                    
                },
            ]
        },
        {
            id: 'transaction-management',
            title: <FormattedMessage id="transaction-management" />,
            type: 'collapse',
            icon: icons.SettingOutlined,
            children: [
                // {
                //     id: 'trans-from',
                //     title: <FormattedMessage id="trans-from" />,
                //     type: 'item',
                //     url: '/transaction-management/trans-from',
                //     icon: icons.SettingOutlined,
                // },
                // {
                //     id: 'trans-to',
                //     title: <FormattedMessage id="trans-to" />,
                //     type: 'item',
                //     url: '/transaction-management/trans-to',
                //     icon: icons.SettingOutlined,
                // },
                // {
                //     id: 'trans-from-mc',
                //     title: <FormattedMessage id="trans-from-mc" />,
                //     type: 'item',
                //     url: '/transaction-management/trans-from-myClient',
                //     icon: icons.SettingOutlined,
                // },
                // {
                //     id: 'trans-to-mc',
                //     title: <FormattedMessage id="trans-to-mc" />,
                //     type: 'item',
                //     url: '/transaction-management/trans-to-myClient',
                //     icon: icons.SettingOutlined,
                // },
                {
                    id: 'trans-report',
                    title: <FormattedMessage id="trans-report" />,
                    type: 'item',
                    url: '/transaction-management/trans-report',
                    icon: icons.SettingOutlined,
                },
                // {
                //     id: 'trans',
                //     title: <FormattedMessage id="trans" />,
                //     type: 'item',
                //     url: '/transaction-management/trans',
                //     icon: icons.SettingOutlined,
                // },
                {
                    id: 'trans-activity',
                    title: <FormattedMessage id="trans-activity" />,
                    type: 'item',
                    url: '/transaction-management/trans-activity',
                    icon: icons.SettingOutlined,
                },
                {
                    id: 'trans-address',
                    title: <FormattedMessage id="trans-address" />,
                    type: 'item',
                    url: '/transaction-management/trans-address',
                    icon: icons.SettingOutlined,
                },
                // {
                //     id: 'tp-myclient',
                //     title: <FormattedMessage id="tp-myclient" />,
                //     type: 'item',
                //     url: '/transaction-management/tp-myclient',
                //     icon: icons.SettingOutlined,
                // },
                // {
                //     id: 'te-myclient',
                //     title: <FormattedMessage id="te-myclient" />,
                //     type: 'item',
                //     url: '/transaction-management/te-myclient',
                //     icon: icons.SettingOutlined,
                // },
                // {
                //     id: 'ta-myclient',
                //     title: <FormattedMessage id="ta-myclient" />,
                //     type: 'item',
                //     url: '/transaction-management/ta-myclient',
                //     icon: icons.SettingOutlined,
                // },
                {
                    id: 'f-currency',
                    title: <FormattedMessage id="f-currency" />,
                    type: 'item',
                    url: '/transaction-management/f-currency',
                    icon: icons.SettingOutlined,
                },
                {
                    id: 'trans-party',
                    title: <FormattedMessage id="trans-party" />,
                    type: 'item',
                    url: '/transaction-management/trans-party',
                    icon: icons.SettingOutlined,
                },
                {
                    id: 'trans-phone',
                    title: <FormattedMessage id="trans-phone" />,
                    type: 'item',
                    url: '/transaction-management/trans-phone',
                    icon: icons.SettingOutlined,
                },
                {
                    id: 'trans-personId',
                    title: <FormattedMessage id="trans-personId" />,
                    type: 'item',
                    url: '/transaction-management/trans-personId',
                    icon: icons.SettingOutlined,
                },
                
                
            ]
            }
    ]
};

export default para;
