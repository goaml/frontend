// third-party
import { FormattedMessage } from 'react-intl';

// assets
import { SettingOutlined, FileOutlined, OrderedListOutlined } from '@ant-design/icons';

// type
import { NavItemType } from 'types/menu';

// icons
const icons = { SettingOutlined, FileOutlined, OrderedListOutlined }

// ==============================|| MENU ITEMS - APPLICATION MANAGEMENT ||============================== //

const app: NavItemType = {
    id: 'app',
    title: <FormattedMessage id="app" />,
    type: 'group',
    children: [
        {
            id: 'application-management',
            title: <FormattedMessage id="application-management" />,
            type: 'collapse',
            icon: icons.SettingOutlined,
            children: [
                {
                    id: 'global',
                    title: <FormattedMessage id="global" />,
                    type: 'item',
                    url: '/applications/global-configuration',
                    icon: icons.SettingOutlined,
                },
                {
                    id: 'note-log',
                    title: <FormattedMessage id="note-log" />,
                    type: 'item',
                    url: '/applications/logs',
                    icon: icons.FileOutlined,
                },
                {
                    id: 'trans-inq',
                    title: <FormattedMessage id="trans-inq" />,
                    type: 'item',
                    url: '/applications/transaction',
                    icon: icons.FileOutlined,
                },
                // {
                //     id: 'goaml-Inquiry',
                //     title: <FormattedMessage id="goaml-Inquiry" />,
                //     type: 'item',
                //     url: '/applications/goAML-Inq',
                //     icon: icons.FileOutlined,
                // },
                {
                    id: 'xml-report',
                    title: <FormattedMessage id="xml-report" />,
                    type: 'item',
                    url: '/applications/xml-report',
                    icon: icons.FileOutlined,
                },
                {
                    id: 'export-data',
                    title: <FormattedMessage id="export-data" />,
                    type: 'item',
                    url: '/applications/export-data',
                    icon: icons.FileOutlined,
                },
                {
                    id: 'upload-data',
                    title: <FormattedMessage id="upload-data" />,
                    type: 'item',
                    url: '/applications/upload-data',
                    icon: icons.FileOutlined,
                },
                {
                    id: 'xml-file-log',
                    title: <FormattedMessage id="xml-file-log" />,
                    type: 'item',
                    url: '/applications/xml-file-log',
                    icon: icons.FileOutlined,
                },
                {
                    id: 'xml-validate-report',
                    title: <FormattedMessage id="xml-validate-report" />,
                    type: 'item',
                    url: '/applications/xml-validate-report',
                    icon: icons.FileOutlined,
                },
                {
                    id: 'trans-err-log',
                    title: <FormattedMessage id="trans-err-log" />,
                    type: 'item',
                    url: '/applications/trans-err-log',
                    icon: icons.FileOutlined,
                },
                {
                    id: 'xml-report-backup',
                    title: <FormattedMessage id="xml-report-backup" />,
                    type: 'item',
                    url: '/applications/xml-report-backup',
                    icon: icons.FileOutlined,
                },
                {
                    id: 'trans-err-summary-log',
                    title: <FormattedMessage id="trans-err-summary-log" />,
                    type: 'item',
                    url: '/applications/trans-err-summary-log',
                    icon: icons.FileOutlined,
                },
            ]
        },
    ]
};

export default app;
