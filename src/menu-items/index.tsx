// project import
import home from './home';

// types
import { NavItemType } from 'types/menu';
import hr from './userManagement/hr';
import app from './applicationManagement/app';
import para from './parameterManagement/parameter';
import link from './link';


// ==============================|| MENU ITEMS ||============================== //

const menuItems: { items: NavItemType[] } = {
  items: [home, hr, app, para, link]
};

export default menuItems;
