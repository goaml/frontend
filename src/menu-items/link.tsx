// third-party
import { DashboardOutlined } from '@ant-design/icons';
import { FormattedMessage } from 'react-intl';

// assets

// type
import { NavItemType } from 'types/menu';

// icons
const icons = { DashboardOutlined };
// ==============================|| MENU ITEMS - SUPPORT ||============================== //

const link: NavItemType = {
    id: 'link',
    title: <FormattedMessage id="link" />,
    type: 'group',
    children: [
        {
            id: 'cbsl',
            title: <FormattedMessage id="cbsl" />,
            type: 'item',
            url: '/link/cbsl',
            icon: icons.DashboardOutlined,
        }
    ]
};

export default link;
