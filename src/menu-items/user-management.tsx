// third-party
import { FormattedMessage } from 'react-intl';

// assets
import { TeamOutlined } from '@ant-design/icons';

// type
import { NavItemType } from 'types/menu';

// icons
const icons = { TeamOutlined };

// ==============================|| MENU ITEMS - user-management ||============================== //

const usermanagement: NavItemType = {
    id: 'user-management',
    title: <FormattedMessage id="user-management" />,
    type: 'group',
    children: [
        {
            id: 'list',
            title: <FormattedMessage id="list" />,
            type: 'item',
            url: '/user-management/list',
            icon: icons.TeamOutlined,
        }
    ]
};

export default usermanagement;
