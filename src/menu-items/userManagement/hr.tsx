// third-party
import { FormattedMessage } from 'react-intl';

// assets
import { UserOutlined, UserSwitchOutlined, SolutionOutlined, StopOutlined, SecurityScanOutlined, ApartmentOutlined, GlobalOutlined } from '@ant-design/icons';

// type
import { NavItemType } from 'types/menu';

// icons
const icons = { UserOutlined, UserSwitchOutlined, SolutionOutlined, StopOutlined, SecurityScanOutlined, ApartmentOutlined, GlobalOutlined };

// ==============================|| MENU ITEMS - hr ||============================== //

const hr: NavItemType = {
    id: 'hr',
    title: <FormattedMessage id="hr" />,
    type: 'group',
    children: [
        {
            id: 'user-management',
            title: <FormattedMessage id="user-management" />,
            type: 'collapse',
            icon: icons.UserOutlined,
            children: [
                {
                    id: 'users',
                    title: <FormattedMessage id="users" />,
                    type: 'item',
                    url: '/hr/user-management/users',
                    icon: icons.UserOutlined,
                },
                {
                    id: 'user-authorization',
                    title: <FormattedMessage id="user-authorization" />,
                    type: 'item',
                    url: '/hr/user-management/user-authorization',
                    icon: icons.UserSwitchOutlined,
                },
                {
                    id: 'user-restriction',
                    title: <FormattedMessage id="user-restriction" />,
                    type: 'item',
                    url: '/hr/user-management/user-restriction',
                    icon: icons.StopOutlined,
                },
                {
                    id: 'role-creation',
                    title: <FormattedMessage id="role-creation" />,
                    type: 'item',
                    url: '/hr/user-management/role-creation',
                    icon: icons.SolutionOutlined,
                },
                {
                    id: 'role-restriction',
                    title: <FormattedMessage id="role-restriction" />,
                    type: 'item',
                    url: '/hr/user-management/role-restriction',
                    icon: icons.SecurityScanOutlined,
                },
                {
                    id: 'user-type',
                    title: <FormattedMessage id="user-type" />,
                    type: 'item',
                    url: '/hr/user-management/user-type',
                    icon: icons.UserSwitchOutlined,
                },
                {
                    id: 'multiple-branches',
                    title: <FormattedMessage id="multiple-branches" />,
                    type: 'item',
                    url: '/hr/user-management/multiple-branches',
                    icon: icons.ApartmentOutlined,
                },
                {
                    id: 'global-para',
                    title: <FormattedMessage id="global-para" />,
                    type: 'item',
                    url: '/hr/user-management/global',
                    icon: icons.GlobalOutlined
                },
                {
                    id: 'menu-action-list',
                    title: <FormattedMessage id="menu-action-list" />,
                    type: 'item',
                    url: '/hr/user-management/menus',
                    icon: icons.GlobalOutlined,
                }
            ]
        },
    ]
};

export default hr;
