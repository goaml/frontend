import { lazy } from 'react';

// project import
import Loadable from 'components/Loadable';
import CommonLayout from 'layout/CommonLayout';
import MainLayout from 'layout/MainLayout';
import AuthGuard from 'utils/route-guard/AuthGuard';


// pages routing
const AuthLogin = Loadable(lazy(() => import('pages/auth/login')));
const AuthRegister = Loadable(lazy(() => import('pages/auth/register')));
const AuthForgotPassword = Loadable(lazy(() => import('pages/auth/forgot-password')));
const AuthResetPassword = Loadable(lazy(() => import('pages/auth/reset-password')));
const AuthCheckMail = Loadable(lazy(() => import('pages/auth/check-mail')));
const AuthCodeVerification = Loadable(lazy(() => import('pages/auth/code-verification')));

const MaintenanceError = Loadable(lazy(() => import('pages/maintenance/404')));
const MaintenanceError500 = Loadable(lazy(() => import('pages/maintenance/500')));
const MaintenanceUnderConstruction = Loadable(lazy(() => import('pages/maintenance/under-construction')));
const MaintenanceComingSoon = Loadable(lazy(() => import('pages/maintenance/coming-soon')));

// render - home page
const Dashboard = Loadable(lazy(() => import('pages/home/dashboard')));

// render - user management page
const List = Loadable(lazy(() => import('pages/user-management/sample-page/list/List')));
const Create = Loadable(lazy(() => import('pages/user-management/sample-page/create/Create')));

// hr routing
const UserOverview = Loadable(lazy(() => import('pages/hr/user-management/user/list')));
const UserRoleOverview = Loadable(lazy(() => import('pages/hr/user-management/user-role/list')));
const UserAuthorizationOverview = Loadable(lazy(() => import('pages/hr/user-management/user-authorization/list')));
const UserTypeOverview = Loadable(lazy(() => import('pages/hr/user-management/user-type/list')));
const UserRestriction = Loadable(lazy(() => import('pages/hr/user-management/user-restriction/view')));
const UserRoleRestriction = Loadable(lazy(() => import('pages/hr/user-management/user-role-restriction/view')));
const HrUserManagementMultipleBranches = Loadable(lazy(() => import('pages/hr/user-management/multiple-branches/list/list')));
const GlobalOverview = Loadable(lazy(() => import('pages/hr/user-management/global/create-edit-view')));
const ViewProfile = Loadable(lazy(() => import('pages/hr/user-management/profile-view/ViewProfile')));
const EditProfile = Loadable(lazy(() => import('pages/hr/user-management/profile-edit/EditProfile')));
const PasswordChange = Loadable(lazy(() => import('pages/hr/user-management/password-change/PasswordChange')))

// applications routing
const GlobalConfiguration = Loadable(lazy(() => import('pages/applications/global-configuration')));
const Logs = Loadable(lazy(() => import('pages/applications/note-log/logs')));
const Menus = Loadable(lazy(() => import('pages/applications/menu-action/menus')));
const ThemeSettings = Loadable(lazy(() => import('sections/applications/global-configuration/ThemeSettings')));
const ApplicationSettings = Loadable(lazy(() => import('sections/applications/global-configuration/ApplicationSettings/ApplicationSettings')))
const TransactionInquiry = Loadable(lazy(() => import('pages/applications/transaction-Inquiry/inquiry')))
const GoAMLInquiry = Loadable(lazy(() => import('pages/applications/goaml-inquiry/list/list')))
const XMLReport = Loadable(lazy(() => import('pages/applications/xml-report/List')))
const ExportData = Loadable(lazy(() => import('pages/applications/export-data/List')))
const UploadData = Loadable(lazy(() => import('pages/applications/upload-data/list')))
const XMLFileGenerationLog = Loadable(lazy(() => import('pages/applications/XMLFileGenerationLog/log')))
const XMLValidateReport = Loadable(lazy(() => import('pages/applications/xml-validate-report/List')))
const TransactionErrorLog = Loadable(lazy(() => import('pages/applications/trans-err-log/errLog')))
const XMLBackupReport = Loadable(lazy(() => import('pages/applications/xml-report-backup/List')))
const TransactionErrorSummaryLog = Loadable(lazy(() => import('pages/applications/trans-err-summary-log/summaryLog')))

//parameter routing
const BranchOverview = Loadable(lazy(() => import('pages/parameter-management/branch-code/list/list')))
const DeptOverview = Loadable(lazy(() => import('pages/parameter-management/departments/list/list')))

//parameter routing
const CategoryCreate = Loadable(lazy(() => import('pages/parameter-management/category/create/Create')));
const CategoryView = Loadable(lazy(() => import('pages/parameter-management/category/list/List')));
const MultiPartyRoleView = Loadable(lazy(() => import('pages/parameter-management/multi-party-role/list/List')));
const MultiPartyFundCode = Loadable(lazy(() => import('pages/parameter-management/multi-party-fund-code/list/List')));
const MultiPartyInvolveParty = Loadable(lazy(() => import('pages/parameter-management/multi-party-involve-party/list/List')));
const ReportType = Loadable(lazy(() => import('pages/parameter-management/report-type/list/List')));
const FundType = Loadable(lazy(() => import('pages/parameter-management/fund-type/list/List')));
const FundTypeDescrition = Loadable(lazy(() => import('pages/parameter-management/fund-type-description/list/List')));
const RPTCode = Loadable(lazy(() => import('pages/parameter-management/rpt-code/list/List')));
const TransactionCode = Loadable(lazy(() => import('pages/parameter-management/transaction-code/list/List')));
const GoAMLTrxCode = Loadable(lazy(() => import('pages/parameter-management/goaml-trx-code/list/List')));
const BusinessNature = Loadable(lazy(() => import('pages/parameter-management/business-nature/list/list')));
const BusinessSubNature = Loadable(lazy(() => import('pages/parameter-management/sub-business-nature/list/list')));
const SubmissionCode = Loadable(lazy(() => import('pages/parameter-management/submission-type/list')));
const AccountType = Loadable(lazy(() => import('pages/parameter-management/account-type/list')));
const AccountStatusType = Loadable(lazy(() => import('pages/parameter-management/accoutStatus-type/list')));
const IdentifierType = Loadable(lazy(() => import('pages/parameter-management/identifier-type/list')));
const ConductionType = Loadable(lazy(() => import('pages/parameter-management/conduction-type/list')));
const ContactType = Loadable(lazy(() => import('pages/parameter-management/contact-type/list')));
const CommunicationType = Loadable(lazy(() => import('pages/parameter-management/communication-type/list')));
const EntityLegalFormType = Loadable(lazy(() => import('pages/parameter-management/entity-legal-form/list')));
const CurrencyType = Loadable(lazy(() => import('pages/parameter-management/currency-code/list')));
const AccountPRole = Loadable(lazy(() => import('pages/parameter-management/account-PRole/list')));
const EntityPRole = Loadable(lazy(() => import('pages/parameter-management/entity-PRole/list')));
const GenderType = Loadable(lazy(() => import('pages/parameter-management/gender-type/list')));
const PartyType = Loadable(lazy(() => import('pages/parameter-management/party-type/list')));
const ReportIndicatorType = Loadable(lazy(() => import('pages/parameter-management/report-indicator/list')));
const StatusType = Loadable(lazy(() => import('pages/parameter-management/status-code/list')));
const CityType = Loadable(lazy(() => import('pages/parameter-management/city-code/list')));
const TransFrom = Loadable(lazy(() => import('pages/transaction/trans-from/list')));
const TransTo = Loadable(lazy(() => import('pages/transaction/trans-to/list')));
const TransFromMyClient = Loadable(lazy(() => import('pages/transaction/trans-from-myClient/list')));
const TransToMyClient = Loadable(lazy(() => import('pages/transaction/trans-to-myClient/list')));
const TransReport = Loadable(lazy(() => import('pages/transaction/trans-report/list')));
const TransActivity = Loadable(lazy(() => import('pages/transaction/trans-activity/list')));
const TransAddress = Loadable(lazy(() => import('pages/transaction/trans-address/list')));
const TransPhone = Loadable(lazy(() => import('pages/transaction/trans-phone/list')));
const Trans = Loadable(lazy(() => import('pages/transaction/trans-transaction/list')));
const TPMyclient = Loadable(lazy(() => import('pages/transaction/tPerson-myclient/list')));
const TEMyclient = Loadable(lazy(() => import('pages/transaction/tEntity-myclient/list')));
const TAMyclient = Loadable(lazy(() => import('pages/transaction/tAcc-mycient/list')));
const FCurrency = Loadable(lazy(() => import('pages/transaction/foreign-currency/list')));
const TransParty = Loadable(lazy(() => import('pages/transaction/trans-party/list')));
const TransPersonId = Loadable(lazy(() => import('pages/transaction/trans-personIdentity/list')));
const Limit = Loadable(lazy(() => import('pages/parameter-management/limit/limit')));
const GLAccountMapping = Loadable(lazy(() => import('pages/parameter-management/gl-account-mapping/list/List')));
const CBSL = Loadable(lazy(() => import('pages/link/cbsl/cbsl-link')));

//DWH routing
const CustomerSummaryMonthly = Loadable(lazy(() => import('pages/dwh/customer_summary/list')));
const TransactioncodeSummaryDaily = Loadable(lazy(() => import('pages/dwh/transactioncode_summary_daily/list')));
const CustomerSummaryMonthlyTrxcodeWise = Loadable(lazy(() => import('pages/dwh/customer_summarry_monthly_trxcode_wise/list')));
const TransactioncodeSummaryMonthly = Loadable(lazy(() => import('pages/dwh/transactioncode_summary_monthly/list')));

// ==============================|| MAIN ROUTING ||============================== //

const MainRoutes = {
  path: '/',
  children: [
    {
      path: '/',
      element: (
        <AuthGuard>
          <MainLayout />
        </AuthGuard>
      ),
      children: [
        {
          path: 'home',
          children: [
            {
              path: 'dashboard',
              element: <Dashboard />
            }
          ]
        },
        {
          path: '/hr',
          children: [
            {
              path: 'user-management',
              children: [
                {
                  path: 'users',
                  element: <UserOverview />
                },
                {
                  path: 'user-authorization',
                  element: <UserAuthorizationOverview />
                },
                {
                  path: 'user-restriction',
                  element: <UserRestriction />
                },
                {
                  path: 'role-creation',
                  element: <UserRoleOverview />
                },
                {
                  path: 'role-restriction',
                  element: <UserRoleRestriction />
                },
                {
                  path: 'multiple-branches',
                  element: <HrUserManagementMultipleBranches />
                },
                {
                  path: 'global',
                  element: <GlobalOverview />
                },
                {
                  path: 'view-profile',
                  element: <ViewProfile />
                },
                {
                  path: 'edit-profile',
                  element: <EditProfile />
                },
                {
                  path: 'password-change',
                  element: <PasswordChange />
                },
                {
                  path: 'user-type',
                  element: <UserTypeOverview />
                },
                {
                  path: 'menus',
                  element: <Menus />
                },
              ]
            }
          ]
        },
        {
          path: 'applications',
          children: [
            {
              path: 'global-configuration',
              element: <GlobalConfiguration />
            },
            {
              path: 'global-configuration/theme-settings',
              element: <ThemeSettings />
            },
            {
              path: 'global-configuration/application-settings',
              element: <ApplicationSettings />
            },
            // {
            //   path: 'menus',
            //   element: <Menus />
            // },
            {
              path: 'logs',
              element: <Logs />
            },
            {
              path: 'transaction',
              element: <TransactionInquiry />
            },
            {
              path: 'goAML-Inq',
              element: <GoAMLInquiry />
            },
            {
              path: 'xml-report',
              element: <XMLReport />
            },
            {
              path: 'export-data',
              element: <ExportData />
            },
            {
              path: 'upload-data',
              element: <UploadData />
            },
            {
              path: 'xml-file-log',
              element: < XMLFileGenerationLog />
            },
            {
              path: 'xml-validate-report',
              element: <XMLValidateReport />
            },
            {
              path: 'trans-err-log',
              element: <TransactionErrorLog />
            },
            {
              path: 'xml-report-backup',
              element: <XMLBackupReport />
            },
            {
              path: 'trans-err-summary-log',
              element: <TransactionErrorSummaryLog />
            },
            {
              path: 'customer-summary',
              element: <CustomerSummaryMonthly />
            },
            {
              path: 'trxcode-summary-daily',
              element: <TransactioncodeSummaryDaily />
            },
            {
              path: 'customer-summary-monthly-trxcode-wise',
              element: <CustomerSummaryMonthlyTrxcodeWise />
            },
            {
              path: 'trxcode-summary-monthly',
              element: <TransactioncodeSummaryMonthly />
            },
          ]
        },
        {
          path: 'user-management',
          children: [
            {
              path: 'list',
              element: <List />
            },
            {
              path: 'create',
              element: <Create />
            }
          ]
        },
        {
          path: 'parameter-management',
          children: [
            {
              path: 'create-category',
              element: <CategoryCreate />
            },
            {
              path: 'view-category',
              element: <CategoryView />
            },
            {
              path: 'multi-party-role',
              element: <MultiPartyRoleView />
            },
            {
              path: 'multi-party-fund-code',
              element: <MultiPartyFundCode />
            },
            {
              path: 'multi-party-involve-party',
              element: <MultiPartyInvolveParty />
            },
            {
              path: 'report-type',
              element: <ReportType />
            },
            {
              path: 'fund-type',
              element: <FundType />
            },
            {
              path: 'fund-type-description',
              element: <FundTypeDescrition />
            },
            {
              path: 'rpt-code',
              element: <RPTCode />
            },
            {
              path: 'transaction-code',
              element: <TransactionCode />
            },
            {
              path: 'goAMLTrxCode',
              element: <GoAMLTrxCode />
            },
            {
              path: 'branchCode',
              element: <BranchOverview />
            },
            {
              path: 'departmentCode',
              element: <DeptOverview />
            },
            {
              path: 'business-nature',
              element: <BusinessNature />
            },
            {
              path: 'sub-business-nature',
              element: <BusinessSubNature />
            },
            {
              path: 'submission-type',
              element: <SubmissionCode />
            },
            {
              path: 'accountStatus-type',
              element: <AccountStatusType />
            },
            {
              path: 'account-type',
              element: <AccountType />
            },
            {
              path: 'identifier-type',
              element: <IdentifierType />
            },
            {
              path: 'conduction-type',
              element: <ConductionType />
            },
            {
              path: 'contact-type',
              element: <ContactType />
            },
            {
              path: 'com-type',
              element: <CommunicationType />
            },
            {
              path: 'entity-type',
              element: <EntityLegalFormType />
            },
            {
              path: 'currency-type',
              element: <CurrencyType />
            },
            {
              path: 'accPRole-type',
              element: <AccountPRole />
            },
            {
              path: 'entityPRole-type',
              element: <EntityPRole />
            },
            {
              path: 'gender-type',
              element: <GenderType />
            },
            {
              path: 'party-type',
              element: <PartyType />
            },
            {
              path: 'report-indi-type',
              element: <ReportIndicatorType />
            },
            {
              path: 'status-type',
              element: <StatusType />
            },
            {
              path: 'city-code',
              element: <CityType />
            },
            {
              path: 'limit',
              element: <Limit />
            },
            {
              path: 'gl-account-mapping',
              element: <GLAccountMapping />
            }
          ]
        },
        {
          path: 'transaction-management',
          children: [
            {
              path: 'trans-from',
              element: <TransFrom />
            },
            {
              path: 'trans-to',
              element: <TransTo />
            },
            {
              path: 'trans-from-myClient',
              element: <TransFromMyClient />
            },
            {
              path: 'trans-to-myClient',
              element: <TransToMyClient />
            },
            {
              path: 'trans-report',
              element: <TransReport />
            },
            {
              path: 'trans-activity',
              element: <TransActivity />
            },
            {
              path: 'trans/:reportId',
              element: <Trans />
            },
            {
              path: 'tp-myclient',
              element: <TPMyclient />
            },
            {
              path: 'te-myclient',
              element: <TEMyclient />
            },
            {
              path: 'ta-myclient',
              element: <TAMyclient />
            },
            {
              path: 'f-currency',
              element: <FCurrency />
            },
            {
              path: 'trans-party',
              element: <TransParty />
            },
            {
              path: 'trans-address',
              element: <TransAddress />
            },
            {
              path: 'trans-phone',
              element: <TransPhone />
            },
            {
              path: 'trans-personId',
              element: <TransPersonId />
            },
          ]
        },
        {
          path: 'link',
          children: [
            {
              path: 'cbsl',
              element: <CBSL />
            }
          ]
        },

      ]
    },
    {
      path: '/maintenance',
      element: <CommonLayout />,
      children: [
        {
          path: '404',
          element: <MaintenanceError />
        },
        {
          path: '500',
          element: <MaintenanceError500 />
        },
        {
          path: 'under-construction',
          element: <MaintenanceUnderConstruction />
        },
        {
          path: 'coming-soon',
          element: <MaintenanceComingSoon />
        }
      ]
    },
    {
      path: '/auth',
      element: <CommonLayout />,
      children: [
        {
          path: 'login',
          element: <AuthLogin />
        },
        {
          path: 'register',
          element: <AuthRegister />
        },
        {
          path: 'forgot-password',
          element: <AuthForgotPassword />
        },
        {
          path: 'reset-password',
          element: <AuthResetPassword />
        },
        {
          path: 'check-mail',
          element: <AuthCheckMail />
        },
        {
          path: 'code-verification',
          element: <AuthCodeVerification />
        }
      ]
    },
  ]
};

export default MainRoutes;
